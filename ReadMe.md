## Project Setup
* Follow instructions from https://facebook.github.io/react-native/docs/getting-started.html (React Native CLI Quickstart tab)

## Setting up on Mac
* install homebrew (https://brew.sh/)
* `brew install yarn`
* `brew cask install fastlane`
* add fastlane to PATH -- in `.bashrc` add `export PATH="$HOME/.fastlane/bin:$PATH"
* `fastlane match development --readonly` (url is git@gitlab.com:taboobat/scribe-certificates.git) -- only if you have access for production builds ;)
* `fastlane match appstore --readonly` (url is git@gitlab.com:taboobat/scribe-certificates.git) -- only if you have access for production builds ;)
* `sudo gem install cocoapods`
* run `yarn` in scribe directory
* if RN Tooltips gives pod errors, `cd node_modules/react-native-tooltips && pod install`

## Building
* Android - `./gradlew bundleRelease`
