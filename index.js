import './config/ReactotronConfig';
import { AppRegistry } from 'react-native';
import App from './app/app';

AppRegistry.registerComponent('scribe', () => App);
