/**
 * Run with 'node scripts/translations.js'
 */
const fs = require('fs');

const NEWLINE = '\r\n'; // dumbass windows line separator

let args = process.argv.splice(2, process.argv.length - 2);

let dirPath = __dirname + "\\translation-files";
let debug = false;

if(args[0] == "-d") {
    debug = true;
}

let translations = {};

fs.readdir(dirPath, (err, files) => {
    for(let i = 0; i < files.length; i++) {
        let data = fs.readFileSync(dirPath+'/'+files[i], 'utf8');
        let lines = data.split(NEWLINE);
        let locales = lines.splice(0, 1)[0].split(',');
        lines.forEach(line => {
            let entries = processEntries(line.split(','));
            let hasContent = false;
            for(let i = 1; i < entries.length; i++) {
                if(entries[i] !== '') {
                    hasContent = true;
                    break;
                }
            }

            if(hasContent) {
                translations[entries[0]] = {};

                for(let i = 1; i < locales.length; i++) {
                    if(entries[i] !== "") {
                        translations[entries[0]][locales[i]] = entries[i];
                    }
                }
            }
        });
    }
    if(debug) {
        console.log(translations);
    } else {
        let contents = "export default translations = " + JSON.stringify(translations);
        fs.writeFile(__dirname+"\\..\\app\\helpers\\translations.js", contents, (err) => {
            if(err) {
                console.log("Error during execution: "+err);
            } else {
                console.log("Translations generated");
            }
        });
    }

});

function processEntries(entries) {
    let processed = [];

    let open = false;
    let pieces = [];
    for(let i = 0; i < entries.length; i++) {
        if(entries[i].charAt(0) == '"') {
            open = true;
            pieces.push(entries[i].slice(1, entries[i].length));
            continue;
        }

        if(open) {
            if(entries[i].charAt(entries[i].length - 1) == '"') {
                pieces.push(entries[i].slice(0, entries[i].length - 1));
                processed.push(pieces.join(','));
                pieces = [];
                open = false;
            } else {
                pieces.push(entries[i]);
            }
        } else {
            processed.push(entries[i]);
        }
    }

    return processed;
}
