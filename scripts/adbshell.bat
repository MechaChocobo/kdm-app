:: Inpired by Windows version https://gist.github.com/thebagchi/df29ae862fc1c296dec2
:: Which is inspired by Linux version of the same https://gist.github.com/christopherperry/3208109

@echo off
SET ARGUMENTS=%*

if "%ARGUMENTS%" == "" (
    GOTO EOF
)

SET "ARGUMENTS=%ARGUMENTS:""="%"

SETLOCAL ENABLEDELAYEDEXPANSION
:: EXECUTE SHELL COMMAND ON ALL ATTACHED DEVICES ::
FOR /F "tokens=1,2 skip=1" %%A IN ('adb devices') DO (
	SET IS_DEV=%%B
	if "!IS_DEV!" == "device" (
	    SET SERIAL=%%A
	    echo "adb -s !SERIAL! shell %ARGUMENTS%"
	    call adb -s !SERIAL! shell %ARGUMENTS%
	)
)
ENDLOCAL
:EOF
