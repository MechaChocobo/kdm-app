/**
 * Run with 'node scripts/processNameCsv.js'
 */
const fs = require('fs');

const NEWLINE = '\r\n'; // dumbass windows line separator

let args = process.argv.splice(2, process.argv.length - 2);

let dirPath = __dirname + "\\name-files";
let debug = false;

if(args[0] == "-d") {
    debug = true;
}

let maleNames = {};
let femaleNames = {};

fs.readdir(dirPath, (err, files) => {
    for(let i = 0; i < files.length; i++) {
        let nameList = {};
        let data = fs.readFileSync(dirPath+'/'+files[i], 'utf8');
        let lines = data.split(NEWLINE);
        let categories = lines.splice(0, 1)[0].split(',');

        lines.forEach(line => {
            let entries = processEntries(line.split(','));

            for(let i = 0; i < categories.length; i++) {
                if(entries[i]) {
                    if(!nameList[categories[i]]) {
                        nameList[categories[i]] = [];
                    }
                    nameList[categories[i]].push(entries[i]);
                }
            }
        });

        if(files[i] === "Male.csv") {
            maleNames = {...nameList};
        } else {
            femaleNames = {...nameList};
        }
    }

    if(debug) {
        console.log(maleNames);
        console.log(femaleNames);
    } else {
        let contents = "export default maleNames = " + JSON.stringify(maleNames);
        fs.writeFile(__dirname+"\\..\\app\\data\\maleNames.js", contents, (err) => {
            if(err) {
                console.log("Error during execution: "+err);
            } else {
                console.log("Male names extracted");
            }
        });

        contents = "export default femaleNames = " + JSON.stringify(femaleNames);
        fs.writeFile(__dirname+"\\..\\app\\data\\femaleNames.js", contents, (err) => {
            if(err) {
                console.log("Error during execution: "+err);
            } else {
                console.log("Female names extracted");
            }
        });
    }
});

function processEntries(entries) {
    let processed = [];

    let open = false;
    let pieces = [];
    for(let i = 0; i < entries.length; i++) {
        if(entries[i].charAt(0) == '"') {
            open = true;
            pieces.push(entries[i].slice(1, entries[i].length));
            continue;
        }

        if(open) {
            if(entries[i].charAt(entries[i].length - 1) == '"') {
                pieces.push(entries[i].slice(0, entries[i].length - 1));
                processed.push(pieces.join(','));
                pieces = [];
                open = false;
            } else {
                pieces.push(entries[i]);
            }
        } else {
            processed.push(entries[i]);
        }
    }

    return processed;
}
