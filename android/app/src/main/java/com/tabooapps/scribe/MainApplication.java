package com.tabooapps.scribe;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.horcrux.svg.SvgPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.reactlibrary.RNTooltipsPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.bugsnag.BugsnagReactNative;
import cl.json.RNSharePackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.sbugert.rnadmob.RNAdMobPackage;
import com.idehub.Billing.InAppBillingBridgePackage;
import com.corbt.keepawake.KCKeepAwakePackage;
import com.filepicker.FilePickerPackage;
import com.rnfs.RNFSPackage;
import com.lwansbrough.RCTCamera.RCTCameraPackage;
import com.pusherman.networkinfo.RNNetworkInfoPackage;
import com.peel.react.TcpSocketsModule;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new SvgPackage(),
            new RNCWebViewPackage(),
            new RNTooltipsPackage(),
            new LinearGradientPackage(),
            BugsnagReactNative.getPackage(),
            new RNSharePackage(),
            new RNDeviceInfo(),
            new RNAdMobPackage(),
            new InAppBillingBridgePackage(),
            new KCKeepAwakePackage(),
            new FilePickerPackage(),
            new RNFSPackage(),
            new RCTCameraPackage(),
            new RNNetworkInfoPackage(),
            new TcpSocketsModule()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
