'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import Popup from './shared/popup';
import ActionLog from './shared/ActionLog';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import RoundControl from './shared/buttons/RoundControl';

class TimelineActionLog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPopup: false,
        }
    }

    onPopupClosed = () => {
        this.setState({showPopup: false});
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    render() {
        return (
            <View>
                <RoundControl icon="format-list-bulleted-type" onPress={this.showPopup} size={30}/>
                <Popup title={t("Action Log")} visible={this.state.showPopup} onDismissed={this.onPopupClosed}>
                    <ActionLog actions={this.props.logs}></ActionLog>
                </Popup>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        logs: state.timelineReducer.logs[props.id],
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineActionLog);
