'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Drawer from 'react-native-drawer';
import CompareVersions from 'compare-versions';
import { AdMobBanner } from 'react-native-admob';
import InAppBilling from "react-native-billing"; // billing wrapper for android
import DeviceInfo from 'react-native-device-info';
import { MarkdownView } from 'react-native-markdown-view';
import iapReceiptValidator from 'iap-receipt-validator';
import { createTransition, SlideLeft, SlideRight } from 'react-native-transition';

import {
    StyleSheet,
    View,
    TouchableOpacity,
    BackHandler,
    Platform,
    ScrollView,
    Easing,
} from 'react-native';

import {changePage, goBack, setPage, setGoingBack, setPageTransitioning, setMenuDrawerOpen} from '../actions/router';
import {reset} from '../actions/global';
import {validateSchema, setVersion, setLocale} from '../actions/settings';

import Home from './home';
import Settlement from './settlement';
import SurvivorHome from './survivor-home';
import Survivor from './survivor';
import SurvivorList from './survivor-list';
import CreateSettlement from './settlement-create';
import CreateSurvivor from './survivor-create';
import Timeline from './timeline';
import Network from './network';
import Settings from './settings';
import About from './about';
import Purchases from './purchases';
import CustomExpansions from './custom-expansions';
import CustomExpansion from './custom-expansion';
import Storage from './Storage';
import SettlementUpgrades from './SettlementUpgrades';
import NavBar from './nav-bar';
import { setShouldShowAds, setIsSubscriber } from '../actions/purchases';
import Button from './shared/button';
import markdownStyles from '../style/markdown';
import { getSupportedLocales, t, setCachedLocale } from '../helpers/intl';
import CreateExpansion from './custom-expansions/edit-components/CreateExpansion';
import { getColors } from '../selectors/general';
import GView from './shared/GView';
import TimelineLy from './TimelineLy';
import LightModal from './shared/LightModal';
import AppText from './shared/app-text';
import styles from '../style/styles';
import KeyboardHider from './shared/KeyboardHider';
import { isNetworked, shouldShowErrorControls } from '../selectors/network';
import NetworkErrorControls from './NetworkErrorControls';
import SettlementSettings from './settlement/SettlementSettings';
import GlobalMilestones from './GlobalMilestones';
import Glossary from './Glossary';
let packageInfo = require('../../package.json');

const Transition = createTransition(SlideLeft);

class Router extends Component {
    drawer = null;
    network = null;
    popup = null;

    constructor(props) {
        super(props);
        this.state = {
            showWhatsNew: false,
        };

        for(let key in markdownStyles) {
            markdownStyles[key].color = this.props.colors.TEXT;
        }
    }

    componentWillMount = () => {
        // sets up back handler for Android back button
        BackHandler.addEventListener('hardwareBackPress', () => {
            if(this.props.numPages > 1) {
                this.props.goBack();
                return true;
            } else {
                return false;
            }
        });

        // issues reset to wipe certain properties of reducers
        this.props.reset();

        // version check -- if we're coming to a new version try to update schema
        if(this.props.currentVersion && CompareVersions(this.props.currentVersion, packageInfo.version) === -1) {
            this.props.validateSchema(this.props.currentVersion);
            if(packageInfo.patchNotes !== "") {
                this.setState({showWhatsNew: true});
            }
        }

        // set version to the current version
        this.props.setVersion(packageInfo.version);

        if(!this.props.locale) {
            let locale = DeviceInfo.getDeviceLocale();
            if(Platform.OS === "android" && locale.length > 2) {
                locale = locale.substr(0, 2); // gets the first 2, e.g. en-US -> en
            }

            let supportedLocales = getSupportedLocales();

            if(supportedLocales[locale]) {
                this.props.setLocale(locale);
            }
        } else {
            setCachedLocale(this.props.locale);
        }

        // validate purchases and decide whether to show ads/subscriber features
        if(!DeviceInfo.isEmulator()) {
            if(Platform.OS === "android") {
                InAppBilling.open().then(() => {
                    InAppBilling.loadOwnedPurchasesFromGoogle().then(() => {
                        InAppBilling.listOwnedSubscriptions().then((results) => {
                            let isSubscriber = false;
                            for(let i = 0; i < results.length; i++) {
                                if(
                                    results[i] === "basic" ||
                                    results[i] === "advanced" ||
                                    results[i] === "godamongmen"
                                ) {
                                    isSubscriber = true;
                                }
                            }

                            if(isSubscriber === true) {
                                this.props.setIsSubscriber(isSubscriber);
                            }

                            return InAppBilling.listOwnedProducts().then((results) => {
                                let isSubscriber = false;
                                let shouldShowAds = true;

                                for(let i = 0; i < results.length; i++) {
                                    if(results[i] === "noads") {
                                        shouldShowAds = false;
                                    } else if(results[i] === "lifetimesubscription") {
                                        isSubscriber = true;
                                    }
                                }

                                if(isSubscriber === true) {
                                    this.props.setIsSubscriber(isSubscriber);
                                } else if(shouldShowAds === false) {
                                    this.props.setShouldShowAds(shouldShowAds);
                                }

                                return InAppBilling.close();
                            });
                        });
                    });
                });
            } else if(Platform.OS === "ios") {
                const sharedSecret = "0da4cf34be0448ff9e24be9634930920";
                const production = true;
                const validateReceipt = iapReceiptValidator(sharedSecret, production);

                let noads = false;
                for(let i = 0; i < this.props.purchasedItems.length; i++) {
                    if(this.props.purchasedItems[i].productIdentifier === "lifetimesubscription") {
                        this.props.setIsSubscriber(true);
                        break;
                    } else if(this.props.purchasedItems[i].productIdentifier === "noads_ios") {
                        noads = true;
                    } else if(
                        this.props.purchasedItems[i].productIdentifier === "basic" ||
                        this.props.purchasedItems[i].productIdentifier === "advanced" ||
                        this.props.purchasedItems[i].productIdentifier === "godamongmen"
                    ) {
                        this.validate(this.props.purchasedItems[i].transactionReceipt, validateReceipt);
                    }
                }

                if(noads === true) {
                    this.props.setShouldShowAds(false);
                }
            }
        }
    }

    async validate(receiptData, validateReceipt) {
        try {
            const validationData = await validateReceipt(receiptData);
            let valid = false;

            if(validationData.latest_receipt_info.expires_date > Date.now()) {
                valid = true;
            }

            if(valid) {
                this.props.setIsSubscriber(true);
            }
        } catch(err) {
            // console.warn(err.valid, err.error, err.message);
        }
    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress');
    }

    componentDidUpdate(prevProps) {
        if(prevProps.page !== this.props.page && (prevProps.page !== "network" && this.props.page !== "network")) {
            this.props.setPageTransitioning(true);
            Transition.show(this.getPage(), this.props.goingBack ? SlideRight : SlideLeft);
            if(this.props.goingBack === true) {
                this.props.setGoingBack(false);
            }
        }
    }

    onTransitionComplete = () => {
        this.props.setPageTransitioning(false);
    }

    getPage = () => {
        let page = null;
        if (this.props.page === 'home') {
            page = <Home />;
        } else if (this.props.page === 'settlement') {
            page = <Settlement />;
        } else if (this.props.page === 'survivor-home') {
            page = <SurvivorHome />;
        } else if (this.props.page === 'survivor-list') { // departing survivors
            page = <SurvivorList />;
        } else if (this.props.page === 'survivor') {
            page = <Survivor />;
        } else if (this.props.page === 'create-settlement') {
            page = <CreateSettlement />;
        } else if (this.props.page === 'create-survivor') {
            page = <CreateSurvivor />;
        } else if (this.props.page === 'timeline') {
            page = <Timeline />;
        } else if (this.props.page === 'settings') {
            page = <Settings />;
        } else if (this.props.page === 'about') {
            page = <About />;
        } else if (this.props.page === 'purchases') {
            page = <Purchases />;
        } else if (this.props.page === 'custom-expansions') {
            page = <CustomExpansions />;
        } else if (this.props.page === 'custom-expansion') {
            page = <CustomExpansion />;
        } else if (this.props.page === 'create-expansion') {
            page = <CreateExpansion />;
        } else if (this.props.page === 'storage') {
            page = <Storage />;
        } else if (this.props.page === 'settlement-upgrades') {
            page = <SettlementUpgrades />;
        } else if (this.props.page === 'timeline-ly') {
            page = <TimelineLy />;
        } else if (this.props.page === 'settlement-settings') {
            page = <SettlementSettings />;
        } else if (this.props.page === 'global-milestones') {
            page = <GlobalMilestones />;
        } else if (this.props.page === 'glossary') {
            page = <Glossary />;
        }

        return page;
    }

    goToPage = (page) => {
        return () => {
            this.drawer.close();
            this.props.changePage(page);
        };
    }

    drawerContent = () => {
        let links = [{
            type: "item",
            link: <TouchableOpacity style={localStyles.menuItem} onPress={this.goToPage("home")}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>{t("Home")}</AppText></TouchableOpacity>
        }];

        links.push({type: "item", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.goToPage("network")}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>{t("Network")}</AppText></TouchableOpacity>});

        if(!this.props.isNetworked && this.props.hostIp) {
            links.push({type: "subItem", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.reconnectClient}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>- {t("Reconnect to Last Host")}</AppText></TouchableOpacity>});
        }

        if(this.props.isClient) {
            links.push({type: "subItem", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.disconnectClient}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>- {t("Disconnect")}</AppText></TouchableOpacity>});
        }

        if(this.props.isHost) {
            links.push({type: "subItem", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.stopHosting}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>- {t("Stop Hosting")}</AppText></TouchableOpacity>});
        }

        links.push({type: "item", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.goToPage("custom-expansions")}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>{t("Custom Expansions")}</AppText></TouchableOpacity>});
        links.push({type: "item", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.goToPage("settings")}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>{t("Settings")}</AppText></TouchableOpacity>});
        links.push({type: "item", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.goToPage("glossary")}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>{t("Glossary")}</AppText></TouchableOpacity>});
        links.push({type: "item", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.goToPage("purchases")}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>{t("Purchases")}</AppText></TouchableOpacity>});
        links.push({type: "item", link: <TouchableOpacity style={localStyles.menuItem} onPress={this.goToPage("about")}><AppText color={this.props.colors.INVERT_TEXT} style={[localStyles.menuText, localStyles.textPadding]}>{t("About")}</AppText></TouchableOpacity>});

        let linkContent = links.map((value, index) => {
            return <View
                key={index}
                style={value.type === "item" ? localStyles.menuItemWrapper : localStyles.menuSubItemWrapper}
            >
                    {value.link}
            </View>
        });

        return <View style={[localStyles.menuWrapper, {backgroundColor: this.props.colors.MENU}]}>
            <View style={localStyles.linkWrapper}>
                {linkContent}
            </View>
            <View style={localStyles.versionWrapper}>
                <AppText color={this.props.colors.INVERT_TEXT}>{t("App Version %version%", {version: packageInfo.version})}</AppText>
            </View>
        </View>
    };

    stopHosting = () => {
        this.network.stopServer();
        this.drawer.close();
    }

    disconnectClient = () => {
        this.network.stopJoining();
        this.drawer.close();
    }

    reconnectClient = () => {
        this.network.reconnectClient(false);
        this.drawer.close();
    }

    menuDrawerClosed = () => {
        this.props.setMenuDrawerOpen(false);
    }

    hideWhatsNew = () => {
        this.setState({showWhatsNew: false});
    }

    render() {
        let page = this.getPage();

        return (
            <Drawer
                ref={(ref) => this.drawer = ref}
                type="displace"
                side="right"
                tapToClose={true}
                open={this.props.menuDrawerOpen}
                openDrawerOffset={.3}
                style={drawerStyles}
                panCloseMask={0.3}
                panOpenMask={0.2}
                closedDrawerOffset={-3}
                content={this.drawerContent()}
                acceptPan={false}
                onClose={this.menuDrawerClosed}
                tweenHandler={(ratio) => ({
                    main: { opacity: (2 - ratio) / 2 }
                })}
                >
                <View style={{flex: 1}}>
                    <GView style={{flex: 1}} colorType="background">
                        {this.props.showNetworkError &&
                            <NetworkErrorControls reconnectFn={this.reconnectClient} onDismiss={this.onNetworkErrorDismiss} />}
                        {page && <Transition onTransitioned={this.onTransitionComplete} duration={300} easing={Easing.ease}>
                            {page}
                        </Transition>}
                        <Network onRef={(ref) => this.network = ref} render={this.props.page === 'network'}/>
                        <KeyboardHider>
                            <NavBar />
                            {this.props.shouldShowAds === true && <AdMobBanner
                                adSize="smartBannerPortrait"
                                adUnitID={Platform.select({ios: "ca-app-pub-2214302154361646/4128171950", android: "ca-app-pub-2214302154361646/2541909574"})}
                                testDevices={[AdMobBanner.simulatorId]}
                                // onAdFailedToLoad={error => console.warn(error)}
                            />}
                        </KeyboardHider>
                    </GView>
                    <LightModal
                        visible={this.state.showWhatsNew}
                        onDismissed={this.hideWhatsNew}
                    >
                        <View style={styles.width100}>
                            <View style={localStyles.popupWrapper}>
                                <AppText style={localStyles.popupTitle}>{t("What's New in %version%", {version: packageInfo.version})}</AppText>
                            </View>
                            <ScrollView style={localStyles.scrollWrapper}>
                                <View onStartShouldSetResponder={() => true}>
                                    <MarkdownView styles={markdownStyles}>{packageInfo.patchNotes}</MarkdownView>
                                    <View style={{height: 50}}/>
                                </View>
                            </ScrollView>
                        </View>
                        <View style={styles.width100}>
                            <Button title={t("Got It")} onPress={this.hideWhatsNew} color={this.props.colors.HIGHLIGHT} shape="rounded"/>
                        </View>
                    </LightModal>
                </View>
            </Drawer>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementName = (state.pageReducer.settlementId ? state.settlementReducer.settlements[state.pageReducer.settlementId].name : "");
    return {
        page: state.pageReducer.page[state.pageReducer.page.length - 1].page,
        goingBack: state.pageReducer.goingBack,
        menuDrawerOpen: state.pageReducer.menuDrawerOpen,
        numPages: state.pageReducer.page.length,
        settlementId: state.pageReducer.settlementId,
        settlementName,
        isHost: state.networkReducer.server !== null,
        isClient: state.networkReducer.client !== null && state.networkReducer.clientError === "",
        hostIp: state.networkReducer.hostIpAddress,
        networkLoading: state.networkReducer.loading,
        currentVersion: state.settingsReducer.version,
        shouldShowAds: state.purchases.shouldShowAds,
        purchasedItems: state.purchases.purchasedItems,
        locale: state.settingsReducer.locale,
        showNetworkError: shouldShowErrorControls(state),
        isNetworked: isNetworked(state),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changePage,
        goBack,
        reset,
        setPage,
        validateSchema,
        setVersion,
        setShouldShowAds,
        setIsSubscriber,
        setLocale,
        setGoingBack,
        setPageTransitioning,
        setMenuDrawerOpen,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Router);

const drawerStyles = {
    drawer: { shadowColor: 'black', shadowOpacity: .8, shadowRadius: 3 },
    main: { paddingLeft: 3},
}

var localStyles = StyleSheet.create({
    menuItem : {
        width: "100%",
        height: "100%",
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: "5%",
    },
    menuItemWrapper : {
        width: "100%",
        height: "8%",
    },
    menuSubItemWrapper : {
        width: "100%",
        height: "8%",
        paddingLeft: "15%",
    },
    menuWrapper: {
        height: "100%",
        width: "100%",
        flex: 1,
    },
    menuText: {
        fontSize: 21,
        lineHeight: 26
    },
    textPadding: {
        paddingRight: 5
    },
    linkWrapper: {
        flex: .7,
        justifyContent: "space-between",
        marginTop: 40,
        marginLeft: 20
    },
    versionWrapper: {
        flex: .3,
        alignItems: "center",
        justifyContent: "flex-end",
        marginBottom: 20
    },
    popupWrapper: {
        alignSelf: "center",
        marginBottom: 20,
    },
    popupTitle: {
        fontSize: 26,
        fontWeight: "bold"
    },
    scrollWrapper: {
        maxHeight: "80%"
    }
});
