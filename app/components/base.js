'use strict';

import React, { Component } from 'react';
import {
    Easing,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createTransition, SlideLeft } from 'react-native-transition';

import SplashScreen from './splash-screen';
import { getColors } from '../selectors/general';
import { setTheme } from '../actions/settings';
import ColorContext from '../context/ColorContext';
import AppWrapper from './AppWrapper';
import { doIntegrityCheck } from '../actions/global';
import { setCustomExpansionConfigs } from '../helpers/helpers';

const Transition = createTransition(SlideLeft);

class Base extends Component {
    componentDidUpdate(prevProps) {
        if(prevProps.bootstrapped === false && this.props.bootstrapped === true) {
            // set expansion configs before the schema validation happens
            setCustomExpansionConfigs(Object.values(this.props.customExpansions));
            this.props.doIntegrityCheck();
            setTimeout(() => {
                Transition.show(<AppWrapper />);
            }, 2500);
        }
    }

    render() {
        return <ColorContext.Provider value={this.props.colors}>
            <Transition duration={300} easing={Easing.ease}><SplashScreen /></Transition>
        </ColorContext.Provider>
    }
};

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
        theme: state.settingsReducer.theme,
        customExpansions: state.customExpansions,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setTheme,
        doIntegrityCheck,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Base);
