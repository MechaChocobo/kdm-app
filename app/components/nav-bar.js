'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import NavButton from './shared/nav-button';
import { goToSettlement, changePage } from '../actions/router';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';

class NavBar extends Component {
    render() {
        if(!this.props.settlementId || !(
            this.props.page === "settlement" ||
            this.props.page === "survivor-home" ||
            this.props.page === "survivor-list" ||
            this.props.page === "survivor" ||
            this.props.page === "create-survivor" ||
            this.props.page === "storage" ||
            this.props.page === "settlement-upgrades" ||
            this.props.page === "settlement-settings" ||
            this.props.page === "timeline" ||
            this.props.page === "timeline-ly"
        )) {
            return null;
        }

        return (
            <View style={{flexDirection: "row", backgroundColor: this.props.colors.NAV_BAR_BACKGROUND, justifyContent: "space-between"}}>
                <View style={styles.buttonWrapper}>
                    <NavButton
                        label={t("settlement")}
                        icon="sk-house"
                        onPress={this.goToSettlement}
                        selected={this.props.page === "settlement" || this.props.page === "settlement-settings"}
                    />
                </View>
                <View style={styles.buttonWrapper}>
                    <NavButton
                        label={t("innovations")}
                        icon="sk-lightbulb"
                        onPress={this.goToSettlementUpgrades}
                        selected={this.props.page === "settlement-upgrades"}
                    />
                </View>
                <View style={styles.buttonWrapper}>
                    <NavButton
                        label={t("storage")}
                        icon="sk-chest"
                        onPress={this.goToStorage}
                        selected={this.props.page === "storage"}
                    />
                </View>
                <View style={styles.buttonWrapper}>
                    <NavButton
                        label={t("survivors")}
                        icon="sk-survivor-head"
                        onPress={this.goToSurvivors}
                        selected={this.props.page === "survivor-home" || this.props.page === "survivor-list" || this.props.page === "survivor" || this.props.page === "create-survivor"}
                    />
                </View>
                <View style={styles.buttonWrapper}>
                    <NavButton
                        label={t("timeline")}
                        icon="format-list-bulleted"
                        onPress={this.goToTimeline}
                        selected={this.props.page === "timeline" || this.props.page === "timeline-ly"}
                    />
                </View>
            </View>
        );
    }

    goToSettlement = () => {
        this.props.goToSettlement(this.props.settlementId);
    }

    goToSurvivors = () => {
        this.props.changePage("survivor-home");
    }

    goToTimeline = () => {
        this.props.changePage("timeline");
    }

    goToStorage = () => {
        this.props.changePage("storage");
    }

    goToSettlementUpgrades = () => {
        this.props.changePage("settlement-upgrades");
    }
};

function mapStateToProps(state, props) {
    return {
        settlementId: state.pageReducer.settlementId,
        page: state.pageReducer.page[state.pageReducer.page.length - 1].page,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goToSettlement, changePage}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);

var styles = StyleSheet.create({
    buttonWrapper: {
        flex: .2
    },
});
