'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import SimpleToast from 'react-native-simple-toast';

import {
    View,
    TouchableOpacity,
    FlatList,
    StyleSheet,
} from 'react-native';

import DeviceInfo from 'react-native-device-info';

import {goToSettlement, goToSurvivor, goToSurvivorList, changePage, clearSurvivorIndex} from '../actions/router';
import { timelineActionWrapper } from '../actions/timeline';
import { settlementActionWrapper, setSurvivorSortAttribute, setSurvivorSortOrder } from '../actions/settlement';
import {survivorActionWrapper, setPinned} from '../actions/survivors';
import {getDepartingSurvivorIds, getDepartingSurvivors, getPinnedSurvivors} from '../selectors/survivors';
import {getDepartingSurvival, getDepartingInsanity, getSortedNemesisEncounters, getSortedSpecialShowdowns, getSortedQuarryShowdowns} from '../selectors/settlement';
import {getDepartingTexts} from '../selectors/innovations';
import {SurvivorLineItem, getCardHeight} from '../components/survivor/survivor-line-item';

import Text from './shared/app-text';
import Button from './shared/button';
import Popup from './shared/popup';
import sharedStyles from '../style/styles';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import RoundControl from './shared/buttons/RoundControl';
import styles from '../style/styles';
import { getShowdownConfig, getEventTitleForType } from '../helpers/helpers';
import ListItem from './shared/ListItem';
import { setDepartingView } from '../actions/settings';
import Icon from './shared/Icon';
import { LIST_VERT_DOUBLE, LIST_HORIZ_DOUBLE } from './survivor-list';
import Hr from './shared/Hr';

let ShowdownConfig = null;

class SurvivorsDeparting extends Component {
    list = null;
    timeout = null;

    constructor(props) {
        super(props);

        ShowdownConfig = getShowdownConfig();

        this.state = {
            showdownVisible: false,
            nemesisVisible: false,
            specialVisible: false,
            controlsVisible: false,
            showdownTypeVisible: false,
            lastPressedAttr: '',
            numTimesPressed: 0,
            showdownEditVisible: false,
            departingMessage: '',
        }
    }

    componentDidMount() {
        if(!this.props.departView) {
            let view = DeviceInfo.isTablet() ? "grid" : "swipe";
            this.props.setDepartingView(view);
        }

        if(!this.props.currentShowdown || !this.props.currentShowdown.type || !this.props.currentShowdown.monster) {
            let showdown = this.getDefaultShowdown();
            if(showdown.type && showdown.monster) {
                this.props.settlementActionWrapper("setCurrentShowdown", [this.props.settlementId, showdown]);
            }
        }
    }

    componentWillUnmount() {
        if(this.timeout) {
            clearTimeout(this.timeout);
        }
    }

    onLayout = () => {
        if(this.props.survivorOffset) {
            this.list.scrollToOffset({offset: this.props.survivorOffset, animated: false});
        }
    }

    survivorPressed = (survivor, index) => {
        // -1 because we insert things into the front of the array
        index = index - 1;
        this.props.goToSurvivorList(index);
    }

    getDefaultShowdown = () => {
        let showdownType = "showdowns";
        let monster = "";
        let found = false;

        if(this.props.currentLySpecialShowdowns.length > 0) {
            for(let i = 0; i < this.props.currentLySpecialShowdowns.length; i++) {
                if(!this.isShowdownCompleted("specialShowdowns", this.props.currentLySpecialShowdowns[i])) {
                    showdownType = "specialShowdowns";
                    monster = this.props.currentLySpecialShowdowns[i];
                    found = true;
                    break;
                }
            }
        }

        if(!found) {
            if(this.props.currentLyNemesisEncounters.length > 0) {
                for(let i = 0; i < this.props.currentLyNemesisEncounters.length; i++) {
                    if(!this.isShowdownCompleted("nemesisEncounters", this.props.currentLyNemesisEncounters[i])) {
                        showdownType = "nemesisEncounters";
                        monster = this.props.currentLyNemesisEncounters[i];
                        found = true;
                        break;
                    }
                }
            }
        }

        if(!found) {
            if(this.props.currentLyShowdowns.length > 0) {
                for(let i = 0; i < this.props.currentLyShowdowns.length; i++) {
                    if(!this.isShowdownCompleted("showdowns", this.props.currentLyShowdowns[i])) {
                        showdownType = "showdowns";
                        monster = this.props.currentLyShowdowns[i];
                        break;
                    }
                }
            }
        }

        return {type: showdownType, monster};
    }

    isShowdownCompleted = (type, monster) => {
        if(this.props.completedShowdowns) {
            for(let i = 0; i < this.props.completedShowdowns.length; i++) {
                if(this.props.completedShowdowns[i].type === type && this.props.completedShowdowns[i].monster === monster) {
                    return true;
                }
            }
        }

        return false;
    }

    renderDepartControls = () => {
        if(this.props.departingSurvivors.length === 0 && !this.props.currentShowdown) {
            return <View></View>;
        }

        let survivalText = this.props.departSurvival > 0 ? <Text style={[sharedStyles.itemText, {marginLeft: 5}]}>{'\u25CF'} Departing survivors gain +{this.props.departSurvival} survival</Text> : null;
        let insanityText = this.props.departInsanity > 0 ? <Text style={[sharedStyles.itemText, {marginLeft: 5}]}>{'\u25CF'} Departing survivors gain +{this.props.departInsanity} insanity</Text> : null;

        let departText = [];
        if(this.props.departTexts.length > 0) {
            for(let i = 0; i < this.props.departTexts.length; i++) {
                departText.push(<Text key={i} style={[sharedStyles.textLineHeight, {marginLeft: 5}]}><Text style={{fontFamily: "icomoon"}}>{'\u25CF'} {this.props.departTexts[i]}</Text></Text>);
            }
        }

        let manageDeparting = null;
        let showdown = null;
        if(!this.props.departing) {
            let disabled = !this.props.currentShowdown || !this.props.currentShowdown.type || !this.props.currentShowdown.monster || this.props.departingSurvivors.length === 0;
            let monster = (this.props.currentShowdown && this.props.currentShowdown.monster) ? this.props.currentShowdown.monster : t("Select Monster");
            let type = (this.props.currentShowdown && this.props.currentShowdown.type) ? this.props.currentShowdown.type : "showdowns";
            showdown = <ListItem
                title={ShowdownConfig[monster] ? ShowdownConfig[monster].title : monster}
                subTitle={getEventTitleForType(type)}
                buttonText={t("Change")}
                onPress={this.changeShowdown}
            />

            manageDeparting = <View>
                <Button title={t("Depart")} onPress={this.onDepart} shape="rounded" disabled={disabled} onDisablePress={this.onDepartDisable}/>
                <Text style={styles.italic}>{t("Departing applies automatic on-depart bonuses and resets armor")}</Text>
            </View>
        } else {
            let monster = (this.props.currentShowdown && this.props.currentShowdown.monster) ? this.props.currentShowdown.monster : t("Select Monster");
            let type = (this.props.currentShowdown && this.props.currentShowdown.type) ? this.props.currentShowdown.type : "showdowns";
            manageDeparting = <Button title={t("Manage Departing Survivors")} onPress={this.showControls} shape="rounded" />
            showdown = <View style={styles.rowCentered}>
                <Text style={[styles.itemText, styles.bold]}>{getEventTitleForType(type)}: </Text>
                <Text style={styles.itemText}>{ShowdownConfig[monster] ? ShowdownConfig[monster].title : monster}</Text>
            </View>
        }

        return(
            <View style={sharedStyles.listRowWrapper}>
                <View style={{padding: 5}}>
                    {((survivalText || insanityText) && !this.props.departing) && <View>
                        <Text style={styles.itemText}>{t("Automatically applied on departure:")}</Text>
                        {survivalText}
                        {insanityText}
                    </View>}
                    {departText.length > 0 && <View>
                        <Text style={styles.itemText}>{t("Manually applied:")}</Text>
                        {departText}
                    </View>}
                </View>
                {showdown}
                {manageDeparting}
                {this.state.showdownEditVisible && <Popup
                    visible={this.state.showdownEditVisible}
                    title={t("Edit Showdown")}
                    onDismissed={this.hideEditShowdown}
                >
                    {this.renderEditShowdown()}
                </Popup>}
            </View>
        )
    }

    renderEditShowdown = () => {
        let monster = (this.props.currentShowdown && this.props.currentShowdown.monster) ? this.props.currentShowdown.monster : t("None");
        let type = (this.props.currentShowdown && this.props.currentShowdown.type) ? this.props.currentShowdown.type : "showdowns";
        return <View>
            <Text style={styles.title}>{t("Showdown Type")}</Text>
            <ListItem
                title={getEventTitleForType(type)}
                buttonText={t("Change")}
                onPress={this.changeShowdownType}
            />
            <Hr />
            <Text style={styles.title}>{t("Monster")}</Text>
            <ListItem
                title={ShowdownConfig[monster] ? ShowdownConfig[monster].title : monster}
                buttonText={t("Change")}
                onPress={this.getMonsterSelector(type)}
            />
            {this.state.showdownVisible && <Popup
                visible={this.state.showdownVisible}
                onDismissed={this.hideShowdown}
            >
                {this.getShowdownPicker()}
            </Popup>}
            {this.state.nemesisVisible && <Popup
                visible={this.state.nemesisVisible}
                onDismissed={this.hideNemesis}
            >
                {this.getNemesisPicker()}
            </Popup>}
            {this.state.specialVisible && <Popup
                visible={this.state.specialVisible}
                onDismissed={this.hideSpecial}
            >
                {this.getSpecialShowdownPicker()}
            </Popup>}
            {this.state.showdownTypeVisible && <Popup
                visible={this.state.showdownTypeVisible}
                onDismissed={this.hideShowdownType}
            >
                {this.getShowdownTypePicker()}
            </Popup>}
        </View>
    }

    changeShowdown = () => {
        this.setState({showdownEditVisible: true});
    }

    hideEditShowdown = () => {
        this.setState({showdownEditVisible: false});
    }

    changeShowdownType = () => {
        this.setState({showdownTypeVisible: true});
    }

    hideShowdownType = () => {
        this.setState({showdownTypeVisible: false});
    }

    getMonsterSelector = (type) => {
        if(type === "showdowns") {
            return this.showShowdown;
        } else if(type === "nemesisEncounters") {
            return this.showNemesis;
        } else if(type === "specialShowdowns") {
            return this.showSpecial;
        }
    }

    onDepartDisable = () => {
        if(this.props.departingSurvivors.length === 0) {
            SimpleToast.show(t("You must have at least one departing survivor in order to depart!"));
        } else {
            SimpleToast.show(t("Select a showdown to be able to depart"));
        }
    }

    onDepart = () => {
        if(this.props.departSurvival > 0 || this.props.departInsanity > 0) {
            SimpleToast.show(t("Automatic departing bonuses applied"));
        }

        this.props.settlementActionWrapper("setSettlementDeparting", [this.props.settlementId, true]);
    }

    setCurrentMonster = (type, monster) => {
        this.props.settlementActionWrapper("setCurrentShowdown", [this.props.settlementId, {type, monster}]);
    }

    setShowdownType = (type) => {
        let monster = this.props.currentShowdown ? this.props.currentShowdown.monster : "";

        if(monster) {
            if(type === "nemesisEncounters" && this.props.nemesisEncounters.indexOf(monster) === -1) {
                monster = "";
            } else if(type === "showdowns" && this.props.showdowns.indexOf(monster) === -1) {
                monster = "";
            } else if(type === "specialShowdowns" && this.props.specialShowdowns.indexOf(monster) === -1) {
                monster = "";
            }
        }

        this.props.settlementActionWrapper("setCurrentShowdown", [this.props.settlementId, {type, monster}]);
        this.hideShowdownType();
    }

    // combination action: return survivors from showdown + end the year
    returnSurvivors = () => {
        this.props.settlementActionWrapper('endYear', [this.props.settlementId, true]);
        this.setState({controlsVisible: false});
        this.props.onReturn();
    }

    // only return survivors
    returnNoEndYear = () => {
        this.props.survivorActionWrapper("returnSurvivors", [this.props.settlementId]);
        this.setState({controlsVisible: false});
        this.props.onReturn();
    }

    setDepartSwipe = () => {
        this.props.setDepartingView("swipe");
    }

    setDepartGrid = () => {
        this.props.setDepartingView("grid");
    }

    setDepartVert = () => {
        this.props.setDepartingView(LIST_VERT_DOUBLE);
    }

    setDepartHoriz = () => {
        this.props.setDepartingView(LIST_HORIZ_DOUBLE);
    }

    getDepartControls = () => {
        return (
            <View>
                <View style={[styles.rowCentered, localStyles.controlRow]}>
                    <RoundControl icon="minus" onPress={this.changeDepartingSurvivors("decreaseXp")} style={localStyles.leftControl}/>
                    <Text style={sharedStyles.itemText}>{t("Hunt XP")}</Text>
                    <RoundControl icon="plus" onPress={this.changeDepartingSurvivors("increaseXp")} style={localStyles.rightControl}/>
                </View>
                <View style={[styles.row, localStyles.controlRow, {marginTop: 10}]}>
                    <View style={[styles.rowCentered, styles.width50]}>
                        <RoundControl icon="minus" onPress={this.changeDepartingSurvivors("decreaseSurvival")} style={localStyles.leftControl}/>
                        <Text style={sharedStyles.itemText}>{t("Survival")}</Text>
                        <RoundControl icon="plus" onPress={this.changeDepartingSurvivors("increaseSurvival")} style={localStyles.rightControl}/>
                    </View>
                    <View style={[styles.rowCentered, styles.width50]}>
                        <RoundControl icon="minus" onPress={this.changeDepartingSurvivors("decreaseInsanity")} style={localStyles.leftControl}/>
                        <Text style={sharedStyles.itemText}>{t("Insanity")}</Text>
                        <RoundControl icon="plus" onPress={this.changeDepartingSurvivors("increaseInsanity")} style={localStyles.rightControl}/>
                    </View>
                </View>
                <View style={[styles.row, localStyles.controlRow, {marginTop: 10}]}>
                    <View style={[styles.rowCentered, styles.width50]}>
                        <RoundControl icon="minus" onPress={this.changeDepartingSurvivors("decreaseCourage")} style={localStyles.leftControl}/>
                        <Text style={sharedStyles.itemText}>{t("Courage")}</Text>
                        <RoundControl icon="plus" onPress={this.changeDepartingSurvivors("increaseCourage")} style={localStyles.rightControl}/>
                    </View>
                    <View style={[styles.rowCentered, styles.width50]}>
                        <RoundControl icon="minus" onPress={this.changeDepartingSurvivors("decreaseUnderstanding")} style={localStyles.leftControl}/>
                        <Text style={sharedStyles.itemText}>{t("Understanding")}</Text>
                        <RoundControl icon="plus" onPress={this.changeDepartingSurvivors("increaseUnderstanding")} style={localStyles.rightControl}/>
                    </View>
                </View>
                <Text style={styles.itemText}>{this.state.departingMessage}</Text>
                <View style={{marginTop: 15}}>
                    <Button title={t("Return from showdown")} onPress={this.returnNoEndYear} color={this.props.colors.HIGHLIGHT} shape="rounded" />
                    {(this.props.currentShowdown && this.props.currentShowdown.type !== "specialShowdowns") && <View>
                        <View style={{height: 5}}/>
                        <Button title={t("Return and advance year")} onPress={this.returnSurvivors} color={this.props.colors.HIGHLIGHT} shape="rounded" />
                    </View>}
                </View>
                <View style={{marginTop: 50}}>
                    <View style={sharedStyles.rowCentered}>
                        <View style={sharedStyles.flex33}>
                            <Text style={styles.itemText}>{t("Survivor View")}</Text>
                        </View>
                        <View style={[sharedStyles.flex50, sharedStyles.row, {justifyContent: "space-evenly"}]}>
                            <TouchableOpacity onPress={this.setDepartSwipe}>
                                <Icon name="view-carousel" color={this.props.departView === "swipe" ? this.props.colors.HIGHLIGHT : this.props.colors.TEXT} size={30}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.setDepartGrid}>
                                <Icon name="view-grid" color={this.props.departView === "grid" ? this.props.colors.HIGHLIGHT : this.props.colors.TEXT} size={30}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.setDepartVert}>
                                <Icon name="sk-vertical-list" color={this.props.departView === LIST_VERT_DOUBLE ? this.props.colors.HIGHLIGHT : this.props.colors.TEXT} size={30}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.setDepartHoriz}>
                                <Icon name="sk-horizontal-list" color={this.props.departView === LIST_HORIZ_DOUBLE ? this.props.colors.HIGHLIGHT : this.props.colors.TEXT} size={30}/>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    getShowdown = () => {
        let title = t("Select");
        if(this.props.showdown) {
            title = ShowdownConfig[this.props.showdown] ? ShowdownConfig[this.props.showdown].title : this.props.showdown;
        }

        return <Button
            title={title}
            skeleton={true}
            shape="pill"
            onPress={this.showShowdown}
            color={this.props.colors.TEXT}
        />
    }

    getNemesis = () => {
        let title = t("Select");
        if(this.props.nemesisEncounter) {
            title = ShowdownConfig[this.props.nemesisEncounter] ? ShowdownConfig[this.props.nemesisEncounter].title : this.props.nemesisEncounter;
        }

        return <Button title={title} skeleton={true} shape="pill" onPress={this.showNemesis} color={this.props.colors.TEXT}/>
    }

    getSpecialShowdown = () => {
        let title = t("Select");
        if(this.props.specialShowdown) {
            title = ShowdownConfig[this.props.specialShowdown] ? ShowdownConfig[this.props.specialShowdown].title : this.props.specialShowdown;
        }

        return <Button title={title} skeleton={true} shape="pill" onPress={this.showSpecial} color={this.props.colors.TEXT}/>
    }

    hideShowdown = () => {
        this.setState({showdownVisible: false});
    }

    showShowdown = () => {
        this.setState({showdownVisible: true});
    }

    hideNemesis = () => {
        this.setState({nemesisVisible: false});
    }

    showNemesis = () => {
        this.setState({nemesisVisible: true});
    }

    hideSpecial = () => {
        this.setState({specialVisible: false});
    }

    showSpecial = () => {
        this.setState({specialVisible: true});
    }

    hideControls = () => {
        this.setState({controlsVisible: false, numTimesPressed: 0});
    }

    showControls = () => {
        this.setState({controlsVisible: true});
    }

    getShowdownPicker = () => {
        let showdowns = this.props.showdowns.map((name, i) => {
            return <ListItem
                key={i}
                title={ShowdownConfig[name].title}
                id={name}
                onPress={this.setShowdown}
                buttonText={t("Select")}
            />
        });

        showdowns.splice(0, 0, this.getNoneOption(this.setShowdown));

        return showdowns;
    }

    getNoneOption = (callback) => {
        return <ListItem
            key="-1"
            title={t("None")}
            id=""
            onPress={callback}
            buttonText={t("Select")}
        />
    }

    setShowdown = (name) => {
        this.setCurrentMonster("showdowns", name);
        this.hideShowdown();
    }

    getNemesisPicker = () => {
        let showdowns = this.props.nemesisEncounters.map((name, i) => {
            return <ListItem
                key={i}
                title={ShowdownConfig[name].title}
                id={name}
                onPress={this.setNemesis}
                buttonText={t("Select")}
            />
        });

        showdowns.splice(0, 0, this.getNoneOption(this.setNemesis));

        return showdowns;
    }

    setNemesis = (name) => {
        this.setCurrentMonster("nemesisEncounters", name);
        this.hideNemesis();
    }

    getSpecialShowdownPicker = () => {
        let showdowns = this.props.specialShowdowns.map((name, i) => {
            return <ListItem
                key={i}
                title={ShowdownConfig[name].title}
                id={name}
                onPress={this.setSpecialShowdown}
                buttonText={t("Select")}
            />
        });

        showdowns.splice(0, 0, this.getNoneOption(this.setSpecialShowdown));

        return showdowns;
    }

    setSpecialShowdown = (name) => {
        this.setCurrentMonster("specialShowdowns", name);
        this.hideSpecial();
    }

    getShowdownTypePicker = () => {
        const types = ["nemesisEncounters", "showdowns", "specialShowdowns"];
        return types.map((type, i) => {
            return <ListItem
                key={i}
                title={getEventTitleForType(type)}
                id={type}
                onPress={this.setShowdownType}
                buttonText={t("Select")}
            />
        });
    }

    changeDepartingSurvivors = (action, args = []) => {
        return () => {
            let numTimesPressed = this.state.numTimesPressed + 1;
            if(action !== this.state.lastPressedAttr) {
                numTimesPressed = 1;
            }
            this.setState({lastPressedAttr: action, numTimesPressed: numTimesPressed}, this.setConfirmMessage);
            // doBatchAction is handled differently. pass the action being performed as an object
            this.props.survivorActionWrapper("doBatchAction", {action: action, ids: {settlementId: this.props.settlementId, survivorIds: this.props.departingSurvivors}, args: [...args]});
        }
    }

    setConfirmMessage = () => {
        let message = '';

        if(this.state.lastPressedAttr === 'decreaseXp') {
            message = t('XP decreased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'increaseXp') {
            message = t('XP increased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'decreaseSurvival') {
            message = t('Survival decreased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'increaseSurvival') {
            message = t('Survival increased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'decreaseInsanity') {
            message = t('Insanity decreased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'increaseInsanity') {
            message = t('Insanity increased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'decreaseCourage') {
            message = t('Courage decreased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'increaseCourage') {
            message = t('Courage increased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'decreaseUnderstanding') {
            message = t('Understanding decreased by %num%', {num: this.state.numTimesPressed});
        } else if(this.state.lastPressedAttr === 'increaseUnderstanding') {
            message = t('Understanding increased by %num%', {num: this.state.numTimesPressed});
        }

        this.setState({departingMessage: message});
        this.timeout = setTimeout(() => {
            this.setState({departingMessage: '', numTimesPressed: 0, lastPressedAttr: ''});
        }, 15000);
    }

    toggleDeparting = (id) => {
        // pass: settlementId, id, ly
        this.props.survivorActionWrapper('toggleDeparting', [this.props.settlementId, id, this.props.ly]);
    }

    toggleSkipNextHunt = (id) => {
        this.props.survivorActionWrapper('toggleSkipNextHunt', [this.props.settlementId, id]);
    }

    toggleReroll = (id) => {
        this.props.survivorActionWrapper('toggleReroll', [this.props.settlementId, id]);
    }

    getSwipeButtons = (survivor) => {
        if(survivor.departing) {
            return [
                <TouchableOpacity style={{height: "100%", flexDirection: "row", alignItems:"center"}} onPress={this.toggleDeparting(survivor.id)}><Text>Leave Departing</Text></TouchableOpacity>
            ];
        } else {
            return [
                <TouchableOpacity style={{height: "100%", flexDirection: "row", alignItems:"center"}} onPress={this.toggleDeparting(survivor.id)}><Text>Join Departing</Text></TouchableOpacity>
            ];
        }
    }

    setSortAttribute = (attr) => {
        this.props.setSurvivorSortAttribute(this.props.settlementId, attr);
    }

    toggleSortDirection = () => {
        this.props.setSurvivorSortOrder(this.props.settlementId, this.props.sortOrder === "asc" ? "desc" : "asc");
    }

    togglePinned = (id) => {
        let value = this.props.pinnedSurvivors[id] === true ? false : true;
        this.props.setPinned(this.props.settlementId, id, value);
    }

    renderSurvivor = (item) => {
        if(item.item === "padding") {
            return <View style={{height: 10}} />
        }

        if(item.item === "departControls") {
            return this.renderDepartControls();
        }

        if(item.item === "noSurvivors") {
            if(this.props.survivors.length === 0) {
                return <Text style={[styles.itemText, styles.italic, {marginLeft: 5, marginTop: 20}]}>
                    {t("There are no departing survivors.  Swipe a survivor left on the survivor list or long press the departing icon to mark them as departing!")}
                </Text>;
            } else {
                return null;
            }
        }

        let survivor = item.item;

        return (
            <SurvivorLineItem
                survivor={survivor}
                isSotf={this.props.isSotf}
                ly={this.props.ly}
                campaign={this.props.campaign}
                onPress={this.survivorPressed}
                index={item.index}
                onDepart={this.toggleDeparting}
                onSkipNextHunt={this.toggleSkipNextHunt}
                onReroll={this.toggleReroll}
                numLinesSurvivorCard={this.props.numLinesSurvivorCard}
                onPinned={this.togglePinned}
                pinned={this.props.pinnedSurvivors[survivor.id] === true}
                tapToDepart={this.props.tapToDepart}
            />
        );
    }

    backToSettlement = () => {
        this.props.goToSettlement(this.props.settlementId);
    };

    goToCreateSurvivor = () => {
        this.props.changePage("create-survivor");
    }

    getItemLayout = (data, index) => {
        let length = 0;
        let offset = 0;

        if(index >= 0) {
            length = 40;
        }

        // departure controls
        if(index >= 1) {
            length = 335;
            offset += 40;
        }

        if(index >= 2) {
            length = getCardHeight(this.props.numLinesSurvivorCard);
            offset += 40 + 335 + (length * (index - 2)); // offset of controls + sorting + number of previous cards
        }

        return {
            length,
            offset,
            index
        }
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <FlatList
                    ref={(ref) => {this.list = ref}}
                    data={["departControls", "noSurvivors", ...this.props.survivors, "padding"]}
                    renderItem={this.renderSurvivor}
                    keyExtractor={(item, index) => index + ""}
                    onLayout={this.onLayout}
                    // onScroll={this.scroll}
                    getItemLayout={this.getItemLayout}
                />
                {this.state.controlsVisible && <Popup
                    visible={this.state.controlsVisible}
                    onDismissed={this.hideControls}
                >
                    {this.getDepartControls()}
                </Popup>}
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return {
        settlementId: settlementId,
        isSotf: (state.settlementReducer.settlements[settlementId].principles.newLife === "survivalOfTheFittest"),
        survivalLimit: state.settlementReducer.settlements[settlementId].survivalLimit,
        survivors: getDepartingSurvivors(state),
        ly: state.settlementReducer.settlements[settlementId].ly,
        campaign: state.settlementReducer.settlements[settlementId].campaign,
        departingSurvivors: getDepartingSurvivorIds(state),
        showdowns: getSortedQuarryShowdowns(state),
        nemesisEncounters: getSortedNemesisEncounters(state),
        specialShowdowns: getSortedSpecialShowdowns(state),
        currentLyShowdowns: state.timelineReducer[settlementId][state.settlementReducer.settlements[settlementId].ly].showdowns,
        currentLyNemesisEncounters: state.timelineReducer[settlementId][state.settlementReducer.settlements[settlementId].ly].nemesisEncounters,
        currentLySpecialShowdowns: state.timelineReducer[settlementId][state.settlementReducer.settlements[settlementId].ly].specialShowdowns,
        departSurvival: getDepartingSurvival(state),
        departInsanity: getDepartingInsanity(state),
        departTexts: getDepartingTexts(state),
        lastSelectedSurvivorIndex: state.pageReducer.lastSelectedSurvivorIndex,
        survivorOffset: state.pageReducer.survivorOffset,
        isSubscriber: state.purchases.isSubscriber,
        sortAttribute: state.settlementReducer.settlements[settlementId].survivorSortAttr,
        sortOrder: state.settlementReducer.settlements[settlementId].survivorSortOrder,
        colors: getColors(state),
        departView: state.settingsReducer.departView,
        numLinesSurvivorCard: state.settingsReducer.numLinesSurvivorCard,
        pinnedSurvivors: getPinnedSurvivors(state),
        departing: state.settlementReducer.settlements[settlementId].departing,
        currentShowdown: state.settlementReducer.settlements[settlementId].currentShowdown,
        completedShowdowns: state.settlementReducer.settlements[settlementId].completedShowdowns,
        tapToDepart: state.settingsReducer.tapToDepart,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goToSettlement, goToSurvivor, goToSurvivorList, changePage, survivorActionWrapper, timelineActionWrapper, settlementActionWrapper, clearSurvivorIndex, setSurvivorSortAttribute, setSurvivorSortOrder, setDepartingView, setPinned}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SurvivorsDeparting);

const localStyles = StyleSheet.create({
    leftControl: {
        marginRight: 10,
    },
    rightControl: {
        marginLeft: 10,
    },
    controlRow: {
        marginBottom: 5,
    }
});
