'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import styles from '../../style/styles';
import AppText from '../shared/app-text';
import AppTextInput from '../shared/AppTextInput';

class Epithet extends Component {
    render() {
        return <View style={styles.rowCentered}>
        <AppText style={[styles.headerFontSize]}>{t("Epithet")}</AppText>
        <AppTextInput
            style={{marginLeft: 20, flex: 1}}
            value={this.props.epithet}
            onChangeText={this.setEpithet}
            inputStyle={{fontSize: 18}}
        />
    </View>
    }

    setEpithet = (value) => {
        this.props.survivorActionWrapper('setEpithet', [this.props.settlementId, this.props.id, value]);
    }
}

function mapStateToProps(state, props) {
    return {
        epithet: state.survivorReducer.survivors[props.settlementId][props.id].epithet !== undefined ? state.survivorReducer.survivors[props.settlementId][props.id].epithet : '',
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Epithet);
