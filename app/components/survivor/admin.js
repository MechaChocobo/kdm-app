'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Uuid from 'react-native-uuid';
import SimpleToast from 'react-native-simple-toast';

import {changePage, addTransitionAction} from '../../actions/router';
import {deleteSurvivor, survivorActionWrapper} from '../../actions/survivors';

import {
    View,
    Alert
} from 'react-native';

import Text from '../../components/shared/app-text';
import Button from '../../components/shared/button';
import sharedStyles from '../../style/styles';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';

class Admin extends Component {
    confirmDelete = () => {
        Alert.alert(t("Are you sure?"), t("Deleting a survivor will remove all traces of the survivor from Scribe and CANNOT BE UNDONE."), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.deleteSurvivor}
        ]);
    }

    deleteSurvivor = () => {
        this.props.addTransitionAction(deleteSurvivor, [this.props.settlementId, this.props.id]);
        this.props.onDelete();
        setTimeout(() => {
            this.props.changePage("survivor-home");
        }, 100);
    }

    copySurvivor = () => {
        let newId = Uuid.v4();
        this.props.survivorActionWrapper("copySurvivor", [this.props.settlementId, this.props.id, newId]);
        SimpleToast.show(t("Survivor copied!"));
    }

    showNetworkWarning = () => {
        SimpleToast.show(t("You cannot delete a survivor while networked"));
    }

    render() {
        return (
            <View>
                <Text style={sharedStyles.title}>{t("Survivor Admin")}</Text>
                <View style={sharedStyles.contentWrapper}>
                    <View style={{padding: "10%", paddingBottom: "5%"}}>
                        <Button title={t("Copy Survivor")} onPress={this.copySurvivor} color={this.props.colors.HIGHLIGHT} shape="rounded"/>
                    </View>
                    <View style={{padding: "10%", paddingTop: "5%"}}>
                        <Button
                            title={t("Delete Survivor")}
                            onPress={this.confirmDelete}
                            color={this.props.colors.RED}
                            shape="rounded"
                            disabled={this.props.isNetworked}
                            onDisablePress={this.showNetworkWarning}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        isNetworked: (state.networkReducer.server !== null || state.networkReducer.client !== null),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({addTransitionAction, changePage, survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
