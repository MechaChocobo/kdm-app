'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

import {
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import Text from '../shared/app-text';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import MilestoneDots from '../shared/MilestoneDots';
import styles from '../../style/styles';
import RoundControl from '../shared/buttons/RoundControl';
import Popup from '../shared/popup';
import { getUnselectedArray, sortIdArrayByConfigTitle, getAbilitiesByType } from '../../helpers/helpers';
import Icon from '../shared/Icon';
import ListItem from '../shared/ListItem';
import Hr from '../shared/Hr';

class Understanding extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAbilityPopup: false,
        }
    }

    render() {
        return (
            <View>
                <View style={[styles.rowCentered, styles.spaceBetween]}>
                    <View style={styles.rowCentered}>
                        <Text style={styles.valueFontSize}>{this.props.understanding}</Text>
                        <View style={localStyles.dividerWidth}/>
                        <View>
                            <Text style={styles.textHeader}>{t("Understanding")}</Text>
                            {this.getMilestoneText()}
                        </View>
                    </View>
                    <View style={styles.rowCentered}>
                        <RoundControl onPress={this.decreaseUnderstanding} icon="minus" />
                        <View style={localStyles.dividerWidth}/>
                        <RoundControl onPress={this.increaseUnderstanding} icon="plus" />
                    </View>
                </View>
                <MilestoneDots milestones={[3, 9]} max={9} currentValue={this.props.understanding}/>
                {this.getAbilitySection()}
            </View>
        );
    }

    getMilestoneText = () => {
        if(this.props.understanding === 9) {
            return <Text> </Text>
        }

        let milestone = "Insight";
        let marker = this.props.understanding >= 3 ? 9 : 3;
        if(this.props.campaign === "PotStars") {
            milestone = "Awake";
        }

        if(marker === 9) {
            milestone = "White Secret"
        }

        return <Text><Text style={styles.bold}>{milestone}</Text> at {marker}</Text>;
    }

    getAbilitySection = () => {
        if(this.props.campaign === "PotStars") {
            return null;
        }

        return <View>
            <Hr slim={true}/>
            <TouchableOpacity onPress={this.showAbilityPopup}>
                <View style={[styles.rowCentered, styles.spaceBetween]}>
                    <View style={{flex: .95}}>
                        {this.getAbilityText()}
                    </View>
                    <Icon name="chevron-down" color={this.props.colors.HIGHLIGHT} size={50}/>
                </View>
            </TouchableOpacity>
            <Popup title={t("Select Ability")} visible={this.state.showAbilityPopup} onDismissed={this.onPopupClosed}>
                {this.getPicker()}
            </Popup>
        </View>
    }

    getPicker = () => {
        let availableAbilities = sortIdArrayByConfigTitle(getUnselectedArray([this.props.ability], Object.keys(this.props.allAbilities), this.props.allAbilities), this.props.allAbilities);
        let abilities = availableAbilities.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={this.props.allAbilities[name].title}
                    id={name}
                    onPress={this.addAbility}
                    buttonText={t("Select")}
                    confirm={false}
                />
            );
        });

        abilities.splice(0, 0,
            <ListItem
                key={-1}
                title={t("No Ability")}
                id={""}
                onPress={this.removeAbility}
                buttonText={t("Select")}
                confirm={false}
            />
        );

        return (
            <View style={{width: "90%", alignSelf: "center"}}>
                {abilities}
            </View>
        );
    }

    getAbilityText = () => {
        if(!this.props.ability) {
            return <Text style={styles.italic}>{t("No ability yet")}</Text>
        }

        return <View>
            <Text style={styles.textLineHeight}>
                <Text style={styles.bold}>{this.props.allAbilities[this.props.ability].title}</Text> - <Text style={styles.icomoon}>{t(this.props.allAbilities[this.props.ability].description)}</Text>
            </Text>
        </View>
    }

    addAbility = (name) => {
        if(!this.props.ability) {
            this.props.survivorActionWrapper('addAbility', [this.props.settlementId, this.props.id, name]);
        } else {
            this.props.survivorActionWrapper('swapAbility', [this.props.settlementId, this.props.id, this.props.ability, name]);
        }

        this.setState({showAbilityPopup: false});
    }

    removeAbility = () => {
        if(this.props.ability) {
            this.props.survivorActionWrapper('removeAbility', [this.props.settlementId, this.props.id, this.props.ability]);
        }

        this.setState({showAbilityPopup: false});
    }

    onPopupClosed = () => {
        this.setState({showAbilityPopup: false});
    }

    showAbilityPopup = () => {
        this.setState({showAbilityPopup: true});
    }

    increaseUnderstanding = () => {
        if(this.props.understanding < 9) {
            this.props.survivorActionWrapper('setUnderstanding', [this.props.settlementId, this.props.id, this.props.understanding + 1]);
        }
    }

    decreaseUnderstanding = () => {
        if(this.props.understanding > 0) {
            this.props.survivorActionWrapper('setUnderstanding', [this.props.settlementId, this.props.id, this.props.understanding - 1]);
        }
    }
}

function mapStateToProps(state, props) {
    let allAbilities = getAbilitiesByType(state.settlementReducer.settlements[props.settlementId].allAbilities, 'insight');
    let abilities = state.survivorReducer.survivors[props.settlementId][props.id].abilities;
    let ability = null;

    for(let i = 0; i < abilities.length; i++) {
        if(allAbilities[abilities[i]]) {
            ability = abilities[i];
            break;
        }
    }

    return {
        understanding: state.survivorReducer.survivors[props.settlementId][props.id].understanding,
        campaign: state.settlementReducer.settlements[props.settlementId].campaign,
        colors: getColors(state),
        allAbilities: allAbilities,
        ability: ability,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Understanding);

const localStyles = StyleSheet.create({
    dividerWidth: {
        width: 10
    }
});
