'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';

import {survivorActionWrapper} from '../../actions/survivors';
import {getUnselectedArray, getRandomInt, getDisorderConfig, sortIdArrayByConfigTitle} from '../../helpers/helpers';

import {
    View,
} from 'react-native';

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';

let DisorderConfig = null;
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import AbilityText from '../shared/AbilityText';
import ListItem from '../shared/ListItem';
import RoundControl from '../shared/buttons/RoundControl';
import Popup from '../shared/popup';
import Hr from '../shared/Hr';

class Disorders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addError: "",
            tempDisorder: "",
            timeout: null,
            showAddList: false,
            shouldConfirmRemove: true,
        };
        DisorderConfig = getDisorderConfig();
    }

    addDisorder = (name) => {
        if(name !== "") {
            this.props.survivorActionWrapper('addDisorder', [this.props.settlementId, this.props.id, name]);
            this.onAddListClosed();
        }
    }

    addRandomDisorder = () => {
        clearInterval(this.state.timeout);
        let name = this.props.allDisorders[getRandomInt(this.props.allDisorders.length)];
        if(this.props.disorders.indexOf(name) !== -1) {
            this.setState({addError: t("Selected Disorder %name%, but survivor already has it!", {name: t(DisorderConfig[name].title)})});
            let timeout = this.setTimeout(() => {
                this.setState({addError: ""});
            }, 5000);
            this.setState({timeout});
        } else if(this.props.disorders.length >= 3) {
            this.setState({
                tempDisorder: name,
                addError: t("Selected Disorder '%name%', but survivor already has 3 Disorders.  Remove one to automatically add '%name%'", {name: t(DisorderConfig[name].title)}),
                shouldConfirmRemove: false,
            });
        } else {
            this.props.survivorActionWrapper('addDisorder', [this.props.settlementId, this.props.id, name]);
        }
    }

    removeDisorder = (name) => {
        this.props.survivorActionWrapper('removeDisorder', [this.props.settlementId, this.props.id, name]);

        if(this.state.tempDisorder !== "") {
            setTimeout(() => {
                this.props.survivorActionWrapper('addDisorder', [this.props.settlementId, this.props.id, this.state.tempDisorder]);
                this.setState({tempDisorder: "", addError: "", shouldConfirmRemove: true,});
            }, 500);
        }
    }

    getPicker = () => {
        let availableDisorders = getUnselectedArray(this.props.disorders, this.props.allDisorders, DisorderConfig);

        let disorders = availableDisorders.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={t(DisorderConfig[name].title)}
                    id={name}
                    onPress={this.addDisorder}
                    buttonText={t("Add")}
                    confirm={false}
                />
            );
        });

        return (
            <View style={{width: "90%", alignSelf: "center"}}>
                {disorders}
            </View>
        );
    }

    showAddList = () => {
        this.setState({showAddList: true});
    }

    onAddListClosed = () => {
        this.setState({showAddList: false});
    }

    render() {
        return (
            <View>
                <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                    <Text style={sharedStyles.title}>{t("Disorders")}</Text>
                    <View style={sharedStyles.rowCentered}>
                        <RoundControl icon="dice-multiple" onPress={this.addRandomDisorder}/>
                        <View width={10}/>
                        <RoundControl icon="plus" onPress={this.showAddList} disabled={this.props.disorders.length >= 3}/>
                    </View>
                </View>
                {this.props.disorders.length > 0 && <Hr />}
                {this.state.addError !== "" && <View style={{padding: 5}}><Text style={sharedStyles.itemText}>{this.state.addError}</Text></View>}
                <View>
                    {this.renderDisorders(this.props.disorders)}
                </View>
                <Popup title={t("Select Disorder")} visible={this.state.showAddList} onDismissed={this.onAddListClosed}>
                    {this.getPicker()}
                </Popup>
            </View>
        );
    }

    renderDisorders = () => {
        let disorders = [];
        for(let i = 0; i < 3; i++) {
            if(this.props.disorders[i]) {
                disorders.push(<AbilityText
                    key={i}
                    title={t(DisorderConfig[this.props.disorders[i]].title)}
                    description={t(DisorderConfig[this.props.disorders[i]].description)}
                    id={this.props.disorders[i]}
                    onPress={this.removeDisorder}
                    confirm={this.state.confirm}
                />);
            }
        }

        return disorders;
    }
}

reactMixin(Disorders.prototype, TimerMixin);

function mapStateToProps(state, props) {
    return {
        disorders: state.survivorReducer.survivors[props.settlementId][props.id].disorders,
        allDisorders: sortIdArrayByConfigTitle(state.settlementReducer.settlements[props.settlementId].allDisorders, getDisorderConfig()),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Disorders);
