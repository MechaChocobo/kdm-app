'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import { t } from '../../helpers/intl';
import styles from '../../style/styles';
import { IncrementorHidden } from '../shared/IncrementorHidden';
import AppText from '../shared/app-text';
import WoundBox from '../shared/WoundBox';

class Insanity extends Component {
    render() {
        return (
            <View style={styles.centeredItems}>
                <AppText style={styles.title}>{t("Insanity")}</AppText>
                <IncrementorHidden
                    value={this.props.insanity}
                    onIncrease={this.increaseInsanity}
                    onDecrease={this.decreaseInsanity}
                    onChange={this.setInsanity}
                />
                <WoundBox heavy={false} filled={this.props.brainInjury} onPress={this.toggleBrainInjury} />
            </View>
        );
    }

    increaseInsanity = () => {
        this.props.survivorActionWrapper('setInsanity', [this.props.settlementId, this.props.id, this.props.insanity + 1]);
    }

    decreaseInsanity = () => {
        if(this.props.insanity > 0) {
            this.props.survivorActionWrapper('setInsanity', [this.props.settlementId, this.props.id, this.props.insanity - 1]);
        }
    }

    // set value with text input (number pad)
    setInsanity = (val) => {
        this.props.survivorActionWrapper('setInsanity', [this.props.settlementId, this.props.id, +val]);
    }

    toggleBrainInjury = () => {
        this.props.survivorActionWrapper('toggleBrainInjury', [this.props.settlementId, this.props.id]);
    }
}

function mapStateToProps(state, props) {
    return {
        insanity: state.survivorReducer.survivors[props.settlementId][props.id].insanity.value,
        brainInjury: state.survivorReducer.survivors[props.settlementId][props.id].insanity.light,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Insanity);
