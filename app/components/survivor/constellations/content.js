'use strict';

import React, { Component } from 'react';
import ColorContext from '../../../context/ColorContext';

import {
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native';

export default class Content extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <TouchableOpacity style={{flex: .2}} onPress={this.toggleConstellation}>
                        <Text style={this.props.selected ? [styles.contentSelected, {color: colors.HIGHLIGHT}] : styles.content}>{this.props.content}</Text>
                    </TouchableOpacity>
                }
            </ColorContext.Consumer>
        )
    }

    toggleConstellation = () => {
        this.props.onToggle(this.props.row, this.props.col);
    }
}

var styles = StyleSheet.create({
    content : {
        fontSize: 12,
        fontFamily: 'lucida grande',
        color: "white"
    },
    contentSelected : {
        fontSize: 12,
        fontFamily: 'lucida grande',
    }
});
