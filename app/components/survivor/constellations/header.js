'use strict';

import React, { Component } from 'react';
import ColorContext from '../../../context/ColorContext';


import {
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native';

export default class Header extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <TouchableOpacity style={{flex: .2}} onPress={this.setHeader}>
                        <Text style={this.props.selected ? [styles.headerSelected, {color: colors.HIGHLIGHT}] : styles.header}>{this.props.header}</Text>
                    </TouchableOpacity>
                }
            </ColorContext.Consumer>
        )
    }

    setHeader = () => {
        this.props.onSet(this.props.header);
    }
}

var styles = StyleSheet.create({
    header : {
        fontSize: 16,
        fontWeight: "bold",
        color: "white",
        fontFamily: 'lucida grande'
    },
    headerSelected : {
        fontSize: 16,
        fontWeight: "bold",
        fontFamily: 'lucida grande'
    }
});
