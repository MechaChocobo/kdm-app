'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    StyleSheet,
    View,
    Keyboard,
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import AppTextInput from '../shared/AppTextInput';

class Notes extends Component {
    onNoteChange = (note) => {
        this.props.survivorActionWrapper('editNote', [this.props.settlementId, this.props.id, note]);
    }

    renderNotes = () => {
        if(this.props.notes === "") {
            return null;
        }

        return (
            <View style={{margin: 5}}>
                {<Text style={{fontSize: 16, fontWeight: "300"}}>{this.props.notes}</Text>}
            </View>
        );
    }

    renderNoteInput = () => {
        return <AppTextInput
            inputStyle={styles.nameInput}
            value={this.props.notes}
            onChangeText={this.onNoteChange}
            multiline={true}
            placeholder={t("Enter Notes")}
            returnKeyType="default"
            onBlur={Keyboard.dismiss}
        />
    }

    render() {
        return (
            <View style={[sharedStyles.container, sharedStyles.centeredWrapper]}>
                <Text style={sharedStyles.title}>{t("Notes")}</Text>
                <View style={sharedStyles.contentWrapper}>
                    {this.renderNoteInput()}
                </View>
            </View>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        notes: state.survivorReducer.survivors[props.settlementId][props.id].notes,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Notes);

const styles = StyleSheet.create({
    nameInput: {
        color: "white",
        fontSize: 16,
        fontWeight: "300",
        width: "98%",
        alignSelf: "center"
    }
});
