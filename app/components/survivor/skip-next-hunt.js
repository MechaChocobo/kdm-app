'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import {settlementActionWrapper} from '../../actions/settlement';
import sharedStyles from '../../style/styles';
import Text from '../shared/app-text';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';

class SkipNextHunt extends Component {
    toggleSkipNextHunt = () => {
        this.props.survivorActionWrapper('toggleSkipNextHunt', [this.props.settlementId, this.props.id]);
    }

    render() {
        return(
            <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                <Text style={sharedStyles.headerFontSize}>{t("Skip Next Hunt")}</Text>
                <TouchableOpacity onPress={this.toggleSkipNextHunt}>
                    <Icon name={this.props.skipNextHunt ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.colors.TEXT}/>
                </TouchableOpacity>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        skipNextHunt: state.survivorReducer.survivors[props.settlementId][props.id].skipNextHunt,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper, settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SkipNextHunt);
