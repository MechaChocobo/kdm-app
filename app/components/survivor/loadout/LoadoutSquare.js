'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import ColorContext from '../../../context/ColorContext';
import styles from '../../../style/styles';
import Icon from '../../shared/Icon';
import Popup from '../../shared/popup';
import { t } from '../../../helpers/intl';
import { getLocationConfig, getGearConfig, getColorCode, getLocationTitle } from '../../../helpers/helpers';
import ListItem from '../../shared/ListItem';
import AppText from '../../shared/app-text';
import SmallGearAffinities from '../../settlement/storage/SmallGearAffinities';
import Button from '../../shared/button';

let LocationConfig = null;
let GearConfig = null;

export default class LoadoutSquare extends Component {
    state = {
        showPopup: false,
        openedSections: {},
        expandedStats: false,
    }

    constructor(props) {
        super(props);

        LocationConfig = getLocationConfig();
        GearConfig = getGearConfig();
    }

    render() {
        return (
            <ColorContext.Consumer>
            { colors => {
                let backgroundColor = colors.isDark ? "rgba(256, 256, 256, 0.10)" : "rgba(0, 0, 0, 0.10)";
                if(this.props.selected) {
                    backgroundColor = colors.HIGHLIGHT;
                }
                return <View style={[styles.container, {borderWidth: 1, borderColor: colors.isDark ? "rgba(256, 256, 256, 0.15)" : "rgba(0, 0, 0, 0.15)"}]}>
                    {this.props.config && <TouchableOpacity style={[styles.container, {backgroundColor: backgroundColor}]} onPress={this.handleOnPress}>
                        {this.renderConfig(colors)}
                    </TouchableOpacity>}
                    {!this.props.config && <TouchableOpacity style={[styles.container, styles.centeredItems, styles.justifyCentered]} onPress={this.handleOnPress}>
                        <Icon name="plus-circle-outline" size={30} color={colors.HIGHLIGHT}/>
                    </TouchableOpacity>}
                    {this.state.showPopup && <Popup
                        title={t("Select gear")}
                        onDismissed={this.hideGearPicker}
                        visible={this.state.showPopup}
                    >
                        {this.renderGearSelector(colors)}
                    </Popup>}
                </View>
            }}
            </ColorContext.Consumer>
        );
    }

    renderConfig = (colors) => {
        if(!this.props.config) {
            return null;
        }

        let affinities = [];

        let config = GearConfig[this.props.config.location][this.props.config.name];

        const blockDec = 20;
        const blockLong = "20%";
        const blockShort = "10%";

        if(config.affinities) {
            const aff = config.affinities;
            const padding = ((100 - blockDec) / 2) + "%";

            if(aff.left) {
                affinities.push(<View key="left" style={{position: "absolute", left: 0, top: padding, height: blockLong, width: blockShort, backgroundColor: getColorCode(aff.left, colors)}}/>)
            }

            if(aff.right) {
                affinities.push(<View key="right" style={{position: "absolute", right: 0, top: padding, height: blockLong, width: blockShort, backgroundColor: getColorCode(aff.right, colors)}}/>)
            }

            if(aff.top) {
                affinities.push(<View key="top" style={{position: "absolute", top: 0, left: padding, height: blockShort, width: blockLong, backgroundColor: getColorCode(aff.top, colors)}}/>)
            }

            if(aff.bottom) {
                affinities.push(<View key="bottom" style={{position: "absolute", bottom: 0, left: padding, height: blockShort, width: blockLong, backgroundColor: getColorCode(aff.bottom, colors)}}/>)
            }
        }

        let armor = null;
        if(config.armor) {
            let icons = [];
            let small = false;
            if(config.armor.locations) {
                if(config.armor.locations.length > 1) {
                    small = true;
                }

                icons = config.armor.locations.map((location, i) => {
                    if(location === "all") {
                        return <AppText key={i}>{t("All")}</AppText>
                    } else {
                        return <Icon key={i} name={"sk-"+ location} size={small ? 15 : 20} color={colors.TEXT} />
                    }
                });

            }
            armor = <View style={[styles.rowCentered, styles.spaceBetween, {position: "absolute", top: 5, left: 0, maxWidth: "40%"}]}>
                <View>
                    <Icon name="sk-armor" size={small ? 20 : 25} color={colors.TEXT}/>
                    <AppText color={colors.INVERT_TEXT} style={{position: "absolute", top: small ? -4 : -4, left: small ? 6 : 8, fontSize: small ? 13 : 16}}>{config.armor.value}</AppText>
                </View>
                {icons}
            </View>
        }

        let stats = null;
        if(config.stats) {
            let altered = false;
            let spdAltered = false;
            let strAltered = false;
            let accAltered = false;
            let speed = config.stats.speed;
            let accuracy = config.stats.accuracy;
            let strength = config.stats.strength;

            if(this.props.attributes.SPD !== 0 || this.props.tempAttributes.SPD !== 0) {
                speed = speed + this.props.attributes.SPD;
                speed = speed + this.props.tempAttributes.SPD;
                altered = true;
                spdAltered = true;
            }

            if(this.props.attributes.ACC !== 0 || this.props.tempAttributes.ACC !== 0) {
                accuracy = accuracy - this.props.attributes.ACC;
                accuracy = accuracy - this.props.tempAttributes.ACC;
                altered = true;
                accAltered = true;
            }

            if(this.props.attributes.STR !== 0 || this.props.tempAttributes.STR !== 0) {
                strength = strength + this.props.attributes.STR;
                strength = strength + this.props.tempAttributes.STR;
                altered = true;
                strAltered = true;
            }

            const fontSize = Math.ceil(this.props.squareSize * .08);
            const children = this.state.expandedStats ?
                [
                    <View key="spd" style={[styles.row]}>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{config.stats.speed}</AppText>
                        <View style={[styles.centeredItems, {width: "20%"}]}><AppText style={{fontSize: fontSize, color: "black"}}>+</AppText></View>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{this.props.attributes.SPD}</AppText>
                        <View style={[styles.centeredItems, {width: "20%"}]}><AppText style={{fontSize: fontSize, color: "black"}}>+</AppText></View>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{this.props.tempAttributes.SPD}</AppText>
                    </View>,
                    <View key="acc" style={[styles.row]}>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{config.stats.accuracy}</AppText>
                        <View style={[styles.centeredItems, {width: "20%"}]}><AppText style={{fontSize: fontSize, color: "black"}}>-</AppText></View>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{this.props.attributes.ACC}</AppText>
                        <View style={[styles.centeredItems, {width: "20%"}]}><AppText style={{fontSize: fontSize, color: "black"}}>-</AppText></View>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{this.props.tempAttributes.ACC}</AppText>
                    </View>,
                    <View key="str" style={[styles.row]}>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{config.stats.strength}</AppText>
                        <View style={[styles.centeredItems, {width: "20%"}]}><AppText style={{fontSize: fontSize, color: "black"}}>+</AppText></View>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{this.props.attributes.STR}</AppText>
                        <View style={[styles.centeredItems, {width: "20%"}]}><AppText style={{fontSize: fontSize, color: "black"}}>+</AppText></View>
                        <AppText style={{fontSize: fontSize, color: "black"}}>{this.props.tempAttributes.STR}</AppText>
                    </View>
                ]
                : [
                    <AppText key="spd" style={{fontSize: fontSize, color: spdAltered ? colors.HIGHLIGHT : "black"}}>{speed}</AppText>,
                    <AppText key="acc" style={{fontSize: fontSize, color: accAltered ? colors.HIGHLIGHT : "black"}}>{accuracy}+</AppText>,
                    <AppText key="str" style={{fontSize: fontSize, color: strAltered ? colors.HIGHLIGHT : "black"}}>{strength}</AppText>
                ];
            stats = <TouchableOpacity style={{position: "absolute", top: 0, left: 0, padding: 3}} onPress={this.toggleExpandedStats}>
                <View style={[styles.centeredItems, {borderRadius: 10, borderWidth: 1, paddingLeft: 1, paddingRight: 1, backgroundColor: colors.WHITE, borderColor: (altered ? colors.HIGHLIGHT : "black")}]}>
                    {children}
                </View>
            </TouchableOpacity>
        }

        return <View style={[styles.container, styles.centeredItems]}>
            <View style={{width: (100 - blockDec - 5) + "%", marginTop: ((blockDec / 2) + 10) + "%" }}>
                <AppText style={[styles.itemText, {textAlign: "center"}]}>{config.title}</AppText>
            </View>
            {affinities}
            {armor}
            {stats}
        </View>
    }


    toggleExpandedStats = () => {
        this.setState({expandedStats: !this.state.expandedStats});
    }


    renderGearSelector = (colors) => {
        let sections = [];

        if(this.props.config) {
            sections.push(
                <View key="clear" style={[styles.centered, styles.container, {marginBottom: 15, marginLeft: "10%", marginRight: "10%"}]}>
                    <Button title={t("Clear Slot")} onPress={this.onClear} shape="rounded"/>
                </View>
            );
        }

        for(let location in this.props.availableGear) {
            sections.push(<View key={location}>
                <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', marginBottom: 5}]} onPress={this.toggleCollapsed(location)}>
                    <AppText style={styles.title}>{getLocationTitle(location, LocationConfig)}</AppText>
                    <Icon name={this.state.openedSections[location] === true ? "chevron-down" : "chevron-right"} color={colors.HIGHLIGHT} size={40}/>
                </TouchableOpacity>
                <Collapsible collapsed={this.state.openedSections[location] !== true}>
                    {Object.keys(this.props.availableGear[location]).map((item, i) => {
                        return <ListItem
                            key={i}
                            id={item}
                            title={GearConfig[location][item].title}
                            onPress={this.selectGear(location)}
                            buttonText={t("Select")}
                            preButton={<SmallGearAffinities affinities={GearConfig[location][item].affinities}/>}
                        />
                    })}
                </Collapsible>
            </View>);
        }

        return sections;
    }

    onClear = () => {
        this.setState({showPopup: false, openedSections: {}});
        this.props.onChange(this.props.x, this.props.y, null);
    }

    selectGear = (location) => {
        return (id) => {
            this.setState({showPopup: false, openedSections: {}});
            this.props.onChange(this.props.x, this.props.y, {name: id, location});
        }
    }

    toggleCollapsed = (name) => {
        return () => {
            let openedSections = {...this.state.openedSections};

            if(openedSections[name]) {
                delete openedSections[name];
            } else {
                openedSections[name] = true;
            }

            this.setState({openedSections});
        }
    }

    handleOnPress = () => {
        if(this.props.isSwap) {
            this.props.onSwapPress(this.props.x, this.props.y);
        } else {
            this.setState({showPopup: true});
        }
    }

    hideGearPicker = () => {
        this.setState({showPopup: false});
    }

}

LoadoutSquare.propTypes = {
    config: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    x: PropTypes.number,
    y: PropTypes.number,
    availableGear: PropTypes.object,
    isSwap: PropTypes.bool,
    onSwapPress: PropTypes.func,
    selected: PropTypes.bool,
    squareSize: PropTypes.number.isRequired,
    attributes: PropTypes.object.isRequired,
    tempAttributes: PropTypes.object.isRequired,
}

LoadoutSquare.defaultProps = {
    isSwap: false,
    selected: false,
}
