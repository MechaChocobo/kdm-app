'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
} from 'react-native';

import styles from '../../../style/styles';
import { t } from '../../../helpers/intl';
import AppText from '../../shared/app-text';
import Button from '../../shared/button';
import { IncrementorHidden } from '../../shared/IncrementorHidden';

export default class LoadoutAttributes extends Component {
    render() {
        return (
            <View>
                <View style={[styles.rowCentered, styles.spaceBetween]}>
                    <AppText style={styles.title}>{t("Loadout Attributes")}</AppText>
                    <Button title={t("Reset")} onPress={this.resetAttributes} shape="pill"/>
                </View>
                <View style={localStyles.rowWrap}>
                    <View style={localStyles.columnWrap}>
                        <AppText style={styles.bold}>{t("MOV")}</AppText>
                        <IncrementorHidden
                            value={this.props.attributes.MOV}
                            onIncrease={this.increaseStat}
                            onDecrease={this.decreaseStat}
                            id="MOV"
                        />
                    </View>
                    <View style={localStyles.columnWrap}>
                        <AppText style={styles.bold}>{t("ACC")}</AppText>
                        <IncrementorHidden
                            value={this.props.attributes.ACC}
                            onIncrease={this.increaseStat}
                            onDecrease={this.decreaseStat}
                            id="ACC"
                        />
                    </View>
                    <View style={localStyles.columnWrap}>
                        <AppText style={styles.bold}>{t("STR")}</AppText>
                        <IncrementorHidden
                            value={this.props.attributes.STR}
                            onIncrease={this.increaseStat}
                            onDecrease={this.decreaseStat}
                            id="STR"
                        />
                    </View>
                    <View style={localStyles.columnWrap}>
                        <AppText style={styles.bold}>{t("EVA")}</AppText>
                        <IncrementorHidden
                            value={this.props.attributes.EVA}
                            onIncrease={this.increaseStat}
                            onDecrease={this.decreaseStat}
                            id="EVA"
                        />
                    </View>
                    <View style={localStyles.columnWrap}>
                        <AppText style={styles.bold}>{t("LCK")}</AppText>
                        <IncrementorHidden
                            value={this.props.attributes.LCK}
                            onIncrease={this.increaseStat}
                            onDecrease={this.decreaseStat}
                            id="LCK"
                        />
                    </View>
                    <View style={localStyles.columnWrap}>
                        <AppText style={styles.bold}>{t("SPD")}</AppText>
                        <IncrementorHidden
                            value={this.props.attributes.SPD}
                            onIncrease={this.increaseStat}
                            onDecrease={this.decreaseStat}
                            id="SPD"
                        />
                    </View>
                </View>
            </View>
        );
    }

    increaseStat = (stat) => {
        let stats = {...this.props.attributes};
        stats[stat] += 1;
        this.onChange(stats);
    }

    decreaseStat = (stat) => {
        let stats = {...this.props.attributes};
        stats[stat] -= 1;
        this.onChange(stats);
    }

    resetAttributes = () => {
        this.onChange({
            MOV: 0,
            ACC: 0,
            STR: 0,
            EVA: 0,
            LCK: 0,
            SPD: 0,
        });
    }

    onChange = (attributes) => {
        this.props.onChange(attributes);
    }
}

LoadoutAttributes.propTypes = {
    attributes: PropTypes.object,
    onChange: PropTypes.func.isRequired,
}

LoadoutAttributes.defaultProps = {
    attributes: {
        MOV: 0,
        ACC: 0,
        STR: 0,
        EVA: 0,
        LCK: 0,
        SPD: 0,
    },
}

const localStyles = StyleSheet.create({
    columnWrap: {
        flex: .1666666,
        alignItems: "center",
    },
    rowWrap: {
        flexDirection: "row",
        minHeight: 100,
        flex: 1,
    }
});
