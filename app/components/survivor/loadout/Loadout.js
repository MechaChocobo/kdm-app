'use strict';

import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Uuid from 'react-native-uuid';

import {
    View,
    Dimensions,
    Alert,
} from 'react-native';

import {survivorActionWrapper} from '../../../actions/survivors';
import {settlementActionWrapper} from '../../../actions/settlement';
import { goBack } from '../../../actions/router';
import { t } from '../../../helpers/intl';
import { getColors } from '../../../selectors/general';
import styles from '../../../style/styles';
import AppTextInput from '../../shared/AppTextInput';
import RoundControl from '../../shared/buttons/RoundControl';
import LoadoutSquare from './LoadoutSquare';
import { getOwnedGear } from '../../../selectors/gear-storage';
import SimpleToast from 'react-native-simple-toast';
import Popup from '../../shared/popup';
import Button from '../../shared/button';
import ListItem from '../../shared/ListItem';
import ArmorSet from '../ArmorSet';
import Hr from '../../shared/Hr';
import LoadoutAttributes from './LoadoutAttributes';

class Loadout extends PureComponent {
    state = {
        width: Dimensions.get('window').width,
        showSelect: false,
        swapping: false,
        swapOrigin: {},
    }

    componentWillMount = () => {
        Dimensions.addEventListener("change", this.setDimensions);
    }

    componentWillUnmount = () => {
        Dimensions.removeEventListener("change", this.setDimensions);
    }

    setDimensions = () => {
        this.setState({width: Dimensions.get('window').width});
    }

    render() {
        const swapStyles = this.state.swapping ? {borderColor: this.props.colors.HIGHLIGHT} : {borderColor: "transparent"};
        return(
            <View style={[styles.container, {marginTop: 20}]}>
                <View style={[styles.width90, styles.centered, styles.rowCentered]}>
                    <RoundControl
                        icon="autorenew"
                        style={{marginRight: 15}}
                        onPress={this.onLoadoutSelect}
                    />
                    <AppTextInput
                        value={this.props.loadout.name ? this.props.loadout.name : ''}
                        onChangeText={this.onLoadoutNameChange}
                        returnKeyType="done"
                        placeholder={t("Loadout Name")}
                        style={{ flex:1, marginRight: 20}}
                    />
                    <RoundControl
                        icon="swap-horizontal"
                        style={{marginRight: 15}}
                        onPress={this.onSwap}
                    />
                    <RoundControl
                        icon="content-save"
                        style={{marginRight: 15}}
                        onPress={this.onSave}
                        disabled={!this.props.loadout.name || this.props.loadout.unsaved !== true}
                        onDisabledPress={this.onSaveDisabled}
                    />
                    <RoundControl
                        icon="delete"
                        onPress={this.onDelete}
                        disabled={!this.props.loadout.id}
                        onDisabledPress={this.onDeleteDisabled}
                    />
                </View>
                <View style={[{flexShrink: 1, margin: 8, marginBottom: 0, borderWidth: 2}, swapStyles]}>
                    {this.renderGrid()}
                </View>
                <Hr />
                <View style={[styles.width90, styles.centered]}>
                    <ArmorSet settlementId={this.props.settlementId} id={this.props.id} />
                </View>
                <Hr />
                <View style={[styles.width90, styles.centered]}>
                    <LoadoutAttributes
                        attributes={this.props.loadout.attributes}
                        onChange={this.onAttributesChange}
                    />
                </View>
                {this.state.showSelect && <Popup
                    visible={this.state.showSelect}
                    onDismissed={this.onLoadoutSelectHide}
                    title={t("Select Loadout")}
                >
                    {this.renderLoadoutSelect()}
                </Popup>}
            </View>
        );
    }

    onAttributesChange = (attributes) => {
        this.props.survivorActionWrapper("setLoadoutAttributes", [this.props.settlementId, this.props.id, attributes]);
    }

    onSwap = () => {
        if(this.state.swapping) {
            this.setState({swapping: false, swapOrigin: {}});
        } else {
            SimpleToast.show(t("Tap two slots to swap them"));
            this.setState({swapping: true});
        }
    }

    onLoadoutSelect = () => {
        this.setState({showSelect: true});
    }

    onLoadoutSelectHide = () => {
        this.setState({showSelect: false});
    }

    renderLoadoutSelect = () => {
        return <View>
            <Button title={t("New Empty Loadout")} onPress={this.confirmNewLoadout} shape="rounded" style={{marginBottom: 10}}/>
            {this.props.allLoadouts.map((loadout, i) => {
                return <ListItem
                    key={i}
                    style={{marginBottom: 10}}
                    title={loadout.name}
                    id={loadout.id}
                    buttonText={t("Choose")}
                    onPress={this.confirmLoadoutChange}
                    preButton={<RoundControl
                        icon="content-copy"
                        style={{marginRight: 15}}
                        id={loadout.id}
                        onPress={this.copyLoadout}
                        gradient={false}
                        color={this.props.colors.WHITE}
                        iconColor={this.props.colors.HIGHLIGHT}
                    />}
                />
            })}
        </View>
    }

    copyLoadout = (id) => {
        this.onLoadoutSelectHide();
        let newLoadout = {...this.props.loadoutMap[id]};
        newLoadout.id = Uuid.v4();
        newLoadout.name = newLoadout.name + "(Copy)";
        this.props.survivorActionWrapper("setLoadout", [this.props.settlementId, this.props.id, newLoadout, true]);
    }

    confirmLoadoutChange = (id) => {
        if(this.props.loadout.unsaved) {
            Alert.alert(t("Are you sure?"), t("Your current loadout has unsaved changes.  Changing loadouts will lose them!"), [
                {text: t("Cancel"), onPress: () => {}},
                {text: t("Confirm"), onPress: () => {this.onLoadoutChange(id)}}
            ]);
        } else {
            this.onLoadoutChange(id);
        }
    }

    confirmNewLoadout = () => {
        if(this.props.loadout.unsaved) {
            Alert.alert(t("Are you sure?"), t("Your current loadout has unsaved changes.  Changing loadouts will lose them!"), [
                {text: t("Cancel"), onPress: () => {}},
                {text: t("Confirm"), onPress: this.newLoadout}
            ]);
        } else {
            this.newLoadout();
        }
    }

    onLoadoutChange = (id) => {
        this.onLoadoutSelectHide();
        this.props.survivorActionWrapper("setLoadout", [this.props.settlementId, this.props.id, this.props.loadoutMap[id]]);
    }

    newLoadout = () => {
        this.onLoadoutSelectHide();
        this.props.survivorActionWrapper("setLoadout", [this.props.settlementId, this.props.id, {}]);
    }

    onSaveDisabled = () => {
        if(!this.props.loadout.name) {
            SimpleToast.show(t("A saved loadout must have a name"));
            return;
        }

        if(this.props.loadout.unsaved !== true) {
            SimpleToast.show(t("Loadout is already saved"));
            return;
        }
    }

    onDeleteDisabled = () => {
        SimpleToast.show(t("Loadout must be saved before it can be deleted"));
    }

    renderGrid = () => {
        let rows = [];
        for(let x = 0; x < 3; x++) {
            let columns = [];
            for(let y = 0; y < 3; y++) {
                const squareSize = this.getSquareSize();
                columns.push(<View key={y} style={{width: squareSize, height: squareSize}}>
                    <LoadoutSquare
                        config={this.getGearConfig(x, y)}
                        onChange={this.onGearChange}
                        x={x}
                        y={y}
                        availableGear={this.props.availableGear}
                        isSwap={this.state.swapping}
                        onSwapPress={this.onSwapPress}
                        selected={this.state.swapOrigin.x === x && this.state.swapOrigin.y === y}
                        squareSize={squareSize}
                        attributes={this.props.attributes}
                        tempAttributes={this.props.tempAttributes}
                    />
                </View>);
            }
            rows.push(<View key={x} style={styles.rowCentered}>{columns}</View>)
        }

        return rows;
    }

    onSwapPress = (x, y) => {
        if(Object.keys(this.state.swapOrigin).length === 0) {
            this.setState({swapOrigin: {x, y}});
        } else {
            this.props.survivorActionWrapper("swapLoadoutSpace", [this.props.settlementId, this.props.id, this.state.swapOrigin.x, this.state.swapOrigin.y, x, y]);
            this.setState({swapOrigin: {}});
        }
    }

    onGearChange = (x, y, gear) => {
        this.props.survivorActionWrapper("setLoadoutGear", [this.props.settlementId, this.props.id, x, y, gear]);
    }

    getGearConfig = (x, y) => {
        if(this.props.loadout.gear) {
            return this.props.loadout.gear[x][y];
        } else {
            return null;
        }
    }

    getSquareSize = () => {
        const divider = this.props.isGrid ? 6 : 3;
        return Math.floor((this.state.width - 20) / divider);
    }

    onDelete = () => {
        if(!this.props.loadout.id) {
            return;
        }

        Alert.alert(t("Are you sure?"), t("Deleting a loadout will also remove it from any survivors who have it equipped."), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.deleteLoadout}
        ]);
    }

    deleteLoadout = () => {
        this.props.settlementActionWrapper("deleteLoadout", [this.props.settlementId, this.props.loadout.id]);
    }

    onSave = () => {
        this.props.survivorActionWrapper("saveLoadout", [this.props.settlementId, this.props.id, this.props.loadout]);
    }

    onLoadoutNameChange = (val) => {
        this.props.survivorActionWrapper("setLoadoutName", [this.props.settlementId, this.props.id, val]);
    }

}

function mapStateToProps(state, props) {
    return {
        loadout: state.survivorReducer.survivors[props.settlementId][props.id].loadout,
        availableGear: getOwnedGear(state),
        allLoadouts: Object.values(state.settlementReducer.settlements[props.settlementId].loadouts),
        loadoutMap: state.settlementReducer.settlements[props.settlementId].loadouts,
        attributes: state.survivorReducer.survivors[props.settlementId][props.id].attributes,
        tempAttributes: state.survivorReducer.survivors[props.settlementId][props.id].tempAttributes,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper, settlementActionWrapper, goBack}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Loadout);
