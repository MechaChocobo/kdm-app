'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';
import ListItem from '../shared/ListItem';
import { getColorCode } from '../../helpers/helpers';

import {
    TouchableOpacity,
    View,
    StyleSheet,
} from 'react-native';

import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import Popup from '../shared/popup';
import styles from '../../style/styles';
import AppText from '../shared/app-text';

class XP extends Component {
    colors = null;
    constructor(props) {
        super(props);

        this.state = {
            showPopup: false,
        }

        this.colors = {
            "blue": t("Blue"),
            "brown": t("Brown"),
            "gray" : t("Gray"),
            "green" : t("Green"),
            "orange" : t("Orange"),
            "purple" : t("Purple"),
            "red" : t("Red"),
            "teal" : t("Teal"),
            "white" : t("White"),
            "yellow" : t("Yellow"),
        };
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    getOptions = () => {
        let options = [];

        options.push(<ListItem
            key="-1"
            title={t("None")}
            id=""
            onPress={this.setColor}
            buttonText={t("Choose")}
        />);

        let key = 0;
        for(let id in this.colors) {
            options.push(<ListItem
                key={key}
                title=""
                id={id}
                onPress={this.setColor}
                buttonText={t("Choose")}
            >
                <View style={styles.width50}>
                    <AppText style={styles.title} color={this.props.colors.WHITE}>{this.colors[id]}</AppText>
                </View>
                {this.getColorCircle(id)}
            </ListItem>);
            key++;
        }

        return options;
    }

    getColorCircle = (color = "") => {
        if(!color) {
            return null;
        }

        return <View style={[localStyles.circle, {backgroundColor: getColorCode(color, this.props.colors)}]}/>
    }

    render() {
        return (
            <View style={[styles.container, styles.rowCentered]}>
                <AppText style={[styles.headerFontSize, styles.flex50]}>{t("Color")}</AppText>
                <TouchableOpacity onPress={this.showPopup} style={[styles.flex50, styles.rowCentered]}>
                    <AppText style={styles.headerFontSize}>{this.props.departingColor ? this.colors[this.props.departingColor] : t("None")}</AppText>
                    {this.getColorCircle(this.props.departingColor)}
                </TouchableOpacity>
                {this.state.showPopup && <Popup
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                    title={t("Select Color")}
                >
                    {this.getOptions()}
                </Popup>}
            </View>
        );
    }

    setColor = (color) => {
        this.props.survivorActionWrapper('setDepartingColor', [this.props.settlementId, this.props.id, color]);
        this.setState({showPopup: false});
    }
}

function mapStateToProps(state, props) {
    return {
        departing: state.survivorReducer.survivors[props.settlementId][props.id].departing,
        departingColor: state.survivorReducer.survivors[props.settlementId][props.id].departingColor,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(XP);

const localStyles = StyleSheet.create({
    circle: {
        width: 25,
        height: 25,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: "black",
        marginLeft: 10,
    }
});
