'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    View,
    TouchableOpacity
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import sharedStyles from '../../style/styles';
import Text from '../shared/app-text';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';

class LifetimeReroll extends Component {
    render() {
        return <View style={[sharedStyles.container, sharedStyles.rowCentered]}>
            <Text style={[sharedStyles.headerFontSize, sharedStyles.flex50]}>{t("Lifetime Reroll")}</Text>
            <TouchableOpacity onPress={this.setHasReroll} style={sharedStyles.flex25}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Icon name={this.props.hasReroll || this.props.isSotf ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.isSotf ? this.props.colors.GRAY : this.props.colors.TEXT}/>
                </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.toggleReroll} style={[sharedStyles.flex25, {paddingLeft: 10}]}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Icon name={this.props.rerollUsed ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.colors.TEXT}/>
                    <Text style={sharedStyles.itemText}>{t("Used")}</Text>
                </View>
            </TouchableOpacity>
        </View>
    }

    toggleReroll = () => {
        this.props.survivorActionWrapper('toggleReroll', [this.props.settlementId, this.props.id]);
    }

    setHasReroll = () => {
        if(this.props.isSotf) {
            Toast.show("With Survival of the Fittest all survivors always have a reroll!", Toast.LONG);
            return;
        }

        this.props.survivorActionWrapper('setHasReroll', [this.props.settlementId, this.props.id, !this.props.hasReroll]);
    }
}

function mapStateToProps(state, props) {
    return {
        hasReroll: state.survivorReducer.survivors[props.settlementId][props.id].hasReroll,
        rerollUsed: state.survivorReducer.survivors[props.settlementId][props.id].reroll,
        isSotf: (state.settlementReducer.settlements[props.settlementId].principles.newLife === "survivalOfTheFittest"),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(LifetimeReroll);
