'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Collapsible from 'react-native-collapsible';

import {survivorActionWrapper} from '../../actions/survivors';

import {
    View,
    TouchableOpacity
} from 'react-native';

import Header from './constellations/header';
import Content from './constellations/content';

import Text from '../shared/app-text';
import Icon from 'react-native-vector-icons/MaterialIcons';
import sharedStyles from '../../style/styles';
import { getConstellationRows, getConstellationCols, getNumDragonTraits, getAbilityConfig } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundControl from '../shared/buttons/RoundControl';
import Zebra from '../shared/Zebra';
const abilities = getAbilityConfig();

let headerRows = getConstellationRows();
let headerCols = getConstellationCols();

class Constellations extends Component {
    constructor(props) {
        super(props);
        this.state = { showDetails: false};
    }

    toggleDetail = () => {
        this.setState({showDetails: !this.state.showDetails});
    }

    render() {
        if(this.props.campaign !== "PotStars") {
            return null;
        }

        return (
            <View>
                <View style={[sharedStyles.row, sharedStyles.spaceBetween, sharedStyles.width90, sharedStyles.centered, {marginBottom: 10}]}>
                    <Text style={sharedStyles.title}>{t("Constellations")}</Text>
                    <RoundControl icon="sk-constellation" onPress={this.toggleDetail} iconSize={30}/>
                </View>
                <Collapsible collapsed={this.state.showDetails}>
                    <Zebra style={[sharedStyles.width90, sharedStyles.centered, {borderRadius: 5, overflow: "hidden", padding: 5}]} zebra={true}>
                        {this.props.constellation !== "" && <Text style={sharedStyles.itemText}>{t("Constellation")}: {this.props.constellation} - {t(abilities["constellation"+this.props.constellation].description)}</Text>}
                        <Text style={sharedStyles.itemText}>{t("Number of Dragon Traits")}: {getNumDragonTraits(this.props.constellationMap)}</Text>
                    </Zebra>
                </Collapsible>
                <Collapsible collapsed={!this.state.showDetails}>
                    <View style={{flex: 1, paddingLeft: 5}}>
                        <View style={{flex: .2}}><View style={{flexDirection: "row", flex: 1}}>
                            <Text style={{flex: .2}}></Text>
                            <Header header={headerCols[0]} onSet={this.setConstellation} selected={this.props.constellation === headerCols[0]}/>
                            <Header header={headerCols[1]} onSet={this.setConstellation} selected={this.props.constellation === headerCols[1]}/>
                            <Header header={headerCols[2]} onSet={this.setConstellation} selected={this.props.constellation === headerCols[2]}/>
                            <Header header={headerCols[3]} onSet={this.setConstellation} selected={this.props.constellation === headerCols[3]}/>
                        </View></View>
                        <View style={{flex: .2}}><View style={{flexDirection: "row", flex: 1}}>
                            <Header header={headerRows[0]} onSet={this.setConstellation} selected={this.props.constellation === headerRows[0]}/>
                            <Content content={t("9 Understanding")} row={0} col={0} onToggle={this.toggleItem} selected={this.props.constellationMap[0][0]}/>
                            <Content content={t("Destined disorder")} row={0} col={1} onToggle={this.toggleItem} selected={this.props.constellationMap[0][1]}/>
                            <Content content={t("Fated Blow fighting art")} row={0} col={2} onToggle={this.toggleItem} selected={this.props.constellationMap[0][2]}/>
                            <Content content={t("Pristine ability")} row={0} col={3} onToggle={this.toggleItem} selected={this.props.constellationMap[0][3]}/>
                        </View></View>
                        <View style={{flex: .2}}><View style={{flexDirection: "row", flex: 1}}>
                            <Header header={headerRows[1]} onSet={this.setConstellation} selected={this.props.constellation === headerRows[1]}/>
                            <Content content={t("Reincarnated surname")} row={1} col={0} onToggle={this.toggleItem} selected={this.props.constellationMap[1][0]}/>
                            <Content content={t("Frozen star secret fighting art")} row={1} col={1} onToggle={this.toggleItem} selected={this.props.constellationMap[1][1]}/>
                            <Content content={t("Iridescent Hide ability")} row={1} col={2} onToggle={this.toggleItem} selected={this.props.constellationMap[1][2]}/>
                            <Content content={t("Champion's Rite fighting art")} row={1} col={3} onToggle={this.toggleItem} selected={this.props.constellationMap[1][3]}/>
                        </View></View>
                        <View style={{flex: .2}}><View style={{flexDirection: "row", flex: 1}}>
                            <Header header={headerRows[2]} onSet={this.setConstellation} selected={this.props.constellation === headerRows[2]}/>
                            <Content content={t("Scar")} row={2} col={0} onToggle={this.toggleItem} selected={this.props.constellationMap[2][0]}/>
                            <Content content={t("Noble surname")} row={2} col={1} onToggle={this.toggleItem} selected={this.props.constellationMap[2][1]}/>
                            <Content content={t("Weapon Mastery")} row={2} col={2} onToggle={this.toggleItem} selected={this.props.constellationMap[2][2]}/>
                            <Content content={t("1+ accuracy attribute")} row={2} col={3} onToggle={this.toggleItem} selected={this.props.constellationMap[2][3]}/>
                        </View></View>
                        <View style={{flex: .2}}><View style={{flexDirection: "row", flex: 1}}>
                            <Header header={headerRows[3]} onSet={this.setConstellation} selected={this.props.constellation === headerRows[3]}/>
                            <Content content={t("Oracle's Eye ability")} row={3} col={0} onToggle={this.toggleItem} selected={this.props.constellationMap[3][0]}/>
                            <Content content={t("Unbreakable fighting art")} row={3} col={1} onToggle={this.toggleItem} selected={this.props.constellationMap[3][1]}/>
                            <Content content={t("3+ strength attribute")} row={3} col={2} onToggle={this.toggleItem} selected={this.props.constellationMap[3][2]}/>
                            <Content content={t("9 courage")} row={3} col={3} onToggle={this.toggleItem} selected={this.props.constellationMap[3][3]}/>
                        </View></View>
                    </View>
                </Collapsible>
            </View>
        );
    }

    // toggle invidivual item from the map
    toggleItem = (row, col) => {
        this.props.survivorActionWrapper('toggleConstellationMap', [this.props.settlementId, this.props.id, row, col]);
    }

    // set a constellation
    setConstellation = (name) => {
        this.props.survivorActionWrapper('setConstellation', [this.props.settlementId, this.props.id, name]);
    }
}

function mapStateToProps(state, props) {
    return {
        campaign: state.settlementReducer.settlements[props.settlementId].campaign,
        constellation: state.survivorReducer.survivors[props.settlementId][props.id].constellation,
        constellationMap: state.survivorReducer.survivors[props.settlementId][props.id].constellationMap,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Constellations);
