'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import {StyleSheet, View} from 'react-native';

import styles from '../../../style/styles';
import { getDepartingSurvivors } from '../../../selectors/survivors';
import Survivor from '../../survivor';
import BackMenu from '../../shared/BackMenu';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import { getColors } from '../../../selectors/general';

class GridList extends Component {
    render() {
        let firstRowHeight = styles.flex50;
        if(this.props.survivors.length <= 2) {
            firstRowHeight = styles.container;
        }

        let firstColWidth = styles.flex50;
        let firstHalfWidth = true;
        if(this.props.survivors.length < 2) {
            firstColWidth = styles.container;
            firstHalfWidth = false;
        }

        let secondColWidth = styles.flex50;
        let secondHalfWidth = true;
        if(this.props.survivors.length === 3) {
            secondColWidth = styles.container;
            secondHalfWidth = false;
        }

        return (
            <View style={styles.container}>
                <BackMenu />
                <AppText style={styles.pageHeader}>{t("Departing")}</AppText>
                <View style={[styles.row, firstRowHeight]}>
                    <View style={[firstColWidth, localStyles.topLeft, {borderColor: this.getBorderColor()}]}>{this.getSurvivor(0, firstHalfWidth)}</View>
                    {this.props.survivors.length > 1 && <View style={[styles.flex50, localStyles.topRight, {borderColor: this.getBorderColor()}]}>{this.getSurvivor(1)}</View>}
                </View>
                {this.props.survivors.length > 2 && <View style={[styles.row, styles.flex50]}>
                    <View style={[secondColWidth, localStyles.bottomLeft, {borderColor: this.getBorderColor()}]}>{this.getSurvivor(2, secondHalfWidth)}</View>
                    {this.props.survivors.length > 3 && <View style={[styles.flex50, localStyles.bottomRight, {borderColor: this.getBorderColor()}]}>{this.getSurvivor(3)}</View>}
                </View>}
            </View>
        );
    }

    getSurvivor = (index, halfWidth = true) => {
        if(this.props.survivors[index]) {
            return <Survivor survivor={this.props.survivors[index]} showNav={false} isGrid={true} halfWidth={halfWidth}/>
        }

        return null;
    }

    getBorderColor = () => {
        return this.props.colors.HIGHLIGHT;
    }
}

function mapStateToProps(state, props) {
    return {
        survivors: getDepartingSurvivors(state),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GridList);

const localStyles = StyleSheet.create({
    topLeft: {
        borderRightWidth: 2,
        borderBottomWidth: 2
    },
    topRight: {
        borderLeftWidth: 2,
        borderBottomWidth: 2,
    },
    bottomLeft: {
        borderTopWidth: 2,
        borderRightWidth: 2,
    },
    bottomRight: {
        borderTopWidth: 2,
        borderLeftWidth: 2,
    }
});
