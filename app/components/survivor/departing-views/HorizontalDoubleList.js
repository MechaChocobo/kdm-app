'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import {
    View,
    FlatList,
    Dimensions,
    StyleSheet,
} from 'react-native';

import {getDepartingSurvivors} from '../../../selectors/survivors';
import {goBack, goToSurvivor, setDepartingSurvivor} from '../../../actions/router';
import Survivor from '../../survivor';
import styles from '../../../style/styles';
import { survivorActionWrapper } from '../../../actions/survivors';
import { getColors } from '../../../selectors/general';
import BackMenu from '../../shared/BackMenu';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';

const window = Dimensions.get('window');

class HorizontalDoubleList extends Component {
    list = null;
    page = 0;
    goBackTimeout = null;

    componentWillMount = () => {
        this.page = Math.floor(this.props.selectedSurvivorIndex / 2);
    }

    renderSurvivor = (item) => {
        let survivors = item.item;
        let firstStyle = styles.flex50;
        let half = true;
        if(!survivors[1]) {
            firstStyle = styles.container;
            half = false;
        }

        return <View style={[styles.container, styles.row]}>
            <View style={[firstStyle, localStyles.border, {borderColor: this.props.colors.HIGHLIGHT}]}>
                <Survivor survivor={survivors[0]} showNav={false} isGrid={true} halfWidth={half}/>
            </View>
            {survivors[1] && <View style={styles.flex50}>
                <Survivor survivor={survivors[1]} showNav={false} isGrid={true} halfWidth={half}/>
            </View>}
        </View>
    }

    getItemLayout = (item, index) => {
        return {length: window.width, offset: window.width * index, index};
    }

    devNull = () => {}

    render() {
        if(this.page > Math.floor((this.props.survivors.length) / 2) || this.props.survivors.length === 0) {
            if(this.props.survivors.length > 0) {
                this.page = Math.floor((this.props.survivors.length) / 2);
                // issues a warning but works pretty well
                if(this.list) {
                    this.list.scrollToIndex({index: this.page, animated: false});
                }
            } else {
                if(!this.goBackTimeout) {
                    this.goBackTimeout = setTimeout(() => {
                        this.props.goBack();
                    }, 0);
                }

                return null;
            }
        }

        return (
            <View style={styles.container}>
                <BackMenu />
                <AppText style={styles.pageHeader}>{t("Departing")}</AppText>
                <FlatList
                    ref={(ref) => {this.list = ref}}
                    data={this.props.survivors}
                    renderItem={this.renderSurvivor}
                    keyExtractor={(item, index) => ''+index}
                    initialScrollIndex={Math.floor(this.props.selectedSurvivorIndex / 2)}
                    getItemLayout={this.getItemLayout}
                    onScrollToIndexFailed={this.devNull}
                    pagingEnabled={true}
                    horizontal={true}
                />
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    let survivors = getDepartingSurvivors(state);
    let grouped = [];

    for(let i = 0; i < survivors.length; i += 2) {
        let group = [];
        group.push(survivors[i]);
        if(survivors[i + 1]) {
            group.push(survivors[i + 1]);
        }

        grouped.push(group);
    }

    return {
        settlementId: state.pageReducer.settlementId,
        survivors: grouped,
        selectedSurvivorIndex: state.pageReducer.selectedSurvivorIndex - 1,
        departingSurvivorId: state.pageReducer.departingSurvivorId,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goBack, goToSurvivor, setDepartingSurvivor, survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(HorizontalDoubleList);

const localStyles = StyleSheet.create({
    border: {
        borderRightWidth: 4,
    }
});
