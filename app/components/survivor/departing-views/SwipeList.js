'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import {
    View,
    FlatList,
    Dimensions
} from 'react-native';

import {getDepartingSurvivors} from '../../../selectors/survivors';
import {goBack, goToSurvivor, setDepartingSurvivor} from '../../../actions/router';
import Survivor from '../../survivor';
import sharedStyles from '../../../style/styles';
import { survivorActionWrapper } from '../../../actions/survivors';
import { getColors } from '../../../selectors/general';

const window = Dimensions.get('window');

class SwipeList extends Component {
    list = null;
    page = 0;

    componentWillMount = () => {
        this.page = this.props.selectedSurvivorIndex;
        if(this.props.selectedSurvivorIndex <= this.props.survivors.length - 1) {
            this.props.setDepartingSurvivor(this.props.survivors[this.props.selectedSurvivorIndex].id);
        }
    }

    renderSurvivor = (item) => {
        return <Survivor survivor={item.item} index={item.index}/>;
    }

    getItemLayout = (item, index) => {
        return {length: window.width, offset: window.width * index, index};
    }

    onScrollEnd = (e) => {
        let offset = e.nativeEvent.contentOffset;
        let viewSize = e.nativeEvent.layoutMeasurement;
        this.page = Math.floor((offset.x + viewSize.width * .10) / viewSize.width);

        if(this.props.survivors[this.page]) {
            this.props.setDepartingSurvivor(this.props.survivors[this.page].id);
        }
    }

    devNull = () => {}

    render() {
        if(this.page > this.props.survivors.length - 1) {
            if(this.props.survivors.length > 0) {
                this.page = this.props.survivors.length - 1;
                // issues a warning but works pretty well
                if(this.list) {
                    this.list.scrollToIndex({index: this.page, animated: false});
                }
            } else {
                setTimeout(() => {
                    this.props.goBack();
                }, 0);

                return null;
            }
        }

        if(this.props.survivors[this.page].id !== this.props.departingSurvivorId) {
            for (let i = 0; i < this.props.survivors.length; i++) {
                if(this.props.survivors[i].id === this.props.departingSurvivorId) {
                    this.page = i;
                    setTimeout(() => {
                        if(this.list) {
                            this.list.scrollToIndex({index: this.page, animated: false});
                        }
                    }, 0);
                }
            }
        }

        return (
            <View style={sharedStyles.container}>
                <FlatList
                    ref={(ref) => {this.list = ref}}
                    data={this.props.survivors}
                    renderItem={this.renderSurvivor}
                    keyExtractor={(item, index) => ''+index}
                    initialScrollIndex={this.props.selectedSurvivorIndex}
                    getItemLayout={this.getItemLayout}
                    onScrollToIndexFailed={this.devNull}
                    onMomentumScrollEnd={this.onScrollEnd}
                    pagingEnabled={true}
                    horizontal={true}
                />
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        settlementId: state.pageReducer.settlementId,
        survivors: getDepartingSurvivors(state),
        selectedSurvivorIndex: state.pageReducer.selectedSurvivorIndex - 1,
        departingSurvivorId: state.pageReducer.departingSurvivorId,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goBack, goToSurvivor, setDepartingSurvivor, survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SwipeList);
