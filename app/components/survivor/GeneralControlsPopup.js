'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    StyleSheet,
    Dimensions,
} from 'react-native';

import Retired from './retired';
import SkipNextHunt from './skip-next-hunt';
import LifetimeReroll from './lifetime-reroll';
import Dead from './dead';
import Gender from './gender';
import Unavailable from './unavailable';
import Parents from './parents';
import styles from '../../style/styles';
import { t } from '../../helpers/intl';
import AppText from '../shared/app-text';
import { getColors } from '../../selectors/general';
import { survivorActionWrapper } from '../../actions/survivors';
import AppTextInput from '../shared/AppTextInput';
import DepartingColor from './departing-color';
import Hr from '../shared/Hr';
import Admin from './admin';
import Epithet from './Epithet';

class GeneralControlsPopup extends Component {
    render() {
        return <View style={[localStyles.popupWrapper, {width: Dimensions.get('window').width}]}>
            <View height={20}/>
            <View style={[styles.centered, styles.width90]}>
                <View style={styles.rowCentered}>
                    <AppText style={[styles.headerFontSize]}>{t("Name")}</AppText>
                    {this.getNameInput()}
                </View>
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <Epithet id={this.props.id} settlementId={this.props.settlementId} />
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <DepartingColor id={this.props.id} settlementId={this.props.settlementId} />
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <Gender id={this.props.id} settlementId={this.props.settlementId}/>
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <Dead id={this.props.id} settlementId={this.props.settlementId} isGrid={this.props.isGrid} onDead={this.props.onDead}/>
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <SkipNextHunt id={this.props.id} settlementId={this.props.settlementId}/>
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <Retired id={this.props.id} settlementId={this.props.settlementId}/>
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <LifetimeReroll id={this.props.id} settlementId={this.props.settlementId}/>
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <Unavailable id={this.props.id} settlementId={this.props.settlementId}/>
            </View>
            <Hr style={styles.width95}/>
            <View style={[styles.centered, styles.width90]}>
                <Parents
                    id={this.props.id}
                    settlementId={this.props.settlementId}
                    onOptionsShow={this.showOptions}
                />
            </View>
            <Hr style={styles.width95}/>
            <Admin id={this.props.id} settlementId={this.props.settlementId} onDelete={this.props.onDelete}/>
        </View>
    }

    getNameInput = () => {
        return <AppTextInput
            style={{flex: 1, marginLeft: 20}}
            value={this.props.name}
            onChangeText={this.setName}
            inputStyle={{fontSize: 18}}
        />
    }

    showOptions = (type) => {
        this.props.onPageChange(type);
    }

    setName = (name) => {
        if(this.props.departing && this.props.sortAttribute === "name") {
            this.props.closePopup();
        }

        // pass: settlementId, id, name
        this.props.survivorActionWrapper('changeName', [this.props.settlementId, this.props.id, name]);
    }
}

function mapStateToProps(state, props) {
    return {
        isSotF: (state.settlementReducer.settlements[props.settlementId].principles.newLife == "survivalOfTheFittest"),
        gender: state.survivorReducer.survivors[props.settlementId][props.id].gender,
        skipNextHunt: state.survivorReducer.survivors[props.settlementId][props.id].skipNextHunt,
        retired: state.survivorReducer.survivors[props.settlementId][props.id].retired,
        dead: state.survivorReducer.survivors[props.settlementId][props.id].dead,
        unavailable: state.survivorReducer.survivors[props.settlementId][props.id].unavailable,
        hasReroll: state.survivorReducer.survivors[props.settlementId][props.id].hasReroll,
        rerollUsed: state.survivorReducer.survivors[props.settlementId][props.id].reroll,
        name: state.survivorReducer.survivors[props.settlementId][props.id].name,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GeneralControlsPopup);

const localStyles = StyleSheet.create({
    popupWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: "space-between",
    },
    editIcon: {
        alignSelf: "flex-start",
        padding: 10
    },
});
