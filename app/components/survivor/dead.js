'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    View,
    TouchableOpacity,
    Alert,
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import {settlementActionWrapper} from '../../actions/settlement';
import styles from '../../style/styles';
import AppText from '../shared/app-text';
import { goBack } from '../../actions/router';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import AppTextInput from '../shared/AppTextInput';
import Hr from '../shared/Hr';

class Dead extends Component {
    setDead = () => {
        if(this.props.dead) {
            return;
        }

        if((this.props.isSotf || this.props.hasReroll) && !this.props.reroll) {
            this.confirmKill();
        } else {
            this.doDead();
        }
    }

    doDead = () => {
        this.props.survivorActionWrapper('setDead', [this.props.settlementId, this.props.id, true]);
        if(this.props.onDead) {
            this.props.onDead();
        }

        // dead people don't typically go on adventures:
        // setDead removes departing/returning, so if the survivor is (was) departing, navigate elsewhere
        if(this.props.isDeparting && !this.props.isGrid) {
            this.props.goBack();
        }
    }

    confirmKill = () => {
        Alert.alert(t("They have a reroll!"), t("This survivor still has their lifetime reroll! Are you sure you want to kill them before using it?"), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.doDead}
        ]);
    }

    setAlive = () => {
        if(!this.props.dead) {
            return;
        }

        this.props.survivorActionWrapper('setDead', [this.props.settlementId, this.props.id, false]);
    }

    setCauseOfDeath = (value) => {
        this.props.survivorActionWrapper('setCauseOfDeath', [this.props.settlementId, this.props.id, value]);
    }

    render() {
        return(
            <View>
                <View style={[styles.container, styles.rowCentered]}>
                    <AppText style={[styles.headerFontSize, styles.flex50]}>{t("Dead or Alive")}</AppText>
                    <TouchableOpacity onPress={this.setAlive} style={styles.flex25}>
                        <View style={styles.rowCentered}>
                            <Icon name={!this.props.dead ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.colors.TEXT}/>
                            <AppText style={styles.itemText}>{t("Alive")}</AppText>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.setDead} style={[styles.flex25, {paddingLeft: 10}]}>
                        <View style={styles.rowCentered}>
                            <Icon name={this.props.dead ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.colors.TEXT}/>
                            <AppText style={styles.itemText}>{t("Dead")}</AppText>
                        </View>
                    </TouchableOpacity>
                </View>
                <Hr style={styles.width95}/>
                <View style={styles.rowCentered}>
                    <AppText style={[styles.headerFontSize]}>{t("Cause of Death")}</AppText>
                    <AppTextInput
                        style={{marginLeft: 20, flex: 1}}
                        value={this.props.causeOfDeath}
                        onChangeText={this.setCauseOfDeath}
                        inputStyle={{fontSize: 18}}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        dead: state.survivorReducer.survivors[props.settlementId][props.id].dead,
        causeOfDeath: state.survivorReducer.survivors[props.settlementId][props.id].causeOfDeath,
        isDeparting: state.survivorReducer.survivors[props.settlementId][props.id].departing !== false,
        hasReroll: state.survivorReducer.survivors[props.settlementId][props.id].hasReroll,
        reroll: state.survivorReducer.survivors[props.settlementId][props.id].reroll,
        isSotf: (state.settlementReducer.settlements[props.settlementId].principles.newLife === "survivalOfTheFittest"),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper, settlementActionWrapper, goBack}, dispatch);
}

Dead.defaultProps = {
    causeOfDeath: "",
}

export default connect(mapStateToProps, mapDispatchToProps)(Dead);
