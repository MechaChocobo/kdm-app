'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';
import { getSevereInjuryConfig, sortIdArrayByConfigTitle} from '../../helpers/helpers';

import {
    View,
} from 'react-native';

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';

let InjuryConfig = null;
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import Popup from '../shared/popup';
import AbilityText from '../shared/AbilityText';
import ListItem from '../shared/ListItem';
import RoundControl from '../shared/buttons/RoundControl';
import Hr from '../shared/Hr';

class SevereInjuries extends Component {
    constructor(props) {
        super(props);
        this.state = { showAddList: false};
        InjuryConfig = getSevereInjuryConfig();
    }

    showAddList = () => {
        this.setState({showAddList: true});
    }

    onAddListClosed = () => {
        this.setState({showAddList: false});
    }

    render() {
        return (
            <View>
                <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                    <Text style={sharedStyles.title}>{t("Severe Injuries")}</Text>
                    <RoundControl icon="plus" onPress={this.showAddList}/>
                </View>
                {this.props.injuries.length > 0 && <Hr />}
                <View style={sharedStyles.contentWrapper}>
                    {this.renderInjuries()}
                    <Popup title={t("Select Injury")} visible={this.state.showAddList} onDismissed={this.onAddListClosed}>
                        {this.getPicker()}
                    </Popup>
                </View>
            </View>
        );
    }

    getPicker = () => {
        let items = this.props.allInjuries.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={t(InjuryConfig[name].title)}
                    id={name}
                    onPress={this.addSevereInjury}
                    buttonText={t("Add")}
                    confirm={false}
                />
            );
        });

        return <View style={{width: "90%", alignSelf: "center"}}>
            {items}
        </View>
    }

    addSevereInjury = (name) => {
        this.props.survivorActionWrapper('addSevereInjury', [this.props.settlementId, this.props.id, name]);
        this.onAddListClosed();
    }

    removeSevereInjury = (name) => {
        this.props.survivorActionWrapper('removeSevereInjury', [this.props.settlementId, this.props.id, name]);
    }

    renderInjuries = () => {
        return this.props.injuries.map((name, i) => {
            return <AbilityText
                key={i}
                title={t(InjuryConfig[name].title)}
                description={t(InjuryConfig[name].description)}
                id={name}
                onPress={this.removeSevereInjury}
            />;
        })
    }
}

function mapStateToProps(state, props) {
    return {
        injuries: state.survivorReducer.survivors[props.settlementId][props.id].severeInjuries,
        allInjuries: sortIdArrayByConfigTitle(state.settlementReducer.settlements[props.settlementId].allSevereInjuries, getSevereInjuryConfig()),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SevereInjuries);
