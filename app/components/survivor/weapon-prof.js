'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast';

import {survivorActionWrapper} from '../../actions/survivors';
import { settlementActionWrapper } from '../../actions/settlement';

import {
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import Text from '../../components/shared/app-text';
import styles from '../../style/styles';

import { getWeaponProfConfig, sortIdArrayByConfigTitle, getUnselectedArray, CHECKBOX_SIZE } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundControl from '../shared/buttons/RoundControl';
import MilestoneDots from '../shared/MilestoneDots';
import Icon from '../shared/Icon';
import Popup from '../shared/popup';
import ListItem from '../shared/ListItem';
import Hr from '../shared/Hr';
import AppText from '../../components/shared/app-text';

let ProficiencyConfig = null;
class WeaponProf extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showWeaponPopup: false,
        }

        ProficiencyConfig = getWeaponProfConfig();
    }

    render() {
        return (
            <View>
                <View style={[styles.rowCentered, styles.spaceBetween]}>
                    <View style={[styles.rowCentered, {flex: .9}]}>
                        <Text style={styles.valueFontSize}>{this.props.rank}</Text>
                        <View style={localStyles.dividerWidth}/>
                        <View>
                            <Text style={styles.textHeader}>{t("Weapon Proficiency")}</Text>
                            {this.getMilestoneText()}
                        </View>
                    </View>
                    <View style={styles.rowCentered}>
                        <RoundControl onPress={this.decreaseWeaponProf} icon="minus" />
                        <View style={localStyles.dividerWidth}/>
                        <RoundControl onPress={this.increaseWeaponProf} icon="plus" />
                    </View>
                </View>
                <MilestoneDots milestones={this.props.weapon ? Object.keys(ProficiencyConfig[this.props.weapon].ranks).map(num => +num) : []} max={8} currentValue={this.props.rank}/>
                {(this.props.departing && this.props.currentShowdown) && <TouchableOpacity style={styles.rowCentered} onPress={this.toggleEarned}>
                    <Icon name={(this.props.earned === true) ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE}/>
                    <AppText style={styles.itemText}>{t("Proficiency point earned")}</AppText>
                </TouchableOpacity>}
                {this.getWeaponPicker()}
            </View>
        );
    }

    getWeaponPicker = () => {
        return <View>
            <Hr slim={true}/>
            <TouchableOpacity onPress={this.showWeaponPopup}>
                <View style={[styles.rowCentered, styles.spaceBetween]}>
                    <View style={{flex: .95}}>
                        {this.getAbilityText()}
                    </View>
                    <Icon name="chevron-down" color={this.props.colors.HIGHLIGHT} size={50}/>
                </View>
            </TouchableOpacity>
            <Popup title={t("Select Ability")} visible={this.state.showWeaponPopup} onDismissed={this.onPopupClosed}>
                {this.getPicker()}
            </Popup>
        </View>
    }

    showWeaponPopup = () => {
        this.setState({showWeaponPopup: true});
    }

    onPopupClosed = () => {
        this.setState({showWeaponPopup: false});
    }

    toggleEarned = () => {
        this.props.survivorActionWrapper("setWeaponProficiencyEarned", [this.props.settlementId, this.props.id, !this.props.earned]);
    }

    getAbilityText = () => {
        if(!this.props.weapon) {
            return <Text style={styles.italic}>{t("No weapon specialization selected")}</Text>
        }

        return <View>
            <Text style={styles.textHeader}>
                {ProficiencyConfig[this.props.weapon].title}
            </Text>
        </View>
    }

    getMilestoneText = () => {
        let ranks = ProficiencyConfig[this.props.weapon] ? Object.keys(ProficiencyConfig[this.props.weapon].ranks) : [];
        let nextRank = null;

        for(let i = 0; i < ranks.length; i++) {
            if(this.props.rank < ranks[i]) {
                nextRank = ranks[i];
                break;
            }
        }

        if(nextRank === null) {
            return <Text> </Text>;
        }

        return <Text><Text style={styles.bold}>{ProficiencyConfig[this.props.weapon].ranks[nextRank].title}</Text> at {nextRank}</Text>;
    }

    getPicker = () => {
        let weaponList = getUnselectedArray([this.props.weapon], this.props.allWeapons);
        let weapons = weaponList.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={ProficiencyConfig[name].title}
                    id={name}
                    onPress={this.changeWeaponProf}
                    buttonText={t("Select")}
                    confirm={false}
                />
            );
        });

        weapons.splice(0, 0,
            <ListItem
                key={-1}
                title={t("No Proficiency")}
                id={""}
                onPress={this.changeWeaponProf}
                buttonText={t("Select")}
                confirm={false}
            />
        );

        return <View style={{width: "90%", alignSelf: "center"}}>
            {weapons}
        </View>
    }

    changeWeaponProf = (name) => {
        this.setState({showWeaponPopup: false});
        this.props.survivorActionWrapper('changeWeaponProf', [this.props.settlementId, this.props.id, name]);
    }

    increaseWeaponProf = () => {
        this.props.survivorActionWrapper('increaseWeaponProf', [this.props.settlementId, this.props.id]);
    }

    decreaseWeaponProf = () => {
        this.props.survivorActionWrapper('decreaseWeaponProf', [this.props.settlementId, this.props.id]);
    }
}

function mapStateToProps(state, props) {
    return {
        weapon: state.survivorReducer.survivors[props.settlementId][props.id].weaponProficiency.weapon,
        rank: state.survivorReducer.survivors[props.settlementId][props.id].weaponProficiency.rank,
        earned: state.survivorReducer.survivors[props.settlementId][props.id].weaponProficiency.earned,
        currentShowdown: state.settlementReducer.settlements[props.settlementId].currentShowdown,
        departing: state.survivorReducer.survivors[props.settlementId][props.id].departing,
        allWeapons: sortIdArrayByConfigTitle(state.settlementReducer.settlements[props.settlementId].allProficiencies, getWeaponProfConfig()),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper, settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WeaponProf);

const localStyles = StyleSheet.create({
    dividerWidth: {
        width: 10
    }
});
