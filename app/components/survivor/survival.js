'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

import {
    View,
} from 'react-native';

import Text from '../shared/app-text';
import { t } from '../../helpers/intl';
import { IncrementorHidden } from '../shared/IncrementorHidden';
import { getColors } from '../../selectors/general';
import Badge from '../shared/Badge';
import styles from '../../style/styles';

class Survival extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showDetails: false,
        }
    }

    render() {
        return (
            <View style={[styles.centeredItems, styles.justifyCentered]}>
                <Text style={styles.title}>{t("Survival")}</Text>
                <IncrementorHidden
                    value={this.props.survival}
                    onIncrease={this.increaseSurvival}
                    onDecrease={this.decreaseSurvival}
                    onChange={this.setSurvival}
                />
                <View style={styles.row}>
                    <Badge
                        color={this.props.colors.RED}
                        tooltip={t("Cannot gain survival.") + " " + t("Long press to toggle.")}
                        active={this.props.canGainSurvival === false}
                        onLongPress={this.toggleCanGainSurvival}
                        icon="sk-no-survival-gain"
                        toast={true}
                    />
                    <Badge
                        color={this.props.colors.PURPLE}
                        tooltip={t("Cannot spend survival.") + " " + t("Long press to toggle.")}
                        active={this.props.canSpendSurvival === false}
                        onLongPress={this.toggleCanSpendSurvival}
                        icon="sk-no-survival"
                        toast={true}
                    />
                </View>
            </View>
        );
    }

    increaseSurvival = () => {
        if(this.props.survival < this.props.settlementLimit) {
            this.props.survivorActionWrapper('setSurvival', [this.props.settlementId, this.props.id, this.props.survival + 1]);
        }
    }

    decreaseSurvival = () => {
        if(this.props.survival > 0) {
            this.props.survivorActionWrapper('setSurvival', [this.props.settlementId, this.props.id, this.props.survival - 1]);
        }
    }

    // set value with text input (number pad)
    setSurvival = (val) => {
        if(+val > this.props.settlementLimit) {
            val = this.props.settlementLimit;
        }

        this.props.survivorActionWrapper('setSurvival', [this.props.settlementId, this.props.id, +val]);
    }

    toggleCanSpendSurvival = () => {
        this.props.survivorActionWrapper('toggleCanSpendSurvival', [this.props.settlementId, this.props.id]);
    }

    toggleCanGainSurvival = () => {
        this.props.survivorActionWrapper('setCanGainSurvival', [this.props.settlementId, this.props.id, !this.props.canGainSurvival]);
    }
}

function mapStateToProps(state, props) {
    return {
        settlementLimit: state.settlementReducer.settlements[props.settlementId].survivalLimit,
        survival: state.survivorReducer.survivors[props.settlementId][props.id].survival,
        canSpendSurvival: state.survivorReducer.survivors[props.settlementId][props.id].canSpendSurvival,
        canGainSurvival: state.survivorReducer.survivors[props.settlementId][props.id].canGainSurvival,
        survivalActions: state.survivorReducer.survivors[props.settlementId][props.id].survivalActions,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Survival);
