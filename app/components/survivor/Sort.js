'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import {setSurvivorSortOrder, setSurvivorSortAttribute} from '../../actions/settlement';
import { t } from '../../helpers/intl';
import Popup from '../shared/popup';
import ListItem from '../shared/ListItem';
import { sortObjectArrayByTitle } from '../../helpers/helpers';
import { getColors } from '../../selectors/general';
import RoundControl from '../shared/buttons/RoundControl';

class Sort extends Component {
    constructor(props) {
        super(props);

        let options = [
            {id: "acc", title: t("Accuracy")},
            {id: "color", title: t("Color")},
            {id: "courage", title: t("Courage")},
            {id: "eva", title: t("Evasion")},
            {id: "xp", title: t("Hunt XP")},
            {id: "insanity", title: t("Insanity")},
            {id: "lck", title: t("Luck")},
            {id: "mov", title: t("Movement")},
            {id: "name", title: t("Name")},
            {id: "numDisorders", title: t("Number of Disorders")},
            {id: "numFA", title: t("Number of Fighting Arts")},
            {id: "numSevereInjuries", title: t("Number of Severe Injuries")},
            {id: "spd", title: t("Speed")},
            {id: "str", title: t("Strength")},
            {id: "survival", title: t("Survival")},
            {id: "understanding", title: t("Understanding")},
            {id: "weaponProfName", title: t("Weapon Prof. Name")},
            {id: "weaponProfAmount", title: t("Weapon Prof. Amount")},
        ];

        if(this.props.campaign === "PotStars") {
            options.push({id: "weaponProfAmount", title: t("Constellation")});
            options.push({id: "weaponProfAmount", title: t("Number of Dragon Traits")});
        }

        options.sort(sortObjectArrayByTitle);

        this.state = {
            showPopup: false,
            options
        }
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    setSort = (attr) => {
        this.props.setSurvivorSortAttribute(this.props.settlementId, attr);
        this.setState({showPopup: false});
    }

    getOptions = () => {
        return this.state.options.map((item, i) => {
            return <ListItem
                key={i}
                id={item.id}
                title={item.title}
                onPress={this.setSort}
                buttonText={t("Choose")}
            />
        });
    }

    toggleSortDirection = () => {
        this.props.setSurvivorSortOrder(this.props.settlementId, this.props.sortOrder === "asc" ? "desc" : "asc");
    }

    getCurrentSortName = () => {
        for(let i = 0; i < this.state.options.length; i++) {
            if(this.state.options[i].id === this.props.sortAttribute) {
                return this.state.options[i].title;
            }
        }
    }

    render() {
        return(
            <View style={{marginLeft: 20, marginRight: 20, marginTop: 20, flexDirection: "row", alignItems: "center"}} >
                <ListItem
                    title={this.getCurrentSortName()}
                    onPress={this.showPopup}
                    buttonText={t("Change")}
                    subTitle={t("Sort Order")}
                >
                    <RoundControl
                        icon={this.props.sortOrder === "asc" ? "arrow-down" : "arrow-up"}
                        gradient={false}
                        color={this.props.colors.WHITE}
                        iconColor={this.props.colors.HIGHLIGHT}
                        onPress={this.toggleSortDirection}
                        style={{marginLeft: 25}}
                    />
                </ListItem>
                {this.state.showPopup === true && <Popup
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                    title={t("Select Sort")}
                >
                    {this.getOptions()}
                </Popup>}
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;

    return {
        settlementId,
        sortAttribute: state.settlementReducer.settlements[settlementId].survivorSortAttr,
        sortOrder: state.settlementReducer.settlements[settlementId].survivorSortOrder,
        campaign: state.settlementReducer.settlements[settlementId].campaign,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setSurvivorSortAttribute, setSurvivorSortOrder}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Sort);
