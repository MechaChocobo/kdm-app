'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';
import {getUnselectedArray, getAbilityConfig, sortIdArrayByConfigTitle} from '../../helpers/helpers';

import {
    View,
} from 'react-native';

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';

import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import ListItem from '../shared/ListItem';
import Popup from '../shared/popup';
import RoundControl from '../shared/buttons/RoundControl';
import AbilityText from '../shared/AbilityText';
import Hr from '../shared/Hr';

let AbilityConfig = null;

class Abilities extends Component {
    constructor(props) {
        super(props);
        this.state = { showAddList: false };
        AbilityConfig = getAbilityConfig();
    }

    render() {
        return (
            <View>
                <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                    <Text style={sharedStyles.title}>{t("Abilities and Impairments")}</Text>
                    <RoundControl icon="plus" onPress={this.showAddList}/>
                </View>
                {this.props.abilities.length > 0 && <Hr />}
                <View style={sharedStyles.contentWrapper}>
                    {this.renderAbilities(this.props.abilities)}
                    <Popup title={t("Select Ability")} visible={this.state.showAddList} onDismissed={this.onAddListClosed}>
                        {this.getPicker()}
                    </Popup>
            </View>
        </View>
        );
    }

    showAddList = () => {
        this.setState({showAddList: true});
    }

    onAddListClosed = () => {
        this.setState({showAddList: false});
    }

    getPicker = () => {
        let availableAbilities = getUnselectedArray(this.props.abilities, this.props.allAbilities, AbilityConfig);

        let items = availableAbilities.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={t(AbilityConfig[name].title)}
                    id={name}
                    onPress={this.addAbility}
                    buttonText={t("Add")}
                    confirm={false}
                />
            );
        });

        return <View style={{width: "90%", alignSelf: "center"}}>
            {items}
        </View>
    }

    addAbility = (name) => {
        this.props.survivorActionWrapper('addAbility', [this.props.settlementId, this.props.id, name]);
        this.setState({showAddList: false});
    }

    removeAbility = (name) => {
        this.props.survivorActionWrapper('removeAbility', [this.props.settlementId, this.props.id, name]);
    }

    renderAbilities = (abilities) => {
        return abilities.map((name, i) => {
            return <AbilityText
                key={i}
                title={t(AbilityConfig[name].title)}
                description={t(AbilityConfig[name].description)}
                id={name}
                onPress={this.removeAbility}
            />;
        });
    }
}

function mapStateToProps(state, props) {
    return {
        abilities: state.survivorReducer.survivors[props.settlementId][props.id].abilities,
        allAbilities: sortIdArrayByConfigTitle(state.settlementReducer.settlements[props.settlementId].allAbilities, getAbilityConfig()),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Abilities);
