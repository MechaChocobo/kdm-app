'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

import {
    View,
} from 'react-native';

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import { IncrementorHidden } from '../shared/IncrementorHidden';

class Affinities extends Component {
    render() {
        return <View>
            <Text style={sharedStyles.title}>{t("Affinities")}</Text>
            <View style={{flexDirection: "row", flex: 1}}>
                <View style={{flex: .33, alignItems: "center"}}>
                    <Text style={sharedStyles.itemText}>{t("Red")}</Text>
                    <IncrementorHidden
                        value={this.props.affinities.red}
                        onIncrease={this.increaseAffinity("red")}
                        onDecrease={this.decreaseAffinity("red")}
                    />
                </View>
                <View style={{flex: .33, alignItems: "center"}}>
                    <Text style={sharedStyles.itemText}>{t("Green")}</Text>
                    <IncrementorHidden
                        value={this.props.affinities.green}
                        onIncrease={this.increaseAffinity("green")}
                        onDecrease={this.decreaseAffinity("green")}
                    />
                </View>
                <View style={{flex: .33, alignItems: "center"}}>
                    <Text style={sharedStyles.itemText}>{t("Blue")}</Text>
                    <IncrementorHidden
                        value={this.props.affinities.blue}
                        onIncrease={this.increaseAffinity("blue")}
                        onDecrease={this.decreaseAffinity("blue")}
                    />
                </View>
            </View>
        </View>
    }

    hasAffinities = () => {
        if(
            this.props.affinities.green === 0 &&
            this.props.affinities.red === 0 &&
            this.props.affinities.blue === 0
        ) {
            return false;
        }

        return true;
    }

    increaseAffinity = (color) => {
        return () => {
            this.props.survivorActionWrapper("setAffinity", [this.props.settlementId, this.props.id, color, this.props.affinities[color] + 1]);
        }
    }

    decreaseAffinity = (color) => {
        return () => {
            // should be able to go negative
            this.props.survivorActionWrapper("setAffinity", [this.props.settlementId, this.props.id, color, this.props.affinities[color] - 1]);
        }
    }
}

function mapStateToProps(state, props) {
    return {
        affinities: state.survivorReducer.survivors[props.settlementId][props.id].affinities,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Affinities);
