'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {View, StyleSheet, TouchableOpacity} from 'react-native';

import Popup from '../shared/popup';
import styles from '../../style/styles';
import { t } from '../../helpers/intl';
import AppText from '../shared/app-text';
import { getColors } from '../../selectors/general';
import { survivorActionWrapper } from '../../actions/survivors';
import GeneralControlsPopup from './GeneralControlsPopup';
import { getColorCode } from '../../helpers/helpers';

class GeneralControls extends Component {
    pageContainer = null;

    constructor(props) {
        super(props);

        this.state = {
            showPopup: false,
        }
    }

    render() {
        let color = {};

        if(this.props.departingColor) {
            color = {backgroundColor: getColorCode(this.props.departingColor, this.props.colors)}
        }

        return(
            <View>
                <TouchableOpacity style={{flexShrink: 1, flexDirection: "row", marginLeft: 30, marginRight: 30, alignItems: "center", justifyContent: "space-between"}} onPress={this.showPopup}>
                    <Icon name={this.props.gender === "M" ? "gender-male" : "gender-female"} size={40} color={this.props.colors.TEXT}/>
                    <View style={[styles.centeredItems, {marginRight: 45}]}>
                        <AppText style={styles.pageHeader}>{(this.props.departing && this.props.showDepartingIndex && this.props.index !== undefined) ? "["+ (this.props.index + 1) +"] " : ""}{this.props.name}</AppText>
                        {this.getEpithet()}
                        <View style={styles.rowCentered}>
                            {this.getStatusText()}
                            {(this.props.isSotF || this.props.hasReroll === true) &&
                                <AppText style={styles.italic}>, {this.props.rerollUsed === false ? t("Reroll Available") : t("Reroll %b%Not%b% Available")}</AppText>
                            }
                        </View>
                    </View>
                    <Icon style={localStyles.editIcon} name="pencil" color={this.props.colors.HIGHLIGHT} size={25}/>
                </TouchableOpacity>
                <View style={[localStyles.colorRibbon, color]}></View>
                <View style={[styles.rowCentered, styles.spaceBetween, styles.width90, styles.centered]}>
                    <View style={[styles.rowCentered, {justifyContent: "center"}]}>
                        <AppText style={styles.itemText}>{t("Surv.")} </AppText>
                        <AppText style={styles.wrappedTextItemTitle}>{this.props.survival}</AppText>
                    </View>
                    <View style={[styles.rowCentered, {justifyContent: "center"}]}>
                        <AppText style={styles.itemText}>{t("Ins.")} </AppText>
                        <AppText style={styles.wrappedTextItemTitle}>{this.props.insanity.value}</AppText>
                    </View>
                    <View style={[styles.rowCentered, {justifyContent: "center"}]}>
                        <AppText style={styles.itemText}>{t("XP")} </AppText>
                        <AppText style={styles.wrappedTextItemTitle}>{this.props.xp}</AppText>
                    </View>
                    <View style={[styles.rowCentered, {justifyContent: "center"}]}>
                        <AppText style={styles.itemText}>{t("Cou.")} </AppText>
                        <AppText style={styles.wrappedTextItemTitle}>{this.props.courage}</AppText>
                    </View>
                    <View style={[styles.rowCentered, {justifyContent: "center"}]}>
                        <AppText style={styles.itemText}>{t("Und.")} </AppText>
                        <AppText style={styles.wrappedTextItemTitle}>{this.props.understanding}</AppText>
                    </View>
                </View>
                <Popup
                    visible={this.state.showPopup}
                    hideTitle={true}
                    onDismissed={this.onPopupDismissed}
                    onRequestClose={() => {}}
                >
                    {this.state.showPopup && <GeneralControlsPopup
                        settlementId={this.props.settlementId}
                        id={this.props.id}
                        onDelete={this.onPopupDismissed}
                        isGrid={this.props.isGrid}
                        sortAttribute={this.props.sortAttribute}
                        departing={this.props.departing}
                        onDead={this.handleDead}
                        closePopup={this.onPopupDismissed}
                    />}
                </Popup>
            </View>
        );
    }

    handleDead = () => {
        if(this.props.isGrid) {
            this.onPopupDismissed();
        }
    }

    getEpithet = () => {
        if(!this.props.epithet) {
            return null;
        }

        return <AppText style={styles.italic}>{this.props.epithet}</AppText>
    }

    getStatusText = () => {
        let text = t("Available to hunt");
        if(this.props.dead) {
            text = t("Dead");
        } else if(this.props.retired) {
            text = t("Retired");
        } else if(this.props.skipNextHunt) {
            text = t("Skipping next hunt");
        } else if(this.props.unavailable) {
            text = t("Unavailable");
        }

        return <AppText style={styles.italic}>{text}</AppText>;
    }

    setName = (name) => {
        // pass: settlementId, id, name
        this.props.survivorActionWrapper('changeName', [this.props.settlementId, this.props.id, name]);
    }

    onPopupDismissed = () => {
        this.setState({showPopup: false});
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }
}

function mapStateToProps(state, props) {
    return {
        isSotF: (state.settlementReducer.settlements[props.settlementId].principles.newLife == "survivalOfTheFittest"),
        gender: state.survivorReducer.survivors[props.settlementId][props.id].gender,
        skipNextHunt: state.survivorReducer.survivors[props.settlementId][props.id].skipNextHunt,
        retired: state.survivorReducer.survivors[props.settlementId][props.id].retired,
        dead: state.survivorReducer.survivors[props.settlementId][props.id].dead,
        unavailable: state.survivorReducer.survivors[props.settlementId][props.id].unavailable,
        hasReroll: state.survivorReducer.survivors[props.settlementId][props.id].hasReroll,
        rerollUsed: state.survivorReducer.survivors[props.settlementId][props.id].reroll,
        name: state.survivorReducer.survivors[props.settlementId][props.id].name,
        departingColor: state.survivorReducer.survivors[props.settlementId][props.id].departingColor,
        departing: state.survivorReducer.survivors[props.settlementId][props.id].departing,
        survival: state.survivorReducer.survivors[props.settlementId][props.id].survival,
        insanity: state.survivorReducer.survivors[props.settlementId][props.id].insanity,
        courage: state.survivorReducer.survivors[props.settlementId][props.id].courage,
        understanding: state.survivorReducer.survivors[props.settlementId][props.id].understanding,
        xp: state.survivorReducer.survivors[props.settlementId][props.id].xp,
        epithet: state.survivorReducer.survivors[props.settlementId][props.id].epithet !== undefined ? state.survivorReducer.survivors[props.settlementId][props.id].epithet : '',
        showDepartingIndex: state.settingsReducer.showDepartingIndex,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GeneralControls);

var localStyles = StyleSheet.create({
    popupWrapper: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: "space-between",
    },
    editIcon: {
        alignSelf: "flex-end",
        paddingRight: 10,
    },
    inputPadding: {
        marginLeft: 20
    },
    nameInput: {
        width: "90%",
        textAlign: "center",
        color: "white",
        fontSize: 20,
        padding: 0,
    },
    colorRibbon: {
        height: 8,
        marginTop: 2,
    }
});
