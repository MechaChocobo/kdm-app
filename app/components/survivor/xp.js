'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

import {
    View,
    StyleSheet,
} from 'react-native';

import Text from '../shared/app-text';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import MilestoneDots from '../shared/MilestoneDots';
import styles from '../../style/styles';
import RoundControl from '../shared/buttons/RoundControl';
import { padNumToDigits } from '../../helpers/helpers';

class XP extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAbilityPopup: false,
        }
    }

    render() {
        return (
            <View>
                <View style={[styles.rowCentered, styles.spaceBetween]}>
                    <View style={styles.rowCentered}>
                        <Text style={styles.valueFontSize}>{padNumToDigits(this.props.xp, 2)}</Text>
                        <View style={localStyles.dividerWidth}/>
                        <View>
                            <Text style={styles.textHeader}>{t("Hunt XP")}</Text>
                            {this.getMilestoneText()}
                        </View>
                    </View>
                    <View style={styles.rowCentered}>
                        <RoundControl onPress={this.decreaseXP} icon="minus" />
                        <View style={localStyles.dividerWidth}/>
                        <RoundControl onPress={this.increaseXP} icon="plus" />
                    </View>
                </View>
                <MilestoneDots milestones={[2, 6, 10, 15, 16]} max={16} currentValue={this.props.xp}/>
            </View>
        );
    }

    getMilestoneText = () => {
        if(this.props.xp === 16) {
            return <Text> </Text>
        }

        let milestone = "Age";
        let marker = 2;

        if(this.props.xp >= 2) {
            marker = 6;
        }

        if(this.props.xp >= 6) {
            marker = 10;
        }

        if(this.props.xp >= 10) {
            marker = 15;
        }

        if(this.props.xp >= 15) {
            marker = 16;
            milestone = "Retired";
        }

        return <Text><Text style={styles.bold}>{milestone}</Text> at {marker}</Text>;
    }

    increaseXP = () => {
        if(this.props.xp < 16) {
            this.props.survivorActionWrapper('setXp', [this.props.settlementId, this.props.id, this.props.xp + 1]);
        }
    }

    decreaseXP = () => {
        if(this.props.xp > 0) {
            this.props.survivorActionWrapper('setXp', [this.props.settlementId, this.props.id, this.props.xp - 1]);
        }
    }
}

function mapStateToProps(state, props) {
    return {
        xp: state.survivorReducer.survivors[props.settlementId][props.id].xp,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(XP);

const localStyles = StyleSheet.create({
    dividerWidth: {
        width: 10
    }
});
