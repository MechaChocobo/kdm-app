'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import {settlementActionWrapper} from '../../actions/settlement';
import sharedStyles from '../../style/styles';
import Text from '../shared/app-text';
import { t } from '../../helpers/intl';
import Icon from '../shared/Icon';
import { getColors } from '../../selectors/general';

class Gender extends Component {
    changeGender = (gender) => {
        return () => {
            this.props.survivorActionWrapper('changeGender', [this.props.settlementId, this.props.id, gender]);
        }
    }

    render() {
        return(
            <View style={[sharedStyles.container, sharedStyles.rowCentered]}>
                <Text style={[sharedStyles.headerFontSize, sharedStyles.flex50]}>{t("Gender")}</Text>
                <TouchableOpacity onPress={this.changeGender('M')} style={sharedStyles.flex25}>
                    <View style={sharedStyles.rowCentered}>
                    <Icon name={this.props.gender === 'M' ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.colors.TEXT}/>
                    <Text style={sharedStyles.itemText}>{t("Male")}</Text></View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.changeGender('F')} style={[sharedStyles.flex25, {paddingLeft: 10}]}>
                    <View style={sharedStyles.rowCentered}>
                    <Icon name={this.props.gender === 'F' ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.colors.TEXT}/>
                    <Text style={sharedStyles.itemText}>{t("Female")}</Text></View>
                </TouchableOpacity>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        gender: state.survivorReducer.survivors[props.settlementId][props.id].gender,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper, settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Gender);
