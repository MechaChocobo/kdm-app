'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';
import {IncrementorHidden} from '../shared/IncrementorHidden';

import {
    View,
    StyleSheet,
} from 'react-native';

import Text from '../shared/app-text';
import sharedStyles from '../../style/styles';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundControl from '../shared/buttons/RoundControl';

class Stats extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showDetails: false,
            editingStat: "",
        };
    }

    child = (stat, type) => {
        return () => {
            return <View style={[sharedStyles.centeredItems]}>
                <Text style={sharedStyles.aboveOverlay}>{t("Temp")}</Text>
                <View style={[sharedStyles.rowCentered, sharedStyles.aboveOverlay]}>
                    <RoundControl icon="minus" onPress={this.decreaseTempStat(type)} size={15}/>
                    <Text style={localStyles.textPadding} color={this.props.colors.HIGHLIGHT}>{stat}</Text>
                    <RoundControl icon="plus" onPress={this.increaseTempStat(type)} size={15}/>
                </View>
            </View>
        }
    }

    render() {
        let MOVnote = (this.props.tempMOV !== 0 && this.state.editingStat !== "MOV") ? <Text>{this.props.MOV} + {this.props.tempMOV}</Text> : null;
        let ACCnote = (this.props.tempACC !== 0 && this.state.editingStat !== "ACC") ? <Text>{this.props.ACC} + {this.props.tempACC}</Text> : null;
        let STRnote = (this.props.tempSTR !== 0 && this.state.editingStat !== "STR") ? <Text>{this.props.STR} + {this.props.tempSTR}</Text> : null;
        let EVAnote = (this.props.tempEVA !== 0 && this.state.editingStat !== "EVA") ? <Text>{this.props.EVA} + {this.props.tempEVA}</Text> : null;
        let LCKnote = (this.props.tempLCK !== 0 && this.state.editingStat !== "LCK") ? <Text>{this.props.LCK} + {this.props.tempLCK}</Text> : null;
        let SPDnote = (this.props.tempSPD !== 0 && this.state.editingStat !== "SPD") ? <Text>{this.props.SPD} + {this.props.tempSPD}</Text> : null;

        return (
            <View>
                <Text style={sharedStyles.title}>{t("Attributes")}</Text>
                <View style={localStyles.rowWrap}>
                    <View style={localStyles.columnWrap}>
                        <Text style={sharedStyles.bold}>{t("MOV")}</Text>
                        <IncrementorHidden
                            value={this.state.editingStat === "MOV" ? this.props.MOV : this.props.MOV + this.props.tempMOV}
                            onIncrease={this.increaseStat("MOV")}
                            onDecrease={this.decreaseStat("MOV")}
                            id="MOV"
                            onEditStart={this.onStatEditStart}
                            onEditEnd={this.onStatEditEnd}
                            child={this.child(this.props.tempMOV, "MOV")}
                        ></IncrementorHidden>
                        {MOVnote}
                    </View>
                    <View style={localStyles.columnWrap}>
                        <Text style={sharedStyles.bold}>{t("ACC")}</Text>
                        <IncrementorHidden
                            value={this.state.editingStat === "ACC" ? this.props.ACC : this.props.ACC + this.props.tempACC}
                            onIncrease={this.increaseStat("ACC")}
                            onDecrease={this.decreaseStat("ACC")}
                            id="ACC"
                            onEditStart={this.onStatEditStart}
                            onEditEnd={this.onStatEditEnd}
                            child={this.child(this.props.tempACC, "ACC")}
                        />
                        {ACCnote}
                    </View>
                    <View style={localStyles.columnWrap}>
                        <Text style={sharedStyles.bold}>{t("STR")}</Text>
                        <IncrementorHidden
                            value={this.state.editingStat === "STR" ? this.props.STR : this.props.STR + this.props.tempSTR}
                            onIncrease={this.increaseStat("STR")}
                            onDecrease={this.decreaseStat("STR")}
                            id="STR"
                            onEditStart={this.onStatEditStart}
                            onEditEnd={this.onStatEditEnd}
                            child={this.child(this.props.tempSTR, "STR")}
                        />
                        {STRnote}
                    </View>
                    <View style={localStyles.columnWrap}>
                        <Text style={sharedStyles.bold}>{t("EVA")}</Text>
                        <IncrementorHidden
                            value={this.state.editingStat === "EVA" ? this.props.EVA : this.props.EVA + this.props.tempEVA}
                            onIncrease={this.increaseStat("EVA")}
                            onDecrease={this.decreaseStat("EVA")}
                            id="EVA"
                            onEditStart={this.onStatEditStart}
                            onEditEnd={this.onStatEditEnd}
                            child={this.child(this.props.tempEVA, "EVA")}
                        />
                        {EVAnote}
                    </View>
                    <View style={localStyles.columnWrap}>
                        <Text style={sharedStyles.bold}>{t("LCK")}</Text>
                        <IncrementorHidden
                            value={this.state.editingStat === "LCK" ? this.props.LCK : this.props.LCK + this.props.tempLCK}
                            onIncrease={this.increaseStat("LCK")}
                            onDecrease={this.decreaseStat("LCK")}
                            id="LCK"
                            onEditStart={this.onStatEditStart}
                            onEditEnd={this.onStatEditEnd}
                            child={this.child(this.props.tempLCK, "LCK")}
                        />
                        {LCKnote}
                    </View>
                    <View style={localStyles.columnWrap}>
                        <Text style={sharedStyles.bold}>{t("SPD")}</Text>
                        <IncrementorHidden
                            value={this.state.editingStat === "SPD" ? this.props.SPD : this.props.SPD + this.props.tempSPD}
                            onIncrease={this.increaseStat("SPD")}
                            onDecrease={this.decreaseStat("SPD")}
                            id="SPD"
                            onEditStart={this.onStatEditStart}
                            onEditEnd={this.onStatEditEnd}
                            child={this.child(this.props.tempSPD, "SPD")}
                        />
                        {SPDnote}
                    </View>
                </View>
            </View>
        );
    }

    onStatEditStart = (stat) => {
        this.setState({editingStat: stat});
    }

    onStatEditEnd = (stat) => {
        this.setState({editingStat: ""});
    }

    // increases/decreases the given stat, passed in the form of "MOV", "ACC", "STR", "EVA", "LCK", "SPD"
    increaseStat = (stat) => {
        return () => {
            // pass: settlementId, id, stat, val
            this.props.survivorActionWrapper('setStat', [this.props.settlementId, this.props.id, stat, this.props[stat] + 1]);
        }
    }

    decreaseStat = (stat) => {
        return () => {
            // pass: settlementId, id, stat, val
            this.props.survivorActionWrapper('setStat', [this.props.settlementId, this.props.id, stat, this.props[stat] - 1]);
        }
    }

    // increases/decreases the given temporary stat, passed in the form of "MOV", "ACC", "STR", "EVA", "LCK", "SPD"
    increaseTempStat = (stat) => {
        return () => {
            // pass: settlementId, id, stat, val
            // stored here as this.props.tempMOV &etc. in reducer as tempAttributes.MOV &etc. so concat "temp" here for simpler access later
            this.props.survivorActionWrapper('setTempStat', [this.props.settlementId, this.props.id, stat, this.props["temp"+stat] + 1]);
        }
    }

    decreaseTempStat = (stat) => {
        return () => {
            // pass: settlementId, id, stat, val
            // stored here as this.props.tempMOV &etc. in reducer as tempAttributes.MOV &etc. so concat "temp" here for simpler access later
            this.props.survivorActionWrapper('setTempStat', [this.props.settlementId, this.props.id, stat, this.props["temp"+stat] - 1]);
        }
    }
}

function mapStateToProps(state, props) {
    return {
        MOV: state.survivorReducer.survivors[props.settlementId][props.id].attributes.MOV,
        ACC: state.survivorReducer.survivors[props.settlementId][props.id].attributes.ACC,
        STR: state.survivorReducer.survivors[props.settlementId][props.id].attributes.STR,
        EVA: state.survivorReducer.survivors[props.settlementId][props.id].attributes.EVA,
        LCK: state.survivorReducer.survivors[props.settlementId][props.id].attributes.LCK,
        SPD: state.survivorReducer.survivors[props.settlementId][props.id].attributes.SPD,
        tempMOV: state.survivorReducer.survivors[props.settlementId][props.id].tempAttributes.MOV,
        tempACC: state.survivorReducer.survivors[props.settlementId][props.id].tempAttributes.ACC,
        tempSTR: state.survivorReducer.survivors[props.settlementId][props.id].tempAttributes.STR,
        tempEVA: state.survivorReducer.survivors[props.settlementId][props.id].tempAttributes.EVA,
        tempLCK: state.survivorReducer.survivors[props.settlementId][props.id].tempAttributes.LCK,
        tempSPD: state.survivorReducer.survivors[props.settlementId][props.id].tempAttributes.SPD,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Stats);

const localStyles = StyleSheet.create({
    textPadding: {
        paddingLeft: 5,
        paddingRight: 5,
    },
    columnWrap: {
        flex: .1666666,
        alignItems: "center",
    },
    rowWrap: {
        flexDirection: "row",
        minHeight: 100,
        flex: 1,
    }
});
