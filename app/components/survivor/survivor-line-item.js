'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    View,
    TouchableOpacity,
} from 'react-native';

import Toast from 'react-native-simple-toast';
import Swipeout from 'react-native-swipeout';

import Text from '../shared/app-text';
import sharedStyles from '../../style/styles';
import { getColorCode, capitalizeFirst, getAbilityConfig, getDisorderConfig, getWeaponProfConfig, getFightingArtConfig, getSevereInjuryConfig, getNumDragonTraits, padNumToDigits, getWidthFromPercentage, getHeightFromPercentage, getGearConfig } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import Icon from '../shared/Icon';
import GView from '../shared/GView';
import Ribbon from '../shared/Ribbon';
import Badge from '../shared/Badge';
import RoundControl from '../shared/buttons/RoundControl';
import StripedRibbon from '../shared/StripedRibbon';

let AbilityConfig = null;
let DisorderConfig = null;
let WeaponProfConfig = null;
let FAConfig = null;
let SevereInjuries = null;
let GearConfig = null;

const COLOR_BANNER_HEIGHT = 8;
export const INDEX_MOD = 2; // number of non-survivor elements added before the survivor list
const TEXT_LINE_HEIGHT = 20;

const BASE_CARD_HEIGHT = 161; // no summary lines

export function getCardHeight(numLines) {
    return BASE_CARD_HEIGHT + (numLines * TEXT_LINE_HEIGHT) + getHeightFromPercentage(.03);
}

export class SurvivorLineItem extends PureComponent {
    constructor(props) {
        super(props);
        AbilityConfig = getAbilityConfig();
        DisorderConfig = getDisorderConfig();
        WeaponProfConfig = getWeaponProfConfig();
        FAConfig = getFightingArtConfig();
        SevereInjuries = getSevereInjuryConfig();
        GearConfig = getGearConfig();

        this.state = {
            tipParent: null,
        }
    }

    onLayout = (e) => {
        console.warn(e.nativeEvent.layout);
    }

    renderNotes = (colors) => {
        if(this.props.survivor.notes === "") {
            return;
        } else {
            return <Text style={sharedStyles.itemText} color={colors.WHITE}>
                <Text style={sharedStyles.bold} color={colors.WHITE}>{t("Notes")}:</Text> {this.props.survivor.notes}
            </Text>
        }
    }

    renderAbilities = (colors) => {
        if(this.props.survivor.abilities.length == 0) {
            return;
        } else {
            let abilities = this.getAbilityTitles();

            return (
                <Text style={sharedStyles.itemText} color={colors.WHITE}>
                    <Text style={sharedStyles.bold} color={colors.WHITE}>{t("Abilities and Impairments")}:</Text> {abilities.join(', ')}
                </Text>
            );
        }
    }

    getAbilityTitles = () => {
        return this.props.survivor.abilities.map((ability) => {
            return (AbilityConfig[ability].title);
        });
    }

    renderDisorders = (colors) => {
        if(this.props.survivor.disorders.length == 0) {
            return;
        } else {
                let disorders = this.getDisorderTitles();

                return (
                    <Text style={sharedStyles.itemText} color={colors.WHITE}>
                        <Text style={sharedStyles.bold} color={colors.WHITE}>{t("Disorders")}:</Text> {disorders.join(', ')}
                    </Text>
                );
        }
    }

    getDisorderTitles = () => {
        return this.props.survivor.disorders.map((disorder) => {
            return (DisorderConfig[disorder].title);
        });
    }

    renderFightingArts = (colors) => {
        if(this.props.survivor.fightingArts.length == 0) {
            return;
        } else {
            let arts = this.getFightingArtTitles();

            return (
                <Text style={sharedStyles.itemText} color={colors.WHITE}>
                    <Text style={sharedStyles.bold} color={colors.WHITE}>{t("Fighting Arts")}:</Text> {arts.join(', ')}
                </Text>
            );
        }
    }

    getFightingArtTitles = () => {
        return this.props.survivor.fightingArts.map((art) => {
            return (FAConfig[art].title);
        });
    }

    renderSevereInjuries = (colors) => {
        if(this.props.survivor.severeInjuries.length == 0) {
            return;
        } else {
            let injuries = this.getSevereInjuryTitles();

            return (
                <Text style={sharedStyles.itemText} color={colors.WHITE}>
                    <Text style={sharedStyles.bold} color={colors.WHITE}>{t("Severe Injuries")}:</Text> {injuries.join(', ')}
                </Text>
            );
        }
    }

    getSevereInjuryTitles = () => {
        return this.props.survivor.severeInjuries.map((injury) => {
            return (SevereInjuries[injury].title);
        });
    }

    renderWeaponProficiency = (colors) => {
        if(this.props.survivor.weaponProficiency.weapon === "") {
            return(null);
        } else {
            return(
                <Text style={sharedStyles.itemText} color={colors.WHITE}>
                    <Text style={sharedStyles.bold} color={colors.WHITE}>{t(WeaponProfConfig[this.props.survivor.weaponProficiency.weapon].title)}:</Text> {this.props.survivor.weaponProficiency.rank}
                </Text>
            )
        }
    }

    renderAffinities = (colors) => {
        let affinities = this.getAffinityCounts();

        if(affinities.length === 0) {
            return null;
        }

        return <Text style={sharedStyles.itemText} color={colors.WHITE}>
            <Text style={sharedStyles.bold} color={colors.WHITE}>{t("Affinities")}:</Text> {affinities.join(', ')}
        </Text>
    }

    getAffinityCounts = () => {
        let affinities = [];

        for(let color in this.props.survivor.affinities) {
            if(this.props.survivor.affinities[color] !== 0) {
                affinities.push(this.props.survivor.affinities[color] + ' ' + t(capitalizeFirst(color)));
            }
        }

        return affinities;
    }

    getCursedGear = () => {
        let gear = [];

        for(let i = 0; i < this.props.survivor.cursedItems.length; i++) {
            gear.push(GearConfig[this.props.survivor.cursedItems[i].location][this.props.survivor.cursedItems[i].item].title);
        }

        return gear;
    }

    renderArmor = (colors) => {
        if(!this.props.survivor.departing === true) {
            return null;
        }

        return (
            <View>
                <View style={[sharedStyles.rowCentered, {justifyContent: "space-evenly"}]}>
                    <View style={sharedStyles.centeredItems}>
                        <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Head")}</Text>
                        <Text style={styles.attrValue} color={this.props.survivor.armor.head.heavy ? "black" : "white" }>{padNumToDigits(this.props.survivor.armor.head.value)}</Text>
                    </View>
                    <View style={sharedStyles.centeredItems}>
                        <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Arms")}</Text>
                        <Text style={styles.attrValue} color={this.props.survivor.armor.arms.heavy ? "black" : (this.props.survivor.armor.arms.light ? "gray" : "white") }>{padNumToDigits(this.props.survivor.armor.arms.value)}</Text>
                    </View>
                    <View style={sharedStyles.centeredItems}>
                        <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Body")}</Text>
                        <Text style={styles.attrValue} color={this.props.survivor.armor.body.heavy ? "black" : (this.props.survivor.armor.body.light ? "gray" : "white") }>{padNumToDigits(this.props.survivor.armor.body.value)}</Text>
                    </View>
                    <View style={sharedStyles.centeredItems}>
                        <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Waist")}</Text>
                        <Text style={styles.attrValue} color={this.props.survivor.armor.waist.heavy ? "black" : (this.props.survivor.armor.waist.light ? "gray" : "white") }>{padNumToDigits(this.props.survivor.armor.waist.value)}</Text>
                    </View>
                    <View style={sharedStyles.centeredItems}>
                        <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Legs")}</Text>
                        <Text style={styles.attrValue} color={this.props.survivor.armor.legs.heavy ? "black" : (this.props.survivor.armor.legs.light ? "gray" : "white") }>{padNumToDigits(this.props.survivor.armor.legs.value)}</Text>
                    </View>
                </View>
                <View style={[sharedStyles.hr, sharedStyles.disabled, {borderColor: "white", width: "100%"}]}/>
            </View>
        )
    }

    renderDragonTraits = (colors) => {
        if(this.props.campaign !== "PotStars") {
            return null;
        }

        let constellation = null;
        if(this.props.survivor.constellation !== "") {
            constellation = <Text color={colors.WHITE} style={sharedStyles.itemText}>{t("Constellation")}: <Text style={{fontWeight: "bold"}} color={colors.WHITE}>{this.props.survivor.constellation}</Text></Text>
        }

        return <View>
            {constellation}
            <Text color={colors.WHITE} style={sharedStyles.itemText}>{t("Number of Dragon Traits")}: <Text style={{fontWeight: "bold"}} color={colors.WHITE}>{getNumDragonTraits(this.props.survivor.constellationMap)}</Text></Text>
        </View>
    }

    renderSurvivorSummary = (colors) => {
        let weapon = null;
        let items = [];
        if(this.props.survivor.weaponProficiency.weapon !== "") {
            weapon = <Text style={sharedStyles.bold} color={colors.WHITE}>{t(WeaponProfConfig[this.props.survivor.weaponProficiency.weapon].title)}:</Text>
            items.push(this.props.survivor.weaponProficiency.rank);
        }

        items.push(...this.getFightingArtTitles());
        items.push(...this.getDisorderTitles());
        items.push(...this.getAbilityTitles());
        items.push(...this.getSevereInjuryTitles());
        items.push(...this.getAffinityCounts());
        items.push(...this.getCursedGear());

        if(this.props.survivor.notes !== "") {
            items.push(this.props.survivor.notes);
        }

        return <Text style={sharedStyles.itemText} color={colors.WHITE} numberOfLines={this.props.numLinesSurvivorCard ? this.props.numLinesSurvivorCard : 2}>
            {weapon} {items.join(', ')}
        </Text>
    }

    isSavior = () => {
        if(
            this.props.survivor.abilities.indexOf("lucernae") !== -1 ||
            this.props.survivor.abilities.indexOf("caratosis") !== -1 ||
            this.props.survivor.abilities.indexOf("dormenatus") !== -1
        ) {
            return true;
        }

        return false;
    }

    isWarriorOfTheSun = () => {
        if(
            this.props.survivor.abilities.indexOf("reflection") !== -1 ||
            this.props.survivor.abilities.indexOf("refraction") !== -1
        ) {
            return true;
        }

        return false;
    }

    getSaviorColor = (colors) => {
        let color = "";

        if(this.props.survivor.abilities.indexOf("lucernae") !== -1) {
            color = colors.BLUE;
        }

        if(this.props.survivor.abilities.indexOf("caratosis") !== -1) {
            color = colors.RED;
        }

        if(this.props.survivor.abilities.indexOf("dormenatus") !== -1) {
            color = colors.GREENDARK;
        }

        return color;
    }

    setTipParent = (ref) => {
        this.setState({tipParent: ref});
    }

    getBadges = (colors) => {
        let badges = [];

        if(this.props.onPinned) {
            badges.push(
                <Badge
                    key="badge-pin"
                    color={colors.GOLD}
                    tooltip={t("Pinned")}
                    tipParent={this.state.tipParent}
                    active={this.props.pinned == true}
                    icon="pin"
                    onLongPress={this.togglePin}
                    iconSize={20}
                />
            );
        }

        if(this.props.isSotf || this.props.survivor.hasReroll) {
            badges.push(<Badge
                key="badge-reroll"
                color={colors.TEAL}
                tooltip={t("Lifetime reroll")}
                tipParent={this.state.tipParent}
                active={this.props.survivor.reroll === false}
                icon="sk-reroll"
                onLongPress={this.toggleReroll}
            />);
        }

        if(this.props.campaign === "PotStars") {
            let tip = t("No constellation");

            if(this.props.survivor.constellation !== "") {
                tip = t("Constellation %name%", {name: this.props.survivor.constellation});
            }

            badges.push(<Badge
                key="badge-constellation"
                color={colors.PURPLE}
                tooltip={tip}
                tipParent={this.state.tipParent}
                active={this.props.survivor.constellation !== ""}
                icon="sk-constellation"
            />);
        }

        if(this.props.campaign === "PotSun") {
            if(this.props.survivor.abilities.indexOf("purified") !== -1) {
                badges.push(<Badge
                    key="badge-purified"
                    color={colors.PURPLE}
                    tooltip={t("Survivor is purified")}
                    tipParent={this.state.tipParent}
                    icon="sk-purified"
                />);
            }

        }

        badges.push(<Badge
            key="badge-skip-hunt"
            color={colors.GREENDARK}
            tooltip={t("Survivor must skip the next hunt")}
            tipParent={this.state.tipParent}
            active={this.props.survivor.skipNextHunt !== false}
            icon="sk-skip-hunt"
            onLongPress={this.toggleSkipNextHuntBadge}
        />);

        if(this.props.survivor.retired !== false) {
            badges.push(<Badge
                key="badge-retired"
                color={colors.BLUE}
                tooltip={t("Survivor is retired")}
                tipParent={this.state.tipParent}
                active={this.props.survivor.retired !== false}
                icon="sk-retired"
            />);
        }

        if(this.props.survivor.canSpendSurvival === false) {
            badges.push(<Badge
                key="badge-spend-survival"
                color={colors.PURPLE}
                tooltip={t("Survivor cannot spend survival")}
                tipParent={this.state.tipParent}
                active={this.props.survivor.canSpendSurvival === false}
                icon="sk-no-survival"
            />);
        }

        if(this.props.survivor.canGainSurvival === false) {
            badges.push(<Badge
                key="badge-gain-survival"
                color={colors.RED}
                tooltip={t("Survivor cannot gain survival")}
                tipParent={this.state.tipParent}
                active={this.props.survivor.canGainSurvival === false}
                icon="sk-no-survival-gain"
            />);
        }

        if(this.props.survivor.canUseFightingArts === false) {
            badges.push(<Badge
                key="badge-fighting-arts"
                color={colors.RED}
                tooltip={t("Survivor cannot use fighting arts")}
                tipParent={this.state.tipParent}
                active={this.props.survivor.canUseFightingArts === false}
                icon="sk-no-fa"
            />);
        }

        if(this.props.survivor.unavailable === true) {
            badges.push(<Badge
                key="badge-unavailable"
                color={colors.BROWN}
                tooltip={t("Survivor is unavailable")}
                tipParent={this.state.tipParent}
                active={this.props.survivor.unavailable === true}
                icon="sk-unavailable"
            />);
        }

        return badges;
    }

    onShowMore = () => {
        this.props.onShowMore(this.props.index - INDEX_MOD);
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) => {
                    let departText = this.props.survivor.departing !== false ? t("Leave Departing") : t("Join Departing");
                    let component = <View style={sharedStyles.swipeoutButton}><Text style={sharedStyles.itemText} color={colors.WHITE}>{departText}</Text></View>;
                    let buttons = [
                        {component, onPress:this.toggleDeparting, backgroundColor:colors.HIGHLIGHT}
                    ];

                    if(this.props.survivor.skipNextHunt) {
                        component = <View style={sharedStyles.swipeoutButton}>
                                <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Skipping Next Hunt")}</Text>
                            </View>;
                        buttons = [
                            {component, onPress:this.devNull, backgroundColor:colors.HIGHLIGHT}
                        ];
                    }

                    if(this.props.survivor.dead === true) {
                        buttons = [];
                    }

                    let containerStyles = {
                        width: getWidthFromPercentage(.9),
                        marginLeft: getWidthFromPercentage(.05),
                        marginRight: getWidthFromPercentage(.05),
                        marginTop: getHeightFromPercentage(.03),
                    };

                    if(this.props.detail === true) {
                        return <TouchableOpacity onPress={this.devNull}>
                            <View style={containerStyles}>
                                {this.getSurvivorCard(colors)}
                            </View>
                        </TouchableOpacity>
                    } else {
                        return <View style={containerStyles}>
                            <Swipeout autoClose={true} right={buttons} buttonWidth={80} backgroundColor="transparent">
                                <TouchableOpacity onPress={this.survivorPressed}>
                                    {this.getSurvivorCard(colors)}
                                </TouchableOpacity>
                            </Swipeout>
                        </View>
                    }
                }}
            </ColorContext.Consumer>
        );
    }

    getEpithet = () => {
        if(!this.props.survivor.epithet && !(this.props.survivor.dead && this.props.survivor.causeOfDeath)) {
            return null;
        }

        let text = [];
        if(this.props.survivor.epithet) {
            text.push(this.props.survivor.epithet);
        }

        if(this.props.survivor.dead && this.props.survivor.causeOfDeath) {
            text.push(this.props.survivor.causeOfDeath);
        }

        return <Text style={sharedStyles.italic} numberOfLines={1}> {text.join(', ')}</Text>
    }

    getSurvivorCard = (colors) => {
        let isReturning = ((this.props.survivor.returning === true || this.props.survivor.returning === this.props.ly) && !this.props.survivor.dead);
        let colorBanner = this.props.survivor.departingColor ? <View style={{height: COLOR_BANNER_HEIGHT, backgroundColor: getColorCode(this.props.survivor.departingColor, colors)}}/> : null;
        const summaryHeight = this.props.numLinesSurvivorCard ? this.props.numLinesSurvivorCard * TEXT_LINE_HEIGHT : 2 * TEXT_LINE_HEIGHT;

        return <GView ref={this.setTipParent} style={sharedStyles.roundedContainer}>
            {/* <View onLayout={this.onLayout}> */}
            <View style={{height: COLOR_BANNER_HEIGHT}}>
                {colorBanner}
            </View>
            <View>
                <View style={sharedStyles.row}>
                    <View style={sharedStyles.rowCentered}>
                        <Icon name={this.props.survivor.gender === "M" ? "gender-male" : "gender-female"} size={25}/>
                        <View style={[sharedStyles.row, {alignItems: "baseline"}]}>
                            <Text style={sharedStyles.largeValue} color={colors.WHITE} numberOfLines={1}>{this.props.survivor.name}</Text>
                            {this.getEpithet()}
                        </View>
                    </View>
                    {this.isSavior() && <View style={{position: "absolute", right: 0, bottom: 8}}>
                        <Ribbon text={t("Savior")} backgroundColor={this.getSaviorColor(colors)}/>
                    </View>}
                    {this.isWarriorOfTheSun() && <View style={{position: "absolute", right: 0, bottom: 8}}>
                        <StripedRibbon text={t("Warrior")}/>
                    </View>}
                </View>
                <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                    <View style={[sharedStyles.rowCentered, {minHeight: 35, marginLeft: 5}]}>
                        {this.getBadges(colors)}
                    </View>
                    <View style={sharedStyles.rowCentered}>
                        {isReturning && <Badge
                            color={colors.GREENDARK}
                            tooltip={t("Returning Survivor")}
                            tipParent={this.state.tipParent}
                            active={isReturning}
                            icon="sk-returning"
                            />}
                        {this.props.survivor.dead === false && <Badge
                            color={colors.BLUE}
                            tooltip={t("Departing")}
                            tipParent={this.state.tipParent}
                            active={this.props.survivor.departing !== false}
                            icon="sk-departing"
                            onLongPress={this.toggleDepartingBadge}
                            invertPress={this.props.tapToDepart}
                        />}
                        {this.props.onShowMore && <RoundControl
                            style={{marginRight: 10}}
                            onPress={this.onShowMore}
                            gradient={false}
                            icon="arrow-expand-all"
                            size={25}
                            color="white"
                            iconColor={colors.HIGHLIGHT}
                        />}
                        {this.props.detail === true && <RoundControl
                            style={{marginRight: 10}}
                            onPress={this.detailSurvivorPressed}
                            gradient={false}
                            icon="pencil"
                            size={25}
                            color="white"
                            iconColor={colors.HIGHLIGHT}
                        />}
                    </View>
                </View>
            </View>
            <View style={[sharedStyles.hrSlim, sharedStyles.disabled, {borderColor: "white", width: "100%"}]}/>
            <View style={sharedStyles.rowCentered}>
                <View style={[styles.statsRow, sharedStyles.rowCentered, {justifyContent: "center"}]}>
                    <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Surv.")} </Text>
                    <Text style={sharedStyles.wrappedTextItemTitle} color={colors.WHITE}>{this.props.survivor.survival}</Text>
                </View>
                <View style={[styles.statsRow, sharedStyles.rowCentered, {justifyContent: "center"}]}>
                    <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Ins.")} </Text>
                    <Text style={sharedStyles.wrappedTextItemTitle} color={colors.WHITE}>{this.props.survivor.insanity.value}</Text>
                </View>
                <View style={[styles.statsRow, sharedStyles.rowCentered, {justifyContent: "center"}]}>
                    <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("XP")} </Text>
                    <Text style={sharedStyles.wrappedTextItemTitle} color={colors.WHITE}>{this.props.survivor.xp}</Text>
                </View>
                <View style={[styles.statsRow, sharedStyles.rowCentered, {justifyContent: "center"}]}>
                    <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Cou.")} </Text>
                    <Text style={sharedStyles.wrappedTextItemTitle} color={colors.WHITE}>{this.props.survivor.courage}</Text>
                </View>
                <View style={[styles.statsRow, sharedStyles.rowCentered, {justifyContent: "center"}]}>
                    <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("Und.")} </Text>
                    <Text style={sharedStyles.wrappedTextItemTitle} color={colors.WHITE}>{this.props.survivor.understanding}</Text>
                </View>
            </View>
            <View style={[sharedStyles.hrSlim, sharedStyles.disabled, {borderColor: "white", width: "100%"}]}/>
            <View style={[sharedStyles.rowCentered, {justifyContent: "space-evenly"}]}>
                <View style={styles.attrRow}>
                    <Text style={sharedStyles.itemText} color={colors.WHITE}>{t("MOV")}</Text>
                    <Text color={colors.WHITE} style={[styles.attrValue, (this.props.survivor.attributes.MOV + this.props.survivor.tempAttributes.MOV === 0 ? sharedStyles.lightDisabled : {})]}>{padNumToDigits(this.props.survivor.attributes.MOV + this.props.survivor.tempAttributes.MOV, 2)}</Text>
                </View>
                <View style={styles.attrRow}>
                    <Text color={colors.WHITE} style={sharedStyles.itemText}>{t("ACC")}</Text>
                    <Text color={colors.WHITE} style={[styles.attrValue, (this.props.survivor.attributes.ACC + this.props.survivor.tempAttributes.ACC === 0 ? sharedStyles.lightDisabled : {})]}>{padNumToDigits(this.props.survivor.attributes.ACC + this.props.survivor.tempAttributes.ACC, 2)}</Text>
                </View>
                <View style={styles.attrRow}>
                    <Text color={colors.WHITE} style={sharedStyles.itemText}>{t("STR")}</Text>
                    <Text color={colors.WHITE} style={[styles.attrValue, (this.props.survivor.attributes.STR + this.props.survivor.tempAttributes.STR === 0 ? sharedStyles.lightDisabled : {})]}>{padNumToDigits(this.props.survivor.attributes.STR + this.props.survivor.tempAttributes.STR, 2)}</Text>
                </View>
                <View style={styles.attrRow}>
                    <Text color={colors.WHITE} style={sharedStyles.itemText}>{t("EVA")}</Text>
                    <Text color={colors.WHITE} style={[styles.attrValue, (this.props.survivor.attributes.EVA + this.props.survivor.tempAttributes.EVA === 0 ? sharedStyles.lightDisabled : {})]}>{padNumToDigits(this.props.survivor.attributes.EVA + this.props.survivor.tempAttributes.EVA, 2)}</Text>
                </View>
                <View style={styles.attrRow}>
                    <Text color={colors.WHITE} style={sharedStyles.itemText}>{t("LCK")}</Text>
                    <Text color={colors.WHITE} style={[styles.attrValue, (this.props.survivor.attributes.LCK + this.props.survivor.tempAttributes.LCK === 0 ? sharedStyles.lightDisabled : {})]}>{padNumToDigits(this.props.survivor.attributes.LCK + this.props.survivor.tempAttributes.LCK, 2)}</Text>
                </View>
                <View style={styles.attrRow}>
                    <Text color={colors.WHITE} style={sharedStyles.itemText}>{t("SPD")}</Text>
                    <Text color={colors.WHITE} style={[styles.attrValue, (this.props.survivor.attributes.SPD + this.props.survivor.tempAttributes.SPD === 0 ? sharedStyles.lightDisabled : {})]}>{padNumToDigits(this.props.survivor.attributes.SPD + this.props.survivor.tempAttributes.SPD, 2)}</Text>
                </View>
            </View>
            {this.props.detail === true && <View>
                <View style={[sharedStyles.hrSlim, sharedStyles.disabled, {borderColor: "white", width: "100%"}]}/>
                {this.renderArmor(colors)}
            </View>}
            {this.props.detail === false && <View style={[sharedStyles.hrSlim, sharedStyles.disabled, {borderColor: "white", width: "100%", marginBottom: 0}]}/>}
            {this.props.detail === false && <View style={{marginLeft: 5, height: summaryHeight}}>{this.renderSurvivorSummary(colors)}</View>}
            {this.props.detail === true && <View>
                <View style={{marginLeft:5}}>
                    {this.renderWeaponProficiency(colors)}
                    {this.renderDragonTraits(colors)}
                    {this.renderFightingArts(colors)}
                    {this.renderDisorders(colors)}
                    {this.renderAbilities(colors)}
                    {this.renderSevereInjuries(colors)}
                    {this.renderNotes(colors)}
                    {this.renderAffinities(colors)}
                </View>
            </View>}
            {/* </View> */}
        </GView>
    }

    toggleDeparting = () => {
        this.props.onDepart(this.props.survivor.id);
    }

    toggleDepartingBadge = () => {
        let text = this.props.survivor.departing ?
            t("%name% no longer departing", {name: this.props.survivor.name}) :
            t("%name% set as departing", {name: this.props.survivor.name});
        let doToggle = true;

        if(this.props.survivor.departing === false && this.props.survivor.skipNextHunt === true) {
            text = t("%name% is skipping the next hunt", {name: this.props.survivor.name});
            doToggle = false;
        }

        if(this.props.survivor.departing === false && this.props.survivor.unavailable === true) {
            text = t("%name% is unavailable", {name: this.props.survivor.name});
            doToggle = false;
        }

        Toast.show(text);
        if(doToggle) {
            this.toggleDeparting();
        }
    }

    toggleSkipNextHuntBadge = () => {
        if(this.props.onSkipNextHunt) {
            let text = this.props.survivor.skipNextHunt ?
                t("%name% no longer skipping the next hunt", {name: this.props.survivor.name}) :
                t("%name% will skip the next hunt", {name: this.props.survivor.name});

            Toast.show(text);

            this.props.onSkipNextHunt(this.props.survivor.id);
        }
    }

    toggleReroll = () => {
        if(this.props.onReroll) {
            let text = this.props.survivor.reroll === false ?
                t("%name% no longer has their reroll", {name: this.props.survivor.name}) :
                t("%name% has their reroll again", {name: this.props.survivor.name});

            Toast.show(text);

            this.props.onReroll(this.props.survivor.id);
        }
    }

    togglePin = () => {
        let text = this.props.pinned ?
            t("%name% unpinned", {name: this.props.survivor.name}) :
            t("%name% pinned to the top", {name: this.props.survivor.name});

        Toast.show(text);

        this.props.onPinned(this.props.survivor.id);
    }

    devNull = () => {}

    survivorPressed = () => {
        this.props.onPress(this.props.survivor, this.props.index);
    }

    detailSurvivorPressed = () => {
        this.props.onPress(this.props.survivor, this.props.index + INDEX_MOD);
    }
}

SurvivorLineItem.propTypes = {
    detail: PropTypes.bool,
    onPinned: PropTypes.func,
    tapToDepart: PropTypes.bool,
}

SurvivorLineItem.defaultProps = {
    detail: false,
    tapToDepart: false,
}

let styles = StyleSheet.create({
    statsRow: {
        width: "20%",
    },
    attrRow: {
        width: "16.6667%",
        alignItems: "center",
    },
    attrValue: {
        fontSize: 20,
    },
    title:{
        fontSize: 15,
        fontWeight: "600",
    },
    row: {
        padding: 5
    },
    departingBanner: {
        paddingLeft: 5,
        fontSize: 16,
        fontWeight: "500"
    },
    retiredBanner: {
        paddingLeft: 5,
        fontSize: 16,
        fontWeight: "500",
        color: "black"
    },
    statText: {
        fontSize: 16,
        fontWeight: "bold",
        paddingRight: "2%"
    }
});
