'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

import Text from '../shared/app-text';
import sharedStyles from '../../style/styles';

import {
    View
} from 'react-native';

import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import ListItem from '../shared/ListItem';
import Popup from '../shared/popup';
import { getAliveMaleSurvivors, getAliveFemaleSurvivors } from '../../selectors/survivors';
import { sortArrayByName } from '../../helpers/helpers';

class Parents extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showFather: false,
            showMother: false,
        }
    }

    render() {
        return (
            <View>
                <Text style={sharedStyles.headerFontSize}>{t("Parents")}</Text>
                <View style={[sharedStyles.rowCentered, sharedStyles.container]}>
                    <ListItem
                        title={this.props.father ? this.props.father.name : t("None")}
                        subTitle={t("Father")}
                        buttonText={t("Choose")}
                        onPress={this.showFather}
                        confirm={false}
                        id="father"
                    />
                </View>
                <View style={[sharedStyles.rowCentered, sharedStyles.container]}>
                    <ListItem
                        title={this.props.mother ? this.props.mother.name : t("None")}
                        subTitle={t("Mother")}
                        buttonText={t("Choose")}
                        onPress={this.showMother}
                        confirm={false}
                        id="mother"
                    />
                </View>
                {<Popup
                    visible={this.state.showFather}
                    onDismissed={this.hideFather}
                >
                    {this.getFathers()}
                </Popup>}
                {<Popup
                    visible={this.state.showMother}
                    onDismissed={this.hideMother}
                >
                    {this.getMothers()}
                </Popup>}
            </View>
        );
    }

    hideFather = () => {
        this.setState({showFather: false});
    }

    showFather = () => {
        this.setState({showFather: true});
    }

    hideMother = () => {
        this.setState({showMother: false});
    }

    showMother = () => {
        this.setState({showMother: true});
    }

    getFathers = () => {
        return this.props.males.map((survivor, i) => {
            return <ListItem
                key={i}
                title={survivor.name}
                buttonText={t("Select")}
                onPress={this.setFather}
                confirm={false}
                id={survivor.id}
            />
        });
    }

    getMothers = () => {
        return this.props.females.map((survivor, i) => {
            return <ListItem
                key={i}
                title={survivor.name}
                buttonText={t("Select")}
                onPress={this.setMother}
                confirm={false}
                id={survivor.id}
            />
        });
    }

    setMother = (id) => {
        if(id === "") {
            return;
        }

        // pass: settlementId, id, father
        this.props.survivorActionWrapper('setMother', [this.props.settlementId, this.props.id, id]);
        this.hideMother();
    }

    setFather = (id) => {
        if(id === "") {
            return;
        }

        // pass: settlementId, id, father
        this.props.survivorActionWrapper('setFather', [this.props.settlementId, this.props.id, id]);
        this.hideFather();
    }
}

function mapStateToProps(state, props) {
    let males = getAliveMaleSurvivors(state).filter((survivor) => {return (survivor.id !== props.id)});
    let females = getAliveFemaleSurvivors(state).filter((survivor) => {return survivor.id !== props.id});

    males.sort(sortArrayByName);
    females.sort(sortArrayByName);
    let father = state.survivorReducer.survivors[props.settlementId][props.id].parents.father;
    let mother = state.survivorReducer.survivors[props.settlementId][props.id].parents.mother;

    if(father) {
        father = state.survivorReducer.survivors[props.settlementId][father];
    }

    if(mother) {
        mother = state.survivorReducer.survivors[props.settlementId][mother];
    }

    return {
        father,
        mother,
        males,
        females,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Parents);
