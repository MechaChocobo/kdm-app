'use strict';

import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

import {
    StyleSheet,
    View,
} from 'react-native';

import Text from '../shared/app-text';
import Button from '../shared/button';
import sharedStyles from '../../style/styles';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import Icon from '../shared/Icon';
import { IncrementorHidden } from '../shared/IncrementorHidden';
import RoundControl from '../shared/buttons/RoundControl';
import WoundBox from '../shared/WoundBox';
import ArmorSet from './ArmorSet';
import Hr from '../shared/Hr';

class Armor extends PureComponent {
    constructor(props) {
        super(props);
        this.state = { showDetails: false};
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <View style={[sharedStyles.rowCentered, {justifyContent: "space-between", marginBottom: 10}]}>
                    <Text style={sharedStyles.title}>{t("Armor")}</Text>
                    <View style={sharedStyles.rowCentered}>
                        <RoundControl onPress={this.decreaseAll} icon="minus"/>
                        <View style={{paddingRight: 10, paddingLeft: 10}}>
                            <Button title={t("Reset")} onPress={this.resetArmor} color={this.props.colors.HIGHLIGHT} shape="pill"/>
                        </View>
                        <RoundControl onPress={this.increaseAll} icon="plus"/>
                    </View>
                </View>
                <View style={{flexDirection: "row"}} >
                    {/* <View style={{width: "2%"}}></View> */}
                    <View style={{width: "20%", alignItems: "center"}}>
                        <Icon name="sk-head" size={30} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.bold}>{t("Head")}</Text>
                        <IncrementorHidden
                            value={this.props.head.value}
                            onIncrease={this.increaseArmor("head")}
                            onDecrease={this.decreaseArmor("head")}
                            onChange={this.setArmor("head")}
                            onEditStart={this.props.onIncrementorShow}
                            onEditEnd={this.props.onIncrementorHide}
                        />
                        <View style={styles.woundRow}>
                                <WoundBox filled={this.props.head.heavy} heavy={true} onPress={this.toggleWound("head", "heavy")}/>
                        </View>
                    </View>
                    <View style={{width: "20%", alignItems: "center"}}>
                        <Icon name="sk-arms" size={30} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.bold}>{t("Arms")}</Text>
                        <IncrementorHidden
                            value={this.props.arms.value}
                            onIncrease={this.increaseArmor("arms")}
                            onDecrease={this.decreaseArmor("arms")}
                            onChange={this.setArmor("arms")}
                            onEditStart={this.props.onIncrementorShow}
                            onEditEnd={this.props.onIncrementorHide}
                        />
                        <View style={sharedStyles.rowCentered}>
                                <WoundBox filled={this.props.arms.light} heavy={false} onPress={this.toggleWound("arms", "light")}/>
                                <WoundBox filled={this.props.arms.heavy} heavy={true} onPress={this.toggleWound("arms", "heavy")}/>
                        </View>
                    </View>
                    <View style={{width: "20%", alignItems: "center"}}>
                        <Icon name="sk-body" size={30} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.bold}>{t("Body")}</Text>
                        <IncrementorHidden
                            value={this.props.body.value}
                            onIncrease={this.increaseArmor("body")}
                            onDecrease={this.decreaseArmor("body")}
                            onChange={this.setArmor("body")}
                            onEditStart={this.props.onIncrementorShow}
                            onEditEnd={this.props.onIncrementorHide}
                        />
                        <View style={styles.woundRow}>
                                <WoundBox filled={this.props.body.light} heavy={false} onPress={this.toggleWound("body", "light")}/>
                                <WoundBox filled={this.props.body.heavy} heavy={true} onPress={this.toggleWound("body", "heavy")}/>
                        </View>
                    </View>
                    <View style={{width: "20%", alignItems: "center"}}>
                    <Icon name="sk-waist" size={30} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.bold}>{t("Waist")}</Text>
                        <IncrementorHidden
                            value={this.props.waist.value}
                            onIncrease={this.increaseArmor("waist")}
                            onDecrease={this.decreaseArmor("waist")}
                            onChange={this.setArmor("waist")}
                            onEditStart={this.props.onIncrementorShow}
                            onEditEnd={this.props.onIncrementorHide}
                        />
                        <View style={styles.woundRow}>
                                <WoundBox filled={this.props.waist.light} heavy={false} onPress={this.toggleWound("waist", "light")}/>
                                <WoundBox filled={this.props.waist.heavy} heavy={true} onPress={this.toggleWound("waist", "heavy")}/>
                        </View>
                    </View>
                    <View style={{width: "20%", alignItems: "center"}}>
                        <Icon name="sk-legs" size={30} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.bold}>{t("Legs")}</Text>
                        <IncrementorHidden
                            value={this.props.legs.value}
                            onIncrease={this.increaseArmor("legs")}
                            onDecrease={this.decreaseArmor("legs")}
                            onChange={this.setArmor("legs")}
                            onEditStart={this.props.onIncrementorShow}
                            onEditEnd={this.props.onIncrementorHide}
                        />
                        <View style={styles.woundRow}>
                                <WoundBox filled={this.props.legs.light} heavy={false} onPress={this.toggleWound("legs", "light")}/>
                                <WoundBox filled={this.props.legs.heavy} heavy={true} onPress={this.toggleWound("legs", "heavy")}/>
                        </View>
                    </View>
                </View>
                {(this.props.armorSet !== "" && this.props.armorSet !== undefined) && <View>
                    <Hr />
                    <ArmorSet settlementId={this.props.settlementId} id={this.props.id} compact={true} />
                </View>}
            </View>
        );
    }

    toggleDetail = () => {
        this.setState({showDetails: !this.state.showDetails});
    }

    increaseArmor = (location) => {
        return () => {
            // pass: settlementId, id, location, val (new value)
            this.props.survivorActionWrapper('setArmor', [this.props.settlementId, this.props.id, location, this.props[location].value + 1]);
        }
    }

    decreaseArmor = (location) => {
        return () => {
            if(this.props[location].value > 0) {
                // pass: settlementId, id, location, val (new value)
                this.props.survivorActionWrapper('setArmor', [this.props.settlementId, this.props.id, location, this.props[location].value - 1]);
            }
        }
    }

    // set value with text input (number pad)
    setArmor = (location) => {
        return (val) => {
            // pass: settlementId, id, location, val (new value)
            this.props.survivorActionWrapper('setArmor', [this.props.settlementId, this.props.id, location, +val]);
        }
    }

    increaseAll = () => {
        this.props.survivorActionWrapper('increaseAllArmor', [this.props.settlementId, this.props.id]);
    }

    decreaseAll = () => {
        this.props.survivorActionWrapper('decreaseAllArmor', [this.props.settlementId, this.props.id]);
    }

    resetArmor = () => {
        this.props.survivorActionWrapper('resetArmor', [this.props.settlementId, this.props.id]);
    }

    toggleWound = (location, severity) => {
        return () => {
            // pass: settlementId, id, location, severity
            this.props.survivorActionWrapper('toggleWound', [this.props.settlementId, this.props.id, location, severity]);
        }
    }
}

function mapStateToProps(state, props) {
    let set = "";
    if(state.survivorReducer.survivors[props.settlementId][props.id].loadout) {
        set = state.survivorReducer.survivors[props.settlementId][props.id].loadout.armorSet;
    }
    return {
        head: state.survivorReducer.survivors[props.settlementId][props.id].armor.head,
        arms: state.survivorReducer.survivors[props.settlementId][props.id].armor.arms,
        body: state.survivorReducer.survivors[props.settlementId][props.id].armor.body,
        waist: state.survivorReducer.survivors[props.settlementId][props.id].armor.waist,
        legs: state.survivorReducer.survivors[props.settlementId][props.id].armor.legs,
        armorSet: set,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Armor);

var styles = StyleSheet.create({
    woundRow : {
        flexDirection: "row",
        justifyContent: "center"
    }
});
