'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

var {
    View,
    Alert,
} = require('react-native');

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';
import { getGearConfig } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundControl from '../shared/buttons/RoundControl';
import Popup from '../shared/popup';
import ListItem from '../shared/ListItem';
import PillListItem from '../shared/PillListItem';

let GearConfig = null;

class CursedGear extends Component {
    constructor(props) {
        super(props);
        this.state = { showAddGear: false};
        GearConfig = getGearConfig();
    }

    render() {
        return (
            <View>
                <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                    <Text style={sharedStyles.title}>{t("Cursed Gear")}</Text>
                    <RoundControl icon="plus" onPress={this.showGearPopup}/>
                </View>
                <View style={sharedStyles.contentWrapper}>
                    <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                        {this.renderItems()}
                    </View>
                </View>
                <Popup title={t("Select Gear")} visible={this.state.showAddGear} onDismissed={this.onGearPopupClosed}>
                    {this.getPicker()}
                </Popup>
            </View>
        );
    }

    showGearPopup = () => {
        this.setState({showAddGear: true});
    }

    onGearPopupClosed = () => {
        this.setState({showAddGear: false});
    }

    renderItems = () => {
        return this.props.cursedItems.map((item, i) => {
            return <PillListItem
                key={i}
                title={GearConfig[item.location][item.item].title}
                icon="close"
                id={item}
                onPress={this.removeCursedItem}
            />
        });
    }

    getPicker = () => {
        let items = this.props.allItems.map((item, index) => {
            return (
                <ListItem
                    key={index}
                    title={GearConfig[item.location][item.item].title}
                    onPress={this.addCursedItem}
                    id={item.item}
                    buttonText={t("Add")}
                />
            );
        });

        return <View>
            {items}
        </View>
    }

    addCursedItem = (item) => {
        if(item === "") {
            return;
        }

        let locationName = "";

        for(let i = 0; i < this.props.allItems.length; i++) {
            if(this.props.allItems[i].item === item) {
                locationName = this.props.allItems[i].location;
                break;
            }
        }

        if(locationName !== "") {
            // pass: settlementId, id, location, item
            this.props.survivorActionWrapper("addCursedGear", [this.props.settlementId, this.props.id, locationName, item]);
        }

        this.setState({showAddGear: false});
    }

    removeCursedItem = (item) => {
        // pass: settlementId, id, location, item
        this.props.survivorActionWrapper("removeCursedGear", [this.props.settlementId, this.props.id, item.location, item.item]);
    }
}

function mapStateToProps(state, props) {
    return {
        allItems: state.settlementReducer.settlements[props.settlementId].allCursedItems,
        cursedItems: state.survivorReducer.survivors[props.settlementId][props.id].cursedItems,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CursedGear);
