'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import {survivorActionWrapper} from '../../actions/survivors';
import {settlementActionWrapper} from '../../actions/settlement';
import sharedStyles from '../../style/styles';
import Text from '../shared/app-text';
import { goBack } from '../../actions/router';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';

class Unavailable extends Component {
    toggleUnavailable = () => {
        this.props.survivorActionWrapper('setUnavailable', [this.props.settlementId, this.props.id, !this.props.unavailable]);
    }

    render() {
        return(
            <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                <Text style={sharedStyles.headerFontSize}>{t("Unavailable")}</Text>
                <TouchableOpacity onPress={this.toggleUnavailable}>
                    <Icon name={this.props.unavailable === true ? "checkbox-blank" : "checkbox-blank-outline"} size={22} color={this.props.colors.TEXT}/>
                </TouchableOpacity>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        unavailable: state.survivorReducer.survivors[props.settlementId][props.id].unavailable,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper, settlementActionWrapper, goBack}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Unavailable);
