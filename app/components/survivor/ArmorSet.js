'use strict';

import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { PropTypes } from "prop-types";

import {survivorActionWrapper} from '../../actions/survivors';
import styles from '../../style/styles';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import { getArmorConfig } from '../../helpers/helpers';
import ListItem from '../shared/ListItem';
import Popup from '../shared/popup';
import { getSortedArmorSets } from '../../selectors/settlement';
import AppText from '../shared/app-text';

let ArmorConfig = null;

class ArmorSet extends Component {
    constructor(props) {
        super(props);

        ArmorConfig = getArmorConfig();

        this.state = {
            showPopup: false
        }
    }

    render() {
        return this.props.compact ? this.renderCompact() : this.renderFull();
    }

    renderCompact = () => {
        if(!this.props.armorSet || !ArmorConfig[this.props.armorSet]) {
            return null;
        } else {
            return <AppText style={[styles.icomoon, styles.itemText, {lineHeight: 16}]}><AppText style={styles.bold}>{ArmorConfig[this.props.armorSet].title}: </AppText>{t(ArmorConfig[this.props.armorSet].description)}</AppText>;
        }
    }

    renderFull = () => {
        return <Fragment>
            <ListItem
                title={this.props.armorSet ? ArmorConfig[this.props.armorSet].title : t("None")}
                subTitle={t("Armor Set")}
                onPress={this.showPopup}
                buttonText={t("Change")}
            />
            {this.getArmorAmount()}
            {(this.props.armorSet !== "" && ArmorConfig[this.props.armorSet]) && <AppText style={[styles.icomoon, styles.itemText, {lineHeight: 16}]}>{t(ArmorConfig[this.props.armorSet].description)}</AppText>}
            {this.state.showPopup && <Popup
                title={t("Select armor set")}
                visible={this.state.showPopup}
                onDismissed={this.hidePopup}
            >
                {this.getArmorOptions()}
            </Popup>}
        </Fragment>
    }

    getArmorAmount = () => {
        if(this.props.armorSet !== "" && ArmorConfig[this.props.armorSet]) {
            if(ArmorConfig[this.props.armorSet].flat) {
                return <AppText style={[styles.itemText, styles.icomoon, {lineHeight: 16}]}>{t("%num%  to each hit location", {num: ArmorConfig[this.props.armorSet].armor})}</AppText>
            } else {
                return <AppText style={[styles.itemText, styles.icomoon, {lineHeight: 16}]}>{t("Add %num%  to each hit location", {num: ArmorConfig[this.props.armorSet].armor})}</AppText>
            }
        }

        return null;
    }

    getArmorOptions = () => {
        let options = [];

        if(this.props.armorSet) {
            options.push(
                <ListItem
                    title={t("None")}
                    key="none"
                    id=""
                    onPress={this.setArmorSet}
                    buttonText={t("Select")}
                />
            );
        }

        this.props.allArmor.map((set, i) => {
            if(set !== this.props.armorSet) {
                options.push(
                    <ListItem
                        title={ArmorConfig[set].title}
                        key={i}
                        id={set}
                        onPress={this.setArmorSet}
                        buttonText={t("Select")}
                    />
                );
            }
        });

        return options;
    }

    setArmorSet = (name) => {
        this.props.survivorActionWrapper("setArmorSet", [this.props.settlementId, this.props.id, name]);
        this.hidePopup();
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }
}

function mapStateToProps(state, props) {
    let set = "";
    if(state.survivorReducer.survivors[props.settlementId][props.id].loadout) {
        set = state.survivorReducer.survivors[props.settlementId][props.id].loadout.armorSet;
    }
    return {
        armorSet: set,
        allArmor: getSortedArmorSets(state),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ArmorSet);

ArmorSet.propTypes = {
    compact: PropTypes.bool,
}

ArmorSet.defaultProps = {
    compact: false,
}
