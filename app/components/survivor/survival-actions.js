'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {survivorActionWrapper} from '../../actions/survivors';

var {
    View,
} = require('react-native');

import CheckboxToggle from '../shared/checkbox-toggle';
import { t } from '../../helpers/intl';

class SurvivalActions extends Component {
    render() {
        return <View style={{flex: 1, justifyContent: 'space-between', alignSelf: "center"}}>
            {this.renderActions()}
        </View>
    }

    renderActions = () => {
        let actions = Object.keys(this.props.actions);
        return actions.map((action, i) => {
            return <View key={i} style={{flex: 1}}>
                <CheckboxToggle text={t(action)} value={action} selected={this.props.actions[action]} onPress={this.toggleAction}/>
            </View>
        });
    }

    toggleAction = (action) => {
        // pass: settlementId, id, action, value
        this.props.survivorActionWrapper("setSurvivalAction", [this.props.settlementId, this.props.id, action, !this.props.actions[action]]);
    }
}

function mapStateToProps(state, props) {
    return {
        actions: state.survivorReducer.survivors[props.settlementId][props.id].survivalActions
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SurvivalActions);
