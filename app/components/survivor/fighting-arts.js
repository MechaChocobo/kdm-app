'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TimerMixin from 'react-timer-mixin';
import reactMixin from 'react-mixin';
import Collapsible from 'react-native-collapsible';

import {survivorActionWrapper} from '../../actions/survivors';
import {getUnselectedArray, getRandomInt, getFightingArtConfig, sortIdArrayByConfigTitle} from '../../helpers/helpers';
import {getNonSecretFightingArts} from '../../selectors/settlement';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import Text from '../../components/shared/app-text';
import Button from '../../components/shared/button';
import sharedStyles from '../../style/styles';

let FightingArtConfig = null;
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundControl from '../shared/buttons/RoundControl';
import AbilityText from '../shared/AbilityText';
import Popup from '../shared/popup';
import ListItem from '../shared/ListItem';
import Hr from '../shared/Hr';
import AppTextInput from '../shared/AppTextInput';

class FightingArts extends Component {
    constructor(props) {
        super(props);
        FightingArtConfig = getFightingArtConfig();
        this.state = {
            showDetails: false,
            addError: "",
            tempArt: "",
            timeout: null,
            drawNum: "3",
            drawnArts: [],
            showAddList: false,
            shouldConfirmRemove: true,
        };
    }

    toggleDetail = () => {
        this.setState({showDetails: !this.state.showDetails});
    }

    drawArts = () => {
        let arts = [];

        while(arts.length < this.state.drawNum) {
            let art = this.props.nonSecretArts[getRandomInt(this.props.nonSecretArts.length)];

            if(arts.indexOf(art) === -1) {
                arts.push(art);
            }
        }

        this.setState({drawnArts: arts});
    }

    numDrawChange = (text) => {
        if(text.charAt(0) === '0') {
            text = text.substr(1);
        }

        if((isNaN(text) || !(text === parseInt(text, 10).toString())) && text !== "") {
            return;
        }

        this.setState({drawNum: text});
    }

    addDrawnArt = (name) => {
        return () => {
            if(this.props.arts.length < 3 && this.props.arts.indexOf(name) === -1) {
                this.addFightingArt(name);
                this.setState({drawnArts: []});
            }
        }
    }

    renderAddControls = () => {
        return (
            <View>
                <View style={{padding: 5}}>
                    <Button
                        title={t("+ Add Random Fighting Art")}
                        onPress={this.addRandomArt}
                        color={this.props.colors.HIGHLIGHT}
                        shape="rounded"
                    />
                </View>
                <View style={{flexDirection: "row", padding: 5, justifyContent: "space-between"}}>
                    <AppTextInput
                        style={{width: "16%", alignSelf: "flex-end"}}
                        keyboardType='numeric'
                        returnKeyType='done'
                        inputStyle={{fontSize: 20, fontWeight: "300"}}
                        onChangeText={this.numDrawChange}
                        value={this.state.drawNum}
                        textAlign="center"
                    />
                    <View style={{width: "80%"}}>
                        <Button
                            title={t("Draw %number% Fighting Arts", {number: this.state.drawNum})}
                            onPress={this.drawArts}
                            color={this.props.colors.HIGHLIGHT}
                            disabled={this.state.drawNum === "" || this.state.drawNum > this.props.nonSecretArts.length}
                            shape="rounded"
                        />
                    </View>
                </View>
                {this.state.drawnArts.length > 0 && <View style={{paddingLeft: 5}}>
                    <Text style={sharedStyles.itemText}> {this.props.arts.length < 3 ? t("Drew the following fighting arts -- tap to choose") : t("Drew the following fighting arts -- remove an existing fighting art to choose")}:</Text>
                    {this.state.drawnArts.map((name, i) => {
                        return <View key={i}>
                            <TouchableOpacity onPress={this.addDrawnArt(name)}>
                                <Text style={{fontSize: 16, fontWeight: "300", color: this.props.colors.HIGHLIGHT}}>{t(FightingArtConfig[name].title)}</Text>
                            </TouchableOpacity>
                            <View style={{height: 5}}/>
                        </View>
                    })}
                </View>}
                {this.state.addError !== "" && <View style={{padding: 5}}><Text style={sharedStyles.itemText}>{this.state.addError}</Text></View>}
            </View>
        );
    }

    showAddList = () => {
        this.setState({showAddList: true});
    }

    onAddListClosed = () => {
        this.setState({showAddList: false});
    }

    render() {
        return (
            <View>
                <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween]}>
                    <Text style={sharedStyles.title}>{t("Fighting Arts")}</Text>
                    <View style={sharedStyles.rowCentered}>
                        <RoundControl icon="dice-multiple" onPress={this.toggleDetail}/>
                        <View width={10}/>
                        <RoundControl icon="plus" onPress={this.showAddList} disabled={this.props.arts.length >= 3}/>
                    </View>
                </View>
                {this.props.arts.length > 0 && <Hr />}
                <View>
                    {this.renderFightingArts()}
                </View>
                <Popup title={t("Select Fighting Art")} visible={this.state.showAddList} onDismissed={this.onAddListClosed}>
                    {this.getPicker()}
                </Popup>
                <Collapsible collapsed={this.state.showDetails === false}>
                    {this.renderAddControls()}
                </Collapsible>
            </View>
        );
    }

    getPicker = () => {
        let availableArts = getUnselectedArray(this.props.arts, this.props.allArts, FightingArtConfig);

        let arts = availableArts.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={this.getFightingArtTitle(name)}
                    id={name}
                    onPress={this.addFightingArt}
                    buttonText={t("Add")}
                    confirm={false}
                />
            );
        });

        return (
            <View style={{width: "90%", alignSelf: "center"}}>
                {arts}
            </View>
        );
    }

    addFightingArt = (name) => {
        this.props.survivorActionWrapper('addFightingArt', [this.props.settlementId, this.props.id, name]);
        this.onAddListClosed();
    }

    addRandomArt = () => {
        clearInterval(this.state.timeout);
        this.setState({tempArt: "", timeout: null});
        let name = this.props.nonSecretArts[getRandomInt(this.props.nonSecretArts.length)];
        if(this.props.arts.indexOf(name) !== -1) {
            this.setState({addError: t("Selected Fighting Art %name%, but survivor already knows it!", {name: t(FightingArtConfig[name].title)})});
            let timeout = this.setTimeout(() => {
                this.setState({addError: ""});
            }, 5000);
            this.setState({timeout});
        } else if(this.props.arts.length >= 3) {
            this.setState({
                tempArt: name,
                addError: t("Selected Fighting Art '%name%', but survivor already has 3 Fighting Arts.  Remove one to automatically add '%name%'", {name: t(FightingArtConfig[name].title)}),
                shouldConfirmRemove: false,
            });
        } else {
            this.props.survivorActionWrapper('addFightingArt', [this.props.settlementId, this.props.id, name]);
        }
    }

    removeFightingArt = (name) => {
        this.props.survivorActionWrapper('removeFightingArt', [this.props.settlementId, this.props.id, name]);

        if(this.state.tempArt !== "") {
            setTimeout(() => {
                this.props.survivorActionWrapper('addFightingArt', [this.props.settlementId, this.props.id, this.state.tempArt]);
                this.setState({tempArt: "", addError: "", shouldConfirmRemove: true});
            }, 500);
        }
    }

    renderFightingArts = () => {
        let arts = [];
        for(let i = 0; i < 3; i++) {
            if(this.props.arts[i]) {
                arts.push(<AbilityText
                    key={i}
                    title={this.getFightingArtTitle(this.props.arts[i])}
                    description={t(FightingArtConfig[this.props.arts[i]].description)}
                    id={this.props.arts[i]}
                    onPress={this.removeFightingArt}
                    confirm={this.state.shouldConfirmRemove}
                    titleColor={this.props.colors.RED}
                />);
            }
        }

        return arts;
    }

    getFightingArtTitle = (art) => {
        let title = t(FightingArtConfig[art].title);

        if(FightingArtConfig[art].type === "secret") {
            title = '(S) ' + title;
        }

        return title;
    }
}

reactMixin(FightingArts.prototype, TimerMixin);

function mapStateToProps(state, props) {
    let nonSecretArts = getNonSecretFightingArts(state);
    if(state.settlementReducer.settlements[props.settlementId].campaign === "PotSun") {
        nonSecretArts = nonSecretArts.filter((name) => {return name !== "leader"});
    }

    return {
        arts: state.survivorReducer.survivors[props.settlementId][props.id].fightingArts,
        allArts: sortIdArrayByConfigTitle(state.settlementReducer.settlements[props.settlementId].allFightingArts, getFightingArtConfig()),
        nonSecretArts,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({survivorActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FightingArts);
