'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';

import {setLocale} from '../../actions/settings';
import { getSupportedLocales, t } from '../../helpers/intl';
import { ToggleList } from '../shared/toggle-list';
import { getColors } from '../../selectors/general';

class SettingsLanguage extends Component {
    render() {
        return (
            <View style={sharedStyles.container}>
                <View style={{marginTop: "2%"}}>
                    <View style={sharedStyles.row}>
                        <Text style={sharedStyles.title}>{t("Language")}</Text>
                    </View>
                    <View>
                        {this.renderLanguage()}
                    </View>
                </View>
            </View>
        );
    }

    renderLanguage = () => {
        return <View>
            <ToggleList onSelect={this.onLocaleChange} onDeselect={this.devNull} config={getSupportedLocales()} selected={this.props.locale} items={Object.keys(getSupportedLocales())}/>
        </View>
    }

    devNull = () => {}

    onLocaleChange = (locale) => {
        return () => {
            this.props.setLocale(locale);
        }
    }
}

function mapStateToProps(state, props) {
    return {
        locale: state.settingsReducer.locale,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setLocale,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsLanguage);
