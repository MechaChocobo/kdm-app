'use strict';

import React, { Component } from 'react';

import {
    View,
} from 'react-native';
import SettingsSurvivorLines from './Behavior/SettingsSurvivorLines';
import Hr from '../shared/Hr';
import SettingsTapToDepart from './Behavior/SettingsTapToDepart';
import AutoHost from './Behavior/AutoHost';
import SurvivorNameGeneration from './Behavior/SurvivorNameGeneration';
import SettingsDepartNumbers from './Behavior/SettingsDepartNumbers';

export default class SettingsBehavior extends Component {
    render() {
        return (
            <View>
                <View style={{height: 10}}/>
                <SettingsSurvivorLines />
                <Hr />
                <SettingsTapToDepart />
                <Hr />
                <AutoHost />
                <Hr />
                <SurvivorNameGeneration />
                <Hr />
                <SettingsDepartNumbers />
            </View>
        );
    }
}
