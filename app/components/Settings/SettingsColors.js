'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import Text from '../../components/shared/app-text';
import Button from '../../components/shared/button';
import sharedStyles from '../../style/styles';

import {setTheme, setColor, resetColors} from '../../actions/settings';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import ColorPicker from '../shared/ColorPicker';
import colors from '../../style/colors';

const colorTitles = {
    BLUE: {title: t("Blue")},
    BROWN: {title: t("Brown")},
    GOLD: {title: t("Gold")},
    GRAY: {title: t("Gray")},
    GREEN: {title: t("Green")},
    GREENDARK: {title: t("Green (Dark)")},
    ORANGE: {title: t("Orange")},
    PINK: {title: t("Pink")},
    PURPLE: {title: t("Purple")},
    RED: {title: t("Red")},
    TEAL: {title: t("Teal")},
    WHITE: {title: t("White")},
    YELLOW: {title: t("Yellow")},
}

class SettingsColors extends Component {
    render() {
        return (
            <View style={sharedStyles.container}>
                <View style={{marginTop: "2%"}}>
                    <View style={[sharedStyles.row, sharedStyles.spaceBetween, {marginRight: "2%"}]}>
                        <Text style={sharedStyles.title}>{t("Colors")}</Text>
                        <Button title={t("Reset to Defaults")} onPress={this.resetColors} shape="pill"/>
                    </View>
                    <View style={{width: "96%", alignSelf: "center"}}>
                        {this.renderColorInputs()}
                    </View>
                </View>
            </View>
        );
    }

    renderColorInputs = () => {
        return Object.keys(colorTitles).map((type, i) => {
            return (
                <ColorPicker
                    key={i}
                    colorId={type}
                    color={this.props.customColors[type] ? this.props.customColors[type] : colors[type]}
                    onColorChange={this.selectColor}
                    title={colorTitles[type].title}
                />
            )
        });
    }

    selectColor = (colorId, color) => {
        this.props.setColor(colorId, color);
    }

    resetColors = () => {
        this.props.resetColors();
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
        customColors: state.settingsReducer.colors,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setTheme,
        setColor,
        resetColors,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsColors);
