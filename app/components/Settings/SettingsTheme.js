'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Collapsible from 'react-native-collapsible';

import {
    StyleSheet,
    View,
    TouchableOpacity,
} from 'react-native';

import Text from '../../components/shared/app-text';
import Button from '../../components/shared/button';
import Icon from '../../components/shared/Icon';
import sharedStyles from '../../style/styles';

import {setTheme, setThemeColor, resetTheme} from '../../actions/settings';
import { t } from '../../helpers/intl';
import { getColors, getCustomDarkThemeColors, getCustomLightThemeColors } from '../../selectors/general';
import ColorPicker from '../shared/ColorPicker';
import { CHECKBOX_SIZE } from '../../helpers/helpers';
import themes from '../../style/themes';

const themeColorTitles = {
    HIGHLIGHT: {title: "Highlight", description: "Used to highlight interactiable items like buttons or draw attention to important text."},
    GRADIENT_START: {title: "Gradient Start", description: "The start (left) color of the gradient used in icons and other small elements."},
    GRADIENT_END: {title: "Gradient End", description: "The end (right) color of the gradient used in the banners, icons, and other small elements."},
    GRADIENT_START_BANNER: {title: "Banner Gradient Start", description: "The start (left) color of the banner gradient found at the top of some pages."},
    GRADIENT_END_BANNER: {title: "Banner Gradient End", description: "The end (right) color of the banner gradient found at the top of some pages."},
    GRADIENT_START_BACKGROUND: {title: "Background Gradient Start", description: "The start (left) color of the background gradient. Also the background color of modals like this one."},
    GRADIENT_END_BACKGROUND: {title: "Background Gradient End", description: "The end (right) color of the background gradient"},
    MENU: {title: "Menu Background", description: "The background color of the navigation menu."},
    TEXT: {title: "Text", description: "The primary text color."},
    INVERT_TEXT: {title: "Inverted Text", description: "The inverse of the primary text color -- dark if the primary text color is light and vice versa."},
    NAV_BAR_BACKGROUND: {title: "Nav Bar Background", description: "The background color of the settlement navigation bar."},
    NAV_BAR_ICON: {title: "Nav Bar Icon", description: "The color of unselected settlement navigation icons."},
    NAV_BAR_TEXT: {title: "Nav Bar Text", description: "The color text in the settlement navigation bar."},
}

class SettingsTheme extends Component {
    render() {
        return (
            <View style={sharedStyles.container} ref={this.setTipParent}>
                <View style={{marginTop: "2%"}}>
                    <View style={sharedStyles.row}>
                        <Text style={sharedStyles.title}>{t("Theme")}</Text>
                    </View>
                    <View style={{width: "96%", alignSelf: "center"}}>
                        {this.renderThemes()}
                    </View>
                </View>
            </View>
        );
    }

    renderCustomTheme = (themeType) => {
        let propColors = themeType === "customDark" ? this.props.customDarkThemeColors : this.props.customLightThemeColors;
        return Object.keys(themeColorTitles).map((type, i) => {
            return (
                <ColorPicker
                    key={i}
                    colorId={type}
                    color={propColors[type]}
                    onColorChange={this.selectColor(themeType)}
                    title={t(themeColorTitles[type].title)}
                    description={t(themeColorTitles[type].description)}
                />
            )
        });
    }

    renderPreview = (theme) => {
        let colors = null;
        if(theme.startsWith('custom')) {
            if(theme === "customDark") {
                colors = this.props.customDarkThemeColors;
            } else if(theme === "customLight") {
                colors = this.props.customLightThemeColors;
            }
        } else {
            colors = themes[theme];
        }

        let colorsOrder = Object.keys(themeColorTitles);

        let blocks = [];

        for(let i = 0; i < colorsOrder.length; i++) {
            let style = [localStyles.block];

            if(i === 0) {
                style.push(localStyles.firstBlock);
            }

            blocks.push(<View
                key={i}
                style={[style, {backgroundColor: colors[colorsOrder[i]]}]}
            />);
        }

        return blocks;
    }

    renderThemes = () => {
        return <View>
            <View style={sharedStyles.container}>
                <TouchableOpacity onPress={this.setThemeDark} style={sharedStyles.rowCentered}>
                    <View style={[sharedStyles.rowCentered, localStyles.optionRow, sharedStyles.flex50]}>
                        <Icon name={this.props.theme === "dark" ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.itemText}>{t("Dark")}</Text>
                    </View>
                    <View style={[sharedStyles.row, sharedStyles.flex50]}>{this.renderPreview("dark")}</View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.setThemeLight} style={sharedStyles.rowCentered}>
                    <View style={[sharedStyles.rowCentered, localStyles.optionRow, sharedStyles.flex50]}>
                        <Icon name={this.props.theme === "light" ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.itemText}>{t("Light")}</Text>
                    </View>
                    <View style={[sharedStyles.row, sharedStyles.flex50]}>{this.renderPreview("light")}</View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.setThemeCustomDark} style={sharedStyles.rowCentered}>
                    <View style={[sharedStyles.rowCentered, localStyles.optionRow, sharedStyles.flex50]}>
                        <Icon name={this.props.theme === "customDark" ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.itemText}>{t("Custom Dark")}</Text>
                    </View>
                    <View style={[sharedStyles.row, sharedStyles.flex50]}>{this.renderPreview("customDark")}</View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.setThemeCustomLight} style={sharedStyles.rowCentered}>
                    <View style={[sharedStyles.rowCentered, localStyles.optionRow, sharedStyles.flex50]}>
                        <Icon name={this.props.theme === "customLight" ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.itemText}>{t("Custom Light")}</Text>
                    </View>
                    <View style={[sharedStyles.row, sharedStyles.flex50]}>{this.renderPreview("customLight")}</View>
                </TouchableOpacity>
            </View>
            {this.props.theme === "customDark" && <Collapsible collapsed={this.props.theme !== "customDark"}>
                {this.renderCustomTheme("customDark")}
                <Button title={t("Reset to Defaults")} onPress={this.resetDarkTheme} shape="rounded" style={{marginTop: 10}}/>
            </Collapsible>}
            {this.props.theme === "customLight" && <Collapsible collapsed={this.props.theme !== "customLight"}>
                {this.renderCustomTheme("customLight")}
                <Button title={t("Reset to Defaults")} onPress={this.resetLightTheme} shape="rounded" style={{marginTop: 10}}/>
            </Collapsible>}
        </View>
    }

    setTipParent = (ref) => {
        this.setState({tipParent: ref});
    }

    resetDarkTheme = () => {
        this.props.resetTheme("customDark");
    }

    resetLightTheme = () => {
        this.props.resetTheme("customLight");
    }

    setThemeDark = () => {
        if(this.props.theme === "dark") {
            return;
        }

        this.props.setTheme("dark");
    }

    setThemeLight = () => {
        if(this.props.theme === "light") {
            return;
        }

        this.props.setTheme("light");
    }

    setThemeCustomDark = () => {
        if(this.props.theme === "customDark") {
            return;
        }

        this.setState({editingTheme: "customDark"});
        this.props.setTheme("customDark");
    }

    setThemeCustomLight = () => {
        if(this.props.theme === "customLight") {
            return;
        }

        this.setState({editingTheme: "customLight"});
        this.props.setTheme("customLight");
    }

    selectColor = (themeType) => {
        return (type, color) => {
            this.props.setThemeColor(themeType, type, color);
        }
    }

    switchTheme = () => {
        let theme = this.props.theme === "dark" ? "light" : "dark";
        this.props.setTheme(theme);
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
        theme: state.settingsReducer.theme,
        customDarkThemeColors: getCustomDarkThemeColors(state),
        customLightThemeColors: getCustomLightThemeColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setTheme,
        setThemeColor,
        resetTheme,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsTheme);

const localStyles = StyleSheet.create({
    optionRow: {
        marginBottom: 8
    },
    firstBlock: {
        borderLeftWidth: 1,
    },
    block: {
        borderTopWidth: 1,
        borderBottomWidth: 1,
        borderRightWidth: 1,
        borderColor: "black",
        height: 20,
        width: "9.1%",
    },
});
