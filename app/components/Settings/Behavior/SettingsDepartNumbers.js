'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Switch from 'react-native-switch-pro';

import {
    View,
} from 'react-native';

import { getColors } from '../../../selectors/general';
import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import { setShowDepartingIndex } from '../../../actions/settings';

class AutoHost extends Component {
    state = {
        showPopup: false,
    }

    render() {
        return (
            <View style={[styles.width90, styles.centered, styles.rowCentered, styles.spaceBetween]}>
                <View style={styles.flexShrink}>
                    <AppText style={styles.fontSize20}>{t("Show departing survivor's number")}</AppText>
                </View>
                <Switch
                    value={this.props.showDepartingIndex}
                    onSyncPress={this.toggleDepartingIndex}
                    circleColorActive={this.props.colors.HIGHLIGHT}
                    backgroundActive={this.props.colors.GRAY}
                    backgroundInactive={this.props.colors.GRAY}
                />
            </View>
        );
    }

    toggleDepartingIndex = () => {
        this.props.setShowDepartingIndex(!this.props.showDepartingIndex);
    }
}

function mapStateToProps(state, props) {
    return {
        showDepartingIndex: state.settingsReducer.showDepartingIndex,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setShowDepartingIndex,
    }, dispatch);
}

AutoHost.defaultProps = {
    showDepartingIndex: false,
}

export default connect(mapStateToProps, mapDispatchToProps)(AutoHost);
