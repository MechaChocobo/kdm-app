'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    StyleSheet,
} from 'react-native';

import { getColors } from '../../../selectors/general';
import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import { setSurvivorNameGeneration } from '../../../actions/settings';
import Popup from '../../shared/popup';
import Button from '../../shared/button';
import ListItem from '../../shared/ListItem';
import Icon from '../../shared/Icon';

class SurvivorNameGeneration extends Component {
    state = {
        showPopup: false,
    }

    render() {
        return (
            <View style={[styles.width90, styles.centered, styles.rowCentered, styles.spaceBetween]}>
                <View style={styles.flexShrink}>
                    <AppText style={localStyles.title}>{t("Survivor Name Generation Source")}</AppText>
                </View>
                <Button style={styles.rowCentered} onPress={this.showPopup} shape="rounded" skeleton={true}>
                    <View style={[styles.rowCentered, {padding: 10, paddingLeft: 15}]}>
                        <AppText style={[styles.itemText, {marginRight: 5}]}>{this.props.survivorNameGeneration === "local" ? t("Fantasy") : t("Generic") }</AppText>
                        <Icon name="pencil" size={20} color={this.props.colors.HIGHLIGHT} />
                    </View>
                </Button>
                <Popup
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                    title={t("Select Name Source")}
                >
                    <ListItem
                        id="net"
                        title={t("Generic")}
                        onPress={this.setSource}
                        buttonText={t("Select")}
                    />
                    <ListItem
                        id="local"
                        title={t("Fantasy")}
                        onPress={this.setSource}
                        buttonText={t("Select")}
                    />
                </Popup>
            </View>
        );
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    setSource = (value) => {
        this.props.setSurvivorNameGeneration(value);
        this.hidePopup();
    }
}

function mapStateToProps(state, props) {
    return {
        survivorNameGeneration: state.settingsReducer.survivorNameGeneration,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setSurvivorNameGeneration,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SurvivorNameGeneration);

const localStyles = StyleSheet.create({
    title: {
        fontSize: 20,
    },
});
