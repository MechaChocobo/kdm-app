'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Switch from 'react-native-switch-pro';


import {
    View,
    StyleSheet,
} from 'react-native';

import { getColors } from '../../../selectors/general';
import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import { setTapToDepart } from '../../../actions/settings';

class SettingsTapToDepart extends Component {
    render() {
        return (
            <View style={[styles.width90, styles.centered, styles.rowCentered, styles.spaceBetween]}>
                <View style={styles.flexShrink}>
                    <AppText style={localStyles.title}>{t("Tap instead of long press to mark departing on survivor list")}</AppText>
                </View>
                <Switch
                    value={this.props.tapToDepart}
                    onSyncPress={this.toggleTapToDepart}
                    circleColorActive={this.props.colors.HIGHLIGHT}
                    backgroundActive={this.props.colors.GRAY}
                    backgroundInactive={this.props.colors.GRAY}
                />
            </View>
        );
    }

    toggleTapToDepart = () => {
        this.props.setTapToDepart(!this.props.tapToDepart);
    }
}

function mapStateToProps(state, props) {
    return {
        tapToDepart: state.settingsReducer.tapToDepart,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setTapToDepart
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsTapToDepart);

const localStyles = StyleSheet.create({
    title: {
        fontSize: 20,
    }
});
