'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    StyleSheet,
} from 'react-native';

import { getColors } from '../../../selectors/general';
import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import RoundControl from '../../shared/buttons/RoundControl';
import { setNumLinesSurvivorCard } from '../../../actions/settings';

class SettingsSurvivorLines extends Component {
    render() {
        return (
            <View style={[styles.width90, styles.centered, styles.rowCentered]}>
                <View style={localStyles.titleWrapper}>
                    <AppText style={localStyles.title}>{t("Number of summary lines on survivor card")}</AppText>
                </View>
                <View style={styles.rowCentered}>
                    <RoundControl
                        icon="minus"
                        onPress={this.valueDecrease}
                    />
                    <AppText style={styles.value}>{this.props.numLinesSurvivorCard}</AppText>
                    <RoundControl
                        icon="plus"
                        onPress={this.valueIncrease}
                    />
                </View>
            </View>
        );
    }

    valueIncrease = () => {
        if(this.props.numLinesSurvivorCard >= 10) {
            return;
        }

        this.props.setNumLinesSurvivorCard(this.props.numLinesSurvivorCard + 1);
    }

    valueDecrease = () => {
        if(this.props.numLinesSurvivorCard <= 1) {
            return;
        }

        this.props.setNumLinesSurvivorCard(this.props.numLinesSurvivorCard - 1);
    }
}

function mapStateToProps(state, props) {
    return {
        numLinesSurvivorCard: state.settingsReducer.numLinesSurvivorCard,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setNumLinesSurvivorCard
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsSurvivorLines);

const localStyles = StyleSheet.create({
    title: {
        fontSize: 20,
    },
    titleWrapper: {
        flexShrink: 1
    },
    inputWrapper: {
        marginLeft: 35,
        marginRight: 35,
    }
});
