'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import { getColors } from '../../../selectors/general';
import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import Icon from '../../shared/Icon';
import Popup from '../../shared/popup';
import { setAutoHostSettlement } from '../../../actions/settings';
import ListItem from '../../shared/ListItem';
import Button from '../../shared/button';

class AutoHost extends Component {
    state = {
        showPopup: false,
    }

    render() {
        return (
            <View style={[styles.width90, styles.centered, styles.rowCentered, styles.spaceBetween]}>
                <View style={localStyles.titleWrapper}>
                    <AppText style={localStyles.title}>{t("Automatically host settlement on app start")}</AppText>
                </View>
                {this.renderButton()}
                <Popup
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                    title={t("Select Settlement")}
                >
                    <View key="clear" style={[styles.centered, styles.container, {marginBottom: 15, marginLeft: "10%", marginRight: "10%"}]}>
                        <Button title={t("None")} onPress={this.onClear} shape="rounded"/>
                    </View>
                    {Object.values(this.props.settlements).map((settlement, i) => {
                        return <ListItem
                            key={i}
                            id={settlement.id}
                            title={settlement.name}
                            onPress={this.selectSettlement}
                            buttonText={t("Select")}
                        />
                    })}
                </Popup>
            </View>
        );
    }

    renderButton = () => {
        return <Button style={styles.rowCentered} onPress={this.showPopup} shape="rounded" skeleton={true}>
            <View style={[styles.rowCentered, {padding: 10, paddingLeft: 15}]}>
                <AppText style={[styles.itemText, {marginRight: 5}]}>{this.props.autoHostSettlement ? this.props.settlements[this.props.autoHostSettlement].name : t("None")}</AppText>
                <Icon name="pencil" size={20} color={this.props.colors.HIGHLIGHT} />
            </View>
        </Button>
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    onClear = () => {
        this.props.setAutoHostSettlement(null);
        this.hidePopup();
    }

    selectSettlement = (id) => {
        this.props.setAutoHostSettlement(id);
        this.hidePopup();
    }
}

function mapStateToProps(state, props) {
    return {
        autoHostSettlement: state.settingsReducer.autoHostSettlement,
        settlements: state.settlementReducer.settlements,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setAutoHostSettlement,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AutoHost);

const localStyles = StyleSheet.create({
    title: {
        fontSize: 20,
    },
    titleWrapper: {
        flexShrink: 1
    },
    inputWrapper: {
        marginLeft: 35,
        marginRight: 35,
    }
});
