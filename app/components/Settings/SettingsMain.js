'use strict';

import React, { Component } from 'react';

import {
    View,
} from 'react-native';

// import Hr from '../shared/Hr';
import SettingsBackups from './SettingsBackups';
import SettingsLanguage from './SettingsLanguage';
import SettingsTheme from './SettingsTheme';
import SettingsColors from './SettingsColors';
import SettingsDateFormat from './SettingsDateFormat';
import styles from '../../style/styles';

export default class SettingsMain extends Component {
    render() {
        return (
            <View style={styles.container}>
                <SettingsBackups />
                {/* <Hr /> */}
                <SettingsLanguage />
                {/* <Hr /> */}
                <SettingsDateFormat />
                {/* <Hr /> */}
                <SettingsTheme />
                {/* <Hr /> */}
                <SettingsColors />
            </View>
        );
    }
}
