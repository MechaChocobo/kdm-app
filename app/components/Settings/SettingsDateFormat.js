'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import Text from '../../components/shared/app-text';
import styles from '../../style/styles';

import {setDateFormat} from '../../actions/settings';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import { CHECKBOX_SIZE } from '../../helpers/helpers';
import AppText from '../../components/shared/app-text';
import Icon from '../shared/Icon';

const formats = {
    mdy: "m/d/y",
    dmy: "d/m/y"
}

class SettingsDateFormat extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={{marginTop: "2%"}}>
                    <View style={[styles.row, styles.spaceBetween]}>
                        <Text style={styles.title}>{t("Date Format")}</Text>
                    </View>
                    <View style={{marginLeft: 8, marginTop: 10}}>
                        {this.renderFormats()}
                    </View>
                </View>
            </View>
        );
    }

    renderFormats = () => {
        let first = true;
        let rendered = [];
        for(let format in formats) {
            rendered.push(this.renderFormat(format, formats[format], first));
            first = false;
        }

        return rendered;
    }

    renderFormat = (format, title, selected = false) => {
        return <TouchableOpacity key={format} onPress={this.setFormat(format)} style={{alignSelf: "flex-start", marginBottom: 10}}>
            <View style={styles.rowCentered}>
                <Icon
                    name={(this.props.dateFormat === format || (!this.props.dateFormat && selected)) ? "checkbox-blank" : "checkbox-blank-outline"}
                    size={CHECKBOX_SIZE}
                    color={this.props.colors.TEXT}
                />
                <AppText style={styles.itemText}>{title}</AppText>
            </View>
        </TouchableOpacity>
    }

    setFormat = (format) => {
        return () => {
            this.props.setDateFormat(format);
        }
    }
}

function mapStateToProps(state, props) {
    return {
        dateFormat: state.settingsReducer.dateFormat,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setDateFormat,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsDateFormat);
