'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FilePicker from 'react-native-file-picker';
import CompareVersions from 'compare-versions';
import RNFS from 'react-native-fs';
import Toast from 'react-native-simple-toast';
import Share from 'react-native-share';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Collapsible from 'react-native-collapsible';

import {
    View,
    TouchableOpacity,
    Platform,
    ScrollView,
    Alert,
    PermissionsAndroid,
} from 'react-native';

import Text from '../../components/shared/app-text';
import Button from '../../components/shared/button';
import sharedStyles from '../../style/styles';

import {createBackup, validateSchema, setBackupSettings, importSettings} from '../../actions/settings';
import {importSettlement} from '../../actions/settlement';
import {importSurvivors} from '../../actions/survivors';
import {importTimeline} from '../../actions/timeline';
import { t } from '../../helpers/intl';
import { importCustomExpansions } from '../../actions/custom-expansions';
import { getColors } from '../../selectors/general';
import { CHECKBOX_SIZE } from '../../helpers/helpers';
import { importMilestones } from '../../actions/milestones';

const packageInfo = require('../../../package.json');

class SettingsBackups extends Component {
    constructor(props) {
        super(props);
        this.state = {
            backupError: false,
            backupSuccess: false,
            importError: false,
            importSuccess: false,
            selectedBackupFile: false,
            showBackups: false,
            backups: [],
            sharing: false,
        };
    }

    createBackupWrapper = () => {
        if(Platform.OS === "android") {
            this.requestFileWritePermission();
        } else {
            this.createBackup();
        }
    }

    toggleBackupSettings = () => {
        this.props.setBackupSettings(!this.props.backupSettings)
    }

    shareBackupFile = () => {
        if(Platform.OS === "ios") {
            if(this.state.showBackups === false || this.state.sharing === false) {
                this.setState({showBackups: true, sharing: true});
                RNFS.readDir(RNFS.DocumentDirectoryPath + '/backups').then(results => {
                    this.setState({backups: results});
                }).catch(() => {});
            } else {
                this.setState({showBackups: false, sharing: false})
            }
        } else {
            FilePicker.showFilePicker({title: t("Pick a backup file to share")}, (response) => {
                if(response.didCancel) {
                } else if(response.error) {
                } else {
                    this.share(response.path);
                }
            });
        }
    }

    shareSelectedBackup = () => {
        let backup = this.state.backups[this.state.selectedBackupFile];
        this.share(backup.path).then(() => {
            this.setState({selectedBackupFile: false});
        });
    }

    share = (path) => {
        return RNFS.readFile(path, "utf8").then((file) => {
            Share.open({
                title: "Share backup via",
                message: file,
                subject: path.match(/[a-zA-Z0-9\-\.]+$/)[0],
            }).then(() => {})
            .catch((err) => {});
        });
    }

    shareBackupWrapper = () => {
        if(Platform.OS === "android") {
            this.requestFileReadPermission(this.shareBackupFile);
        } else {
            this.shareBackupFile();
        }
    }

    async requestFileWritePermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': t('Scribe External Storage Permission'),
                    'message': t('Scribe needs access to your external storage in order to create a backup file')
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED || granted === true) {
                this.createBackup();
            } else {
                Toast.show(t("You must grant storage access in order to create backups"), Toast.LONG);
            }
        } catch (err) {
            Toast.show(t("Error requesting storage permission"), Toast.LONG);
        }
    }

    createBackup = () => {
        this.setState({backupError: false, backupSuccess: false});
        // ExternalDirectoryPath goes to emulated/0/android/data/com.tabooapps.scribe/files (android only)
        let dir = Platform.select({
            ios: RNFS.DocumentDirectoryPath + '/backups',
            android: RNFS.ExternalStorageDirectoryPath + '/scribe-kdm'
        });

        let d = new Date();
        let settings = this.props.backupSettings ? 'with-settings-' : '';
        let date = d.getFullYear() + '-' + (d.getMonth() + 1) + '-' + (d.getDate()) + '-' + d.getHours() +'-' + d.getMinutes() + '-' + d.getSeconds();
        let path = Platform.select({
            ios: dir + '/backup-'+settings+date+'.json',
            android: dir + '/backup-'+settings+date+'.json'
        });

        let contents = {
            settlements: this.props.settlements,
            survivors: this.props.survivors,
            timelines: this.props.timelines,
            customExpansions: this.props.customExpansions,
            milestones: this.props.milestones,
            version: packageInfo.version
        };

        if(this.props.backupSettings === true) {
            contents.settings = this.props.settings;
        }

        contents = JSON.stringify(contents);

        RNFS.mkdir(dir);

        RNFS.writeFile(path, contents, "utf8").then(() => {
            let message = Platform.select({
                ios: '/backups/backup-'+settings+date+'.json',
                android: '/scribe-kdm/backup-'+settings+date+'.json'
            });

            this.setState({backupError: false, backupSuccess: message});
        }).catch((error) => {
            this.setState({backupError: error, backupSuccess: false});
        });
    }

    importBackupWrapper = () => {
        if(Platform.OS === "android") {
            this.requestFileReadPermission(this.importBackup);
        } else {
            this.importBackup();
        }
    }

    async requestFileReadPermission(callback) {
        try {
            const granted = await PermissionsAndroid.requestMultiple(
                [PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE],
                {
                    'title': t('Scribe External Storage Permission'),
                    'message': t('Scribe needs access to your external storage in order to import a backup file')
                }
            )

            let valid = true;
            for(let permission in granted) {
                if(granted[permission] !== PermissionsAndroid.RESULTS.GRANTED || granted === true) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                callback();
            } else {
                Toast.show(t("You must grant storage access in order to import backups"), Toast.LONG);
            }
        } catch (err) {
            Toast.show(t("Error requesting storage permission"), Toast.LONG);
        }
    }

    importBackup = () => {
        if(Platform.OS === "ios") {
            if(this.state.showBackups === false || this.state.sharing === true) {
                this.setState({showBackups: true, sharing: false});
                RNFS.readDir(RNFS.DocumentDirectoryPath + '/backups').then(results => {
                    this.setState({backups: results});
                }).catch(() => {});
            } else {
                this.setState({showBackups: false, sharing: false});
            }
        } else {
            FilePicker.showFilePicker({title: t("Pick a backup file to import")}, (response) => {
                if(response.didCancel) {
                } else if(response.error) {
                } else {
                    RNFS.readFile(response.path, "utf8").then((file) => {
                        this.runImport(file);
                    }).catch((error) => {
                        this.setState({importError: error, importSuccess: false});
                    });
                }
            });
        }
    }

    runImport = (file) => {
        let data = null;
        try {
            data = JSON.parse(file);
        } catch(e) {
            return;
        }

        if(!data.settlements || !data.survivors || !data.timelines) {
            Toast.show(t("Backup file not formatted properly"));
            return;
        }

        if(data.version && CompareVersions(data.version, packageInfo.version) === 1) {
            Toast.show(t("Trying to import a backup from a future version (%version%), please update Scribe to the latest version.", {version: data.version}), Toast.LONG);
            return;
        }

        // do data import
        let settlements = Object.values(data.settlements);
        for (let i = 0; i < settlements.length; i++) {
            this.props.importSettlement(settlements[i]);
            this.props.importSurvivors(settlements[i].id, data.survivors[settlements[i].id]);
            this.props.importTimeline(settlements[i].id, data.timelines[settlements[i].id]);
        }

        if(data.customExpansions) {
            this.props.importCustomExpansions(data.customExpansions);
        }

        if(data.milestones) {
            this.props.importMilestones(data.milestones);
        }

        if(data.settings) {
            this.props.importSettings(data.settings);
        }

        if(data.version && CompareVersions(data.version, packageInfo.version) === -1) {
            this.props.validateSchema(data.version);
        }
        this.setState({importError: false, importSuccess: true});
    }

    renderBackups = () => {
        let files = this.state.backups.map((backup, i) => {
            return <View style={{flexDirection: "row", flex: 1, alignItems: "center"}} key={i}>
                <TouchableOpacity style={{flexDirection: "row", flex: .9, alignItems: "center"}} onPress={this.selectBackup(i)}>
                    <Icon style={{paddingRight: 15}} name={this.state.selectedBackupFile === i ? "checkbox-blank" : "checkbox-blank-outline"} size={15} color="white"/>
                    <Text style={sharedStyles.itemText}>{backup.name}</Text>
                </TouchableOpacity>
                {this.state.sharing === false && <MaterialIcon name="close" onPress={this.confirmDelete(i)} size={30} />}
            </View>
        });

        let button = <Button title={t("Import Selected")} onPress={this.importSelectedBackup} disabled={this.state.selectedBackupFile === false} shape="rounded"/>
        let header = t("Select file to import");
        if(this.state.sharing === true) {
            button = <Button title={t("Share Selected")} onPress={this.shareSelectedBackup} disabled={this.state.selectedBackupFile === false} shape="rounded"/>
            header = t("Select file to share");
        }

        return <ScrollView style={{paddingLeft: "2%"}}>
            <Text style={{fontSize: 20, fontWeight: "bold", paddingTop: 10}}>{header}</Text>
            {files}
            <View style={{padding: "2%", paddingLeft: 0}}>
                {button}
            </View>
        </ScrollView>
    }

    importSelectedBackup = () => {
        let backup = this.state.backups[this.state.selectedBackupFile];
        RNFS.readFile(backup.path, "utf8").then((file) => {
            this.runImport(file);
            this.setState({selectedBackupFile: false});
        }).catch((error) => {
            this.setState({importError: error, importSuccess: false});
        });
    }

    selectBackup = (i) => {
        return () => {
            if(this.state.selectedBackupFile === i) {
                this.setState({selectedBackupFile: false});
            } else {
                this.setState({selectedBackupFile: i});
            }
        }
    }

    deleteBackup = (i) => {
        return () => {
            let backups = this.state.backups;
            let toDelete = backups[i];
            RNFS.unlink(toDelete.path).then(() => {
                backups.splice(i, 1);
                this.setState({backups});
            });
        }
    }

    confirmDelete = (i) => {
        return () => {
            Alert.alert(t("Are you sure?"), t("Deleting a backup is permanent and cannot be undone."), [
                {text: t("Cancel"), onPress: () => {}},
                {text: t("Confirm"), onPress: this.deleteBackup(i)}
            ]);
        }
    }

    devNull = () => {}

    render() {
        let error = this.state.backupError ? <Text style={sharedStyles.itemText}>{this.state.backupError}</Text> : null;
        let success = this.state.backupSuccess ? <Text style={sharedStyles.itemText}>{t("Backup file located at: %filePath%", {filePath: this.state.backupSuccess})}</Text> : null;
        let importSuccess = this.state.importSuccess ? <Text style={sharedStyles.itemText}>{t("Backup was imported successfully!")}</Text> : null;
        return (
            <View style={sharedStyles.container}>
                <Text style={sharedStyles.title}>{t("Backup and Restore")}</Text>
                <View style={sharedStyles.contentWrapper}>
                    {Platform.OS === "ios" && <View style={{paddingLeft: 5}}><Text style={sharedStyles.itemText}>{t("Warning: on iOS uninstalling Scribe will also remove any backups on the device.  If you want to transfer them to your computer follow %a%these steps%a%.", {linkUrl: "https://support.apple.com/en-us/HT201301", linkColor: this.props.colors.BLUE})}</Text></View>}
                    <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "10%", paddingBottom: "5%"}}>
                        <TouchableOpacity onPress={this.toggleBackupSettings} style={[sharedStyles.rowCentered, {marginBottom: 20}]}>
                            <Icon color={this.props.colors.TEXT} name={this.props.backupSettings === true ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE}/>
                            <Text style={sharedStyles.itemText}>{t("Include settings in backups")}</Text>
                        </TouchableOpacity>
                        <Button title={t("Create Backup")} onPress={this.createBackupWrapper} shape="rounded"/>
                        {error}
                        {success}
                    </View>
                    <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%", paddingBottom: "5%"}}>
                        <Button title={t("Import Backup")} onPress={this.importBackupWrapper} shape="rounded"/>
                        {importSuccess}
                    </View>
                    <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%", paddingBottom: "10%"}}>
                        <Button title={t("Share Backup")} onPress={this.shareBackupWrapper} shape="rounded"/>
                    </View>
                    <Collapsible collapsed={this.state.showBackups === false}>
                        {this.renderBackups()}
                    </Collapsible>
                </View>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    // don't want the version getting backed up
    let settings = {...state.settingsReducer};
    delete settings.version;

    return {
        settlements: state.settlementReducer.settlements,
        survivors: state.survivorReducer.survivors,
        timelines: state.timelineReducer,
        customExpansions: Object.values(state.customExpansions),
        milestones: state.milestones,
        locale: state.settingsReducer.locale,
        backupSettings: state.settingsReducer.backupSettings,
        settings,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        createBackup,
        importSettlement,
        importSurvivors,
        importTimeline,
        importCustomExpansions,
        importMilestones,
        validateSchema,
        setBackupSettings,
        importSettings,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsBackups);
