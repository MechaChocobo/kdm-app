'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Uuid from 'react-native-uuid';

import {
    View,
    TouchableOpacity,
    ScrollView
} from 'react-native';

import {goToSurvivor, goBack, changePage} from '../actions/router';
import {survivorActionWrapper} from '../actions/survivors';
import {settlementActionWrapper} from '../actions/settlement';
import Text from '../components/shared/app-text';
import Button from '../components/shared/button';
import AppTextInput from '../components/shared/AppTextInput';
import Hr from '../components/shared/Hr';
import Popup from '../components/shared/popup';
import ListItem from '../components/shared/ListItem';
import styles from '../style/styles';
import { getAliveMaleSurvivors, getAliveFemaleSurvivors } from '../selectors/survivors';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import Icon from './shared/Icon';
import { CHECKBOX_SIZE } from '../helpers/helpers';
import BackMenu from './shared/BackMenu';
import { getRandomName } from '../helpers/nameGeneration';
import { setSurvivorNameGeneration } from '../actions/settings';

const NAME_SERVICE_URL = "https://namey.muffinlabs.com/name.json";

class CreateSurvivor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            gender: "F",
            father: "",
            mother: "",
            nameFetchError: "",
            nameLoading: false,
            showFather: false,
            showMother: false,
        };
    }

    getFather = () => {
        let title = t("None");
        if(this.state.father) {
            for(let i = 0; i < this.props.maleSurvivors.length; i++) {
                if(this.props.maleSurvivors[i].id === this.state.father) {
                    title = this.props.maleSurvivors[i].name;
                    break;
                }
            }
        }

        return <ListItem
            title={title}
            onPress={this.showFather}
            buttonText={t("Choose")}
            subTitle={t("Father")}
        />
    }

    getFatherPicker = () => {
        let survivors = this.renderSurvivorOptions(this.props.maleSurvivors, "father");
        survivors.splice(0, 0, <ListItem
            id=""
            key="-1"
            title={t("None")}
            onPress={this.setMother}
            buttonText={t("Choose")}
        />);

        return survivors;
    }

    setFather = (id) => {
        this.setState({father: id});
        this.hideFather();
    }

    getMother = () => {
        let title = t("None");
        if(this.state.mother) {
            for(let i = 0; i < this.props.femaleSurvivors.length; i++) {
                if(this.props.femaleSurvivors[i].id === this.state.mother) {
                    title = this.props.femaleSurvivors[i].name;
                    break;
                }
            }
        }

        return <ListItem
            title={title}
            onPress={this.showMother}
            buttonText={t("Choose")}
            subTitle={t("Mother")}
        />
    }

    getMotherPicker = () => {
        let survivors = this.renderSurvivorOptions(this.props.femaleSurvivors);
        survivors.splice(0, 0, <ListItem
            id=""
            key="-1"
            title={t("None")}
            onPress={this.setMother}
            buttonText={t("Choose")}
        />);

        return survivors;
    }

    setMother = (id) => {
        this.setState({mother: id});
        this.hideMother();
    }

    renderSurvivorOptions = (survivors, mode = "mother") => {
        survivors.sort((a, b) => {
            if(a.name > b.name) {
                return 1;
            } else if(a.name < b.name) {
                return -1;
            } else {
                return 0;
            }
        });

        return survivors.map((survivor, i) => {
            return <ListItem
                key={i}
                title={survivor.name}
                id={survivor.id}
                onPress={mode === "mother" ? this.setMother : this.setFather}
                buttonText={t("Choose")}
            />
        });
    }

    getRandomName = () => {
        if(this.props.survivorNameGeneration === "local") {
            let name = getRandomName(this.state.gender);
            this.setState({name});
        } else {
            this.setState({nameLoading: true});
            let config = {
                count: 1,
                type: (this.state.gender == "M" ? "male" : "female"),
                frequency: "all",
                with_surname: "false"
            }

            let queryString = '?';
            let pieces = [];
            for(var key in config) {
                pieces.push(key + '=' + config[key]);
            }
            queryString += pieces.join('&');

            fetch(NAME_SERVICE_URL + queryString).then((response) => {
                return response.json();
            }).then((response) => {
                this.setState({name: response[0], nameLoading: false});
            }).catch((error) => {
                this.setState({nameFetchError: error, nameLoading: false});
            }) ;
        }
    }

    showFather = () => {
        this.setState({showFather: true});
    }

    hideFather = () => {
        this.setState({showFather: false});
    }

    showMother = () => {
        this.setState({showMother: true});
    }

    hideMother = () => {
        this.setState({showMother: false});
    }

    render() {
        return (
            <View style={styles.container}>
                <BackMenu />
                <Text style={styles.pageHeader}>{t("New Survivor")}</Text>
                <ScrollView>
                    <View style={styles.wrapper}>
                        <View style={{paddingBottom: 2, alignItems: "center"}}>
                            <Text style={styles.itemText}>{t("In Settlement")}: {this.props.maleSurvivors.length} [{t("M")}] / {this.props.femaleSurvivors.length} [{t("F")}]</Text>
                        </View>
                        <Text style={styles.title}>{t("Gender")}</Text>
                        <View style={{flexDirection: "row", flex: 1}}>
                            <TouchableOpacity onPress={() => {this.setState({gender: "F"})}} style={{width: "50%"}}>
                                <View style={styles.headerWrapper}>
                                    <Icon name={this.state.gender === "F" ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                                    <Text style={styles.itemText}>{t("Female")}</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {this.setState({gender: "M"})}} style={{width: "50%"}}>
                                <View style={styles.headerWrapper}>
                                    <Icon name={this.state.gender === "M" ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                                    <Text style={styles.itemText}>{t("Male")}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Hr />
                    <View style={styles.wrapper}>
                        <Text style={styles.title}>{t("Name")}</Text>
                        <AppTextInput
                            inputStyle={{fontSize: 20}}
                            value={this.state.name}
                            onChangeText={(name) => {this.setState({name})}}
                        />
                        <Button style={{width: "75%", alignSelf: "center", marginTop: 10, marginBottom: 10}} title={t("Random Name")} onPress={this.getRandomName} loading={this.state.nameLoading} shape="rounded"/>
                        <View style={[styles.rowCentered, styles.centered]}>
                            <View style={styles.flexShrink}>
                                <Text style={styles.itemText}>{t("Survivor name type:")}</Text>
                            </View>
                            <TouchableOpacity style={styles.rowCentered} onPress={this.cycleNameSource}>
                                <Icon name="autorenew" color={this.props.colors.HIGHLIGHT} size={20} />
                                <Text style={styles.itemText}>{this.props.survivorNameGeneration === "local" ? t("Fantasy") : t("Generic")}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Hr />
                    <View style={styles.wrapper}>
                        <Text style={styles.title}>{t("Parents")}</Text>
                        <Text>{t("Only survivors with 2 parents are eligible to receive settlement bonuses at birth.")}</Text>
                        {this.getFather()}
                        {this.getMother()}
                    </View>
                </ScrollView>
                <View style={{width: "90%", alignSelf: "center", marginTop: 20, marginBottom: 20}}>
                    <Button title={t("Create")} onPress={this.createSurvivor} disabled={this.state.name === ""} shape="rounded"/>
                </View>
                {this.state.showFather && <Popup
                    visible={this.state.showFather}
                    onDismissed={this.hideFather}
                >
                    {this.getFatherPicker()}
                </Popup>}
                {this.state.showMother && <Popup
                    visible={this.state.showMother}
                    onDismissed={this.hideMother}
                >
                    {this.getMotherPicker()}
                </Popup>}
            </View>
        );
    }

    cycleNameSource = () => {
        if(this.props.survivorNameGeneration === "net") {
            this.props.setSurvivorNameGeneration("local");
        } else {
            this.props.setSurvivorNameGeneration("net");
        }
    }

    createSurvivor = () => {
        // params go in here
        let id = Uuid.v4();
        let parents = {father: this.state.father, mother: this.state.mother};
        let bonuses = {principles: this.props.principles, innovations: this.props.innovations, settlement: this.props.settlementBonuses};

        // pass: settlementId, id, name, gender, parents, bonuses, numSurvivors, campaign
        this.props.survivorActionWrapper('createSurvivor', [this.props.settlementId, id, this.state.name, this.state.gender, parents, bonuses, (this.props.maleSurvivors.length + this.props.femaleSurvivors.length), this.props.campaign]);

        this.props.goBack(); // clears this page from history so back goes to survivor list
        if(this.props.isClient) {
            this.props.changePage("survivor-home");
        } else {
            this.props.goToSurvivor(id);
        }
    }
}

function mapStateToProps(state, props) {
    return {
        settlementId: state.pageReducer.settlementId,
        population: state.settlementReducer.settlements[state.pageReducer.settlementId].population,
        numSurvivors: Object.keys(state.survivorReducer.survivors[state.pageReducer.settlementId]).length,
        maleSurvivors: getAliveMaleSurvivors(state),
        femaleSurvivors: getAliveFemaleSurvivors(state),
        principles: state.settlementReducer.settlements[state.pageReducer.settlementId].principles,
        innovations: state.settlementReducer.settlements[state.pageReducer.settlementId].innovations,
        settlementBonuses: state.settlementReducer.settlements[state.pageReducer.settlementId].settlementEffects,
        campaign: state.settlementReducer.settlements[state.pageReducer.settlementId].campaign,
        isClient: state.networkReducer.client !== null,
        survivorNameGeneration: state.settingsReducer.survivorNameGeneration ? state.settingsReducer.survivorNameGeneration : "net",
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goBack, goToSurvivor, survivorActionWrapper, settlementActionWrapper, changePage, setSurvivorNameGeneration}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSurvivor);
