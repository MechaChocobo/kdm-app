'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import {goToSettlement, goToSurvivor} from '../actions/router';
import {getDeadSurvivors} from '../selectors/survivors';
import {SurvivorLineItem, getCardHeight} from '../components/survivor/survivor-line-item';
import { RecyclerListView, DataProvider, LayoutProvider } from "recyclerlistview";

import {
    View,
    Dimensions,
} from 'react-native';

import sharedStyles from '../style/styles';
import { getColors } from '../selectors/general';
import { getSettlementId } from '../selectors/settlement';
import { initRecylcerContext } from '../helpers/recycler-context';

class SurvivorsDead extends Component {
    constructor(props) {
        super(props);

        let { width } = Dimensions.get("window");

        let dataProvider = new DataProvider((r1, r2) => {
            return r1 != r2;
        }).cloneWithRows([...this.props.survivors]);

        this.layoutProvider = new LayoutProvider((index) => {
            return index;
        },
        (type, dim) => {
            dim.width = width;
            dim.height = getCardHeight(this.props.numLinesSurvivorCard);
        });

        this.recyclerContext = initRecylcerContext('survivor-home-dead');

        this.state = {
            dataProvider
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.survivors !== this.props.survivors) {
            this.setState({dataProvider: new DataProvider((r1, r2) => {
                return r1 != r2;
            }).cloneWithRows([...this.props.survivors])});
        }
    }

    onPress = (survivor) => {
        this.props.goToSurvivor(survivor.id);
    }

    renderSurvivor = (type, item) => {
        let survivor = item;
        return (
            <SurvivorLineItem
                survivor={survivor}
                isSotf={this.props.isSotf}
                onPress={this.onPress}
                numLinesSurvivorCard={this.props.numLinesSurvivorCard}
                tapToDepart={this.props.tapToDepart}
            />
        );
    }

    backToSettlement = () => {
        this.props.goToSettlement(this.props.id);
    };

    render() {
        return (
            <View style={sharedStyles.container}>
                {this.props.survivors.length > 0 && <RecyclerListView
                    layoutProvider={this.layoutProvider}
                    dataProvider={this.state.dataProvider}
                    rowRenderer={this.renderSurvivor}
                    contextProvider={this.recyclerContext}
                />}
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementId = getSettlementId(state);
    return {
        id: settlementId,
        isSotf: (state.settlementReducer.settlements[settlementId].principles.newLife == "survivalOfTheFittest"),
        survivors: getDeadSurvivors(state),
        colors: getColors(state),
        numLinesSurvivorCard: state.settingsReducer.numLinesSurvivorCard,
        tapToDepart: state.settingsReducer.tapToDepart,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goToSettlement, goToSurvivor}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SurvivorsDead);
