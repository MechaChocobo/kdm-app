'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import {
    ScrollView,
} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view-universal';

import Text from './shared/app-text';
import sharedStyles from '../style/styles';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import BGBanner from './shared/BGBanner';
import Innovations from './settlement/innovations';
import Principles from './settlement/principles';

import styles from '../style/styles';
import BonusSummary from './settlement/BonusSummary';
import Milestones from './settlement/Milestones';

class SettlementUpgrades extends Component {
    state = {
        scroll: null,
    }

    setScrollRef = (ref) => {
        this.setState({scroll: ref});
    }

    render() {
        return (
            <ScrollView style={[sharedStyles.container]} ref={this.setScrollRef}>
                <BGBanner>
                    <Text style={sharedStyles.pageHeader} color={this.props.colors.WHITE}>{this.props.settlementName}</Text>
                </BGBanner>
                <ScrollableTabView
                    locked={true}
                    tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                    tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                    tabBarInactiveTextColor={this.props.colors.TEXT}
                    tabBarTextStyle={styles.tabText}
                >
                    <Innovations tabLabel={t("Innovations")} id={this.props.id} scrollRef={this.state.scroll}/>
                    <Principles tabLabel={t("Principles")} id={this.props.id}/>
                    <Milestones tabLabel={t("Milestones")} id={this.props.id}/>
                    <BonusSummary tabLabel={t("Bonuses")} id={this.props.id}/>
                </ScrollableTabView>
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return {
        id: settlementId,
        settlementName: state.settlementReducer.settlements[settlementId].name,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementUpgrades);
