'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import SwipeList from './survivor/departing-views/SwipeList';
import GridList from './survivor/departing-views/GridList';
import { getDepartingSurvivorIds } from '../selectors/survivors';
import VerticalDoubleList from './survivor/departing-views/VerticalDoubleList';
import HorizontalDoubleList from './survivor/departing-views/HorizontalDoubleList';

export const LIST_GRID = "grid";
export const LIST_SWIPE = "swipe";
export const LIST_VERT_DOUBLE = "vert-double";
export const LIST_HORIZ_DOUBLE = "horiz-double";

class SurvivorList extends Component {
    render() {
        if(this.props.departView === LIST_GRID && this.props.numDeparting <= 4) {
            return <GridList />
        } else if(this.props.departView === LIST_VERT_DOUBLE) {
            return <VerticalDoubleList />
        } else if(this.props.departView === LIST_HORIZ_DOUBLE) {
            return <HorizontalDoubleList />
        } else {
            return <SwipeList />
        }
    }
}

function mapStateToProps(state, props) {
    return {
        departView: state.settingsReducer.departView,
        numDeparting: getDepartingSurvivorIds(state).length,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SurvivorList);
