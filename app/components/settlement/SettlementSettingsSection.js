'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import styles from '../../style/styles';
import { t } from '../../helpers/intl';
import { changePage } from '../../actions/router';
import Button from '../shared/button';
import AppText from '../../components/shared/app-text';

class SettlementSettingsSection extends Component {
    render() {
        return (
            <View style={[styles.width90, styles.centered]}>
                <AppText style={styles.title}>{t("Settlement Settings")}</AppText>
                <AppText style={styles.itemText}>{t("Change what content is available to your settlement")}</AppText>
                <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%"}}>
                    <Button title={t("Change Settings")} onPress={this.goToSettings}shape="rounded"/>
                </View>
            </View>
        );
    }

    goToSettings = () => {
        this.props.changePage('settlement-settings');
    }
}

function mapStateToProps(state, props) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({changePage}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementSettingsSection);
