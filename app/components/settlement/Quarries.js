'use strict';

import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import {settlementActionWrapper} from '../../actions/settlement';
import styles from '../../style/styles';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import Popup from '../shared/popup';
import ToggleListItem from '../shared/ToggleListItem';
import AppText from '../../components/shared/app-text';
import RoundControl from '../shared/buttons/RoundControl';
import { getQuarriesForShowdowns } from '../../helpers/helpers';

class Quarries extends PureComponent {
    state = {
        showPopup: false,
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.width90, styles.centered,]}>
                    <View style={[styles.rowCentered, styles.spaceBetween]}>
                        <AppText style={styles.title}>{t("Quarries")}</AppText>
                        <RoundControl icon="pencil" onPress={this.showPopup}/>
                    </View>
                    {this.renderQuarries()}
                </View>
                {this.state.showPopup && <Popup
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                    title={t("Edit Quarries")}
                >
                    {this.renderPopup()}
                </Popup>}
            </View>
        )
    }

    renderQuarries = () => {
        return <AppText style={styles.itemText}>{this.props.quarries.join(', ')}</AppText>
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    renderPopup = () => {
        return this.props.allQuarries.map((quarry, i) => {
            return <ToggleListItem
                key={i}
                title={quarry}
                id={quarry}
                onToggle={this.toggleQuarry}
                value={this.props.quarries.indexOf(quarry) !== -1}
            />
        });
    }

    toggleQuarry = (quarry) => {
        if(this.props.quarries.indexOf(quarry) === -1) {
            this.props.settlementActionWrapper("addQuarry", [this.props.id, quarry]);
        } else {
            this.props.settlementActionWrapper("removeQuarry", [this.props.id, quarry]);
        }
    }
}

function mapStateToProps(state, props) {
    return {
        quarries: getQuarriesForShowdowns(state.settlementReducer.settlements[props.id].quarryShowdowns),
        allQuarries: getQuarriesForShowdowns(state.settlementReducer.settlements[props.id].allShowdowns),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Quarries);
