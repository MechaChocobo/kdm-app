'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import styles from '../../style/styles';
import PrincipleWrapper from './PrincipleWrapper';
import PrincipleConfig from '../../config/principles.json';

class Principles extends Component {
    render() {
        return (
            <View>
                <View style={[styles.wrapper, {width: "90%", alignSelf: "center"}]}>
                    <View style={styles.contentWrapper}>
                        <PrincipleWrapper
                            principle={this.props.principles.newLife}
                            config={this.getNewLifeConfig()}
                            principleTitle={t("New Life")}
                            principleType="newLife"
                            id={this.props.id}
                        />
                        <PrincipleWrapper
                            principle={this.props.principles.death}
                            config={PrincipleConfig.death}
                            principleTitle={t("Death")}
                            principleType="death"
                            id={this.props.id}
                        />
                        <PrincipleWrapper
                            principle={this.props.principles.conviction}
                            config={PrincipleConfig.conviction}
                            principleTitle={t("Conviction")}
                            principleType="conviction"
                            id={this.props.id}
                        />
                        <PrincipleWrapper
                            principle={this.props.principles.society}
                            config={PrincipleConfig.society}
                            principleTitle={t("Society")}
                            principleType="society"
                            id={this.props.id}
                        />
                    </View>
                </View>
            </View>
        );
    }

    getNewLifeConfig = () => {
        let config = {...PrincipleConfig.newLife};

        if(this.props.campaign === "PotSun") {
            let keys = Object.keys(config);
            delete config[keys[0]];
        }

        return config;
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
        principles: state.settlementReducer.settlements[props.id].principles,
        campaign: state.settlementReducer.settlements[props.id].campaign,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Principles);
