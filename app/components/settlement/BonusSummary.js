'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';


import { getInnovationConfig, getPrinciplesConfig } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import { getSettlementBonuses } from '../../selectors/settlement';
import AppText from '../shared/app-text';
import styles from '../../style/styles';
import Hr from '../shared/Hr';

let InnovationConfig = null;
let PrincipleConfig = null;

/**
 * settlementEffects:
 *   survivalLimit
 *
 * Survivor Effects:
 *   newBornEffects
 *   allNewSurvivors
 *   survivalActions
 *     understanding
 *     courage
 *     xp
 *     attributes
 *       STR (etc)
 *
 * Departure effects:
 *   settlementEffects - survivalDepart
 *   settlementEffects - insanityDepart
 *   departingText
 */

class BonusSummary extends Component {
    constructor(props) {
        super(props);
        this.state = { pickingLocation: false};
        InnovationConfig = getInnovationConfig();
        PrincipleConfig = getPrinciplesConfig();
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderSections()}
            </View>
        );
    }

    renderSections = () => {
        let sections = [];

        if(this.hasDeparting()) {
            sections.push(this.renderDeparting());
        }

        if(this.hasSurvivalLimit()) {
            sections.push(this.renderSurvivalLimit());
        }

        if(this.hasAllNew()) {
            sections.push(this.renderAllNewSurvivors());
        }

        if(this.hasNewBorn()) {
            sections.push(this.renderNewBornSurvivors());
        }

        if(this.hasSurvivalActions()) {
            sections.push(this.renderSurvivalActions());
        }

        for(let i = 1; i < sections.length ; i += 2) {
            sections.splice(i, 0, <Hr key={i} />);
        }

        return sections;
    }

    hasSurvivalLimit = () => {
        if(this.props.bonuses.survivalLimit) {
            return true;
        }

        return false;
    }

    hasNewBorn = () => {
        if(this.props.bonuses.newBornEffects) {
            return true;
        }

        return false;
    }

    hasAllNew = () => {
        if(this.props.bonuses.allNewSurvivors) {
            return true;
        }

        return false;
    }

    hasDeparting = () => {
        if(this.props.bonuses.departing) {
            return true;
        }

        return false;
    }

    hasSurvivalActions = () => {
        if(this.props.bonuses.survivalActions) {
            return true;
        }

        return false;
    }

    renderNewBornSurvivors = () => {
        if(!this.props.bonuses.newBornEffects) {
            return null;
        }

        return <View style={[styles.width90, styles.centered]} key="newBorn">
            <AppText style={styles.fontSize20}>{t('New Born Survivors')}</AppText>
            <Hr slim={true}/>
            {this.props.bonuses.newBornEffects.map(this.getAllNewSurvivorText)}
        </View>
    }

    renderAllNewSurvivors = () => {
        if(!this.props.bonuses.allNewSurvivors) {
            return null;
        }

        return <View style={[styles.width90, styles.centered]} key="allNew">
            <AppText style={styles.fontSize20}>{t('All New Survivors')}</AppText>
            <Hr slim={true}/>
            {this.props.bonuses.allNewSurvivors.map(this.getAllNewSurvivorText)}
        </View>
    }

    getAllNewSurvivorText = (item, index) => {
        return <AppText key={index} style={styles.itemText}><AppText style={styles.bold}>{this.getItemTitle(item)}:</AppText> {this.getSurvivorStatText(item)}</AppText>
    }

    getSurvivorStatText = (item) => {
        let stats = [];

        for(let stat in item.value) {
            if(stat === "attributes") {
                for(let attr in item.value[stat]) {
                    stats.push(attr + ' +' + item.value[stat][attr]);
                }
            } else if(stat === 'xp') {
                stats.push('hunt xp +'+item.value[stat]);
            } else if(stat === 'survivalActions') {
                continue;
            } else {
                stats.push(stat + ' +' + item.value[stat]);
            }
        }

        return stats.join(', ');
    }

    renderSurvivalActions = () => {
        if(!this.hasSurvivalActions()) {
            return null;
        }

        return <View style={[styles.width90, styles.centered]} key="survival">
            <AppText style={styles.fontSize20}>{t('Survival Actions')}</AppText>
            <Hr slim={true}/>
            {this.props.bonuses.survivalActions.map(this.getSurvivalText)}
        </View>
    }

    getSurvivalText = (item, index) => {
        return <AppText key={index} style={styles.itemText}><AppText style={styles.bold}>{this.getItemTitle(item) + ': '}</AppText>{item.value}</AppText>
    }

    renderDeparting = () => {
        if(!this.props.bonuses.departing) {
            return null;
        }

        return <View style={[styles.width90, styles.centered]} key="departing">
            <AppText style={styles.fontSize20}>{t('Departing Bonuses')}</AppText>
            <Hr slim={true}/>
            {this.props.bonuses.departing.map(this.getDepartingText)}
        </View>
    }

    getDepartingText = (item, index) => {
        let text = [];

        if(item.value.survivalDepart) {
            text.push('Survival +' + item.value.survivalDepart);
        } if(item.value.insanityDepart) {
            text.push('Insanity +' + item.value.insanityDepart);
        } if(item.value.departingText) {
            text.push(item.value.departingText);
        }

        return <AppText key={index} style={[styles.itemText, styles.icomoon, {lineHeight: 22}]}><AppText style={styles.bold}>{this.getItemTitle(item)}: </AppText>{text.join(', ')}</AppText>
    }

    renderSurvivalLimit = () => {
        if(!this.props.bonuses.survivalLimit) {
            return null;
        }

        return <View style={[styles.width90, styles.centered]} key="limit">
            <AppText style={styles.fontSize20}>{t('Survival Limit')} {this.getSurvivalLimitTotal()}</AppText>
            <Hr slim={true}/>
            {this.props.bonuses.survivalLimit.map(this.getSurvivalLimitText)}
        </View>
    }

    getSurvivalLimitTotal = () => {
        let total = 0;
        for(let i = 0; i < this.props.bonuses.survivalLimit.length; i++) {
            total += this.props.bonuses.survivalLimit[i].value;
        }

        return total > 0 ? '+'+total : total;
    }

    getSurvivalLimitText = (item, index) => {
        return <AppText key={index} style={styles.itemText}><AppText style={styles.bold}>{this.getItemTitle(item)}:</AppText> +{item.value}</AppText>
    }

    getItemTitle = (item) => {
        if(item.type === 'innovation') {
            return InnovationConfig[item.id].title;
        } else if(item.type === 'principle') {
            let principle = item.id.split(';');
            return PrincipleConfig[principle[0]][principle[1]].title;
        }
    }
}

function mapStateToProps(state, props) {
    return {
        bonuses: getSettlementBonuses(state),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BonusSummary);
