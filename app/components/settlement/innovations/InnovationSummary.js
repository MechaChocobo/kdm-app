'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
} from 'react-native';

import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import Hr from '../../shared/Hr';
import { extractBonuses } from '../../../selectors/settlement';
import { t } from '../../../helpers/intl';
import EndeavorList from '../endeavors/EndeavorList';
import { getInnovationConfig, getAbilityConfig } from '../../../helpers/helpers';
import { getInnovationConsequences } from '../../../helpers/innovations';

let InnovationConfig = null;

export default class InnovationSummary extends PureComponent {
    constructor(props) {
        super(props);

        InnovationConfig = getInnovationConfig();
    }

    render() {
        return (
            <View>
                <AppText style={[styles.title, styles.centered]}>{this.props.innovation.title}</AppText>
                <Hr />
                {this.props.innovation.description !== '' && <AppText style={[styles.italic, {marginBottom: 10}]}>{t(this.props.innovation.description)}</AppText>}
                {this.renderInnovationBonuses()}
                {this.hasEndeavors() && <View style={{height: 10}} />}
                {this.renderEndeavors()}
                <View style={{height: 10}} />
                {this.renderConsequences()}
            </View>
        )
    }

    renderConsequences = () => {
        let consequences = getInnovationConsequences(this.props.innovation.name, this.props.innovations, this.props.allInnovations);
        consequences = consequences.filter((name) => {
            return (this.props.innovationDeck.indexOf(name) === -1)
        });

        if(consequences.length === 0) {
            return null;
        }

        consequences = consequences.map((name) => {
            return InnovationConfig[name].title;
        });

        return <AppText key={0} style={[styles.itemText, styles.icomoon, {lineHeight: 22}]}><AppText style={styles.bold}>{t("Consequences")}: </AppText>{consequences.join(', ')}</AppText>
    }

    renderEndeavors = () => {
        if(!this.props.innovation.settlementEffects || !this.props.innovation.settlementEffects.endeavors) {
            return null;
        }

        return <EndeavorList
            endeavors={this.props.innovation.settlementEffects.endeavors}
            details={true}
            zebra={true}
        />
    }

    hasEndeavors = () => {
        return this.props.innovation.settlementEffects && this.props.innovation.settlementEffects.endeavors;
    }

    renderInnovationBonuses = () => {
        let bonuses = extractBonuses({}, 'innovation', this.props.innovation, this.props.innovation.name);

        if(Object.keys(bonuses).length === 0) {
            return null;
        }

        let children = [];

        if(bonuses.departing) {
            children.push(this.addDepartingText(bonuses.departing[0].value));
        }

        if(bonuses.survivalLimit) {
            children.push(this.addSurvivalLimitText(bonuses.survivalLimit[0].value));
        }

        if(bonuses.newBornEffects) {
            children.push(this.addNewBornEffectsText(bonuses.newBornEffects[0].value));
        }

        if(bonuses.allNewSurvivors) {
            children.push(this.addAllNewSurvivorsText(bonuses.allNewSurvivors[0].value));
        }

        if(bonuses.survivalActions) {
            children.push(this.addSurvivalActionsText(bonuses.survivalActions[0].value));
        }

        return <View>
            {children}
        </View>
    }

    addDepartingText = (bonus) => {
        let text = [];

        if(bonus.survivalDepart) {
            text.push('Survival +' + bonus.survivalDepart);
        } if(bonus.insanityDepart) {
            text.push('Insanity +' + bonus.insanityDepart);
        } if(bonus.departingText) {
            text.push(bonus.departingText);
        }

        return <AppText key={0} style={[styles.itemText, styles.icomoon, {lineHeight: 22}]}><AppText style={styles.bold}>{t("Departing Effects")}: </AppText>{text.join(', ')}</AppText>
    }

    addSurvivalLimitText = (bonus) => {
        return <AppText key={1} style={styles.itemText}><AppText style={styles.bold}>{t("Survival Limit")}:</AppText> +{bonus}</AppText>
    }

    addNewBornEffectsText = (stats) => {
        return <AppText key={2} style={styles.itemText}><AppText style={styles.bold}>{t("New Born Survivors")}:</AppText> {this.getSurvivorStatText(stats)}</AppText>
    }

    addAllNewSurvivorsText = (stats) => {
        return <AppText key={3} style={styles.itemText}><AppText style={styles.bold}>{t("All New Survivors")}:</AppText> {this.getSurvivorStatText(stats)}</AppText>
    }

    addSurvivalActionsText = (action) => {
        return <AppText key={4} style={styles.itemText}><AppText style={styles.bold}>{t("Survival Actions")}:</AppText> {action}</AppText>
    }

    getSurvivorStatText = (bonuses) => {
        let stats = [];

        for(let stat in bonuses) {
            if(stat === "attributes") {
                for(let attr in bonuses[stat]) {
                    stats.push(attr + ' +' + bonuses[stat][attr]);
                }
            } else if(stat === 'xp') {
                stats.push('hunt xp +'+bonuses[stat]);
            } else if(stat === 'survivalActions') {
                continue;
            } else if(stat === 'abilities') {
                const AbilityConfig = getAbilityConfig();
                let abilities = [];
                for (let i = 0; i < bonuses[stat].length; i++) {
                    abilities.push(AbilityConfig[bonuses[stat][i]].title);
                }
                stats.push('Abilities: '+abilities.join(', '));
            } else {
                stats.push(stat + ' +' + bonuses[stat]);
            }
        }

        return stats.join(', ');
    }
}

InnovationSummary.propTypes = {
    innovation: PropTypes.object.isRequired,
}

InnovationSummary.defaultProps = {
}

const localStyles = StyleSheet.create({
});
