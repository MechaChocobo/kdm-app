'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Uuid from 'react-native-uuid';
import SimpleToast from 'react-native-simple-toast';

import {changePage} from '../../actions/router';
import {deleteSettlement, toggleArchive, addExpansions, copySettlement, addCustomExpansionsToSettlement, removeCustomExpansionsFromSettlement, downgradeCustomExpansionsInSettlement, removeExpansionsFromSettlement, downgradeExpansionsInSettlement} from '../../actions/settlement';

import {
    View, Alert,
} from 'react-native';

import Text from '../../components/shared/app-text';
import Button from '../../components/shared/button';
import sharedStyles from '../../style/styles';
import ExpansionSelector from '../settlement-create/expansion-selector';
import { getExpansionList } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import Popup from '../shared/popup';

class Admin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showExpansions: false,
            selectedExpansions: {...this.props.expansions},
            selectedCustomExpansions: {...this.props.customExpansions},
            expansionsChanged: false,
            customExpansionsChanged: false,
        };
    }

    renderExpansionAdd = () => {
        return <ExpansionSelector
            onChange={this.onExpansionChange}
            expansions={this.state.selectedExpansions}
            expansionList={getExpansionList()}
            onConfirm={this.confirmExpansions}
            title={t("Add Expansions")}
            allowEmptySelection={true}
        />
    }

    renderCustomExpansionAdd = () => {
        let list = Object.keys(this.props.allCustomExpansions);

        if(list.length === 0) {
            return null;
        }

        let config = {};
        for(let i = 0; i < list.length; i++) {
            config[list[i]] = this.props.allCustomExpansions[list[i]].name;
        }

        return <ExpansionSelector
            sectionTitle={t("Custom Expansions")}
            title={t("Add Expansions")}
            onChange={this.onCustomExpansionChange}
            expansions={this.state.selectedCustomExpansions}
            expansionList={list}
            expansionConfig={config}
            onConfirm={this.confirmCustomExpansions}
            allowEmptySelection={true}
        />
    }

    onCustomExpansionChange = (selected) => {
        this.setState({selectedCustomExpansions: selected, customExpansionsChanged: true});
    }

    confirmCustomExpansions = () => {
        let current = this.props.customExpansions;
        let chosen = this.state.selectedCustomExpansions;

        let toRemove = Object.keys(current).filter((item) => {
            return !chosen[item];
        });

        let toDowngrade = [];
        for(let id in chosen) {
            if(current[id] &&
                (current[id] === "all" && chosen[id] === "cards")
            ){
                toDowngrade.push(id);
            }
        }

        let message = t("Expansions added!");
        if(toRemove.length > 0) {
            message = t("Expansions modified!")
            this.props.removeCustomExpansionsFromSettlement(this.props.id, toRemove);
        }

        if(toDowngrade.length > 0) {
            message = t("Expansions modified!")
            this.props.downgradeCustomExpansionsInSettlement(this.props.id, toDowngrade);
        }

        this.props.addCustomExpansionsToSettlement(this.props.id, chosen);
        this.setState({customExpansionsChanged: false});
        SimpleToast.show(message);
    }

    onExpansionChange = (selectedExpansions) => {
        this.setState({selectedExpansions, expansionsChanged: true});
    }

    confirmExpansions = () => {
        let current = this.props.expansions;
        let chosen = this.state.selectedExpansions;
        delete chosen.core;

        let toRemove = Object.keys(current).filter((item) => {
            return !chosen[item] && item !== "core";
        });

        let toDowngrade = [];
        for(let id in chosen) {
            if(current[id] &&
                (current[id] === "all" && chosen[id] === "cards")
            ){
                toDowngrade.push(id);
            }
        }

        let message = t("Expansions added!");

        if(toRemove.length > 0) {
            this.props.removeExpansionsFromSettlement(this.props.id, toRemove);

            if(chosen.length === 0) {
                message = t("Expansions modified!");
            } else {
                message = t("Expansions removed!");
            }
        }

        if(toDowngrade.length > 0) {
            message = t("Expansions modified!")
            this.props.downgradeExpansionsInSettlement(this.props.id, toDowngrade);
        }

        if(Object.keys(chosen).length > 0) {
            this.props.addExpansions(this.props.id, chosen);
        }

        this.setState({selectedExpansions : {}, expansionsChanged: false});
        SimpleToast.show(message);
    }

    copySettlement = () => {
        let newId = Uuid.v4();
        this.props.copySettlement(this.props.id, newId);
        SimpleToast.show(t("Settlement Copied!"));
    }

    hideExpansions = () => {
        if(this.state.expansionsChanged || this.state.customExpansionsChanged) {
            const confirmFn = this.state.expansionsChanged ? this.confirmExpansions : this.confirmCustomExpansions;
            if(this.state.expansionsChanged && this.state.customExpansionsChanged) {
                confirmFn = this.confirmAllExpansions;
            }
            Alert.alert(t("Apply changes?"), t("You haven't applied your expansion selections to the settlement yet."), [
                {text: t("Discard"), onPress: this.hideExpansionPopup},
                {text: t("Apply"), onPress: confirmFn}
            ]);
        } else {
            this.hideExpansionPopup();
        }
    }

    confirmAllExpansions = () => {
        this.confirmExpansions();
        this.confirmCustomExpansions();
    }

    hideExpansionPopup = () => {
        this.setState({showExpansions: false});
    }

    showExpansions = () => {
        this.setState({showExpansions: true});
    }

    showNetworkWarning = () => {
        SimpleToast.show(t("You cannot change expansions while networked"));
    }

    render() {
        return (
            <View style={[sharedStyles.width90, sharedStyles.centered]}>
                <Text style={sharedStyles.title}>{t("Settlement Admin")}</Text>
                <Text style={sharedStyles.itemText}>{t("To delete or archive a settlement swipe it to the left on the settlement list")}</Text>
                <View style={sharedStyles.contentWrapper}>
                    <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%"}}>
                        <Button
                            title={t("Assign Expansions")}
                            onPress={this.showExpansions}
                            color={this.props.colors.HIGHLIGHT}
                            shape="rounded"
                            disabled={this.props.isNetworked}
                            onDisablePress={this.showNetworkWarning}
                        />
                    </View>
                    <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%", paddingBottom: "5%",}}>
                        <Button title={t("Copy Settlement")} onPress={this.copySettlement} color={this.props.colors.HIGHLIGHT} shape="rounded"/>
                    </View>
                </View>
                <Popup
                    visible={this.state.showExpansions}
                    onDismissed={this.hideExpansions}
                    title={t("Choose Expansions")}
                >
                        {this.renderExpansionAdd()}
                        {this.renderCustomExpansionAdd()}
                </Popup>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        archived: state.settlementReducer.settlements[props.id].archived,
        expansions: state.settlementReducer.settlements[props.id].expansions,
        allCustomExpansions: state.customExpansions,
        customExpansions: state.settlementReducer.settlements[props.id].customExpansions,
        colors: getColors(state),
        isNetworked: (state.networkReducer.server !== null || state.networkReducer.client !== null),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({deleteSettlement,
        changePage,
        toggleArchive,
        addExpansions,
        copySettlement,
        addCustomExpansionsToSettlement,
        removeCustomExpansionsFromSettlement,
        downgradeCustomExpansionsInSettlement,
        removeExpansionsFromSettlement,
        downgradeExpansionsInSettlement,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin);
