'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View
} from 'react-native';

import {settlementActionWrapper} from '../../actions/settlement';
import Text from '../shared/app-text';
import sharedStyles from '../../style/styles';
import { getNonSecretFightingArts } from '../../selectors/settlement';
import { getFightingArtConfig } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import ListItem from '../shared/ListItem';
import Popup from '../shared/popup';

let FightingArtConfig = null;

class InspirationalStatue extends Component {
    constructor(props) {
        super(props);

        FightingArtConfig = getFightingArtConfig();

        this.state = {
            showChooseStatue: false,
        }
    }

    render() {
        return (
            <View style={[sharedStyles.container, sharedStyles.centeredWrapper]}>
                <Text style={[sharedStyles.title, {paddingBottom: 8}]}>{t("Inspirational Statue")}</Text>
                <ListItem
                    title={(this.props.statue ? FightingArtConfig[this.props.statue].title : t("None"))}
                    onPress={this.showChooseStatue}
                    id=""
                    buttonText={t("Choose")}
                />
                <Popup title={t("Select Fighting Art")} visible={this.state.showChooseStatue} onDismissed={this.onChooseStatueDismissed}>
                    {this.getPicker()}
                </Popup>
            </View>
        );
    }

    showChooseStatue = () => {
        this.setState({showChooseStatue: true});
    }

    onChooseStatueDismissed = () => {
        this.setState({showChooseStatue: false});
    }

    setInspirationalStatue = (name) => {
        this.props.settlementActionWrapper('setInspirationalStatue', [this.props.id, name]);
        this.onChooseStatueDismissed();
    }

    getPicker = () => {
        let arts = this.props.allArts.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={FightingArtConfig[name].title}
                    id={name}
                    buttonText={t("Select")}
                    onPress={this.setInspirationalStatue}
                />
            );
        });

        return <View style={sharedStyles.centeredWrapper}>
            {arts}
        </View>
    }
}

function mapStateToProps(state, props) {
    return {
        allArts: getNonSecretFightingArts(state).sort(),
        innovations: state.settlementReducer.settlements[props.id].innovations,
        statue: state.settlementReducer.settlements[props.id].inspirationalStatue,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(InspirationalStatue);
