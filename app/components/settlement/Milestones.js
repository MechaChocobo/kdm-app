'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    StyleSheet,
} from 'react-native';

import { getColors } from '../../selectors/general';
import styles from '../../style/styles';
import { getMilestoneConfig } from '../../helpers/helpers';
import { settlementActionWrapper } from '../../actions/settlement';
import { getSettlementMilestones } from '../../selectors/milestones';
import { Condition } from '../GlobalMilestones';
import { milestoneActionWrapper } from '../../actions/milestones';
import AppText from '../shared/app-text';
import Hr from '../shared/Hr';

let MilestoneConfig = null;

class Milestones extends Component {
    constructor(props) {
        super(props);

        MilestoneConfig = getMilestoneConfig();
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.width90, styles.centered, {marginTop: 20}]}>
                    {this.renderMilestones()}
                </View>
                <View style={{height: 50}} />
            </View>
        );
    }

    renderMilestones = () => {
        let length = Object.keys(this.props.milestones).length;
        const builtIn = Object.keys(this.props.milestones).map((key, i) => {
            let conditions = Object.keys(this.props.milestones[key].conditions).map((condKey, j) => {
                return <View style={styles.container} key={key + j}>
                    <Condition
                        value={this.props.milestones[key].conditions[condKey].value}
                        isComplete={this.props.milestones[key].conditions[condKey].complete}
                        milestoneId={key}
                        id={condKey}
                        milestoneConfig={MilestoneConfig[key].conditions[condKey]}
                        onChange={this.onMilestoneChange}
                        colors={this.props.colors}
                        />
                </View>
            });

            return <View key={key}>
                <AppText style={styles.header}>{MilestoneConfig[key].title}</AppText>
                {conditions}
                {i !== length - 1 && <Hr />}
            </View>

});

        length = Object.keys(this.props.globalMilestones).length;
        let custom = Object.keys(this.props.globalMilestones).map((key, i) => {
            if(!MilestoneConfig[key]) {
                return null;
            }

            let conditions = Object.keys(this.props.globalMilestones[key].conditions).map((condKey, j) => {
                return <View style={styles.container} key={key + j}>
                    <Condition
                        value={this.props.globalMilestones[key].conditions[condKey].value}
                        isComplete={this.props.globalMilestones[key].conditions[condKey].complete}
                        milestoneId={key}
                        id={condKey}
                        milestoneConfig={MilestoneConfig[key].settlementConditions[condKey]}
                        onChange={this.onMilestoneChange}
                        colors={this.props.colors}
                        global={true}
                    />
                </View>
            });

            return <View key={key}>
                <AppText style={styles.header}>{MilestoneConfig[key].title}</AppText>
                {conditions}
                {i !== length - 1 && <Hr />}
            </View>;
        });

        return builtIn.concat([<Hr key='separator' />], custom);
    }

    onConditionValueChange = (milestoneId, id, value, target) => {
        this.props.milestoneActionWrapper("setSettlementConditionValue", [milestoneId, id, this.props.id, value, target]);
    }

    onMilestoneChange = (key, id, value, target) => {
        this.props.settlementActionWrapper("setMilestoneValue", [this.props.id, key, id, value, target]);
    }

}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
        milestones: state.settlementReducer.settlements[props.id].milestones,
        globalMilestones: getSettlementMilestones(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper, milestoneActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Milestones);
