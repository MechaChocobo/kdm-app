'use strict';

import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    StyleSheet,
    View,
    Alert,
    TouchableOpacity,
} from 'react-native';

import { getAvailableInnovations } from '../../helpers/innovations';
import { settlementActionWrapper } from '../../actions/settlement';
import Text from '../shared/app-text';
import Button from '../shared/button';
import sharedStyles from '../../style/styles';
import { getRandomInt, getInnovationConfig, sortIdArrayByConfigTitle } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundControl from '../shared/buttons/RoundControl';
import ListItem from '../shared/ListItem';
import Popup from '../shared/popup';
import PillListItem from '../shared/PillListItem';
import AppTextInput from '../shared/AppTextInput';
import InnovationSummary from './innovations/InnovationSummary';
import Icon from '../shared/Icon';

let InnovationConfig = null;

class Innovations extends PureComponent {
    constructor(props) {
        super(props);
        let drawNum = props.innovations.indexOf("symposium") !== -1 ? "4" : "2";

        this.state = {
            showAddInnovation: false,
            showAddInnovationDeck: false,
            showDeckDetails: false,
            drawNum,
            showInnovationDetails: false,
        };

        InnovationConfig = getInnovationConfig();
    }

    renderInnovations = (innovations) => {
        innovations.sort();
        return innovations.map((name, i) => {
            return <PillListItem
                key={i}
                title={t(InnovationConfig[name].title)}
                icon="close"
                id={name}
                onPress={this.confirmRemoveInnovation}
            >
                <TouchableOpacity onPress={this.showInnovationSummary(name)}>
                    <Icon name="format-list-bulleted" size={25} color={this.props.colors.WHITE}/>
                </TouchableOpacity>
            </PillListItem>
        });
    }

    showInnovationSummary = (name) => {
        return () => {
            this.setState({showInnovationDetails: name});
        }
    }

    confirmRemoveInnovation = (name) => {
        Alert.alert(t("Are you sure?"), t("Deleting an innovation will remove it from your settlement"), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.removeInnovation(name)}
        ]);
    }

    renderInnovationDeck = (deck) => {
        deck = deck.sort();
        return deck.map((name, i) => {
            return <PillListItem
                key={i}
                title={t(InnovationConfig[name].title)}
                id={name}
                icon="close"
                onPress={this.confirmRemoveInnovationFromDeck}
                onLongPress={this.addInnovation}
                longPressHint={t("Long press to add innovation to the settlement")}
            >
                <TouchableOpacity onPress={this.showInnovationSummary(name)}>
                    <Icon name="format-list-bulleted" size={25} color={this.props.colors.WHITE}/>
                </TouchableOpacity>
            </PillListItem>
        });
    }

    removeInnovation = (name) => {
        return () => {
            this.props.settlementActionWrapper('removeInnovation', [this.props.id, name]);
        }
    };

    addInnovationWrapper = (name) => {
        return () => {
            if(this.state.showDeckDetails === true) {
                return;
            }

            this.addInnovation(name);
        }
    }

    addInnovation = (name) => {
        if(name === "0") {
            return;
        }

        this.props.settlementActionWrapper('addInnovation', [this.props.id, name]);
        this.setState({showAddInnovation: false});
    }

    addInnovationToDeck = (name) => {
        if(name === "") {
            return;
        }

        this.props.settlementActionWrapper('addToInnovationDeck', [this.props.id, name]);
        this.setState({showAddInnovationDeck: false});
    }

    confirmRemoveInnovationFromDeck = (name) => {
        Alert.alert(t("Are you sure?"), t("Deleting an innovation will remove it from your settlement's innovation deck"), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.removeInnovationFromDeck(name)}
        ]);
    }

    removeInnovationFromDeck = (name) => {
        return () => {
            this.props.settlementActionWrapper('removeFromInnovationDeck', [this.props.id, name]);
        }
    }

    showAddInnovation = () => {
        this.setState({showAddInnovation: true});
    }

    onInnovationPopupClosed = () => {
        this.setState({showAddInnovation: false});
    }

    showAddInnovationDeck = () => {
        this.setState({showAddInnovationDeck: true});
    }

    onInnovationDeckPopupClosed = () => {
        this.setState({showAddInnovationDeck: false});
    }

    render() {
        return (
            <View style={[sharedStyles.container]}>
                <View style={[sharedStyles.rowCentered, sharedStyles.width90, sharedStyles.centered, {justifyContent: "space-between", marginBottom: 10, marginTop: 10}]}>
                    <Text style={sharedStyles.title}>{t("Innovations")} ({this.props.innovationCount})</Text>
                    <RoundControl icon="plus" onPress={this.showAddInnovation}/>
                </View>
                <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                    {this.renderInnovations(this.props.innovations)}
                </View>
                <View style={[sharedStyles.rowCentered, sharedStyles.width90, sharedStyles.centered, {justifyContent: "space-between", marginBottom: 10, marginTop: 20}]}>
                    <Text style={sharedStyles.title}>{t("Innovation Deck")} ({this.props.deckCount})</Text>
                    <RoundControl icon="plus" onPress={this.showAddInnovationDeck}/>
                </View>
                <View style={styles.innovationDeck}>
                    {this.renderInnovationDeck(this.props.innovationDeck)}
                </View>
                <View style={[sharedStyles.width90, sharedStyles.centered]}>
                    {this.getDrawControls()}
                </View>
                <Popup title="Select Innovation" visible={this.state.showAddInnovation} onDismissed={this.onInnovationPopupClosed}>
                    {this.getPicker()}
                </Popup>
                <Popup title="Select Innovation" visible={this.state.showAddInnovationDeck} onDismissed={this.onInnovationDeckPopupClosed}>
                    {this.getDeckPicker()}
                </Popup>
                {this.state.showInnovationDetails !== false && <Popup
                    onDismissed={this.hideInnovationDetails}
                    visible={this.state.showInnovationDetails !== false}
                >
                    <InnovationSummary
                        innovation={InnovationConfig[this.state.showInnovationDetails]}
                        innovations={this.props.innovations}
                        allInnovations={this.props.allInnovations}
                        innovationDeck={this.props.innovationDeck}
                    />
                </Popup>}
            </View>
        );
    }

    getPicker = () => {
        let availableinnovations = getAvailableInnovations(this.props.innovations, this.props.allInnovations);

        let innovations = availableinnovations.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={t(InnovationConfig[name].title)}
                    onPress={this.addInnovation}
                    id={name}
                    buttonText={t("Add")}
                />
            );
        });

        return innovations;
    }

    getDeckPicker = () => {
        let availableinnovations = getAvailableInnovations(this.props.innovations.concat(this.props.innovationDeck), this.props.allInnovations);

        let innovations = availableinnovations.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={t(InnovationConfig[name].title)}
                    onPress={this.addInnovationToDeck}
                    id={name}
                    buttonText={t("Add")}
                />
            );
        });

        return innovations;
    }

    getDrawControls = () => {
        return <View>
            <View style={{flexDirection: "row", padding: 5, justifyContent: "space-between"}}>
                <AppTextInput
                    style={{width: "16%", alignSelf: "flex-end"}}
                    keyboardType='numeric'
                    returnKeyType='done'
                    inputStyle={{fontSize: 20, fontWeight: "300"}}
                    onChangeText={this.numDrawChange}
                    value={this.state.drawNum}
                    textAlign="center"
                />
                <View style={{width: "80%"}}>
                    <Button
                        title={t("Draw %number% Innovations", {number: this.state.drawNum})}
                        onPress={this.drawInnovations}
                        color={this.props.colors.HIGHLIGHT}
                        disabled={this.state.drawNum === "" || this.state.drawNum > this.props.innovationDeck.length}
                        shape="rounded"
                    />
                </View>
            </View>
            {(Array.isArray(this.props.drawnInnovations) && this.props.drawnInnovations.length > 0) && <View style={{paddingLeft: 5}}>
                <View style={[sharedStyles.rowCentered, sharedStyles.spaceBetween, {marginTop: 10, marginBottom: 10}]}>
                    <Text style={sharedStyles.itemText}>{t("Drew the following Innovations")}:</Text>
                    <TouchableOpacity onPress={this.clearDrawnInnovations} style={{padding: 5}}>
                        <Icon name="close" color={this.props.colors.HIGHLIGHT} size={20} />
                    </TouchableOpacity>
                </View>
                {this.props.drawnInnovations.map((name, i) => {
                    return <ListItem
                        key={i}
                        title={t(InnovationConfig[name].title)}
                        onPress={this.addDrawnInnovation}
                        id={name}
                        buttonText={t("Select")}
                        preButton={<RoundControl
                            icon="format-list-bulleted"
                            style={{marginRight: 15}}
                            id={name}
                            onPress={this.showInnovationDetails}
                            gradient={false}
                            color={this.props.colors.WHITE}
                            iconColor={this.props.colors.HIGHLIGHT}
                        />}
                    />
                })}
            </View>}
        </View>
    }

    showInnovationDetails = (name) => {
        this.setState({showInnovationDetails: name});
    }

    hideInnovationDetails = () => {
        this.setState({showInnovationDetails: false});
    }

    addDrawnInnovation = (name) => {
        this.props.settlementActionWrapper('addInnovation', [this.props.id, name]);
        this.props.settlementActionWrapper("setDrawnInnovations", [this.props.id, []]);
    }

    clearDrawnInnovations = () => {
        this.props.settlementActionWrapper("setDrawnInnovations", [this.props.id, []]);
    }

    numDrawChange = (text) => {
        if(text.charAt(0) === '0') {
            text = text.substr(1);
        }

        if((isNaN(text) || !(text === parseInt(text, 10).toString())) && text !== "") {
            return;
        }

        this.setState({drawNum: text});
    }

    drawInnovations = () => {
        let innovations = [];

        while(innovations.length < this.state.drawNum) {
            let innovation = this.props.innovationDeck[getRandomInt(this.props.innovationDeck.length)];

            if(innovations.indexOf(innovation) === -1) {
                innovations.push(innovation);
            }
        }

        this.props.settlementActionWrapper("setDrawnInnovations", [this.props.id, sortIdArrayByConfigTitle(innovations, InnovationConfig)]);

        setTimeout(() => {
            this.props.scrollRef.scrollToEnd();
        }, 200);
    }
}

function mapStateToProps(state, props) {
    return {
        settlementName: state.settlementReducer.settlements[props.id].name,
        innovations: state.settlementReducer.settlements[props.id].innovations,
        allInnovations: state.settlementReducer.settlements[props.id].allInnovations,
        innovationDeck: state.settlementReducer.settlements[props.id].innovationDeck,
        drawnInnovations: state.settlementReducer.settlements[props.id].drawnInnovations,
        innovationCount: state.settlementReducer.settlements[props.id].innovations.length,
        deckCount: state.settlementReducer.settlements[props.id].innovationDeck.length,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Innovations);

var styles = StyleSheet.create({
    innovationDeck: {
        flexDirection: "row",
        flexWrap: "wrap",
    }
});
