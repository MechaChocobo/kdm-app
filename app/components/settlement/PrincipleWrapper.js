'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {settlementActionWrapper} from '../../actions/settlement';

import {
    View,
} from 'react-native';

import Popup from '../shared/popup';
import { t } from '../../helpers/intl';
import ListItem from '../shared/ListItem';

class PrincipleWrapper extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPopup: false,
        }
    }

    selectPrinciple = (name) => {
        let statType = "increase";
        let stats = null;
        if(this.props.config[name].allSurvivors) {
            stats = this.props.config[name].allSurvivors;
        } else if(this.props.principle && this.props.config[this.props.principle].allSurvivors && name !== this.props.principle) {
            statType = "decrease";
            stats = this.props.config[this.props.principle].allSurvivors;
        }

        this.props.settlementActionWrapper('setPrinciple', [this.props.id, this.props.principleType, name, statType, stats]);
        this.setState({showPopup: false});
    }

    unsetPrinciple = () => {
        let statType = "decrease";
        let stats = null;
        if(this.props.config[this.props.principle] && this.props.config[this.props.principle].allSurvivors) {
            stats = this.props.config[this.props.principle].allSurvivors;
        }

        this.props.settlementActionWrapper('unsetPrinciple', [this.props.id, this.props.principleType, statType, stats]);
        this.setState({showPopup: false});
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    onPopupDismissed = () => {
        this.setState({showPopup: false});
    }

    getPrinciples = (config) => {
        let principles = Object.keys(this.props.config);
        let items = [];

        if(this.props.principle !== false) {
            items.push(<ListItem
                key={-1}
                title={t("None")}
                id={""}
                onPress={this.unsetPrinciple}
                buttonText={t("Choose")}
                confirm={false}
            />);
        }

        principles.map((item, i) => {
            if(this.props.principle !== item) {
                items.push(<ListItem
                    key={i}
                    title={config[item].title}
                    id={item}
                    onPress={this.selectPrinciple}
                    buttonText={t("Choose")}
                    confirm={false}
                />);
            }
        });

        return items;
    }

    render() {
        return (
            <View>
                <ListItem
                    title={this.props.principle ? this.props.config[this.props.principle].title : t("None")}
                    subTitle={this.props.principleTitle}
                    buttonText={t("Choose")}
                    onPress={this.showPopup}
                    confirm={false}
                    id=""
                />
                <Popup
                    visible={this.state.showPopup}
                    onDismissed={this.onPopupDismissed}
                    title={t("Select Principle")}
                >
                    {this.getPrinciples(this.props.config)}
                </Popup>
            </View>
        );
    }
}

PrincipleWrapper.propTypes = {
    principle: PropTypes.any.isRequired,
    config: PropTypes.object.isRequired,
    principleTitle: PropTypes.string.isRequired,
    principleType: PropTypes.string.isRequired,
}

PrincipleWrapper.defaultProps = {

}

function mapStateToProps(state, props) {
    return {
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PrincipleWrapper);
