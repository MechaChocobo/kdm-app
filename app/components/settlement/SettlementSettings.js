'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    ScrollView,
    View
} from 'react-native';

import { getColors } from '../../selectors/general';
import styles from '../../style/styles';
import { getSettlementId } from '../../selectors/settlement';
import BGBanner from '../shared/BGBanner';
import AppText from '../shared/app-text';
import ContentToggles from './toggles/ContentToggles';

class SettlementSettings extends Component {
    render() {
        return (
            <ScrollView>
                <BGBanner>
                    <AppText style={styles.pageHeader}>{this.props.name}</AppText>
                </BGBanner>
                <View style={{height: 10}}/>
                <ContentToggles />
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    const settlementId = getSettlementId(state);
    return {
        colors: getColors(state),
        name: state.settlementReducer.settlements[settlementId].name,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementSettings);
