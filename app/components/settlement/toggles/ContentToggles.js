'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Collapsible from 'react-native-collapsible';

import {
    View,
    TouchableOpacity,
    Dimensions,
} from 'react-native';

import { t } from '../../../helpers/intl';
import { getColors } from '../../../selectors/general';
import styles from '../../../style/styles';
import { getSettlementId, getSettlementFightingArtsByExpansion, getSettlementInnovationsByExpansion, getSettlementDisordersByExpansion, getSettlementEventsByExpansion, getSettlementLocationsByExpansion, getWeaponProficienciesByExpansion, getSettlementGearByExpansion, getSettlementResourcesByExpansion, getSettlementAbilitiesByExpansion, getSettlementArmorSetsByExpansion } from '../../../selectors/settlement';
import AppText from '../../shared/app-text';
import { getFightingArtConfig, getExpansionTitle, sortObjectArrayByTitle, getInnovationConfig, getDisorderConfig, getSettlementEventConfig, getLocationConfig, getWeaponProfConfig, getGearConfig, getResourceConfig, getAbilityConfig, getArmorConfig } from '../../../helpers/helpers';
import Popup from '../../shared/popup';
import Button from '../../shared/button';
import { settlementActionWrapper } from '../../../actions/settlement';
import GView from '../../shared/GView';
import Icon from '../../shared/Icon';
import Hr from '../../shared/Hr';
import ToggleListItem from '../../shared/ToggleListItem';
import StorageToggle from './StorageToggle';

let FAConfig = null;
let InnovationConfig = null;
let DisorderConfig = null;
let SEConfig = null;
let LocationConfig = null;
let ProfConfig = null;
let GearConfig = null;
let ResourceConfig = null;
let AbilityConfig = null;
let ArmorConfig = null;

const FIGHTING_ARTS = "fighting-arts";
const INNOVATIONS = "innovations";
const DISORDERS = "disorders";
const SETTLEMENT_EVENTS = "settlement-events";
const LOCATIONS = "locations";
const WEAPON_PROFS = "weapon-profs";
const ABILITIES = "abilities";
const ARMOR = "armor";

const typeFnMap = {
    'fighting-arts': {
        add: 'addFightingArtToAll',
        remove: 'removeFightingArtFromAll',
    },
    'innovations': {
        add: 'addInnovationToAll',
        remove: 'removeInnovationFromAll',
    },
    'disorders': {
        add: 'addDisorderToAll',
        remove: 'removeDisorderFromAll',
    },
    'settlement-events': {
        add: 'addSettlementEventToAll',
        remove: 'removeSettlementEventFromAll',
    },
    'locations': {
        add: 'addSettlementLocationToAll',
        remove: 'removeSettlementLocationFromAll',
    },
    'weapon-profs': {
        add: 'addSettlementWeaponProficiencyToAll',
        remove: 'removeSettlementWeaponProficiencyFromAll',
    },
    'abilities': {
        add: 'addAbilityToAll',
        remove: 'removeAbilityFromAll',
    },
    'armor': {
        add: 'addSettlementArmorSetToAll',
        remove: 'removeSettlementArmorSetFromAll',
    },
}

const PopupTitles = {
    "fighting-arts" : "Select fighting arts",
    "innovations" : "Select innovations",
    "disorders" : "Select disorders",
    "settlement-events" : "Select settlement events",
    "locations" : "Select locations",
    "weapon-profs" : "Select proficiencies",
    "abilities" : "Select abilities",
    "armor" : "Select armor sets",
}

class ContentToggles extends Component {
    constructor(props) {
        super(props);

        FAConfig         = getFightingArtConfig();
        InnovationConfig = getInnovationConfig();
        DisorderConfig   = getDisorderConfig();
        SEConfig         = getSettlementEventConfig();
        LocationConfig   = getLocationConfig();
        ProfConfig       = getWeaponProfConfig();
        GearConfig       = getGearConfig();
        ResourceConfig   = getResourceConfig();
        AbilityConfig    = getAbilityConfig();
        ArmorConfig      = getArmorConfig();

        this.state = {
            popup: false,
            openedSections: {},
            width: Dimensions.get('window').width,
        }
    }

    render() {
        let buttonSize = Math.floor(this.state.width / 4);
        return (
            <View style={[styles.width90, styles.centered]}>
                <AppText style={styles.title}>{t("Edit Available Content")}</AppText>
                <Hr />
                <View style={[styles.rowCentered, styles.spaceBetween, styles.marginBottom20]}>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeInnovations} shape="rounded" skeleton={true}>
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="sk-lightbulb" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Innovations")}</AppText>
                        </View>
                    </Button>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeLocations} shape="rounded" skeleton={true} >
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="sk-house" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Locations")}</AppText>
                        </View>
                    </Button>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeSettlementEvents} shape="rounded" skeleton={true} >
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="cards-outline" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Settlement Events")}</AppText>
                        </View>
                    </Button>
                </View>
                <View style={[styles.rowCentered, styles.spaceBetween, styles.marginBottom20]}>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeFightingarts} shape="rounded" skeleton={true}>
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="sk-fist" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Fighting Arts")}</AppText>
                        </View>
                    </Button>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeDisorders} shape="rounded" skeleton={true}>
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="brain" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Disorders")}</AppText>
                        </View>
                    </Button>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeAbilities} shape="rounded" skeleton={true}>
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="sk-cartwheel" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Abilities")}</AppText>
                        </View>
                    </Button>
                </View>
                <View style={[styles.rowCentered, styles.spaceBetween, styles.marginBottom20]}>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeWeaponProfs} shape="rounded" skeleton={true} >
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="sword" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Weapon Profs")}</AppText>
                        </View>
                    </Button>
                    <StorageToggle
                        buttonSize={buttonSize}
                        buttonTitle={t("Gear")}
                        popupTitle={t("Select Gear")}
                        ownedItems={this.props.gear}
                        ownedItemsByExpansion={this.props.gearByExpansion}
                        storageConfig={GearConfig}
                        customExpansions={this.props.customExpansions}
                        colors={this.props.colors}
                        onToggle={this.toggleGear}
                        icon="tshirt-crew-outline"
                    />
                    <StorageToggle
                        buttonSize={buttonSize}
                        buttonTitle={t("Resources")}
                        popupTitle={t("Select Resources")}
                        ownedItems={this.props.resources}
                        ownedItemsByExpansion={this.props.resourcesByExpansion}
                        storageConfig={ResourceConfig}
                        customExpansions={this.props.customExpansions}
                        colors={this.props.colors}
                        onToggle={this.toggleResource}
                        icon="sk-heart"
                    />
                </View>
                <View style={[styles.rowCentered, styles.spaceBetween, styles.marginBottom20]}>
                    <Button style={{height: buttonSize, width: buttonSize}} onPress={this.changeArmorSets} shape="rounded" skeleton={true}>
                        <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                            <Icon name="sk-helmet" color={this.props.colors.TEXT} size={30}/>
                            <AppText style={{textAlign: "center"}}>{t("Armor Sets")}</AppText>
                        </View>
                    </Button>
                    <View style={{height: buttonSize, width: buttonSize}} />
                    <View style={{height: buttonSize, width: buttonSize}} />
                </View>
                <Popup
                    visible={this.state.popup !== false}
                    onDismissed={this.hidePopup}
                    title={this.state.popup ? t(PopupTitles[this.state.popup]) : ''}
                >
                    {this.getPopupContents()}
                </Popup>
            </View>
        );
    }

    changeLocations = () => {
        this.setState({popup: LOCATIONS});
    }

    changeFightingarts = () => {
        this.setState({popup: FIGHTING_ARTS});
    }

    changeInnovations = () => {
        this.setState({popup: INNOVATIONS});
    }

    changeDisorders = () => {
        this.setState({popup: DISORDERS});
    }

    changeSettlementEvents = () => {
        this.setState({popup: SETTLEMENT_EVENTS});
    }

    changeWeaponProfs = () => {
        this.setState({popup: WEAPON_PROFS});
    }

    changeAbilities = () => {
        this.setState({popup: ABILITIES});
    }

    changeArmorSets = () => {
        this.setState({popup: ARMOR});
    }

    getPopupContents = () => {
        if(this.state.popup === false) {
            // popup complains if you pass it null as children
            return <View />;
        }

        let allItems = [];
        let config = null;
        let checkedItems = null;
        let checkedByExpansion = {};

        if(this.state.popup === FIGHTING_ARTS) {
            config = FAConfig;
            checkedItems = this.props.fightingArts;
            checkedByExpansion = this.props.fightingArtsByExpansion;
        } else if(this.state.popup === INNOVATIONS) {
            config = InnovationConfig;
            checkedItems = this.props.innovations;
            checkedByExpansion = this.props.innovationsByExpansion;
        } else if(this.state.popup === DISORDERS) {
            config = DisorderConfig;
            checkedItems = this.props.disorders;
            checkedByExpansion = this.props.disordersByExpansion;
        } else if(this.state.popup === SETTLEMENT_EVENTS) {
            config = SEConfig;
            checkedItems = this.props.settlementEvents;
            checkedByExpansion = this.props.settlementEventsByExpansion;
        } else if(this.state.popup === LOCATIONS) {
            config = LocationConfig;
            checkedItems = this.props.locations;
            checkedByExpansion = this.props.locationsByExpansion;
        } else if(this.state.popup === WEAPON_PROFS) {
            config = ProfConfig;
            checkedItems = this.props.proficiencies;
            checkedByExpansion = this.props.proficienciesByExpansion;
        } else if(this.state.popup === ABILITIES) {
            config = AbilityConfig;
            checkedItems = this.props.abilities;
            checkedByExpansion = this.props.abilitiesByExpansion;
        } else if(this.state.popup === ARMOR) {
            config = ArmorConfig;
            checkedItems = this.props.armorSets;
            checkedByExpansion = this.props.armorSetsByExpansion;
        }

        allItems = this.getItemsByExpansion(config);

        let sortedExpansions = Object.keys(allItems).sort((a, b) => {
            a = getExpansionTitle(a, this.props.customExpansions);
            b = getExpansionTitle(b, this.props.customExpansions);

            if(a > b) {
                return 1;
            } else if(a < b) {
                return -1;
            } else {
                return 0;
            }
        });

        let sections = [];

        for(let i = 0; i < sortedExpansions.length; i++) {
            const expansion = sortedExpansions[i];
            let numSelected = checkedByExpansion[expansion] ? checkedByExpansion[expansion].length : 0;
            sections.push(<View key={i}>
                <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', marginBottom: 5}]} onPress={this.toggleCollapsed(expansion)}>
                    <AppText style={styles.title}>{getExpansionTitle(expansion, this.props.customExpansions)}</AppText>
                    <View style={styles.rowCentered}>
                        <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 13, justifyContent: "center", alignItems: "center"}}>
                            <AppText style={styles.title} color={this.props.colors.WHITE}>{numSelected}/{allItems[expansion].length}</AppText>
                        </GView>
                        <Icon name={this.state.openedSections[expansion] === true ? "chevron-down" : "chevron-right"} color={this.props.colors.HIGHLIGHT} size={40}/>
                    </View>
                </TouchableOpacity>
                <Collapsible collapsed={this.state.openedSections[expansion] !== true}>
                    {allItems[expansion].sort(sortObjectArrayByTitle).map((item, i) => {
                        return <ToggleListItem
                            key={i}
                            id={item.name}
                            title={config[item.name].title}
                            value={checkedItems.indexOf(item.name) !== -1}
                            onToggle={this.toggleItem(config)}
                        />
                    })}
                </Collapsible>
            </View>);
        }

        return sections;
    }

    toggleCollapsed = (name) => {
        return () => {
            let openedSections = {...this.state.openedSections};

            if(openedSections[name]) {
                delete openedSections[name];
            } else {
                openedSections[name] = true;
            }

            this.setState({openedSections});
        }
    }

    toggleItem = (config) => {
        return (id) => {
            if(!typeFnMap[this.state.popup]) {
                return;
            }

            let checkedItems = [];
            if(this.state.popup === FIGHTING_ARTS) {
                checkedItems = this.props.fightingArts;
            } else if(this.state.popup === INNOVATIONS) {
                checkedItems = this.props.innovations;
            } else if(this.state.popup === DISORDERS) {
                checkedItems = this.props.disorders;
            } else if(this.state.popup === SETTLEMENT_EVENTS) {
                checkedItems = this.props.settlementEvents;
            } else if(this.state.popup === LOCATIONS) {
                checkedItems = this.props.locations;
            } else if(this.state.popup === WEAPON_PROFS) {
                checkedItems = this.props.proficiencies;
            } else if(this.state.popup === ABILITIES) {
                checkedItems = this.props.abilities;
            } else if(this.state.popup === ARMOR) {
                checkedItems = this.props.armorSets;
            }

            let fn = checkedItems.indexOf(id) > -1 ? typeFnMap[this.state.popup].remove : typeFnMap[this.state.popup].add;
            this.props.settlementActionWrapper("contentToggleWrapper", [fn, this.props.settlementId, id, config, checkedItems.indexOf(id) > -1]);
        }
    }

    toggleGear = (location, gearId) => {
        if(this.props.gear[location] && this.props.gear[location][gearId] !== undefined) {
            this.props.settlementActionWrapper("storageContentToggleWrapper", ["removeSettlementGearFromAll", this.props.settlementId, location, gearId, GearConfig, true]);
        } else {
            this.props.settlementActionWrapper("storageContentToggleWrapper", ["addSettlementGearToAll", this.props.settlementId, location, gearId, GearConfig]);
        }
    }

    toggleResource = (location, resourceId) => {
        if(this.props.resources[location] && this.props.resources[location][resourceId] !== undefined) {
            this.props.settlementActionWrapper("storageContentToggleWrapper", ["removeSettlementResourceFromAll", this.props.settlementId, location, resourceId, ResourceConfig, true]);
        } else {
            this.props.settlementActionWrapper("storageContentToggleWrapper", ["addSettlementResourceToAll", this.props.settlementId, location, resourceId, ResourceConfig]);
        }
    }

    getItemsByExpansion = (config) => {
        let formatted = {};

        const items = Object.values(config);

        for(let i = 0; i < items.length; i++) {
            let expansion = items[i].expansion ? items[i].expansion : "core";

            if(!formatted[expansion]) {
                formatted[expansion] = [];
            }

            formatted[expansion].push(items[i]);
        }

        return formatted;
    }

    hidePopup = () => {
        this.setState({popup: false, openedSections: {}});
    }
}

function mapStateToProps(state, props) {
    const settlementId = getSettlementId(state);
    return {
        colors: getColors(state),
        settlementId,
        name: state.settlementReducer.settlements[settlementId].name,
        fightingArts: state.settlementReducer.settlements[settlementId].allFightingArts,
        fightingArtsByExpansion: getSettlementFightingArtsByExpansion(state),
        innovations: state.settlementReducer.settlements[settlementId].allInnovations,
        innovationsByExpansion: getSettlementInnovationsByExpansion(state),
        disorders: state.settlementReducer.settlements[settlementId].allDisorders,
        disordersByExpansion: getSettlementDisordersByExpansion(state),
        settlementEvents: state.settlementReducer.settlements[settlementId].allSettlementEvents,
        settlementEventsByExpansion: getSettlementEventsByExpansion(state),
        locations: state.settlementReducer.settlements[settlementId].allLocations,
        locationsByExpansion: getSettlementLocationsByExpansion(state),
        proficiencies: state.settlementReducer.settlements[settlementId].allProficiencies,
        proficienciesByExpansion: getWeaponProficienciesByExpansion(state),
        gear: state.settlementReducer.settlements[settlementId].storage.gear,
        gearByExpansion: getSettlementGearByExpansion(state),
        resources: state.settlementReducer.settlements[settlementId].storage.resources,
        resourcesByExpansion: getSettlementResourcesByExpansion(state),
        abilities: state.settlementReducer.settlements[settlementId].allAbilities,
        abilitiesByExpansion: getSettlementAbilitiesByExpansion(state),
        armorSets: state.settlementReducer.settlements[settlementId].allArmorSets,
        armorSetsByExpansion: getSettlementArmorSetsByExpansion(state),
        customExpansions: state.customExpansions,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ContentToggles);
