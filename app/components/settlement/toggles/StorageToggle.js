'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';

import {
    View,
    TouchableOpacity,
} from 'react-native';
import Button from '../../shared/button';
import Popup from '../../shared/popup';
import { getExpansionTitle, sortIdArrayByConfigTitle, getLocationConfig, sortObjectArrayByTitle } from '../../../helpers/helpers';
import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import GView from '../../shared/GView';
import Icon from '../../shared/Icon';
import ToggleListItem from '../../shared/ToggleListItem';

let LocationConfig = null;

export default class StorageToggle extends PureComponent {
    state = {
        showPopup: false,
        openExpansions: {},
        openExpansionLocations: {},
    };

    constructor(props) {
        super(props);

        LocationConfig = getLocationConfig();
    }

    render() {
        return (
            <View>
                <Button style={{height: this.props.buttonSize, width: this.props.buttonSize}} onPress={this.showPopup} shape="rounded" skeleton={true} >
                    <View style={[styles.justifyCentered, styles.centeredItems, {padding: 10}]}>
                        <Icon name={this.props.icon} color={this.props.colors.TEXT} size={30}/>
                        <AppText style={{textAlign: "center"}}>{this.props.buttonTitle}</AppText>
                    </View>
                </Button>
                {this.state.showPopup && <Popup
                    title={this.props.popupTitle}
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                >
                    {this.renderPopupContents()}
                </Popup>}
            </View>
        );
    }

    renderPopupContents = () => {
        const allItems = this.formatConfigByExpansion();
        const sortedExpansions = Object.keys(allItems).sort((a, b) => {
            a = getExpansionTitle(a, this.props.customExpansions);
            b = getExpansionTitle(b, this.props.customExpansions);

            if(a > b) {
                return 1;
            } else if(a < b) {
                return -1;
            } else {
                return 0;
            }
        });

        let sections = [];

        for(let i = 0; i < sortedExpansions.length; i++) {
            const expansion = sortedExpansions[i];
            sections.push(<View key={i}>
                <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', marginBottom: 5}]} onPress={this.toggleCollapsed(expansion)}>
                    <AppText style={styles.title}>{getExpansionTitle(expansion, this.props.customExpansions)}</AppText>
                    <View style={styles.rowCentered}>
                        <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 13, justifyContent: "center", alignItems: "center"}}>
                            <AppText style={styles.title} color={this.props.colors.WHITE}>{this.getNumberOfItemsInExpansion(this.props.ownedItemsByExpansion, expansion)}/{this.getNumberOfItemsInExpansion(allItems, expansion)}</AppText>
                        </GView>
                        <Icon name={this.state.openExpansions[expansion] === true ? "chevron-down" : "chevron-right"} color={this.props.colors.HIGHLIGHT} size={40}/>
                    </View>
                </TouchableOpacity>
                {<Collapsible collapsed={this.state.openExpansions[expansion] !== true}>
                    {this.getLocationSections(allItems, expansion)}
                </Collapsible>}
            </View>);
        }

        return sections;
    }

    getLocationSections = (allItems, expansion) => {
        if(!this.state.openExpansions[expansion]) {
            return null;
        }

        const sortedLocations = sortIdArrayByConfigTitle(Object.keys(allItems[expansion]), LocationConfig);

        return sortedLocations.map((location, i) => {
                const items = (!this.state.openExpansionLocations[expansion] || this.state.openExpansionLocations[expansion][location] !== true) ? null : Object.values(allItems[expansion][location]).sort(sortObjectArrayByTitle).map((item, i) => {
                    return <ToggleListItem
                        key={i}
                        id={location + ":" + item.id}
                        title={this.props.storageConfig[location][item.id].title}
                        value={this.props.ownedItemsByExpansion[expansion] && this.props.ownedItemsByExpansion[expansion][location] && this.props.ownedItemsByExpansion[expansion][location][item.id] !== undefined}
                        onToggle={this.toggleItem}
                    />
                });

                return <View key={i} style={[styles.width90, styles.centered]}>
                <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', marginBottom: 5}]} onPress={this.toggleCollapsedLocation(expansion, location)}>
                    <AppText style={{fontSize: 18, fontWeight: "bold"}}>{LocationConfig[location] ? LocationConfig[location].title : location}</AppText>
                    <View style={styles.rowCentered}>
                        <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 8, justifyContent: "center", alignItems: "center", marginBottom: 4}}>
                            <AppText style={{fontSize: 18, fontWeight: "bold"}} color={this.props.colors.WHITE}>{this.getNumberOfItemsInLocation(this.props.ownedItemsByExpansion[expansion], location)}/{this.getNumberOfItemsInLocation(allItems[expansion], location)}</AppText>
                        </GView>
                        <Icon name={(this.state.openExpansionLocations[expansion] && this.state.openExpansionLocations[expansion][location] === true) ? "chevron-down" : "chevron-right"} color={this.props.colors.HIGHLIGHT} size={30}/>
                    </View>
                </TouchableOpacity>
                <Collapsible collapsed={(this.state.openExpansionLocations[expansion] && this.state.openExpansionLocations[expansion][location] !== true)}>
                    {items}
                </Collapsible>
            </View>
        });
    }

    toggleItem = (name) => {
        name = name.split(":");

        const location = name[0];
        const id = name[1];

        this.props.onToggle(location, id);
    }

    getNumberOfItemsInExpansion = (items, expansion) => {
        let count = 0;

        if(items[expansion]) {
            for(let location in items[expansion]) {
                count += Object.keys(items[expansion][location]).length;
            }
        }

        return count;
    }

    getNumberOfItemsInLocation = (items, location) => {
        let count = 0;

        if(items && items[location]) {
            count += Object.keys(items[location]).length;
        }

        return count;
    }

    toggleCollapsed = (expansion) => {
        return () => {
            let open = {...this.state.openExpansions};
            let openLocations = {...this.state.openExpansionLocations};
            if(open[expansion]) {
                delete open[expansion];
                delete openLocations[expansion];
            } else {
                open[expansion] = true;
                openLocations[expansion] = {};
            }
            this.setState({openExpansions: open, openExpansionLocations: openLocations});
        }
    }

    toggleCollapsedLocation = (expansion, location) => {
        return () => {
            let open = {...this.state.openExpansionLocations};
            if(open[expansion][location]) {
                delete open[expansion][location];
            } else {
                open[expansion][location] = true;
            }
            this.setState({openExpansionLocations: open});
        }
    }

    formatConfigByExpansion = () => {
        let formatted = {};

        for(let location in this.props.storageConfig) {
            for(let key in this.props.storageConfig[location]) {
                const expansion = this.props.storageConfig[location][key].expansion ? this.props.storageConfig[location][key].expansion : "core";
                if(!formatted[expansion]) {
                    formatted[expansion] = {};
                }

                if(!formatted[expansion][location]) {
                    formatted[expansion][location] = {};
                }

                formatted[expansion][location][key] = this.props.storageConfig[location][key];
            }
        }

        return formatted;
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }
}

StorageToggle.propTypes = {
    buttonSize: PropTypes.number.isRequired,
    buttonTitle: PropTypes.string.isRequired,
    popupTitle: PropTypes.string.isRequired,
    ownedItems: PropTypes.object.isRequired,
    ownedItemsByExpansion: PropTypes.object.isRequired,
    storageConfig: PropTypes.object.isRequired,
    customExpansions: PropTypes.object.isRequired,
    colors: PropTypes.object.isRequired,
}
