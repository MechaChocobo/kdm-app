'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {settlementActionWrapper} from '../../actions/settlement';
import {getCurrentYearEndeavors} from '../../selectors/endeavors';

import {
    View,
    Image,
    FlatList,
    Modal,
    Dimensions,
} from 'react-native';

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';
import { getInnovationConfig, getLocationConfig, getSettlementEventConfig, getWidthFromPercentage } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getEndeavorList } from '../../helpers/helpers';
import { getColors } from '../../selectors/general';
import GView from '../shared/GView';
import Button from '../shared/button';
import RoundControl from '../shared/buttons/RoundControl';
import Hr from '../shared/Hr';
import Endeavor from './endeavors/Endeavor';
import EndeavorList from './endeavors/EndeavorList';

class Endeavors extends Component {
    numEndeavors = 0;
    smallScroll = null;
    fullScroll = null;
    offset = 0;

    constructor(props) {
        super(props);
        this.state = { showDetails: false, numEndeavors: 0, popupListHeight: 0 };
    }

    render() {
        let endeavor = this.getFirstEndeavor();
        return (
            <View style={sharedStyles.container}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <View style={{flex: .33, alignItems: "center"}}>
                        <Text style={sharedStyles.title}>{t("Endeavors")}</Text>
                    </View>
                    <View style={{flex: .33, alignItems: "center"}}>
                        <View style={sharedStyles.rowCentered}>
                            <Text style={sharedStyles.largeValue}>{this.props.endeavors}</Text>
                            <View style={{width: 7}}/>
                            <Image style={{width: 30, height: 30}} source={require('../../images/endeavor.png')} />
                        </View>
                    </View>
                    <View style={{flex: .33, alignItems: "center"}}>
                        <View style={sharedStyles.rowCentered}>
                            <RoundControl onPress={this.decreaseEndeavors} icon="minus" />
                            <View style={{width: 10}}/>
                            <RoundControl onPress={this.increaseEndeavors} icon="plus" />
                        </View>
                    </View>
                </View>
                {endeavor && <Hr style={{width: "90%", height: 15}} slim={true}/>}
                {endeavor && <View style={{height: 15}}/>}
                {endeavor && this.renderEndeavors(this.getEndeavorList())}
                <Modal
                    visible={this.state.showDetails}
                    onRequestClose={this.onModalDismiss}
                    transparent={true}
                    animationType="fade"
                >
                    <View style={{justifyContent: "center", flex: 1, backgroundColor: "rgba(0, 0, 0, .8)"}}>
                        {this.renderEndeavors(this.getEndeavorList(), true)}
                    </View>
                </Modal>
            </View>
        );
    }

    onModalDismiss = () => {
        this.setState({showDetails : false});
    }

    toggleDetail = () => {
        if(this.state.showDetails) {
            this.smallScroll.scrollToOffset({ offset: this.offset, animated: false });
        }

        this.setState({showDetails: !this.state.showDetails});
    }

    increaseEndeavors = () => {
        this.props.settlementActionWrapper('setEndeavors', [this.props.id, this.props.endeavors + 1]);
    }

    decreaseEndeavors = () => {
        if(this.props.endeavors > 0) {
            this.props.settlementActionWrapper('setEndeavors', [this.props.id, this.props.endeavors - 1]);
        }
    }

    getEndeavorList = () => {
        let list = getEndeavorList(this.props.locations, this.props.innovations, this.props.globalEndeavors);
        list = this.props.currentYearEndeavors.concat(list);
        return list;
    }

    renderEndeavors = (endeavors, details = false) => {
        // this is a really awkward filter
        for (let i = 0; i < endeavors.length; i++) {
            endeavors[i].endeavors = endeavors[i].endeavors.filter((endeavor) => {
                let keep = true;
                if(endeavor.title === "Build" && this.props.locations.indexOf(endeavor.description) !== -1) {
                    keep = false;
                }

                if(endeavor.title === "Special Innovate" && this.props.innovations.indexOf(endeavor.description) !== -1) {
                    keep = false;
                }

                return (keep);
            });
        }

        endeavors = endeavors.filter((item) => {
            return item.endeavors.length > 0;
        });

        this.numEndeavors = endeavors.length;

        if(details) {
            return this.getFullList(endeavors);
        } else {
            return this.getSmallList(endeavors);
        }
    }

    getItemLayout = (data, index) => {
        return {
            length: getWidthFromPercentage(.85),
            offset: getWidthFromPercentage(.85) * index,
            index,
        }
    }

    getFullList = (endeavors) => {
        return <View>
            <FlatList
                ref={(ref) => {this.fullScroll = ref}}
                data={endeavors}
                renderItem={this.renderEndeavorFull}
                keyExtractor={(item, index) => ''+index}
                horizontal={true}
                extraData={this.props}
                onScroll={this.handleScroll}
                onLayout={this.setFullScroll}
                getItemLayout={this.getItemLayout}
            />
        </View>
    }

    getSmallList = (endeavors) => {
        return <FlatList
            ref={(ref) => {this.smallScroll = ref}}
            data={endeavors}
            renderItem={this.renderEndeavorSmall}
            keyExtractor={(item, index) => ''+index}
            horizontal={true}
            extraData={this.props}
            onScroll={this.handleScroll}
            getItemLayout={this.getItemLayout}
        />
    }

    setFullScroll = (e) => {
        this.fullScroll.scrollToOffset({ offset: this.offset, animated: false });
    }

    handleScroll = (e) => {
        this.offset = e.nativeEvent.contentOffset.x;
    }

    getFirstEndeavor = () => {
        return this.getEndeavorList()[0];
    }

    renderEndeavorSmall = (item) => {
        return this.renderEndeavor(item, this.props.currentYearSettlementEvents, this.props.colors.HIGHLIGHT);
    }

    renderEndeavorFull = (item) => {
        return this.renderEndeavor(item, this.props.currentYearSettlementEvents, this.props.colors.HIGHLIGHT, true);
    }

    getEndeavorTypeTitle = (type) => {
        if(type === "location") {
            return t("Settlement Location");
        } else if(type === "settlementEvent") {
            return t("Settlement Event");
        } else if(type === "survivorAbility") {
            return t("Survivor Ability");
        }
    }

    renderEndeavor(item, settlementEvents, color, details = false) {
        let index = item.index;
        item = item.item;
        let tags = "";
        let disabled = "";
        if(item.tags && Array.isArray(item.tags)) {
            tags = item.tags.join(', ');
            if(settlementEvents.length) {
                let EventConfig = getSettlementEventConfig();
                for (let i = 0; i < settlementEvents.length; i++) {
                    let event = EventConfig[settlementEvents[i]];
                    if(event.settlementEffects && event.settlementEffects.preventsEndeavors) {
                        for (let j = 0; j < event.settlementEffects.preventsEndeavors.length; j++) {
                            if(item.tags.indexOf(event.settlementEffects.preventsEndeavors[j]) !== -1) {
                                disabled = t("Endeavor disabled due to %event%", {event: event.title});
                                break;
                            }
                        }
                    }

                    if(disabled !== "") {
                        break;
                    }
                }
            }
        }

        if(tags === "") {
            tags = this.getEndeavorTypeTitle(item.type);
        }

        const maxHeight = Dimensions.get("window").height * .9;

        let containerStyle = {
            width: getWidthFromPercentage(.8),
            borderRadius: 5,
            marginLeft: getWidthFromPercentage(.05),
            marginRight: getWidthFromPercentage(.05),
            maxHeight,
        };

        if(index > 0) {
            containerStyle.marginLeft = 0;
        }

        if(index === this.numEndeavors - 1) {
            containerStyle.marginRight = getWidthFromPercentage(.1);
        }

        return(
            <GView style={containerStyle}>
                <View style={[sharedStyles.rowCentered, {paddingLeft: "3%", paddingRight: "3%", paddingTop: "2%", paddingBottom: "2%", justifyContent: "space-between", width: "100%"}]}>
                    <View style={sharedStyles.flexShrink}>
                        <Text style={sharedStyles.textHeader} color={this.props.colors.WHITE}>
                            {t(item.title)}
                        </Text>
                        <Text style={{fontSize: 16, fontWeight: "normal", fontStyle: "italic"}} color={this.props.colors.WHITE}>{tags}</Text>
                    </View>
                    <Button title={details ? t("Show Less") : t("Show More")} shape="pill" color="white" textColor={color} onPress={this.toggleDetail} fatPill={true}/>
                </View>
                <View style={[sharedStyles.hr, sharedStyles.disabled, {borderColor: "white", width: "100%"}]}/>
                {disabled !== "" && <View style={{flexWrap: "wrap"}}>
                    <Text style={[sharedStyles.itemText, sharedStyles.italic, {marginLeft: 5}]} color={this.props.colors.WHITE}>{disabled}</Text>
                </View>}
                <View style={{padding: 5, paddingTop: 0, flexWrap: "wrap", maxHeight: maxHeight - 100}}>
                    <EndeavorList
                        endeavors={item.endeavors}
                        details={details}
                    />
                </View>
                <View style={{height: 10}}></View>
            </GView>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        endeavors: state.settlementReducer.settlements[props.id].endeavors,
        innovations: state.settlementReducer.settlements[props.id].innovations,
        locations: state.settlementReducer.settlements[props.id].locations,
        currentYearEndeavors: getCurrentYearEndeavors(state),
        currentYearSettlementEvents: state.timelineReducer[props.id][state.settlementReducer.settlements[props.id].ly].settlementEvents,
        globalEndeavors: state.settlementReducer.settlements[props.id].globalEndeavors,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Endeavors);
