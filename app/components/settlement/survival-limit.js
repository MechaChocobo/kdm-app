'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {settlementActionWrapper} from '../../actions/settlement';
import { t } from '../../helpers/intl';
import RoundedIcon from '../shared/rounded-icon';
import { getColors } from '../../selectors/general';
import AppText from '../shared/app-text';

import {
    StyleSheet,
    View
} from 'react-native';
import { IncrementorHidden } from '../shared/IncrementorHidden';

class SurvivalLimit extends Component {
    render() {
        return (
            <View style={localStyles.incDecVal}>
                <RoundedIcon
                    icon="sk-survival"
                    size={40}
                    extraBorder={5}
                    color={this.props.colors.WHITE}
                    gradient={this.props.colors.isDark ? false : true}
                    iconColor={this.props.colors.isDark ? this.props.colors.HIGHLIGHT : this.props.colors.WHITE}
                />
                <IncrementorHidden
                    value={this.props.limit}
                    onIncrease={this.increaseSurvival}
                    onDecrease={this.decreaseSurvival}
                />
                <AppText style={localStyles.title}>{t("Survival Limit")}</AppText>
            </View>
        );
    }

    increaseSurvival = () => {
        return this.props.settlementActionWrapper('increaseSurvival', [this.props.id]);
    }

    decreaseSurvival = () => {
        return this.props.settlementActionWrapper('decreaseSurvival', [this.props.id]);
    }
}

function mapStateToProps(state, props) {
    return {
        limit: state.settlementReducer.settlements[props.id].survivalLimit,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SurvivalLimit);

var localStyles = StyleSheet.create({
    incDecVal : {
        width: "33%",
        paddingTop: "2%",
        paddingLeft: "2%",
        paddingRight: "2%",
        alignItems: "center",
    },
    title: {
        fontSize: 16,
        fontStyle: "italic"
    }
});
