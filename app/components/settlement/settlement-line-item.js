'use strict';

import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Alert,
    StyleSheet
} from 'react-native';

import Swipeout from 'react-native-swipeout';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';
import { getCampaignTitle, getExpansionTitle, padNumToDigits } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import RoundedIcon from '../shared/rounded-icon';
import Zebra from '../shared/Zebra';

export default class SettlementsActive extends Component {
    render() {
        let archiveButton = <View style={sharedStyles.swipeoutButton}><Icon name="archive" color="white" size={30}/></View>;
        let deleteButton = <View style={sharedStyles.swipeoutButton}><Icon name="delete" color="white" size={30}/></View>;

        return (
            <ColorContext.Consumer>
                {(colors) => {
                    let buttons = [
                        {component: archiveButton, onPress:this.archive, backgroundColor:colors.HIGHLIGHT},
                        {component: deleteButton, onPress:this.promptToDelete, backgroundColor:colors.RED}
                    ];
                    return (
                        <Swipeout autoClose={true} right={buttons} buttonWidth={80} backgroundColor="transparent">
                            <Zebra zebra={this.props.light}>
                                <TouchableOpacity onPress={this.goToSettlement}>
                                    <View style={[sharedStyles.rowCentered, styles.itemWrapper]}>
                                        <View style={styles.summaryTextWrapper}>
                                            <Text style={styles.title}>
                                                {this.props.settlement.name}
                                            </Text>
                                            <Text style={sharedStyles.italic}>{getCampaignTitle(this.props.settlement.campaign)}</Text>
                                        </View>
                                        <View style={styles.iconGroup}>
                                            <RoundedIcon icon="sk-lantern" gradient={true} size={30}/>
                                            <View style={styles.numTextWrapper}>
                                                <Text style={[styles.numText, {color: colors.HIGHLIGHT}]}>
                                                    {padNumToDigits(this.props.settlement.ly, 2)}
                                                </Text>
                                                <Text style={sharedStyles.italic}>{t("Year")}</Text>
                                            </View>
                                        </View>
                                        <View style={styles.iconGroup}>
                                            <RoundedIcon icon="sk-group" gradient={true} size={30}/>
                                            <View style={styles.numTextWrapper}>
                                                <Text style={[styles.numText, {color: colors.HIGHLIGHT}]}>
                                                    {padNumToDigits(this.props.settlement.population, 2)}
                                                </Text>
                                                <Text style={sharedStyles.italic}>{t("Pop.")}</Text>
                                            </View>
                                        </View>
                                            {/* <Text style={sharedStyles.listItemDescription}>
                                                {t("Campaign")}: {getCampaignTitle(this.props.settlement.campaign)}
                                            </Text> */}
                                            {/* {Object.keys(this.props.settlement.expansions).length > 1 && <Text style={sharedStyles.listItemDescription}>
                                                {this.getExpansionList()}
                                                </Text>}
                                                {Object.keys(this.props.settlement.customExpansions).length > 0 && <Text style={sharedStyles.listItemDescription}>
                                                {this.getCustomExpansionList()}
                                            </Text>} */}
                                    </View>
                                </TouchableOpacity>
                            </Zebra>
                        </Swipeout>
                    )
                }}
            </ColorContext.Consumer>
        );
    }

    goToSettlement = () => {
        this.props.onPress(this.props.settlement.id);
    }

    promptToDelete = () => {
        Alert.alert(t("Are you sure?"), t("Deleting a settlement will remove all traces of the settlement from Scribe and CANNOT BE UNDONE."), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.delete}
        ]);
    }

    delete = () => {
        this.props.onDelete(this.props.settlement.id);
    }

    archive = () => {
        this.props.onArchive(this.props.settlement.id);
    }

    getCustomExpansionList = () => {
        let ids = Object.keys(this.props.settlement.customExpansions);

        let text = [];

        for(let i = 0; i < ids.length; i++) {
            text.push(this.props.customExpansions[ids[i]].name);
        }

        return t("Custom Expansions: ") + text.join(', ');
    }

    getExpansionList = () => {
        let all = [];
        let cards = [];

        for(var expansion in this.props.settlement.expansions) {
            if(expansion === "core") {
                continue;
            }

            if(this.props.settlement.campaign === "PotSun" && expansion === "sunstalker") {
                continue;
            } else if (this.props.settlement.campaign === "PotStars" && expansion === "dragon-king") {
                continue;
            } else if (this.props.settlement.campaign === "PotBloom" && expansion === "flower-knight") {
                continue;
            }

            if(this.props.settlement.expansions[expansion] === "cards") {
                cards.push(getExpansionTitle(expansion));
            } else {
                all.push(getExpansionTitle(expansion));
            }
        }

        if(all.length === 0 && cards.length === 0) {
            return null;
        }

        if(all.length === 0) {
            return t("Expansions with cards only")+": " + cards.join(', ');
        } else if(cards.length === 0) {
            return t("Expansions with all content")+": " + all.join(', ');
        } else {
            return t("Expansions with all content")+": " + all.join(', ') + "\n" +
                t("Expansions with cards only")+": " + cards.join(', ');
        }
    }
};

const styles = StyleSheet.create({
    title:{
        fontSize: 20,
        fontWeight: "600",
    },
    row: {
        padding: 5
    },
    iconGroup: {
        flexDirection: "row",
        flex: .25,
        alignItems: "center"
    },
    numText: {
        fontSize: 20,
        fontWeight: "bold",
    },
    numTextWrapper: {
        alignItems: "center",
        paddingLeft: 5,
    },
    summaryTextWrapper: {
        justifyContent: "center",
        flex: .5,
    },
    itemWrapper: {
        paddingTop: 20,
        paddingBottom: 25,
        paddingLeft: "4%",
    }
});
