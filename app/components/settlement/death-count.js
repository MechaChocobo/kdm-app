'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {settlementActionWrapper} from '../../actions/settlement';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundedIcon from '../shared/rounded-icon';
import AppText from '../shared/app-text';
import { IncrementorHidden } from '../shared/IncrementorHidden';

import {
    StyleSheet,
    View
} from 'react-native';

class DeathCount extends Component {
    render() {
        return (
            <View style={localStyles.incDecVal}>
                <RoundedIcon
                    icon="sk-skull"
                    size={40}
                    extraBorder={5}
                    color={this.props.colors.WHITE}
                    gradient={this.props.colors.isDark ? false : true}
                    iconColor={this.props.colors.isDark ? this.props.colors.HIGHLIGHT : this.props.colors.WHITE}
                />
                <IncrementorHidden
                    value={this.props.deathCount}
                    onIncrease={this.increaseDeathCount}
                    onDecrease={this.decreaseDeathCount}
                />
                <AppText style={localStyles.title}>{t("Death Count")}</AppText>
            </View>
        );
    }

    increaseDeathCount = () => {
        this.props.settlementActionWrapper('increaseDeathCount', [this.props.id]);
    }

    decreaseDeathCount = () => {
        this.props.settlementActionWrapper('decreaseDeathCount', [this.props.id]);
    }
}

function mapStateToProps(state, props) {
    return {
        deathCount: state.settlementReducer.settlements[props.id].deathCount,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(DeathCount);

var localStyles = StyleSheet.create({
    incDecVal : {
        width: "33%",
        paddingTop: "2%",
        paddingLeft: "2%",
        paddingRight: "2%",
        alignItems: "center",
    },
    title: {
        fontSize: 16,
        fontStyle: "italic"
    }
});
