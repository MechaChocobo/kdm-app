'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
    Alert,
} from 'react-native';


import { settlementActionWrapper } from '../../actions/settlement'
import { getAvailableLocations } from '../../helpers/locations';
import Text from '../../components/shared/app-text';
import sharedStyles from '../../style/styles';
import { getLocationConfig } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import Popup from '../shared/popup';
import RoundControl from '../shared/buttons/RoundControl';
import PillListItem from '../shared/PillListItem';
import ListItem from '../shared/ListItem';

let LocationConfig = null;

class Locations extends Component {
    constructor(props) {
        super(props);
        this.state = { pickingLocation: false};
        LocationConfig = getLocationConfig();
    }

    renderLocations = (locations) => {
        if(this.props.locations.length === 0) {
            return <Text style={[sharedStyles.itemText, sharedStyles.italic, sharedStyles.centered, {marginLeft: "5%"}]}>{t("No locations yet")}</Text>
        }

        locations.sort();
        return locations.map((name, i) => {
            return <PillListItem
                key={i}
                title={t(LocationConfig[name].title)}
                onPress={this.confirmRemove}
                id={name}
                icon="close"
            />
        });
    }

    confirmRemove = (name) => {
        Alert.alert(t("Are you sure?"), t("Deleting a location will remove it from your settlement"), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.removeLocation(name)}
        ]);
    }

    removeLocation = (name) => {
        return () => {
            this.props.settlementActionWrapper('removeLocation', [this.props.id, name]);
        }
    };

    addLocation = (name) => {
        this.props.settlementActionWrapper('addLocation', [this.props.id, name]);
        this.onPopupClosed();
    }

    getPicker = () => {
        let availableLocations = getAvailableLocations(this.props.locations, this.props.allLocations);
        let locations = availableLocations.map((name, index) => {
            return (
                <ListItem
                    key={index}
                    title={t(LocationConfig[name].title)}
                    id={name}
                    onPress={this.addLocation}
                    buttonText={t("Add")}
                    confirm={false}
                />
            );
        });

        return (
            <View style={{width: "90%", alignSelf: "center"}}>
                {locations}
            </View>
        );
    }

    toggleDetail = () => {
        this.setState({showDetails: !this.state.showDetails});
    }

    showAddLocation = () => {
        this.setState({pickingLocation: true});
    }

    onPopupClosed = () => {
        this.setState({pickingLocation: false});
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <View style={[sharedStyles.rowCentered, sharedStyles.width90, sharedStyles.centered, {justifyContent: "space-between", marginBottom: 10}]}>
                    <Text style={sharedStyles.title}>{t("Locations")}</Text>
                    <RoundControl icon="plus" onPress={this.showAddLocation}/>
                </View>
                <View style={{flexDirection: "row", flexWrap: "wrap"}}>
                    {this.renderLocations(this.props.locations)}
                </View>
                <Popup title={t("Select Location")} visible={this.state.pickingLocation} onDismissed={this.onPopupClosed}>
                    {this.getPicker()}
                </Popup>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        locations: state.settlementReducer.settlements[props.id].locations,
        allLocations: state.settlementReducer.settlements[props.id].allLocations,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Locations);
