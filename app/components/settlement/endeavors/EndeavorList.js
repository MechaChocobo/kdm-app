'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { getLocationConfig, getInnovationConfig } from '../../../helpers/helpers';
import Endeavor from './Endeavor';

let LocationConfig = null;
let InnovationConfig = null;

export default class EndeavorList extends PureComponent {
    constructor(props) {
        super(props);

        LocationConfig = getLocationConfig();
        InnovationConfig = getInnovationConfig();
    }

    render() {
        return this.props.endeavors.map((endeavor, index) => {
            let extraCost = "";
            if(endeavor.extraCost) {
                let extra = [];
                for (const resource in endeavor.extraCost) {
                    extra.push(resource+" x"+endeavor.extraCost[resource]);
                }
                extraCost = extra.join(", ");
                extraCost += " - ";
            }

            let description = endeavor.description;
            if(endeavor.title === "Build") {
                description = LocationConfig[endeavor.description].title;
            } else if(endeavor.title === "Special Innovate" && !endeavor.plainDescription) {
                description = InnovationConfig[endeavor.description].title;
            }

            let title = endeavor.title;
            if(title && this.props.details) {
                title += ": ";
            }

            if(!title && !this.props.details) {
                title = description;
            }

            return(
                <Endeavor
                    key={index}
                    details={this.props.details}
                    title={title}
                    extraCost={extraCost}
                    description={description}
                    cost={endeavor.endeavorCost}
                    zebra={this.props.zebra}
                />
            );
        });
    }
}

EndeavorList.propTypes = {
    endeavors: PropTypes.arrayOf(PropTypes.object).isRequired,
    details: PropTypes.bool,
    zebra: PropTypes.bool,
}

EndeavorList.defaultProps = {
    details: false,
    zebra: false,
}
