'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
    Image,
    ScrollView,
} from 'react-native';
import styles from '../../../style/styles';
import ColorContext from '../../../context/ColorContext';
import AppText from '../../shared/app-text';
import Zebra from '../../shared/Zebra';
import { t } from '../../../helpers/intl';


export default class Endeavor extends PureComponent {
    render() {
        return (
            <ColorContext.Consumer>
            { colors =>
                <View style={[localStyles.endeavor, {flexDirection: "row", alignSelf: "center", marginBottom: 3, justifyContent: "center"}]}>
                    <View style={{
                        borderTopLeftRadius: 20,
                        borderBottomLeftRadius: 20,
                        backgroundColor: "white",
                        padding: 6,
                        paddingRight: 4,
                        alignSelf: 'stretch',
                        justifyContent: "space-evenly",
                        alignItems: "center",
                        flexDirection: "row",
                        flexWrap: "wrap",
                        width: 45,
                        borderWidth: this.props.zebra ? 1 : 0,
                        borderRightWidth: 0,
                    }}>
                        {this.renderEndeavorCost()}
                    </View>
                    <ScrollView style={localStyles.textStyles} contentContainerStyle={{justifyContent: "center"}}>
                        {this.props.zebra && <Zebra zebra={true} style={[localStyles.textWrapper, {borderWidth: 1, borderLeftWidth: 0}]}>
                            <AppText style={styles.itemText}><AppText style={styles.wrappedTextItemTitle}> {this.props.title}</AppText> {this.props.details ? this.props.extraCost : ''}{this.props.details ? t(this.props.description) : ""}</AppText>
                            <View style={{height: 5}}></View>
                        </Zebra>}
                        {!this.props.zebra && <View style={[localStyles.textWrapper, {backgroundColor: "rgba(256, 256, 256, 0.2)"}]}>
                            <AppText style={styles.itemText}><AppText style={styles.wrappedTextItemTitle} color={colors.WHITE}> {this.props.title}</AppText> {this.props.details ? this.props.extraCost : ''}{this.props.details ? t(this.props.description) : ""}</AppText>
                            <View style={{height: 5}}></View>
                        </View>}
                    </ScrollView>
                </View>
            }
            </ColorContext.Consumer>
        )
    }

    renderEndeavorCost = () => {
        let endeavorCost = [];
        let imageSize = 15;
        for(var i = 0; i < this.props.cost; i++) {
            endeavorCost.push(<Image key={i} style={{width: imageSize, height: imageSize}} source={require('../../../images/endeavor.png')}/>)
        }

        endeavorCost = endeavorCost.map((item, i) => {
            return item;
        });

        return endeavorCost;
    }
}

Endeavor.propTypes = {
    details: PropTypes.bool,
    title: PropTypes.string,
    extraCost: PropTypes.string,
    description: PropTypes.string,
    cost: PropTypes.number,
}

Endeavor.defaultProps = {
    details: false,
}

const localStyles = StyleSheet.create({
    endeavor: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "center",
    },
    textStyles: {
        alignSelf: 'stretch',
        marginRight: 8,
        width: "80%",
    },
    textWrapper: {
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,
        padding: 5,
    }
});
