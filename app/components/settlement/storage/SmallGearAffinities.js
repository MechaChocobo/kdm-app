'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    View,
}  from 'react-native';

import ColorContext from '../../../context/ColorContext';
import { getColorCode } from '../../../helpers/helpers';
import styles from '../../../style/styles';

export default class SmallGearAffinities extends PureComponent {
    render() {
        if(!this.props.affinities) {
            return null;
        }

        const squareSize = 30;
        const width = 10;
        const height = width / 2;
        const padding = (squareSize - width) / 2;

        return <ColorContext.Consumer>
            { colors => {
                return <View style={{width: squareSize, height: squareSize, overflow: "hidden", marginRight: 7, alignItems: "center", justifyContent: "center"}}>
                    {this.props.affinities.top && <View style={{backgroundColor: getColorCode(this.props.affinities.top, colors), width: width, height: height, position: "absolute", top: 0, left: padding}}/>}
                    {this.props.affinities.left && <View style={{backgroundColor: getColorCode(this.props.affinities.left, colors), width: height, height: width, position: "absolute", left: 0, top: padding}}/>}
                    {this.props.affinities.right && <View style={{backgroundColor: getColorCode(this.props.affinities.right, colors), width: height, height: width, position: "absolute", right: 0, top: padding}}/>}
                    {this.props.affinities.bottom && <View style={{backgroundColor: getColorCode(this.props.affinities.bottom, colors), width: width, height: height, position: "absolute", bottom: 0, left: padding}}/>}
                    {this.props.affinities.complete && this.renderCompleteAffinities(colors)}
                </View>
            }}
        </ColorContext.Consumer>
    }

    renderCompleteAffinities = (colors) => {
        if(!this.props.affinities.complete) {
            return null;
        }

        let boxes = [];

        const total = Object.values(this.props.affinities.complete).reduce((t, value) => t + value);

        const size = Math.min(10, Math.floor(15 / total));
        const margin = 2;

        let counter = 1;
        for(let color in this.props.affinities.complete) {
            for(let i = 0; i < this.props.affinities.complete[color]; i++) {
                boxes.push(<View
                    key={color + i}
                    style={{backgroundColor: getColorCode(color, colors), width: size, height: size, marginRight: counter < total ? margin : 0}}
                />);
                counter++;
            }
        }

        return <View style={styles.rowCentered}>
            {boxes}
        </View>
    }
}

SmallGearAffinities.propTypes = {
    affinities: PropTypes.object,
}

SmallGearAffinities.defaultProps = {
}
