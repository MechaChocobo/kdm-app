'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    ScrollView,
} from 'react-native';

import ListItem from '../../shared/ListItem';
import Popup from '../../shared/popup';
import { t } from '../../../helpers/intl';
import { getResourceConfig } from '../../../helpers/helpers';
import AppText from '../../shared/app-text';
import Hr from '../../shared/Hr';
import styles from '../../../style/styles';

let ResourceConfig = null;

export default class ResourceSelection extends PureComponent {
    constructor(props) {
        super(props);

        ResourceConfig = getResourceConfig();

        this.state = {
            showPopup: false,
        }
    }

    render() {
        return <ScrollView>
            <ListItem
                title={this.props.resource.selected ? ResourceConfig[this.props.resource.selected.location][[this.props.resource.selected.name]].title : t("Not Selected")}
                onPress={this.showPopup}
                buttonText={t("Change")}
                subTitle={this.props.resource.type}
            />
            {this.state.showPopup && <Popup
                visible={this.state.showPopup}
                title={t("Select %type%", {type: this.props.type})}
                onDismissed={this.hidePopup}
            >
                {this.renderResourceList()}
            </Popup>}
        </ScrollView>
    }

    renderResourceList = () => {
        let sections = [];

        for(let location in this.props.resources) {
            let resources = this.getResourcesForLocation(location);
            if(resources.length > 0) {
                sections.push(
                    <View key={location}>
                        <AppText style={styles.title}>{location}</AppText>
                        <Hr />
                        {resources.map((resource, i) => {
                            return <ListItem
                                key={i}
                                title={ResourceConfig[location][resource.name].title + " ["+resource.qty+"]"}
                                id={{location, name: resource.name}}
                                onPress={this.onSelect}
                                subTitle={ResourceConfig[location][resource.name].keywords.join(', ')}
                                buttonText={t("Choose")}
                            />
                        })}
                    </View>
                );
            }
        }

        return sections;
    }

    getResourcesForLocation = (location) => {
        let resources = [];

        for(let resource in this.props.resources[location]) {
            if(this.props.resources[location][resource] > 0) {
                if(this.props.type === "any" || ResourceConfig[location][resource].keywords.indexOf(this.props.type) !== -1) {
                    resources.push({name: resource, qty: this.props.resources[location][resource]});
                }
            }
        }

        return resources;
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    onSelect = (resource) => {
        this.props.onSelect(this.props.index, resource, this.props.type);
        this.setState({showPopup: false});
    }
}

ResourceSelection.propTypes = {
    resource: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
    type: PropTypes.string.isRequired,
}
