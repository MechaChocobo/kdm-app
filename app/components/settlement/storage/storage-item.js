'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {settlementActionWrapper} from '../../../actions/settlement';

import {
    View,
    StyleSheet,
} from 'react-native';

import Text from '../../shared/app-text';
import sharedStyles from '../../../style/styles';
import { getColors } from '../../../selectors/general';
import RoundControl from '../../shared/buttons/RoundControl';
import Hr from '../../shared/Hr';

class StorageItem extends Component {
    increment = () => {
        this.props.settlementActionWrapper(this.props.incrementFunc, [this.props.id, this.props.type, this.props.name]);
    }

    decrement = () => {
        this.props.settlementActionWrapper(this.props.decrementFunc, [this.props.id, this.props.type, this.props.name]);
    }

    render() {
        return (
            <View>
                <View style={[sharedStyles.rowCentered, localStyles.wrapper]}>
                    <View style={[sharedStyles.rowCentered, {flexShrink: 1}]}>
                        <View style={this.props.quantity === 0 ? sharedStyles.disabled : {}}>
                            <Text style={localStyles.value}>{this.props.quantity}</Text>
                        </View>
                        {this.props.preText}
                        <View style={{flexShrink: 1}}>
                            <Text style={localStyles.title}>{this.props.title}</Text>
                            {this.props.keywords && <Text style={localStyles.keywords}>{this.props.keywords}</Text>}
                            {(this.props.description !== undefined && this.props.description !== '') && <Text style={localStyles.keywords}>{this.props.description}</Text>}
                        </View>
                    </View>
                    <View style={[sharedStyles.rowCentered, localStyles.controlsWrapper]}>
                        {this.props.extraButtons}
                        <RoundControl onPress={this.decrement} icon="minus" style={localStyles.controlsMargin}/>
                        <RoundControl onPress={this.increment} icon="plus" style={localStyles.controlsMargin} />
                    </View>
                </View>
                <Hr slim={true}/>
                {this.props.children}
            </View>

        )
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(StorageItem);

const localStyles = StyleSheet.create({
    value: {
        fontSize: 35,
        marginRight: 5,
        marginLeft: 5
    },
    keywords: {
        fontStyle: "italic"
    },
    title: {
        fontWeight: "bold",
        fontSize: 20,
    },
    wrapper: {
        flex: 1,
        justifyContent: "space-between",
    },
    controlsWrapper: {
        alignContent: "flex-end"
    },
    controlsMargin: {
        marginLeft: 12
    }
});
