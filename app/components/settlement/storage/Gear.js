'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Collapsible from 'react-native-collapsible';
import Switch from 'react-native-switch-pro';

import {
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

import {settlementActionWrapper} from '../../../actions/settlement';
import {getSortedGear, getGearForDetailedOverview, getGearCountsByType, getGear} from '../../../selectors/gear-storage';
import AppText from '../../../components/shared/app-text';
import { getLocationConfig, getGearConfig } from '../../../helpers/helpers';
import { t } from '../../../helpers/intl';
import { getColors } from '../../../selectors/general';
import GView from '../../shared/GView';
import GearItem from './GearItem';
import { getResources } from '../../../selectors/resource-storage';
import styles from '../../../style/styles';
import Icon from '../../shared/Icon';
import Button from '../../shared/button';
import AffinitySelector from '../../shared/AffinitySelector';

let LocationConfig = null;
let GearConfig = null;

class Gear extends Component {
    colors;

    constructor(props) {
        super(props);
        this.state = {
            showOwnedOnly: false,
            leftFilter: null,
            rightFilter: null,
            topFilter: null,
            bottomFilter: null,
            showFilter: false,
            showFilterList: false,
            completeFilter: null,
        };
        LocationConfig = getLocationConfig();
        GearConfig = getGearConfig();

        this.colors = {
            "blue": t("Blue"),
            "green" : t("Green"),
            "red" : t("Red"),
            "none": t("None"),
        };
    }

    renderGear = (type, gear) => {
        return Object.keys(gear).map((item, i) => {
            if(!this.validGear(item, type, gear)) {
                return null
            }

            return <GearItem
                key={i}
                id={this.props.id}
                quantity={gear[item]}
                type={type}
                name={item}
                state={this.props.state}
                gearConfig={GearConfig[type][item]}
                resourceStorage={this.props.resourceStorage}
                gearStorage={this.props.gearStorage}
                onCraft={this.onCraft}
                colors={this.props.colors}
            ></GearItem>
        });
    }

    validGear = (item, type, storage) => {
        if(!GearConfig[type][item]) {
            return false;
        }

        // owned only filter
        if(this.state.showOwnedOnly && storage[item] === 0) {
            return false;
        }

        // affinity filters
        const affinities = GearConfig[type][item].affinities;
        if(this.state.topFilter) {
            if(!affinities) {
                return false;
            }

            if(!affinities.top || affinities.top !== this.state.topFilter) {
                return false;
            }
        }

        if(this.state.bottomFilter) {
            if(!affinities) {
                return false;
            }

            if(!affinities.bottom || affinities.bottom !== this.state.bottomFilter) {
                return false;
            }
        }

        if(this.state.leftFilter) {
            if(!affinities) {
                return false;
            }

            if(!affinities.left || affinities.left !== this.state.leftFilter) {
                return false;
            }
        }

        if(this.state.rightFilter) {
            if(!affinities) {
                return false;
            }

            if(!affinities.right || affinities.right !== this.state.rightFilter) {
                return false;
            }
        }

        if(this.state.completeFilter) {
            if(!affinities) {
                return false;
            }

            if(!affinities.complete) {
                return false;
            }

            for(let color in this.state.completeFilter) {
                if(!affinities.complete[color] || affinities.complete[color] < this.state.completeFilter[color]) {
                    return false;
                }
            }
        }

        return true;
    }

    onCraft = (gear, resources, gearCost) => {
        this.props.settlementActionWrapper('craftGear', [this.props.id, gear, resources, gearCost]);
    }

    renderGearStorage = (gear) => {
        return Object.keys(gear).map((type, i) => {
            if(this.state.showOwnedOnly === true && this.props.typeCounts[type] === 0) {
                return null;
            }

            let gearContent = (!this.hasFilters() && this.state[type] !== true) ? null : this.renderGear(type, gear[type]);
            return <View key={i} style={styles.centeredWrapper}>
                <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)'}]} onPress={this.toggleLocation(type)}>
                    <AppText style={localStyles.title}>{LocationConfig[type] ? t(LocationConfig[type].title) : type}</AppText>
                    <View style={styles.rowCentered}>
                        <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 13, justifyContent: "center", alignItems: "center"}}>
                            <AppText style={styles.title} color={this.props.colors.WHITE}>{this.props.typeCounts[type]}</AppText>
                        </GView>
                        <Icon name={(!this.hasFilters() && this.state[type] !== true) ? "chevron-right" : "chevron-down"} color={this.props.colors.HIGHLIGHT} size={40}/>
                    </View>
                </TouchableOpacity>
                <Collapsible collapsed={!this.hasFilters() && this.state[type] !== true}>
                    {gearContent}
                </Collapsible>
            </View>
        });
    }

    hasFilters = () => {
        if(this.state.showOwnedOnly) {
            return true;
        }

        if(
            this.state.topFilter ||
            this.state.bottomFilter ||
            this.state.leftFilter ||
            this.state.rightFilter ||
            this.state.completeFilter
        ) {
            return true;
        }

        return false
    }

    toggleLocation = (type) => {
        return () => {
            let state = { ...this.state };
            if (this.state[type]) {
                state[type] = false;
                this.setState(state);
            } else {
                state[type] = true;
                this.setState(state);
            }
        }
    }

    setShowOwnedOnly = (val) => {
        this.setState({showOwnedOnly: val});
    }

    renderAffinityFilter = () => {
        if(!this.props.isSubscriber) {
            return null;
        }

        return <View style={styles.container, {marginTop: 10}}>
            <Button
                title={this.state.showFilterList ? t("Clear affinity filters") : t("Show affinity filters")}
                onPress={this.toggleShowAffinityFilters}
                style={[styles.width90, styles.centered]}
                shape="rounded"
            />
            <Collapsible collapsed={this.state.showFilterList === false}>
                <AffinitySelector
                    onChange={this.setColorFilter}
                    top={this.state.topFilter}
                    bottom={this.state.bottomFilter}
                    left={this.state.leftFilter}
                    right={this.state.rightFilter}
                    complete={this.state.completeFilter}
                />
            </Collapsible>
        </View>
    }

    toggleShowAffinityFilters = () => {
        if(this.state.showFilterList) {
            this.setState({showFilterList: false, topFilter: null, bottomFilter: null, leftFilter: null, rightFilter: null, completeFilter: null});
        } else {
            this.setState({showFilterList: true});
        }
    }

    setColorFilter = (location, color) => {
        if(location === "top") {
            this.setState({topFilter: color});
        } else if(location === "bottom") {
            this.setState({bottomFilter: color});
        } else if(location === "left") {
            this.setState({leftFilter: color});
        } else if(location === "right") {
            this.setState({rightFilter: color});
        } else if(location === "complete") {
            this.setState({completeFilter: color});
        }
    }

    render() {
        return(
            <View>
                {this.renderAffinityFilter()}
                <GView style={[styles.rowCentered, localStyles.ownedOnlyWrapper]}>
                    <AppText style={localStyles.title} color={this.props.colors.WHITE}>{t("Show Owned Only")}</AppText>
                    <Switch
                        value={this.state.showOwnedOnly}
                        onSyncPress={this.setShowOwnedOnly}
                        circleColorActive={this.props.colors.HIGHLIGHT}
                        backgroundActive={this.props.colors.GRAY}
                        backgroundInactive={this.props.colors.GRAY}
                    />
                </GView>
                {this.renderGearStorage(this.props.gear)}
            </View>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        gear: getSortedGear(state),
        overviewGear: getGearForDetailedOverview(state),
        typeCounts: getGearCountsByType(state),
        colors: getColors(state),
        state: state,
        resourceStorage: getResources(state),
        gearStorage: getGear(state),
        isSubscriber: state.purchases.isSubscriber,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Gear);

const localStyles = StyleSheet.create({
    title: {
        fontSize: 24,
        fontWeight: "500"
    },
    ownedOnlyWrapper: {
        width: "90%",
        alignSelf: "center",
        justifyContent: "space-between",
        padding: 5,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
    },
    filterBox: {
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 1,
        borderWidth: 1,
    },
    circle: {
        width: 25,
        height: 25,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: "black",
        marginLeft: 10,
    }
});
