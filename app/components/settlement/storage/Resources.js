'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Collapsible from 'react-native-collapsible';
import Switch from 'react-native-switch-pro';
import Icon from 'react-native-vector-icons/MaterialIcons';

import {
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

import {settlementActionWrapper} from '../../../actions/settlement';
import { getSortedResources, getResourcesForDetailedOverview, getResourcesForOverview, getResourceCountsByType } from '../../../selectors/resource-storage';
import Text from '../../../components/shared/app-text';
import sharedStyles from '../../../style/styles';
import StorageItem from './storage-item';
import { getResourceConfig } from '../../../helpers/helpers';
import { t } from '../../../helpers/intl';
import { getColors } from '../../../selectors/general';
import GView from '../../shared/GView';

let ResourceConfig = null;

class Resources extends Component {
    constructor(props) {
        super(props);
        this.state = {showOwnedOnly: false};
        ResourceConfig = getResourceConfig();
    }

    renderResource = (type, resources) => {
        return Object.keys(resources).map((resource, i) => {
            if(!ResourceConfig[type][resource] || (this.state.showOwnedOnly && resources[resource] === 0)) {
                return null;
            }

            return <StorageItem
                key={i}
                id={this.props.id}
                title={t(ResourceConfig[type][resource].title)}
                keywords={ResourceConfig[type][resource].keywords.length > 0 ? ResourceConfig[type][resource].keywords.join(', ') : null}
                quantity={resources[resource]}
                type={type}
                name={resource}
                incrementFunc="increaseResource"
                decrementFunc="decreaseResource"
            />
        });
    }

    renderResourceStorage = (resources) => {
        return Object.keys(resources).map((type, i) => {
            if(this.state.showOwnedOnly === true && this.props.typeCounts[type] === 0) {
                return null;
            }

            let resourceContent = (this.state.showOwnedOnly === false && this.state[type] !== true) ? null : this.renderResource(type, resources[type]);
            return <View key={i} style={sharedStyles.centeredWrapper}>
                <TouchableOpacity style={[sharedStyles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)'}]} onPress={this.toggleResource(type)}>
                    <Text style={localStyles.title}>{type}</Text>
                    <View style={sharedStyles.rowCentered}>
                        <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 13, justifyContent: "center", alignItems: "center"}}>
                            <Text style={sharedStyles.title} color={this.props.colors.WHITE}>{this.props.typeCounts[type]}</Text>
                        </GView>
                        <Icon name={(this.state.showOwnedOnly === false && this.state[type] !== true) ? "chevron-right" : "expand-more"} color={this.props.colors.HIGHLIGHT} size={40}/>
                    </View>
                </TouchableOpacity>
                <Collapsible collapsed={this.state.showOwnedOnly === false && this.state[type] !== true}>
                    {resourceContent}
                </Collapsible>
            </View>
        });
    }

    renderOverview = () => {
        let summary = [];

        for (const type in this.props.overviewResources) {
            summary.push({type: type, quantity: this.props.overviewResources[type]});
        }

        return <View>
            <View style={[sharedStyles.rowCentered, localStyles.summaryWrapper, {borderColor: 'rgba(256, 256, 256, 0.5)'}]}>{
                summary.map((value, index) => {
                    return <View
                        key={index}
                        style={[{alignItems: "center", flexWrap: "nowrap"}, (value.quantity === 0 ? sharedStyles.disabled : {})]}
                    >
                        <Text style={sharedStyles.itemText}>{value.quantity}</Text>
                        <Text>{value.type}</Text>
                    </View>
                })}
            </View>
            <GView style={[sharedStyles.rowCentered, localStyles.ownedOnlyWrapper]}>
                <Text style={localStyles.title} color={this.props.colors.WHITE}>{t("Show Owned Only")}</Text>
                <Switch
                    value={this.state.showOwnedOnly}
                    onSyncPress={this.setShowOwnedOnly}
                    circleColorActive={this.props.colors.HIGHLIGHT}
                    backgroundActive={this.props.colors.GRAY}
                    backgroundInactive={this.props.colors.GRAY}
                />
            </GView>
        </View>
    }

    onItemDecrement = (func, deck, name) => {
        this.props.settlementActionWrapper(func, [this.props.id, deck, name]);
    }

    toggleResource = (type) => {
        return () => {
            let state = { ...this.state };
            if (this.state[type]) {
                state[type] = false;
                this.setState(state);
            } else {
                state[type] = true;
                this.setState(state);
            }
        }
    }

    setShowOwnedOnly = (val) => {
        this.setState({showOwnedOnly: val});
    }

    render() {
        return(
            <View>
                {this.renderOverview()}
                {this.renderResourceStorage(this.props.resources)}
            </View>
        )
    }
}

function mapStateToProps(state, props) {
    return {
        resources: getSortedResources(state),
        detailedOverviewResources: getResourcesForDetailedOverview(state),
        overviewResources: getResourcesForOverview(state),
        typeCounts: getResourceCountsByType(state),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Resources);

const localStyles = StyleSheet.create({
    summaryWrapper : {
        flex: 1,
        justifyContent: "space-evenly",
        borderBottomWidth: 1,
    },
    title: {
        fontSize: 24,
        fontWeight: "500"
    },
    ownedOnlyWrapper: {
        width: "90%",
        alignSelf: "center",
        justifyContent: "space-between",
        padding: 5,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 5,
        marginTop: 10,
        marginBottom: 10,
    }
});
