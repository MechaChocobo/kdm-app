'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    ScrollView,
} from 'react-native';

import { getLocationConfig, getResourceConfig, getGearConfig } from '../../../helpers/helpers';
import styles from '../../../style/styles';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import Hr from '../../shared/Hr';
import { getDefaultResource, orderResourcesByCraftingPriority } from '../../../helpers/storage';
import ResourceSelection from './ResourceSelection';
import Button from '../../shared/button';
import ListItem from '../../shared/ListItem';

let LocationConfig = null;
let ResourceConfig = null;
let GearConfig = null;

export default class GearCrafting extends PureComponent {
    constructor(props) {
        super(props);

        LocationConfig = getLocationConfig();
        ResourceConfig = getResourceConfig();
        GearConfig = getGearConfig();

        let requiredSlots = {
            resourceTypes: [],
            resources: [],
            gear: [],
        };

        let resources = JSON.parse(JSON.stringify(this.props.resources));
        let gearStorage = JSON.parse(JSON.stringify(this.props.gear));

        if(this.props.cost.resources) {
            for(let i = 0; i < this.props.cost.resources.length; i++) {
                for(let j = 0; j < this.props.cost.resources[i].qty; j++) {
                    let res = this.props.cost.resources[i];
                    if(resources[res.location][res.name] > 0) {
                        requiredSlots.resources.push({
                            location: res.location,
                            name: res.name,
                            title: ResourceConfig[res.location][res.name].title + " (" + res.location + ")"}
                        );
                        resources[res.location][res.name] = resources[res.location][res.name] - 1;
                    }
                }
            }
        }

        if(this.props.cost.gear) {
            for(let i = 0; i < this.props.cost.gear.length; i++) {
                for(let j = 0; j < this.props.cost.gear[i].qty; j++) {
                    let gear = this.props.cost.gear[i];
                    let locationName = LocationConfig[gear.location] ? LocationConfig[gear.location].name : gear.location;
                    if(gearStorage[gear.location][gear.name] > 0) {
                        requiredSlots.gear.push({
                            location: gear.location,
                            name: gear.name,
                            title: GearConfig[gear.location][gear.name].title + " (" + locationName + ")"
                        });
                    }
                }
            }
        }

        // transforms into an array ordered by crafting priority
        let orderedResources = orderResourcesByCraftingPriority(resources);
        if(this.props.cost.resourceTypes) {
            for(let keyword in this.props.cost.resourceTypes) {
                for(let i = 0; i < this.props.cost.resourceTypes[keyword]; i++) {
                    let preselected = undefined;
                    [preselected, orderedResources] = getDefaultResource(keyword, orderedResources);

                    if(!preselected) {
                        continue;
                    }

                    resources[preselected.location][preselected.name] = resources[preselected.location][preselected.name] - 1;
                    requiredSlots.resourceTypes.push({type: keyword, selected: preselected});
                }
            }
        }

        this.state = {
            resources,
            gearStorage,
            requiredSlots,
        }
    }

    render() {
        return (
            <View style={[styles.container, styles.spaceBetween]}>
                <ScrollView style={styles.container}>
                    {this.renderCost()}
                    {this.renderResourceChoices()}
                    {this.renderRequiredResources()}
                    {this.renderRequiredGear()}
                </ScrollView>
                <View>
                    <Hr />
                    <Button
                        title={t("Craft")}
                        onPress={this.craftGear}
                        shape="rounded"
                    />
                </View>
            </View>
        );
    }

    craftGear = () => {
        let resources = this.state.requiredSlots.resources;

        if(this.state.requiredSlots.resourceTypes.length > 0) {
            for(let i = 0; i < this.state.requiredSlots.resourceTypes.length; i++) {
                resources.push(this.state.requiredSlots.resourceTypes[i].selected);
            }
        }

        this.props.onCraft(resources, this.state.requiredSlots.gear);
    }

    renderCost = () => {
        if(!this.props.showCost === true) {
            return null;
        }

        let costs = [];

        if(this.props.cost.resourceTypes) {
            Object.keys(this.props.cost.resourceTypes).map((type) => {
                costs.push(type + ' x ' + this.props.cost.resourceTypes[type]);
            });
        }

        if(this.props.cost.resources) {
            this.props.cost.resources.map((resource) => {
                costs.push(ResourceConfig[resource.location][resource.name].title + ' (' + resource.location + ')');
            });
        }

        if(this.props.cost.gear) {
            this.props.cost.gear.map((gear) => {
                costs.push(GearConfig[gear.location][gear.name].title + ' (' + gear.location + ')');
            });
        }

        let title = t("Cost");

        if(this.props.optionNumber) {
            title = t("Cost for option %num%", {num: this.props.optionNumber});
        }

        return <View>
            <AppText style={styles.title}>{title}</AppText>
            <AppText style={styles.itemText}>{costs.join(", ")}</AppText>
        </View>
    }

    renderResourceChoices = () => {
        if(this.state.requiredSlots.resourceTypes.length === 0) {
            return null;
        }

        return <View>
            <AppText style={styles.title}>{t("Select Resources")}</AppText>
            <Hr />
            {this.state.requiredSlots.resourceTypes.map((resource, i) => {
                return <ResourceSelection
                    key={i}
                    index={i}
                    resources={this.state.resources}
                    resource={resource}
                    onSelect={this.onResourceSelect}
                    type={resource.type}
                />
            })}
        </View>
    }

    onResourceSelect = (index, resource, type) => {
        // swap selected resource
        let selected = [...this.state.requiredSlots.resourceTypes];
        let oldResource = selected[index].selected;
        selected[index] = {selected: {...resource}, type};

        // update required slots in state with the selection
        let requiredSlots = {...this.state.requiredSlots};
        requiredSlots.resourceTypes = selected;

        // update what is in resource storage based on the switch
        let resources = {...this.state.resources};
        resources[oldResource.location][oldResource.name] = resources[oldResource.location][oldResource.name] + 1;
        resources[resource.location][resource.name] = resources[resource.location][resource.name] - 1;

        this.setState({requiredSlots, resources});
    }

    renderRequiredResources = () => {
        if(this.state.requiredSlots.resources.length === 0) {
            return null;
        }

        return <View>
            <AppText style={styles.title}>{t("Required Resources")}</AppText>
            <Hr />
            {this.state.requiredSlots.resources.map((resource, i) => {
                return <ListItem
                    key={i}
                    title={resource.title}
                    subTitle={ResourceConfig[resource.location][resource.name].keywords.join(', ')}
                />
            })}
        </View>
    }

    renderRequiredGear = () => {
        if(this.state.requiredSlots.gear.length === 0) {
            return null;
        }

        return <View>
            <AppText style={styles.title}>{t("Required Gear")}</AppText>
            <Hr />
            {this.state.requiredSlots.gear.map((gear, i) => {
                return <ListItem
                    key={i}
                    title={gear.title}
                />
            })}
        </View>
    }
}

GearCrafting.propTypes = {
    cost: PropTypes.object,
    resources: PropTypes.object.isRequired,
    gear : PropTypes.object.isRequired,
    showCost : PropTypes.bool,
}

GearCrafting.defaultProps = {
    showCost: false,
}
