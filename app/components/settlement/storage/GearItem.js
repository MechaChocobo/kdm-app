'use strict';

import React, { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import Toast from 'react-native-simple-toast';

import {
    View,
} from 'react-native';

import { isGearCraftable } from '../../../helpers/storage';
import StorageItem from './storage-item';
import RoundControl from '../../shared/buttons/RoundControl';
import Popup from '../../shared/popup';
import GearCrafting from './GearCrafting';
import Hr from '../../shared/Hr';
import SmallGearAffinities from './SmallGearAffinities';
import { t } from '../../../helpers/intl';

export default class GearItem extends PureComponent {
    isCraftable = false;

    constructor(props) {
        super(props);

        this.state = {
            showPopup: false,
            isCraftable: isGearCraftable(this.props.gearConfig.cost, this.props.state),
        }
    }

    componentDidUpdate() {
        this.setState({isCraftable: isGearCraftable(this.props.gearConfig.cost, this.props.state)});
    }

    render() {
        return (
            <View>
                <StorageItem
                    id={this.props.id}
                    title={t(this.props.gearConfig.title)}
                    quantity={this.props.quantity}
                    type={this.props.type}
                    name={this.props.name}
                    incrementFunc="increaseGear"
                    decrementFunc="decreaseGear"
                    description={this.props.gearConfig.description}
                    extraButtons={
                        <Fragment>
                            {this.getAffinities()}
                            {(this.props.gearConfig.craftable === false || this.props.gearConfig.cost === undefined) ? null :
                                <RoundControl
                                    onPress={this.showCrafting}
                                    icon="hammer"
                                    disabled={this.state.isCraftable !== true}
                                    onDisabledPress={this.warnNotCraftable}
                                />}
                        </Fragment>
                    }
                ></StorageItem>
                {this.state.showPopup && <Popup
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                    title={t(this.props.gearConfig.title)}
                    noScrollView={!Array.isArray(this.props.gearConfig.cost)}
                >
                    {this.renderCrafting()}
                </Popup>}
            </View>
        )
    }

    getAffinities = () => {
        const affinities = this.props.gearConfig.affinities;

        if(!affinities) {
            return null;
        }

        return <SmallGearAffinities affinities={affinities} />
    }

    warnNotCraftable = () => {
        if(typeof this.state.isCraftable === "string") {
            Toast.show(this.state.isCraftable, Toast.LONG);
        }
    }

    renderCrafting = () => {
        if(Array.isArray(this.props.gearConfig.cost)) {
            let costs = [];

            for(let i = 0; i < this.props.gearConfig.cost.length; i++) {
                if(isGearCraftable(this.props.gearConfig.cost[i], this.props.state) === true) {
                    costs.push(
                        <GearCrafting
                            key={i}
                            cost={this.props.gearConfig.cost[i]}
                            resources={this.props.resourceStorage}
                            gear={this.props.gearStorage}
                            onCraft={this.onCraft}
                            showCost={true}
                            optionNumber={i + 1}
                        />
                    );
                }
            }

            for(let i = 1; i < costs.length ; i += 2) {
                costs.splice(i, 0, <Hr key={'hr' + i} style={{borderBottomWidth: 6, borderColor: this.props.colors.HIGHLIGHT}}/>);
            }

            return costs;
        }

        return <GearCrafting
            cost={this.props.gearConfig.cost}
            resources={this.props.resourceStorage}
            gear={this.props.gearStorage}
            onCraft={this.onCraft}
        />
    }

    onCraft = (resources, gearCost) => {
        this.hidePopup();
        this.props.onCraft({location: this.props.type, name: this.props.name}, resources, gearCost);
    }

    showCrafting = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }
}

GearItem.propTypes = {
    id: PropTypes.string,
    gearConfig: PropTypes.object.isRequired,
    quantity: PropTypes.number,
    type: PropTypes.string,
    name: PropTypes.string,
    state: PropTypes.object.isRequired,
    resourceStorage: PropTypes.object.isRequired,
    gearStorage: PropTypes.object.isRequired,
}
