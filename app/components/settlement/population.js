'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    StyleSheet,
    View
} from 'react-native';

import {settlementActionWrapper} from '../../actions/settlement';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import RoundedIcon from '../shared/rounded-icon';
import AppText from '../shared/app-text';

import { IncrementorHidden } from '../shared/IncrementorHidden';

class Population extends Component {
    render() {
        return (
            <View style={localStyles.incDecVal}>
                <RoundedIcon
                    icon="sk-group"
                    size={40}
                    extraBorder={5}
                    color={this.props.colors.WHITE}
                    gradient={this.props.colors.isDark ? false : true}
                    iconColor={this.props.colors.isDark ? this.props.colors.HIGHLIGHT : this.props.colors.WHITE}
                />
                <IncrementorHidden
                    value={this.props.population}
                    onIncrease={this.increasePopulation}
                    onDecrease={this.decreasePopulation}
                />
                <AppText style={localStyles.title}>{t("Population")}</AppText>
            </View>
        );
    }

    increasePopulation = () => {
        this.props.settlementActionWrapper('increasePopulation', [this.props.id]);
    }

    decreasePopulation = () => {
        this.props.settlementActionWrapper('decreasePopulation', [this.props.id]);
    }
}

function mapStateToProps(state, props) {
    return {
        population: state.settlementReducer.settlements[props.id].population,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({settlementActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Population);

var localStyles = StyleSheet.create({
    incDecVal : {
        width: "33%",
        paddingTop: "2%",
        paddingLeft: "2%",
        paddingRight: "2%",
        alignItems: "center",
    },
    title: {
        fontSize: 16,
        fontStyle: "italic"
    }
});
