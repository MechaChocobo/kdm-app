'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    Animated,
    StyleSheet,
    View,
    TouchableOpacity,
    Dimensions
} from 'react-native';

import styles from '../style/styles';
import Icon from './shared/Icon';
import AppText from './shared/app-text';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import { setErrorControlsOverride, clientStop } from '../actions/network';

const ANIMATION_DURATION = 250;
const BUTTON_WIDTH = 50;
let EXPANDED_WIDTH = 0;

class NetworkErrorControls extends Component {
    constructor(props) {
        super(props);

        this.resetAnimations();

        this.state = {
            showControls: true,
            showConnectionProgress: false,
        }

        EXPANDED_WIDTH = Dimensions.get('window').width - 100;
    }

    componentDidUpdate(prevProps) {
        if(prevProps.loading && !this.props.loading) {
            if(this.props.clientError) {
                this.setState({showControls: true, showConnectionProgress: false});
                Animated.timing(this.controlsFadeAnimation, {
                    toValue: 1,
                    duration: ANIMATION_DURATION,
                    isInteraction: true,
                }).start();
                this.props.setErrorControlsOverride(false);
            } else {
                setTimeout(() => {
                    Animated.timing(this.widthAnimation, {
                        toValue: 0,
                        duration: ANIMATION_DURATION,
                        isInteraction: true,
                    }).start(() => {
                        this.fadeButton(() => {this.props.setErrorControlsOverride(false)})
                    });
                }, 1250);
            }
        }
    }

    resetAnimations = () => {
        this.widthAnimation = new Animated.Value(0);
        this.controlsFadeAnimation = new Animated.Value(0);
        this.containerFadeAnimation = new Animated.Value(1);
        this.progressFadeAnimation = new Animated.Value(0);
    }

    componentDidMount() {
        this.widthAnimation.setValue(0);
        this.expand();
    }

    render() {
        let styleAr = [localStyles.base, {backgroundColor: this.props.colors.MENU}];
        styleAr.push({
            width: this.widthAnimation.interpolate({
                inputRange: [0, 1],
                outputRange: [0, EXPANDED_WIDTH],
                extrapolate: 'clamp',
            }),
            opacity: this.containerFadeAnimation,
        });
        styleAr.push(localStyles.expanded);
        let fadeInStyle = {
            flex: 1,
            height: 50,
            flexDirection: "row",
            justifyContent: "space-evenly",
            opacity: this.controlsFadeAnimation,
            width: EXPANDED_WIDTH,
            alignItems: "center"
        };
        let progressIconStyle = {
            opacity: this.progressFadeAnimation,
        }

        return <Animated.View style={styleAr}>
            <View style={[styles.row, {backgroundColor: this.props.colors.RED, flex: 1, height: 50, width: "100%", alignItems: "center", justifyContent: "center"}]}>
                <Icon name="sk-warning" size={30} color={this.props.colors.WHITE}/>
                <AppText style={[styles.title, {color: this.props.colors.WHITE}]}>{t('Network disconnected')}</AppText>
            </View>
            {this.state.showControls && <Animated.View style={fadeInStyle}>
                <TouchableOpacity onPress={this.reconnect} style={[styles.centeredItems]}>
                    <Icon name="autorenew" size={30} color={this.props.colors.ORANGE}/>
                    <AppText color={this.props.colors.INVERT_TEXT}>{t("Reconnect")}</AppText>
                </TouchableOpacity>
                <TouchableOpacity onPress={this.dismiss} style={styles.centeredItems}>
                    <Icon name="close" size={30} color={this.props.colors.RED}/>
                    <AppText color={this.props.colors.INVERT_TEXT}>{t("Dismiss")}</AppText>
                </TouchableOpacity>
            </Animated.View>}
            {this.state.showConnectionProgress && <Animated.View style={progressIconStyle}>
                <Icon name={this.props.loading ? "autorenew" : "check"} size={30} color={this.props.loading ? this.props.colors.ORANGE : this.props.colors.GREENDARK}/>
                <AppText> </AppText>
            </Animated.View>}
        </Animated.View>
    }

    dismiss = () => {
        Animated.timing(this.widthAnimation, {
            toValue: 0,
            duration: ANIMATION_DURATION,
            isInteraction: true,
        }).start(() => {
            this.fadeButton(this.props.clientStop)
        });
    }

    fadeButton = (callback) => {
        Animated.timing(this.containerFadeAnimation, {
            toValue: 0,
            duration: ANIMATION_DURATION,
            isInteraction: true,
        }).start();

        if(typeof callback === "function") {
            setTimeout(() => {
                callback();
            }, 500);
        }
    }

    fadeControls = (callback) => {
        Animated.timing(this.controlsFadeAnimation, {
            toValue: 0,
            duration: ANIMATION_DURATION,
            isInteraction: true,
        }).start(() => {
            if(typeof callback === "function") {
                callback();
            }
        });
    }

    reconnect = () => {
        this.props.setErrorControlsOverride(true);
        this.fadeControls(() => {
            this.props.reconnectFn(false);
            this.setState({showControls: false, showConnectionProgress: true}, () => {
                Animated.timing(this.progressFadeAnimation, {
                    toValue: 1,
                    duration: ANIMATION_DURATION,
                    isInteraction: true,
                }).start();
            });
        })
    }

    expand = () => {
        Animated.timing(this.widthAnimation, {
            toValue: 1,
            duration: ANIMATION_DURATION,
            isInteraction: true,
            delay: 100,
        }).start(() => {
            Animated.timing(this.controlsFadeAnimation, {
                toValue: 1,
                duration: ANIMATION_DURATION,
                isInteraction: true,
            }).start();
        });
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
        loading: state.networkReducer.loading,
        clientError: state.networkReducer.clientError,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setErrorControlsOverride,
        clientStop,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NetworkErrorControls);

NetworkErrorControls.propTypes = {
    reconnectFn: PropTypes.func.isRequired,
}

NetworkErrorControls.defaultProps = {
}

const localStyles = StyleSheet.create({
    base: {
        position: 'absolute',
        bottom: 120,
        right: 50,
        zIndex: 50,
        height: 100,
        borderRadius: 10,
        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden",
        borderWidth: 2,
        borderColor: "#da1010",
        flex: 1,
    },
    button: {
        width: BUTTON_WIDTH,
    },
});
