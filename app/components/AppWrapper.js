'use strict';

import React, { Component } from 'react';
import {
    Platform,
    View,
    SafeAreaView,
    StatusBar,
    NativeModules,
} from 'react-native';

const { StatusBarManager } = NativeModules;

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getColors } from '../selectors/general';
import Router from './router';
import styles from '../style/styles';

class AppWrapper extends Component {
    padding = 0;

    constructor(props) {
        super(props);

        this.state = {
            statusBarHeight: 0,
        }

        if(Platform.OS === "ios") {
            this.padding = 20;
            if(parseInt(Platform.Version, 10) >= 11) {
                this.padding = 0;
            }
        }
    }

    componentDidMount() {
        if (Platform.OS === 'ios') {
            StatusBarManager.getHeight(response =>
                this.setState({statusBarHeight: response.height})
            )
        }
    }

    render() {
        return <View style={styles.container}>
            <StatusBar
                backgroundColor={this.props.colors.GRADIENT_START_BACKGROUND}
                barStyle={this.props.colors.isDark ? "light-content" : "dark-content"}
            />
            {Platform.OS === "ios" && <View style={{height: this.state.statusBarHeight, backgroundColor: this.props.colors.GRADIENT_START_BACKGROUND}}/>}
            <SafeAreaView style={styles.container}>
                <View style={{height: this.padding}} />
                <Router />
            </SafeAreaView>
        </View>
    }
};

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AppWrapper);
