'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
} from 'react-native';

import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';
import Icon from './Icon';
import Button from './button';
import AppText from './app-text';
import PageIndicator from './PageIndicator';

export default class PageControls extends Component {
    render() {
        let containerStyle = [styles.rowCentered, styles.container];

        if(this.props.style) {
            if(Array.isArray(this.props.style)) {
                containerStyle = containerStyle.concat(this.props.style);
            } else {
                containerStyle = [...containerStyle, this.props.style];
            }
        }

        return (
            <ColorContext.Consumer>
            { colors => {
                return <View style={containerStyle}>
                    <View style={styles.container}>
                        <View>
                            <Button style={{padding: 10, paddingTop: 5, paddingBottom: 5}} onPress={this.onPrevious} color={colors.HIGHLIGHT} disabled={this.props.previousDisabled} shape="rounded">
                                <Icon name="arrow-left" color={colors.WHITE} size={20}/>
                                {this.props.previousTitle !== '' && <AppText style={{color: colors.WHITE, fontSize: 14, fontWeight: "bold", marginLeft: 5}}>{this.props.previousTitle}</AppText>}
                            </Button>
                        </View>
                    </View>
                    <View style={[styles.container, styles.centeredItems]}>
                        <PageIndicator numPages={this.props.numPages} currentPage={this.props.currentPage} />
                    </View>
                    <View style={styles.container}>
                        <Button style={{padding: 10, paddingTop: 5, paddingBottom: 5}} onPress={this.onNext} color={colors.HIGHLIGHT} disabled={this.props.nextDisabled} shape="rounded">
                            {this.props.nextTitle !== '' && <AppText style={{color: colors.WHITE, fontSize: 14, fontWeight: "bold", marginRight: 5, marginLeft: 5}}>{this.props.nextTitle}</AppText>}
                            <Icon name="arrow-right" color={colors.WHITE} size={20}/>
                        </Button>
                    </View>
                </View>
            }}
            </ColorContext.Consumer>
        );
    }

    onPrevious = () => {
        this.props.onPrevious();
    }

    onNext = () => {
        this.props.onNext();
    }
}

PageControls.propTypes = {
    onPrevious: PropTypes.func.isRequired,
    onNext: PropTypes.func.isRequired,
    previousDisabled: PropTypes.bool,
    nextDisabled: PropTypes.bool,
    previousTitle: PropTypes.string,
    nextTitle: PropTypes.string,
    style: PropTypes.any,
    numPages: PropTypes.number,
    currentPage: PropTypes.number,
}

PageControls.defaultProps = {
    previousDisabled: false,
    nextDisabled: false,
    previousTitle: '',
    nextTitle: '',
}
