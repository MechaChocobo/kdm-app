'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
} from 'react-native';

import ColorContext from '../../context/ColorContext';

export default class Zebra extends Component {
    render() {
        return (
            <ColorContext.Consumer>
            { colors => {
                let style = Array.isArray(this.props.style) ? [...this.props.style] : [this.props.style];
                style.push({backgroundColor: this.getBackgroundColor(colors)});

                return <View style={style}>
                    {this.props.children}
                </View>
            }}
            </ColorContext.Consumer>
        );
    }

    getBackgroundColor = (colors) => {
        let color = "transparent";

        if(this.props.zebra === true) {
            if(colors.isDark) {
                color = "rgba(256, 256, 256, 0.15)";
            } else {
                color = "rgba(0, 0, 0, 0.15)";
            }
        }

        return color;
    }
}

Zebra.propTypes = {
    zebra: PropTypes.bool,
    style: PropTypes.any,
}

Zebra.defaultProps = {
    zebra: false,
    style: [],
}
