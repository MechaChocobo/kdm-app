'use strict';

import React, { Component } from 'react';
import {BackHandler, View} from 'react-native';
import PropTypes from 'prop-types';
import ScrollableTabView from 'react-native-scrollable-tab-view-universal';

export default class MultiPageContainer extends Component {
    ref = null;

    constructor(props) {
        super(props);

        let pages = {};
        let defaultPage = this.props.defaultPage;
        let key = 0;
        for(let pageId in this.props.pages) {
            if(!defaultPage) {
                defaultPage = pageId;
            }
            pages[pageId] = React.cloneElement(this.props.pages[pageId], {
                onPageChange: this.onPageChange,
                onBack: this.goBack,
                key,
            });
            key++;
        }
        this.state = {
            pages,
            defaultPage,
            history: [defaultPage],
            backHandlerAdded: false,
            currentPage: defaultPage,
        }
    }

    componentWillUnmount = () => {
        if(this.state.backHandlerAdded) {
            BackHandler.removeEventListener('hardwareBackPress', this.goBack);
        }
    }

    render() {
        let children = [this.state.pages[this.state.defaultPage]];
        if(this.state.history.length > 1) {
            children.push(this.state.pages[this.state.currentPage]);
        }

        return (
            <ScrollableTabView
                ref={(ref) => this.ref = ref}
                renderTabBar={() => {return <View />}}
                locked={true}
            >
                {children}
            </ScrollableTabView>
        );
    }

    onPageChange = (page) => {
        if(this.state.pages[page]) {
            let history = [...this.state.history];
            history.push(page);
            if(this.state.backHandlerAdded === false) {
                BackHandler.addEventListener('hardwareBackPress', this.goBack);
            }
            this.setState({history, backHandlerAdded: true, currentPage: page}, () => {
                setTimeout(() => {
                    this.ref.goToPage(1);
                }, 200)
            });
        }
    }

    goBack = () => {
        let history = [...this.state.history];

        if(history.length > 1) {
            history.splice(history.length - 1, 1);
            this.ref.goToPage(0);
            let currentPage = history[history.length - 1];
            this.setState({history, currentPage});
            return true;
        } else {
            BackHandler.removeEventListener('hardwareBackPress', this.goBack);
            this.setState({backHandlerAdded: false});
            return false;
        }
    }
}

MultiPageContainer.propTypes = {
    pages: PropTypes.object.isRequired,
    defaultPage: PropTypes.string,
}

MultiPageContainer.defaultProps = {
}
