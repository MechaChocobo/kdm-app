'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    TouchableOpacity,
}  from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import styles from '../../style/styles';
import AppText from './app-text';
import ColorContext from '../../context/ColorContext';
import GView from './GView';
import SimpleToast from 'react-native-simple-toast';

export default class PillListItem extends PureComponent {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <GView style={[localStyles.wrapper, this.props.style]}>
                        {this.props.onLongPress && <TouchableOpacity onLongPress={this.onLongPress} onPress={this.pressHint} >
                            {this.getText(colors)}
                        </TouchableOpacity>}
                        {!this.props.onLongPress && this.getText(colors)}
                        {this.props.children}
                        {this.props.icon !== "" && <TouchableOpacity onPress={this.press}>
                            <Icon name={this.props.icon} size={25} color={colors.WHITE}/>
                        </TouchableOpacity>}
                    </GView>
                }
            </ColorContext.Consumer>
        );
    }

    pressHint = () => {
        SimpleToast.show(this.props.longPressHint);
    }

    getText = (colors) => {
        return <AppText style={[styles.title, {flexShrink: 1}]} color={colors.WHITE}>{this.props.title}</AppText>;
    }

    press = () => {
        this.props.onPress(this.props.id);
    }

    onLongPress = () => {
        this.props.onLongPress(this.props.id);
    }
}

PillListItem.propTypes = {
    title: PropTypes.string.isRequired,
    icon: PropTypes.string,
    id: PropTypes.any,
    onPress: PropTypes.func.isRequired,
    onLongPress: PropTypes.func,
    style: PropTypes.any,
    longPressHint: PropTypes.string,
}

PillListItem.defaultProps = {
    subTitle: "",
    style: {},
    disabled: false,
    icon: "",
    longPressHint: '',
}

const localStyles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        alignItems: "center",
        padding: 5,
        paddingLeft: 7,
        paddingRight: 7,
        borderRadius: 500,
        margin: 4,
    },
});
