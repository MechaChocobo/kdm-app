'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';

import {
    View,
    TouchableOpacity,
}  from 'react-native';

import ColorContext from '../../context/ColorContext';
import ListItem from './ListItem';
import styles from '../../style/styles';
import { getExpansionTitle, sortIdArrayByConfigTitle, sortObjectArrayByTitle } from '../../helpers/helpers';
import Icon from './Icon';
import AppText from './app-text';
import { t } from '../../helpers/intl';
import GView from './GView';
import LocationConfig from '../../config/locations.json';

export default class ExpansionStorageContentList extends PureComponent {
    state = {
        openExpansions: {},
        openExpansionLocations: {},
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    this.props.expansions.map((expansion, i) => {
                        return <View key={i}>
                        <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', marginBottom: 5}]} onPress={this.toggleCollapsed(expansion)}>
                            <AppText style={styles.title}>{getExpansionTitle(expansion, this.props.customExpansions)}</AppText>
                            <View style={styles.rowCentered}>
                                <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 13, justifyContent: "center", alignItems: "center"}}>
                                    <AppText style={styles.title} color={colors.WHITE}>{this.getNumberOfItemsInExpansion(this.props.allItems, expansion)}</AppText>
                                </GView>
                                <Icon name={this.state.openExpansions[expansion] === true ? "chevron-down" : "chevron-right"} color={colors.HIGHLIGHT} size={40}/>
                            </View>
                        </TouchableOpacity>
                        {<Collapsible collapsed={this.state.openExpansions[expansion] !== true}>
                            {this.getLocationSections(this.props.allItems, expansion, colors)}
                        </Collapsible>}
                    </View>
                    })
                }
            </ColorContext.Consumer>
        );
    }

    getLocationSections = (allItems, expansion, colors) => {
        if(!this.state.openExpansions[expansion]) {
            return null;
        }

        const sortedLocations = sortIdArrayByConfigTitle(Object.keys(allItems[expansion]), LocationConfig);

        return sortedLocations.map((location, i) => {
                const items = (!this.state.openExpansionLocations[expansion] || this.state.openExpansionLocations[expansion][location] !== true) ? null : Object.values(allItems[expansion][location]).sort(sortObjectArrayByTitle).map((item, i) => {
                    return <ListItem
                        key={i}
                        id={location + ":" + item.id}
                        title={this.props.config[location][item.id].title}
                        onPress={this.selectItem}
                        buttonText={t("Select")}
                    />
                });

                return <View key={i} style={[styles.width90, styles.centered]}>
                <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', marginBottom: 5}]} onPress={this.toggleCollapsedLocation(expansion, location)}>
                    <AppText style={{fontSize: 18, fontWeight: "bold"}}>{LocationConfig[location] ? LocationConfig[location].title : location}</AppText>
                    <View style={styles.rowCentered}>
                        <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 8, justifyContent: "center", alignItems: "center", marginBottom: 4}}>
                            <AppText style={{fontSize: 18, fontWeight: "bold"}} color={colors.WHITE}>{this.getNumberOfItemsInLocation(allItems[expansion], location)}</AppText>
                        </GView>
                        <Icon name={(this.state.openExpansionLocations[expansion] && this.state.openExpansionLocations[expansion][location] === true) ? "chevron-down" : "chevron-right"} color={colors.HIGHLIGHT} size={30}/>
                    </View>
                </TouchableOpacity>
                <Collapsible collapsed={(this.state.openExpansionLocations[expansion] && this.state.openExpansionLocations[expansion][location] !== true)}>
                    {items}
                </Collapsible>
            </View>
        });
    }

    getNumberOfItemsInExpansion = (items, expansion) => {
        let count = 0;

        if(items[expansion]) {
            for(let location in items[expansion]) {
                count += Object.keys(items[expansion][location]).length;
            }
        }

        return count;
    }

    getNumberOfItemsInLocation = (items, location) => {
        let count = 0;

        if(items && items[location]) {
            count += Object.keys(items[location]).length;
        }

        return count;
    }

    toggleCollapsed = (expansion) => {
        return () => {
            let open = {...this.state.openExpansions};
            let openLocations = {...this.state.openExpansionLocations};
            if(open[expansion]) {
                delete open[expansion];
                delete openLocations[expansion];
            } else {
                open[expansion] = true;
                openLocations[expansion] = {};
            }
            this.setState({openExpansions: open, openExpansionLocations: openLocations});
        }
    }

    toggleCollapsedLocation = (expansion, location) => {
        return () => {
            let open = {...this.state.openExpansionLocations};
            if(open[expansion][location]) {
                delete open[expansion][location];
            } else {
                open[expansion][location] = true;
            }
            this.setState({openExpansionLocations: open});
        }
    }

    selectItem = (name) => {
        name = name.split(":");
        this.props.onSelect({location: name[0], id: name[1]});
    }
}

ExpansionStorageContentList.propTypes = {
    expansions: PropTypes.array.isRequired,
    allItems: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
}

ExpansionStorageContentList.defaultProps = {
}
