'use strict';

import React, { Component } from 'react';

import {
    TouchableOpacity,
    StyleSheet,
}  from 'react-native';

import Icon from './Icon';
import ColorContext from '../../context/ColorContext';

export default class MultiPageBackButton extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <TouchableOpacity onPress={this.props.onBack} style={localStyles.backIconStyle}>
                        <Icon name="chevron-left" size={50} color={this.props.color ? this.props.color : colors.TEXT} style={localStyles.transparent}/>
                    </TouchableOpacity>
                }
            </ColorContext.Consumer>
        );
    }
}

MultiPageBackButton.propTypes = {
}

MultiPageBackButton.defaultProps = {
}

const localStyles = StyleSheet.create({
    backIconStyle: {
        position: "absolute",
        top: -6,
        left: 0,
    },
    transparent: {
        backgroundColor:'transparent',
    }
});
