'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';

import {
    View,
    TouchableOpacity,
}  from 'react-native';

import ColorContext from '../../context/ColorContext';
import ListItem from './ListItem';
import styles from '../../style/styles';
import { getExpansionTitle, sortIdArrayByConfigTitle } from '../../helpers/helpers';
import Icon from './Icon';
import AppText from './app-text';
import { t } from '../../helpers/intl';
import GView from './GView';

export default class ExpansionContentList extends PureComponent {
    state = {
        openedSections: {},
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    this.props.expansions.map((expansion, i) => {
                        return <View key={i}>
                        <TouchableOpacity style={[styles.rowCentered, {justifyContent: "space-between", borderBottomWidth: 1, borderColor: 'rgba(256, 256, 256, 0.5)', marginBottom: 5}]} onPress={this.toggleCollapsed(expansion)}>
                            <AppText style={styles.title}>{getExpansionTitle(expansion)}</AppText>
                            <View style={styles.rowCentered}>
                                <GView style={{borderRadius: 500, paddingLeft: 8, paddingRight: 13, justifyContent: "center", alignItems: "center"}}>
                                    <AppText style={styles.title} color={colors.WHITE}>{this.props.allItems[expansion].length}</AppText>
                                </GView>
                                <Icon name={this.state.openedSections[expansion] === true ? "chevron-down" : "chevron-right"} color={colors.HIGHLIGHT} size={40}/>
                            </View>
                        </TouchableOpacity>
                        <Collapsible collapsed={this.state.openedSections[expansion] !== true}>
                            {sortIdArrayByConfigTitle(this.props.allItems[expansion], this.props.config).map((item, i) => {
                                return <ListItem
                                    key={i}
                                    id={item}
                                    title={this.props.config[item].title}
                                    onPress={this.selectItem}
                                    buttonText={t("Select")}
                                />
                            })}
                        </Collapsible>
                    </View>
                    })
                }
            </ColorContext.Consumer>
        );
    }

    toggleCollapsed = (name) => {
        return () => {
            let openedSections = {...this.state.openedSections};

            if(openedSections[name]) {
                delete openedSections[name];
            } else {
                openedSections[name] = true;
            }

            this.setState({openedSections});
        }
    }

    selectItem = (item) => {
        this.props.onSelect(item);
    }
}

ExpansionContentList.propTypes = {
    expansions: PropTypes.array.isRequired,
    allItems: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
}

ExpansionContentList.defaultProps = {
}
