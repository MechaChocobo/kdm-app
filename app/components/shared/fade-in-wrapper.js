import React, { Component } from 'react';
import {Animated, View} from 'react-native';
import PropTypes from 'prop-types';

const ANIMATION_DURATION = 250;
export default class FadeInWrapper extends Component {
    height = 0;
    constructor(props) {
        super(props);

        this._animated = this.props.skipRenderAnimation ? new Animated.Value(1) : new Animated.Value(0);

        this.state = {
            renderComplete: false,
        }
    }

    componentDidMount() {
        if(this.props.skipRenderAnimation) {
            this.setState({renderComplete: true});
        } else {
            Animated.timing(this._animated, {
                toValue: 1,
                duration: this.getAnimationDuration(),
                isInteraction: false,
            }).start(() => {
                this.setState({renderComplete: true});
            });
        }
    }

    getAnimationDuration = () => {
        return this.props.duration ? this.props.duration : ANIMATION_DURATION;
    }

    measureHeight = (e) => {
        this.height = e.nativeEvent.layout.height;
        if(this.props.logHeight === true) {
            console.warn(this.height);
        }
    }

    remove = (callback) => {
        this.setState({remove: true});
        Animated.timing(this._animated, {
            toValue: 0,
            duration: this.getAnimationDuration(),
            isInteraction: false,
        }).start(() => {
            this.setState({remove: false});
            callback();
        });
    }

    render() {
        const animatedStyles = [
            this.props.styles,
        ];

        if(!this.props.skipRenderAnimation && !this.state.renderComplete) {
            animatedStyles.push({
                opacity: this._animated,
            });

            animatedStyles.push({
                height: this._animated.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, this.props.baseHeight],
                    extrapolate: 'clamp',
                }),
            });
        }

        if(this.state.remove === true) {
            animatedStyles.push({
                opacity: this._animated,
            });

            animatedStyles.push({
                height: this._animated.interpolate({
                    inputRange: [0, 1],
                    outputRange: [0, this.height],
                    extrapolate: 'clamp',
                }),
            });
        }

        return (
            <Animated.View style={animatedStyles}>
                <View onLayout={this.measureHeight}>
                    {this.props.children}
                </View>
            </Animated.View>
        )
    }
}

FadeInWrapper.propTypes = {
    baseHeight: PropTypes.number.isRequired,
    styles: PropTypes.any,
    skipRenderAnimation: PropTypes.bool,
    duration: PropTypes.number,
    logHeight: PropTypes.bool,
}

FadeInWrapper.defaultProps = {
    logHeight: false,
}
