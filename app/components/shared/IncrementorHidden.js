'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    View,
    TouchableOpacity,
    Modal,
    TouchableWithoutFeedback,
} from 'react-native';

import ColorContext from '../../context/ColorContext';
import AppText from './app-text';
import styles from '../../style/styles';
import RoundControl from './buttons/RoundControl';
import AppTextInput from './AppTextInput';

const INPUT_WIDTH = 15;
const PADDING = 4;

export class IncrementorHidden extends Component {
    ref = null;
    valueRef = null;

    constructor(props) {
        super(props);

        this.state = {
            editing: false,
            tempText: "",
            xOffset: "",
            yOffset: "",
            valueWidth: 0,
            valueHeight: 0,
        };
    }

    startEditing = () => {
        this.ref.measure(this.setValueOffset);
    }

    setValueOffset = (ox, oy, width, height, px, py) => {
        this.setState({xOffset: px + PADDING, yOffset: py + PADDING});
        this.getValueWidth();
    }

    getValueWidth = () => {
        this.valueRef.measure(this.setValueWidth);
    }

    setValueWidth = (ox, oy, width, height, px, py) => {
        this.setState({editing: true, valueWidth: (width - (PADDING * 2)), valueHeight: (height - PADDING - 1)});
        if(this.props.onEditStart) {
            this.props.onEditStart(this.props.id);
        }
    }

    stopEditing = () => {
        this.setState({editing: false});
        if(this.props.onEditEnd) {
            this.props.onEditEnd(this.props.id);
        }
    }

    render() {
        let inputWidth = this.state.editingText === true ? (this.state.tempText === "" ? INPUT_WIDTH : this.state.tempText.length * INPUT_WIDTH) : this.props.value.toString().length * INPUT_WIDTH;
        let valueStyles = this.props.smallValue ? styles.smallValue : styles.largeValue;
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={styles.rowCentered} ref={(r) => this.ref = r } onLayout={this.devNull}>
                        <TouchableOpacity onPress={this.startEditing} style={localStyles.textPadding} ref={(r) => this.valueRef = r } onLayout={this.devNull}>
                            {!this.state.editing && <AppText style={[valueStyles, localStyles.valuePadding]}>{this.props.value}</AppText>}
                            {this.state.editing && <View style={{width: this.state.valueWidth, height: this.state.valueHeight}}/>}
                        </TouchableOpacity>
                        {this.state.editing && <Modal
                            visible={this.state.editing}
                            transparent={true}
                            onRequestClose={this.stopEditing}
                        >
                            <TouchableWithoutFeedback onPress={this.stopEditing}>
                                <View style={styles.darkOverlay}>
                                    <View style={[styles.selfFlexStart, {marginLeft: this.state.xOffset - 40, marginTop: this.state.yOffset}]}>
                                        <View style={[styles.rowCentered]}>
                                            <RoundControl onPress={this.onDecrease} icon="minus" style={[localStyles.rightPadding]}/>
                                            {!this.props.onChange && <AppText style={[valueStyles, {color: colors.HIGHLIGHT}]}>{this.props.value}</AppText>}
                                            {this.props.onChange && <AppTextInput
                                                style={{marginLeft: 1}}
                                                keyboardType='numeric'
                                                returnKeyType='done'
                                                value={this.state.editingText ? this.state.tempText : "" + this.props.value}
                                                textAlign="center"
                                                onChangeText={this.onTextInputChange}
                                                onBlur={this.submitEdit}
                                                inputStyle={[localStyles.textInput, {color: colors.HIGHLIGHT, width: inputWidth}, valueStyles]}
                                                underlineColor={colors.HIGHLIGHT}
                                            />}
                                            <RoundControl onPress={this.onIncrease} icon="plus" style={localStyles.leftPadding}/>
                                        </View>
                                        {this.props.child && this.props.child()}
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </Modal>}
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    onDecrease = () => {
        this.props.onDecrease(this.props.id);
    }

    onIncrease = () => {
        this.props.onIncrease(this.props.id);
    }

    onTextInputChange = (text) => {
        if(text.charAt(0) === '0') {
            text = text.substr(1);
        }

        if((isNaN(text) || !(text === parseInt(text, 10).toString())) && text !== "") {
            return;
        }

        this.props.onChange(text);
    }

    devNull = () => {}
}

IncrementorHidden.propTypes = {
    value: PropTypes.number.isRequired,
    onChange: PropTypes.func,
    onEditStart: PropTypes.func,
    onEditEnd: PropTypes.func,
    id: PropTypes.string,
    onIncrease: PropTypes.func,
    onDecrease: PropTypes.func,
    smallValue: PropTypes.bool,
}

IncrementorHidden.defaultProps = {
    id: "",
    smallValue: false,
}

var localStyles = StyleSheet.create({
    rightPadding: {
        paddingRight: 10,
    },
    leftPadding: {
        paddingLeft: 10,
    },
    textInput: {
        padding: 0,
    },
    underline: {
        borderBottomWidth: 1,
    },
    textPadding: {
        padding: PADDING,
        paddingBottom: 1,
    },
    valuePadding: {
        paddingBottom: 2,
    },
});
