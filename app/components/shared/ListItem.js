'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    StyleSheet,
    View,
}  from 'react-native';

import GView from './GView';
import Button from './button';
import styles from '../../style/styles';
import AppText from './app-text';
import ColorContext from '../../context/ColorContext';

export default class ListItem extends Component {
    render() {
        let combinedStyles = [styles.rowCentered, localStyles.wrapper]
        if(this.props.style && Object.keys(this.props.style).length > 0) {
            combinedStyles.push(this.props.style);
        }

        return (
            <ColorContext.Consumer>
                {colors =>
                    <GView style={combinedStyles}>
                        <View style={localStyles.textContainer}>
                            <View style={styles.rowCentered}>
                                <AppText style={styles.title} color={colors.WHITE}>{this.props.title}</AppText>
                                {this.props.children}
                            </View>
                            {this.props.subTitle !== "" && <AppText style={localStyles.subTitle} color={colors.WHITE}>{this.props.subTitle}</AppText>}
                        </View>
                        <View style={styles.rowCentered}>
                            {this.props.preButton !== undefined ? this.props.preButton : null}
                            {this.props.buttonText && <Button
                                shape="pill"
                                color={colors.WHITE}
                                textColor={colors.HIGHLIGHT}
                                title={this.props.buttonText}
                                onPress={this.press}
                            />}
                            {this.props.controlElement !== undefined ? this.props.controlElement : null}
                        </View>
                    </GView>
                }
            </ColorContext.Consumer>
        );
    }

    press = () => {
        this.props.onPress(this.props.id);
    }
}

ListItem.propTypes = {
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
    id: PropTypes.any,
    onPress: PropTypes.func,
    buttonText: PropTypes.string,
    styles: PropTypes.object,
    disabled: PropTypes.bool,
    controlElement: PropTypes.node,
    preButton: PropTypes.node,
}

ListItem.defaultProps = {
    subTitle: "",
    styles: {},
    disabled: false,
}

const localStyles = StyleSheet.create({
    wrapper: {
        justifyContent: "space-between",
        flex: 1,
        padding: 10,
        borderRadius: 10,
        marginBottom: 10,
    },
    subTitle: {
        fontStyle: "italic"
    },
    textContainer: {
        flexShrink: 1
    }
});
