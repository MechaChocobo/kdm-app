'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    TouchableOpacity,
    View,
    StyleSheet,
    Alert,
}  from 'react-native';

import Text from './app-text';
import Icon from './Icon';
import Zebra from './Zebra';
import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';
import { t } from '../../helpers/intl';

export default class AbilityText extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={[localStyles.wrapper, {width: this.props.width, minHeight: this.props.height}]}>
                        <Zebra zebra={true} style={localStyles.zebra}>
                            <Text style={styles.wrappedTextItemTitle}>{this.props.title}</Text>
                            <Text style={[styles.icomoon, localStyles.text]}>{this.props.description}</Text>
                            {this.props.showClose && <TouchableOpacity onPress={this.onPress} style={{
                                    position: "absolute",
                                    top: 2,
                                    right: 5,
                                    padding: 5
                                }}>
                                <Icon name="close" color={colors.HIGHLIGHT} size={20} />
                            </TouchableOpacity>}
                        </Zebra>
                    </View>
                }
            </ColorContext.Consumer>
        );
    }

    onPress = () => {
        if(this.props.onPress) {
            if(this.props.confirm) {
                Alert.alert(t("Are you sure?"), t("Are you sure you want to remove this?"), [
                    {text: t("Cancel"), onPress: () => {}},
                    {text: t("Confirm"), onPress: this.sendPress}
                ]);
            } else {
                this.sendPress();
            }
        }
    }

    sendPress = () => {
        this.props.onPress(this.props.id);
    }
}

AbilityText.propTypes = {
    id: PropTypes.string,
    title: PropTypes.string.isRequired,
    description: PropTypes.any.isRequired,
    width: PropTypes.string,
    height: PropTypes.number,
    showClose: PropTypes.bool,
    confirm: PropTypes.bool,
}

AbilityText.defaultProps = {
    width: '100%',
    height: 50,
    showClose: true,
    confirm: true,
}

const localStyles = StyleSheet.create({
    wrapper: {
        borderRadius: 5,
        alignItems: "center",
        minHeight: 100,
        overflow: "hidden",
        marginBottom: 5,
    },
    text: {
        lineHeight: 20,
    },
    zebra: {
        padding: 5,
        width: "100%",
    }
});
