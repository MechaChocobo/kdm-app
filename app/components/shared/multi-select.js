'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

var {
    View,
} = require('react-native');

import SelectItem from './select-item';

export default class MultiSelect extends Component {
    render() {
        return (
            <View>
                {this.renderItems()}
            </View>
        );
    }

    renderItems = () => {
        return this.props.items.map((item, i) => {
            return <SelectItem
                key={i}
                value={item.name}
                title={item.title}
                selected={this.props.selected.indexOf(item.name) !== -1}
                onUnselected={this.onUnselected}
                onSelected={this.onSelected}
            />
        });
    }

    onUnselected = (value) => {
        let newValues = [...this.props.selected];
        newValues.splice(newValues.indexOf(value), 1);

        this.props.onChange(newValues);
    }

    onSelected = (value) => {
        let newValues = [...this.props.selected];
        newValues.push(value);

        this.props.onChange(newValues);
    }

    devNull = () => {}
}

MultiSelect.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
    selected: PropTypes.arrayOf(PropTypes.string),
    onChange: PropTypes.func.isRequired,
}
