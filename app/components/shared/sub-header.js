'use strict';

import React, {Component} from 'react';
import {
    View,
    TouchableOpacity
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from '../../style/styles';
import Text from './app-text';
import ColorContext from '../../context/ColorContext';
import Hr from './Hr';

export default class SubHeader extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={{marginTop: 10}}>
                        <View style={{flexDirection: "row"}}>
                            {this.renderTitle(colors)}
                            {this.props.children}
                        </View>
                        <Hr slim={true}/>
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    renderTitle = (colors) => {
        if(this.props.onPress) {
            return (
                <TouchableOpacity onPress={this.props.onPress}>
                    <View style={{flexDirection: "row", alignItems: "center", flexShrink: 1}}>
                        <Icon name={this.props.icon} color={this.props.iconColor ? this.props.iconColor : colors.HIGHLIGHT} size={18}/>
                        <Text style={this.getTitleStyle()}> {this.props.title}</Text>
                    </View>
                </TouchableOpacity>
            )
        } else {
            return (
                <Text style={this.getTitleStyle()}> {this.props.title}</Text>
            )
        }
    }

    getTitleStyle = () => {
        if(this.props.titleWeight === "heavy") {
            return styles.textHeavy;
        } else if(this.props.titleWeight === "light") {
            return styles.itemText;
        }
    }
}

SubHeader.propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    icon: PropTypes.string,
    iconColor: PropTypes.string,
    titleWeight: PropTypes.string,
    tooltip: PropTypes.string,
}

SubHeader.defaultProps = {
    icon: "pencil",
    titleWeight: "heavy",
    tooltip: "",
}
