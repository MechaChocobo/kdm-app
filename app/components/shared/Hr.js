'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
} from 'react-native';

import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';

export default class Hr extends Component {
    render() {
        return (
            <ColorContext.Consumer>
            { colors => {
                let style = [];

                if(this.props.slim === true) {
                    style.push(styles.hrSlim);
                } else {
                    style.push(styles.hr);
                }

                style.push({borderColor: (colors.isDark ? "rgba(256, 256, 256, 0.5)" : "rgba(0, 0, 0, 0.5)")});

                if(Array.isArray(this.props.style)) {
                    style = style.concat(this.props.style);
                } else {
                    style.push(this.props.style);
                }


                return <View style={style} />
            }}
            </ColorContext.Consumer>
        );
    }
}

Hr.propTypes = {
    slim: PropTypes.bool,
    style: PropTypes.any,
}

Hr.defaultProps = {
    slim: false,
    style: [],
}
