'use strict';

import React, {Component} from 'react';
import {
    Text,
    Platform
} from 'react-native';

import ColorContext from '../../context/ColorContext';

const fontFamily = Platform.select({
    ios: "Helvetica",
    android: ""
});

export default class AppText extends Component {
    shouldComponentUpdate(nextProps) {
        // only update if color is changing
        if(nextProps.color && nextProps.color !== this.props.color) {
            return true;
        }

        if(nextProps.children !== this.props.children) {
            return true;
        }

        return false;
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors => {
                    let style = this.props.style;
                    if(!Array.isArray(style)) {
                        style = [style];
                    }

                    if(this.props.color) {
                        style.push({color: this.props.color});
                    }

                    return <Text {...this.props} style={[{fontFamily, color: colors.TEXT}, ...style]}>
                        {this.props.children}
                    </Text>
                }}
            </ColorContext.Consumer>
        )
    }
}
