'use strict';

import React, { Component } from 'react';
import {View, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import Icon from './Icon';
import styles from '../../style/styles';
import ColorContext from '../../context/ColorContext';

export default class MilestoneDots extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={[styles.rowCentered, localStyles.wrapper]}>
                        {this.getDots(colors)}
                    </View>
                }
            </ColorContext.Consumer>
        );
    }

    getDots = (colors) => {
        let dots = [];

        for(let i = 0; i < this.props.max; i++) {
            let size = 10;
            let icon = i < this.props.currentValue ? 'checkbox-blank-circle' : 'checkbox-blank-circle-outline';
            let color = colors.TEXT;
            if(this.props.milestones.indexOf(i + 1) !== -1) {
                size = 12;
                color = colors.HIGHLIGHT;
            }

            dots.push(<Icon key={i} name={icon} color={color} size={size} style={localStyles.marginRight}/>);
        }

        return dots;
    }
}

MilestoneDots.propTypes = {
    milestones: PropTypes.array,
    max: PropTypes.number.isRequired,
    currentValue: PropTypes.number.isRequired,
}

MilestoneDots.defaultProps = {
    milestones: [],
}

const localStyles = StyleSheet.create({
    marginRight: {
        marginRight: 3
    },
    wrapper: {
        marginTop: 5,
        marginBottom: 5
    }
});
