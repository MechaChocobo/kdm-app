'use strict';

import React, { Component } from 'react';

import {
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native';

import Text from '../../components/shared/app-text';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { CHECKBOX_SIZE } from '../../helpers/helpers';
import ColorContext from '../../context/ColorContext';

export class ToggleList extends Component {
    renderItems = (colors) => {
        return this.props.items.map((name, i) => {
            if(name == this.props.selected) {
                return <TouchableOpacity key={i} onPress={this.props.onDeselect} style={{alignSelf: "flex-start"}}>
                    <View style={styles.wrapper}>
                        <Icon name="checkbox-blank" size={CHECKBOX_SIZE} color={colors.TEXT}/>
                        <Text style={styles.activeItemText}>{this.props.config[name].title}</Text>
                    </View>
                </TouchableOpacity>
            } else {
                return <TouchableOpacity key={i} onPress={this.props.onSelect(name)} style={{alignSelf: "flex-start"}}>
                    <View style={styles.wrapper}>
                        <Icon name="checkbox-blank-outline" size={CHECKBOX_SIZE} color={colors.TEXT}/>
                        <Text style={styles.inactiveItemText}>{this.props.config[name].title}</Text>
                    </View>
                </TouchableOpacity>
            }
        });
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View>
                        {this.renderItems(colors)}
                    </View>
                }
            </ColorContext.Consumer>
        );
    }
}

var styles = StyleSheet.create({
    wrapper: {
        flexDirection: "row",
        margin: 8,
        alignItems: "center"
    },
    activeItemText: {
        fontSize: 16,
        fontWeight: "300"
    },
    inactiveItemText: {
        fontSize: 16,
        fontWeight: "300"
    }
});
