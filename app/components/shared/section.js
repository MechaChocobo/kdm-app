import React, { Component } from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Text from './app-text';
import sharedStyles from '../../style/styles';
import ColorContext from '../../context/ColorContext';
import Hr from './Hr';

export default class Section extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={localStyles.wrapper}>
                        {this.renderTitle(colors)}
                        <Hr slim={true}/>
                        <View style={localStyles.contentWrapper}>
                            {this.props.children}
                        </View>
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    renderTitle = (colors) => {
        if(this.props.icon) {
            return <View>
                <TouchableOpacity onPress={this.props.onPress}>
                    <View style={sharedStyles.rowCentered}>
                        <Icon name={this.props.icon} color={colors.HIGHLIGHT} size={22}/>
                        <Text style={sharedStyles.title}>{this.props.title}</Text>
                    </View>
                </TouchableOpacity>
                <View />
            </View>
        } else {
            return <Text style={sharedStyles.title}>{this.props.title}</Text>
        }
    }
}

Section.propTypes = {
    icon: PropTypes.string,
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    rule: PropTypes.string,
}

Section.defaultProps = {
    rule: "weak"
}

let localStyles = StyleSheet.create({
    wrapper: {
    },
    contentWrapper: {
        marginLeft: 3,
    }
});
