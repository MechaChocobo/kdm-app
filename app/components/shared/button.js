'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    ActivityIndicator,
    TouchableOpacity,
    StyleSheet,
}  from 'react-native';

import Text from './app-text';
import ColorContext from '../../context/ColorContext';

export default class Button extends Component {
    render() {
        let textStyle = {};

        if(this.props.shape === "pill" && !this.props.fatPill) {
            textStyle = localStyles.pillText;
        }

        return (
            <ColorContext.Consumer>
                {colors =>
                    <TouchableOpacity onPress={(this.props.disabled || this.props.loading) ? this.onDisablePress : this.onPress} style={this.getContainerStyle(colors)}>
                        {this.props.loading && <ActivityIndicator size="small" color={colors.HIGHLIGHT}/>}
                        {this.props.children !== undefined ? this.props.children : null}
                        {this.props.title && <Text style={[localStyles.text, textStyle, this.props.textStyle]} color={this.getTextColor(colors)}>{this.props.title.toUpperCase()}</Text>}
                    </TouchableOpacity>
                }
            </ColorContext.Consumer>
        );
    }

    getTextColor = (colors) => {
        if(this.props.skeleton) {
            return this.getButtonColor(colors);
        } else {
            return this.props.textColor;
        }
    }

    getButtonColor = (colors) => {
        return (this.props.disabled || this.props.loading) ? colors.GRAY : (this.props.color ? this.props.color : colors.HIGHLIGHT);
    }

    onPress = () => {
        if(this.props.id) {
            this.props.onPress(this.props.id);
        } else {
            this.props.onPress();
        }
    }

    getContainerStyle = (colors) => {
        let baseStyle = {
            backgroundColor: this.getButtonColor(colors),
            justifyContent: "center",
            flexDirection: "row",
            alignItems: "center",
        };

        if(this.props.fitText) {
            baseStyle.alignSelf = "flex-start";
        }

        if(this.props.skeleton === true) {
            baseStyle.borderColor = baseStyle.backgroundColor;
            baseStyle.backgroundColor = "transparent";
            baseStyle.borderWidth = 3;
        }

        let style = [baseStyle];
        if(this.props.shape === "rounded") {
            style.push(localStyles.roundedButton);
        } else if(this.props.shape === "pill") {
            style.push(localStyles.pillButton);
        }

        if(Array.isArray(this.props.style)) {
            style = style.concat(this.props.style);
        } else if(this.props.style) {
            style.push(this.props.style);
        }

        return style;
    }

    onDisablePress = () => {
        if(this.props.loading) {
            return;
        }

        if(this.props.onDisablePress) {
            this.props.onDisablePress(this.props.id);
        }
    }
}

Button.propTypes = {
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    onPress: PropTypes.func.isRequired,
    onDisablePress: PropTypes.func,
    color: PropTypes.string,
    title: PropTypes.string,
    shape: PropTypes.string,
    style: PropTypes.any,
    textColor: PropTypes.string,
    textStyle: PropTypes.object,
    fitText: PropTypes.bool,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    skeleton: PropTypes.bool,
    fatPill: PropTypes.bool,
}

Button.defaultProps = {
    disabled: false,
    loading: false,
    shape: "square",
    textColor: "white",
    textStyle: {},
    fitText: false,
    skeleton: false,
    fatPill: false,
}

const localStyles = StyleSheet.create({
    text: {
        fontSize: 14,
        fontWeight: "bold",
        padding: 10,
        marginLeft: 5,
    },
    roundedButton: {
        borderRadius: 10,
        overflow: "hidden",
    },
    pillButton: {
        borderRadius: 500,
        overflow: "hidden",
    },
    pillText: {
        paddingTop: 6,
        paddingBottom: 6
    }
});
