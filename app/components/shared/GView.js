'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';

import ColorContext from '../../context/ColorContext';

export default class GView extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <LinearGradient
                        style={this.props.style}
                        colors={this.getColors(colors)}
                        {...this.getGradientDirection()}
                    >
                        {this.props.children}
                    </LinearGradient>
                }
            </ColorContext.Consumer>
        );
    }

    getColors = (colors) => {
        if(this.props.colorType === "default") {
            return [colors.GRADIENT_START, colors.GRADIENT_END];
        } else if(this.props.colorType === "background") {
            return [colors.GRADIENT_START_BACKGROUND, colors.GRADIENT_END_BACKGROUND];
        } else if(this.props.colorType === "banner") {
            return [colors.GRADIENT_START_BANNER, colors.GRADIENT_END_BANNER];
        }
    }

    getGradientDirection = () => {
        if(this.props.direction === "horizontal") {
            return {start:{x: 0, y: 0}, end:{x: 1, y: 0}};
        } else if(this.props.direction === "diagonal") {
            return {start:{x: 0, y: 0}, end:{x: 1, y: 1}};
        }
    }
}

GView.propTypes = {
    direction: PropTypes.string,
    style: PropTypes.any,
    colorType: PropTypes.string,
}

GView.defaultProps = {
    direction: "horizontal",
    colorType: "default",
}
