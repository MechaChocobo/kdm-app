'use strict';

import React, { Component } from 'react';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    View,
    TouchableOpacity
} from 'react-native';

import Text from './app-text';
import sharedStyles from '../../style/styles';
import ColorContext from '../../context/ColorContext';

export default class CheckboxToggle extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <TouchableOpacity onPress={this.onPress}>
                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <Icon name={this.props.selected === true ? "checkbox-blank" : "checkbox-blank-outline"} size={15} color={colors.TEXT}/>
                            <Text style={sharedStyles.itemText}>{this.props.text}</Text>
                        </View>
                    </TouchableOpacity>
                }
            </ColorContext.Consumer>
        );
    }

    onPress = () => {
        this.props.onPress(this.props.value);
    }
}
