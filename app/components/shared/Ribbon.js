'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
} from 'react-native';

import Text from './app-text';
import ColorContext from '../../context/ColorContext';

export default class Ribbon extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={localStyles.main}>
                        <View style={[localStyles.ribbon, {borderBottomColor: this.props.backgroundColor, borderTopColor: this.props.backgroundColor}]} />
                        <View style={[localStyles.ribbonBody, {backgroundColor: this.props.backgroundColor}]}><Text color={colors.WHITE}>{this.props.text}</Text></View>
                    </View>
                }
            </ColorContext.Consumer>
        );
    }
}

Ribbon.propTypes = {
    text: PropTypes.string,
    backgroundColor: PropTypes.string.isRequired,
}

const localStyles = StyleSheet.create({
    main: {
        flexDirection: "row",
        height: 24,
    },
    ribbonBody: {
        padding: 2,
        paddingRight: 5,
    },
    ribbon: {
        width: 0,
        height: 0,
        borderLeftWidth: 15,
        borderLeftColor: 'transparent',
        borderBottomWidth: 12,
        borderTopWidth: 12,
        // shadowColor: '#000000',
        // shadowRadius: 5,
        // shadowOpacity: 1.0,
    }
});
