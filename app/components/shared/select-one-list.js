'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Collapsible from 'react-native-collapsible';
import {View, StyleSheet} from 'react-native';

import Button from './button';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import Hr from './Hr';
import AppTextInput from './AppTextInput';

export default class SelectOneList extends Component {
    input;

    constructor(props) {
        super(props);

        this.state = {
            showCustomInput: false,
            customText: "",
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View>
                        {this.renderItems(colors)}
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    renderItems = (colors) => {
        let items = this.props.items.map((item, i) => {
            return(
                <View key={i} style={localStyles.button}>
                    <Button onPress={this.onSelect(item.name)} title={item.title} color={colors.HIGHLIGHT}/>
                </View>
            )
        });

        if(this.props.allowCustom === true) {
            items.push(
                <View style={{marginTop: 20}} key={-1}>
                    <Hr slim={true}/>
                    <View height={20}/>
                    <Collapsible collapsed={!this.state.showCustomInput}>
                        <View style={{width: "70%", alignSelf: "center"}}>
                            <AppTextInput
                                textRef={(ref) => this.input = ref}
                                inputStyle={localStyles.input}
                                value={this.state.customText}
                                onChangeText={this.customTextChange}
                                placeholder={t("Enter Name")}
                            />
                        </View>
                    </Collapsible>
                    <View style={localStyles.button}>
                        <Button onPress={this.state.showCustomInput ? this.submitCustom : this.enterCustom} title={this.state.showCustomInput ? t("Select Custom") : t("Enter Custom")}/>
                    </View>
                </View>
            );
        }

        return items;
    }

    customTextChange = (text) => {
        this.setState({customText: text});
    }

    enterCustom = () => {
        this.setState({showCustomInput: true});
    }

    onSelect = (val) => {
        return () => {
            this.props.onChange(val);
        }
    }

    submitCustom = () => {
        this.props.onChange(this.state.customText);
        this.setState({customText: "", showCustomInput: false});
    }
}

const localStyles = StyleSheet.create({
    button: {
        borderRadius: 10,
        marginTop: 5,
        marginBottom: 5,
        width: "70%",
        alignSelf: "center",
        overflow: "hidden",
    },
    input: {
        fontSize: 16,
        padding: 0,
    },
});

SelectOneList.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object),
    onChange: PropTypes.func.isRequired,
    allowCustom: PropTypes.bool,
}

SelectOneList.defaultProps = {
    allowCustom : false
}
