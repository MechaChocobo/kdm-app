'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
    ImageBackground,
} from 'react-native';

import Text from './app-text';
import ColorContext from '../../context/ColorContext';

export default class StripedRibbon extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={localStyles.main}>
                        <ImageBackground source={require('../../images/ribbon.png')} style={{width: '100%', height: '100%'}}>
                            <Text style={localStyles.text} color={colors.WHITE}>{this.props.text}</Text>
                        </ImageBackground>
                    </View>
                }
            </ColorContext.Consumer>
        );
    }
}

StripedRibbon.propTypes = {
    text: PropTypes.string,
}

const localStyles = StyleSheet.create({
    main: {
        height: 24,
    },
    text: {
        margin: 2,
        marginRight: 5,
        marginLeft: 10
    }
});
