'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
} from 'react-native';

import ColorContext from '../../context/ColorContext';
import Icon from './Icon';
import styles from '../../style/styles';

export default class PageIndicator extends Component {
    render() {
        return (
            <ColorContext.Consumer>
            { colors => {
                return <View style={styles.rowCentered}>
                    {this.renderDots(colors)}
                </View>
            }}
            </ColorContext.Consumer>
        );
    }

    renderDots = (colors) => {
        let dots = [];

        for(let i = 0; i < this.props.numPages; i++) {
            let size = 10;
            let icon = (i == this.props.currentPage ? 'checkbox-blank-circle' : 'checkbox-blank-circle-outline');
            let color = colors.HIGHLIGHT;
            if(i == this.props.currentPage) {
                size = 12;
            }

            dots.push(<Icon key={i} name={icon} color={color} size={size} style={localStyles.marginRight}/>);
        }

        return dots;
    }
}

PageIndicator.propTypes = {
    numPages: PropTypes.number.isRequired,
    currentPage: PropTypes.number.isRequired,
}

PageIndicator.defaultProps = {
}

const localStyles = StyleSheet.create({
    marginRight: {
        marginRight: 3
    }
});
