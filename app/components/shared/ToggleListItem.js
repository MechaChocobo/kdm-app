'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Switch from 'react-native-switch-pro';

import {
    View,
    StyleSheet,
}  from 'react-native';

import GView from './GView';
import styles from '../../style/styles';
import AppText from './app-text';
import ColorContext from '../../context/ColorContext';

export default class ToggleListItem extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <GView style={[styles.rowCentered, localStyles.wrapper]}>
                        <View style={localStyles.textContainer}>
                            <AppText style={styles.title} color={colors.WHITE}>{this.props.title}</AppText>
                            {this.props.subTitle !== "" && <AppText style={styles.italic} color={colors.WHITE}>{this.props.subTitle}</AppText>}
                        </View>
                        <Switch
                            value={this.props.value}
                            onSyncPress={this.toggleItem}
                            circleColorActive={colors.HIGHLIGHT}
                            backgroundActive={colors.GRAY}
                            backgroundInactive={colors.GRAY}
                        />
                    </GView>
                }
            </ColorContext.Consumer>
        );
    }

    toggleItem = () => {
        this.props.onToggle(this.props.id);
    }
}

ToggleListItem.propTypes = {
    title: PropTypes.string.isRequired,
    id: PropTypes.any,
    onToggle: PropTypes.func.isRequired,
    value: PropTypes.bool
}

ToggleListItem.defaultProps = {
    subTitle: '',
}

const localStyles = StyleSheet.create({
    wrapper: {
        justifyContent: "space-between",
        padding: 10,
        borderRadius: 10,
        marginBottom: 10,
    },
    textContainer: {
        flexShrink: 1
    }
});
