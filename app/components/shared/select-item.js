'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Text from './app-text';
import styles from '../../style/styles';

export default class SelectItem extends Component {
    render() {
        return(
            <TouchableOpacity onPress={this.onPress}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <Icon name={this.props.selected ? "checkbox-marked" : "checkbox-blank-outline"} color="white" size={18}/>
                    <View style={{paddingLeft: 5, paddingBottom: 3}}><Text style={styles.itemText}>{this.props.title}</Text></View>
                </View>
            </TouchableOpacity>
        );
    }

    onPress = () => {
        if(this.props.selected) {
            this.props.onUnselected(this.props.value);
        } else {
            this.props.onSelected(this.props.value);
        }
    }
}

SelectItem.propTypes = {
    value: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    selected: PropTypes.bool.isRequired,
    onUnselected: PropTypes.func.isRequired,
    onSelected: PropTypes.func.isRequired,
}
