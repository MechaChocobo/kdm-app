'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import ColorContext from '../../context/ColorContext';
import GradientIcon from './GradientIcon';
import Icon from './Icon';

export default class RoundedIcon extends Component {
    render() {
        return (
            <ColorContext.Consumer>
            { colors =>{
                let style = {
                    width: (this.props.borderSize ? this.props.borderSize + this.props.extraBorder : this.props.size + 10 + this.props.extraBorder),
                    height: (this.props.borderSize ? this.props.borderSize + this.props.extraBorder : this.props.size + 10 + this.props.extraBorder),
                    borderRadius: (this.props.borderSize ? this.props.borderSize + this.props.extraBorder : this.props.size + 10 + this.props.extraBorder) / 2,
                    alignItems: "center",
                    justifyContent: "center",
                    overflow: "hidden"
                };

                let icon = null;

                if(this.props.gradient) {
                    icon = <GradientIcon
                        style={style}
                        icon={this.props.icon}
                        iconColor={this.props.iconColor}
                        size={this.props.iconSize ? this.props.iconSize : this.props.size}
                    />
                } else {
                    icon = <View style={[style, {backgroundColor: this.props.color ? this.props.color : colors.HIGHLIGHT}]}>
                        <Icon name={this.props.icon} size={this.props.iconSize ? this.props.iconSize : this.props.size} color={this.props.iconColor}/>
                    </View>
                }

                return <View style={this.props.style}>
                    {this.props.onPress && <TouchableOpacity onPress={this.props.disabled ? this.devNull : this.props.onPress}>
                        {icon}
                    </TouchableOpacity>}
                    {!this.props.onPress && icon}
                </View>
            }}
            </ColorContext.Consumer>
        );
    }

    devNull = () => {}
}

RoundedIcon.propTypes = {
    disabled: PropTypes.bool,
    onPress: PropTypes.func,
    color: PropTypes.string,
    borderSize: PropTypes.number,
    icon: PropTypes.string.isRequired,
    size: PropTypes.number,
    iconColor: PropTypes.string,
    gradient: PropTypes.bool,
    iconSize: PropTypes.number,
    extraBorder: PropTypes.number,
}

RoundedIcon.defaultProps = {
    disabled: false,
    size: 15,
    iconColor: "white",
    gradient: false,
    extraBorder: 0,
}
