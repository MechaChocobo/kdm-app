'use strict';

import React, { Component } from 'react';

import {
    View,
    TouchableOpacity,
} from 'react-native';

import Icon from './Icon';
import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';
import AppText from './app-text';

export default class NavButton extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <TouchableOpacity onPress={this.props.onPress}>
                        <View style={[styles.centeredItems, {paddingTop: 2}]}>
                            {this.props.icon && <Icon name={this.props.icon} color={this.getColor(colors)} size={30}/>}
                            <AppText color={colors.NAV_BAR_TEXT}>{this.props.label}</AppText>
                        </View>
                    </TouchableOpacity>
                }
            </ColorContext.Consumer>
        );
    }

    getColor = (colors) => {
        if(this.props.selected) {
            return colors.HIGHLIGHT;
        }

        return colors.NAV_BAR_ICON;
    }
}
