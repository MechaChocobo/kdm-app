'use strict';

import React, { Component } from 'react';

import {
    Platform,
    Keyboard,
} from 'react-native';


export default class KeyboardHider extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hide: false,
        }
    }

    componentDidMount() {
        if(Platform.OS === "android") {
            this.keyboardDidShowListener = Keyboard.addListener(
                'keyboardDidShow',
                this._keyboardDidShow,
            );

            this.keyboardDidHideListener = Keyboard.addListener(
                'keyboardDidHide',
                this._keyboardDidHide,
            );
        }
    }

    _keyboardDidShow = () => {
        this.setState({hide: true});
    }

    _keyboardDidHide = () => {
        this.setState({hide: false});
    }

    componentWillUnmount() {
        if(Platform.OS === "android") {
            this.keyboardDidShowListener.remove();
            this.keyboardDidHideListener.remove();
        }
    }

    render() {
        if(this.state.hide === true) {
            return null;
        }

        return this.props.children;
    }
};
