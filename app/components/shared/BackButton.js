'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    TouchableOpacity,
    StyleSheet,
}  from 'react-native';

import { goBack } from '../../actions/router';
import Icon from './Icon';
import { getColors } from '../../selectors/general';

class BackButton extends Component {
    render() {
        if(this.props.numPages > 1) {
            return (
                <TouchableOpacity onPress={this.props.goBack} style={localStyles.backIconStyle}>
                    <Icon name="chevron-left" size={50} color={this.props.color ? this.props.color : this.props.colors.TEXT} style={localStyles.transparent}/>
                </TouchableOpacity>
            );
        } else {
            return null;
        }
    }
}

BackButton.propTypes = {
}

BackButton.defaultProps = {
}

function mapStateToProps(state, props) {
    return {
        numPages: state.pageReducer.page.length,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        goBack,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(BackButton);

const localStyles = StyleSheet.create({
    backIconStyle: {
        position: "absolute",
        top: -6,
        left: 0,
        zIndex:10,
    },
    transparent: {
        backgroundColor:'transparent',
    }
});
