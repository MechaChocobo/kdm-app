'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
    TextInput,
    Platform,
}  from 'react-native';

import ColorContext from '../../context/ColorContext';

export default class AppTextInput extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value,
            editing: false,
        };
    }

    componentDidUpdate(prevProps) {
        if(this.props.value !== prevProps.value) {
            this.setState({value: this.props.value});
        }
    }

    render() {
        let style = [localStyles.input];
        if(Array.isArray(this.props.inputStyle)) {
            style = style.concat(this.props.inputStyle);
        } else {
            style.push(this.props.inputStyle);
        }

        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={this.props.style}>
                        <TextInput
                            style={[{color: colors.TEXT}, [...style]]}
                            value={this.state.editing === true ? this.state.value : this.props.value}
                            onChangeText={this.onValueChange}
                            keyboardType={this.props.keyboardType}
                            returnKeyType={this.props.returnKeyType}
                            textAlign={this.props.textAlign}
                            multiline={this.props.multiline}
                            placeholder={this.props.placeholder}
                            placeholderTextColor={colors.GRAY}
                            ref={this.props.textRef}
                            onBlur={this.onBlur}
                            onFocus={this.onFocus}
                        />
                        <View style={{borderColor: (this.props.underlineColor ? this.props.underlineColor : colors.TEXT), borderBottomWidth: 1}}/>
                    </View>
                }
            </ColorContext.Consumer>
        );
    }

    onValueChange = (val) => {
        if(this.props.keyboardType === "numeric") {
            if(val.charAt(0) === '0') {
                val = val.substr(1);
            }

            if((isNaN(val) || !(val === parseInt(val, 10).toString())) && val !== "") {
                return;
            }
        }

        this.setState({value: val});

        if(this.props.realtimeChange) {
            this.props.onChangeText(val);
        }
    }

    onFocus = () => {
        this.setState({editing: true});
    }

    onBlur = () => {
        if(this.props.onBlur) {
            this.props.onBlur();
        }

        this.props.onChangeText(this.state.value);
        this.setState({editing: false});
    }
}

AppTextInput.propTypes = {
    value: PropTypes.string.isRequired,
    onChangeText: PropTypes.func.isRequired,
    onBlur: PropTypes.func,
    keyboardType: PropTypes.string,
    underlineColor: PropTypes.string,
    textAlign: PropTypes.string,
    returnKeyType: PropTypes.string,
    multiline: PropTypes.bool,
    inputStyle: PropTypes.any,
    placeholder: PropTypes.string,
    realtimeChange: PropTypes.bool,
}

AppTextInput.defaultProps = {
    keyboardType: 'default',
    textAlign: 'left',
    returnKeyType: "done",
    multiline: false,
    placeholder: '',
    realtimeChange: false,
}

const localStyles = StyleSheet.create({
    input: {
        textAlign: "left",
        fontSize: 20,
        padding: 0
    }
});
