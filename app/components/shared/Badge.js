'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RNTooltips from 'react-native-tooltips';
import SimpleToast from 'react-native-simple-toast';

import {
    View,
    TouchableOpacity,
    StyleSheet,
    Platform,
} from 'react-native';
import ColorContext from '../../context/ColorContext';
import Icon from './Icon';

export default class Badge extends Component {
    tipElement = null;
    colors = null;

    render() {
        return (
            <ColorContext.Consumer>
                {colors => {
                    this.colors = colors;
                    return <TouchableOpacity
                            onPress={this.props.invertPress ? this.onLongPress : this.showTip}
                            ref={this.setRef}
                            style={localStyles.container}
                            onLongPress={this.props.invertPress ? this.showTip : this.onLongPress}
                            delayLongPress={250}
                        >
                        {this.getBadge(colors)}
                    </TouchableOpacity>
                }}
            </ColorContext.Consumer>
        );
    }

    setRef = (ref) => {
        this.tipElement = ref;
    }

    getBadge = (colors) => {
        return <View style={[localStyles.main, {backgroundColor:this.props.color}]}>
            {!this.props.icon && <View style={[localStyles.dot, {backgroundColor: this.props.centerColor}]}/>}
            {this.props.icon && <Icon name={this.props.icon} size={this.props.iconSize} color={colors.WHITE}/>}
            {this.props.active === false && <View style={localStyles.shadow} />}
        </View>
    }

    showTip = () => {
        if(this.props.toast) {
            SimpleToast.show(this.props.tooltip);
        } else {
            let args = {
                text: this.props.tooltip,
                tintColor: this.colors.GRADIENT_START_BACKGROUND,
                textColor: this.colors.TEXT,
            };

            if(Platform.OS === "ios") {
                args.corner = 5;
            }

            RNTooltips.Show(this.tipElement, this.props.tipParent, args);
        }
    }

    onLongPress = () => {
        if(!this.props.onLongPress) {
            return;
        }

        this.props.onLongPress();
    }
}

Badge.propTypes = {
    tooltip: PropTypes.string,
    tipParent: PropTypes.any,
    icon: PropTypes.string,
    color: PropTypes.string,
    centerColor: PropTypes.string,
    onLongPress: PropTypes.func,
    iconSize: PropTypes.number,
    toast: PropTypes.bool,
    invertPress: PropTypes.bool,
}

Badge.defaultProps = {
    color: "red",
    centerColor: "white",
    iconSize: 30,
    toast: false,
    invertPress: false,
}

const localStyles = StyleSheet.create({
    main: {
        height: 30,
        width: 30,
        borderRadius: 15,
        alignItems: "center",
        justifyContent: "center",
        overflow: "hidden",
    },
    dot: {
        height: 10,
        width: 10,
        borderRadius: 5,
    },
    shadow: {
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "rgba(0, 0, 0, 0.75)"
    },
    container: {
        marginRight: 10,
    }
});
