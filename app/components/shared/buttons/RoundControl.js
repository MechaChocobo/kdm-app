'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    TouchableOpacity,
}  from 'react-native';
import RoundedIcon from '../rounded-icon';

export default class RoundControl extends Component {
    render() {
        return (
            <TouchableOpacity onPress={this.onPress} style={this.props.style}>
                <RoundedIcon
                    icon={this.props.icon}
                    size={this.props.size}
                    gradient={this.props.disabled ? false : this.props.gradient}
                    color={this.props.disabled ? "gray" : this.props.color}
                    iconColor={this.props.iconColor}
                    iconSize={this.props.iconSize}
                />
            </TouchableOpacity>
        );
    }

    onPress = () => {
        if(!this.props.disabled) {
            if(this.props.id !== undefined) {
                this.props.onPress(this.props.id);
            } else {
                this.props.onPress();
            }
        } else {
            if(this.props.onDisabledPress) {
                this.props.onDisabledPress(this.props.id);
            }
        }
    }
}

RoundControl.propTypes = {
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.string.isRequired,
    style: PropTypes.any,
    gradient: PropTypes.bool,
    disabled: PropTypes.bool,
    size: PropTypes.number,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    color: PropTypes.string,
    iconColor: PropTypes.string,
    onDisabledPress: PropTypes.func,
}

RoundControl.defaultProps = {
    style: {},
    gradient: true,
    disabled: false,
    size: 20,
}
