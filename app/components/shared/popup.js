'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from './button';

import {
    View,
    Modal,
    ScrollView,
    StyleSheet,
    Platform,
    SafeAreaView,
    Keyboard,
} from 'react-native';

import styles from '../../style/styles';
import Text from './app-text';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import GView from './GView';
import Hr from './Hr';

export default class Popup extends Component {
    state = {
        closeDisabled: false,
    }

    componentDidMount() {
        this.keyboardDidShowListener = Keyboard.addListener(
            'keyboardDidShow',
            this.onKeyboardShow,
        );
        this.keyboardDidHideListener = Keyboard.addListener(
            'keyboardDidHide',
            this.onKeyboardHide,
        );
    }

    componentWillUnmount() {
        this.keyboardDidHideListener.remove();
        this.keyboardDidShowListener.remove();
    }

    onKeyboardShow = () => {
        this.setState({closeDisabled: true});
    }

    onKeyboardHide = () => {
        this.setState({closeDisabled: false});
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.props.visible}
                        onRequestClose={this.props.onRequestClose ? this.props.onRequestClose : this.props.onDismissed}
                    >
                        <SafeAreaView style={styles.container}>
                            <GView style={localStyles.container} colorType="background">
                                {this.props.hideTitle !== true && this.props.title &&
                                    <View>
                                        <Text style={styles.pageHeader}>{this.props.title}</Text>
                                        <Hr />
                                    </View>
                                }
                                {!this.props.title && <View style={{height: 20}}></View>}
                                {this.props.noScrollView !== true && <ScrollView>
                                    {this.props.children}
                                </ScrollView>}
                                {this.props.noScrollView === true && <View style={styles.container}>
                                    {this.props.children}
                                </View>}
                                {this.props.hideCloseButton !== true &&
                                    <View style={{alignItems: "flex-end", bottom: "2%",}}>
                                        <View style={{height: 10}}/>
                                        <Hr />
                                        <View style={localStyles.button}>
                                            <Button
                                                title={this.props.closeText ? this.props.closeText : t("Close")}
                                                color={colors.HIGHLIGHT}
                                                onPress={this.props.onDismissed}
                                                shape="rounded"
                                                skeleton={true}
                                                disabled={this.state.closeDisabled}
                                            />
                                        </View>
                                    </View>
                                }
                            </GView>
                        </SafeAreaView>
                    </Modal>
                }
            </ColorContext.Consumer>
        );
    }
}

Popup.propTypes = {
    children: PropTypes.node.isRequired,
    hideCloseButton: PropTypes.bool,
    hideTitle: PropTypes.bool,
    closeText: PropTypes.string,
    title: PropTypes.string,
    visible: PropTypes.bool.isRequired,
    onDismissed: PropTypes.func.isRequired,
    noScrollView: PropTypes.bool,
}

let margin = 0;
if(Platform.OS === "ios") {
    margin = 20;
    if(parseInt(Platform.Version, 10) >= 11) {
        margin = 0;
    }
}

const localStyles = StyleSheet.create({
    button: {
        borderRadius: 10,
        marginTop: 5,
        marginBottom: 5,
        width: "70%",
        alignSelf: "center",
        overflow: "hidden",
    },
    container: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 11,
        marginTop: margin
    }
});
