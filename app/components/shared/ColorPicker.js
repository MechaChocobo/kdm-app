'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { fromHsv, TriangleColorPicker, toHsv } from 'react-native-color-picker';
import Toast from 'react-native-simple-toast';

import {
    View,
    StyleSheet,
    TouchableOpacity,
    Clipboard,
} from 'react-native';

import styles from '../../style/styles';
import AppText from './app-text';
import AppTextInput from './AppTextInput';
import { t } from '../../helpers/intl';
import Popup from './popup';
import LightModal from './LightModal';
import Button from './button';
import RoundedIcon from './rounded-icon';
import ColorContext from '../../context/ColorContext';
import RoundControl from './buttons/RoundControl';

export default class ColorPicker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPopup: false,
            tempColor: this.props.color,
            showTip: false,
            textColor: this.formatTextColor(this.props.color),
            tempTextColor: null,
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={styles.container}>
                        <View style={[styles.rowCentered, styles.spaceBetween, localStyles.textWrapper]}>
                            <View style={styles.rowCentered}>
                                <AppText style={[styles.fontSize20, localStyles.titlePadding]}>{t(this.props.title)}</AppText>
                                {this.props.description.length > 0 &&
                                    <RoundedIcon icon="help" color={colors.HIGHLIGHT} onPress={this.showTip} />
                                }
                            </View>
                            <TouchableOpacity onPress={this.showPopup}>
                                <View style={[localStyles.colorBox, {backgroundColor: this.props.color}]} />
                            </TouchableOpacity>
                        </View>
                        {this.state.showPopup && <Popup
                            visible={this.state.showPopup}
                            title={t("Select Color")}
                            onDismissed={this.hidePopup}
                        >
                            <TriangleColorPicker
                                style={localStyles.colorPicker}
                                onColorChange={this.colorChanged}
                                oldColor={this.props.color}
                                color={toHsv(this.state.tempColor)}
                            />
                            <View style={[styles.rowCentered, styles.centered, {marginTop: 20, marginBottom: 10}]}>
                                <AppText style={styles.fontSize20}># </AppText>
                                <AppTextInput
                                    value={this.state.tempTextColor !== null ? this.state.tempTextColor : this.state.textColor}
                                    onChangeText={this.textChange}
                                    style={{width: 100}}
                                    onBlur={this.setTextColor}
                                />
                                <RoundControl
                                    onPress={this.copyColor}
                                    icon="content-copy"
                                    style={{marginLeft: 20}}
                                    color={colors.HIGHLIGHT}
                                />
                            </View>
                            <Button title={t("Choose Color")} onPress={this.selectColor} shape={"rounded"} style={localStyles.buttonMargin}/>
                        </Popup>}
                        {this.state.showTip && <LightModal onDismissed={this.hideTip} visible={this.state.showTip}>
                            <AppText style={styles.itemText}>{this.props.description}</AppText>
                        </LightModal>}
                    </View>
                }
            </ColorContext.Consumer>
        );
    }

    copyColor = () => {
        Clipboard.setString(this.state.textColor);
        Toast.show(t("Color copied to clipboard"));
    }

    showTip = () => {
        this.setState({showTip: true});
    }

    hideTip = () => {
        this.setState({showTip: false});
    }

    colorChanged = (color) => {
        this.setColor(fromHsv(color));
    }

    setColor = (color) => {
        this.setState({tempColor: color, textColor: this.formatTextColor(color)});
    }

    textChange = (val) => {
        if(val.length > 6 || val.match(/[g-zG-Z]/g)) {
            return;
        }

        this.setState({tempTextColor: val});
    }

    setTextColor = () => {
        if(!this.state.tempTextColor || !this.state.tempTextColor.match(/^[0-9A-F]{6}$/i)) {
            this.setState({tempTextColor: null});
            return;
        }

        this.setColor('#' + this.state.tempTextColor);
        this.setState({tempTextColor: null, textColor: this.state.tempTextColor});
    }

    selectColor = () => {
        this.props.onColorChange(this.props.colorId, this.state.tempColor);
        this.setState({showPopup: false});
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    formatTextColor = (color) => {
        if(color.startsWith('#')) {
            return color.slice(1);
        }

        return color;
    }
}

ColorPicker.propTypes = {
    colorId: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    onColorChange: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string,
}

ColorPicker.defaultProps = {
    description: '',
}

const localStyles = StyleSheet.create({
    colorBox: {
        height: 40,
        width: 40,
        borderWidth: 2,
        borderColor: "black",
        borderRadius: 5,
    },
    colorPicker: {
        height: 300,
        width: 300,
        alignSelf: "center",
    },
    textWrapper: {
        marginTop: 5,
    },
    titlePadding: {
        marginRight: 4,
    },
    buttonMargin: {
        marginTop: 15
    }
});
