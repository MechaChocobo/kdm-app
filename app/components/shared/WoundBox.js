'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    TouchableOpacity,
}  from 'react-native';

import ColorContext from '../../context/ColorContext';
import Icon from './Icon';

export default class WoundBox extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <TouchableOpacity onPress={this.props.onPress}>
                        <Icon color={this.getColor(colors)} name={this.getName()} size={30}/>
                    </TouchableOpacity>
                }
            </ColorContext.Consumer>
        );
    }

    getColor = (colors) => {
        let color = "rgba(0, 0, 0";
        let weight = ".3";

        if(this.props.heavy) {
            weight = ".8";
        }

        if(colors.isDark === true) {
            color = "rgba(256, 256, 256";
        }

        return color + ', ' + weight + ')';
    }

    getName = () => {
        if(this.props.filled === true) {
            return "checkbox-blank";
        }

        return "checkbox-blank-outline";
    }
}

WoundBox.propTypes = {
    filled: PropTypes.bool,
    heavy: PropTypes.bool,
    onPress: PropTypes.func.isRequired,
}

WoundBox.defaultProps = {
    filled: false,
    heavy: false,
}
