'use strict';

import React, { Component } from 'react';
import {View, Image} from 'react-native';

import PropTypes from 'prop-types';
import { getHeightFromPercentage, getImageWidthFromHeightPercentage } from '../../helpers/helpers';
import GView from './GView';
import BackMenu from './BackMenu';

export default class BGBanner extends Component {
    imageStyle = null;

    constructor(props) {
        super(props);

        this.imageStyle = {
            height: getHeightFromPercentage(.13),
            width: getImageWidthFromHeightPercentage(.13),
            position: "absolute",
            top: getHeightFromPercentage(.08),
            opacity: .5
        };
    }

    render() {
        let image = <Image
            source={require('../../images/icon-book.png')}
            style={this.imageStyle}
        />

        if(this.props.color) {
            return (
                <View
                    style={{
                        alignItems: "center",
                        height: this.props.height,
                        backgroundColor: this.props.color,
                        overflow: "hidden",
                        zIndex: 1,
                    }}
                >
                    {this.props.showButtons && <BackMenu />}
                    {image}
                    {this.props.children}
                </View>
            )
        } else {
            return(
                <GView
                    colorType="banner"
                    style={{
                        alignItems: "center",
                        height: this.props.height,
                        overflow: "hidden",
                        zIndex: 1,
                    }}
                >
                    {this.props.showButtons && <BackMenu color="white"/>}
                    {image}
                    {this.props.children}
                </GView>
            )
        }
    }
}

BGBanner.propTypes = {
    height: PropTypes.number,
    color: PropTypes.string,
    showButtons: PropTypes.bool,
}

BGBanner.defaultProps = {
    height: getHeightFromPercentage(.16),
    showButtons: true,
}
