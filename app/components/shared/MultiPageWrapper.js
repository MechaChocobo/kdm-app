'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
} from 'react-native';

import styles from '../../style/styles';
import Button from './button';

export default class MultiPageWrapper extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Button title="back" onPress={this.props.onBack}/>
                <View style={{width: "90%", alignSelf: "center"}}>
                    {this.props.children}
                </View>
            </View>
        );
    }

    null = () => {}
}

MultiPageWrapper.propTypes = {
}

MultiPageWrapper.defaultProps = {
}
