'use strict';

import React, {Component} from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import SubHeader from './sub-header';

import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import AppTextInput from './AppTextInput';

export default class TitleAndDescription extends Component {
    titleInput;
    descriptionInput;

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View>
                        <SubHeader
                            title={t("Title")}
                            onPress={this.titleInputFocus}
                        />
                        <AppTextInput
                            textRef={(ref) => this.titleInput = ref}
                            inputStyle={localStyles.input}
                            placeholder={t("Enter Title")}
                            value={this.props.title}
                            onChangeText={this.onTitleChange}
                        />

                        <SubHeader
                            title={t("Description")}
                            onPress={this.descriptionInputFocus}
                        />
                        <AppTextInput
                            textRef={(ref) => this.descriptionInput = ref}
                            inputStyle={localStyles.input}
                            placeholder={t("Enter Description")}
                            value={this.props.description}
                            onChangeText={this.onDescriptionChange}
                        />
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    descriptionInputFocus = () => {
        this.descriptionInput.focus();
    }

    onTitleChange = (text) => {
        this.props.onTitleChange(text);
    }

    onDescriptionChange = (text) => {
        this.props.onDescriptionChange(text);
    }
}

TitleAndDescription.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    onTitleChange: PropTypes.func.isRequired,
    onDescriptionChange: PropTypes.func.isRequired,
}

var localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    }
});
