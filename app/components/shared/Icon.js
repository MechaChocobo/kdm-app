'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';

import IcomoonIcon from './IcomoonIcon';

export default class Icon extends Component {
    render() {
        return (
            this.getIcon()
        );
    }

    getIcon = () => {
        if(this.props.name.match(/^sk-.*/)) {
            return <IcomoonIcon {...this.props}/>
        } else {
            return <MaterialIcon {...this.props}/>
        }
    }
}

Icon.propTypes = {
    size: PropTypes.number,
    name: PropTypes.string.isRequired,
    color: PropTypes.string,
}

Icon.defaultProps = {
    size: 15,
    color: "white",
}
