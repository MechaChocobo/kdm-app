'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import ColorContext from '../../context/ColorContext';
import GView from './GView';
import Icon from './Icon';

export default class GradientIcon extends Component {
    render() {
        return (
            <ColorContext.Consumer>
            { colors => {
                return <GView
                    direction="diagonal"
                    style={this.props.style}>
                        <Icon name={this.props.icon} size={this.props.size} color={this.props.iconColor ? this.props.iconColor : colors.WHITE}/>
                    </GView>
            }}
            </ColorContext.Consumer>
        );
    }

    devNull = () => {}
}

GradientIcon.propTypes = {
    size: PropTypes.number,
    icon: PropTypes.string.isRequired,
    iconColor: PropTypes.string,
    gradient: PropTypes.object,
}

GradientIcon.defaultProps = {
    disabled: false,
    size: 15,
}
