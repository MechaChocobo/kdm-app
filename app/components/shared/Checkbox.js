'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from './Icon';
import { CHECKBOX_SIZE } from '../../helpers/helpers';
import ColorContext from '../../context/ColorContext';

export default class Checkbox extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <Icon
                        name={this.props.checked === true ? "checkbox-blank" : "checkbox-blank-outline"}
                        color={this.props.color ? this.props.color : colors.TEXT}
                        size={this.props.size}
                    />
            }
            </ColorContext.Consumer>
        );
    }

}

Checkbox.propTypes = {
    size: PropTypes.number,
    color: PropTypes.string,
    checked: PropTypes.bool,
}

Checkbox.defaultProps = {
    size: CHECKBOX_SIZE,
    checked: false,
}
