'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';
import Icon from './Icon';
import { t } from '../../helpers/intl';
import Popup from './popup';
import ListItem from './ListItem';
import AppText from './app-text';
import { getColorCode } from '../../helpers/helpers';
import RoundControl from './buttons/RoundControl';
import Hr from './Hr';

export default class AffinitySelector extends PureComponent {
    state = {
        showPopup: false,
        showCompletePopup: false,
    }

    colors = {
        "blue": t("Blue"),
        "green" : t("Green"),
        "red" : t("Red"),
        "none": t("None"),
    };

    render() {
        const itemSize = 40;
        const itemPadding = (this.props.size - itemSize) / 2;

        return (
            <ColorContext.Consumer>
            { colors => {
                return <View style={[styles.container, styles.centered, styles.centeredItems, styles.justifyCentered, {height: this.props.size, width: this.props.size, marginTop: 10}]}>
                    <TouchableOpacity onPress={this.showTopFilter} style={[localStyles.filterBox, {height: itemSize, width: itemSize, top: 0, left: itemPadding, borderColor: colors.HIGHLIGHT, backgroundColor: (this.props.top ? this.props.top : "transparent"), borderStyle: (this.props.top ? "solid" : "dashed")}]}>
                        <Icon name="plus-circle-outline" size={25} color={this.props.top ? colors.WHITE : colors.HIGHLIGHT}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showBottomFilter} style={[localStyles.filterBox, {height: itemSize, width: itemSize, bottom: 0, left: itemPadding, borderColor: colors.HIGHLIGHT, backgroundColor: (this.props.bottom ? this.props.bottom : "transparent"), borderStyle: (this.props.bottom ? "solid" : "dashed")}]}>
                        <Icon name="plus-circle-outline" size={25} color={this.props.bottom ? colors.WHITE : colors.HIGHLIGHT}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showLeftFilter} style={[localStyles.filterBox, {height: itemSize, width: itemSize, left: 0, top: itemPadding, borderColor: colors.HIGHLIGHT, backgroundColor: (this.props.left ? this.props.left : "transparent"), borderStyle: (this.props.left ? "solid" : "dashed")}]}>
                        <Icon name="plus-circle-outline" size={25} color={this.props.left ? colors.WHITE : colors.HIGHLIGHT}/>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.showRightFilter} style={[localStyles.filterBox, {height: itemSize, width: itemSize, right: 0, top: itemPadding, borderColor: colors.HIGHLIGHT, backgroundColor: (this.props.right ? this.props.right : "transparent"), borderStyle: (this.props.right ? "solid" : "dashed")}]}>
                        <Icon name="plus-circle-outline" size={25} color={this.props.right ? colors.WHITE : colors.HIGHLIGHT}/>
                    </TouchableOpacity>
                    {this.renderCompleteAffinities(colors)}
                    {this.state.showCompletePopup !== false && <Popup
                        visible={this.state.showCompletePopup !== false}
                        title={t("Set Complete Affinities")}
                        onDismissed={this.hideCompletePopup}
                    >
                        {this.renderCompletePopup(colors)}
                    </Popup>}
                    {this.state.showPopup !== false && <Popup
                        visible={this.state.showPopup !== false}
                        title={t("Select Color")}
                        onDismissed={this.hideAffinityFilter}
                    >
                        {this.renderAffinityPopup(colors)}
                    </Popup>}
                </View>
            }}
            </ColorContext.Consumer>
        );
    }

    renderCompleteAffinities = (colors) => {
        const squareSize = 40;
        let boxes = [];

        if(this.props.complete) {
            const size = Math.floor(squareSize / 2) - 2;
            for(let color in this.props.complete) {
                for(let i = 0; i < this.props.complete[color]; i++) {
                    boxes.push(<View
                        key={color + i}
                        style={{backgroundColor: getColorCode(color, colors), width: size, height: size, borderWidth: 1}}
                    />);
                }
            }
        }

        return <TouchableOpacity onPress={this.showCompleteFilter} style={[styles.centeredItems, styles.justifyCentered, {overflow: "hidden", borderRadius: 1, borderWidth: 1,height: squareSize, width: squareSize, borderColor: colors.HIGHLIGHT, backgroundColor: "transparent", borderStyle: (this.props.complete ? "solid" : "dashed")}]}>
            <View style={[styles.row, {flexWrap: "wrap", position: "absolute", top: 1, left: 1}]}>
                {boxes}
            </View>
            <Icon style={{position: "absolute"}} name={this.props.complete ? "pencil" : "plus-circle-outline"} size={25} color={this.props.complete ? colors.WHITE : colors.HIGHLIGHT}/>
        </TouchableOpacity>
    }

    showCompleteFilter = () => {
        this.setState({showCompletePopup: true});
    }

    hideCompletePopup = () => {
        this.setState({showCompletePopup: false});
    }

    showTopFilter = () => {
        this.setState({showPopup: "top"});
    }

    showLeftFilter = () => {
        this.setState({showPopup: "left"});
    }

    showRightFilter = () => {
        this.setState({showPopup: "right"});
    }

    showBottomFilter = () => {
        this.setState({showPopup: "bottom"});
    }

    hideAffinityFilter = () => {
        this.setState({showPopup: false});
    }

    renderAffinityPopup = (colors) => {
        let colorOptions = ["none", "blue", "green", "red"];

        if(this.state.showPopup === "top") {
            if(this.props.top) {
                colorOptions.splice(colorOptions.indexOf(this.props.top), 1);
            }
        } else if(this.state.showPopup === "bottom") {
            if(this.props.bottom) {
                colorOptions.splice(colorOptions.indexOf(this.props.bottom), 1);
            }
        } else if(this.state.showPopup === "left") {
            if(this.props.left) {
                colorOptions.splice(colorOptions.indexOf(this.props.left), 1);
            }
        } else if(this.state.showPopup === "right") {
            if(this.props.right) {
                colorOptions.splice(colorOptions.indexOf(this.props.right), 1);
            }
        }

        return colorOptions.map((color) => {
            return <ListItem
                key={color}
                title=""
                id={color}
                onPress={this.setColorSelection}
                buttonText={t("Choose")}
            >
                <View style={styles.width50}>
                    <AppText style={styles.title} color={colors.WHITE}>{this.colors[color]}</AppText>
                </View>
                {this.getColorCircle(color, colors)}
            </ListItem>
        });
    }

    renderCompletePopup = (colors) => {
        let red = 0;
        let blue = 0;
        let green = 0;

        if(this.props.complete) {
            if(this.props.complete.red) {
                red = this.props.complete.red;
            }

            if(this.props.complete.green) {
                green = this.props.complete.green;
            }

            if(this.props.complete.blue) {
                blue = this.props.complete.blue;
            }
        }

        return <View>
            <View style={[styles.rowCentered, styles.spaceBetween]}>
                <AppText style={styles.title}>{t("Blue")}</AppText>
                <View style={styles.rowCentered}>
                    <RoundControl icon="minus" id="blue" onPress={this.subtractComplete} disabled={blue <= 0} />
                    <AppText style={[styles.largeValue, {marginLeft: 10, marginRight: 10}]}>{blue}</AppText>
                    <RoundControl icon="plus" id="blue" onPress={this.addComplete} />
                </View>
            </View>
            <Hr />
            <View style={[styles.rowCentered, styles.spaceBetween]}>
                <AppText style={styles.title}>{t("Green")}</AppText>
                <View style={styles.rowCentered}>
                    <RoundControl icon="minus" id="green" onPress={this.subtractComplete} disabled={green <= 0} />
                    <AppText style={[styles.largeValue, {marginLeft: 10, marginRight: 10}]}>{green}</AppText>
                    <RoundControl icon="plus" id="green" onPress={this.addComplete} />
                </View>
            </View>
            <Hr />
            <View style={[styles.rowCentered, styles.spaceBetween]}>
                <AppText style={styles.title}>{t("Red")}</AppText>
                <View style={styles.rowCentered}>
                    <RoundControl icon="minus" id="red" onPress={this.subtractComplete} disabled={red <= 0} />
                    <AppText style={[styles.largeValue, {marginLeft: 10, marginRight: 10}]}>{red}</AppText>
                    <RoundControl icon="plus" id="red" onPress={this.addComplete} />
                </View>
            </View>
        </View>
    }

    subtractComplete = (color) => {
        let complete = this.props.complete ? {...this.props.complete} : {};
        complete[color] = complete[color] ? complete[color] - 1 : 0;

        for(let key in complete) {
            if(complete[key] === 0) {
                delete complete[key];
            }
        }

        if(Object.keys(complete).length === 0) {
            complete = null;
        }

        this.props.onChange("complete", complete);
    }

    addComplete = (color) => {
        let complete = this.props.complete ? {...this.props.complete} : {};
        complete[color] = complete[color] ? complete[color] + 1 : 1;
        this.props.onChange("complete", complete);
    }

    getColorCircle = (color = "", colors) => {
        if(!color || color === "none") {
            return null;
        }

        return <View style={[localStyles.circle, {backgroundColor: getColorCode(color, colors)}]}/>
    }

    setColorSelection = (color) => {
        if(color === "none") {
            color = null;
        }

        this.props.onChange(this.state.showPopup, color);
        this.hideAffinityFilter();
    }
}

AffinitySelector.propTypes = {
    size: PropTypes.number,
    top: PropTypes.string,
    bottom: PropTypes.string,
    left: PropTypes.string,
    right: PropTypes.string,
    onChange: PropTypes.func.isRequired,
}

AffinitySelector.defaultProps = {
    size: 150,
}

const localStyles = StyleSheet.create({
    title: {
        fontSize: 24,
        fontWeight: "500"
    },
    filterBox: {
        position: "absolute",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 1,
        borderWidth: 1,
    },
    circle: {
        width: 25,
        height: 25,
        borderRadius: 15,
        borderWidth: 1,
        borderColor: "black",
        marginLeft: 10,
    }
});
