'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    ScrollView,
} from 'react-native';

import styles from '../../style/styles';
import AppText from './app-text';
import { messageMap } from '../../helpers/LogMessages';
import { t } from '../../helpers/intl';
import { padNumToDigits } from '../../helpers/helpers';

export default class ActionLog extends PureComponent {
    render() {
        return (
            <ScrollView style={styles.container}>
                {this.renderLog()}
            </ScrollView>
        );
    }

    renderLog = () => {
        let logs = [];
        for(let i = this.props.actions.length - 1; i >= 0; i--) {
            logs.push(
                <AppText key={i} style={styles.itemText}>{this.getLogText(this.props.actions[i])}</AppText>
            );
        }

        return logs;
    }

    getLogText = (log) => {
        let timeString = this.getFormattedTime(log.t);
        return timeString + ' - ' + t(messageMap[log.m], log.a);
    }

    getFormattedTime = (timestamp) => {
        const time = new Date(timestamp);
        if(this.props.dateFormat === 'dmy') {
            return time.getDate() + '/' + (time.getMonth() + 1) + '/' + time.getFullYear() + ' ' + padNumToDigits(time.getHours()) + ':' + padNumToDigits(time.getMinutes());
        } else {
            return (time.getMonth() + 1) + '/' + time.getDate() + '/' + time.getFullYear() + ' ' + padNumToDigits(time.getHours()) + ':' + padNumToDigits(time.getMinutes());
        }
    }
}

ActionLog.propTypes = {
    actions: PropTypes.arrayOf(PropTypes.object),
    dateFormat: PropTypes.string,
}

ActionLog.defaultProps = {
    actions: [],
    dateFormat: '',
}
