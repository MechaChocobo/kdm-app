'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import MenuButton from './MenuButton';
import BackButton from './BackButton';

export default class BackMenu extends Component {
    render() {
        return (
            <React.Fragment>
                <MenuButton color={this.props.color}/>
                <BackButton color={this.props.color}/>
            </React.Fragment>
        );
    }
}

BackMenu.propTypes = {
    color: PropTypes.string,
}

BackMenu.defaultProps = {
}

