'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    TouchableOpacity,
    StyleSheet,
}  from 'react-native';

import Icon from './Icon';
import {getColors} from '../../selectors/general';
import { setMenuDrawerOpen } from '../../actions/router';
import { isDisconnectedClient } from '../../selectors/network';

class MenuButton extends Component {
    render() {
        let networkIndicator = null;
        if(this.props.isHost || this.props.isClient) {
            networkIndicator = <Icon style={localStyles.networkIconStyle} name="check" size={20} color={this.props.colors.GREEN}/>;
        }

        if(this.props.networkLoading > 0) {
            networkIndicator = <Icon style={localStyles.networkIconStyle} name="autorenew" size={20} color={this.props.colors.ORANGE}/>
        }

        if(this.props.clientDisconnected) {
            networkIndicator = <Icon style={localStyles.networkIconStyle} name="close" size={20} color={this.props.colors.RED} />
        }

        return (
            <TouchableOpacity onPress={this.onPress} style={localStyles.menuIconStyle}>
                <Icon name="menu" size={40} color={this.props.color ? this.props.color : this.props.colors.TEXT} style={localStyles.transparent}/>
                {networkIndicator}
            </TouchableOpacity>
        );
    }

    onPress = () => {
        this.props.setMenuDrawerOpen(true);
    }
}

MenuButton.propTypes = {
}

MenuButton.defaultProps = {
}

function mapStateToProps(state, props) {
    return {
        isHost: state.networkReducer.server !== null,
        isClient: state.networkReducer.client !== null && state.networkReducer.clientError === "",
        clientError: state.networkReducer.clientError,
        networkLoading: state.networkReducer.loading,
        clientDisconnected: isDisconnectedClient(state),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setMenuDrawerOpen,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuButton);

const localStyles = StyleSheet.create({
    networkIconStyle: {
        position: "absolute",
        right: 0,
        top: 20,
        zIndex: 11,
        backgroundColor:'transparent',
    },
    menuIconStyle: {
        position: "absolute",
        right: 5,
        top: 0,
        zIndex:10,
    },
    transparent: {
        backgroundColor:'transparent',
    }
});
