'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    Modal,
    TouchableWithoutFeedback,
    StyleSheet,
} from 'react-native';

import styles from '../../style/styles';
import ColorContext from '../../context/ColorContext';

export default class LightModal extends Component {
    render() {
        let alignment = {};

        if(this.props.centered) {
            alignment = styles.centeredItems;
        }

        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <Modal
                        transparent={true}
                        visible={this.props.visible}
                        onRequestClose={this.props.onRequestClose ? this.props.onRequestClose : this.props.onDismissed}
                    >
                        <TouchableWithoutFeedback onPress={this.props.onDismissed} style={this.props.style}>
                            <View style={[styles.darkBlackOpaqueBackground, styles.container, localStyles.centered]}>
                                <TouchableWithoutFeedback onPress={this.devNull}>
                                    <View style={[styles.width90, styles.centered, alignment, localStyles.wrapperPadding, {backgroundColor: colors.GRADIENT_START_BACKGROUND}]}>
                                        {this.props.children}
                                    </View>
                                </TouchableWithoutFeedback>
                            </View>
                        </TouchableWithoutFeedback>
                    </Modal>
                }
            </ColorContext.Consumer>
        );
    }

    devNull = () => {}
}

LightModal.propTypes = {
    children: PropTypes.node.isRequired,
    visible: PropTypes.bool.isRequired,
    onDismissed: PropTypes.func.isRequired,
    centered: PropTypes.bool,
}

LightModal.defaultProps = {
    centered: true
}

const localStyles = StyleSheet.create({
    wrapperPadding: {
        padding: 15,
        paddingTop: 30,
        paddingBottom: 30,
    },
    centered: {
        justifyContent: "center"
    }
});
