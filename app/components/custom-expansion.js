'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    ScrollView,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ScrollableTabView from 'react-native-scrollable-tab-view-universal';

import sharedStyles from '../style/styles';
import Overview from './custom-expansions/overview';
import Settlement from './custom-expansions/settlement';
import Survivor from './custom-expansions/survivor';
import Timeline from './custom-expansions/Timeline';
import { t } from '../helpers/intl';
import { setName } from '../actions/custom-expansions';
import { getColors } from '../selectors/general';
import BGBanner from './shared/BGBanner';
import styles from '../style/styles';
import Milestones from './custom-expansions/Milestones';
import AppText from './shared/app-text';

class CustomExpansion extends Component {
    state = {
        scrollRef: null
    }

    render() {
        return (
            <ScrollView style={sharedStyles.container} ref={this.setScrollRef}>
                <BGBanner>
                    <AppText style={sharedStyles.pageHeader} color={this.props.colors.WHITE}>{this.props.expansion.name}</AppText>
                </BGBanner>
                <ScrollableTabView
                    locked={true}
                    tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                    tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                    tabBarInactiveTextColor={this.props.colors.TEXT}
                    tabBarTextStyle={styles.tabText}
                >
                    <Overview tabLabel={t("Overview")} expansion={this.props.expansion}/>
                    <Settlement tabLabel={t("Settlement")} expansion={this.props.expansion} scrollRef={this.state.scrollRef}/>
                    <Survivor tabLabel={t("Survivor")} expansion={this.props.expansion} scrollRef={this.state.scrollRef}/>
                    <Timeline tabLabel={t("Timeline")} expansion={this.props.expansion}/>
                    <Milestones tabLabel={t("Milestones")} expansion={this.props.expansion}/>
                </ScrollableTabView>
            </ScrollView>
        );
    }

    setScrollRef = (ref) => {
        this.setState({scrollRef: ref});
    }

    onChangeText = (text) => {
        this.setState({name: text});
    }

    setName = () => {
        this.props.setName(this.props.expansion.id, this.state.name);
    }
};

function mapStateToProps(state, props) {

    return {
        expansion: state.customExpansions[state.pageReducer.customExpansionId],
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setName}, dispatch);
}

var localStyles = StyleSheet.create({
    nameInput: {
        marginLeft: 45,
        marginRight: 45,
        textAlign: "center",
        color: "white",
        fontSize: 30,
        fontWeight: "600",
        padding: 0
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomExpansion);
