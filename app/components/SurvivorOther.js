'use strict';

import React, { Component } from 'react';

import {
    View,
} from 'react-native';

import Affinities from './survivor/affinities';
import CursedGear from './survivor/cursed-gear';
import Courage from './survivor/courage';
import Understanding from './survivor/understanding';
import WeaponProf from './survivor/weapon-prof';
import XP from './survivor/xp';

import styles from '../style/styles';
import Hr from './shared/Hr';

export default class SurvivorOther extends Component {
    render() {
        return (
            <View>
                <View style={[styles.centered, styles.width90]}>
                    <XP id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <Understanding id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <Courage id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <WeaponProf id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <Affinities id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <CursedGear id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
            </View>
        );
    }
}

SurvivorOther.propTypes = {
}

SurvivorOther.defaultProps = {
}
