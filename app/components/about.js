'use strict';

import React, { Component } from 'react';

import {
    View,
    ScrollView
} from 'react-native';

import Text from './shared/app-text';
import Hr from './shared/Hr';
import sharedStyles from '../style/styles';
import {t} from '../helpers/intl';
import ColorContext from '../context/ColorContext';
import BackMenu from './shared/BackMenu';
const packageInfo = require('../../package.json');

export default class About extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={sharedStyles.container}>
                        <BackMenu />
                        <Text style={sharedStyles.pageHeader}>{t("About Scribe")}</Text>
                        <ScrollView>
                            <View style={sharedStyles.wrapper}>
                                <Text style={sharedStyles.itemText}>{t("%b%Scribe%b% is not developed, maintained, authorized or in any other way supported by or affiliated with Kingdom Death or Adam Poots Games.")}</Text>
                                <Hr />
                                <Text style={sharedStyles.itemText}>{t("App version %version%", {version: packageInfo.version})}</Text>
                                <Hr />
                                <Text style={sharedStyles.itemText}>{t("App icon and splash screen crest by %b%Tommy Rayburn%b%")}</Text>
                                <Hr />
                                <Text style={sharedStyles.value}>{t("Reporting Bugs")}</Text>
                                <Text style={sharedStyles.itemText}>{t("%b%Scribe%b% is under active development, and as such you may encounter bugs or strange behavior. If you do experience a bug, please report it to the project's %a%issue board%a%.", {linkUrl: "https://gitlab.com/taboobat/kdm-app/issues", linkColor: colors.BLUE})}</Text>
                                <Text style={sharedStyles.itemText}>{t("When submitting an issue, please use the issue templates to give the issue the best chance of being fixed!")}</Text>
                                <Text style={sharedStyles.itemText}>{t("Feel free to join the %a%Scribe Discord server%a% as well!", {linkUrl: "https://discordapp.com/invite/gA7sNtZ", linkColor: colors.BLUE})}  {t("It's the quickest way to get in touch with me if you have an urgent issue.")}</Text>
                                <Hr />
                                <Text style={sharedStyles.value}>{t("Release Notes")}</Text>
                                <Text style={sharedStyles.itemText}>{t("The app store blurbs don't always have enough space to detail all the changes in each release, you can keep up to date by checking the %a%Release History%a%.", {linkUrl: "https://gitlab.com/taboobat/kdm-app/wikis/Release-History", linkColor: colors.BLUE})}</Text>
                                <Hr />
                                <Text style={sharedStyles.value}>{t("Documentation")}</Text>
                                <Text style={sharedStyles.itemText}>{t("For help using %b%Scribe%b% check out the %a%official wiki%a%.", {linkUrl: "https://gitlab.com/taboobat/kdm-app/wikis/Getting-Started", linkColor: colors.BLUE})}</Text>
                            </View>
                        </ScrollView>
                    </View>
                }
            </ColorContext.Consumer>
        );
    }
}
