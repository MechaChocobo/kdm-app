'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import SettlementsActive from './settlements-active';
import SettlementsArchived from './settlements-archived';
import Text from '../components/shared/app-text';
import sharedStyles from '../style/styles';
import { t } from '../helpers/intl';
import { getArchivedSettlements } from '../selectors/settlement';
import { getColors } from '../selectors/general';
import BGBanner from './shared/BGBanner';
import { getHeightFromPercentage, setCustomExpansionConfigs } from '../helpers/helpers';
import { addCustomExpansionToSettlement } from '../actions/settlement';
import { doIntegrityCheck } from '../actions/global';
import { applyGlobalMilestones } from '../actions/custom-expansions';

class Home extends Component {
    componentDidMount() {
        setCustomExpansionConfigs(Object.values(this.props.customExpansions));
        this.props.applyGlobalMilestones();
        for(let i = 0; i < this.props.settlements.length; i++) {
            if(this.props.settlements[i].customExpansions && Object.keys(this.props.settlements[i].customExpansions).length > 0) {
                for(let id in this.props.settlements[i].customExpansions) {
                    this.props.addCustomExpansionToSettlement(this.props.settlements[i].id, id, this.props.settlements[i].customExpansions[id]);
                }
            }
        }

        if(this.props.integrityCheck) {
            this.props.doIntegrityCheck();
        }
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <BGBanner height={getHeightFromPercentage(.25)}>
                    <Text style={sharedStyles.pageHeader} color={this.props.colors.WHITE}>{t("Settlements")}</Text>
                </BGBanner>
                <ScrollableTabView
                    locked={true}
                    tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                    tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                    tabBarInactiveTextColor={this.props.colors.TEXT}
                >
                    <SettlementsActive tabLabel={t("Active")}/>
                    <SettlementsArchived tabLabel={t("Archived") + (this.props.numArchived > 0 ? " ("+this.props.numArchived+")" : "")}/>
                </ScrollableTabView>
            </View>
        );
    }
};

function mapStateToProps(state, props) {
    return {
        settlements: Object.values(state.settlementReducer.settlements),
        customExpansions: state.customExpansions,
        numArchived: getArchivedSettlements(state).length,
        integrityCheck: state.global.integrityCheck,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({addCustomExpansionToSettlement, doIntegrityCheck, applyGlobalMilestones}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
