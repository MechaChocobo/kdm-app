'use strict';

import React, { Component } from 'react';
var {
    StyleSheet,
    FlatList,
    View,
} = require('react-native');

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {changePage, goToSettlement} from '../actions/router';
import { getArchivedSettlements } from '../selectors/settlement';
import SettlementLineItem from './settlement/settlement-line-item';
import sharedStyles from '../style/styles';
import { toggleArchive, deleteSettlement } from '../actions/settlement';
import { getColors } from '../selectors/general';
import { resetListContext } from '../helpers/recycler-context';

class SettlementsArchived extends Component {
    render() {
        return (
            <View style={sharedStyles.container}>
                <FlatList
                    data={Object.values(this.props.settlements)}
                    renderItem={this.renderSettlement}
                    keyExtractor={(item, index) => index + ""}
                    />
            </View>
        );
    }

    goToSettlement = (id) => {
        resetListContext(); // resets saved scroll indexes of lists
        this.props.goToSettlement(id);
    }

    archiveSettlement = (id) => {
        this.props.toggleArchive(id);
    }

    deleteSettlement = (id) => {
        this.props.deleteSettlement(id);
    }

    renderSettlement = (item) => {
        return (
            <SettlementLineItem
                settlement={item.item}
                customExpansions={this.props.customExpansions}
                onPress={this.goToSettlement}
                onArchive={this.archiveSettlement}
                onDelete={this.deleteSettlement}
                light={item.index % 2 !== 0}
            />
        )
    }
};

function mapStateToProps(state, props) {
    return {
        settlements: getArchivedSettlements(state),
        customExpansions: state.customExpansions,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changePage,
        goToSettlement,
        toggleArchive,
        deleteSettlement,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementsArchived);
