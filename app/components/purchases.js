'use strict';

import React, { Component } from 'react';
import InAppBilling from "react-native-billing"; // billing wrapper for android
import Toast from 'react-native-simple-toast';
import iapReceiptValidator from 'iap-receipt-validator';
import { MarkdownView } from 'react-native-markdown-view';

import {
    View,
    Platform,
    NativeModules,
    ScrollView,
    Linking
} from 'react-native';

const { InAppUtils } = NativeModules; // billing wrapper for iOS

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Text from './shared/app-text';
import Button from './shared/button';
import sharedStyles from '../style/styles';
import { ToggleList } from './shared/toggle-list';
import { setShouldShowAds, setIsSubscriber, setPurchasedItems, addPurchasedItem } from '../actions/purchases';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import BackMenu from './shared/BackMenu';
import markdownStyles from '../style/markdown';

let subscriptionConfig = {};
let shortTitles = {};
let productConfig = {};
let products = [];
let subscriptions = ["basic", "advanced", "godamongmen"];
let noads = Platform.select({
    android: "noads",
    ios: "noads_ios"
});

class Purchases extends Component {
    constructor(props) {
        super(props);
        this.state = {selectedOption: "", subscriptionMessage: "", ownedItems: [], productMessage: ""}

        subscriptionConfig = {
            basic: {title: t("Basic Subscription - %b%$2/month.%b%  Every subscription is a huge help.  Thank you!")},
            advanced: {title: t("Advanced Subscription - %b%$5/month.%b%  Above and beyond.  A huge thanks for your support!")},
            godamongmen: {title: t("God Among Men Subscription - %b%$10/month.%b%  Unbelievable.  Your generosity is incredible, thank you so much!")}
        }

        shortTitles = {
            basic: t("Basic"),
            advanced: t("Advanced"),
            godamongmen: t("God Among Men"),
            lifetimesubscription: t("Lifetime Subscription")
        }

        shortTitles[noads] = t("No Ads");

        productConfig = {
            lifetimesubscription: {title: t("Lifetime Subscription - $50. A one-time purchase that gives you a lifetime subscription, including no ads and all other subscriber benefits.")}
        }

        productConfig[noads] = {title: t("No Ads - $5.  Permanently removes ads from the app.  Does not include subscriber benefits.")};
        products = [noads, "lifetimesubscription"];

        for(let key in markdownStyles) {
            markdownStyles[key].color = this.props.colors.TEXT;
        }
    }

    componentWillMount = () => {
        if(Platform.OS === "android") {
            InAppBilling.open().then(() => {
                InAppBilling.listOwnedSubscriptions().then((subscriptionResults) => {
                    return InAppBilling.listOwnedProducts().then((productResults) => {
                        this.setState({ownedItems: subscriptionResults.concat(productResults)})
                        return InAppBilling.close();
                    });
                });
            }).catch(() => {});
        } else if(Platform.OS === "ios") {
            // loads products -- should swap out hardcoded prices for what's returned here
            InAppUtils.loadProducts(subscriptions.concat(products), (error, items) => {

            });
        }
    }

    componentWillUnmount = () => {
        if(Platform.OS === "android") {
            InAppBilling.close();
        }
    }

    getOwnedTitles = () => {
        let titles = this.state.ownedItems.map((item) => {
            return shortTitles[item];
        });

        return titles.join(', ');
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <BackMenu />
                <Text style={sharedStyles.pageHeader}>{t("Purchases")}</Text>
                <ScrollView style={{paddingLeft: 7}}>
                    <Text style={sharedStyles.itemText}>{t("Purchasing a subscription is entirely optional, but all proceeds go directly into further development of %b%Scribe%b%.  Subscribing will not only give you the warm fuzzies, but it will also remove any in-app ads and unlock some advanced features.")}</Text>
                    <View style={{height: 5}}></View>
                    <Text style={sharedStyles.itemText}>{t("Subscribing currently unlocks")}:</Text>
                    <MarkdownView styles={markdownStyles}>{"* "+t("Custom sorting of the survivor list") + "\n"}
                        {"* "+t("The ability to search for gear by its affinities")}
                    </MarkdownView>
                    <View style={{paddingTop: "5%"}}>
                        <Text style={sharedStyles.itemText}><Text style={[sharedStyles.hyperlink, {color: this.props.colors.BLUE}]} onPress={() => Linking.openURL("https://gitlab.com/taboobat/kdm-app/wikis/Terms-of-Use")}>{t("View Terms of Use")}</Text></Text>
                        <View style={{height:5}}/>
                        <Text style={sharedStyles.itemText}><Text style={[sharedStyles.hyperlink, {color: this.props.colors.BLUE}]} onPress={() => Linking.openURL("https://gitlab.com/taboobat/kdm-app/wikis/Privacy-Policy")}>{t("View Privacy Policy")}</Text></Text>
                        {this.state.ownedItems.length > 0 && <Text style={sharedStyles.itemText}>{t("You have already purchased the following")}: {this.getOwnedTitles()}</Text>}
                        <Text style={{fontSize: 25, fontWeight: "400"}}>{t("One Time Purchases")}</Text>
                        <View style={{width: "90%"}}>
                            <ToggleList selected={this.state.selectedOption} items={products} onSelect={this.onSelect} onDeselect={this.onDeselect} config={productConfig}/>
                        </View>
                        <View style={{paddingLeft: "10%", paddingRight: "10%"}}>
                            <Button title={t("Purchase")} onPress={this.submitSelection} shape="rounded" disabled={this.state.selectedOption === "" || products.indexOf(this.state.selectedOption) === -1}/>
                        </View>
                        {this.state.productMessage !== "" && <Text style={sharedStyles.itemText}>{this.state.productMessage}</Text>}
                        <Text style={{fontSize: 25, fontWeight: "400"}}>{t("Subscriptions")}</Text>
                        <View style={{width: "90%"}}>
                            <ToggleList selected={this.state.selectedOption} items={subscriptions} onSelect={this.onSelect} onDeselect={this.onDeselect} config={subscriptionConfig}/>
                        </View>
                        <View style={{paddingLeft: "10%", paddingRight: "10%"}}>
                            <Button title={t("Subscribe")} onPress={this.submitSelection} shape="rounded" disabled={this.state.selectedOption === "" || subscriptions.indexOf(this.state.selectedOption) === -1}/>
                        </View>
                        {this.state.subscriptionMessage !== "" && <Text style={sharedStyles.itemText}>{this.state.subscriptionMessage}</Text>}

                        {Platform.OS === "ios" && <View>
                            <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: 20}}>
                                <Button title={t("Restore Past Purchases")} onPress={this.restorePurchases} shape="rounded" />
                            </View>
                            <Text style={sharedStyles.itemText}>{t("Please review the following information regarding the recurring subscriptions")}:</Text>
                            <Text style={sharedStyles.itemText}>- {t("Payment will be charged to your iTunes Account at confirmation of purchase")}</Text>
                            <Text style={sharedStyles.itemText}>- {t("Subscriptions automatically renew unless auto-renew is turned off at least 24-hours before the end of the current period")}</Text>
                            <Text style={sharedStyles.itemText}>- {t("Your account will be charged for renewal within 24-hours prior to the end of the current period at the same price as you've selected")}</Text>
                            <Text style={sharedStyles.itemText}>- {t("Subscriptions may be managed and auto-renewal may be turned off by going to your Account Settings after purchase")}</Text>
                        </View>}
                    </View>
                </ScrollView>
            </View>
        );
    }

    restorePurchases = () => {
        InAppUtils.restorePurchases((error, response) => {
            if(error) {
                this.setState({subscriptionMessage: t("Unable to connect to itunes store.")});
            } else {
                if (response.length === 0) {
                    Toast.show(t("No purchases found."));
                    return;
                }

                const sharedSecret = "0da4cf34be0448ff9e24be9634930920";
                const production = true;
                const validateReceipt = iapReceiptValidator(sharedSecret, production);

                let disableAds = false;
                for(let i = 0; i < response.length; i++) {
                    if(response[i].productIdentifier === "lifetimesubscription") {
                        this.props.setIsSubscriber(true);
                        break;
                    } else if(response[i].productIdentifier === "noads_ios") {
                        disableAds = true;
                    } else if(
                        response[i].productIdentifier === "basic" ||
                        response[i].productIdentifier === "advanced" ||
                        response[i].productIdentifier === "godamongmen"
                    ) {
                        this.validate(response[i].transactionReceipt, validateReceipt);
                    }
                }

                if(disableAds === true) {
                    this.props.setShouldShowAds(false);
                }

                this.props.setPurchasedItems(response);

                Toast.show(t("All purchases have been successfully restored!"));
            }
         });
    }

    async validate(receiptData, validateReceipt) {
        try {
            const validationData = await validateReceipt(receiptData);
            let valid = false;

            if(validationData.latest_receipt_info.expires_date > Date.now()) {
                valid = true;
            }

            if(valid) {
                this.props.setIsSubscriber(true);
            }
        } catch(err) {
            // console.warn(err.valid, err.error, err.message);
        }
    }

    onSelect = (value) => {
        return () => {
            this.setState({selectedOption: value});
        }
    }

    onDeselect = () => {
        this.setState({selectedOption: ""});
    }

    submitSelection = () => {
        if(this.state.selectedOption === "") {
            return;
        }

        if(Platform.OS === "android") {
            InAppBilling.open()
                .then(() => {
                    if(subscriptions.indexOf(this.state.selectedOption !== -1)) {
                        InAppBilling.subscribe(this.state.selectedOption).then(this.androidPostPurchase).catch(() => {
                            return InAppBilling.close();
                        });
                    } else {
                        InAppBilling.purchase(this.state.selectedOption).then(this.androidPostPurchase).catch(() => {
                            return InAppBilling.close();
                        });
                    }
                })

        } else if(Platform.OS === "ios") {
            InAppUtils.purchaseProduct(this.state.selectedOption, (error, response) => {
                // NOTE for v3.0: User can cancel the payment which will be available as error object here.
                if(response && response.productIdentifier) {
                    if(response.productIdentifier === noads) {
                        this.props.setShouldShowAds(false);
                    } else {
                        this.props.setIsSubscriber(true);
                    }

                    this.props.addPurchasedItem(response);

                    if(subscriptions.indexOf(this.state.selectedOption !== -1)) {
                        this.setState({subscriptionMessage: t("Successfully purchased!  Thanks so much!")});
                    } else {
                        this.setState({productMessage: t("Successfully purchased!  Thanks so much!")});
                    }
                }
            });
        }
    }

    androidPostPurchase = (details) => {
        if(subscriptions.indexOf(this.state.selectedOption !== -1)) {
            this.setState({subscriptionMessage: t("Successfully purchased!  Thanks so much!")});
        } else {
            this.setState({productMessage: t("Successfully purchased!  Thanks so much!")});
        }
        if(this.state.selectedOption === noads) {
            this.props.setShouldShowAds(false);
        } else {
            this.props.setIsSubscriber(true);
        }
        return InAppBilling.close();
    }
};

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setShouldShowAds, setIsSubscriber, setPurchasedItems, addPurchasedItem}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Purchases);
