'use strict';

import React, { Component } from 'react';
import {
    View,
    StyleSheet,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Text from '../shared/app-text';
import sharedStyles from '../../style/styles';
import Section from '../shared/section';
import {t} from '../../helpers/intl';
import { getLocationConfig, expansionHasTimelineSection, expansionHasSurvivorSection, expansionHasSettlementSection } from '../../helpers/helpers';
import SubHeader from '../shared/sub-header';
import AppText from '../shared/app-text';
import styles from '../../style/styles';
import { getColors } from '../../selectors/general';
import Popup from '../shared/popup';
import { setName, setDescription, setVersion, setReleaseNote } from '../../actions/custom-expansions';
import AppTextInput from '../shared/AppTextInput';

class Overview extends Component {
    titleInput;
    descriptionInput;
    versionInput;
    releaseNoteInput;
    state = {
        showEditDescription : false,
    }

    render() {
        return (
            <View>
                {this.renderDescription()}
                {expansionHasSettlementSection(this.props.expansion) && <Section title={t("Settlement")} rule="strong">
                    <View style={localStyles.wrapper}>
                        {this.renderInnovations(this.props.expansion)}
                        {this.renderLocations(this.props.expansion)}
                        {this.renderResources(this.props.expansion)}
                        {this.renderGear(this.props.expansion)}
                    </View>
                </Section>}
                {expansionHasSurvivorSection(this.props.expansion) && <Section title={t("Survivors")} rule="strong">
                    <View style={localStyles.wrapper}>
                        {this.renderFightingArts(this.props.expansion)}
                        {this.renderDisorders(this.props.expansion)}
                        {this.renderAbilities(this.props.expansion)}
                        {this.renderSevereInjuries(this.props.expansion)}
                        {this.renderWeaponProfs(this.props.expansion)}
                    </View>
                </Section>}
                {expansionHasTimelineSection(this.props.expansion) && <Section title={t("Timeline")} rule="strong">
                    <View style={localStyles.wrapper}>
                        {this.renderSettlementEvents(this.props.expansion)}
                        {this.renderTextEvents(t('Story Events'), this.props.expansion.storyEvents)}
                        {this.renderTextEvents(t('Showdowns'), this.props.expansion.showdowns)}
                        {this.renderTextEvents(t('Nemesis Encounters'), this.props.expansion.nemesisEncounters)}
                    </View>
                </Section>}
                {(
                    !expansionHasSettlementSection(this.props.expansion) &&
                    !expansionHasSurvivorSection(this.props.expansion) &&
                    !expansionHasTimelineSection(this.props.expansion)) &&
                        <AppText style={[styles.itemText, {paddingLeft: 5}]}>{t("Nothing here yet, use the tabs above to add content!")}</AppText>
                    }
            </View>
        );
    }

    renderDescription = () => {
        return <View>
            <Section title={this.props.expansion.name} rule="strong" icon="pencil" onPress={this.showEditDescription}>
                <AppText style={styles.italic}>{this.props.expansion.description ? this.props.expansion.description : t("No description")}</AppText>
                {!!this.props.expansion.version && <AppText>{t("Version %ver%", {ver: this.props.expansion.version})}</AppText>}
                {!!this.props.expansion.releaseNote && <AppText>{t("Latest change:")} {this.props.expansion.releaseNote}</AppText>}
            </Section>
            <Popup
                visible={this.state.showEditDescription}
                onDismissed={this.hideEditDescription}
            >
                {this.renderEditPopup()}
            </Popup>
        </View>
    }

    renderEditPopup = () => {
        return <View>
            <SubHeader title={t("Title")} onPress={this.focusTitleInput}/>
            <AppTextInput
                textRef={(ref) => this.titleInput = ref}
                inputStyle={styles.input}
                placeholder={t("Enter Title")}
                value={this.props.expansion.name}
                onChangeText={this.onTitleChange}
            />
            <View style={{height: 20}}></View>
            <SubHeader title={t("Description")} onPress={this.focusDescriptionInput}/>
            <AppTextInput
                textRef={(ref) => this.descriptionInput = ref}
                inputStyle={styles.input}
                placeholder={t("Enter Description")}
                value={this.props.expansion.description ? this.props.expansion.description : ''}
                onChangeText={this.onDescriptionChange}
            />
            <View style={{height: 20}}></View>
            <SubHeader title={t("Version")} onPress={this.focusVersionInput}/>
            <AppTextInput
                textRef={(ref) => this.versionInput = ref}
                inputStyle={styles.input}
                placeholder={t("Enter Version")}
                value={this.props.expansion.version ? this.props.expansion.version : ''}
                onChangeText={this.onVersionChange}
            />
            <View style={{height: 20}}></View>
            <SubHeader title={t("Release Note")} onPress={this.focusReleaseNoteInput}/>
            <AppTextInput
                textRef={(ref) => this.releaseNoteInput = ref}
                inputStyle={styles.input}
                placeholder={t("Enter Release Note")}
                value={this.props.expansion.releaseNote ? this.props.expansion.releaseNote : ''}
                onChangeText={this.onReleaseNoteChange}
            />
        </View>
    }

    focusTitleInput = () => {
        this.titleInput.focus();
    }

    focusDescriptionInput = () => {
        this.descriptionInput.focus();
    }

    focusVersionInput = () => {
        this.versionInput.focus();
    }

    focusReleaseNoteInput = () => {
        this.releaseNoteInput.focus();
    }

    onTitleChange = (text) => {
        this.props.setName(this.props.expansion.id, text);
    }

    onDescriptionChange = (text) => {
        this.props.setDescription(this.props.expansion.id, text);
    }

    onVersionChange = (text) => {
        this.props.setVersion(this.props.expansion.id, text);
    }

    onReleaseNoteChange = (text) => {
        this.props.setReleaseNote(this.props.expansion.id, text);
    }

    showEditDescription = () => {
        this.setState({showEditDescription: true});
    }

    hideEditDescription = () => {
        this.setState({showEditDescription: false});
    }

    renderInnovations = (exp) => {
        if(exp.innovations && exp.innovations.length > 0) {
            let innovationTitles = exp.innovations.map((name) => {
                return exp.innovationConfig[name].title;
            });

            return <View>
                <SubHeader title={t('Innovations')}/>
                <Text style={sharedStyles.itemText}>{innovationTitles.join(", ")}</Text>
            </View>
        }

        return null;
    }

    renderLocations = (exp) => {
        if(exp.locations && exp.locations.length > 0) {
            let locationTitles = exp.locations.map((name) => {
                return exp.locationConfig[name].title;
            });

            return <View>
                <SubHeader title={t('Locations')}/>
                <Text style={sharedStyles.itemText}>{locationTitles.join(", ")}</Text>
            </View>
        }

        return null;
    }

    renderResources = (exp) => {
        let locationConfig = getLocationConfig();
        let resources = [];
        let key = 0;
        if(exp.resourceConfig && Object.keys(exp.resourceConfig).length > 0) {
            for(let location in exp.resourceConfig) {
                let title = location;
                if(locationConfig[title]) {
                    title = locationConfig[title].title;
                }

                let resourceList = Object.values(exp.resourceConfig[location]).map((resource, i) => {
                    return <Text style={sharedStyles.itemText} key={i}>{resource.title}</Text>
                });

                resources.push(<View key={key}>
                    <Text style={sharedStyles.itemText}>{title}</Text>
                    <View style={{paddingLeft: 10}}>{resourceList}</View>
                </View>);

                key++;
            }

            return <View>
                <SubHeader title={t('Resources')}/>
                {resources}
            </View>;
        } else {
            return null;
        }
    }

    renderGear = (exp) => {
        let locationConfig = getLocationConfig();
        let gear = [];
        let key = 0;
        if(exp.gearConfig && Object.keys(exp.gearConfig).length > 0) {
            for(let location in exp.gearConfig) {
                let title = location;
                if(locationConfig[title]) {
                    title = locationConfig[title].title;
                }

                let gearList = Object.keys(exp.gearConfig[location]).map((gear, i) => {
                    return <Text style={sharedStyles.itemText} key={i}>{exp.gearConfig[location][gear].title}</Text>
                });

                gear.push(<View key={key}>
                    <Text style={sharedStyles.itemText}>{title}</Text>
                    <View style={{paddingLeft: 10}}>{gearList}</View>
                </View>);

                key++;
            }

            return <View>
                <SubHeader title={t('Gear')}/>
                {gear}
            </View>;
        } else {
            return null;
        }
    }

    renderFightingArts = (exp) => {
        if(exp.fightingArts && exp.fightingArts.length > 0) {
            let arts = exp.fightingArts.map((art, i) => {
                return <Text style={sharedStyles.itemText} key={i}>{exp.fightingArtConfig[art].title}</Text>
            });

            return <View>
                <SubHeader title={t('Fighting Arts')}/>
                <View>
                    {arts}
                </View>
            </View>
        } else {
            return null;
        }
    }

    renderDisorders = (exp) => {
        if(exp.disorders && exp.disorders.length > 0) {
            let disorders = exp.disorders.map((name, i) => {
                return <Text style={sharedStyles.itemText} key={i}>{exp.disorderConfig[name].title}</Text>
            });

            return <View>
                <SubHeader title={t('Disorders')}/>
                <View>
                    {disorders}
                </View>
            </View>
        } else {
            return null;
        }
    }

    renderAbilities = (exp) => {
        if(exp.abilities && exp.abilities.length > 0) {
            let abilities = exp.abilities.map((name, i) => {
                return <Text style={sharedStyles.itemText} key={i}>{exp.abilityConfig[name].title}</Text>
            });

            return <View>
                <SubHeader title={t('Abilities/Impairments')}/>
                <View>
                    {abilities}
                </View>
            </View>
        } else {
            return null;
        }
    }

    renderSevereInjuries = (exp) => {
        if(exp.severeInjuries && exp.severeInjuries.length > 0) {
            let severeInjuries = exp.severeInjuries.map((name, i) => {
                return <Text style={sharedStyles.itemText} key={i}>{exp.severeInjuryConfig[name].title}</Text>
            });

            return <View>
                <SubHeader title={t('Severe Injuries')}/>
                <View>
                    {severeInjuries}
                </View>
            </View>
        } else {
            return null;
        }
    }

    renderWeaponProfs = (exp) => {
        if(exp.weaponProfs && exp.weaponProfs.length > 0) {
            let profs = exp.weaponProfs.map((prof, i) => {
                return exp.weaponProfConfig[prof].title;
            });

            return <View>
                <SubHeader title={t('Weapon Proficiencies')}/>
                <Text style={sharedStyles.itemText}>{profs.join(', ')}</Text>
            </View>
        } else{
            return null;
        }
    }

    renderSettlementEvents = (exp) => {
        if(exp.settlementEvents && exp.settlementEvents.length > 0) {
            let events = exp.settlementEvents.map((prof, i) => {
                return exp.settlementEventConfig[prof].title;
            });

            return <View>
                <SubHeader title={t('Settlement Events')}/>
                <Text style={sharedStyles.itemText}>{events.join(', ')}</Text>
            </View>
        } else{
            return null;
        }
    }

    renderTextEvents = (title, events) => {
        events = Object.values(events);
        events = events.map((event) => {
            return event.title;
        });

        if(events && events.length > 0) {
            return <View>
                <SubHeader title={title}/>
                <Text style={sharedStyles.itemText}>{events.join(', ')}</Text>
            </View>
        } else {
            return null;
        }
    }
};

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setName, setDescription, setVersion, setReleaseNote}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Overview);

const localStyles = StyleSheet.create({
    wrapper: {
        paddingBottom: 5,
    },
});
