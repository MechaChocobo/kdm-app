'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    TouchableOpacity,
} from 'react-native';
import ColorContext from '../../../context/ColorContext';
import Icon from '../../shared/Icon';


export default class DeleteIcon extends Component {
    render() {
        return (
            <ColorContext.Consumer>
            { colors => {
                return <TouchableOpacity onPress={this.props.onPress} style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                    <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                </TouchableOpacity>
            }}
            </ColorContext.Consumer>
        );
    }
}

DeleteIcon.propTypes = {
    onPress: PropTypes.func.isRequired,
}

DeleteIcon.defaultProps = {
}
