import React, { PureComponent } from 'react';
import {
    View, ActivityIndicator,
} from 'react-native';
import compareVersions from 'compare-versions';

import RoundControl from '../../shared/buttons/RoundControl';
import RoundedIcon from '../../shared/rounded-icon';
import AppText from '../../shared/app-text';
import Button from '../../shared/button';
import LightModal from '../../shared/LightModal';
import ColorContext from '../../../context/ColorContext';
import styles from '../../../style/styles';
import { t } from '../../../helpers/intl';

export default class ExpansionStatusControl extends PureComponent {
    state = {
        showUpdate: false,
    }

    render() {
        return <ColorContext.Consumer>
            {colors =>
                <View>
                    {this.renderControl(colors)}
                    {this.hasUpdate() && <LightModal
                        visible={this.state.showUpdate}
                        onDismissed={this.hideUpdate}
                    >
                        <AppText style={styles.title}>{this.getUpdateHeader()}</AppText>
                        <AppText style={styles.itemText}>{this.props.releaseNotes}</AppText>
                        <View style={styles.row}>
                            <Button shape="rounded" onPress={this.hideUpdate} title={t("No")} style={{marginRight: 15}}/>
                            <Button shape="rounded" onPress={this.onUpdate} title={t("Yes")} />
                        </View>
                    </LightModal>}
                </View>
            }
        </ColorContext.Consumer>
    }

    getUpdateHeader = () => {
        if(this.props.installedVersion) {
            return t("Update %iver% to %ver%?", {iver: this.props.installedVersion, ver: this.props.version});
        } else {
            return t("Update to %ver%?", {ver: this.props.version});
        }
    }

    hideUpdate = () => {
        this.setState({showUpdate: false});
    }

    onUpdatePress = () => {
        this.setState({showUpdate: true});
    }

    onUpdate = () => {
        this.hideUpdate();
        this.onDownload();
    }

    renderControl = (colors) => {
        if(this.props.loading) {
            let style = {
                width: 35,
                height: 35,
                borderRadius: 35,
                alignItems: "center",
                justifyContent: "center",
                overflow: "hidden",
                backgroundColor: colors.WHITE,
            };

            return <View style={style}>
                <ActivityIndicator
                    size="small"
                    color={colors.HIGHLIGHT}
                />
            </View>
        }

        if(this.props.installed) {
            if(this.hasUpdate()) {
                // update
                return <View style={styles.centeredItems}>
                    <RoundControl onPress={this.onUpdatePress} size={25} gradient={false} icon="sk-warning" iconColor={colors.GOLD} color={colors.WHITE} />
                    <AppText>{this.props.version}</AppText>
                </View>
            } else { // up to date
                return <RoundedIcon size={25} icon="check" gradient={false} iconColor={colors.GREENDARK} color={colors.WHITE} />;
            }
        } else {
            return <RoundControl onPress={this.onDownload} size={25} gradient={false} icon="download-outline" iconColor={colors.HIGHLIGHT} color={colors.WHITE} />
        }
    }

    hasUpdate = () => {
        // downloaded from the drive
        if(this.props.version && !this.props.installedVersion) {
            return true;
        }

        return (
            this.props.installed &&
            this.props.installedVersion &&
            this.props.version &&
            compareVersions(this.props.version, this.props.installedVersion) === 1
        );
    }

    onDownload = () => {
        this.props.onDownload();
    }
}
