import React, { PureComponent } from 'react';
import {
    View,
} from 'react-native';
import Zebra from '../../shared/Zebra';
import AppText from '../../shared/app-text';
import ExpansionStatusControl from './ExpansionStatusControl';
import styles from '../../../style/styles';
import Icon from '../../shared/Icon';
import { t } from '../../../helpers/intl';

export default class ExpansionTile extends PureComponent {
    render() {
        return <View onPress={this.onPress} style={[styles.container, {borderRadius: 10, overflow: "hidden", marginTop: 15}]}>
            <Zebra zebra={true} style={[styles.row, styles.spaceBetween, styles.container, {padding: 5}]}>
                <View style={styles.flexShrink}>
                    <View style={[styles.rowCentered, styles.flexShrink]}>
                        {this.props.installed && <Icon
                            name="download-outline"
                            color={this.props.colors.WHITE}
                            size={15}
                        />}
                        {this.props.expansion.new === true && <AppText style={{color: this.props.colors.HIGHLIGHT}}>{t("New!")}</AppText>}
                        <AppText style={[styles.title, {flex: 1, flexWrap: 'wrap'}]}>{this.props.expansion.title}</AppText>
                    </View>
                    <AppText style={styles.italic}>{this.props.expansion.description}</AppText>
                </View>
                <View style={[styles.centeredItems, styles.justifyCentered, {marginRight: 5}]}>
                    <ExpansionStatusControl
                        installed={this.props.installed}
                        version={this.props.expansion.version}
                        installedVersion={this.props.installedVersion}
                        onDownload={this.onPress}
                        releaseNotes={this.props.expansion.releaseNotes}
                        loading={this.props.loading}
                    />
                </View>
            </Zebra>
        </View>
    }

    onPress = () => {
        if(this.props.loading) {
            return null;
        }

        this.props.onPress(this.props.expansion.downloadId);
    }
}
