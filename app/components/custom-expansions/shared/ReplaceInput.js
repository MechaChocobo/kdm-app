'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import ColorContext from '../../../context/ColorContext';
import { View } from 'react-native';
import SubHeader from '../../shared/sub-header';
import Popup from '../../shared/popup';
import ExpansionContentList from '../../shared/ExpansionContentList';
import AppText from '../../shared/app-text';
import DeleteIcon from './DeleteIcon';
import styles from '../../../style/styles';
import ExpansionStorageContentList from '../../shared/ExpansionStorageContentList';

export default class ReplaceInput extends PureComponent {
    state = {
        showReplace: false,
    }

    render() {
        return (
            <ColorContext.Consumer>
            { colors => {
                return <View>
                    <SubHeader
                        title={this.props.title}
                        onPress={this.showReplace}
                        icon="plus-circle-outline"
                        iconColor={colors.HIGHLIGHT}
                    />
                    {this.renderReplace()}
                    <Popup
                        visible={this.state.showReplace}
                        title={this.props.popupTitle}
                        onDismissed={this.hideReplace}
                    >
                        {this.props.storage && <ExpansionStorageContentList
                            allItems={this.props.allItems}
                            config={this.props.itemConfig}
                            expansions={this.props.expansions}
                            onSelect={this.setReplace}
                        />}
                        {!this.props.storage && <ExpansionContentList
                            allItems={this.props.allItems}
                            config={this.props.itemConfig}
                            expansions={this.props.expansions}
                            onSelect={this.setReplace}
                        />}
                    </Popup>
                </View>
            }}
            </ColorContext.Consumer>
        );
    }

    setReplace = (name) => {
        let config = {...this.props.config};
        if(name) {
            config.replace = name;
        } else {
            delete config.replace;
        }

        this.props.onChange(config);
        this.hideReplace();
    }

    showReplace = () => {
        this.setState({showReplace: true});
    }

    hideReplace = () => {
        this.setState({showReplace: false});
    }

    clearReplace = () => {
        this.setReplace(null);
    }

    renderReplace = () => {
        const item = this.props.config.replace;
        if(item) {
            if(typeof(item) === "object") {
                return <View>
                <AppText style={styles.itemText}>{this.props.itemConfig[item.location][item.id].title}</AppText>
                    <DeleteIcon onPress={this.clearReplace} />
                </View>
            } else {
                return <View>
                    <AppText style={styles.itemText}>{this.props.itemConfig[item].title}</AppText>
                    <DeleteIcon onPress={this.clearReplace} />
                </View>
            }
        } else {
            return null;
        }
    }
}

ReplaceInput.propTypes = {
    config: PropTypes.object.isRequired,
    itemConfig: PropTypes.object.isRequired,
    expansions: PropTypes.array.isRequired,
    allItems: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    popupTitle: PropTypes.string.isRequired,
    storage: PropTypes.bool,
}

ReplaceInput.defaultProps = {
    storage: false,
}
