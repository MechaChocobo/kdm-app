'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
} from 'react-native';

import ScrollableTabView from 'react-native-scrollable-tab-view-universal';
import SettlementEvents from './SettlementEvents';
import StoryEvents from './StoryEvents';
import Showdowns from './Showdowns';
import NemesisEncounters from './NemesisEncounters';
import TimelineEvents from './TimelineEvents';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';

export default class Timeline extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <ScrollableTabView
                        locked={true}
                        tabBarUnderlineStyle={{backgroundColor: colors.HIGHLIGHT}}
                        tabBarActiveTextColor={colors.HIGHLIGHT}
                        tabBarInactiveTextColor={colors.TEXT}
                        tabBarTextStyle={styles.tabText}
                    >
                        <SettlementEvents tabLabel={t("Settlement Events")} expansion={this.props.expansion}/>
                        <StoryEvents tabLabel={t("Story Events")} expansion={this.props.expansion}/>
                        <Showdowns tabLabel={t("Showdowns")} expansion={this.props.expansion}/>
                        <NemesisEncounters tabLabel={t("Nemesis Encounters")} expansion={this.props.expansion}/>
                        <TimelineEvents tabLabel={t("Timeline Events")} expansion={this.props.expansion}/>
                    </ScrollableTabView>
                }
            </ColorContext.Consumer>
        );
    }
};

var localStyles = StyleSheet.create({
    tabHeader: {
        marginLeft: 4
    },
    tabText: {
        color: "white",
        fontSize: 12,
    },
});
