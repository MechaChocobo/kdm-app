'use strict';

import React, { Component } from 'react';
import ScrollableTabView, { ScrollableTabBar } from 'react-native-scrollable-tab-view-universal';

import FightingArts from './FightingArts';
import Disorders from './Disorders';
import Abilities from './Abilities';
import SevereInjuries from './SevereInjuries';
import WeaponProficiencies from './WeaponProficiencies';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';
import ArmorSets from './ArmorSets';

export default class Survivor extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <ScrollableTabView
                        renderTabBar={() => <ScrollableTabBar />}
                        locked={true}
                        tabBarUnderlineStyle={{backgroundColor: colors.HIGHLIGHT}}
                        tabBarActiveTextColor={colors.HIGHLIGHT}
                        tabBarInactiveTextColor={colors.TEXT}
                        tabBarTextStyle={styles.tabText}
                    >
                        <FightingArts tabLabel={t("Fighting Arts")} expansion={this.props.expansion}/>
                        <Disorders tabLabel={t("Disorders")} expansion={this.props.expansion}/>
                        <Abilities tabLabel={t("Abilities/Impairments")} expansion={this.props.expansion} scrollRef={this.props.scrollRef}/>
                        <WeaponProficiencies tabLabel={t("Weapon Proficencies")} expansion={this.props.expansion}/>
                        <SevereInjuries tabLabel={t("Severe Injuries")} expansion={this.props.expansion}/>
                        <ArmorSets tabLabel={t("Armor Sets")} expansion={this.props.expansion}/>
                    </ScrollableTabView>
                }
            </ColorContext.Consumer>
        );
    }
};
