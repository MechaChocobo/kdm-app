'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { addSevereInjury, setSevereInjuryConfig, deleteSevereInjury } from '../../actions/custom-expansions';
import SurvivorAddition from './edit-components/SurvivorAddition';
import { getColors } from '../../selectors/general';

class SevereInjuries extends Component {
    constructor(props) {
        super(props);

        let renderedInjuries = {};
        for(let i = 0; i < props.expansion.severeInjuries.length; i++) {
            renderedInjuries[props.expansion.severeInjuries[i]] = true;
        }

        this.state = {renderedInjuries};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Severe Injury')}
                    icon="plus-circle-outline"
                    onPress={this.addSevereInjury}
                >
                    {this.props.expansion.severeInjuries.map((id) => {
                        return <SurvivorAddition
                            key={id}
                            id={id}
                            config={this.props.expansion.severeInjuryConfig[id]}
                            onConfigChange={this.onConfigChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={this.state.renderedInjuries[id]}
                        />
                    })}
                </Section>
            </View>
        )
    }

    addSevereInjury = () => {
        this.props.addSevereInjury(this.props.expansion.id);
    }

    onConfigChange = (id, config) => {
        this.props.setSevereInjuryConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        this.props.deleteSevereInjury(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setSevereInjuryConfig, deleteSevereInjury, addSevereInjury}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SevereInjuries);
