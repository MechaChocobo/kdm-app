'use strict';

import React, { PureComponent } from 'react';
import {
    StyleSheet,
    View,
    Alert,
    TouchableOpacity
} from 'react-native';
import Swipeout from 'react-native-swipeout';

import Text from '../shared/app-text';
import styles from '../../style/styles';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import ExpansionStatusControl from './shared/ExpansionStatusControl';
import Icon from '../shared/Icon';
import Zebra from '../shared/Zebra';

export default class ExpansionLineItem extends PureComponent {
    render() {
        return (
            <ColorContext.Consumer>
                {(colors) => {
                    let component = <View style={styles.swipeoutButton}><Icon name="delete" color="white" size={30}/></View>;
                    let buttons = [
                        {component, onPress:this.promptToDelete, backgroundColor:colors.RED}
                    ];

                    return (
                        <Swipeout
                            autoClose={true}
                            right={buttons}
                            buttonWidth={80}
                            backgroundColor="transparent"
                        >
                            <TouchableOpacity onPress={this.goToExpansion} style={{borderRadius: 10, overflow: "hidden"}}>
                                <Zebra zebra={true} style={[styles.row, styles.spaceBetween]}>
                                    <View style={[localStyles.content, styles.flexShrink]}>
                                        <Text style={localStyles.title}>
                                            {this.props.expansion.name}
                                        </Text>
                                        <View style={styles.listRow}>
                                            <Text style={styles.listItemDescription}>
                                                {this.buildContentString()}
                                            </Text>
                                        </View>
                                    </View>
                                    {this.renderStatus(colors)}
                                </Zebra>
                            </TouchableOpacity>
                        </Swipeout>
                    );
                }}
            </ColorContext.Consumer>
        );
    }

    isExpansionRemote = () => {
        return this.props.expansion.source && this.props.expansion.source === "internet";
    }

    renderStatus = () => {
        if(!this.isExpansionRemote()) {
            return null;
        }

        return <View style={[styles.justifyCentered, {marginRight: 10}]}>
            <ExpansionStatusControl
                installed={true}
                remoteData={this.props.remoteData}
                version={this.props.remoteData ? this.props.remoteData.version : '1.0.0'}
                installedVersion={this.props.expansion.version}
                onDownload={this.onExpansionUpdate}
                releaseNotes={this.props.remoteData ? this.props.remoteData.releaseNotes : ''}
                loading={this.props.loading}
            />
        </View>
    }

    onExpansionUpdate = () => {
        this.props.onUpdate(this.props.expansion.id, this.props.remoteData.downloadId);
    }

    showUpdate = () => {
        this.setState({showUpdate: true});
    }

    promptToDelete = () => {
        Alert.alert(t("Are you sure?"), t("Deleting an expansion from Scribe will remove it from any settlements and CANNOT BE UNDONE."), [
            {text: t("Cancel"), onPress: () => {}},
            {text: t("Confirm"), onPress: this.delete}
        ]);
    }

    delete = () => {
        this.props.onDelete(this.props.expansion.id);
    }

    goToExpansion = () => {
        this.props.onPress(this.props.expansion.id);
    }

    buildContentString = () => {
        let parts = [];

        if(this.props.expansion.innovations && this.props.expansion.innovations.length > 0) {
            parts.push(this.props.expansion.innovations.length === 1 ? t("1 innovation") : t("%num% innovations", {num: this.props.expansion.innovations.length}));
        }

        if(this.props.expansion.locations && this.props.expansion.locations.length > 0) {
            parts.push(this.props.expansion.locations.length === 1 ? t("1 location") : t("%num% locations", {num: this.props.expansion.locations.length}));
        }

        let resourceCount = this.getResourceCount();
        if(resourceCount > 0) {
            parts.push(resourceCount === 1 ? t("1 resource") : t("%num% resources", {num: resourceCount}));
        }

        let gearCount = this.getGearCount();
        if(gearCount > 0) {
            parts.push(gearCount === 1 ? t("1 gear item") : t("%num% gear items", {num: gearCount}));
        }

        if(this.props.expansion.fightingArts && this.props.expansion.fightingArts.length > 0) {
            parts.push(this.props.expansion.fightingArts.length === 1 ? t("1 fighting art") : t("%num% fighting arts", {num: this.props.expansion.fightingArts.length}));
        }

        if(this.props.expansion.disorders && this.props.expansion.disorders.length > 0) {
            parts.push(this.props.expansion.disorders.length === 1 ? t("1 disorder") : t("%num% disorders", {num: this.props.expansion.disorders.length}));
        }

        if(this.props.expansion.abilities && this.props.expansion.abilities.length > 0) {
            parts.push(this.props.expansion.abilities.length === 1 ? t("1 ability") : t("%num% abilities", {num: this.props.expansion.abilities.length}));
        }

        if(this.props.expansion.weaponProfs && this.props.expansion.weaponProfs.length > 0) {
            parts.push(this.props.expansion.weaponProfs.length === 1 ? t("1 weapon proficiency") : t("%num% weapon proficiencies", {num: this.props.expansion.weaponProfs.length}));
        }

        if(this.props.expansion.severeInjuries && this.props.expansion.severeInjuries.length > 0) {
            parts.push(this.props.expansion.severeInjuries.length === 1 ? t("1 severe injury") : t("%num% severe injuries", {num: this.props.expansion.severeInjuries.length}));
        }

        if(this.props.expansion.armorSets && this.props.expansion.armorSets.length > 0) {
            parts.push(this.props.expansion.armorSets.length === 1 ? t("1 armor set") : t("%num% armor sets", {num: this.props.expansion.armorSets.length}));
        }

        if(this.props.expansion.settlementEvents && this.props.expansion.settlementEvents.length > 0) {
            parts.push(this.props.expansion.settlementEvents.length === 1 ? t("1 settlement event") : t("%num% settlement events", {num: this.props.expansion.settlementEvents.length}));
        }

        if(this.props.expansion.storyEvents && Object.keys(this.props.expansion.storyEvents).length > 0) {
            parts.push(Object.keys(this.props.expansion.storyEvents).length === 1 ? t("1 story event") : t("%num% story events", {num: Object.keys(this.props.expansion.storyEvents).length}));
        }

        if(this.props.expansion.showdowns && Object.keys(this.props.expansion.showdowns).length > 0) {
            parts.push(Object.keys(this.props.expansion.showdowns).length === 1 ? t("1 showdown") : t("%num% showdowns", {num: Object.keys(this.props.expansion.showdowns).length}));
        }

        if(this.props.expansion.nemesisEncounters && Object.keys(this.props.expansion.nemesisEncounters).length > 0) {
            parts.push(Object.keys(this.props.expansion.nemesisEncounters).length === 1 ? t("1 nemesis encounter") : t("%num% nemesis encounters", {num: Object.keys(this.props.expansion.nemesisEncounters).length}));
        }

        if(this.props.expansion.milestones && Object.keys(this.props.expansion.milestones).length > 0) {
            parts.push(Object.keys(this.props.expansion.milestones).length === 1 ? t("1 milestone") : t("%num% milestones", {num: Object.keys(this.props.expansion.milestones).length}));
        }

        return parts.join(', ');
    }

    getResourceCount = () => {
        let count = 0;
        for(let source in this.props.expansion.resourceConfig) {
            count += Object.keys(this.props.expansion.resourceConfig[source]).length;
        }

        return count;
    }

    getGearCount = () => {
        let count = 0;
        for(let source in this.props.expansion.gearConfig) {
            count += Object.keys(this.props.expansion.gearConfig[source]).length;
        }

        return count;
    }
};

var localStyles = StyleSheet.create({
    title:{
        fontSize: 20,
        fontWeight: "600",
    },
    content: {
        marginLeft: "2%"
    }
});
