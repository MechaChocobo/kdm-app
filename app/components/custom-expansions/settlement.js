'use strict';

import React, { Component } from 'react';
import ScrollableTabView from 'react-native-scrollable-tab-view-universal';
import Innovations from './innovations';
import Locations from './locations';
import Resources from './resources';
import Gear from './gear';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import styles from '../../style/styles';

export default class Settlement extends Component {
    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <ScrollableTabView
                        locked={true}
                        tabBarUnderlineStyle={{backgroundColor: colors.HIGHLIGHT}}
                        tabBarActiveTextColor={colors.HIGHLIGHT}
                        tabBarInactiveTextColor={colors.TEXT}
                        tabBarTextStyle={styles.tabText}
                    >
                        <Innovations tabLabel={t("Innovations")} expansion={this.props.expansion}/>
                        <Locations tabLabel={t("Locations")} expansion={this.props.expansion}/>
                        <Resources tabLabel={t("Resources")} expansion={this.props.expansion}/>
                        <Gear tabLabel={t("Gear")} expansion={this.props.expansion} scrollRef={this.props.scrollRef}/>
                    </ScrollableTabView>
                }
            </ColorContext.Consumer>
        );
    }
};
