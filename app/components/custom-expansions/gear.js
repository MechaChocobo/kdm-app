'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { setGearConfig, deleteSingleGear, deleteGearLocation, addGear, addGearLocation, setGearCursed } from '../../actions/custom-expansions';
import SelectOneList from '../shared/select-one-list';
import Popup from '../shared/popup';
import { sortObjectArrayByTitle, getBaseLocationConfig } from '../../helpers/helpers';
import LocationGear from './edit-components/location-gear';
import { getColors } from '../../selectors/general';
import PageControls from '../shared/PageControls';

const MAX_GEAR_RENDER = 30;

class Gear extends Component {
    constructor(props) {
        super(props);

        let renderedLocations = {};
        let locations = Object.keys(this.props.expansion.gearConfig);

        for(let i = 0; i < locations.length; i++) {
            renderedLocations[locations[i]] = true;
        }

        this.state = {
            renderedLocations,
            showLocationPopup: false,
            currentLocation: 0,
        }
    }

    render() {
        let allLocations = Object.keys(this.props.expansion.gearConfig);
        let oversize = this.getNumGear() > MAX_GEAR_RENDER;
        // only render 1 location at a time if there's too much gear
        let locations = [];
        if(oversize) {
            locations = [allLocations[this.state.currentLocation]];
        } else {
            locations = allLocations;
        }

        return (
            <View style={styles.container}>
                <Section
                    title={t('Add Gear')}
                    icon="plus-circle-outline"
                    onPress={this.showLocationPopup}
                >
                    {oversize && <PageControls
                        style={[styles.width90, styles.centered, {marginTop: 15, marginBottom: 15}]}
                        onPrevious={this.onPagePrevious}
                        onNext={this.onPageNext}
                        previousDisabled={this.state.currentLocation === 0}
                        nextDisabled={this.state.currentLocation === allLocations.length - 1}
                        previousTitle={this.state.currentLocation > 0 ? this.getLocationTitle(allLocations[this.state.currentLocation - 1]) : ''}
                        nextTitle={this.state.currentLocation < allLocations.length - 1 ? this.getLocationTitle(allLocations[this.state.currentLocation + 1]) : ''}
                        numPages={allLocations.length}
                        currentPage={this.state.currentLocation}
                    />}
                    {locations.map((location) => {
                        return <LocationGear
                            key={location}
                            location={location}
                            locationTitle={this.getLocationTitle(location)}
                            gear={this.props.expansion.gearConfig[location]}
                            onAdd={this.onAdd}
                            onChange={this.onChange}
                            onDelete={this.onDelete}
                            onLocationDelete={this.onLocationDelete}
                            skipRenderAnimation={this.state.renderedLocations[location]}
                            onCursedChange={this.onCursedChange}
                            resources={this.props.expansion.resourceConfig}
                        />
                    })}
                    {oversize && <PageControls
                        style={[styles.width90, styles.centered, {marginTop: 15, marginBottom: 15}]}
                        onPrevious={this.onPagePreviousScroll}
                        onNext={this.onPageNextScroll}
                        previousDisabled={this.state.currentLocation === 0}
                        nextDisabled={this.state.currentLocation === allLocations.length - 1}
                        previousTitle={this.state.currentLocation > 0 ? this.getLocationTitle(allLocations[this.state.currentLocation - 1]) : ''}
                        nextTitle={this.state.currentLocation < allLocations.length - 1 ? this.getLocationTitle(allLocations[this.state.currentLocation + 1]) : ''}
                        numPages={allLocations.length}
                        currentPage={this.state.currentLocation}
                    />}
                </Section>
                <Popup
                    visible={this.state.showLocationPopup}
                    title={"Select Location"}
                    onDismissed={this.hideSourcePopup}
                >
                    <SelectOneList
                        items={this.getAvailableLocations()}
                        onChange={this.addLocation}
                        allowCustom={true}
                    />
                </Popup>
            </View>
        )
    }

    onPagePrevious = () => {
        this.setState({currentLocation: this.state.currentLocation - 1});
    }

    onPageNext = () => {
        this.setState({currentLocation: this.state.currentLocation + 1});
    }

    onPagePreviousScroll = () => {
        if(this.props.scrollRef) {
            this.props.scrollRef.scrollTo({y: 0, animated: false});
        }
        this.setState({currentLocation: this.state.currentLocation - 1});
    }

    onPageNextScroll = () => {
        if(this.props.scrollRef) {
            this.props.scrollRef.scrollTo({y: 0, animated: false});
        }
        this.setState({currentLocation: this.state.currentLocation + 1});
    }

    onCursedChange = (location, id, cursed) => {
        this.props.setGearCursed(this.props.expansion.id, location, id, cursed);
    }

    getLocationTitle = (location) => {
        let locations = {
            ...getBaseLocationConfig(),
            ...this.props.expansion.locationConfig
        }

        if(locations[location]) {
            return locations[location].title;
        } else {
            return location;
        }
    }

    showLocationPopup = () => {
        this.setState({showLocationPopup: true});
    }

    hideSourcePopup = () => {
        this.setState({showLocationPopup: false});
    }

    onLocationDelete = (location) => {
        if(this.state.currentLocation === Object.keys(this.props.expansion.gearConfig).length - 1) {
            this.setState({currentLocation: this.state.currentLocation - 1});
        }

        this.props.deleteGearLocation(this.props.expansion.id, location);
    }

    onAdd = (location) => {
        this.props.addGear(this.props.expansion.id, location);
    }

    onChange = (location, id, config) => {
        this.props.setGearConfig(this.props.expansion.id, location, id, config);
    }

    onDelete = (location, id) => {
        this.props.deleteSingleGear(this.props.expansion.id, location, id);
    }

    addLocation = (location) => {
        this.setState({showLocationPopup: false, currentLocation: 0});
        this.props.addGearLocation(this.props.expansion.id, location);
    }

    getNumGear = () => {
        let num = 0;

        for(let location in this.props.expansion.gearConfig) {
            num += Object.keys(this.props.expansion.gearConfig[location]).length;
        }

        return num;
    }

    getAvailableLocations = () => {
        let configs = [
            ...Object.values(getBaseLocationConfig()),
            {
                name: "Rare Gear",
                title: "Rare Gear",
            }
        ];
        let current = this.props.expansion.gearConfig;

        let available = [];
        for(let i = 0; i < configs.length; i++) {
            if(!current[configs[i].name]) {
                available.push({
                    name: configs[i].name,
                    title: configs[i].title,
                });
            }
        }

        available.sort(sortObjectArrayByTitle);
        return available;
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setGearConfig,
        deleteSingleGear,
        deleteGearLocation,
        addGear,
        addGearLocation,
        setGearCursed,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Gear);
