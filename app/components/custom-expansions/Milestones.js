'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Uuid from 'react-native-uuid';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import Milestone from './edit-components/Milestone';
import { addMilestone, setMilestoneConfig, deleteMilestone } from '../../actions/custom-expansions';
import LightModal from '../shared/LightModal';
import Button from '../shared/button';
import { MILESTONE_TYPE_GLOBAL, MILESTONE_TYPE_SETTLEMENT } from '../../reducers/custom-expansions';
import Icon from '../shared/Icon';
import AppText from '../shared/app-text';

class Milestones extends Component {
    animateFirst;
    constructor(props) {
        super(props);

        this.animateFirst = false;

        let stateIds = [];
        if(this.props.expansion.milestones) {
            for(let i = 0; i < Object.keys(this.props.expansion.milestones).length; i++) {
                stateIds.push(Uuid.v4());
            }
        }

        this.state = {
            stateIds,
            showPopup: false,
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Milestone')}
                    icon="plus-circle-outline"
                    onPress={this.showPopup}
                >
                    {this.props.expansion.milestones && Object.values(this.props.expansion.milestones).map((milestone, i) => {
                        return <Milestone
                            key={milestone.id}
                            config={milestone}
                            id={milestone.id}
                            onChange={this.onChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={!(this.animateFirst && i === 0)}
                            fightingArtConfig={this.props.expansion.fightingArtConfig}
                            gearConfig={this.props.expansion.gearConfig}
                            resourceConfig={this.props.expansion.resourceConfig}
                            settlementEventConfig={this.props.expansion.settlementEventConfig}
                        />
                    })}
                </Section>
                <LightModal
                    visible={this.state.showPopup}
                    onDismissed={this.hidePopup}
                >
                    <AppText style={[styles.title, {marginBottom: 20}]}>{t("Milestone Type")}</AppText>
                    <View style={[styles.rowCentered, styles.spaceBetween]}>
                        <Button style={{width: "45%"}} shape="rounded" onPress={this.addGlobal} title={t("Global")}>
                            <Icon name="sk-globe" size={20} color={this.props.colors.TEXT} />
                        </Button>
                        <View style={{width: 30}} />
                        <Button style={{width: "45%", paddingLeft: 15}} shape="rounded" onPress={this.addSettlement} title={t("Settlement")}>
                            <Icon name="sk-house" size={20} color={this.props.colors.TEXT} />
                        </Button>
                    </View>
                </LightModal>
            </View>
        )
    }

    showPopup = () => {
        this.setState({showPopup: true});
    }

    hidePopup = () => {
        this.setState({showPopup: false});
    }

    addGlobal = () => {
        this.addMilestone(MILESTONE_TYPE_GLOBAL);
        this.hidePopup();
    }

    addSettlement = () => {
        this.addMilestone(MILESTONE_TYPE_SETTLEMENT);
        this.hidePopup();
    }

    addMilestone = (type) => {
        this.animateFirst = true;
        let stateIds = this.state.stateIds;
        stateIds.splice(0, 0, Uuid.v4());
        this.setState({stateIds});
        this.props.addMilestone(this.props.expansion.id, type);
    }

    onChange = (id, config) => {
        this.props.setMilestoneConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        this.props.deleteMilestone(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({addMilestone, setMilestoneConfig, deleteMilestone}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Milestones);
