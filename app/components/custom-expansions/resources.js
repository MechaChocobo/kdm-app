'use strict';

import React, { Component } from 'react';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
    View,
} from 'react-native';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import Resource from './edit-components/resource';
import { setResourceConfig, deleteSingleResource, addResource, deleteResourceSource, addResourceSource } from '../../actions/custom-expansions';
import SelectOneList from '../shared/select-one-list';
import Popup from '../shared/popup';
import { getBaseResourceConfig, sortObjectArrayByTitle } from '../../helpers/helpers';
import { getColors } from '../../selectors/general';

class Resources extends Component {
    constructor(props) {
        super(props);

        let renderedSources = {};
        let sources = Object.keys(this.props.expansion.resourceConfig);

        for(let i = 0; i < sources.length; i++) {
            renderedSources[sources[i]] = true;
        }

        this.state = {
            renderedSources,
            showSourcePopup: false,
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add Resources')}
                    icon="plus-circle-outline"
                    onPress={this.showSourcePopup}
                >
                    {Object.keys(this.props.expansion.resourceConfig).map((source) => {
                        return <Resource
                            key={source}
                            source={source}
                            resources={this.props.expansion.resourceConfig[source]}
                            onAdd={this.onAdd}
                            onChange={this.onChange}
                            onDelete={this.onDelete}
                            onSourceDelete={this.onSourceDelete}
                            skipRenderAnimation={this.state.renderedSources[source]}
                        />
                    })}
                </Section>
                <Popup
                    visible={this.state.showSourcePopup}
                    title={"Select Type"}
                    onDismissed={this.hideSourcePopup}
                >
                    <SelectOneList
                        items={this.getResourceSources()}
                        onChange={this.addSource}
                        allowCustom={true}
                    />
                </Popup>
            </View>
        )
    }

    showSourcePopup = () => {
        this.setState({showSourcePopup: true});
    }

    hideSourcePopup = () => {
        this.setState({showSourcePopup: false});
    }

    onSourceDelete = (source) => {
        this.props.deleteResourceSource(this.props.expansion.id, source);
    }

    onAdd = (source) => {
        this.props.addResource(this.props.expansion.id, source);
    }

    onChange = (source, id, config) => {
        this.props.setResourceConfig(this.props.expansion.id, source, id, config);
    }

    onDelete = (source, id) => {
        this.props.deleteSingleResource(this.props.expansion.id, source, id);
    }

    addSource = (source) => {
        this.setState({showSourcePopup: false});
        this.props.addResourceSource(this.props.expansion.id, source);
    }

    getResourceSources = () => {
        let configs = [
            ...Object.keys(getBaseResourceConfig()),
        ];
        let current = Object.keys(this.props.expansion.resourceConfig);

        let available = [];
        for(let i = 0; i < configs.length; i++) {
            if(current.indexOf(configs[i]) === -1) {
                available.push({
                    name: configs[i],
                    title: configs[i],
                });
            }
        }

        available.sort(sortObjectArrayByTitle);
        return available;
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setResourceConfig, deleteSingleResource, addResource, deleteResourceSource, addResourceSource}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Resources);
