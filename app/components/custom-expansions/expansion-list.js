'use strict';

import React, { Component } from 'react';
import {
    FlatList,
    View,
    Linking,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SimpleToast from 'react-native-simple-toast';

import ExpansionLineItem from './expansion-line-item';
import Text from '../shared/app-text';
import Button from '../shared/button';
import sharedStyles from '../../style/styles';
import { goToCustomExpansion, changePage } from '../../actions/router';
import RoundedIcon from '../shared/rounded-icon';
import { t } from '../../helpers/intl';
import { deleteExpansion, importJsonExpansion } from '../../actions/custom-expansions';
import { getColors } from '../../selectors/general';
import { fetchExpansion } from '../../helpers/expansionList';

class ExpansionList extends Component {
    state = {
        loadingExpansions: {},
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <View style={{alignItems: "flex-end", marginTop: 10, marginRight: 20}}>
                    <RoundedIcon icon="help" size={20} color={this.props.colors.HIGHLIGHT} onPress={this.helpLink}/>
                </View>
                {this.props.expansions.length === 0 && <View style={sharedStyles.wrapper}><Text style={sharedStyles.itemStyle}>{t("No expansions created yet.  Click the button below to create one or tap on the Import/Export tab to import one!")}</Text></View>}
                <FlatList
                    style={[sharedStyles.width90, sharedStyles.centered]}
                    data={this.props.expansions}
                    renderItem={this.renderExpansion}
                    keyExtractor={(item, index) => index + ""}
                    refreshing={this.props.loading}
                    onRefresh={this.props.onRefresh}
                />
                <View style={{padding: "4%", paddingTop: 0}}>
                    <Button
                        title={"+ "+t("Create New Expansion")}
                        onPress={this.createNewExpansion}
                        color={this.props.colors.HIGHLIGHT}
                        shape="rounded"
                    />
                </View>
            </View>
        );
    }

    helpLink = () => {
        Linking.openURL("https://gitlab.com/taboobat/kdm-app/wikis/Custom-Expansions");
    }

    createNewExpansion = () => {
        this.props.changePage("create-expansion");
    }

    renderExpansion = (item) => {
        return(
            <View style={{paddingTop: 15}}>
                <ExpansionLineItem
                    expansion={item.item}
                    onPress={this.goToExpansion}
                    onDelete={this.deleteExpansion}
                    remoteData={this.props.expansionList[item.item.id]}
                    onUpdate={this.onExpansionUpdate}
                    loading={this.props.loading || this.state.loadingExpansions[item.item.id]}
                />
            </View>
        )
    }

    onExpansionUpdate = (id, urlId) => {
        let loadingExpansions = {...this.state.loadingExpansions};
        loadingExpansions[id] = true;
        this.setState({loadingExpansions});
        fetchExpansion(urlId).then((data) => {
            const error = this.props.importJsonExpansion(data, "internet");
            let loadingExpansions = {...this.state.loadingExpansions};
            delete loadingExpansions[id];
            this.setState({loadingExpansions});

            if(error !== true) {
                SimpleToast.show(error, SimpleToast.LONG);
            }
        });
    }

    deleteExpansion = (id) => {
        this.props.deleteExpansion(id);
    }

    goToExpansion = (id) => {
        this.props.goToCustomExpansion(id);
    }
};

function mapStateToProps(state, props) {
    return {
        expansions: Object.values(state.customExpansions),
        expansionList: state.expansionList.expansions,
        loading: state.expansionList.loading,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goToCustomExpansion, changePage, deleteExpansion, importJsonExpansion}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpansionList);
