'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { addAbility, setAbilityConfig, deleteAbility } from '../../actions/custom-expansions';
import SurvivorAddition from './edit-components/SurvivorAddition';
import { getColors } from '../../selectors/general';
import PageControls from '../shared/PageControls';
import AbilityConfig from '../../config/abilities.json';

const ABILITIES_PER_PAGE = 30;

class Abilities extends Component {
    constructor(props) {
        super(props);

        let renderedAbilities = {};
        for(let i = 0; i < props.expansion.abilities.length; i++) {
            renderedAbilities[props.expansion.abilities[i]] = true;
        }

        this.state = {
            renderedAbilities,
            currentAbilityPage: 0,
        };
    }

    render() {
        let allAbilities = this.props.expansion.abilities;
        const numPages = Math.ceil(allAbilities.length / ABILITIES_PER_PAGE);
        let oversize = allAbilities.length > ABILITIES_PER_PAGE;
        let abilities = [];
        let begin = this.state.currentAbilityPage * ABILITIES_PER_PAGE;
        let end = begin + ABILITIES_PER_PAGE > allAbilities.length ? allAbilities.length : begin + ABILITIES_PER_PAGE;
        if(oversize) {
            abilities = allAbilities.slice(begin, end);
        } else {
            abilities = allAbilities;
        }

        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Ability/Impairment')}
                    icon="plus-circle-outline"
                    onPress={this.addAbility}
                >
                    {oversize && <PageControls
                        style={[styles.width90, styles.centered, {marginTop: 15, marginBottom: 15}]}
                        onPrevious={this.onPagePrevious}
                        onNext={this.onPageNext}
                        previousDisabled={this.state.currentAbilityPage === 0}
                        nextDisabled={this.state.currentAbilityPage === numPages - 1}
                        previousTitle={this.state.currentAbilityPage > 0 ? (begin + 1 - ABILITIES_PER_PAGE) + ' - ' + (begin) : ''}
                        nextTitle={this.state.currentAbilityPage < numPages - 1 ? (begin + 1 + ABILITIES_PER_PAGE) + ' - ' + (end + ABILITIES_PER_PAGE > allAbilities.length ? allAbilities.length : end + ABILITIES_PER_PAGE) : ''}
                        numPages={numPages}
                        currentPage={this.state.currentAbilityPage}
                    />}
                    {abilities.map((id) => {
                        return <SurvivorAddition
                            key={id}
                            id={id}
                            config={this.props.expansion.abilityConfig[id]}
                            onConfigChange={this.onConfigChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={this.state.renderedAbilities[id]}
                            baseConfig={AbilityConfig}
                            replaceTitle={t('Replaces Ability')}
                            replacePopupTitle={t('Select Ability')}
                        />
                    })}
                    {oversize && <PageControls
                        style={[styles.width90, styles.centered, {marginTop: 15, marginBottom: 15}]}
                        onPrevious={this.onPagePreviousScroll}
                        onNext={this.onPageNextScroll}
                        previousDisabled={this.state.currentAbilityPage === 0}
                        nextDisabled={this.state.currentAbilityPage === numPages - 1}
                        previousTitle={this.state.currentAbilityPage > 0 ? (begin + 1 - ABILITIES_PER_PAGE) + ' - ' + (begin) : ''}
                        nextTitle={this.state.currentAbilityPage < numPages - 1 ? (begin + 1 + ABILITIES_PER_PAGE) + ' - ' + (end + ABILITIES_PER_PAGE > allAbilities.length ? allAbilities.length : end + ABILITIES_PER_PAGE) : ''}
                        numPages={numPages}
                        currentPage={this.state.currentAbilityPage}
                    />}
                </Section>
            </View>
        )
    }

    onPagePrevious = () => {
        this.setState({currentAbilityPage: this.state.currentAbilityPage - 1});
    }

    onPageNext = () => {
        this.setState({currentAbilityPage: this.state.currentAbilityPage + 1});
    }

    onPagePreviousScroll = () => {
        if(this.props.scrollRef) {
            this.props.scrollRef.scrollTo({y: 0, animated: false});
        }
        this.setState({currentAbilityPage: this.state.currentAbilityPage - 1});
    }

    onPageNextScroll = () => {
        if(this.props.scrollRef) {
            this.props.scrollRef.scrollTo({y: 0, animated: false});
        }
        this.setState({currentAbilityPage: this.state.currentAbilityPage + 1});
    }

    addAbility = () => {
        this.setState({currentAbilityPage: 0});
        this.props.addAbility(this.props.expansion.id);
    }

    onConfigChange = (id, config) => {
        this.props.setAbilityConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        if(this.state.currentAbilityPage * ABILITIES_PER_PAGE >= this.props.expansion.abilities.length - 1) {
            this.setState({currentAbilityPage: this.state.currentAbilityPage - 1});
        }
        this.props.deleteAbility(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setAbilityConfig, deleteAbility, addAbility}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Abilities);
