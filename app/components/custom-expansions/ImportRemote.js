'use strict';

import React, { PureComponent } from 'react';
import {
    View,
    FlatList,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import SimpleToast from 'react-native-simple-toast';

import styles from '../../style/styles';
import { getColors } from '../../selectors/general';
import { t } from '../../helpers/intl';
import { importJsonExpansion } from '../../actions/custom-expansions';
import { setExpansionList, setExpansionListFilter } from '../../actions/expansion-list';
import { fetchExpansion, sortExpansionList } from '../../helpers/expansionList';
import ExpansionTile from './shared/ExpansionTile';
import Button from '../shared/button';
import AppText from '../shared/app-text';

class CustomExpansions extends PureComponent {
    state = {
        loadingExpansions: {},
    }

    render() {
        return (
            <View style={styles.container}>
                {this.renderExpansions()}
                {Object.keys(this.props.expansionList).length === 0 &&
                    <AppText style={[styles.itemText, styles.italic, styles.width90, styles.centered]}>{t("No community expansions could be loaded.  Check your internet connection and try again")}</AppText>
                }
            </View>
        );
    }

    renderExpansions = () => {
        if(Object.keys(this.props.expansionList).length === 0) {
            return null;
        }

        return <FlatList
            style={[styles.width90, styles.centered]}
            data={["filters", ...Object.values(this.props.expansionList).sort(sortExpansionList), "spacer"]}
            renderItem={this.renderExpansion}
            keyExtractor={(item, index) => index + ""}
            onRefresh={this.props.onRefresh}
            refreshing={this.props.loading}
        />
    }

    renderExpansion = (item) => {
        if(item.item === "filters") {
            return <View style={[styles.rowCentered, styles.spaceEvenly, {marginTop: 15}]}>
                <Button skeleton={true} color={this.props.filter === 'all' ? this.props.colors.HIGHLIGHT : this.props.colors.TEXT} onPress={this.setFilterAll} title={t("All")} shape="rounded" />
                <Button skeleton={true} color={this.props.filter === 'downloaded' ? this.props.colors.HIGHLIGHT : this.props.colors.TEXT} onPress={this.setFilterDownloaded} title={t("Downloaded")} shape="rounded" />
                <Button skeleton={true} color={this.props.filter === 'remote' ? this.props.colors.HIGHLIGHT : this.props.colors.TEXT} onPress={this.setFilterRemote} title={t("Not Downloaded")} shape="rounded" />
            </View>
        }

        if(item.item === "spacer") {
            return <View style={{height: 15}} />
        }

        const expansion = item.item;

        if(this.props.filter === "downloaded" && !this.props.installedExpansions[expansion.id]) {
            return null
        }

        if(this.props.filter === "remote" && this.props.installedExpansions[expansion.id]) {
            return null
        }

        return <ExpansionTile
            expansion={expansion}
            onPress={this.downloadExpansion}
            loading={this.props.loading || this.state.loadingExpansions[expansion.downloadId]}
            installed={this.props.installedExpansions[expansion.id] !== undefined}
            colors={this.props.colors}
            installedVersion={this.props.installedExpansions[expansion.id] ? this.props.installedExpansions[expansion.id].version : undefined}
        />
    }

    setFilterAll = () => {
        this.props.setExpansionListFilter('all');
    }

    setFilterDownloaded = () => {
        this.props.setExpansionListFilter('downloaded');
    }

    setFilterRemote = () => {
        this.props.setExpansionListFilter('remote');
    }

    downloadExpansion = (id) => {
        let loadingExpansions = {...this.state.loadingExpansions};
        loadingExpansions[id] = true;
        this.setState({loadingExpansions, error: false});
        fetchExpansion(id).then((response) => {
            let loadingExpansions = {...this.state.loadingExpansions};
            delete loadingExpansions[id];
            this.setState({loadingExpansions});
            this.runImport(response);
        }).catch((error) => {
            let loadingExpansions = {...this.state.loadingExpansions};
            delete loadingExpansions[id];
            this.setState({loadingExpansions, error: t("Error downloading expansion: %error%", {error})});
        });
    }

    runImport = (data) => {
        const error = this.props.importJsonExpansion(data, "internet");
        if(error !== true) {
            SimpleToast.show(error, SimpleToast.LONG);
        }
    }
};

function mapStateToProps(state, props) {
    return {
        installedExpansions: state.customExpansions,
        expansionList: state.expansionList.expansions,
        loading: state.expansionList.loading,
        filter: state.expansionList.filter,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({importJsonExpansion, setExpansionList, setExpansionListFilter}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomExpansions);
