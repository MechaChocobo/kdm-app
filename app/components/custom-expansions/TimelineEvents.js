'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { setLyEventConfig, addLyToTimeline, deleteLyFromTimeline } from '../../actions/custom-expansions';
import TimelineEvent from './edit-components/TimelineEvent';
import Popup from '../shared/popup';
import SelectOneList from '../shared/select-one-list';
import { getColors } from '../../selectors/general';

class TimelineEvents extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAddLyPopup: false,
            renderedLys: Object.keys(this.props.expansion.timeline),
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Timeline Event')}
                    icon="plus-circle-outline"
                    onPress={this.showAddLyPopup}
                >
                    {this.renderEvents()}
                </Section>
                <Popup
                    visible={this.state.showAddLyPopup}
                    title={"Select Lantern Year"}
                    onDismissed={this.onAddLyPopupClosed}
                >
                    <SelectOneList items={this.getAvailableLys()} onChange={this.addLy}/>
                </Popup>
            </View>
        )
    }

    renderEvents = () => {
        let events = this.props.expansion.timeline;
        return Object.keys(events).map((ly) => {
            return <TimelineEvent
                key={ly}
                ly={ly}
                events={events[ly]}
                settlementEvents={this.props.settlementEvents}
                settlementEventConfig={this.props.settlementEventConfig}
                storyEvents={this.props.storyEvents}
                showdowns={this.props.showdowns}
                nemesisEncounters={this.props.nemesisEncounters}
                specialShowdowns={{...this.props.showdowns, ...this.props.nemesisEncounters}}
                onChange={this.onChange}
                onDelete={this.onDelete}
                skipRenderAnimation={this.state.renderedLys.indexOf(ly) !== -1}
            />
        });
    }

    getAvailableLys = () => {
        let events = this.props.expansion.timeline;
        let lys = [];

        for(let i = 0; i <= 30; i++) {
            if(events[i]) {
                continue;
            }

            lys.push({name: i, title: ''+i});
        }

        return lys;
    }

    showAddLyPopup = () => {
        this.setState({showAddLyPopup: true});
    }

    onAddLyPopupClosed = () => {
        this.setState({showAddLyPopup: false});
    }

    addLy = (ly) => {
        this.setState({showAddLyPopup: false});
        this.props.addLyToTimeline(this.props.expansion.id, ly);
    }

    onChange = (ly, config) => {
        this.props.setLyEventConfig(this.props.expansion.id, ly, config);
    }

    onDelete = (ly) => {
        this.props.deleteLyFromTimeline(this.props.expansion.id, ly);
    }
}


function mapStateToProps(state, props) {
    return {
        settlementEventConfig: state.customExpansions[props.expansion.id].settlementEventConfig,
        settlementEvents: state.customExpansions[props.expansion.id].settlementEvents,
        storyEvents: state.customExpansions[props.expansion.id].storyEvents,
        showdowns: state.customExpansions[props.expansion.id].showdowns,
        nemesisEncounters: state.customExpansions[props.expansion.id].nemesisEncounters,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setLyEventConfig, addLyToTimeline, deleteLyFromTimeline}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineEvents);
