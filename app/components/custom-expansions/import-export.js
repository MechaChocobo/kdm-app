'use strict';

import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
    Platform,
    ScrollView,
    Alert,
    PermissionsAndroid,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Toast from 'react-native-simple-toast';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import RNFS from 'react-native-fs';
import FilePicker from 'react-native-file-picker';
import CompareVersions from 'compare-versions';
import Collapsible from 'react-native-collapsible';
import Share from 'react-native-share';

import Text from '../shared/app-text';
import styles from '../../style/styles';
import Button from '../shared/button';
import { t } from '../../helpers/intl';
import { toFileSafeName, CHECKBOX_SIZE } from '../../helpers/helpers';
import { importCustomExpansions, validateCESchema } from '../../actions/custom-expansions';
import { getColors } from '../../selectors/general';
import Hr from '../shared/Hr';
let packageInfo = require('../../../package.json');

class ImportExport extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedExpansion: "",
            showExported: false,
            exported: [],
            selectedExported: "",
            sharing: false,
        }
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                {Platform.OS === "ios" && <View style={{paddingLeft: 5}}><Text style={styles.itemText}>{t("Warning: on iOS uninstalling Scribe will also remove any backups on the device.  If you want to transfer them to your computer follow %a%these steps%a%.", {linkUrl: "https://support.apple.com/en-us/HT201301", linkColor: this.props.colors.BLUE})}</Text></View>}
                <View style={{marginBottom: 50}}>
                    <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%"}}>
                        {this.renderExpansions()}
                        <Button title={t("Export Expansion")} onPress={this.createBackupWrapper} shape="rounded" disabled={!this.state.selectedExpansion}/>
                    </View>
                    <Hr />
                    <Button style={{marginLeft: "10%", marginRight: "10%"}} title={t("Import Expansion")} onPress={this.importBackupWrapper} shape="rounded"/>
                    <Hr />
                    <Button style={{marginLeft: "10%", marginRight: "10%"}} title={t("Share Exported Expansion")} onPress={this.shareBackupWrapper} color={this.props.colors.HIGHLIGHT} shape="rounded"/>
                    <Collapsible collapsed={this.state.showExported === false}>
                        {this.renderExports()}
                    </Collapsible>
                </View>
            </ScrollView>
        );
    }

    renderExports = () => {
        let files = this.state.exported.map((backup, i) => {
            return <View style={{flexDirection: "row", flex: 1, alignItems: "center"}} key={i}>
                <TouchableOpacity style={{flexDirection: "row", flex: .9, alignItems: "center"}} onPress={this.selectExported(i)}>
                    <Icon style={{paddingRight: 15}} name={this.state.selectedExported === i ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color="white"/>
                    <Text style={styles.itemText}>{backup.name}</Text>
                </TouchableOpacity>
                {this.state.sharing === false && <Icon name="close" onPress={this.confirmDelete(i)} size={30} color={this.props.colors.SECONDARY} />}
            </View>
        });

        let button = <Button title={t("Import Selected")} onPress={this.importSelectedExport} shape="rounded" disabled={this.state.selectedExported === ""}/>
        let header = t("Select file to import");
        if(this.state.sharing === true) {
            button = <Button title={t("Share Selected")} onPress={this.shareSelectedBackup} color={this.props.colors.SECONDARY} disabled={this.state.selectedExported === ""} shape="rounded"/>
            header = t("Select file to share");
        }

        return <View style={{paddingLeft: "2%"}}>
            <Text style={{fontSize: 20, fontWeight: "bold", paddingTop: 10}}>{header}</Text>
            {files}
            <View style={{paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%", paddingBottom: "5%"}}>
                {button}
            </View>
        </View>
    }

    confirmDelete = (i) => {
        return () => {
            Alert.alert(t("Are you sure?"), t("Deleting an exported expansion is permanent and cannot be undone."), [
                {text: t("Cancel"), onPress: () => {}},
                {text: t("Confirm"), onPress: this.deleteExported(i)}
            ]);
        }
    }

    deleteExported = (i) => {
        return () => {
            let exported = this.state.exported;
            let toDelete = exported[i];
            RNFS.unlink(toDelete.path).then(() => {
                exported.splice(i, 1);
                this.setState({exported});
            });
        }
    }

    selectExported = (i) => {
        return () => {
            if(this.state.selectedExported === i) {
                this.setState({selectedExported: ""});
            } else {
                this.setState({selectedExported: i});
            }
        }
    }

    importSelectedExport = () => {
        let chosenFile = this.state.exported[this.state.selectedExported];
        RNFS.readFile(chosenFile.path, "utf8").then((file) => {
            this.runImport(file);
            this.setState({selectedExported: ""});
        }).catch((error) => {
            Toast.show(t("Error importing file"));
        });
    }

    renderExpansions = () => {
        return Object.values(this.props.expansions).map((expansion, i) => {
            return(
                <TouchableOpacity style={{marginBottom: 10}} onPress={this.toggleSelectExpansion(expansion.id)} key={i}>
                    <View style={styles.rowCentered}>
                        <Icon name={this.state.selectedExpansion == expansion.id ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color="white"/>
                        <Text style={styles.itemText}>{expansion.name}</Text>
                    </View>
                </TouchableOpacity>
            );
        });
    }

    toggleSelectExpansion = (id) => {
        return () => {
            if(id === this.state.selectedExpansion) {
                this.setState({selectedExpansion: ""});
            } else {
                this.setState({selectedExpansion: id});
            }
        }
    }

    shareBackupWrapper = () => {
        if(Platform.OS === "android") {
            this.requestFilePickerPermissions(this.shareBackupFile);
        } else {
            this.shareBackupFile();
        }
    }

    shareBackupFile = () => {
        if(Platform.OS === "ios") {
            if(this.state.showExported === false || this.state.sharing === false) {
                this.setState({showExported: true, sharing: true});
                RNFS.readDir(RNFS.DocumentDirectoryPath + '/custom-expansions').then(results => {
                    this.setState({exported: results});
                }).catch(() => {});
            } else {
                this.setState({showExported: false, sharing: false})
            }
        } else {
            FilePicker.showFilePicker({title: t("Pick a file to share")}, (response) => {
                if(response.didCancel) {
                } else if(response.error) {
                } else {
                    this.share(response.path);
                }
            });
        }
    }

    shareSelectedBackup = () => {
        let backup = this.state.exported[this.state.selectedExported];
        this.share(backup.path).then(() => {
            this.setState({selectedExported: ""});
        });
    }

    share = (path) => {
        return RNFS.readFile(path, "utf8").then((file) => {
            Share.open({
                title: "Share via",
                message: file,
                subject: path.match(/[a-zA-Z0-9\-\.]+$/)[0],
            }).then(() => {})
            .catch((err) => {});
        });
    }

    createBackupWrapper = () => {
        if(Platform.OS === "android") {
            this.requestFileWritePermission();
        } else {
            this.exportExpansion();
        }
    }

    requestFileWritePermission = () => {
        PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
                'title': t('Scribe External Storage Permission'),
                'message': t('Scribe needs access to your external storage in order to create a backup file')
            }
        ).then((granted) => {
            if (granted === PermissionsAndroid.RESULTS.GRANTED || granted === true) {
                this.exportExpansion();
            } else {
                Toast.show(t("You must grant storage access in order to create backups"), Toast.LONG);
            }
        });
    }

    exportExpansion = () => {
        if(!this.props.expansions[this.state.selectedExpansion]) {
            Toast.show(t("Something went wrong selecting the expansion.  Reselect and try again."));
            return;
        }

        if(Platform.OS === "android") {
            // safety just to make sure that this already exists
            RNFS.mkdir(RNFS.ExternalStorageDirectoryPath + '/scribe-kdm');
        }

        let dir = Platform.select({
            ios: RNFS.DocumentDirectoryPath + '/custom-expansions',
            android: RNFS.ExternalStorageDirectoryPath + '/scribe-kdm/custom-expansions'
        });

        let stamp = Date.now();
        let name = toFileSafeName(this.props.expansions[this.state.selectedExpansion].name);
        let filename = name+'-'+stamp+'.json';
        let path = Platform.select({
            ios: dir + '/'+filename,
            android: dir + '/'+filename
        });

        let contents = JSON.stringify({
            expansion: this.props.expansions[this.state.selectedExpansion],
            version: packageInfo.version,
        });

        // safety to make sure that the expansion dir exists
        RNFS.mkdir(dir);

        RNFS.writeFile(path, contents, "utf8").then(() => {
            let message = Platform.select({
                ios: '/custom-expansions/'+filename,
                android: '/scribe-kdm/custom-expansions/'+filename
            });

            Toast.show(t("Expansion exported to %file%", {file: message}), Toast.LONG);
        }).catch((error) => {
            Toast.show(t("There was an error writing the export file."), Toast.LONG);
        });
    }

    importBackupWrapper = () => {
        if(Platform.OS === "android") {
            this.requestFilePickerPermissions(this.importBackup);
        } else {
            this.importBackup();
        }
    }

    requestFilePickerPermissions = (callback) => {
        PermissionsAndroid.requestMultiple(
            [PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE],
            {
                'title': t('Scribe External Storage Permission'),
                'message': t('Scribe needs access to your external storage in order to import a backup file')
            }
        ).then((granted) => {
            let valid = true;
            for(let permission in granted) {
                if(granted[permission] !== PermissionsAndroid.RESULTS.GRANTED || granted === true) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                callback();
            } else {
                Toast.show(t("You must grant storage access in order to import backups"), Toast.LONG);
            }
        });
    }

    importBackup = () => {
        if(Platform.OS === "ios") {
            if(this.state.showExported === false || this.state.sharing === true) {
                this.setState({showExported: true, sharing: false});
                RNFS.readDir(RNFS.DocumentDirectoryPath + '/custom-expansions').then(results => {
                    this.setState({exported: results});
                }).catch(() => {});
            } else {
                this.setState({showExported: false, sharing: false});
            }
        } else {
            FilePicker.showFilePicker({title: t("Pick an expansion file to import")}, (response) => {
                if(response.didCancel) {
                } else if(response.error) {
                } else {
                    RNFS.readFile(response.path, "utf8").then((file) => {
                        this.runImport(file);
                    }).catch((error) => {
                        Toast.show(t("Error importing file"), Toast.LONG);
                    });
                }
            });
        }
    }

    runImport = (file) => {
        let data = null;
        try {
            data = JSON.parse(file);
        } catch(e) {
            Toast.show(t("Improperly formatted file, it should be in JSON format"), Toast.LONG);
            return;
        }

        if(!data.expansion || !data.version) {
            Toast.show(t("The chosen file does not appear to contain a valid expansion"), Toast.LONG);
            return;
        }

        if(data.version && CompareVersions(data.version, packageInfo.version) === 1) {
            Toast.show(t("Trying to import an expansion from a newer version of Scribe (%version%), please update to the latest version.", {version: data.version}), Toast.LONG);
            return;
        }

        this.props.importCustomExpansions(data.expansion);

        if(data.version && CompareVersions(data.version, packageInfo.version) === -1) {
            this.props.validateCESchema(data.version);
        }

        Toast.show(t("Expansion %name% imported!", {name: data.expansion.name}), Toast.LONG);
    }
};

function mapStateToProps(state, props) {
    return {
        expansions: state.customExpansions,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({importCustomExpansions, validateCESchema}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ImportExport);
