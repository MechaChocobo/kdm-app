'use strict';

import React, { Component } from 'react';
import {
    View,
    Keyboard,
    StyleSheet,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Uuid from 'react-native-uuid';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { addShowdown, setShowdown, deleteShowdown, setShowdownConfig } from '../../actions/custom-expansions';
import TextEvent from './edit-components/TextEvent';
import { getColors } from '../../selectors/general';
import AppTextInput from '../shared/AppTextInput';
import SubHeader from '../shared/sub-header';

class Showdowns extends Component {
    animateFirst;
    quarryInput;

    constructor(props) {
        super(props);

        this.animateFirst = false;

        let stateIds = [];
        for(let i = 0; i < this.props.expansion.showdowns.length; i++) {
            stateIds.push(Uuid.v4());
        }

        this.state = {stateIds};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Showdown')}
                    icon="plus-circle-outline"
                    onPress={this.addEvent}
                >
                    {Object.values(this.props.expansion.showdowns).map((event, i) => {
                        return <TextEvent
                            key={event.name}
                            text={event.title}
                            id={event.name}
                            label={t('Showdown')}
                            placeholder={t('Enter Showdown')}
                            onChange={this.onChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={!(this.animateFirst && i === 0)}
                        >
                            <SubHeader
                                title={t("Quarry")}
                                onPress={this.quarryInputFocus}
                            />
                            <AppTextInput
                                textRef={(ref) => this.quarryInput = ref}
                                inputStyle={localStyles.input}
                                placeholder={t("Enter quarry name")}
                                value={event.quarry ? event.quarry : ''}
                                onChangeText={this.onQuarryChange(event)}
                                returnKeyType="default"
                                onBlur={Keyboard.dismiss}
                            />
                        </TextEvent>
                    })}
                </Section>
            </View>
        )
    }

    onQuarryChange = (event) => {
        return (text) => {
            let config = {...event};
            config.quarry = text;
            if(!config.quarry) {
                delete config.quarry;
            }
            this.props.setShowdownConfig(this.props.expansion.id, event.name, config);
        }
    }

    quarryInputFocus = () => {
        this.quarryInput.focus();
    }

    addEvent = () => {
        this.animateFirst = true;
        let stateIds = this.state.stateIds;
        stateIds.splice(0, 0, Uuid.v4());
        this.setState({stateIds});
        this.props.addShowdown(this.props.expansion.id);
    }

    onChange = (id, text) => {
        this.props.setShowdown(this.props.expansion.id, id, text);
    }

    onDelete = (id) => {
        this.props.deleteShowdown(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setShowdown, addShowdown, deleteShowdown, setShowdownConfig}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Showdowns);

const localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
});
