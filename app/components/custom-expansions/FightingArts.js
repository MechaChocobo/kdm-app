'use strict';

import React, { Component } from 'react';
import {
    View,
    TouchableOpacity,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { setFAConfig, deleteFA, addFA } from '../../actions/custom-expansions';
import SurvivorAddition from './edit-components/SurvivorAddition';
import AppText from '../shared/app-text';
import SubHeader from '../shared/sub-header';
import { getColors } from '../../selectors/general';
import { CHECKBOX_SIZE } from '../../helpers/helpers';
import FAConfig from '../../config/fighting-arts.json';

class FightingArts extends Component {
    constructor(props) {
        super(props);

        let renderedArts = {};
        for(let i = 0; i < props.expansion.fightingArts.length; i++) {
            renderedArts[props.expansion.fightingArts[i]] = true;
        }

        this.state = {renderedArts};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Fighting Art')}
                    icon="plus-circle-outline"
                    onPress={this.addFightingArt}
                >
                    {this.props.expansion.fightingArts.map((art) => {
                        return <SurvivorAddition
                            key={art}
                            id={art}
                            config={this.props.expansion.fightingArtConfig[art]}
                            onConfigChange={this.onConfigChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={this.state.renderedArts[art]}
                            baseConfig={FAConfig}
                            replaceTitle={t('Replaces Fighting Art')}
                            replacePopupTitle={t('Select Fighting Art')}
                        >
                            <SubHeader title={t("Secret Fighting Art")} />
                            <TouchableOpacity onPress={this.toggleSecret(art)}>
                                <View style={styles.rowCentered}>
                                    <Icon name={(this.props.expansion.fightingArtConfig[art].type === "secret") ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                                    <AppText style={styles.itemText}> {t("Secret Fighting Art")}</AppText>
                                </View>
                            </TouchableOpacity>
                            <SubHeader title={t("Add to Deck By Default")} />
                            <TouchableOpacity onPress={this.toggleAddByDefault(art)}>
                                <View style={styles.rowCentered}>
                                    <Icon name={(this.props.expansion.fightingArtConfig[art].skipAdd !== true) ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                                    <AppText style={styles.itemText}> {t("Add to deck by default")}</AppText>
                                </View>
                            </TouchableOpacity>
                        </SurvivorAddition>
                    })}
                </Section>
            </View>
        )
    }

    toggleAddByDefault = (art) => {
        return () => {
            let config = {...this.props.expansion.fightingArtConfig[art]};

            if(config.skipAdd === true) {
                delete config.skipAdd;
            } else {
                config.skipAdd = true;
            }

            this.props.setFAConfig(this.props.expansion.id, art, config);
        }
    }

    toggleSecret = (art) => {
        return () => {
            let config = {...this.props.expansion.fightingArtConfig[art]};

            if(config.type === "secret") {
                delete config.type;
            } else {
                config.type = "secret";
            }

            this.props.setFAConfig(this.props.expansion.id, art, config);
        }
    }

    addFightingArt = () => {
        this.props.addFA(this.props.expansion.id);
    }

    onConfigChange = (id, config) => {
        this.props.setFAConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        this.props.deleteFA(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setFAConfig, deleteFA, addFA}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FightingArts);
