'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Uuid from 'react-native-uuid';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { addNemesis, setNemesis, deleteNemesis } from '../../actions/custom-expansions';
import TextEvent from './edit-components/TextEvent';
import { getColors } from '../../selectors/general';

class NemesisEncounters extends Component {
    animateFirst;
    constructor(props) {
        super(props);

        this.animateFirst = false;
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Nemesis Encounter')}
                    icon="plus-circle-outline"
                    onPress={this.addEvent}
                >
                    {Object.values(this.props.expansion.nemesisEncounters).map((event, i) => {
                        return <TextEvent
                            key={event.name}
                            text={event.title}
                            id={event.name}
                            label={t('Nemesis Encounter')}
                            placeholder={t('Enter Nemesis Encounter')}
                            onChange={this.onChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={!(this.animateFirst && i === 0)}
                        />
                    })}
                </Section>
            </View>
        )
    }

    addEvent = () => {
        this.animateFirst = true;
        this.props.addNemesis(this.props.expansion.id);
    }

    onChange = (id, text) => {
        this.props.setNemesis(this.props.expansion.id, id, text);
    }

    onDelete = (id) => {
        this.props.deleteNemesis(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setNemesis, addNemesis, deleteNemesis}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NemesisEncounters);
