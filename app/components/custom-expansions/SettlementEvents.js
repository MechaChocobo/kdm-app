'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { addSettlementEvent, setSettlementEventConfig, deleteSettlementEvent } from '../../actions/custom-expansions';
import SettlementEvent from './edit-components/SettlementEvent';
import { getColors } from '../../selectors/general';

class SettlementEvents extends Component {
    constructor(props) {
        super(props);

        let renderedEvents = {};
        for(let i = 0; i < props.expansion.settlementEvents.length; i++) {
            renderedEvents[props.expansion.settlementEvents[i]] = true;
        }

        this.state = {renderedEvents};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Settlement Event')}
                    icon="plus-circle-outline"
                    onPress={this.addSettlementEvent}
                >
                {this.props.expansion.settlementEvents.map((id) => {
                    return <SettlementEvent
                        key={id}
                        id={id}
                        config={this.props.expansion.settlementEventConfig[id]}
                        onConfigChange={this.onConfigChange}
                        onDelete={this.onDelete}
                        skipRenderAnimation={this.state.renderedEvents[id]}
                    />
                })}
                </Section>
            </View>
        )
    }

    addSettlementEvent = () => {
        this.props.addSettlementEvent(this.props.expansion.id);
    }

    onConfigChange = (id, config) => {
        this.props.setSettlementEventConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        this.props.deleteSettlementEvent(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setSettlementEventConfig, deleteSettlementEvent, addSettlementEvent}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementEvents);
