import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {View, StyleSheet, Keyboard, TouchableOpacity} from 'react-native';
import Text from '../../shared/app-text';
import styles from '../../../style/styles';
import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import ColorContext from '../../../context/ColorContext';
import AppTextInput from '../../shared/AppTextInput';

export default class SettlementEffect extends Component {
    constructor(props) {
        super(props);

        this.titleInput = null;
        this.descriptionInput = null;
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={{marginBottom: 5}}>
                        {(this.props.effect === "survivalLimit" || this.props.effect === "survivalDepart") && <View>
                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <TouchableOpacity onPress={this.removeEffect} style={{paddingRight: 5}}><Icon name="close" color={colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                                <Text style={styles.itemText}>{this.props.effect === "survivalLimit" ? t("Survival Limit") : t("Survival on Depart")}: </Text>
                                <View style={{width: 40}}>
                                    <AppTextInput
                                        inputStyle={localStyles.input}
                                        value={""+this.props.value}
                                        onChangeText={this.numChange}
                                    />
                                </View>
                            </View>
                        </View>}
                        {this.props.effect === "endeavors" && <View>
                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <TouchableOpacity onPress={this.removeEffect} style={{paddingRight: 5}}><Icon name="close" color={colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                                <Text style={styles.itemText}>{t("Endeavors")}:</Text>
                                <TouchableOpacity onPress={this.addEndeavor} style={{paddingLeft: 5}}><Icon name="plus-circle-outline" color={colors.HIGHLIGHT} size={18}/></TouchableOpacity>
                            </View>
                            {this.renderEndeavors(colors)}
                        </View>}
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    renderEndeavors = (colors) => {
        return this.props.value.map((item, i) => {
            return (
                <View key={i} style={{borderColor: colors.PRIMARYLIGHT, borderTopWidth: 1, marginTop: 5}}>
                    <View style={{marginLeft: 25}}>
                        <TouchableOpacity onPress={this.removeEndeavor(i)}>
                            <View style={styles.rowCentered}>
                                <Icon name="close" color={colors.HIGHLIGHT} size={22}/>
                                <Text style={styles.itemText}> {t("Remove Endeavor")}</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.rowCentered}>
                            <Text style={styles.itemText}>{t('Endeavor Cost')}: </Text>
                            <AppTextInput
                                inputStyle={localStyles.input}
                                value={""+item.endeavorCost}
                                onChangeText={this.changeEndeavorCost(i)}
                            />
                        </View>
                        <View>
                            <SubHeader
                                title={t("Title")}
                                onPress={this.titleInputFocus}
                                titleWeight="light"
                            />
                            <AppTextInput
                                    textRef={ref => this.titleInput = ref}
                                    inputStyle={localStyles.inputLeft}
                                    placeholder={t("Enter Title")}
                                    value={item.title}
                                    onChangeText={this.changeEndeavorTitle(i)}
                            />
                        </View>
                        <View>
                        <SubHeader
                                title={t("Description")}
                                onPress={this.descriptionInputFocus}
                                titleWeight="light"
                            />
                            <AppTextInput
                                    textRef={ref => this.descriptionInput = ref}
                                    inputStyle={localStyles.inputLeft}
                                    placeholder={t("Enter Description")}
                                    value={item.description}
                                    onChangeText={this.changeEndeavorDesc(i)}
                                    multiline={true}
                                    returnKeyType="default"
                                    onBlur={Keyboard.dismiss}
                            />
                        </View>
                    </View>
                </View>
            );
        });
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    descriptionInputFocus = () => {
        this.descriptionInput.focus();
    }

    removeEndeavor = (i) => {
        return () => {
            let value = [...this.props.value];
            value.splice(i, 1);
            this.effectChange(value);
        }
    }

    changeEndeavorTitle = (i) => {
        return (text) => {
            let value = [...this.props.value];
            value[i].title = text;
            this.effectChange(value);
        }
    }

    changeEndeavorDesc = (i) => {
        return (text) => {
            let value = [...this.props.value];
            value[i].description = text;
            this.effectChange(value);
        }
    }

    changeEndeavorCost = (i) => {
        return (text) => {
            if(text.charAt(0) === '0') {
                text = text.substr(1);
            }

            if((isNaN(text) || !(text === parseInt(text, 10).toString())) && text !== "") {
                return;
            }

            let value = [...this.props.value];
            value[i].endeavorCost = +text;
            this.effectChange(value);
        }
    }

    numChange = (text) => {
        if(text.charAt(0) === '0') {
            text = text.substr(1);
        }

        if((isNaN(text) || !(text === parseInt(text, 10).toString())) && text !== "") {
            return;
        }

        this.effectChange(+text);
    }

    removeEffect = () => {
        this.props.onRemove(this.props.effect);
    }

    effectChange = (value) => {
        this.props.onChange(this.props.effect, value);
    }

    addEndeavor = () => {
        let value = [...this.props.value];
        value.push({
            "endeavorCost": 1,
            "title": "",
            "description": ""
        });
        this.effectChange(value);
    }
}

const localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        fontWeight: "600",
        padding: 0,
        textAlign: "center",
    },
    inputLeft: {
        fontSize: 16,
        fontWeight: "300",
        borderBottomWidth: 1,
        padding: 0,
    }
});

SettlementEffect.propTypes = {
    effect: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    onRemove: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
}
