import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity, Keyboard} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Text from '../../shared/app-text';
import sharedStyles from '../../../style/styles';
import { t } from '../../../helpers/intl';
import Popup from '../../shared/popup';
import MultiSelect from '../../shared/multi-select';
import SelectOneList from '../../shared/select-one-list';
import SettlementEffect from './settlement-effect';
import SurvivorEffect from './survivor-effect';
import SubHeader from '../../shared/sub-header';
import FadeInWrapper from '../../shared/fade-in-wrapper';
import ColorContext from '../../../context/ColorContext';

import InnovationConfig from '../../../config/innovations.json';
import AppTextInput from '../../shared/AppTextInput';
import { getItemsByExpansion, getSortedExpansions, sortObjectArrayByTitle } from '../../../helpers/helpers';
import ReplaceInput from '../shared/ReplaceInput';

const settlementEffects = [
    {"name": "survivalLimit", "title": "Survival Limit"},
    {"name": "survivalDepart", "title": "Survival on Depart"},
    {"name": "endeavors", "title": "Endeavors"}
];
const survivorEffects = [
    {"name": "allSurvivors", "title": "All Current Survivors"},
    {"name": "allNewSurvivors", "title": "All New Survivors"},
    {"name": "newBornEffects", "title": "All Newborn Survivors"},
];

const BASE_HEIGHT = 251;

export default class Innovation extends Component {
    sortedInnovations;
    wrapper = null;

    constructor(props) {
        super(props);

        const innovationsByExpansion = getItemsByExpansion(InnovationConfig);
        this.state = {
            showConsequencePopup: false,
            showAddSettlementEffect: false,
            availableSettlementEffects: this.getAvailableSettlementEffects(),
            showAddSurvivorEffects: false,
            availableSurvivorEffects: this.getAvailableSurvivorEffects(),
            innovationConfig: {...InnovationConfig, ...this.props.customInnovations},
            coreInnovationsByExpansion: innovationsByExpansion,
            sortedExpansions: getSortedExpansions(Object.keys(innovationsByExpansion)),
            showReplace: false,
        };

        this.sortedInnovations = Object.values(this.state.innovationConfig);
        this.sortedInnovations.sort(sortObjectArrayByTitle);

        for(let i = 0; i < this.sortedInnovations.length; i++) {
            if(this.sortedInnovations[i].name === this.props.innovation.name) {
                this.sortedInnovations.splice(i, 1);
                break;
            }
        }

        this.titleInput = null;
        this.descriptionInput = null;
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={sharedStyles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                        // logHeight={true}
                    >
                        <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                            <TouchableOpacity onPress={this.deleteInnovation}>
                                <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <SubHeader
                                title={t("Title")}
                                onPress={this.titleInputFocus}
                            />
                            <AppTextInput
                                textRef={(ref) => this.titleInput = ref}
                                inputStyle={[styles.input, {color: colors.TEXT}]}
                                placeholder={t("Enter Title")}
                                placeholderTextColor={colors.GRAY}
                                value={this.props.innovation.title}
                                onChangeText={this.onTitleChange}
                            />
                        </View>
                        <View>
                            <SubHeader
                                title={t("Consequence Of")}
                                onPress={this.showConsequencePopup}
                            />
                            {this.renderConsequences()}
                        </View>
                        <Popup
                            visible={this.state.showConsequencePopup}
                            title={t("Select Innovations")}
                            onDismissed={this.onConsequencePopupDismissed}
                        >
                            <MultiSelect
                                items={this.sortedInnovations}
                                selected={Array.isArray(this.props.innovation.consequenceOf) ? this.props.innovation.consequenceOf : [this.props.innovation.consequenceOf]}
                                onChange={this.onConsequenceChange}
                            />
                        </Popup>
                        <View>
                            <SubHeader
                                title={t("Description")}
                                onPress={this.descriptionInputFocus}
                            />
                                <AppTextInput
                                    textRef={ref => this.descriptionInput = ref}
                                    inputStyle={[styles.input, {borderColor: colors.PRIMARYDARK}]}
                                    placeholder={t("Enter Description")}
                                    placeholderTextColor="gray"
                                    value={this.props.innovation.description}
                                    onChangeText={this.onDescriptionChange}
                                    multiline={true}
                                    returnKeyType="default"
                                    onBlur={Keyboard.dismiss}
                                />
                        </View>
                        <View>
                            <SubHeader
                                title={t("Settlement Effects")}
                                onPress={this.showAddSettlementEffect}
                                icon="plus-circle-outline"
                                iconColor={this.state.availableSettlementEffects.length > 0 ? colors.HIGHLIGHT : colors.GRAY}
                            />
                            {this.renderSettlementEffects()}
                            <Popup
                                visible={this.state.showAddSettlementEffect}
                                title={t("Select Type")}
                                onDismissed={this.onAddSettlementEffectDismissed}
                            >
                                <SelectOneList items={this.state.availableSettlementEffects} onChange={this.addSettlementEffect}/>
                            </Popup>
                        </View>
                        <View>
                            <SubHeader
                                title={t("Survivor Effects")}
                                onPress={this.showAddSurvivorEffects}
                                icon="plus-circle-outline"
                                iconColor={this.state.availableSurvivorEffects.length > 0 ? colors.HIGHLIGHT : colors.GRAY}
                            />
                            {this.renderSurvivorEffects()}
                            <Popup
                                visible={this.state.showAddSurvivorEffects}
                                title={t("Select Type")}
                                onDismissed={this.onAddSurvivorEffectDismissed}
                            >
                                <SelectOneList items={this.state.availableSurvivorEffects} onChange={this.addSurvivorEffect}/>
                            </Popup>
                            <ReplaceInput
                                title={t("Replaces Innovation")}
                                popupTitle={t("Select Innovation")}
                                config={this.props.innovation}
                                allItems={this.state.coreInnovationsByExpansion}
                                itemConfig={InnovationConfig}
                                expansions={this.state.sortedExpansions}
                                onChange={this.triggerConfigChanged}
                            />
                        </View>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    deleteInnovation = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.innovation.name);
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    descriptionInputFocus = () => {
        this.descriptionInput.focus();
    }

    renderSurvivorEffects = () => {
        let effects = [];
        for(let i = 0; i < survivorEffects.length; i++) {
            if(this.props.innovation[survivorEffects[i].name]) {
                effects.push(survivorEffects[i].name);
            }
        }

        return effects.map((type, i) => {
            return (
                <SurvivorEffect
                    key={i}
                    effect={type}
                    value={this.props.innovation[type]}
                    onRemove={this.removeSurvivorEffect}
                    onChange={this.changeSurvivorEffect}
                />
            )
        });
    }

    removeSurvivorEffect = (type) => {
        let config = {...this.props.innovation};
        delete config[type];
        this.triggerConfigChanged(config);
        this.setState({availableSurvivorEffects: this.getAvailableSurvivorEffects(config)});
    }

    changeSurvivorEffect = (type, val) => {
        let config = {...this.props.innovation};
        config[type] = val;
        this.triggerConfigChanged(config);
    }

    addSurvivorEffect = (type) => {
        let config = {...this.props.innovation};

        config[type] = {};
        this.setState({showAddSurvivorEffects: false, availableSurvivorEffects: this.getAvailableSurvivorEffects(config)});
        this.triggerConfigChanged(config);
    }

    showAddSurvivorEffects = () => {
        if(this.state.availableSurvivorEffects.length > 0) {
            this.setState({showAddSurvivorEffects: true});
        }
    }

    onAddSurvivorEffectDismissed = () => {
        this.setState({showAddSurvivorEffects: false});
    }

    getAvailableSettlementEffects = () => {
        return settlementEffects.filter((item) => {
            return !this.props.innovation.settlementEffects[item.name];
        });
    }

    getAvailableSurvivorEffects = (config = null) => {
        if(!config) {
            config = this.props.innovation;
        }

        return survivorEffects.filter((item) => {
            return !config[item.name];
        });
    }

    renderSettlementEffects = () => {
        if(this.props.innovation.settlementEffects) {
            return Object.keys(this.props.innovation.settlementEffects).map((effect, i) => {
                return <SettlementEffect
                    key={i}
                    effect={effect}
                    value={this.props.innovation.settlementEffects[effect]}
                    onRemove={this.removeSettlementEffect}
                    onChange={this.changeSettlementEffect}
                />
            });
        }

        return null;
    }

    changeSettlementEffect = (type, val) => {
        let config = {...this.props.innovation};
        config.settlementEffects[type] = val;
        this.triggerConfigChanged(config);
    }

    removeSettlementEffect = (type) => {
        let config = {...this.props.innovation};
        delete config.settlementEffects[type];
        config.settlementEffects = {...config.settlementEffects};
        this.triggerConfigChanged(config);
        this.setState({availableSettlementEffects: this.getAvailableSettlementEffects()});
    }

    addSettlementEffect = (type) => {
        let config = {...this.props.innovation};
        if(type === "endeavors") {
            config.settlementEffects[type] = [];
        } else {
            config.settlementEffects[type] = 1;
        }

        this.setState({showAddSettlementEffect: false, availableSettlementEffects: this.getAvailableSettlementEffects()});
        this.triggerConfigChanged(config);
    }

    showAddSettlementEffect = () => {
        if(this.state.availableSettlementEffects.length > 0) {
            this.setState({showAddSettlementEffect: true});
        }
    }

    onAddSettlementEffectDismissed = () => {
        this.setState({showAddSettlementEffect: false});
    }

    onConsequenceChange = (values) => {
        let innovation = {...this.props.innovation};
        innovation.consequenceOf = values;
        this.props.onConfigChange(innovation);
    }

    renderConsequences = () => {
        if(this.props.innovation.consequenceOf.length > 0) {
            return <Text style={sharedStyles.itemText}>{this.props.innovation.consequenceOf.map((name) => this.state.innovationConfig[name].title).join(', ')}</Text>
        } else {
            return <Text style={sharedStyles.itemText}>None</Text>
        }
    }

    onConsequencePopupDismissed = () => {
        this.setState({showConsequencePopup: false});
    }

    showConsequencePopup = () => {
        this.setState({showConsequencePopup: true});
    }

    onTitleChange = (title) => {
        let config = {...this.props.innovation};
        config.title = title;
        this.triggerConfigChanged(config);
    }

    onDescriptionChange = (description) => {
        let config = {...this.props.innovation};
        config.description = description;
        this.triggerConfigChanged(config);
    }

    triggerConfigChanged = (config) => {
        this.setState({config});
        this.props.onConfigChange(config);
    }
}

var styles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
    container: {
        marginBottom: 20
    }
});
