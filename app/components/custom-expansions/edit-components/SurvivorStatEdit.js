import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import { getModdableSurvivorStats } from '../../../helpers/helpers';
import SurvivorStatInput from './survivor-stat-input';
import Popup from '../../shared/popup';
import SelectOneList from '../../shared/select-one-list';

const survivorStats = getModdableSurvivorStats();
export default class SurvivorStatEdit extends Component {
    render() {
        return (
            <View>
                <View style={{marginLeft: 25}}>
                    <SurvivorStatInput value={this.props.value} onChange={this.onStatChange}/>
                </View>
                <Popup
                    visible={this.props.showAddStatChange}
                    title={"Select Attribute to Change"}
                    onDismissed={this.props.onAddStatChangeDismissed}
                >
                    <SelectOneList items={Object.values(survivorStats)} onChange={this.addStatChange}/>
                </Popup>
            </View>
        )
    }

    addStatChange = (stat) => {
        let value = {...this.props.value};
        let arr = stat.split(".");
        if(arr.length > 1) {
            if(!value[arr[0]]) {
                value[arr[0]] = {};
            }

            value[arr[0]][arr[1]] = this.getDefaultStatValue(stat);
        } else {
            value[arr[0]] = this.getDefaultStatValue(stat);
        }

        this.triggerChange(value);
        this.props.onAddStatChangeDismissed();
    }

    getDefaultStatValue = (stat) => {
        if(survivorStats[stat].defaultVal) {
            return survivorStats[stat].defaultVal;
        }

        if(survivorStats[stat].type === PropTypes.number) {
            return 1;
        } else if(survivorStats[stat].type === PropTypes.string) {
            return "";
        }
    }

    triggerChange = (val) => {
        this.props.onChange(val);
    }

    onStatChange = (value) => {
        this.props.onChange(value);
    }
}

SurvivorStatEdit.propTypes = {
    value: PropTypes.object,
    showAddStatChange: PropTypes.bool.isRequired,
    onAddStatChangeDismissed: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
}

SurvivorStatEdit.defaulProps = {
    value: {},
}
