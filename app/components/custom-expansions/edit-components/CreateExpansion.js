'use strict';

import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    TextInput,
    ScrollView,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Button from '../../shared/button';
import sharedStyles from '../../../style/styles';
import { goBack } from '../../../actions/router';
import SubHeader from '../../shared/sub-header';
import { t } from '../../../helpers/intl';
import AppText from '../../shared/app-text';
import { createExpansion } from '../../../actions/custom-expansions';
import { getColors } from '../../../selectors/general';
import AppTextInput from '../../shared/AppTextInput';
import BackMenu from '../../shared/BackMenu';

class CreateExpansion extends Component {
    titleInput;
    descriptionInput;
    versionInput;
    state = {
        title :"",
        description :"",
        version: "1.0.0",
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <BackMenu />
                <AppText style={sharedStyles.pageHeader}>{t("Create Expansion")}</AppText>
                <ScrollView style={{flex: 1, paddingLeft: 5}}>
                    <SubHeader title={t("Title")} onPress={this.focusTitleInput}/>
                    <AppTextInput
                        textRef={(ref) => this.titleInput = ref}
                        inputStyle={styles.input}
                        placeholder={t("Enter Title")}
                        value={this.state.title}
                        onChangeText={this.onTitleChange}
                    />
                    <View style={{height: 20}}></View>
                    <SubHeader title={t("Description")} onPress={this.focusDescriptionInput}/>
                    <AppTextInput
                        textRef={(ref) => this.descriptionInput = ref}
                        inputStyle={styles.input}
                        placeholder={t("Enter Description")}
                        value={this.state.description}
                        onChangeText={this.onDescriptionChange}
                    />
                    <View style={{height: 20}}></View>
                    <SubHeader title={t("Version")} onPress={this.focusVersionInput}/>
                    <AppTextInput
                        textRef={(ref) => this.versionInput = ref}
                        inputStyle={styles.input}
                        placeholder={t("Enter Version")}
                        value={this.state.version}
                        onChangeText={this.onVersionChange}
                    />
                </ScrollView>
                <View style={{alignItems: "flex-end"}}>
                    <View style={styles.button}>
                        <Button title={t("Create Expansion")} onPress={this.createExpansion} disabled={this.state.title === ""}/>
                    </View>
                </View>
            </View>
        );
    }

    createExpansion = () => {
        this.props.createExpansion(this.state.title, this.state.description, this.state.version);
        this.setState({title: ""});
        this.props.goBack();
    }

    focusTitleInput = () => {
        this.titleInput.focus();
    }

    focusDescriptionInput = () => {
        this.descriptionInput.focus();
    }

    focusVersionInput = () => {
        this.versionInput.focus();
    }

    onTitleChange = (text) => {
        this.setState({title: text});
    }

    onDescriptionChange = (text) => {
        this.setState({description: text});
    }

    onVersionChange = (text) => {
        this.setState({version: text});
    }
};

function mapStateToProps(state, props) {
    return {
        expansions: Object.values(state.customExpansions),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({createExpansion, goBack}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateExpansion);

var styles = StyleSheet.create({
    input: {
        fontSize: 20,
        padding: 0,
        borderBottomWidth: 1,
    },
    button: {
        borderRadius: 10,
        marginTop: 5,
        marginBottom: 5,
        width: "70%",
        alignSelf: "center",
        overflow: "hidden",
    }
});
