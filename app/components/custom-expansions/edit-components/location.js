import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { t } from '../../../helpers/intl';
import SettlementEffect from './settlement-effect';
import SubHeader from '../../shared/sub-header';
import FadeInWrapper from '../../shared/fade-in-wrapper';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import styles from '../../../style/styles';
import AppTextInput from '../../shared/AppTextInput';
import LocationConfig from '../../../config/locations.json';
import { getItemsByExpansion, getSortedExpansions } from '../../../helpers/helpers';
import ReplaceInput from '../shared/ReplaceInput';

const BASE_HEIGHT = 157;
export default class Location extends Component {
    wrapper;
    titleInput;

    constructor(props) {
        super(props);

        const locationsByExpansion = getItemsByExpansion(LocationConfig);
        this.state = {
            coreLocationsByExpansion: locationsByExpansion,
            sortedExpansions: getSortedExpansions(Object.keys(locationsByExpansion)),
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true} style={styles.marginBottom20}>
                            <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                                <TouchableOpacity onPress={this.deleteLocation}>
                                    <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                                </TouchableOpacity>
                            </View>
                            <SubHeader
                                title={t("Title")}
                                onPress={this.titleInputFocus}
                            />
                            <AppTextInput
                                textRef={(ref) => this.titleInput = ref}
                                inputStyle={localStyles.input}
                                placeholder={t("Enter Title")}
                                value={this.props.location.title}
                                onChangeText={this.onTitleChange}
                            />
                            <SubHeader
                                title={t("Endeavors")}
                            />
                            <SettlementEffect
                                effect="endeavors"
                                value={this.props.location.settlementEffects.endeavors}
                                onRemove={this.removeEndeavors}
                                onChange={this.changeEndeavors}
                            />
                            <ReplaceInput
                                title={t("Replaces Location")}
                                popupTitle={t("Select Location")}
                                config={this.props.location}
                                allItems={this.state.coreLocationsByExpansion}
                                itemConfig={LocationConfig}
                                expansions={this.state.sortedExpansions}
                                onChange={this.triggerConfigChanged}
                            />
                        </Zebra>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    removeEndeavors = () => {
        let config = {...this.props.location};
        config.settlementEffects.endeavors = [];
        this.triggerConfigChanged(config);
    }

    changeEndeavors = (type, val) => {
        let config = {...this.props.location};
        config.settlementEffects.endeavors = val;
        this.triggerConfigChanged(config);
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    onTitleChange = (title) => {
        let config = {...this.props.location};
        config.title = title;
        this.triggerConfigChanged(config);
    }

    deleteLocation = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.location.name);
    }

    triggerConfigChanged = (config) => {
        this.props.onConfigChange(config);
    }
}

var localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
    },
});
