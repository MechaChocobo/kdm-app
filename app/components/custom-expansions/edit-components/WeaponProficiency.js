import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import FadeInWrapper from '../../shared/fade-in-wrapper';
import Popup from '../../shared/popup';
import SelectOneList from '../../shared/select-one-list';
import WeaponProfRank from './WeaponProfRank';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import AppTextInput from '../../shared/AppTextInput';
import ProfConfig from '../../../config/weapon-profs.json';
import ReplaceInput from '../shared/ReplaceInput';
import { getItemsByExpansion, getSortedExpansions } from '../../../helpers/helpers';

const BASE_HEIGHT = 102;
export default class WeaponProficiency extends Component {
    wrapper;
    titleInput;

    constructor(props) {
        super(props);

        const profsByExpansion = getItemsByExpansion(ProfConfig);
        this.state = {
            showRankPopup: false,
            availableRanks: this.filterAvailableRanks(this.props.config.ranks),
            coreProfsByExpansion: profsByExpansion,
            sortedExpansions: getSortedExpansions(Object.keys(profsByExpansion)),
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={styles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true}>
                            <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                                <TouchableOpacity onPress={this.delete}>
                                    <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <SubHeader
                                    title={t("Title")}
                                    onPress={this.titleInputFocus}
                                />
                                <AppTextInput
                                    textRef={(ref) => this.titleInput = ref}
                                    inputStyle={styles.input}
                                    placeholder={t("Enter Title")}
                                    value={this.props.config.title}
                                    onChangeText={this.onTitleChange}
                                />
                                <SubHeader
                                    title={t("Proficiency Ranks")}
                                    onPress={this.showRankPopup}
                                    icon="plus-circle-outline"
                                />
                                {this.renderRanks()}
                                <ReplaceInput
                                    title={t("Replaces Weapon Proficiency")}
                                    popupTitle={t("Select Weapon Proficiency")}
                                    config={this.props.config}
                                    allItems={this.state.coreProfsByExpansion}
                                    itemConfig={ProfConfig}
                                    expansions={this.state.sortedExpansions}
                                    onChange={this.triggerConfigChanged}
                                />
                                <Popup
                                    visible={this.state.showRankPopup}
                                    title={t("Select Rank")}
                                    onDismissed={this.onRankPopupClosed}
                                >
                                    <SelectOneList items={this.state.availableRanks} onChange={this.addRank}/>
                                </Popup>
                            </View>
                        </Zebra>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    renderRanks = () => {
        let ranks = [];

        for(let rank in this.props.config.ranks) {
            ranks.push(
                <WeaponProfRank
                    key={rank}
                    rank={rank}
                    config={this.props.config.ranks[rank]}
                    onChange={this.onRankChange}
                    onDelete={this.onRankDelete}
                    allAbilites={this.props.abilities}
                    allInnovations={this.props.innovations}
                />
            );
        }

        return ranks;
    }

    onRankDelete = (rank) => {
        let config = {...this.props.config};
        delete config.ranks[rank];
        this.triggerConfigChanged(config);
        this.setState({
            availableRanks: this.filterAvailableRanks(config),
        });
    }

    onRankChange = (rank, rankConfig) => {
        let config = {...this.props.config};
        config.ranks[rank] = rankConfig;
        this.triggerConfigChanged(config);
    }

    showRankPopup = () => {
        this.setState({showRankPopup: true});
    }

    onRankPopupClosed = () => {
        this.setState({showRankPopup: false});
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    onTitleChange = (title) => {
        let config = {...this.props.config};
        config.title = title;
        this.triggerConfigChanged(config);
    }

    addRank = (rank) => {
        let config = {...this.props.config};
        config.ranks[rank] = {
            title: "",
            abilities: [],
            innovations: [],
        }
        this.triggerConfigChanged(config);
        this.setState({showRankPopup: false, availableRanks: this.filterAvailableRanks(config.ranks)});
    }

    filterAvailableRanks = (config) => {
        let ranks = [
            {name: 1, title: "1"},
            {name: 2, title: "2"},
            {name: 3, title: "3"},
            {name: 4, title: "4"},
            {name: 5, title: "5"},
            {name: 6, title: "6"},
            {name: 7, title: "7"},
            {name: 8, title: "8"},
        ];

        if(Object.keys(config).length > 0) {
            ranks = ranks.filter((rank) => {
                if(config[rank.name]) {
                    return false;
                }

                return true;
            });
        }

        return ranks;
    }

    delete = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.id);
    }

    triggerConfigChanged = (config) => {
        this.props.onConfigChange(this.props.id, config);
    }
}

var styles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
    container: {
        marginBottom: 20
    }
});
