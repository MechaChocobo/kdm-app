import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { t } from '../../../helpers/intl';
import SettlementEffect from './settlement-effect';
import SubHeader from '../../shared/sub-header';
import FadeInWrapper from '../../shared/fade-in-wrapper';
import TitleAndDescription from '../../shared/TitleAndDescription';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import ReplaceInput from '../shared/ReplaceInput';
import EventConfig from '../../../config/settlement-events.json';
import { getItemsByExpansion, getSortedExpansions } from '../../../helpers/helpers';
import Checkbox from '../../shared/Checkbox';
import AppText from '../../shared/app-text';
import styles from '../../../style/styles';

const BASE_HEIGHT = 157;
export default class SettlementEvent extends Component {
    wrapper;
    titleInput;

    constructor(props) {
        super(props);

        const eventsByExpansion = getItemsByExpansion(EventConfig);
        this.state = {
            coreEventsByExpansion: eventsByExpansion,
            sortedExpansions: getSortedExpansions(Object.keys(eventsByExpansion)),
        };
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                <FadeInWrapper
                    ref={(ref) => this.wrapper = ref}
                    styles={localStyles.container}
                    skipRenderAnimation={this.props.skipRenderAnimation}
                    baseHeight={BASE_HEIGHT}
                >
                    <Zebra zebra={true}>
                        <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                            <TouchableOpacity onPress={this.deleteEvent}>
                                <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                            </TouchableOpacity>
                        </View>
                        <TitleAndDescription
                            title={this.props.config.title}
                            description={this.props.config.description}
                            onTitleChange={this.onTitleChange}
                            onDescriptionChange={this.onDescriptionChange}
                        />
                        <SubHeader
                            title={t("Endeavors")}
                        />
                        <SettlementEffect
                            effect="endeavors"
                            value={this.props.config.settlementEffects.endeavors}
                            onRemove={this.removeEndeavors}
                            onChange={this.changeEndeavors}
                        />
                        <SubHeader title={t("Add to Deck By Default")} />
                        <TouchableOpacity onPress={this.toggleAddByDefault}>
                            <View style={styles.rowCentered}>
                                <Checkbox checked={this.props.config.skipAdd !== true} />
                                <AppText style={styles.itemText}> {t("Add to deck by default")}</AppText>
                            </View>
                        </TouchableOpacity>
                        <ReplaceInput
                            title={t("Replaces Settlement Event")}
                            popupTitle={t("Select Settlement Event")}
                            config={this.props.config}
                            allItems={this.state.coreEventsByExpansion}
                            itemConfig={EventConfig}
                            expansions={this.state.sortedExpansions}
                            onChange={this.triggerConfigChanged}
                        />
                    </Zebra>
                </FadeInWrapper>}
            </ColorContext.Consumer>
        )
    }

    toggleAddByDefault = () => {
        let config = {...this.props.config};
        if(config.skipAdd === true) {
            delete config.skipAdd;
        } else {
            config.skipAdd = true;
        }

        this.triggerConfigChanged(config);
    }

    removeEndeavors = () => {
        let config = {...this.props.config};
        config.settlementEffects.endeavors = [];
        this.triggerConfigChanged(config);
    }

    changeEndeavors = (type, val) => {
        let config = {...this.props.config};
        config.settlementEffects.endeavors = val;
        this.triggerConfigChanged(config);
    }

    onTitleChange = (title) => {
        let config = {...this.props.config};
        config.title = title;
        this.triggerConfigChanged(config);
    }

    onDescriptionChange = (text) => {
        let config = {...this.props.config};
        config.description = text;
        this.triggerConfigChanged(config);
    }

    deleteEvent = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.config.name);
    }

    triggerConfigChanged = (config) => {
        this.props.onConfigChange(this.props.config.name, config);
    }
}

var localStyles = StyleSheet.create({
    container: {
        marginBottom: 20
    }
});
