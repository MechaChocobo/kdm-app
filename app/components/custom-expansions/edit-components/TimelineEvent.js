import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FadeInWrapper from '../../shared/fade-in-wrapper';
import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import AppText from '../../shared/app-text';
import styles from '../../../style/styles';
import Popup from '../../shared/popup';
import SelectOneList from '../../shared/select-one-list';
import TimelineEventEdit from './TimelineEventEdit';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';

const BASE_HEIGHT = 65;

const eventTypes = [
    {name: "settlementEvents", title: "Settlement Event"},
    {name: "showdowns", title: "Showdown"},
    {name: "nemesisEncounters", title: "Nemesis Encounter"},
    {name: "specialShowdowns", title: "Special Showdown"},
    {name: "storyEvents", title: "Story Event"},
];

export default class TimelineEvent extends Component {
    wrapper;

    constructor(props) {
        super(props);

        this.state = {
            showAddEventPopup: false,
            availableEventTypes: this.calculateAvailableTypes(this.props.events),
            settlementEvents: this.props.settlementEvents.map(event => {return {name: event, title: this.props.settlementEventConfig[event].title}}),
            storyEvents: this.props.storyEvents,
            showdowns: this.props.showdowns,
            nemesisEncounters: this.props.nemesisEncounters,
            specialShowdowns: this.props.specialShowdowns,
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={localStyles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true}>
                            <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                                <TouchableOpacity onPress={this.onDelete}>
                                    <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                                </TouchableOpacity>
                            </View>

                            <View style={{flexDirection: 'row', marginTop: 10}}>
                                <TouchableOpacity onPress={this.showAddEventPopup}>
                                    <View style={styles.rowCentered}>
                                        <Icon name="plus-circle-outline" color={colors.HIGHLIGHT} size={18}/>
                                        <AppText style={styles.textHeavy}>{t('Add Event')}</AppText>
                                    </View>
                                </TouchableOpacity>
                                <View/>
                            </View>

                            <Popup
                                visible={this.state.showAddEventPopup}
                                title={t("Select Type")}
                                onDismissed={this.onAddEventPopupClosed}
                            >
                                <SelectOneList items={this.state.availableEventTypes} onChange={this.addEvent}/>
                            </Popup>

                            <SubHeader
                                title={t('Lantern Year')}
                            />
                            <AppText style={styles.itemText}>{this.props.ly}</AppText>

                            {this.props.events.settlementEvents && <TimelineEventEdit
                                title={t('Settlement Events')}
                                selected={this.props.events.settlementEvents}
                                options={this.state.settlementEvents}
                                onChange={this.onEventsChange}
                                config={this.props.settlementEventConfig}
                                type="settlementEvents"
                            />}

                            {this.props.events.storyEvents && <TimelineEventEdit
                                title={t('Story Events')}
                                selected={this.props.events.storyEvents}
                                options={Object.values(this.state.storyEvents)}
                                onChange={this.onEventsChange}
                                config={this.state.storyEvents}
                                type="storyEvents"
                                />}

                            {this.props.events.showdowns && <TimelineEventEdit
                                title={t('Showdowns')}
                                selected={this.props.events.showdowns}
                                options={Object.values(this.state.showdowns)}
                                onChange={this.onEventsChange}
                                config={this.state.showdowns}
                                type="showdowns"
                            />}

                            {this.props.events.nemesisEncounters && <TimelineEventEdit
                                title={t('Nemesis Encounters')}
                                selected={this.props.events.nemesisEncounters}
                                options={Object.values(this.state.nemesisEncounters)}
                                onChange={this.onEventsChange}
                                config={this.state.nemesisEncounters}
                                type="nemesisEncounters"
                            />}

                            {this.props.events.specialShowdowns && <TimelineEventEdit
                                title={t('Special Showdowns')}
                                selected={this.props.events.specialShowdowns}
                                options={Object.values(this.state.specialShowdowns)}
                                onChange={this.onEventsChange}
                                config={this.state.specialShowdowns}
                                type="specialShowdowns"
                            />}
                        </Zebra>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    onEventsChange = (type, events) => {
        let config = {...this.props.events};
        if(events.length === 0) {
            delete config[type];
            this.setState({availableEventTypes: this.calculateAvailableTypes(config)});
        } else {
            config[type] = events;
        }

        this.onChange(config);
    }

    showAddEventPopup = () => {
        if(this.state.availableEventTypes.length > 0) {
            this.setState({showAddEventPopup: true});
        }
    }

    onAddEventPopupClosed = () => {
        this.setState({showAddEventPopup: false});
    }

    addEvent = (type) => {
        let config = {...this.props.events};
        config[type] = [];
        this.onChange(config);
        this.setState({showAddEventPopup: false, availableEventTypes: this.calculateAvailableTypes(config)});
    }

    onDelete = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.ly);
    }

    onChange = (config) => {
        this.props.onChange(this.props.ly, config);
    }

    calculateAvailableTypes = (config) => {
        let availableEventTypes = [];

        for(let i = 0; i < eventTypes.length; i++) {
            if(!config[eventTypes[i].name]) {
                availableEventTypes.push(eventTypes[i]);
            }
        }

        return availableEventTypes;
    }
}

TimelineEvent.propTypes = {
    ly: PropTypes.string.isRequired,
    events: PropTypes.object.isRequired,
    settlementEventConfig: PropTypes.object,
    settlementEvents: PropTypes.array,
    storyEvents: PropTypes.object,
    showdowns: PropTypes.object,
    nemesisEncounters: PropTypes.object,
    specialShowdowns: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    skipRenderAnimation: PropTypes.bool,
}

TimelineEvent.defaultProps = {
    skipRenderAnimation: true,
    settlementEventConfig: {},
    settlementEvents: [],
    storyEvents: [],
    showdowns: [],
    nemesisEncounters: [],
    specialShowdowns: [],
}

var localStyles = StyleSheet.create({
    container: {
        marginBottom: 20
    }
});
