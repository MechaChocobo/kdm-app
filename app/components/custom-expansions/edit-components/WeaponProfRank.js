import React, { Component } from 'react';
import {View, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import PropTypes from 'prop-types';

import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import Popup from '../../shared/popup';
import SelectOneList from '../../shared/select-one-list';
import AppText from '../../shared/app-text';
import styles from '../../../style/styles';
import ColorContext from '../../../context/ColorContext';

export default class WeaponProfRank extends Component {
    wrapper;
    titleInput;

    constructor(props) {
        super(props);

        this.state = {
            availableTypes: this.getAvailableTypes(this.props.config.title),
            showTypePopup: false,
            showAbilityPopup: false,
            showInnovationPopup: false,
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View>
                        <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                            <TouchableOpacity onPress={this.delete}>
                                <Icon name="close" color={colors.SECONDARY} size={25}/>
                            </TouchableOpacity>
                        </View>
                        <SubHeader title={t("Rank %rank%", {rank: this.props.rank})}/>
                        {this.renderDescription(colors)}
                        <View height={5}/>
                        {this.renderAbilities(colors)}
                        <View height={5}/>
                        {this.renderInnovations(colors)}

                        <Popup
                            visible={this.state.showTypePopup}
                            title={t("Select Rank")}
                            onDismissed={this.onTypePopupClosed}
                        >
                            <SelectOneList items={this.state.availableTypes} onChange={this.changeType}/>
                        </Popup>

                        <Popup
                            visible={this.state.showAbilityPopup}
                            title={t("Select Ability")}
                            onDismissed={this.onAbilityPopupClosed}
                        >
                            <SelectOneList items={Object.values(this.props.allAbilites)} onChange={this.addAbility}/>
                        </Popup>

                        <Popup
                            visible={this.state.showInnovationPopup}
                            title={t("Select Innovation")}
                            onDismissed={this.onInnovationPopupClosed}
                        >
                            <SelectOneList items={Object.values(this.props.allInnovations)} onChange={this.addInnovation}/>
                        </Popup>
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    delete = () => {
        this.props.onDelete(this.props.rank);
    }

    showTypePopup = () => {
        this.setState({showTypePopup: true});
    }

    onTypePopupClosed = () => {
        this.setState({showTypePopup: false});
    }

    changeType = (type) => {
        let title = "";

        if(type === "spec") {
            title = "Weapon Specialty";
        } else if(type === "mastery") {
            title = "Weapon Mastery";
        } else {
            title = "";
        }

        let config = {...this.props.config}
        config.title = title;

        this.triggerChange(config);
        this.setState({
            showTypePopup: false,
            availableTypes: this.getAvailableTypes(title)
        });
    }

    triggerChange = (config) => {
        this.props.onChange(this.props.rank, config);
    }

    getAvailableTypes = (title) => {
        let types = [
            {name: "custom", title: t("Custom")},
            {name: "spec", title: t("Weapon Specialty")},
            {name: "mastery", title: t("Weapon Mastery")},
        ];

        if(title === "Weapon Specialty") {
            types.splice(1, 1);
        } else if(title === "Weapon Mastery") {
            types.splice(2, 1);
        } else {
            types.splice(0, 1);
        }

        return types;
    }

    renderDescription = (colors) => {
        let type = <AppText style={styles.itemText}>{t(this.props.config.title)}</AppText>;
        let custom = false;

        if(this.props.config.title !== "Weapon Specialty" && this.props.config.title !== "Weapon Mastery") {
            type = <AppText style={styles.itemText}>{t("Custom")}</AppText>;
            custom = true;
        }

        return <View>
            <View style={styles.rowCentered}>
                <TouchableOpacity onPress={this.showTypePopup}>
                    <View style={styles.rowCentered}>
                        <Icon name="pencil" color={colors.SECONDARY} size={18}/>
                        <AppText style={styles.itemText}>{t("Type")}: </AppText>
                    </View>
                </TouchableOpacity>
                {type}
            </View>
            {custom && <TextInput
                ref={(ref) => this.titleInput = ref}
                style={[localStyles.input, {borderColor: colors.PRIMARYDARK}]}
                placeholder={t("Enter Rank Text")}
                placeholderTextColor="gray"
                value={this.props.config.title}
                onChangeText={this.onTitleChange}
            />}
        </View>
    }

    renderAbilities = (colors) => {
        let abilities = this.props.config.abilities;
        return <View>
            <TouchableOpacity onPress={this.showAbilityPopup}>
                <View style={styles.rowCentered}>
                    <Icon name="plus-circle-outline" color={colors.SECONDARY} size={18}/>
                    <AppText style={styles.textHeavy}> {t("Abilities")}: </AppText>
                </View>
            </TouchableOpacity>
            {abilities.map((ability, i) => {
                return <View key={i}>
                    <View style={styles.rowCentered}>
                        <AppText style={styles.itemText}>{this.props.allAbilites[ability].title}</AppText>
                        <TouchableOpacity onPress={this.removeAbility(ability)}>
                            <Icon name="close" color={colors.SECONDARY} size={18}/>
                        </TouchableOpacity>
                    </View>
                </View>
            })}
        </View>
    }

    addAbility = (ability) => {
        let config = {...this.props.config};

        if(config.abilities.indexOf(ability) !== -1) {
            return;
        }

        config.abilities.push(ability);

        this.triggerChange(config);
        this.onAbilityPopupClosed();
    }

    removeAbility = (ability) => {
        return () => {
            let config = {...this.props.config};
            config.abilities.splice(config.abilities.indexOf(ability), 1);
            this.triggerChange(config);
        }
    }

    showAbilityPopup = () => {
        this.setState({
            showAbilityPopup: true
        });
    }

    onAbilityPopupClosed = () => {
        this.setState({
            showAbilityPopup: false
        });
    }

    renderInnovations = (colors) => {
        if(this.props.config.title !== "Weapon Mastery") {
            return null;
        }

        let innovations = this.props.config.innovations;
        return <View>
            <TouchableOpacity onPress={this.showInnovationPopup}>
                <View style={styles.rowCentered}>
                    <Icon name="plus-circle-outline" color={colors.SECONDARY} size={18}/>
                    <AppText style={styles.textHeavy}> {t("Innovations")}: </AppText>
                </View>
            </TouchableOpacity>
            {innovations.map((innovation, i) => {
                return <View key={i}>
                    <View style={styles.rowCentered}>
                        <AppText style={styles.itemText}>{this.props.allInnovations[innovation].title}</AppText>
                        <TouchableOpacity onPress={this.removeInnovation(innovation)}>
                            <Icon name="close" color={colors.SECONDARY} size={18}/>
                        </TouchableOpacity>
                    </View>
                </View>
            })}
        </View>
    }

    addInnovation = (innovation) => {
        let config = {...this.props.config};

        if(config.innovations.indexOf(innovation) !== -1) {
            return;
        }

        config.innovations.push(innovation);

        this.triggerChange(config);
        this.onInnovationPopupClosed();
    }

    removeInnovation = (innovation) => {
        return () => {
            let config = {...this.props.config};
            config.innovations.splice(config.innovations.indexOf(innovation), 1);
            this.triggerChange(config);
        }
    }

    showInnovationPopup = () => {
        this.setState({
            showInnovationPopup: true
        });
    }

    onInnovationPopupClosed = () => {
        this.setState({
            showInnovationPopup: false
        });
    }

    onTitleChange = (title) => {
        let config = {...this.props.config}
        config.title = title;
        this.triggerChange(config);
    }
}

WeaponProfRank.propTypes = {
    rank: PropTypes.string.isRequired,
    config: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    allAbilites: PropTypes.object,
    allInnovations: PropTypes.object,
}

var localStyles = StyleSheet.create({
    input: {
        color: "white",
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5
    },
    subSubHeader: {
        fontSize: 16,
        fontWeight: "600"
    }
});

