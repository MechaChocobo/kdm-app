'use strict';

import React, { PureComponent } from 'react';

import {
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

import styles from '../../../style/styles';
import StorageItem, { keywords } from './storage-item';
import SubHeader from '../../shared/sub-header';
import Icon from '../../shared/Icon';
import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import Popup from '../../shared/popup';
import SelectOneList from '../../shared/select-one-list';
import AppTextInput from '../../shared/AppTextInput';
import { getBaseResourceConfig, getGearConfig, getLocationConfig, getBaseGearConfig, sortArrayByTitle, sortIdArrayByConfigTitle } from '../../../helpers/helpers';
import { IncrementorHidden } from '../../shared/IncrementorHidden';
import ToggleListItem from '../../shared/ToggleListItem';
import Checkbox from '../../shared/Checkbox';
import AffinitySelector from '../../shared/AffinitySelector';

export default class GearItem extends PureComponent {
    state = {
        showCostTypePopup: false,
        showAddResourceType: false,
        showAddResources: false,
        showAddGear: false,
        showArmorLocationPopup: false,
    }

    render() {
        return <View>
            <StorageItem
                type="gear"
                config={this.props.config}
                onChange={this.props.onChange}
                onDelete={this.props.onDelete}
                skipRenderAnimation={this.props.skipRenderAnimation}
            />
            <SubHeader title={t("Cursed Gear")} />
            <TouchableOpacity onPress={this.onCursedToggle}>
                <View style={styles.rowCentered}>
                    <Checkbox checked={this.props.config.cursed === true} />
                    <AppText style={styles.itemText}> {t("Cursed")}</AppText>
                </View>
            </TouchableOpacity>
            <SubHeader title={t("Cost")} onPress={this.showCostTypePopup} icon="plus-circle-outline"/>
            {this.renderResourceTypes()}
            {this.renderResources()}
            {this.renderGear()}
            <SubHeader title={t("Affinities")} onPress={this.addAffinities} icon="plus-circle-outline">
                <View style={{position: "absolute", right: 0}}>
                    {this.props.config.affinities && <TouchableOpacity onPress={this.deleteAffinities}>
                        <Icon name="close" color={this.props.colors.HIGHLIGHT} size={25}/>
                    </TouchableOpacity>}
                </View>
            </SubHeader>
            {this.renderAffinities()}
            <SubHeader title={t("Armor")} onPress={this.addArmor} icon="plus-circle-outline">
                <View style={{position: "absolute", right: 0}}>
                    {this.props.config.armor && <TouchableOpacity onPress={this.deleteArmor}>
                        <Icon name="close" color={this.props.colors.HIGHLIGHT} size={25}/>
                    </TouchableOpacity>}
                </View>
            </SubHeader>
            {this.renderArmor()}
            <SubHeader title={t("Attack Stats")} onPress={this.addStats} icon="plus-circle-outline">
                <View style={{position: "absolute", right: 0}}>
                    {this.props.config.stats && <TouchableOpacity onPress={this.deleteStats}>
                        <Icon name="close" color={this.props.colors.HIGHLIGHT} size={25}/>
                    </TouchableOpacity>}
                </View>
            </SubHeader>
            {this.renderStats()}
            {this.state.showCostTypePopup && <Popup
                visible={this.state.showCostTypePopup}
                onDismissed={this.hideCostTypePopup}
                title={t("Select cost type")}
            >
                <SelectOneList
                    items={[{name: "resourceTypes", title: t("Resource Type")}, {name: "resources", title: t("Specific Resource")}, {name: "gear", title: t("Gear")}]}
                    onChange={this.onCostTypeAdd}
                />
            </Popup>}
        </View>
    }

    onCursedToggle = () => {
        this.props.onCursedToggle(this.props.config.id);
    }

    deleteAffinities = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        delete newConfig.affinities;
        this.onConfigChanged(newConfig);
    }

    addAffinities = () => {
        if(this.props.config.affinities) {
            return;
        } else {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.affinities = {};
            this.onConfigChanged(newConfig);
        }
    }

    // render out a similar grid as the affinity search
    renderAffinities = () => {
        if(!this.props.config.affinities) {
            return null;
        }

        return <AffinitySelector
            onChange={this.setAffinityColor}
            top={this.props.config.affinities.top}
            bottom={this.props.config.affinities.bottom}
            left={this.props.config.affinities.left}
            right={this.props.config.affinities.right}
            complete={this.props.config.affinities.complete}
        />
    }

    setAffinityColor = (location, color) => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        if(color === "none" || color === null) {
            delete newConfig.affinities[location];
        } else {
            newConfig.affinities[location] = color;
        }
        this.onConfigChanged(newConfig);
    }

    addArmor = () => {
        if(this.props.config.armor) {
            return;
        } else {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.armor = {
                value: 1,
                locations: [],
            };
            this.onConfigChanged(newConfig);
        }
    }

    deleteArmor = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        delete newConfig.armor;
        this.onConfigChanged(newConfig);
    }

    renderArmor = () => {
        if(!this.props.config.armor) {
            return null;
        }

        return <View style={{marginLeft: 15}}>
            <View style={styles.rowCentered}>
                <AppText style={styles.itemText}>{t("Armor amount")}: </AppText>
                <IncrementorHidden
                    value={this.props.config.armor.value}
                    onIncrease={this.onArmorIncrease}
                    onDecrease={this.onArmorDecrease}
                    smallValue={true}
                />
            </View>
            <View style={styles.rowCentered}>
                <TouchableOpacity style={styles.rowCentered} onPress={this.showArmorLocationPopup}>
                    <Icon name="format-list-checkbox" size={18} color={this.props.colors.HIGHLIGHT}/>
                    <AppText style={styles.itemText}>{t("Armor locations")}: </AppText>
                    {this.props.config.armor.locations.length === 0 && <AppText style={styles.italic}>{t("No locations yet, tap to add")}</AppText>}
                </TouchableOpacity>
                {this.renderLocations()}
            </View>
            {this.state.showArmorLocationPopup && <Popup
                title={t("Select location")}
                onDismissed={this.hideArmorLocationPopup}
                visible={this.state.showArmorLocationPopup}
                >
                {this.getArmorLocationPicker()}
            </Popup>}
        </View>
    }

    addStats = () => {
        if(this.props.config.stats) {
            return;
        } else {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.stats = {
                speed: 2,
                accuracy: 6,
                strength: 2,
            };
            this.onConfigChanged(newConfig);
        }
    }

    deleteStats = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        delete newConfig.stats;
        this.onConfigChanged(newConfig);
    }

    renderStats = () => {
        if(!this.props.config.stats) {
            return null;
        }

        return <View style={{marginLeft: 5}}>
            <View style={styles.rowCentered}>
                <AppText style={styles.itemText}>{t("Speed")}: </AppText>
                <IncrementorHidden
                    value={this.props.config.stats.speed}
                    onIncrease={this.onStatSpeedIncrease}
                    onDecrease={this.onStatSpeedDecrease}
                    smallValue={true}
                />
            </View>
            <View style={styles.rowCentered}>
                <AppText style={styles.itemText}>{t("Accuracy")}: </AppText>
                <IncrementorHidden
                    value={this.props.config.stats.accuracy}
                    onIncrease={this.onStatAccuracyIncrease}
                    onDecrease={this.onStatAccuracyDecrease}
                    smallValue={true}
                />
            </View>
            <View style={styles.rowCentered}>
                <AppText style={styles.itemText}>{t("Strength")}: </AppText>
                <IncrementorHidden
                    value={this.props.config.stats.strength}
                    onIncrease={this.onStatStrengthIncrease}
                    onDecrease={this.onStatStrengthDecrease}
                    smallValue={true}
                    />
            </View>
        </View>
    }

    onStatSpeedIncrease = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.stats.speed += 1;
        this.onConfigChanged(newConfig);
    }

    onStatSpeedDecrease = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.stats.speed -= 1;
        this.onConfigChanged(newConfig);
    }

    onStatAccuracyIncrease = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.stats.accuracy += 1;
        this.onConfigChanged(newConfig);
    }

    onStatAccuracyDecrease = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.stats.accuracy -= 1;
        this.onConfigChanged(newConfig);
    }

    onStatStrengthIncrease = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.stats.strength += 1;
        this.onConfigChanged(newConfig);
    }

    onStatStrengthDecrease = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.stats.strength -= 1;
        this.onConfigChanged(newConfig);
    }

    renderLocations = () => {
        return this.props.config.armor.locations.map((location, i) => {
            if(location === "all") {
                return <AppText key={i} style={styles.itemText}>{t("All")}</AppText>
            } else {
                return <Icon key={i} name={"sk-"+ location} size={20} color={this.props.colors.TEXT} />
            }
        });
    }

    showArmorLocationPopup = () => {
        this.setState({showArmorLocationPopup: true});
    }

    hideArmorLocationPopup = () => {
        this.setState({showArmorLocationPopup: false});
    }

    getArmorLocationPicker = () => {
        const titleMap = {"all": t("All"), "head": t("Head"), "arms": t("Arms"), "body": t("Body"), "waist": t("Waist"), "legs": t("Legs"),}
        let options = ["all", "head", "arms", "body", "waist", "legs"];

        return options.map((location, i) => {
            return <ToggleListItem
                key={i}
                id={location}
                title={titleMap[location]}
                value={this.props.config.armor.locations.indexOf(location) !== -1}
                onToggle={this.toggleArmorLocation}
            />
        });
    }

    toggleArmorLocation = (location) => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));

        if(newConfig.armor.locations.indexOf(location) === -1) {
            newConfig.armor.locations.push(location);
        } else {
            newConfig.armor.locations.splice(newConfig.armor.locations.indexOf(location), 1);
        }

        this.onConfigChanged(newConfig);
    }

    onArmorIncrease = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.armor.value += 1;
        this.onConfigChanged(newConfig);
    }

    onArmorDecrease = () => {
        if(this.props.config.armor.value < 1) {
            return;
        }

        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.armor.value -= 1;
        this.onConfigChanged(newConfig);
    }

    renderResourceTypes = () => {
        if(!this.props.config.cost || !this.props.config.cost.resourceTypes) {
            return null;
        }

        return <View>
            <View style={{flexDirection: "row", alignItems: "center"}}>
                <TouchableOpacity onPress={this.removeResourceTypes} style={{paddingRight: 5}}><Icon name="close" color={this.props.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                <AppText style={styles.itemText}>{t("Resource Types")}: </AppText>
                <TouchableOpacity onPress={this.showAddResourceType}><Icon name="plus-circle-outline" color={this.props.colors.HIGHLIGHT} size={18}/></TouchableOpacity>
            </View>
            {Object.keys(this.props.config.cost.resourceTypes).map((type, i) => {
                return <View key={i} style={[styles.rowCentered, localStyles.indentedText]}>
                    <TouchableOpacity onPress={this.removeResourceType(type)} style={{paddingRight: 5}}><Icon name="close" color={this.props.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                    <AppText style={[styles.itemText, localStyles.inputMargin]}>{type}</AppText>
                    <AppTextInput
                        value={""+this.props.config.cost.resourceTypes[type]}
                        onChangeText={this.onResourceTypeValueChanged(type)}
                        keyboardType="numeric"
                        returnKeyType="done"
                        textAlign="center"
                        inputStyle={{fontSize: 20, fontWeight: "300"}}
                    />
                </View>
            })}
            {this.state.showAddResourceType && <Popup
                visible={this.state.showAddResourceType}
                onDismissed={this.hideAddResourceType}
                title={t("Select resource type")}
            >
                <SelectOneList
                    items={this.getResourceTypes()}
                    onChange={this.onResourceTypeAdd}
                />
            </Popup>}
        </View>
    }

    onResourceTypeValueChanged = (type) => {
        return (num) => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.cost.resourceTypes[type] = num;
            this.onConfigChanged(newConfig);
        }
    }

    getResourceTypes = () => {
        let types = [...keywords];

        // remove the ones that we already have
        for(let keyword in this.props.config.cost.resourceTypes) {
            for(let i = 0; i < types.length; i++) {
                if(types[i].name === keyword) {
                    types.splice(i, 1);
                    break;
                }
            }
        }

        types.sort((a, b) => {
            return a.title > b.title ? 1 : -1;
        });

        return types;
    }

    onResourceTypeAdd = (type) => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        newConfig.cost.resourceTypes[type] = 1;
        this.onConfigChanged(newConfig);
        this.setState({showAddResourceType: false});
    }

    removeResourceType = (type) => {
        return () => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            delete newConfig.cost.resourceTypes[type];
            this.onConfigChanged(newConfig);
        }
    }

    removeResourceTypes = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));

        delete newConfig.cost.resourceTypes;

        if(Object.keys(newConfig.cost) === 0) {
            delete newConfig.cost;
        }

        this.onConfigChanged(newConfig);
    }

    showAddResourceType = () => {
        this.setState({showAddResourceType: true});
    }

    hideAddResourceType = () => {
        this.setState({showAddResourceType: false});
    }

    renderResources = () => {
        if(!this.props.config.cost || !this.props.config.cost.resources) {
            return null;
        }

        const ResourceConfig = this.getResourceConfig();
        let baseResources = this.getAvailableResources();

        return <View>
            <View style={{flexDirection: "row", alignItems: "center"}}>
                <TouchableOpacity onPress={this.removeResources} style={{paddingRight: 5}}><Icon name="close" color={this.props.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                <AppText style={styles.itemText}>{t("Specific Resources")}: </AppText>
                <TouchableOpacity onPress={this.showAddResources}><Icon name="plus-circle-outline" color={this.props.colors.HIGHLIGHT} size={18}/></TouchableOpacity>
            </View>
            {this.props.config.cost.resources.map((resource, i) => {
                return <View key={i} style={[styles.rowCentered, localStyles.indentedText]}>
                    <TouchableOpacity onPress={this.removeResource(i)} style={{paddingRight: 5}}><Icon name="close" color={this.props.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                    <AppText style={[styles.itemText, localStyles.inputMargin]}>{ResourceConfig[resource.location][resource.name].title}</AppText>
                    <AppTextInput
                        value={""+this.props.config.cost.resources[i].qty}
                        onChangeText={this.onResourceValueChanged(i)}
                        keyboardType="numeric"
                        returnKeyType="done"
                        textAlign="center"
                        inputStyle={{fontSize: 20, fontWeight: "300"}}
                    />
                </View>
            })}
            {this.state.showAddResources && <Popup
                visible={this.state.showAddResources}
                onDismissed={this.hideAddResources}
                title={t("Select resource")}
            >
                {Object.keys(baseResources).map((location) => {
                    return <View key={location}>
                        <AppText style={styles.title}>{location}</AppText>
                        <SelectOneList
                            items={this.getStorageItemList(baseResources[location])}
                            onChange={this.onResourceAdd(location)}
                        />
                    </View>
                })}
            </Popup>}
        </View>
    }

    getStorageItemList = (items) => {
        let list = [];

        for(let name in items) {
            let obj = items[name];
            obj.name = name;
            list.push(obj);
        }

        return list;
    }

    getResourceConfig = () => {
        let config = JSON.parse(JSON.stringify(getBaseResourceConfig()));

        for(let location in this.props.resources) {
            const resources = JSON.parse(JSON.stringify(this.props.resources[location]));
            if(!config[location]) {
                config[location] = resources;
            } else {
                config[location] = {...config[location], ...resources};
                const unsorted = JSON.parse(JSON.stringify(config[location]));

                const keys = sortIdArrayByConfigTitle(Object.keys(unsorted), config[location]);
                config[location] = {};
                for(let i = 0; i < keys.length; i++) {
                    config[location][keys[i]] = unsorted[keys[i]];
                }
            }
        }

        return config;
    }

    getAvailableResources = () => {
        let configs = this.getResourceConfig();

        for(let i = 0; i < this.props.config.cost.resources.length; i++) {
            delete configs[this.props.config.cost.resources[i].location][this.props.config.cost.resources[i].name];
        }

        return configs;
    }

    removeResources = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));

        delete newConfig.cost.resources;

        if(Object.keys(newConfig.cost) === 0) {
            delete newConfig.cost;
        }

        this.onConfigChanged(newConfig);
    }

    removeResource = (index) => {
        return () => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.cost.resources.splice(index, 1);
            this.onConfigChanged(newConfig);
        }
    }

    onResourceAdd = (location) => {
        return (name) => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.cost.resources.push({location, name, qty: 1});
            this.onConfigChanged(newConfig);
            this.setState({showAddResources: false});
        }
    }

    onResourceValueChanged = (index) => {
        return (val) => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            let resource = {...newConfig.cost.resources[index]};
            resource.qty = val;
            newConfig.cost.resources[index] = resource;
            this.onConfigChanged(newConfig);
        }
    }

    showAddResources = () => {
        this.setState({showAddResources: true});
    }

    hideAddResources = () => {
        this.setState({showAddResources: false});
    }

    renderGear = () => {
        if(!this.props.config.cost || !this.props.config.cost.gear) {
            return null;
        }

        let baseGear = this.getAvailableGear();
        const GearConfig = getGearConfig();
        const LocationConfig = getLocationConfig();

        return <View>
            <View style={{flexDirection: "row", alignItems: "center"}}>
                <TouchableOpacity onPress={this.removeGear} style={{paddingRight: 5}}><Icon name="close" color={this.props.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                <AppText style={styles.itemText}>{t("Specific Gear")}: </AppText>
                <TouchableOpacity onPress={this.showAddGear}><Icon name="plus-circle-outline" color={this.props.colors.HIGHLIGHT} size={18}/></TouchableOpacity>
            </View>
            {this.props.config.cost.gear.map((gear, i) => {
                return <View key={i} style={[styles.rowCentered, localStyles.indentedText]}>
                    <TouchableOpacity onPress={this.removeGearItem(i)} style={{paddingRight: 5}}><Icon name="close" color={this.props.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                    <AppText style={[styles.itemText, localStyles.inputMargin]}>{GearConfig[gear.location][gear.name].title}</AppText>
                    <AppTextInput
                        value={""+this.props.config.cost.gear[i].qty}
                        onChangeText={this.onGearValueChanged(i)}
                        keyboardType="numeric"
                        returnKeyType="done"
                        textAlign="center"
                        inputStyle={{fontSize: 20, fontWeight: "300"}}
                    />
                </View>
            })}
            {this.state.showAddGear && <Popup
                visible={this.state.showAddGear}
                onDismissed={this.hideAddGear}
                title={t("Select gear")}
            >
                {Object.keys(baseGear).sort(sortArrayByTitle(LocationConfig)).map((location) => {
                    return <View key={location}>
                        <AppText style={styles.title}>{LocationConfig[location] ? LocationConfig[location].title : location}</AppText>
                        <SelectOneList
                            items={this.getStorageItemList(baseGear[location])}
                            onChange={this.onGearAdd(location)}
                        />
                    </View>
                })}
            </Popup>}
        </View>
    }

    getAvailableGear = () => {
        let configs = JSON.parse(JSON.stringify(getBaseGearConfig()));

        for(let i = 0; i < this.props.config.cost.gear.length; i++) {
            delete configs[this.props.config.cost.gear[i].location][this.props.config.cost.gear[i].name];
        }

        return configs;
    }

    removeGear = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));

        delete newConfig.cost.gear;

        if(Object.keys(newConfig.cost) === 0) {
            delete newConfig.cost;
        }

        this.onConfigChanged(newConfig);
    }

    removeGearItem = (index) => {
        return () => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.cost.gear.splice(index, 1);
            this.onConfigChanged(newConfig);
        }
    }

    onGearAdd = (location) => {
        return (name) => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            newConfig.cost.gear.push({location, name, qty: 1});
            this.onConfigChanged(newConfig);
            this.setState({showAddGear: false});
        }
    }

    onGearValueChanged = (index) => {
        return (val) => {
            let newConfig = JSON.parse(JSON.stringify(this.props.config));
            let item = {...newConfig.cost.gear[index]};
            item.qty = val;
            newConfig.cost.gear[index] = item;
            this.onConfigChanged(newConfig);
        }
    }

    showAddGear = () => {
        this.setState({showAddGear: true});
    }

    hideAddGear = () => {
        this.setState({showAddGear: false});
    }

    hideCostTypePopup = () => {
        this.setState({showCostTypePopup: false});
    }

    showCostTypePopup = () => {
        this.setState({showCostTypePopup: true});
    }

    onCostTypeAdd = (type) => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        if(!newConfig.cost) {
            newConfig.cost = {};
        }

        if(type === "resourceTypes") {
            newConfig.cost[type] = {};
        } else {
            newConfig.cost[type] = [];
        }

        this.onConfigChanged(newConfig);
        this.setState({showCostTypePopup: false});
    }

    onConfigChanged = (newConfig) => {
        this.props.onChange(newConfig);
    }
}

const localStyles = StyleSheet.create({
    indentedText: {
        marginLeft: 20
    },
    inputMargin: {
        marginRight: 15,
    },
});
