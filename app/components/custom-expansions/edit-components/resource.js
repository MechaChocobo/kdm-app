import React, { Component } from 'react';
import {View, StyleSheet,  TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FadeInWrapper from '../../shared/fade-in-wrapper';
import AppText from '../../shared/app-text';
import StorageItem from './storage-item';
import styles from '../../../style/styles';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import Hr from '../../shared/Hr';

const BASE_HEIGHT = 157;
export default class Resource extends Component {
    wrapper;

    constructor(props) {
        super(props);

        let renderedResources = {};
        let resources = Object.keys(this.props.resources);
        for(let i = 0; i < resources.length; i++) {
            renderedResources[resources[i]] = true;
        }

        this.state = {
            renderedResources,
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={localStyles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true} >
                            <View style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                                <TouchableOpacity style={{flexDirection: "row", alignItems: "center"}} onPress={this.addResource}>
                                    <Icon name="plus-circle-outline" size={18} color={colors.HIGHLIGHT}/>
                                    <AppText style={styles.title}>{this.props.source}</AppText>
                                    <View />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.onSourceDelete}>
                                    <Icon name="close" color={colors.HIGHLIGHT} size={24} />
                                </TouchableOpacity>
                            </View>
                            {this.renderResources()}
                        </Zebra>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    renderResources = () => {
        return Object.values(this.props.resources).map((resource, i) => {
            return <View key={resource.id}>
                <StorageItem
                    type="resource"
                    config={resource}
                    onChange={this.onResourceChange}
                    onDelete={this.onResourceDelete}
                    skipRenderAnimation={this.state.renderedResources[resource.id]}
                />
                {i < Object.values(this.props.resources).length - 1 && <Hr />}
            </View>
        });
    }

    addResource = () => {
        this.props.onAdd(this.props.source);
    }

    onResourceChange = (config) => {
        this.props.onChange(this.props.source, config.id, config);
    }

    onResourceDelete = (id) => {
        this.props.onDelete(this.props.source, id);
    }

    onSourceDelete = () => {
        this.wrapper.remove(this.triggerSourceDelete);
    }

    triggerSourceDelete = () => {
        this.props.onSourceDelete(this.props.source);
    }

    triggerConfigChange = (config) => {
        this.props.onChange(this.props.source, config);
    }
}

Resource.propTypes = {
    source: PropTypes.string.isRequired,
    resources: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onAdd: PropTypes.func.isRequired,
    onSourceDelete: PropTypes.func.isRequired,
}

var localStyles = StyleSheet.create({
    container: {
        marginBottom: 20
    }
});
