import React, { Component } from 'react';
import {View, StyleSheet, Keyboard, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import FadeInWrapper from '../../shared/fade-in-wrapper';
import SurvivorStatEdit from './SurvivorStatEdit';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import AppTextInput from '../../shared/AppTextInput';
import styles from '../../../style/styles';
import { getItemsByExpansion, getSortedExpansions } from '../../../helpers/helpers';
import ReplaceInput from '../shared/ReplaceInput';

const BASE_HEIGHT = 157;
export default class SurvivorAddition extends Component {
    wrapper;
    titleInput;
    descriptionInput;

    constructor(props) {
        super(props);

        this.state = {
            showAddStatChange: false,
        }

        if(this.props.baseConfig) {
            const itemsByExpansion = getItemsByExpansion(this.props.baseConfig);
            this.state.coreItemsByExpansion = itemsByExpansion;
            this.state.sortedExpansions = getSortedExpansions(Object.keys(itemsByExpansion));
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                <FadeInWrapper
                    ref={(ref) => this.wrapper = ref}
                    styles={localStyles.container}
                    skipRenderAnimation={this.props.skipRenderAnimation}
                    baseHeight={BASE_HEIGHT}
                >
                    <Zebra zebra={true}>
                        <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                            <TouchableOpacity onPress={this.deleteItem}>
                                <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                            </TouchableOpacity>
                        </View>
                        <SubHeader
                            title={t("Title")}
                            onPress={this.titleInputFocus}
                        />
                        <AppTextInput
                            textRef={(ref) => this.titleInput = ref}
                            inputStyle={localStyles.input}
                            placeholder={t("Enter Title")}
                            value={this.props.config.title}
                            onChangeText={this.onTitleChange}
                        />

                        <SubHeader
                            title={t("Description")}
                            onPress={this.descriptionInputFocus}
                        />
                        <AppTextInput
                            textRef={(ref) => this.descriptionInput = ref}
                            inputStyle={[localStyles.input, styles.icomoon]}
                            placeholder={t("Enter Description")}
                            value={this.props.config.description}
                            onChangeText={this.onDescriptionChange}
                            multiline={true}
                            returnKeyType="default"
                            onBlur={Keyboard.dismiss}
                        />

                        <SubHeader
                            title={t("Survivor Effects")}
                            onPress={this.showAddStatChange}
                            icon={"plus-circle-outline"}
                        />
                        <SurvivorStatEdit
                            value={this.props.config.survivorEffects || {}}
                            showAddStatChange={this.state.showAddStatChange}
                            onAddStatChangeDismissed={this.onAddStatChangeDismissed}
                            onChange={this.changeSurvivorEffect}
                            onStatChange={this.changeSurvivorEffect}
                        />
                        {this.props.baseConfig && <ReplaceInput
                            title={this.props.replaceTitle}
                            popupTitle={this.props.replacePopupTitle}
                            config={this.props.config}
                            allItems={this.state.coreItemsByExpansion}
                            itemConfig={this.props.baseConfig}
                            expansions={this.state.sortedExpansions}
                            onChange={this.triggerConfigChange}
                        />}
                        {this.props.children}
                    </Zebra>
                </FadeInWrapper>}
            </ColorContext.Consumer>
        )
    }

    showAddStatChange = () => {
        this.setState({showAddStatChange: true});
    }

    onAddStatChangeDismissed = () => {
        this.setState({showAddStatChange: false});
    }

    changeSurvivorEffect = (val) => {
        let config = {...this.props.config};
        config.survivorEffects = val;
        this.triggerConfigChange(config);
    }

    deleteItem = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.id);
    }

    onTitleChange = (text) => {
        let config = {...this.props.config};
        config.title = text;
        this.triggerConfigChange(config);
    }

    onDescriptionChange = (text) => {
        let config = {...this.props.config};
        config.description = text;
        this.triggerConfigChange(config);
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    descriptionInputFocus = () => {
        this.descriptionInput.focus();
    }

    triggerConfigChange = (config) => {
        this.props.onConfigChange(this.props.id, config);
    }
}

SurvivorAddition.propTypes = {
    id: PropTypes.string.isRequired,
    onConfigChange: PropTypes.func.isRequired,
    config: PropTypes.object.isRequired,
    onDelete: PropTypes.func.isRequired,
    skipRenderAnimation: PropTypes.bool
}

SurvivorAddition.defaultProps = {
    skipRenderAnimation: false,
}

var localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
    container: {
        marginBottom: 20
    }
});
