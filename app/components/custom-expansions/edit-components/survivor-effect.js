import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {View, TouchableOpacity} from 'react-native';
import Text from '../../shared/app-text';
import styles from '../../../style/styles';
import { t } from '../../../helpers/intl';
import SurvivorStatEdit from './SurvivorStatEdit';
import ColorContext from '../../../context/ColorContext';
import Icon from '../../shared/Icon';

export default class SurvivorEffect extends Component {
    constructor(props) {
        super(props);
        this.state = {value: this.props.value, showAddStatChange: false}
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View>
                        <View style={{flexDirection: "row", alignItems: "center"}}>
                            <TouchableOpacity onPress={this.removeEffect} style={{paddingRight: 5}}><Icon name="close" color={colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                            <Text style={styles.itemText}>{this.getTitle()}: </Text>
                            <TouchableOpacity onPress={this.showAddStatChange}><Icon name="plus-circle-outline" color={colors.HIGHLIGHT} size={18}/></TouchableOpacity>
                        </View>
                        <SurvivorStatEdit
                            value={this.props.value}
                            showAddStatChange={this.state.showAddStatChange}
                            onAddStatChangeDismissed={this.onAddStatChangeDismissed}
                            onChange={this.triggerChange}
                            onStatChange={this.triggerChange}
                        />
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    showAddStatChange = () => {
        this.setState({showAddStatChange: true});
    }

    onAddStatChangeDismissed = () => {
        this.setState({showAddStatChange: false});
    }

    getTitle = () => {
        if(this.props.title) {
            return this.props.title;
        }

        if(this.props.effect === "allSurvivors") {
            return t("All Current Survivors");
        } else if(this.props.effect === "allNewSurvivors") {
            return t("All New Survivors");
        } else if(this.props.effect === "newBornEffects") {
            return t("All Newborn Survivors");
        } else {
            return "";
        }
    }

    removeEffect = () => {
        this.props.onRemove(this.props.effect);
    }

    triggerChange = (config) => {
        this.props.onChange(this.props.effect, config);
    }
}

SurvivorEffect.propTypes = {
    effect: PropTypes.string,
    value: PropTypes.any,
    onRemove: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
    title: PropTypes.string,
}

SurvivorEffect.defaultProps = {
    value: {},
    title: '',
}
