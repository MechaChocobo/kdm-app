import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, Keyboard} from 'react-native';
import Uuid from 'react-native-uuid';

import FadeInWrapper from '../../shared/fade-in-wrapper';
import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import AppTextInput from '../../shared/AppTextInput';
import AppText from '../../shared/app-text';
import { MILESTONE_TYPE_GLOBAL } from '../../../reducers/custom-expansions';
import Popup from '../../shared/popup';
import ListItem from '../../shared/ListItem';
import styles from '../../../style/styles';
import ToggleListItem from '../../shared/ToggleListItem';
import { IncrementorHidden } from '../../shared/IncrementorHidden';
import { getLocationConfig } from '../../../helpers/helpers';
import DeleteIcon from '../shared/DeleteIcon';
import Icon from '../../shared/Icon';
import Hr from '../../shared/Hr';

const BASE_HEIGHT = 65;
export default class Milestone extends PureComponent {
    wrapper;
    titleInput;
    descInput;
    rewardInput;

    state = {
        showEffectPopup: false,
        showFightingArtPopup: false,
        showGearPopup: false,
        showResourcePopup: false,
        showSettlementEventPopup: false,
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={localStyles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true}>
                            <DeleteIcon onPress={this.onDelete} />
                            <View style={styles.rowCentered}>
                                <Icon name={this.props.config.type === MILESTONE_TYPE_GLOBAL ? "sk-globe" : "sk-house"} size={20} color={colors.TEXT}/>
                                <AppText style={styles.title}>{this.props.config.type === MILESTONE_TYPE_GLOBAL ? t("Global Milestone") : t("Settlement Milestone")}</AppText>
                            </View>
                            <SubHeader
                                title={t("Title")}
                                onPress={this.titleInputFocus}
                            />
                            <AppTextInput
                                textRef={(ref) => this.titleInput = ref}
                                inputStyle={localStyles.input}
                                placeholder={t("Enter Title")}
                                value={this.props.config.title}
                                onChangeText={this.onTitleChange}
                                returnKeyType="default"
                                onBlur={Keyboard.dismiss}
                            />
                            {/* <SubHeader
                                title={t("Description")}
                                onPress={this.descInputFocus}
                            />
                            <AppTextInput
                                textRef={(ref) => this.titleInput = ref}
                                inputStyle={localStyles.input}
                                placeholder={t("Enter Description")}
                                value={this.props.config.description}
                                onChangeText={this.onDescriptionChange}
                                multiline={true}
                                returnKeyType="default"
                                onBlur={Keyboard.dismiss}
                            /> */}
                            <SubHeader
                                title={t("Reward Text")}
                                onPress={this.rewardInputFocus}
                            />
                            <AppTextInput
                                textRef={(ref) => this.rewardInput = ref}
                                inputStyle={localStyles.input}
                                placeholder={t("Enter Reward Text")}
                                value={this.props.config.reward}
                                onChangeText={this.onRewardChange}
                                returnKeyType="default"
                                onBlur={Keyboard.dismiss}
                            />
                            <SubHeader
                                title={t("Effects When Gained")}
                                icon="plus-circle-outline"
                                onPress={this.showEffectPopup}
                            />
                            {this.props.config.effects.addFightingArt && <View>
                                <DeleteIcon onPress={this.deleteAddFightingArt} />
                                <SubHeader
                                    title={t("Fighting Arts")}
                                    titleWeight="light"
                                    onPress={this.showFightingArtPopup}
                                />
                                <AppText>{this.props.config.effects.addFightingArt.map((id, i) => {
                                    return this.props.fightingArtConfig[id].title;
                                }).join(', ')}</AppText>
                                <Popup
                                    visible={this.state.showFightingArtPopup}
                                    title={t("Choose Fighting Art")}
                                    onDismissed={this.hideFightingArtPopup}
                                >
                                    {Object.values(this.props.fightingArtConfig).map((art, i) => {
                                        return <ToggleListItem
                                            key={i}
                                            title={art.title}
                                            id={art.name}
                                            onToggle={this.toggleFightingArt}
                                            value={this.props.config.effects.addFightingArt.indexOf(art.name) !== -1}
                                        />
                                    })}
                                </Popup>
                            </View>}
                            {this.props.config.effects.addGear && <View>
                                <DeleteIcon onPress={this.deleteAddGear} />
                                <SubHeader
                                    title={t("Gear")}
                                    titleWeight="light"
                                    onPress={this.showGearPopup}
                                />
                                <AppText>{this.props.config.effects.addGear.map((gear, i) => {
                                    return this.props.gearConfig[gear.location][gear.id].title;
                                }).join(', ')}</AppText>
                                <Popup
                                    visible={this.state.showGearPopup}
                                    title={t("Choose Gear")}
                                    onDismissed={this.hideGearPopup}
                                >
                                    {this.renderGearPopup()}
                                </Popup>
                            </View>}
                            {this.props.config.effects.addResource && <View>
                                <DeleteIcon onPress={this.deleteAddResource} />
                                <SubHeader
                                    title={t("Resources")}
                                    titleWeight="light"
                                    onPress={this.showResourcePopup}
                                />
                                <AppText>{this.props.config.effects.addResource.map((resource, i) => {
                                    return this.props.resourceConfig[resource.location][resource.id].title;
                                }).join(', ')}</AppText>
                                <Popup
                                    visible={this.state.showResourcePopup}
                                    title={t("Choose Gear")}
                                    onDismissed={this.hideResourcePopup}
                                >
                                    {this.renderResourcePopup()}
                                </Popup>
                            </View>}
                            {this.props.config.effects.addSettlementEvent && <View>
                                <DeleteIcon onPress={this.deleteAddSettlementEvent} />
                                <SubHeader
                                    title={t("Settlement Events")}
                                    titleWeight="light"
                                    onPress={this.showSettlementEventPopup}
                                />
                                <AppText>{this.props.config.effects.addSettlementEvent.map((event, i) => {
                                    return this.props.settlementEventConfig[event].title;
                                }).join(', ')}</AppText>
                                <Popup
                                    visible={this.state.showSettlementEventPopup}
                                    title={t("Choose Gear")}
                                    onDismissed={this.hideSettlementEventPopup}
                                >
                                    {Object.values(this.props.settlementEventConfig).map((event, i) => {
                                        return <ToggleListItem
                                            key={i}
                                            title={event.title}
                                            id={event.name}
                                            onToggle={this.toggleSettlementEvent}
                                            value={this.props.config.effects.addSettlementEvent.indexOf(event.name) !== -1}
                                        />
                                    })}
                                </Popup>
                            </View>}
                            <SubHeader
                                title={this.props.config.type === MILESTONE_TYPE_GLOBAL ? t("Global Conditions") : t("Conditions")}
                                icon="plus-circle-outline"
                                onPress={this.addCondition}
                            />
                            {this.renderConditions()}
                            {this.props.config.type === MILESTONE_TYPE_GLOBAL && <View>
                                <SubHeader
                                    title={t("Settlement Conditions")}
                                    icon="plus-circle-outline"
                                    onPress={this.addSettlementCondition}
                                />
                                {this.renderSettlementConditions()}
                            </View>}
                        </Zebra>
                        <Popup
                            visible={this.state.showEffectPopup}
                            title={t("Choose Effect Type")}
                            onDismissed={this.hideEffectPopup}
                        >
                            {!this.props.config.effects.addFightingArt && <ListItem
                                title={t("Add Fighting Art")}
                                buttonText={t("Select")}
                                onPress={this.addEffectFightingArt}
                            />}
                            {!this.props.config.effects.addGear && <ListItem
                                style={{marginTop: 10}}
                                title={t("Add Gear")}
                                buttonText={t("Select")}
                                onPress={this.addEffectGear}
                            />}
                            {!this.props.config.effects.addResource && <ListItem
                                style={{marginTop: 10}}
                                title={t("Add Resource")}
                                buttonText={t("Select")}
                                onPress={this.addEffectResource}
                            />}
                            {!this.props.config.effects.addSettlementEvent && <ListItem
                                style={{marginTop: 10}}
                                title={t("Add Settlement Event")}
                                buttonText={t("Select")}
                                onPress={this.addEffectSettlementEvent}
                            />}
                        </Popup>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    deleteAddGear = () => {
        let config = {...this.props.config};
        config.effects = {...this.props.config.effects};

        delete config.effects.addGear;
        this.changeConfig(config);
    }

    deleteAddSettlementEvent = () => {
        let config = {...this.props.config};
        config.effects = {...this.props.config.effects};

        delete config.effects.addSettlementEvent;
        this.changeConfig(config);
    }

    deleteAddFightingArt = () => {
        let config = {...this.props.config};
        config.effects = {...this.props.config.effects};

        delete config.effects.addFightingArt;
        this.changeConfig(config);
    }

    deleteAddResource = () => {
        let config = {...this.props.config};
        config.effects = {...this.props.config.effects};

        delete config.effects.addResource;
        this.changeConfig(config);
    }

    renderResourcePopup = () => {
        const resources = [];
        for(let location in this.props.resourceConfig) {
            for(let id in this.props.resourceConfig[location]) {
                resources.push(<ToggleListItem
                    key={id}
                    title={this.props.resourceConfig[location][id].title}
                    subTitle={location}
                    id={location+';'+id}
                    onToggle={this.toggleResource}
                    value={this.isStorageItemSelected(location, id, this.props.config.effects.addResource)}
                />);
            }
        }

        return resources;
    }

    renderGearPopup = () => {
        const gear = [];
        const LocationConfig = getLocationConfig();
        for(let location in this.props.gearConfig) {
            for(let id in this.props.gearConfig[location]) {
                gear.push(<ToggleListItem
                    key={id}
                    title={this.props.gearConfig[location][id].title}
                    subTitle={LocationConfig[location] ? LocationConfig[location].title : location}
                    id={location+';'+id}
                    onToggle={this.toggleGear}
                    value={this.isStorageItemSelected(location, id, this.props.config.effects.addGear)}
                />);
            }
        }

        return gear;
    }

    isStorageItemSelected(location, id, selected) {
        for(let i = 0; i < selected.length; i++) {
            if(selected[i].location === location && selected[i].id === id) {
                return true;
            }
        }

        return false;
    }

    renderConditions = () => {
        if(this.props.config.conditions) {
            const length = Object.keys(this.props.config.conditions).length;
            return Object.values(this.props.config.conditions).map((condition, i) => {
                return <View key={i}>
                    <Condition
                        index={length > 1 ? i + 1 : false}
                        condition={condition}
                        id={condition.id}
                        onChange={this.onConditionChange}
                        onDelete={this.onConditionDelete}
                        />
                    {i !== length - 1 && <Hr style={[styles.centeredWrapper, styles.width90]} />}
                </View>
            });
        } else {
            return null;
        }
    }

    renderSettlementConditions = () => {
        if(this.props.config.settlementConditions) {
            const length = Object.keys(this.props.config.settlementConditions).length;
            return Object.values(this.props.config.settlementConditions).map((condition, i) => {
                return <View key={i}>
                    <Condition
                        index={length > 1 ? i + 1 : false}
                        condition={condition}
                        id={condition.id}
                        onChange={this.onSettlementConditionChange}
                        onDelete={this.onSettlementConditionDelete}
                        />
                    {i !== length - 1 && <Hr style={[styles.centeredWrapper, styles.width90]} />}
                </View>
            });
        } else {
            return null;
        }
    }

    onConditionChange = (id, condition) => {
        let conditions = {...this.props.config.conditions};
        conditions[id] = condition;
        this.changeConfig({...this.props.config, conditions});
    }

    onConditionDelete = (id) => {
        let conditions = {...this.props.config.conditions};
        delete conditions[id];
        this.changeConfig({...this.props.config, conditions});
    }

    onSettlementConditionChange = (id, condition) => {
        let conditions = {...this.props.config.settlementConditions};
        conditions[id] = condition;
        this.changeConfig({...this.props.config, settlementConditions: conditions});
    }

    onSettlementConditionDelete = (id, condition) => {
        let conditions = {...this.props.config.settlementConditions};
        delete conditions[id];
        this.changeConfig({...this.props.config, settlementConditions: conditions});
    }

    addCondition = () => {
        let config = {...this.props.config};
        if(config.conditions) {
            const id = Uuid.v4();
            config.conditions = {...config.conditions}
            config.conditions[id] = {
                id,
                description: '',
                value: 1,
            }
        }
        this.changeConfig(config);
    }

    addSettlementCondition = () => {
        let config = {...this.props.config};
        if(config.settlementConditions) {
            const id = Uuid.v4();
            config.settlementConditions = {...config.settlementConditions}
            config.settlementConditions[id] = {
                id,
                description: '',
                value: 1,
            }
        }
        this.changeConfig(config);
    }

    onDelete = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.id);
    }

    onTitleChange = (text) => {
        this.changeConfig({...this.props.config, title: text});
    }

    onDescriptionChange = (text) => {
        this.changeConfig({...this.props.config, description: text});
    }

    onRewardChange = (text) => {
        this.changeConfig({...this.props.config, reward: text});
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    descInputFocus = () => {
        this.descInput.focus();
    }

    rewardInputFocus = () => {
        this.rewardInput.focus();
    }

    showEffectPopup = () => {
        this.setState({showEffectPopup: true});
    }

    showFightingArtPopup = () => {
        this.setState({showFightingArtPopup: true});
    }

    hideFightingArtPopup = () => {
        this.setState({showFightingArtPopup: false});
    }

    showGearPopup = () => {
        this.setState({showGearPopup: true});
    }

    hideGearPopup = () => {
        this.setState({showGearPopup: false});
    }

    showResourcePopup = () => {
        this.setState({showResourcePopup: true});
    }

    hideResourcePopup = () => {
        this.setState({showResourcePopup: false});
    }

    showSettlementEventPopup = () => {
        this.setState({showSettlementEventPopup: true});
    }

    hideSettlementEventPopup = () => {
        this.setState({showSettlementEventPopup: false});
    }

    hideEffectPopup = (callback) => {
        if(callback) {
            this.setState({showEffectPopup: false}, callback);
        } else {
            this.setState({showEffectPopup: false});
        }
    }

    addEffectFightingArt = () => {
        this.changeConfig({...this.props.config, effects: {...this.props.config.effects, addFightingArt: []}});
        this.hideEffectPopup();
    }

    addEffectGear = () => {
        this.changeConfig({...this.props.config, effects: {...this.props.config.effects, addGear: []}});
        this.hideEffectPopup();
    }

    addEffectResource = () => {
        this.changeConfig({...this.props.config, effects: {...this.props.config.effects, addResource: []}});
        this.hideEffectPopup();
    }

    addEffectSettlementEvent = () => {
        this.changeConfig({...this.props.config, effects: {...this.props.config.effects, addSettlementEvent: []}});
        this.hideEffectPopup();
    }

    toggleFightingArt = (id) => {
        let effects = {...this.props.config.effects};
        if(!effects.addFightingArt) {
            effects.addFightingArt = [id];
        } else {
            let arts = [...effects.addFightingArt];
            if(arts.indexOf(id) !== -1) {
                arts.splice(arts.indexOf(id), 1);
            } else {
                arts.push(id);
            }
            effects.addFightingArt = arts;
        }
        this.changeConfig({...this.props.config, effects});
    }

    toggleSettlementEvent = (id) => {
        let effects = {...this.props.config.effects};
        if(!effects.addSettlementEvent) {
            effects.addSettlementEvent = [id];
        } else {
            let events = [...effects.addSettlementEvent];
            if(events.indexOf(id) !== -1) {
                events.splice(events.indexOf(id), 1);
            } else {
                events.push(id);
            }
            effects.addSettlementEvent = events;
        }
        this.changeConfig({...this.props.config, effects});
    }

    toggleGear = (idString) => {
        this.toggleStorageItem('addGear', idString);
    }

    toggleResource = (idString) => {
        this.toggleStorageItem('addResource', idString);
    }

    toggleStorageItem = (type, idString) => {
        const split = idString.split(';');
        const location = split[0];
        const id = split[1];
        let effects = {...this.props.config.effects};

        if(!effects[type]) {
            effects[type] = [{location, id}];
        } else {
            effects[type] = [...effects[type]];
            let toSplice = -1;

            for(let i = 0; i < this.props.config.effects[type].length; i++) {
                if(this.props.config.effects[type][i].location === location && this.props.config.effects[type][i].id === id) {
                    toSplice = i;
                    break;
                }
            }

            if(toSplice !== -1) {
                effects[type].splice(toSplice, 1);
            } else {
                effects[type].push({location, id});
            }
        }

        this.changeConfig({...this.props.config, effects});
    }

    changeConfig = (config) => {
        config = JSON.parse(JSON.stringify(config));
        this.props.onChange(this.props.id, config);
    }
}

Milestone.propTypes = {
    config: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    skipRenderAnimation: PropTypes.bool,
    id: PropTypes.string.isRequired,
}

const localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
    container: {
        marginBottom: 20
    }
});

class Condition extends PureComponent {
    input = null;

    render() {
        return <View>
            <DeleteIcon onPress={this.onDelete}/>
            <SubHeader
                title={this.props.index ? t("Condition %num% Description", {num: this.props.index}) : t("Condition Description")}
                onPress={this.focusInput}
            />
            <AppTextInput
                textRef={(ref) => this.input = ref}
                inputStyle={localStyles.input}
                placeholder={t("Enter Description")}
                value={this.props.condition.description}
                onChangeText={this.onDescriptionChange}
                returnKeyType="default"
                onBlur={Keyboard.dismiss}
            />
            <View style={styles.rowCentered}>
                <AppText style={styles.itemText}>{t("Target Value")}: </AppText>
                <IncrementorHidden
                    value={this.props.condition.value}
                    onIncrease={this.onValueIncrease}
                    onDecrease={this.onValueDecrease}
                    smallValue={true}
                />
            </View>
        </View>
    }

    onDelete = () => {
        this.props.onDelete(this.props.id);
    }

    onValueIncrease = () => {
        this.props.onChange(this.props.id, {...this.props.condition, value: this.props.condition.value + 1});
    }

    onValueDecrease = () => {
        if(this.props.condition.value > 1) {
            this.props.onChange(this.props.id, {...this.props.condition, value: this.props.condition.value - 1});
        }
    }

    onDescriptionChange = (text) => {
        this.props.onChange(this.props.id, {...this.props.condition, description: text});
    }

    focusInput = () => {
        this.input.focus();
    }
}
