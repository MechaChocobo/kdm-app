import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {View, StyleSheet, TextInput, TouchableOpacity} from 'react-native';
import Text from '../../shared/app-text';
import styles from '../../../style/styles';
import { getModdableSurvivorStats } from '../../../helpers/helpers';
import ColorContext from '../../../context/ColorContext';
import AppTextInput from '../../shared/AppTextInput';
import { IncrementorHidden } from '../../shared/IncrementorHidden';

const survivorStats = getModdableSurvivorStats();
export default class SurvivorStatInput extends Component {
    constructor(props) {
        super(props);
        this.state = {value: props.value}
        this.colors = null;
    }

    render() {
        return(
            <ColorContext.Consumer>
                {colors => {
                    this.colors = colors;
                    return(
                        <View>
                            {this.renderItems()}
                        </View>
                    )
                }}
            </ColorContext.Consumer>
        )
    }

    renderItems = () => {
        return Object.keys(this.props.value).map((item, i) => {
            if(typeof this.props.value[item] === "object") {
                return Object.keys(this.props.value[item]).map((subItem, i) => {
                    return <View key={i}>{this.renderItem(item, this.props.value[item][subItem], subItem)}</View>
                });
            } else {
                return <View key={i}>{this.renderItem(item, this.props.value[item])}</View>;
            }
        });
    }

    renderItem = (item, value, subItem = null) => {
        let itemName = item;
        if(subItem) {
            itemName += '.' + subItem;
        }

        if(typeof value === "boolean") {
            return (
                <View>
                    <View style={styles.rowCentered}>
                        <TouchableOpacity onPress={this.removeStat(itemName)}><Icon name="close" color={this.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                        <Text style={styles.itemText}>{survivorStats[itemName].title}</Text>
                    </View>
                    <View height={5}/>
                </View>
            )
        } else {
            return <View>
                <View style={styles.rowCentered}>
                    <TouchableOpacity onPress={this.removeStat(itemName)}><Icon name="close" color={this.colors.HIGHLIGHT} size={22}/></TouchableOpacity>
                    <Text style={styles.itemText}>{survivorStats[itemName].title}: </Text>
                    <IncrementorHidden
                        value={value}
                        onIncrease={this.onIncrease(itemName)}
                        onDecrease={this.onDecrease(itemName)}
                        smallValue={true}
                    />
                </View>
                <View height={5}/>
            </View>
        }
    }

    onIncrease = (name) => {
        return () => {
            let value = {...this.props.value};
            let arr = name.split(".");
            if(arr.length > 1) {
                value[arr[0]][arr[1]] = value[arr[0]][arr[1]] + 1;
            } else {
                value[arr[0]] = value[arr[0]] + 1;
            }
            this.valueChanged(value);
        }
    }

    onDecrease = (name) => {
        return () => {
            let value = {...this.props.value};
            let arr = name.split(".");
            if(arr.length > 1) {
                value[arr[0]][arr[1]] = value[arr[0]][arr[1]] - 1;
            } else {
                value[arr[0]] = value[arr[0]] - 1;
            }
            this.valueChanged(value);
        }
    }

    setValue = (name) => {
        return (text) => {
            let value = {...this.props.value};
            let arr = name.split(".");
            if(arr.length > 1) {
                value[arr[0]][arr[1]] = +text;
            } else {
                value[arr[0]] = +text;
            }

            this.valueChanged(value);
        }
    }

    removeStat = (name) => {
        return () => {
            let value = {...this.props.value};
            let arr = name.split(".");
            if(arr.length > 1) {
                delete value[arr[0]][arr[1]];
            } else {
                delete value[arr[0]];
            }

            this.valueChanged(value);
        }
    }

    valueChanged = (value) => {
        this.props.onChange(value);
    }
}

SurvivorStatInput.propTypes = {
    value: PropTypes.any.isRequired,
    onChange: PropTypes.func.isRequired,
}

var localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
        fontWeight: "600",
        fontSize: 18,
        textAlign: "center",
    },
});
