import React, { Component } from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import FadeInWrapper from '../../shared/fade-in-wrapper';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import AppTextInput from '../../shared/AppTextInput';
import { IncrementorHidden } from '../../shared/IncrementorHidden';
import ArmorConfig from '../../../config/armor.json';
import { getItemsByExpansion, getSortedExpansions } from '../../../helpers/helpers';
import ReplaceInput from '../shared/ReplaceInput';

const BASE_HEIGHT = 102;
export default class ArmorSet extends Component {
    wrapper;
    titleInput;
    descriptionInput;

    constructor(props) {
        super(props);

        const armorByExpansion = getItemsByExpansion(ArmorConfig);
        this.state = {
            coreArmorByExpansion: armorByExpansion,
            sortedExpansions: getSortedExpansions(Object.keys(armorByExpansion)),
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={styles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true}>
                            <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                                <TouchableOpacity onPress={this.delete}>
                                    <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <SubHeader
                                    title={t("Title")}
                                    onPress={this.titleInputFocus}
                                />
                                <AppTextInput
                                    textRef={(ref) => this.titleInput = ref}
                                    inputStyle={styles.input}
                                    placeholder={t("Enter Title")}
                                    value={this.props.config.title}
                                    onChangeText={this.onTitleChange}
                                />
                                <SubHeader
                                    title={t("Description")}
                                    onPress={this.descriptionInputFocus}
                                />
                                <AppTextInput
                                    textRef={(ref) => this.descriptionInput = ref}
                                    inputStyle={styles.input}
                                    placeholder={t("Enter Description")}
                                    value={this.props.config.description}
                                    onChangeText={this.onDescriptionChange}
                                    multiline={true}
                                />
                                <SubHeader
                                    title={t("Armor Amount")}
                                />
                                <View style={{marginLeft: 50}}>
                                    <IncrementorHidden
                                        value={this.props.config.armor ? this.props.config.armor : 0}
                                        onIncrease={this.onArmorIncrease}
                                        onDecrease={this.onArmorDecrease}
                                        smallValue={true}
                                    />
                                </View>
                                <ReplaceInput
                                    title={t("Replaces Armor Set")}
                                    popupTitle={t("Select Armor Set")}
                                    config={this.props.config}
                                    allItems={this.state.coreArmorByExpansion}
                                    itemConfig={ArmorConfig}
                                    expansions={this.state.sortedExpansions}
                                    onChange={this.triggerConfigChanged}
                                />
                            </View>
                        </Zebra>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    onArmorIncrease = () => {
        let config = {...this.props.config};
        config.armor += 1;
        this.triggerConfigChanged(config);
    }

    onArmorDecrease = () => {
        let config = {...this.props.config};

        if(config.armor > 0) {
            config.armor -= 1;
            this.triggerConfigChanged(config);
        }
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    descriptionInputFocus = () => {
        this.descriptionInput.focus();
    }

    onTitleChange = (title) => {
        let config = {...this.props.config};
        config.title = title;
        this.triggerConfigChanged(config);
    }

    onDescriptionChange = (description) => {
        let config = {...this.props.config};
        config.description = description;
        this.triggerConfigChanged(config);
    }

    delete = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.id);
    }

    triggerConfigChanged = (config) => {
        this.props.onConfigChange(this.props.id, config);
    }
}

var styles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
    container: {
        marginBottom: 20
    }
});
