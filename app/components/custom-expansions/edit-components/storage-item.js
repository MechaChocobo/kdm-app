'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
    View,
    TouchableOpacity,
    StyleSheet,
} from 'react-native';

import AppText from '../../shared/app-text';
import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import Popup from '../../shared/popup';
import MultiSelect from '../../shared/multi-select';
import styles from '../../../style/styles';
import FadeInWrapper from '../../shared/fade-in-wrapper';
import AppTextInput from '../../shared/AppTextInput';
import ColorContext from '../../../context/ColorContext';
import Icon from '../../shared/Icon';
import Checkbox from '../../shared/Checkbox';
import ReplaceInput from '../shared/ReplaceInput';
import ResourceConfig from '../../../config/resources.json';
import GearConfig from '../../../config/gear.json';
import { getSortedExpansions, sortStorageByExpansion } from '../../../helpers/helpers';

export const keywords = [
    {name: "bone", title: "bone"},
    {name: "hide", title: "hide"},
    {name: "organ", title: "organ"},
    {name: "scrap", title: "scrap"},
    {name: "consumable", title: "consumable"},
    {name: "leather", title: "leather"},
    {name: "iron", title: "iron"},
    {name: "herb", title: "herb"},
    {name: "vermin", title: "vermin"},
    {name: "skull", title: "skull"},
    {name: "other", title: "other"},
    {name: "dung", title: "dung"},
    {name: "fish", title: "fish"},
    {name: "flower", title: "flower"},
    {name: "silk", title: "silk"},
];

const BASE_HEIGHT = 157;

export default class StorageItem extends Component {
    wrapper;
    titleInput;
    descriptionInput;

    constructor(props) {
        super(props);

        const storageConfig = this.props.type === "resource" ? ResourceConfig : GearConfig;
        const itemsByExpansion = sortStorageByExpansion(storageConfig, storageConfig);
        this.state = {
            showKeywordPopup: false,
            storageConfig,
            coreItemsByExpansion: itemsByExpansion,
            sortedExpansions: getSortedExpansions(Object.keys(itemsByExpansion)),
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={{}}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                            <TouchableOpacity onPress={this.deleteResource}>
                                <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                            </TouchableOpacity>
                        </View>
                        <SubHeader
                            title={t("Title")}
                            onPress={this.titleInputFocus}
                        />
                        <AppTextInput
                            textRef={(ref) => this.titleInput = ref}
                            inputStyle={localStyles.input}
                            placeholder={t("Enter Title")}
                            value={this.props.config.title}
                            onChangeText={this.onTitleChange}
                        />

                        <SubHeader
                            title={t("Description")}
                            onPress={this.descriptionInputFocus}
                        />
                        <AppTextInput
                            textRef={(ref) => this.descriptionInput = ref}
                            inputStyle={localStyles.input}
                            placeholder={t("Enter Description")}
                            value={this.props.config.description}
                            onChangeText={this.onDescriptionChange}
                        />

                        <SubHeader title={t("Add to Storage By Default")} />
                        <TouchableOpacity onPress={this.toggleAddByDefault}>
                            <View style={styles.rowCentered}>
                                <Checkbox checked={this.props.config.skipAdd !== true} />
                                <AppText style={styles.itemText}> {t("Add to storage by default")}</AppText>
                            </View>
                        </TouchableOpacity>
                        {this.props.type === "resource" && <View>
                            <SubHeader
                                title={t("Keywords")}
                                onPress={this.showKeywordPopup}
                            />
                            {this.renderKeywords()}

                            <Popup
                                visible={this.state.showKeywordPopup}
                                title={t("Select Type")}
                                onDismissed={this.hideKeywordPopup}
                            >
                                <MultiSelect
                                    items={keywords}
                                    selected={this.props.config.keywords}
                                    onChange={this.setKeywords}
                                    allowCustom={true}
                                />
                            </Popup>
                        </View>}
                        <ReplaceInput
                            storage={true}
                            title={this.props.type === "resource" ? t("Replaces Resource") : t("Replaces Gear")}
                            popupTitle={this.props.type === "resource" ? t("Select Resource") : t("Select Gear")}
                            config={this.props.config}
                            allItems={this.state.coreItemsByExpansion}
                            itemConfig={this.state.storageConfig}
                            expansions={this.state.sortedExpansions}
                            onChange={this.triggerChange}
                        />
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        );
    }

    toggleAddByDefault = () => {
        let newConfig = JSON.parse(JSON.stringify(this.props.config));
        if(newConfig.skipAdd === true) {
            delete newConfig.skipAdd;
        } else {
            newConfig.skipAdd = true;
        }

        this.triggerChange(newConfig);
    }

    deleteResource = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.config.id);
    }

    renderKeywords = () => {
        return <AppText style={styles.itemText}>{this.props.config.keywords.join(', ')}</AppText>
    }

    showKeywordPopup = () => {
        this.setState({showKeywordPopup: true});
    }

    hideKeywordPopup = () => {
        this.setState({showKeywordPopup: false});
    }

    setKeywords = (words) => {
        let config = {...this.props.config};
        config.keywords = words;
        this.triggerChange(config);
    }

    onDescriptionChange = (text) => {
        let config = {...this.props.config};
        config.description = text;
        this.triggerChange(config);
    }

    onTitleChange = (text) => {
        let config = {...this.props.config};
        config.title = text;
        this.triggerChange(config);
    }

    titleInputFocus = () => {
        this.titleInput.focus();
    }

    descriptionInputFocus = () => {
        this.descriptionInput.focus();
    }

    triggerChange = (config) => {
        this.props.onChange(config);
    }
}

StorageItem.propTypes = {
    config: PropTypes.object.isRequired,
    type: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
}

var localStyles = StyleSheet.create({
    input: {
        color: "white",
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
});
