import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {View, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import SubHeader from '../../shared/sub-header';
import AppText from '../../shared/app-text';
import styles from '../../../style/styles';
import Popup from '../../shared/popup';
import SelectOneList from '../../shared/select-one-list';
import ColorContext from '../../../context/ColorContext';
import { t } from '../../../helpers/intl';

export default class TimelineEventEdit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showAddEventPopup: false,
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View>
                        <SubHeader
                            title={this.props.title}
                            icon="plus-circle-outline"
                            onPress={this.showPopup}
                        />

                        {this.props.selected.map((text, i) => {
                            return (<View key={i} style={styles.rowCentered}>
                                <TouchableOpacity onPress={this.removeEvent(text)}>
                                    <Icon name="close" size={18} color={colors.HIGHLIGHT}/>
                                </TouchableOpacity>
                                <AppText style={styles.itemText}>{this.props.config ? this.props.config[text].title : text}</AppText>
                            </View>)
                        })}

                        <Popup
                            visible={this.state.showAddEventPopup}
                            title={t("Select Event")}
                            onDismissed={this.onAddEventPopupClosed}
                        >
                            <SelectOneList items={this.props.options} onChange={this.addEvent} allowCustom={this.props.config ? false : true}/>
                        </Popup>
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    removeEvent = (text) => {
        return () => {
            let config = [...this.props.selected];
            config.splice(config.indexOf(text), 1);
            this.onChange(config);
        }
    }

    showPopup = () => {
        this.setState({showAddEventPopup: true});
    }

    onAddEventPopupClosed = () => {
        this.setState({showAddEventPopup: false});
    }

    addEvent = (name) => {
        let config = [...this.props.selected];
        config.push(name);
        this.setState({showAddEventPopup: false});
        this.onChange(config);
    }

    onChange = (config) => {
        this.props.onChange(this.props.type, config);
    }
}

TimelineEventEdit.propTypes = {
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    options: PropTypes.arrayOf(PropTypes.object),
    selected: PropTypes.arrayOf(PropTypes.string),
    onChange: PropTypes.func.isRequired,
    config: PropTypes.any
}

TimelineEventEdit.defaultProps = {
    selected: [],
    options: [],
    config: false,
}
