import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {View, StyleSheet, TouchableOpacity, Keyboard} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FadeInWrapper from '../../shared/fade-in-wrapper';
import { t } from '../../../helpers/intl';
import SubHeader from '../../shared/sub-header';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import AppTextInput from '../../shared/AppTextInput';

const BASE_HEIGHT = 65;
export default class TextEvent extends Component {
    wrapper;
    input;

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={localStyles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true}>
                            <View style={{alignSelf: "flex-end", position: "absolute", zIndex: 100}}>
                                <TouchableOpacity onPress={this.onDelete}>
                                    <Icon name="close" color={colors.HIGHLIGHT} size={25}/>
                                </TouchableOpacity>
                            </View>
                            <SubHeader
                                title={this.props.label}
                                onPress={this.inputFocus}
                            />
                            <AppTextInput
                                textRef={(ref) => this.input = ref}
                                inputStyle={localStyles.input}
                                placeholder={this.props.placeholder}
                                value={this.props.text}
                                onChangeText={this.onChange}
                                multiline={true}
                                returnKeyType="default"
                                onBlur={Keyboard.dismiss}
                            />
                            {this.props.children}
                        </Zebra>
                    </FadeInWrapper>
                }
            </ColorContext.Consumer>
        )
    }

    onDelete = () => {
        this.wrapper.remove(this.triggerDelete);
    }

    triggerDelete = () => {
        this.props.onDelete(this.props.id);
    }

    onChange = (text) => {
        this.props.onChange(this.props.id, text);
    }

    inputFocus = () => {
        this.input.focus();
    }
}

TextEvent.propTypes = {
    text: PropTypes.string,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    skipRenderAnimation: PropTypes.bool,
    id: PropTypes.string.isRequired,
}

TextEvent.defaultProps = {
    text: "",
    label: t("Event"),
    placeholder: t("Enter Event"),
    skipRenderAnimation: true,
}

var localStyles = StyleSheet.create({
    input: {
        fontSize: 16,
        padding: 0,
        borderBottomWidth: 1,
    },
    container: {
        marginBottom: 20
    }
});
