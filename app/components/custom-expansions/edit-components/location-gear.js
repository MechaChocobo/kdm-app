import React, { PureComponent } from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import FadeInWrapper from '../../shared/fade-in-wrapper';
import AppText from '../../shared/app-text';
import styles from '../../../style/styles';
import ColorContext from '../../../context/ColorContext';
import Zebra from '../../shared/Zebra';
import Hr from '../../shared/Hr';
import GearItem from './GearItem';

const BASE_HEIGHT = 157;
export default class LocationGear extends PureComponent {
    wrapper;

    constructor(props) {
        super(props);

        let renderedGear = {};
        let gear = Object.keys(this.props.gear);
        for(let i = 0; i < gear.length; i++) {
            renderedGear[gear[i]] = true;
        }

        this.state = {
            renderedGear
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <FadeInWrapper
                        ref={(ref) => this.wrapper = ref}
                        styles={localStyles.container}
                        skipRenderAnimation={this.props.skipRenderAnimation}
                        baseHeight={BASE_HEIGHT}
                    >
                        <Zebra zebra={true}>
                            <View style={{flexDirection: "row", alignItems: "center", justifyContent: "space-between"}}>
                                <TouchableOpacity style={{flexDirection: "row", alignItems: "center"}} onPress={this.addGear}>
                                    <Icon name="plus-circle-outline" size={18} color={colors.HIGHLIGHT}/>
                                    <AppText style={styles.title}>{this.props.locationTitle}</AppText>
                                    <View />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.onLocationDelete}>
                                    <Icon name="close" color={colors.HIGHLIGHT} size={24} />
                                </TouchableOpacity>
                            </View>
                            {this.renderGear(colors)}
                        </Zebra>
                    </FadeInWrapper>}
            </ColorContext.Consumer>
        )
    }

    renderGear = (colors) => {
        return Object.values(this.props.gear).map((gear, i) => {
            return <View key={gear.id}>
                <GearItem
                    config={gear}
                    onChange={this.onGearChange}
                    onDelete={this.onGearDelete}
                    skipRenderAnimation={this.state.renderedGear[gear.id]}
                    onCursedToggle={this.toggleCursed}
                    colors={colors}
                    resources={this.props.resources}
                />
                {i < Object.values(this.props.gear).length - 1 && <Hr/>}
            </View>
        });
    }

    toggleCursed = (id) => {
        let config = this.props.gear[id];
        let cursed = true;
        if(config.cursed === true) {
            cursed = false;
        }

        this.props.onCursedChange(this.props.location, id, cursed);
    }

    addGear = () => {
        this.props.onAdd(this.props.location);
    }

    onGearChange = (config) => {
        this.props.onChange(this.props.location, config.id, config);
    }

    onGearDelete = (id) => {
        this.props.onDelete(this.props.location, id);
    }

    onLocationDelete = () => {
        this.wrapper.remove(this.triggerLocationDelete);
    }

    triggerLocationDelete = () => {
        this.props.onLocationDelete(this.props.location);
    }
}

LocationGear.propTypes = {
    locationTitle: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    gear: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    onDelete: PropTypes.func.isRequired,
    onAdd: PropTypes.func.isRequired,
    onLocationDelete: PropTypes.func.isRequired,
}

var localStyles = StyleSheet.create({
    container: {
        marginBottom: 20
    }
});
