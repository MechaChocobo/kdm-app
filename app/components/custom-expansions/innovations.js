'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from '../../style/styles';
import Section from '../shared/section';
import Zebra from '../shared/Zebra';
import { t } from '../../helpers/intl';
import { setInnovationConfig, addInnovation, deleteInnovation } from '../../actions/custom-expansions';
import Innovation from './edit-components/innovation';
import { getColors } from '../../selectors/general';

class Innovations extends Component {
    constructor(props) {
        super(props);

        let renderedInnovations = {};
        for(let i = 0; i < props.expansion.innovations.length; i++) {
            renderedInnovations[props.expansion.innovations[i]] = true;
        }

        this.state = {renderedInnovations};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Innovation')}
                    icon="plus-circle-outline"
                    onPress={this.addInnovation}
                >
                    {this.props.expansion.innovations.map((innovation, index) => {
                        return <Zebra zebra={true} key={this.props.expansion.innovationConfig[innovation].name} style={styles.marginBottom20}>
                            <Innovation
                                name={innovation}
                                innovation={this.props.expansion.innovationConfig[innovation]}
                                customInnovations={this.props.expansion.innovationConfig}
                                onConfigChange={this.onInnovationConfigChange}
                                onDelete={this.deleteInnovation}
                                skipRenderAnimation={this.state.renderedInnovations[innovation]}
                                />
                        </Zebra>
                    })}
                    <View height={15}/>
                </Section>
            </View>
        )
    }

    deleteInnovation = (id) => {
        this.props.deleteInnovation(this.props.expansion.id, id);
    }

    addInnovation = () => {
        this.props.addInnovation(this.props.expansion.id);
    }

    onInnovationConfigChange = (config) => {
        this.props.setInnovationConfig(this.props.expansion.id, config.name, config);
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setInnovationConfig, addInnovation, deleteInnovation}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Innovations);
