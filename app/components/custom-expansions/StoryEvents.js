'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Uuid from 'react-native-uuid';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { setStoryEvent, addStoryEvent, deleteStoryEvent } from '../../actions/custom-expansions';
import TextEvent from './edit-components/TextEvent';
import { getColors } from '../../selectors/general';

class StoryEvents extends Component {
    animateFirst;
    constructor(props) {
        super(props);

        this.animateFirst = false;

        let stateIds = [];
        for(let i = 0; i < this.props.expansion.storyEvents.length; i++) {
            stateIds.push(Uuid.v4());
        }

        this.state = {stateIds};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Story Event')}
                    icon="plus-circle-outline"
                    onPress={this.addEvent}
                >
                    {Object.values(this.props.expansion.storyEvents).map((event, i) => {
                        return <TextEvent
                            key={event.name}
                            text={event.title}
                            id={event.name}
                            onChange={this.onChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={!(this.animateFirst && i === 0)}
                        />
                    })}
                </Section>
            </View>
        )
    }

    addEvent = () => {
        this.animateFirst = true;
        let stateIds = this.state.stateIds;
        stateIds.splice(0, 0, Uuid.v4());
        this.setState({stateIds});
        this.props.addStoryEvent(this.props.expansion.id);
    }

    onChange = (id, text) => {
        this.props.setStoryEvent(this.props.expansion.id, id, text);
    }

    onDelete = (id) => {
        this.props.deleteStoryEvent(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setStoryEvent, addStoryEvent, deleteStoryEvent}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(StoryEvents);
