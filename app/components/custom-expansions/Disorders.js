'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { addDisorder, setDisorderConfig, deleteDisorder } from '../../actions/custom-expansions';
import SurvivorAddition from './edit-components/SurvivorAddition';
import { getColors } from '../../selectors/general';
import DisorderConfig from '../../config/disorders.json';

class Disorders extends Component {
    constructor(props) {
        super(props);

        let renderedDisorders = {};
        for(let i = 0; i < props.expansion.disorders.length; i++) {
            renderedDisorders[props.expansion.disorders[i]] = true;
        }

        this.state = {renderedDisorders};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Disorder')}
                    icon="plus-circle-outline"
                    onPress={this.addDisorder}
                >
                    {this.props.expansion.disorders.map((id) => {
                        return <SurvivorAddition
                            key={id}
                            id={id}
                            config={this.props.expansion.disorderConfig[id]}
                            onConfigChange={this.onConfigChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={this.state.renderedDisorders[id]}
                            baseConfig={DisorderConfig}
                            replaceTitle={t('Replaces Disorder')}
                            replacePopupTitle={t('Select Disorder')}
                        />
                    })}
                </Section>
            </View>
        )
    }

    addDisorder = () => {
        this.props.addDisorder(this.props.expansion.id);
    }

    onConfigChange = (id, config) => {
        this.props.setDisorderConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        this.props.deleteDisorder(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setDisorderConfig, deleteDisorder, addDisorder}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Disorders);
