'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { getColors } from '../../selectors/general';
import { addArmorSet, setArmorSetConfig, deleteArmorSet } from '../../actions/custom-expansions';
import ArmorSet from './edit-components/ArmorSet';

class ArmorSets extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Armor Set')}
                    icon="plus-circle-outline"
                    onPress={this.addArmorSet}
                >
                {this.renderArmorSets()}
                </Section>
            </View>
        )
    }

    renderArmorSets = () => {
        if(this.props.expansion.armorSets) {
            return this.props.expansion.armorSets.map((id, i) => {
                return <ArmorSet
                    key={id}
                    id={id}
                    config={this.props.expansion.armorSetConfig[id]}
                    onDelete={this.onDelete}
                    onConfigChange={this.onChange}
                />
            });
        }
    }

    addArmorSet = () => {
        this.props.addArmorSet(this.props.expansion.id);
    }

    onChange = (id, config) => {
        this.props.setArmorSetConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        this.props.deleteArmorSet(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({deleteArmorSet, setArmorSetConfig, addArmorSet}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ArmorSets);
