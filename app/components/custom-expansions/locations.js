'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Location from './edit-components/location';
import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { setLocationConfig, addLocation, deleteLocation } from '../../actions/custom-expansions';
import { getColors } from '../../selectors/general';

class Locations extends Component {
    constructor(props) {
        super(props);

        let renderedLocations = {};
        for(let i = 0; i < props.expansion.locations.length; i++) {
            renderedLocations[props.expansion.locations[i]] = true;
        }

        this.state = {renderedLocations};
    }

    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Location')}
                    icon="plus-circle-outline"
                    onPress={this.addLocation}
                >
                    {this.props.expansion.locations.map((location) => {
                        return <Location
                            key={this.props.expansion.locationConfig[location].name}
                            location={this.props.expansion.locationConfig[location]}
                            onConfigChange={this.onConfigChange}
                            onDelete={this.onDelete}
                            skipRenderAnimation={this.state.renderedLocations[location]}
                        />
                    })}
                </Section>
            </View>
        )
    }

    onConfigChange = (config) => {
        this.props.setLocationConfig(this.props.expansion.id, config.name, config);
    }

    onDelete = (id) => {
        this.props.deleteLocation(this.props.expansion.id, id);
    }

    addLocation = () => {
        this.props.addLocation(this.props.expansion.id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({setLocationConfig, addLocation, deleteLocation}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Locations);
