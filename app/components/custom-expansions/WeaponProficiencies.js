'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';
import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import styles from '../../style/styles';
import Section from '../shared/section';
import { t } from '../../helpers/intl';
import { addWeaponProf, deleteWeaponProf, setWeaponProfConfig } from '../../actions/custom-expansions';
import WeaponProficiency from './edit-components/WeaponProficiency';
import { getColors } from '../../selectors/general';

class WeaponProficiencies extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Section
                    title={t('Add New Weapon Proficiency')}
                    icon="plus-circle-outline"
                    onPress={this.addWeaponProf}
                >
                {this.renderProficiencies()}
                </Section>
            </View>
        )
    }

    renderProficiencies = () => {
        return this.props.expansion.weaponProfs.map((id, i) => {
            return <WeaponProficiency
                key={id}
                id={id}
                config={this.props.expansion.weaponProfConfig[id]}
                onDelete={this.onDelete}
                onConfigChange={this.onChange}
                abilities={this.props.expansion.abilityConfig}
                innovations={this.props.expansion.innovationConfig}
            />
        });
    }

    addWeaponProf = () => {
        this.props.addWeaponProf(this.props.expansion.id);
    }

    onChange = (id, config) => {
        this.props.setWeaponProfConfig(this.props.expansion.id, id, config);
    }

    onDelete = (id) => {
        this.props.deleteWeaponProf(this.props.expansion.id, id);
    }
}


function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({addWeaponProf, deleteWeaponProf, setWeaponProfConfig}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(WeaponProficiencies);
