'use strict';

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
    View,
} from 'react-native';

import RoundedIcon from '../shared/rounded-icon';
import Text from '../shared/app-text';
import sharedStyles from '../../style/styles';
import { getExpansionList, getExpansionTitle } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';
import LightModal from '../shared/LightModal';
import Button from '../shared/button';
import Hr from '../shared/Hr';

class ExpansionSelector extends Component {
    constructor(props) {
        super(props);
        let expansions = props.expansions ? props.expansions : {};
        this.state = { expansions, showExpansions: false, showExpansionHelp: false };
    }

    componentDidUpdate() {
        if(this.props.campaign === "PotSun" && !this.state.expansions.sunstalker) {
            this.setState({expansions: {...this.state.expansions, sunstalker: "cards"}});
        } else if (this.props.campaign === "PotStars" && !this.state.expansions["dragon-king"]) {
            let expansions = {...this.state.expansions};
            expansions["dragon-king"] = "cards";
            this.setState({expansions});
        }
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <View style={sharedStyles.wrapper}>
                        <View style={sharedStyles.rowCentered}>
                            <Text style={[sharedStyles.title, {marginRight: 10}]}>{this.props.sectionTitle ? this.props.sectionTitle : t("Expansions")}</Text>
                            <RoundedIcon onPress={this.toggleExpansionHelp} icon="help" size={15} borderSize={20} color={colors.HIGHLIGHT}/>
                        </View>
                        <View style={sharedStyles.contentWrapper}>
                            {this.renderExpansionList(colors)}
                            {this.props.onConfirm && <Button title={t("Confirm Selection")} shape="rounded" onPress={this.props.onConfirm} disabled={(Object.keys(this.state.expansions).length === 0 && !this.props.allowEmptySelection)}/>}
                        </View>
                        <LightModal
                            visible={this.state.showExpansionHelp}
                            onDismissed={this.hideHelpModal}
                        >
                            <Text style={sharedStyles.itemText}>{t("%b%All Content%b% will add all expansion content to your game.")}  {t("The monster will be huntable as a quarry or nemesis, and all locations and gear will be available.")}</Text>
                            <Hr style={sharedStyles.width90}/>
                            <Text style={sharedStyles.itemText}>{t("%b%Cards Only%b% will add all expansion cards to your campaign (fighting arts, disorders, innovations, etc.), but the monster will %b%not%b% be available as a quarry or nemesis.")}</Text>
                        </LightModal>
                    </View>
                }
            </ColorContext.Consumer>
        );
    }

    hideHelpModal = () => {
        this.setState({showExpansionHelp: false});
    }

    getExpansionList = () => {
        return this.props.expansionList ? this.props.expansionList : getExpansionList();
    }

    toggleShowExpansions = () => {
        this.setState({showExpansions: !this.state.showExpansions});
    }

    toggleExpansion = (expansion, type) => {
        return () => {
            let expansions = this.state.expansions;
            if(this.state.expansions[expansion] === type) {
                delete expansions[expansion];
            } else {
                expansions[expansion] = type;
            }

            this.triggerChange(expansions);
        }
    }

    addAllContent = () => {
        let expansions = this.getExpansionList();

        let expansionsState = {};
        for(var i = 0; i < expansions.length; i++) {
            expansionsState[expansions[i]] = "all";
        }

        this.triggerChange(expansionsState);
    }

    clearAllContent = () => {
        let expansions = this.state.expansions;

        for(var expansion in expansions) {
            if(expansions[expansion] === "all") {
                delete expansions[expansion];
            }
        }

        this.triggerChange(expansions);
    }

    clearAllCards = () => {
        let expansions = this.state.expansions;

        for(var expansion in expansions) {
            if(expansions[expansion] === "cards") {
                delete expansions[expansion];
            }
        }

        this.triggerChange(expansions);
    }

    addAllCards = () => {
        let expansions = this.getExpansionList();

        let expansionsState = {};
        for(var i = 0; i < expansions.length; i++) {
            expansionsState[expansions[i]] = "cards";
        }

        this.triggerChange(expansionsState);
    }

    getAllContentButton = () => {
        if(Object.values(this.state.expansions).filter(item => item === "all").length === this.getExpansionList().length) {
            return <Button title={t("Clear All")} onPress={this.clearAllContent} shape="pill" fitText={true} />
        } else {
            return <Button title={t("Add All")} onPress={this.addAllContent} shape="pill" fitText={true} />
        }
    }

    getAllCardsButton = () => {
        if(Object.values(this.state.expansions).filter(item => item === "cards").length === this.getExpansionList().length) {
            return <Button title={t("Clear All")} onPress={this.clearAllCards} shape="pill" fitText={true} />
        } else {
            return <Button title={t("Add All")} onPress={this.addAllCards} shape="pill" fitText={true} />
        }
    }

    toggleExpansionHelp = () => {
        this.setState({showExpansionHelp: !this.state.showExpansionHelp});
    }

    triggerChange = (expansions) => {
        if (this.props.campaign === "PotBloom") {
            expansions["flower-knight"] = "cards";
        }

        this.setState({expansions});
        this.props.onChange(expansions);
    }

    getExpansionTitle = (id) => {
        if(this.props.expansionConfig && this.props.expansionConfig[id]) {
            return this.props.expansionConfig[id];
        }

        return getExpansionTitle(id);
    }

    getAllExpansionButton = (expansion, colors) => {
        let color = this.state.expansions[expansion] === "all" ? colors.HIGHLIGHT : colors.GRAY;

        return <Button
            shape="pill"
            title={t("All Content")}
            onPress={this.toggleExpansion(expansion, "all")}
            color={color}
            textStyle={{paddingLeft: 2, paddingRight: 3}}
            // skeleton={true}
        />
    }

    getCardsExpansionButton = (expansion, colors) => {
        let color = this.state.expansions[expansion] === "cards" ? colors.HIGHLIGHT : colors.GRAY;

        return <Button
            shape="pill"
            title={t("Cards Only")}
            onPress={this.toggleExpansion(expansion, "cards")}
            color={color}
            textStyle={{paddingLeft: 2, paddingRight: 3}}
            // skeleton={true}
        />
    }

    renderExpansionList = (colors) => {
        let expansions = this.getExpansionList();

        return <View style={{padding: 5}}>
            <View style={[sharedStyles.rowCentered, sharedStyles.container]}>
                <View style={sharedStyles.flex40}>
                    <Text style={sharedStyles.itemText}>  <Text style={{fontWeight: "bold"}}>{this.props.title ? this.props.title : t("Add Expansion")}</Text></Text>
                </View>
                <View style={[sharedStyles.centeredItems, {flex: .3}]}>
                    {this.getAllContentButton()}
                </View>
                <View style={[sharedStyles.centeredItems, {flex: .3}]}>
                    {this.getAllCardsButton()}
                </View>
            </View>
            <Hr />
            {expansions.map((expansion, i) => {
                if(this.props.campaign === "PotBloom" && expansion === "flower-knight") {
                    return null;
                }

                return <View key={i}>
                    <View style={[sharedStyles.rowCentered, sharedStyles.container]}>
                        <View style={sharedStyles.flex40}>
                            <Text style={sharedStyles.itemText}>{this.getExpansionTitle(expansion)}</Text>
                        </View>
                        <View style={[sharedStyles.centeredItems, {flex: .3}]}>
                            {this.getAllExpansionButton(expansion, colors)}
                        </View>
                        {expansion !== "green-knight" && <View style={[sharedStyles.centeredItems, {flex: .3}]}>
                            {this.getCardsExpansionButton(expansion, colors)}
                        </View>}
                    </View>
                    {i < expansions.length - 1 && <Hr />}
                </View>})}
        </View>
    }
}

ExpansionSelector.propTypes = {
    title: PropTypes.string,
    sectionTitle: PropTypes.string,
    expansionConfig: PropTypes.object,
    allowEmptySelection: PropTypes.bool,
}

ExpansionSelector.defaultProps = {
    allowEmptySelection: false,
}

export default ExpansionSelector;
