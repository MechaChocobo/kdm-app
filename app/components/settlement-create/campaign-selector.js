'use strict';

import React, { Component } from 'react';

var {
    View,
} = require('react-native');

import {ToggleList} from '../shared/toggle-list';

import Text from '../shared/app-text';
import sharedStyles from '../../style/styles';
import { getCampaignList, getCampaignConfig } from '../../helpers/helpers';
import { t } from '../../helpers/intl';
import ColorContext from '../../context/ColorContext';

class CampaignSelector extends Component {
    constructor(props) {
        super(props);
        let campaignList = getCampaignConfig();
        for(let campaign in campaignList) {
            campaignList[campaign].title = t(campaignList[campaign].title);
        }
        this.state = { campaign: props.campaign, campaignList};
    }

    render() {
        return (
            <ColorContext.Consumer>
                {(colors) =>
                    <View style={sharedStyles.wrapper}>
                        <Text style={sharedStyles.title}>{t("Campaign")}</Text>
                        <ToggleList
                            items={getCampaignList()}
                            config={this.state.campaignList}
                            selected={this.state.campaign}
                            onSelect={this.onSelect}
                            onDeselect={this.onDeselect}
                        />
                    </View>
                }
            </ColorContext.Consumer>
        )
    }

    onSelect = (campaign) => {
        return () => {
            this.setState({campaign});
            this.props.onChange(campaign);
        }
    }

    onDeselect = () => {}
}

export default CampaignSelector;
