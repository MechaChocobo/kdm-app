'use strict';

import React, { PureComponent } from 'react';
import {
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import AppText from '../components/shared/app-text';
import styles from '../style/styles';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import BGBanner from './shared/BGBanner';
import { getGlobalMilestones } from '../selectors/milestones';
import Checkbox from './shared/Checkbox';
import Hr from './shared/Hr';
import { milestoneActionWrapper } from '../actions/milestones';
import { IncrementorHidden } from './shared/IncrementorHidden';
import Icon from './shared/Icon';
import LightModal from './shared/LightModal';

class GlobalMilestones extends PureComponent {
    render() {
        return (
            <ScrollView style={styles.container}>
                <BGBanner>
                    <AppText style={styles.pageHeader} color={this.props.colors.WHITE}>{t("Global Milestones")}</AppText>
                </BGBanner>
                <View style={{height: 20}}/>
                {this.renderMilestones()}
            </ScrollView>
        );
    }

    renderMilestones = () => {
        const milestones = Object.values(this.props.milestones).sort((a, b) => {return (a.complete && !b.complete ? 1 : (!a.complete && b.complete ? -1 : 0))})
        if(milestones.length === 0) {
            return <AppText style={[styles.itemText, styles.italic, styles.width90, styles.centered]}>{t("No global milestones have been created yet.  Create your own milestones in Custom Expansions!")}</AppText>
        }

        return milestones.map((milestone, i) => {
            if(!this.props.milestoneConfig[milestone.id]) {
                return null;
            }

            const conditions = milestone.conditions ? milestone.conditions : {};
            const conditionComponents = [];
            for(let id in conditions) {
                conditionComponents.push(<Condition
                    key={id}
                    value={conditions[id].value}
                    isComplete={conditions[id].complete}
                    id={id}
                    milestoneId={milestone.id}
                    milestoneConfig={this.props.milestoneConfig[milestone.id].conditions[id]}
                    onChange={this.onConditionValueChange}
                    colors={this.props.colors}
                />);
            }

            const settlementConditions = milestone.settlementConditions ? milestone.settlementConditions : {};
            const settlementComponents = [];
            for(let id in settlementConditions) {
                settlementComponents.push(<SettlementCondition
                    key={id}
                    id={id}
                    settlementProgress={settlementConditions[id].settlements}
                    settlements={this.getSettlements(Object.keys(settlementConditions[id].settlements))}
                    isComplete={settlementConditions[id].complete}
                    config={this.props.milestoneConfig[milestone.id].settlementConditions}
                    colors={this.props.colors}
                />);
            }

            return <View key={i}>
                <View style={[styles.width90, styles.centered]}>
                    <TouchableOpacity style={styles.rowCentered} onPress={this.toggleMilestone(milestone.id)}>
                        <Checkbox checked={milestone.complete} />
                        <AppText style={styles.smallValue}>{this.props.milestoneConfig[milestone.id].title}</AppText>
                    </TouchableOpacity>
                    {(conditionComponents.length > 0 || settlementComponents.length > 0) && <View style={{paddingLeft: 30}}>
                        {conditionComponents}
                        {settlementComponents}
                    </View>}
                </View>
                <Hr />
            </View>
        });
    }

    getSettlements = (ids) => {
        let settlements = {};

        for(let i = 0; i < ids.length; i++) {
            settlements[ids[i]] = this.props.settlements[ids[i]];
        }

        return settlements;
    }

    onConditionValueChange = (milestoneId, id, value, target) => {
        this.props.milestoneActionWrapper('setConditionValue', [milestoneId, id, value, target]);
    }

    toggleMilestone = (id) => {
        return () => {
            this.props.milestoneActionWrapper('setGlobalMilestoneComplete', [id, !this.props.milestones[id].complete]);
        }
    }
};

function mapStateToProps(state, props) {
    return {
        milestones: state.milestones,
        milestoneConfig: getGlobalMilestones(state),
        settlements: state.settlementReducer.settlements,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({milestoneActionWrapper}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(GlobalMilestones);

export class Condition extends PureComponent {
    render() {
        if(this.props.milestoneConfig.value === 1 || !this.props.milestoneConfig.value) {
            return <TouchableOpacity style={[styles.rowCentered, {minHeight: 30}]} onPress={this.toggleCondition}>
                <Checkbox checked={this.props.isComplete}/>
                {this.renderGlobalIcon()}
                {this.getTitle()}
            </TouchableOpacity>
        } else {
            return <View style={[styles.rowCentered, {minHeight: 30}]}>
                <Checkbox checked={this.props.isComplete} />
                {this.renderGlobalIcon()}
                {this.getTitle()}
                <IncrementorHidden
                    value={this.props.value}
                    onIncrease={this.increaseCondition}
                    onDecrease={this.decreaseCondition}
                    smallValue={true}
                />
                <View style={[styles.row, {paddingTop: 2}]}>
                    <AppText style={styles.smallValue}>/</AppText><AppText style={[styles.smallValue, {paddingLeft: 4}]}>{this.props.milestoneConfig.value}</AppText>
                </View>
            </View>
        }
    }

    getTitle = () => {
        return <View style={{paddingLeft: 10, paddingRight: 10}}>
            <AppText style={styles.itemText}>{t(this.props.milestoneConfig.description)}{this.props.milestoneConfig.value > 1 ? ' -' : ''}</AppText>
        </View>
    }

    renderGlobalIcon = () => {
        if(this.props.global) {
            return <Icon name="sk-globe" size={20} color={this.props.colors.TEXT} />
        }

        return null;
    }

    toggleCondition = () => {
        this.props.onChange(this.props.milestoneId, this.props.id, this.props.value === 0 ? 1 : 0, this.props.milestoneConfig.value);
    }

    increaseCondition = () => {
        if(this.props.milestoneConfig.value > this.props.value) {
            this.props.onChange(this.props.milestoneId, this.props.id, this.props.value + 1, this.props.milestoneConfig.value);
        }
    }

    decreaseCondition = () => {
        if(this.props.value > 0) {
            this.props.onChange(this.props.milestoneId, this.props.id, this.props.value - 1, this.props.milestoneConfig.value);
        }
    }
}

class SettlementCondition extends PureComponent {
    state = {
        popup: false,
    }

    render() {
        return <View>
            <TouchableOpacity style={[styles.rowCentered, {height: 30}]} onPress={this.showPopup}>
                <Checkbox checked={this.props.isComplete}/>
                <Icon name="sk-house" color={this.props.colors.HIGHLIGHT} size={20}/>
                <AppText style={styles.itemText}>{this.props.config[this.props.id].description}</AppText>
            </TouchableOpacity>
            {this.state.popup && <LightModal
                visible={this.state.popup}
                onDismissed={this.hidePopup}
            >
                <AppText style={styles.header}>{this.props.config[this.props.id].description}</AppText>
                <Hr />
                <View style={{alignContent: "flex-start"}}>
                    {this.renderSettlementProgress()}
                </View>
            </LightModal>}
        </View>
    }

    renderSettlementProgress = () => {
        let settlements = [];

        for(let id in this.props.settlementProgress) {
            settlements.push(<View key={id} style={styles.rowCentered}>
                <Checkbox checked={this.props.settlementProgress[id].complete}/>
                <AppText style={styles.itemText}>{this.props.settlements[id].name}: {this.props.settlementProgress[id].value} / {this.props.config[this.props.id].value}</AppText>
            </View>
            );
        }

        return settlements;
    }

    showPopup = () => {
        this.setState({popup: true});
    }

    hidePopup = () => {
        this.setState({popup: false});
    }
}
