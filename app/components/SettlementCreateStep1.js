'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    View,
    TouchableOpacity,
    ScrollView,
} from 'react-native';

import CampaignSelector from './settlement-create/campaign-selector';
import BGBanner from '../components/shared/BGBanner';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Text from '../components/shared/app-text';
import AppTextInput from '../components/shared/AppTextInput';
import Button from '../components/shared/button';
import sharedStyles from '../style/styles';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import { CHECKBOX_SIZE } from '../helpers/helpers';
import { setSettlementCreateName, setSettlementCreateCampaign, setSettlementCreateSurvivors } from '../actions/settlementCreate';

const NAME_SERVICE_URL = "https://www.mithrilandmages.com/utilities/CityNamesServer.php?count=1&dataset=historic";

class SettlementCreateStep1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nameFetchError: "",
            nameLoading: false,
        };
    }

    getRandomName = () => {
        this.setState({nameLoading: true});
        fetch(NAME_SERVICE_URL, {headers: {'Cache-Control': 'no-cache'}}).then((response) => {
            return response.text();
        }).then((response) => {
            this.onNameChange(response);
            this.setState({nameLoading: false});
        }).catch((error) => {
            this.setState({nameFetchError: error, nameLoading: false});
        }) ;
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <ScrollView style={sharedStyles.container}>
                    <BGBanner height={150}>
                        <Text style={sharedStyles.pageHeader} color={this.props.colors.WHITE}>{t("New Settlement")}</Text>
                    </BGBanner>
                    <View style={[sharedStyles.contentWrapper, {marginTop: 15}]}>
                        <AppTextInput
                            inputStyle={{fontSize: 30, fontWeight: "600"}}
                            value={this.props.name}
                            onChangeText={this.onNameChange}
                            textAlign="center"
                            placeholder={t("Enter Name")}
                        />
                        <View style={{width: "75%", alignSelf: "center", marginTop: 15}}>
                            <Button
                                title={t("Random Name")}
                                shape="rounded"
                                onPress={this.getRandomName}
                                color={this.props.colors.HIGHLIGHT}
                                loading={this.state.nameLoading}
                            />
                        </View>
                    </View>
                    <View style={sharedStyles.wrapper}>
                        <Text style={sharedStyles.title}>{t("Options")}</Text>
                        <View style={sharedStyles.contentWrapper}>
                            <TouchableOpacity onPress={this.toggleCreateSurvivors}>
                                <View style={sharedStyles.rowCentered}>
                                    <Icon name={this.props.createSurvivors ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                                    <Text style={[sharedStyles.itemText, {marginLeft: 4}]}>{t("Also create 4 starting survivors")}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <CampaignSelector onChange={this.onCampaignChange} campaign={this.props.campaign}/>
                </ScrollView>
                <View style={{width: "92%", alignSelf: "center", marginBottom: 20, marginTop: 20}}>
                    <Button
                        title={t("Next")}
                        onPress={this.goToStep2}
                        color={this.props.colors.HIGHLIGHT}
                        disabled={this.props.name === ""}
                        shape="rounded"
                    />
                </View>
            </View>
        );
    }

    onNameChange = (name) => {
        this.props.setSettlementCreateName(name);
    }

    goToStep2 = () => {
        this.props.onPageChange('step2');
    }

    toggleCreateSurvivors = () => {
        this.props.setSettlementCreateSurvivors(!this.props.createSurvivors)
    }

    onCampaignChange = (campaign) => {
        this.props.setSettlementCreateCampaign(campaign);
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
        name: state.settlementCreate.name,
        campaign: state.settlementCreate.campaign,
        createSurvivors: state.settlementCreate.createSurvivors,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        setSettlementCreateName,
        setSettlementCreateCampaign,
        setSettlementCreateSurvivors,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementCreateStep1);
