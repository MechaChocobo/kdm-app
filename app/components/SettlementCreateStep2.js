'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
    View,
    ScrollView,
} from 'react-native';

import {addCustomExpansionsToSettlement} from '../actions/settlement';
import ExpansionSelector from './settlement-create/expansion-selector';
import BGBanner from '../components/shared/BGBanner';
import Text from '../components/shared/app-text';
import Button from '../components/shared/button';
import sharedStyles from '../style/styles';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import MultiPageBackButton from './shared/MultiPageBackButton';
import { setSettlementCreateExpansions, setSettlementCreateCustomExpansions, launchSettlementCreate } from '../actions/settlementCreate';
import MenuButton from './shared/MenuButton';
import { resetListContext } from '../helpers/recycler-context';

class SettlementCreateStep2 extends Component {
    render() {
        return (
            <View style={sharedStyles.container}>
                <ScrollView style={sharedStyles.container}>
                    <BGBanner height={150} showButtons={false}>
                        <MultiPageBackButton onBack={this.props.onBack} color={this.props.colors.WHITE}/>
                        <MenuButton color={this.props.colors.WHITE}/>
                        <Text style={sharedStyles.pageHeader} color={this.props.colors.WHITE}>{this.props.name}</Text>
                    </BGBanner>
                    <ExpansionSelector
                        onChange={this.onExpansionChange}
                        expansions={this.props.expansions}
                        campaign={this.props.campaign}
                    />
                    {this.renderCustomExpansionSelector()}
                </ScrollView>
                <View style={{width: "92%", alignSelf: "center", marginBottom: 20, marginTop: 20}}>
                    <Button
                        title={t("Create")}
                        onPress={this.onCreate}
                        color={this.props.colors.HIGHLIGHT}
                        shape="rounded"
                    />
                </View>
            </View>
        );
    }

    onCreate = () => {
        this.props.launchSettlementCreate();
        resetListContext();
    }

    renderCustomExpansionSelector = () => {
        let list = Object.keys(this.props.customExpansions);

        if(list.length === 0) {
            return null;
        }

        let config = {};
        for(let i = 0; i < list.length; i++) {
            config[list[i]] = this.props.customExpansions[list[i]].name;
        }

        return <ExpansionSelector
            sectionTitle={t("Custom Expansions")}
            title={t("Add Expansions")}
            onChange={this.onCustomExpansionChange}
            expansions={this.props.selectedCustomExpansions}
            expansionList={list}
            expansionConfig={config}
        />
    }

    onCustomExpansionChange = (selected) => {
        this.props.setSettlementCreateCustomExpansions(selected);
    }

    onExpansionChange = (expansions) => {
        this.props.setSettlementCreateExpansions(expansions);
    }
}

function mapStateToProps(state, props) {
    return {
        customExpansions: state.customExpansions,
        colors: getColors(state),
        expansions: state.settlementCreate.expansions,
        campaign: state.settlementCreate.campaign,
        selectedCustomExpansions: state.settlementCreate.selectedCustomExpansions,
        name: state.settlementCreate.name,
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        addCustomExpansionsToSettlement,
        setSettlementCreateExpansions,
        setSettlementCreateCustomExpansions,
        launchSettlementCreate,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementCreateStep2);
