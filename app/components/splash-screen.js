'use strict';

import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import DeviceInfo from 'react-native-device-info';

import {
    Image,
    View,
    Text,
    Dimensions,
    StatusBar,
    Platform,
} from 'react-native';

import themes from '../style/themes';
let packageInfo = require('../../package.json');

const IMAGE_WIDTH_RATIO = 6.63594470046083;
const IMAGE_HEIGHT_MULTIPLIER = 0.467741935483871;

const fontFamily = Platform.select({
    ios: "Helvetica",
    android: ""
});

export default class SplashScreen extends Component {
    interval = null;
    constructor(props) {
        super(props);

        this.state = {
            height: Dimensions.get('window').height,
            width: Dimensions.get('window').width,
            gradientCoord: 1,
            isTablet: DeviceInfo.isTablet(),
        }
    }

    componentDidMount() {
        this.startBackgroundColorAnimation();
        setTimeout(() => {
            clearInterval(this.interval);
        }, 2200);
    }

    startBackgroundColorAnimation = () => {
        this.interval = setInterval(() => {
            if(this.state.gradientCoord <= .02) {
                clearInterval(this.interval);
            } else {
                this.setState({gradientCoord: this.state.gradientCoord - .02});
            }
        }, 40);
    }

    getBackgroundImages = () => {
        let images = [];

        let imageWidth = this.state.width / IMAGE_WIDTH_RATIO;
        let imageHeight = imageWidth * IMAGE_HEIGHT_MULTIPLIER;

        let numRows = Math.floor((this.state.height / imageHeight) / 2);

        let imageMargin = Math.ceil((this.state.width - (imageWidth * 5)) / 5 / 2);

        for(let j = 0; j < numRows; j++) {
            let row = [];
            let numCols = j % 2 === 0 ? 5 : 6;
            for(let i = 0; i < numCols; i++){
                row.push((
                   <Image
                        source={require('../images/icon-book.png')}
                        style={{height: imageHeight, width: imageWidth, margin: imageMargin}}
                        key={'' + i + j}
                    />
                ))
            }
            images.push(row);
        }

        return images;
    }

    getMarginMultiplier = () => {
        if(this.state.isTablet) {
            return 1;
        }

        return 0;
    }

    render() {
        return (
            <LinearGradient
                style={{flex: 1, justifyContent: "space-between"}}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={[themes.dark.GRADIENT_START_BACKGROUND, themes.dark.GRADIENT_END_BACKGROUND]}
            >
                <View style={{flex: 1, opacity: .1}}>
                    {
                        this.getBackgroundImages().map((row, i) => {
                            return <View style={{
                                flex:1,
                                flexDirection:'row',
                                left: (i % 2 === 0 ? 0 :( 0 - (this.state.width / IMAGE_WIDTH_RATIO / 2) - 10))}}
                                key={i}
                                >
                                {row.map((img, j) => {
                                    return img;
                                })}
                            </View>
                        })
                    }
                </View>
                <View style={{position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, flex: 1}}>
                    <StatusBar
                        backgroundColor={themes.dark.GRADIENT_START_BACKGROUND}
                        barStyle="light-content"
                    />
                    <View style={{flex: 1}}>
                        <View style={{flex: .7}}>
                            <LinearGradient
                                start={{x: 1, y: 1}} end={{x: this.state.gradientCoord, y:this.state.gradientCoord}}
                                colors={[themes.dark.GRADIENT_END, themes.dark.GRADIENT_START]}
                                style={{
                                    flex: 1,
                                    padding: 10,
                                    marginTop: (this.state.height * -.2 * this.getMarginMultiplier()),
                                    justifyContent: "center",
                                    left: 0 - (this.state.width / 2),
                                    width: this.state.width * 2,
                                    borderBottomLeftRadius: this.state.width,
                                    borderBottomRightRadius: this.state.width,
                                }}
                            >
                                <View style={{flex: .7, marginTop: (this.state.height * .2 * this.getMarginMultiplier())}}>
                                    <Image
                                        style={{flex:1, height: undefined, width: undefined}}
                                        source={require('../images/icon.png')}
                                        resizeMode="contain"
                                        />
                                </View>
                            </LinearGradient>
                        </View>
                        <View style={{flex: .4, alignItems: "center"}}>
                            <Text style={{fontFamily, fontSize: 40, fontWeight: "bold", color: "white", marginTop: "5%"}}>Scribe</Text>
                            <Text style={{fontFamily, fontSize: 20, fontWeight: "300", color: "white"}}>for KD:M</Text>
                            <View style={{flex: .4}} />
                            <Text style={{fontFamily, fontSize: 20, color: "white"}}>A Kingdom Death: Monster</Text>
                            <Text style={{fontFamily, fontSize: 20, color: "white"}}>Campaign Management App</Text>
                            <View style={{flex: .5}} />
                            <Text style={{fontFamily, fontSize: 20, fontWeight: "bold", color: "white"}}>Version {packageInfo.version}</Text>
                        </View>
                    </View>
                </View>
            </LinearGradient>
        )
    }
}
