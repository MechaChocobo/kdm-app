'use strict';

import React, { Component } from 'react';
import {
    View,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import Text from './shared/app-text';
import sharedStyles from '../style/styles';
import ExpansionList from './custom-expansions/expansion-list';
import ImportExport from './custom-expansions/import-export';
import { resetNavigationStack } from '../actions/router';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import BGBanner from './shared/BGBanner';
import ImportRemote from './custom-expansions/ImportRemote';
import { setExpansionList, setExpansionListLoading } from '../actions/expansion-list';
import SimpleToast from 'react-native-simple-toast';

const EXPANSION_LIST_URL = "https://pastebin.com/raw/1GLj7qdM";

class CustomExpansions extends Component {
    componentDidMount() {
        this.props.resetNavigationStack();
        this.loadExpansionList();
    }

    loadExpansionList = () => {
        this.props.setExpansionListLoading(true);
        fetch(EXPANSION_LIST_URL, {headers: {'Cache-Control': 'no-cache'}}).then((response) => {
            return response.text();
        }).then((response) => {
            try {
                const list = JSON.parse(response);
                this.props.setExpansionList(list);
            } catch(e) {
                SimpleToast.show(t("Error processing downloaded community expansion list"), SimpleToast.LONG);
            }
        }).catch((error) => {
            SimpleToast.show(t("Error downloading community expansion list: %error%", {error}), SimpleToast.LONG);
        }).finally(() => {
            this.props.setExpansionListLoading(false);
        });
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <BGBanner>
                    <Text style={sharedStyles.pageHeader} color={this.props.colors.WHITE}>{t("Custom Expansions")}</Text>
                </BGBanner>
                <ScrollableTabView
                    locked={true}
                    tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                    tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                    tabBarInactiveTextColor={this.props.colors.TEXT}
                >
                    <ExpansionList tabLabel={t("My Expansions")} onRefresh={this.loadExpansionList} />
                    <ImportRemote tabLabel={t("Community")} onRefresh={this.loadExpansionList} />
                    <ImportExport tabLabel={t("Import/Export")}/>
                </ScrollableTabView>
            </View>
        );
    }
};

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({resetNavigationStack, setExpansionList, setExpansionListLoading}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomExpansions);
