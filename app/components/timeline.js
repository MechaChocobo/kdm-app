'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { timelineActionWrapper } from '../actions/timeline';
import { settlementActionWrapper } from '../actions/settlement';
import { RecyclerListView, DataProvider, LayoutProvider } from "recyclerlistview";

import {
    View,
    Dimensions,
} from 'react-native';

import RoundedIcon from './shared/rounded-icon';
import AppText from './shared/app-text';
import Button from './shared/button';
import BGBanner from './shared/BGBanner';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import TimelineListItem, { TEXT_EVENT_HEIGHT, MIN_EVENTS_NUM } from './TimelineListItem';
import { goToTimelineLy } from '../actions/router';
import { getRandomInt } from '../helpers/helpers';
import TimelineActionLog from './TimelineActionLog';
import styles from '../style/styles';

const BASE_ITEM_HEIGHT = 57;

class Timeline extends Component {
    constructor(props) {
        super(props);

        let dataProvider = new DataProvider((r1, r2) => {
            return r1 != r2;
        }).cloneWithRows([...this.props.timeline, "addYearButton"]);

        let { width } = Dimensions.get("window");

        this.layoutProvider = new LayoutProvider((index) => {
            return index;
        },
        (type, dim, index) => {
            dim.width = width;
            dim.height = this.calculateItemHeight(index);
        });

        this.state = {
            dataProvider: dataProvider,
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.timeline !== this.props.timeline || prevProps.currentYear !== this.props.currentYear) {
            this.setState({
                dataProvider: new DataProvider((r1, r2) => {
                    return r1 != r2;
                }).cloneWithRows([...this.props.timeline, "addYearButton"]),
            });
        }
    }

    calculateItemHeight = (index) => {
        let ly = this.props.timeline[index];
        let count = 0;
        for(let type in ly) {
            count += ly[type].length;
        }

        count = Math.max(MIN_EVENTS_NUM, count);

        return BASE_ITEM_HEIGHT + (count * TEXT_EVENT_HEIGHT);
    }

    endYear = () => {
        this.props.settlementActionWrapper('endYear', [this.props.settlementId]);
    }

    unendYear = () => {
        this.props.settlementActionWrapper('unendYear', [this.props.settlementId]);
    }

    addYear = () => {
        this.props.timelineActionWrapper("addYear", [this.props.settlementId]);
    }

    renderLy = (index, item) => {
        let LY = item;

        if(LY === "addYearButton") {
            return (
                <View style={[styles.rowCentered, {alignSelf: "flex-end", marginRight: "2%", paddingRight: 5, paddingBottom: "2%", marginTop: 10}]}>
                    <RoundedIcon icon="playlist-plus" onPress={this.addYear} gradient={true} size={30}/>
                    <View style={{width: 15}}/>
                    <TimelineActionLog id={this.props.settlementId}/>
                </View>
            )
        }

        return <TimelineListItem
            events={LY}
            ly={index}
            isActiveLy={index === this.props.currentYear}
            goToLy={this.goToLy}
            isPassed={index < this.props.currentYear}
            onAddRandomSettlementEvent={this.onAddRandomSettlementEvent}
        />
    }

    goToLy = (ly) => {
        this.props.goToTimelineLy(ly);
    }

    onAddRandomSettlementEvent = (ly) => {
        let events = this.props.settlementEvents.filter(event => event !== "firstDay");
        let event = events[getRandomInt(events.length)];
        this.props.timelineActionWrapper('addToTimeline', [this.props.settlementId, ly, "settlementEvents", event]);
    }

    render() {
        return (
            <View style={styles.container}>
                <BGBanner>
                    <View style={[styles.spaceBetween, styles.container, styles.width100]}>
                        <AppText style={styles.pageHeader} color={this.props.colors.WHITE}>{this.props.settlementName}</AppText>
                        <View style={[styles.rowCentered, {marginBottom: 20}]}>
                            <View style={styles.flex50}>
                                <Button
                                    title={t("Previous Year")}
                                    onPress={this.unendYear}
                                    shape="pill"
                                    fitText={true}
                                    style={styles.centered}
                                    disabled={this.props.currentYear === 0}
                                    skeleton={true}
                                    color="white"
                                />
                            </View>
                            <View style={styles.flex50}>
                                <Button
                                    title={t("Advance Year")}
                                    onPress={this.endYear}
                                    shape="pill"
                                    fitText={true}
                                    style={styles.centered}
                                    disabled={this.props.currentYear === this.props.timeline.length - 1}
                                    skeleton={true}
                                    color="white"
                                />
                            </View>
                        </View>
                    </View>
                </BGBanner>
                <RecyclerListView
                    layoutProvider={this.layoutProvider}
                    dataProvider={this.state.dataProvider}
                    rowRenderer={this.renderLy}
                    initialRenderIndex={this.props.currentYear}
                    extendedState={{currentYear: this.props.currentYear}}
                />
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return {
        settlementId: settlementId,
        timeline: state.timelineReducer[settlementId],
        currentYear: state.settlementReducer.settlements[settlementId].ly,
        settlementEvents: state.settlementReducer.settlements[settlementId].allSettlementEvents.sort(),
        settlementName: state.settlementReducer.settlements[settlementId].name,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ timelineActionWrapper, settlementActionWrapper, goToTimelineLy }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Timeline);
