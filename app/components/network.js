'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Toast from 'react-native-simple-toast';
import DeviceInfo from 'react-native-device-info';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    View,
    TouchableOpacity,
    Platform,
    PermissionsAndroid,
    AppState,
} from 'react-native';

import Collapsible from 'react-native-collapsible';
import { NetworkInfo } from 'react-native-network-info';
import QRCodeScanner from 'react-native-qrcode-scanner';
import TCP from 'react-native-tcp';
import * as Progress from 'react-native-progress';
import KeepAwake from 'react-native-keep-awake';
import QRCode from 'react-native-qrcode-svg';

import {startServer, stopServer, setupSocket, sendServerPacket, sendClientPacket, clientStart, clientStop, setShareId, loadingUp, loadingDown, setReconnectFunction, setReconnectLockout, setHostIpAddress} from '../actions/network';
import {doSettlementHostAction, settlementActionWrapper, importSettlement} from '../actions/settlement';
import {survivorActionWrapper, doSurvivorHostAction, importSurvivors} from '../actions/survivors';
import {timelineActionWrapper, doTimelineHostAction, importTimeline} from '../actions/timeline';
import {goToSettlement} from '../actions/router';
import { setIsSubscriber, setShouldShowAds } from '../actions/purchases';
import { t } from '../helpers/intl';
import { setCamera, setDefaultIpAddress } from '../actions/settings';
import { importCustomExpansions } from '../actions/custom-expansions';
import Text from '../components/shared/app-text';
import Button from '../components/shared/button';
import Popup from '../components/shared/popup';
import Hr from '../components/shared/Hr';
import SubHeader from '../components/shared/sub-header';
import sharedStyles from '../style/styles';
import { getColors } from '../selectors/general.js';
import { CHECKBOX_SIZE } from '../helpers/helpers';
import AppTextInput from './shared/AppTextInput';
import BackMenu from './shared/BackMenu';
import { isDisconnectedClient } from '../selectors/network';
import { milestoneActionWrapper, importMilestones, doMilestoneHostAction } from '../actions/milestones';

let packageInfo = require('../../package.json');

const PORT = 55666;
const MAX_RECONNECT_ATTEMPTS = 5;

class Router extends Component {
    ipInput = null;
    defaultInput = null;
    appState = "active";
    prevShareId = null;

    constructor(props) {
        super(props);

        let myIpAddress = '';

        if(this.props.defaultIpAddress) {
            myIpAddress = this.props.defaultIpAddress;
        }

        this.state = {
            myIpAddress,
            showHosting: false,
            showJoining: false,
            showScanner: false,
            showHostConfig: false,
            selectedSettlement: null,
            splitData: {},
            dataToSend: [],
            loading: false,
            dataSendStatus: {},
            redirectOnLoadComplete: true,
            loadingTimeouts: {},
            disconnecting: false,
            messages: [],
            joiningTimeout: null,
            showSettings: false,
            reconnectAttempts: 0,
        };
    }


    componentDidMount = () => {
        this.props.onRef(this);

        NetworkInfo.getIPV4Address(ip => {
            if(this.state.myIpAddress && this.ipMatches(this.state.myIpAddress)) {
                return;
            } else {
                this.setState({myIpAddress: ip});
            }
        });

        if(this.props.isHost) {
            this.setState({showHosting: true});
        }

        if(this.props.isClient) {
            this.setState({showJoining: true, showScanner: false});
        }

        this.props.setReconnectFunction(this.reconnectClient);

        if(!this.props.camera) {
            this.props.setCamera("back");
        }

        DeviceInfo.getIPAddress().then(ip => {
            if(this.state.myIpAddress && this.ipMatches(this.state.myIpAddress)) {
                return;
            } else {
                this.setState({myIpAddress: ip});
            }
        });

        if(this.props.autoHostSettlement) {
            let hostAttemptCount = 0;
            if(!this.state.myIpAddress) {
                const interval = setInterval(() => {
                    hostAttemptCount++;
                    if(hostAttemptCount > 6) {
                        clearInterval(interval);
                        Toast.show(t("Error getting IP address, auto-hosting not started"));
                        return;
                    }

                    if(!this.state.myIpAddress) {
                        return;
                    } else {
                        clearInterval(interval);
                        this.startAutoHost();
                    }
                }, 200);
            } else {
                this.startAutoHost();
            }
        }
    }

    startAutoHost = () => {
        if(!this.props.autoHostSettlement || !this.props.settlements[this.props.autoHostSettlement]) {
            return;
        }

        this.props.setShareId(this.props.autoHostSettlement).then(() => {
            this.setState({showHosting: true});
            this.startServer();
            Toast.show(t("Auto started hosting settlement %name%", {name: this.props.settlements[this.props.autoHostSettlement].name}), Toast.LONG);
        });
    }

    componentWillUnmount() {
        this.props.onRef(undefined)
    }

    ipMatches(ip) {
        let valid = true;
        if(!ip.match(/192\.168\..*/)) {
            valid = false;
        } else {
            if(ip >= "192.0.0.0" && ip < "192.0.0.8") {
                valid = false;
            }
        }

        return valid;
    }

    hostGame = () => {
        if(this.props.selectedSettlement === null) {
            return;
        }

        this.setState({showHosting: true, showHostConfig: false});
        this.startServer();
    }

    joinGame = () => {
        this.setState({showJoining: true, showScanner: true});
    }

    joinGameWrapper = () => {
        if(Platform.OS === "android") {
            this.requestCameraPermission();
        } else {
            this.joinGame();
        }
    }

    async requestCameraPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.CAMERA,
                {
                    'title': t('Scribe Camera Permission'),
                    'message': t('Scribe needs access to your camera in order to scan QR codes and join multiplayer games')
                }
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED || granted === true) {
                this.joinGame();
            } else {
                Toast.show(t("You must grant camera access in order to join multiplayer games"), Toast.LONG);
            }
        } catch (err) {
            Toast.show(t("Error requesting camera permission"), Toast.LONG);
        }
    }

    stopJoining = () => {
        this.props.clientStop();
        AppState.removeEventListener('change', this.handleClientStateChange);
        this.setState({showJoining: false, showScanner: false, disconnecting: true, messages: [], loading: false});
    }

    startServer = () => {
        if(Platform.OS === "ios") {
            AppState.addEventListener('change', this.handleIOSServerStateChange);
            this.prevShareId = this.props.shareId;
        }

        if(this.props.isHost) {
            return;
        }

        KeepAwake.activate();

        let server = TCP.createServer((socket) => {
            socket.on('data', (data) => {
                try {
                    data = JSON.parse(data.toString('ascii'));
                } catch (e) { // ignore if not JSON
                    return;
                }

                if(data.subStatus) {
                    if(data.subStatus === "sub") {
                        this.props.setIsSubscriber(true);
                        this.props.sendServerPacket(JSON.stringify({subStatus: "sub"}));
                    } else if(data.subStatus === "noads") {
                        this.props.setShouldShowAds(false);
                        this.props.sendServerPacket(JSON.stringify({subStatus: "noads"}));
                    }
                }

                if(data.message && data.message === "version-check") {
                    socket.write(JSON.stringify({version: packageInfo.version}));
                    return;
                }

                if(data.message && data.message == "client-connect") {
                    if(this.props.isSubscriber === true) {
                        socket.write(JSON.stringify({subStatus: "sub"}));
                    } else if(this.props.shouldShowAds === false) {
                        socket.write(JSON.stringify({subStatus: "noads"}));
                    }

                    let timeouts = this.state.loadingTimeouts;
                    this.props.setupSocket(socket);
                    this.props.loadingDown();
                    clearTimeout(timeouts[data.id]);
                    delete timeouts[data.id];
                    this.setState({timeouts, messages: [...this.state.messages, t("Client %id% downloaded and connected successfully", {id: data.id})]});
                }

                if(data.message && data.message == "client-setup") {
                    let id = Date.now();
                    let data = JSON.stringify({
                        settlement: this.props.settlements[this.props.selectedSettlement],
                        survivors: this.props.survivors[this.props.selectedSettlement],
                        timeline: this.props.timelines[this.props.selectedSettlement],
                        customExpansions: this.getCustomExpansions(),
                        globalMilestones: this.props.globalMilestones,
                        id,
                    });
                    socket.write(new Buffer(data));
                    socket.end();
                    let timeout = setTimeout(() => {
                        let timeouts = this.state.loadingTimeouts;
                        this.props.loadingDown();
                        delete timeouts[data.id];
                        this.setState({timeouts});
                    }, 10000);
                    let timeouts = this.state.loadingTimeouts;
                    timeouts[id] = timeout;
                    this.setState({timeouts, messages: [...this.state.messages, t("Connection received from client, assigning them id %id% and streaming settlement data", {id})]});
                    this.props.loadingUp();
                    return;
                }

                if(data.reducer == "settlementReducer") {
                    this.props.settlementActionWrapper(data.action, data.args);
                } else if(data.reducer == "survivorReducer") {
                    this.props.survivorActionWrapper(data.action, data.args);
                } else if(data.reducer == "timelineReducer") {
                    this.props.timelineActionWrapper(data.action, data.args);
                } else if(data.reducer == "milestones") {
                    this.props.milestoneActionWrapper(data.action, data.args);
                }
            });

            socket.on('error', () => {
                socket.end();
            });
        });

        if(Platform.OS === "android") {
            server.listen({port: PORT});
        } else if(Platform.OS === "ios") {
            server.listen(PORT, this.state.myIpAddress);
        }

        this.props.startServer(server);
        this.props.setShareId(this.props.selectedSettlement);
        this.setState({messages: [...this.state.messages, t("Server started on port %port%, listening for connections", {port: PORT})]});
    }

    stopServer = (keepHandler = false) => {
        if(Platform.OS === "ios" && !keepHandler) {
            AppState.removeEventListener('change', this.handleIOSServerStateChange);
        }

        KeepAwake.deactivate();
        this.props.sendServerPacket({message: "disconnect"});
        this.props.stopServer();
        this.setState({showHosting: false, showHostConfig: false, messages: []});
    }

    codeScanned = (e) => {
        let data = null;

        try {
            data = JSON.parse(e.data);
        } catch(e) {
            Toast.show(t("Error reading QR code, try again"));
            return;
        }


        this.setState({showScanner: false, remoteIpAddress: data.ip});
        this.initClient(data.ip);
    }

    initClient = (ip) => {
        this.props.loadingUp();
        let client = null;

        try {
            client = TCP.createConnection({host: ip, port: PORT}, () => {
                client.write(JSON.stringify({message: "version-check"}));
            });
        } catch(e) {
            Toast.show('Error initializing client');
            return;
        }

        let chunks = [];
        let versionError = false;
        client.on('data', (data) => {
            try {
                let message = JSON.parse(data);
                if(message.version) {
                    if(message.version.replace(/\.\d+$/, '') !== packageInfo.version.replace(/\.\d+$/, '')) {
                        versionError = true;
                        Toast.show(t("The host and your app versions are incompatible (%theirVersion% vs %myVersion%). Please upgrade to the same version.", {theirVersion: message.version, myVersion: packageInfo.version}), Toast.LONG);
                        this.stopJoining();
                        this.props.loadingDown();
                        return;
                    } else {
                        client.write(JSON.stringify({message: "client-setup"}));
                        this.setState({messages: [...this.state.messages, t("Client connected to %ip%, beginning data load", {ip})]});
                    }
                }

                return;
            } catch (e) {}

            chunks.push(data);
            const message = Buffer.concat(chunks);
            // manually forces the end handler to trigger in case it doesn't on its own
            try {
                data = JSON.parse(message.toString());
                client.end();
            } catch (e) { // ignore if not JSON
            }
        });

        client.on("close", () => {
            if(versionError === true) {
                return;
            }

            const data = Buffer.concat(chunks);
            let message = null;

            try {
                message = JSON.parse(data.toString());
            } catch (e) {
                // abort and attempt reconnect
                this.setState({reconnectAttempts: this.state.reconnectAttempts + 1});
                this.props.loadingDown();
                if(this.state.reconnectAttempts < MAX_RECONNECT_ATTEMPTS) {
                    this.initClient(ip);
                } else {
                    Toast.show(t("%num% connection errors, verify internet connection and try again", {num: MAX_RECONNECT_ATTEMPTS}), Toast.LONG);
                    this.stopJoining();
                }
                return;
            }

            this.props.importSettlement(message.settlement);
            this.props.importSurvivors(message.settlement.id, message.survivors);
            this.props.importTimeline(message.settlement.id, message.timeline);
            this.props.importCustomExpansions(message.customExpansions);
            this.props.importMilestones(message.globalMilestones);
            this.props.setShareId(message.settlement.id);
            this.setState({loading: false, reconnectAttempts: 0});
            if(this.state.redirectOnLoadComplete) {
                this.props.goToSettlement(message.settlement.id);
            } else {
                this.setState({redirectOnLoadComplete: true});
            }

            this.connectClient(ip, message.id);
        });

        client.on('error', (data) => {
            this.props.clientStop(t('Error connecting to host'));
            if(this.props.loading) {
                this.props.loadingDown();
            }
            client.end();
        });

        this.setState({loading: true, messages: [...this.state.messages, t("Client created, initializing connection to %ip%", {ip})]});
    }

    connectClient = (ip, id) => {
        // remove just in case it's already bound
        AppState.removeEventListener('change', this.handleClientStateChange);
        AppState.addEventListener('change', this.handleClientStateChange);
        let client = TCP.createConnection({host: ip, port: PORT}, () => {
            client.write(JSON.stringify({message: "client-connect", id}));
            if(this.props.isSubscriber === true) {
                client.write(JSON.stringify({subStatus: "sub"}));
            } else if(this.props.shouldShowAds === false) {
                client.write(JSON.stringify({subStatus: "noads"}));
            }

            client.on('data', (data) => {
                try {
                    data = JSON.parse(data.toString());
                } catch (e) { // ignore if not JSON
                }

                if(data.subStatus && data.subStatus === "sub") {
                    this.props.setIsSubscriber(true);
                } else if(data.subStatus && data.subStatus === "noads") {
                    this.props.setShouldShowAds(false);
                }

                // disconnect fired from iOS host that's being backgrounded
                if(data.message && data.message === "disconnect") {
                    this.props.clientStop(t("Host has disconnected, reconnect once they're available again"));
                }

                if(data.reducer == "settlementReducer") {
                    this.props.doSettlementHostAction(data.action, data.args);
                } else if(data.reducer == "survivorReducer") {
                    this.props.doSurvivorHostAction(data.action, data.args);
                } else if(data.reducer == "timelineReducer") {
                    this.props.doTimelineHostAction(data.action, data.args);
                } else if(data.reducer == "milestones") {
                    this.props.doMilestoneHostAction(data.action, data.args);
                }
            });

            client.on('error', (data) => {
                this.props.clientStop(data.toString('ascii'));
                if(!this.state.disconnecting && !this.props.reconnectLockout) {
                    Toast.show(t("Network error, attempting reconnect"));
                    this.reconnectClient(false);
                }

                this.props.setReconnectLockout(true);

                setTimeout(() => {
                    this.props.setReconnectLockout(false);
                }, 30000);

                if(this.props.loading > 0) {
                    this.props.loadingDown();
                }

                this.setState({loading: false, disconnecting: false});
            });

            this.props.clientStart(client, ip);
            this.props.loadingDown();
            // sets in the global reducer for persistence
            this.props.setHostIpAddress(ip);
        });
    }

    showConfig = () => {
        this.setState({showHostConfig: true});
    };

    renderSettlementsList = () => {
        let settlements = Object.values(this.props.settlements);
        return settlements.map((settlement, i) => {
            if(settlement.archived === true) {
                return null;
            }

            return(
                <TouchableOpacity onPress={this.toggleSelectSettlement(settlement.id)} style={sharedStyles.selfFlexStart} key={i}>
                    <View style={[sharedStyles.rowCentered, {marginBottom: 10}]}>
                        <Icon name={this.props.selectedSettlement == settlement.id ? "checkbox-blank" : "checkbox-blank-outline"} size={CHECKBOX_SIZE} color={this.props.colors.TEXT}/>
                        <Text style={sharedStyles.itemText}>{settlement.name}</Text>
                    </View>
                </TouchableOpacity>
            );
        });
    }

    toggleSelectSettlement = (id) => {
        return () => {
            if(this.state.showHosting) {
                return;
            }

            if(this.props.selectedSettlement == id) {
                this.props.setShareId(null);
            } else {
                this.props.setShareId(id);
            }
        }
    }

    getCustomExpansions = () => {
        let expansions = [];

        let ids = Object.keys(this.props.settlements[this.props.selectedSettlement].customExpansions);

        if(ids.length > 0) {
            for(let i = 0; i < ids.length; i++) {
                expansions.push(this.props.customExpansions[ids[i]]);
            }
        }

        // grabs custom expansions that you may have just toggled some aspects of instead of actually adding to the settlement
        if(this.props.settlements[this.props.selectedSettlement].partialExpansions) {
            let partials = Object.keys(this.props.settlements[this.props.selectedSettlement].partialExpansions);
            for(let i = 0; i < partials.length; i++) {
                if(expansions.indexOf(partials[i]) === -1) {
                    expansions.push(partials[i]);
                }
            }
        }

        return expansions;
    }

    renderButtons = () => {
        let buttons = <View style={[sharedStyles.row, sharedStyles.spaceBetween]}>
                <View style={[sharedStyles.width40, sharedStyles.centered, {marginLeft: "5%"}]}>
                    <Button title={t("Host game")} onPress={this.showConfig} shape="rounded"/>
                </View>
                <View style={[sharedStyles.width40, sharedStyles.centered, {marginRight: "5%"}]}>
                    <Button title={t("Join game")} onPress={this.joinGameWrapper} shape="rounded" />
                </View>
            </View>
        if(this.state.showHostConfig || this.state.showHosting) {
            buttons = <View style={[sharedStyles.width90, sharedStyles.centered]}><Button title={t("Stop Hosting")} onPress={this.stopServer} shape="rounded" /></View>
        } else if(this.state.showJoining) {
            let title = t("Stop Joining");
            if(this.props.isClient) {
                title = t("Disconnect");
            }
            buttons = <View style={[sharedStyles.width90, sharedStyles.centered]}><Button title={title} onPress={this.stopJoining} shape="rounded" /></View>
        }

        return buttons;
    }

    renderGenerateButton = () => {
        let button = this.props.selectedSettlement ?
            <Button title={t("Generate code")} onPress={this.hostGame} shape="rounded"/> : null;

        return button;
    }

    reconnectClient = (redirectOnLoadComplete = true) => {
        if(this.props.loading > 0) {
            return;
        }

        this.setState({showScanner: false, remoteIpAddress: this.props.hostIp, redirectOnLoadComplete});
        this.initClient(this.props.hostIp);
    }

    getReconnectButton = () => {
        let button = null;
        if(!this.props.isClient && this.props.hostIp) {
            button = <View>
                <Button title={t("Reconnect to Last Host")} onPress={this.reconnectClient}/>
                <View style={{paddingLeft: 5}}>
                    <Text style={sharedStyles.itemText}>
                        {t("This will reconnect to the last successfully connected host, %ip%", {ip: this.props.hostIp})}
                    </Text>
                </View>
            </View>
        }
        return button;
    }

    refreshClient = (redirectOnLoadComplete = true) => {
        this.props.clientStop();
        this.initClient();
        this.setState({redirectOnLoadComplete});
    }

    getClientProgressBar = () => {
        return(
            <Progress.Bar
                indeterminate={true}
                animated={true}
                color={this.props.colors.HIGHLIGHT}
                width={null}
            />
        )
    }

    getClientView = () => {
        if(this.state.loading) {
            return (
                <View style={{paddingLeft: 5, paddingRight: 5, width: "100%"}}>
                    <Text style={sharedStyles.itemText}>{t("Loading settlement data...")}</Text>
                    <View style={{paddingTop: 10}}>
                        {this.getClientProgressBar()}
                    </View>
                </View>
            )
        } else {
            let scanner = this.state.showScanner === false ? null : <QRCodeScanner
                title='Scan Code'
                onRead={this.codeScanned}
                cameraType={this.props.camera}
                topContent={(
                    <Text>{t("Scan a host's QR code")}</Text>
                )}
            />;

            return (<View style={sharedStyles.container}>
                <View style={{paddingLeft: 5}}><Text style={sharedStyles.itemText}>{t("While successfully joined to a game you will see a green checkmark over the menu button in the top right.")}</Text></View>
                <View style={{flexDirection: "row", paddingRight: 15, justifyContent: "flex-start"}}>
                    <View style={{width: "90%"}}>{this.getReconnectButton()}</View>
                    <View style={{top: -5}}><TouchableOpacity onPress={this.changeCamera}><Icon name="camera-switch" color="white" size={50}/></TouchableOpacity></View>
                </View>
                <View style={{overflow: "hidden"}}>
                    {scanner}
                </View>
            </View>);
        }
    }

    changeCamera = () => {
        let camera = this.props.camera === "back" ? "front" : "back";
        this.props.setCamera(camera);
        this.setState({showScanner: false, showJoining: false});
        setTimeout(() => {
            this.setState({showScanner: true, showJoining: true});
        }, 0);
    }

    renderQRCode = () => {
        if(this.state.myIpAddress && this.state.myIpAddress >= "192.0.0.0" && this.state.myIpAddress < "192.0.0.8") {
            return <View style={{paddingLeft: 5}}><Text style={sharedStyles.itemText}><Text style={{fontWeight: "bold"}}>{t("IMPORTANT")}:</Text></Text><Text style={sharedStyles.itemText}>{t("Your IP address was detected as %ip%. This is an IP address assigned by your mobile carrier and reserved for DS-LITE. If you want to host a game you need to turn off mobile data, stop hosting, and restart the host process. You may re-enable mobile data after the QR code has been generated.", {ip: this.state.myIpAddress})}</Text></View>
        }

        return <View style={{alignItems: "center", backgroundColor: "white", padding: 10}}>
            <Text style={{color:"black"}}>{t("IP address")}: {this.state.myIpAddress}</Text>
            <QRCode
                value={JSON.stringify({ip: this.state.myIpAddress, version: packageInfo.version})}
                size={200}
                ecl='Q'
            />
        </View>
    }

    renderMessages = () => {
        if(this.state.messages.length === 0) {
            return null;
        }

        return <View style={{paddingLeft: 5}}>
            <Hr />
            {this.state.messages.map((message, i) => {
                return <Text key={i}>{message}</Text>
            })}
        </View>
    }

    showSettings = () => {
        this.setState({showSettings: true});
    }

    onSettingsHide = () => {
        this.setState({showSettings: false});
    }

    ipAddressInputFocus = () => {
        this.ipInput.focus();
    }

    defaultAddressInputFocus = () => {
        this.defaultInput.focus();
    }

    onIpChange = (val) => {
        this.setState({myIpAddress: val});
    }

    onDefaultIpChange = (val) => {
        this.props.setDefaultIpAddress(val);
    }

    renderSettings = () => {
        return <View>
            <SubHeader
                title={t("My IP Address")}
                onPress={this.ipAddressInputFocus}
            />
            <AppTextInput
                textRef={ref => this.ipInput = ref}
                placeholder={t("Enter IP Address")}
                value={this.state.myIpAddress}
                onChangeText={this.onIpChange}
            />
            <View style={{height: 20}} />
            <SubHeader
                title={t("My IP Address")}
                onPress={this.defaultAddressInputFocus}
            />
            <AppTextInput
                textRef={ref => this.defaultInput = ref}
                placeholder={t("Enter Default IP Address")}
                value={this.props.defaultIpAddress}
                onChangeText={this.onDefaultIpChange}
            />
        </View>
    }

    render() {
        if(!this.props.render) {
            return null;
        }

        return (
            <View style={sharedStyles.container}>
                <BackMenu />
                <Text style={sharedStyles.pageHeader}>{t("Network")}</Text>
                <View style={{alignSelf: "flex-end"}}>
                    <TouchableOpacity onPress={this.showSettings}>
                        <Icon name="settings" color={this.props.colors.HIGHLIGHT} size={30}/>
                    </TouchableOpacity>
                </View>
                <View style={{paddingLeft: 5}}>
                    <Text style={sharedStyles.itemText}>{t("All devices must be on the same Wi-Fi network to be able to connect to each other.")}</Text>
                </View>
                {this.renderButtons()}
                <Collapsible collapsed={this.state.showHostConfig === false}>
                <View style={sharedStyles.wrapper}>
                    <View style={sharedStyles.row}>
                        <Text style={sharedStyles.title}>{t("Choose Settlement to Host")}</Text>
                    </View>
                    <View style={sharedStyles.contentWrapper}>
                        {this.renderSettlementsList()}
                        {this.renderGenerateButton()}
                    </View>
                </View>
                </Collapsible>
                <Collapsible collapsed={this.state.showHosting === false}>
                    <View style={{paddingLeft: 5}}><Text style={sharedStyles.itemText}>{t("While successfully hosting a game you will see a green checkmark over the menu button in the top right.")}</Text></View>
                    {this.renderQRCode()}
                </Collapsible>
                <Collapsible collapsed={this.state.showJoining === false}>
                    {this.getClientView()}
                </Collapsible>
                {this.renderMessages()}
                <Popup
                    visible={this.state.showSettings}
                    onDismissed={this.onSettingsHide}
                    title={t("Network Settings")}
                >
                    {this.renderSettings()}
                </Popup>
            </View>
        );
    }

    handleClientStateChange = (nextAppState) => {
        if(this.appState.match(/inactive|background/) && nextAppState === "active") {
            if(this.props.isDisconnected) {
                Toast.show(t('Disconnect detected, reconnecting'));
                this.reconnectClient();
            }
        }

        this.appState = nextAppState;
    }

    // automatically recycle the host when backgrounded on iOS, the OS does something really weird to the server
    handleIOSServerStateChange = (nextAppState) => {
        if(this.appState === "active" && nextAppState.match(/inactive|background/)) {
            this.stopServer(true);
        } else if(this.appState.match(/inactive|background/) && nextAppState === "active") {
            this.props.setShareId(this.prevShareId);
            this.startServer();
        }

        this.appState = nextAppState;
    }
}

function mapStateToProps(state, props) {
    return {
        settlements: state.settlementReducer.settlements,
        survivors: state.survivorReducer.survivors,
        timelines: state.timelineReducer,
        isHost: state.networkReducer.server !== null,
        isClient: state.networkReducer.client !== null,
        selectedSettlement: state.networkReducer.shareId,
        hostIp: state.networkReducer.hostIpAddress,
        shouldShowAds: state.purchases.shouldShowAds,
        isSubscriber: state.purchases.isSubscriber,
        loading: state.networkReducer.loading,
        shareId: state.networkReducer.shareId,
        clientConnected: state.networkReducer.clientConnected,
        reconnectLockout: state.networkReducer.reconnectLockout,
        camera: state.settingsReducer.camera,
        customExpansions: state.customExpansions,
        isDisconnected: isDisconnectedClient(state),
        globalMilestones: state.milestones,
        autoHostSettlement: state.settingsReducer.autoHostSettlement,
        defaultIpAddress: state.settingsReducer.defaultIpAddress,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        startServer,
        stopServer,
        setupSocket, setShareId,
        sendServerPacket, sendClientPacket,
        clientStart, clientStop,
        doSettlementHostAction, settlementActionWrapper,
        survivorActionWrapper, doSurvivorHostAction,
        timelineActionWrapper, doTimelineHostAction, doMilestoneHostAction,
        importSettlement, importSurvivors, importTimeline, importCustomExpansions, importMilestones,
        goToSettlement,
        loadingUp, loadingDown,
        setIsSubscriber, setShouldShowAds,
        setReconnectFunction,
        setReconnectLockout,
        setCamera,
        setHostIpAddress,
        milestoneActionWrapper,
        setDefaultIpAddress,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Router);
