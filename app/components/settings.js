'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import ScrollableTabView from 'react-native-scrollable-tab-view-universal';

import {
    View,
    ScrollView,
} from 'react-native';

import Text from '../components/shared/app-text';
import sharedStyles from '../style/styles';

import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import BackMenu from './shared/BackMenu';
import SettingsMain from './Settings/SettingsMain';
import SettingsBehavior from './Settings/SettingsBehavior';

class Settings extends Component {
    render() {
        return (
            <ScrollView>
                <BackMenu />
                <Text style={sharedStyles.pageHeader}>{t("Settings & Tools")}</Text>
                <ScrollableTabView
                    locked={true}
                    tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                    tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                    tabBarInactiveTextColor={this.props.colors.TEXT}
                    tabBarTextStyle={sharedStyles.tabText}
                >
                    <SettingsMain tabLabel={("Settings")}/>
                    <SettingsBehavior tabLabel={("Behavior")}/>
                </ScrollableTabView>
                <View height={50}/>
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
