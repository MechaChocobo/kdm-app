'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ScrollableTabView from 'react-native-scrollable-tab-view-universal';

import {
    View,
    Dimensions,
} from 'react-native';

import {changePage} from '../actions/router';
import {survivorActionWrapper} from '../actions/survivors';

import sharedStyles from '../style/styles';
import GeneralControls from './survivor/general-controls';
import { getColors } from '../selectors/general';

import SurvivorMain from './SurvivorMain';
import { t } from '../helpers/intl';
import SurvivorOther from './SurvivorOther';
import BackMenu from './shared/BackMenu';
import styles from '../style/styles';
import Loadout from './survivor/loadout/Loadout';

class Survivor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: Dimensions.get('window').width,
            enableScroll: true,
        };
    }

    componentWillMount = () => {
        Dimensions.addEventListener("change", this.setWidth);
    }

    componentWillUnmount = () => {
        Dimensions.removeEventListener("change", this.setWidth);
    }

    setWidth = () => {
        this.setState({width: Dimensions.get('window').width});
    }

    getWidth = () => {
        if(this.props.halfWidth) {
            return (this.state.width / 2 - 1);
        }

        return this.state.width;
    }

    onIncrementorShow = () => {
        this.setState({enableScroll: false});
    }

    onIncrementorHide = () => {
        this.setState({enableScroll: true});
    }

    render() {
        if(!this.props.survivor) {
            return null;
        }

        return (
            <View style={sharedStyles.container}>
                <View style={{width: this.getWidth(), flex: 1}}>
                    {this.props.showNav && <BackMenu />}
                    <GeneralControls
                        id={this.props.survivor.id}
                        settlementId={this.props.settlementId}
                        isGrid={this.props.isGrid}
                        sortAttribute={this.props.sortAttribute}
                        index={this.props.index}
                    />
                    <KeyboardAwareScrollView enableAutomaticScroll={this.state.enableScroll}>
                        <ScrollableTabView
                            tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                            tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                            tabBarInactiveTextColor={this.props.colors.TEXT}
                            tabBarTextStyle={styles.tabText}
                            locked={true}
                            // initialPage={1}
                        >
                            <SurvivorMain
                                tabLabel={t("Main")}
                                id={this.props.survivor.id}
                                settlementId={this.props.settlementId}
                                campaign={this.props.campaign}
                                onIncrementorShow={this.onIncrementorShow}
                                onIncrementorHide={this.onIncrementorHide}
                            />
                            <SurvivorOther tabLabel={t("Other")} id={this.props.survivor.id} settlementId={this.props.settlementId}/>
                            <Loadout tabLabel={t("Loadout")} id={this.props.survivor.id} settlementId={this.props.settlementId} isGrid={this.props.isGrid}/>
                        </ScrollableTabView>
                        <View style={{height: 100}}></View>
                    </KeyboardAwareScrollView>
                </View>
            </View>
        );
    }

    backToSurvivors = () => {
        this.props.changePage('survivor-home');
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    let survivorId = state.pageReducer.survivorId ? state.pageReducer.survivorId : state.pageReducer.prevSurvivorId;
    let survivor = props.survivor;

    if(!survivor) {
        survivor = state.survivorReducer.survivors[settlementId][survivorId];
    }

    return {
        settlementId: settlementId,
        selectedSurvivorIndex: state.pageReducer.selectedSurvivorIndex - 1,
        survivor,
        campaign: state.settlementReducer.settlements[settlementId].campaign,
        isHost: state.networkReducer.server !== null,
        isClient: state.networkReducer.client !== null,
        sortAttribute: state.settlementReducer.settlements[settlementId].survivorSortAttr,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({changePage, survivorActionWrapper}, dispatch);
}

Survivor.defaultProps = {
    showNav: true,
    isGrid: false,
    index: undefined,
}

export default connect(mapStateToProps, mapDispatchToProps)(Survivor);
