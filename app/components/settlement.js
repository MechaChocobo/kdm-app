'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {changePage} from '../actions/router';
import {settlementActionWrapper} from '../actions/settlement';
import {initTimeline} from '../actions/timeline';

import {
    StyleSheet,
    View,
    Dimensions,
} from 'react-native';

import SurvivalLimit from './settlement/survival-limit';
import Population from './settlement/population';
import DeathCount from './settlement/death-count';
import Locations from './settlement/locations';
import Endeavors from './settlement/endeavors';
import InspirationalStatue from './settlement/inspirational-statue';
import Notes from './settlement/notes';
import Admin from './settlement/admin';

import sharedStyles from '../style/styles';
import { getColors } from '../selectors/general';
import BGBanner from './shared/BGBanner';
import Hr from './shared/Hr';
import SettlementActionLog from './settlement/SettlementActionLog';
import AppTextInput from './shared/AppTextInput';
import SettlementSettingsSection from './settlement/SettlementSettingsSection';
import Quarries from './settlement/Quarries';

class Settlement extends Component {
    constructor(props) {
        super(props);
        this.state = {width: Dimensions.get('window').width };
    }

    componentWillMount = () => {
        Dimensions.addEventListener("change", this.setWidth);
    }

    componentWillUnmount = () => {
        Dimensions.removeEventListener("change", this.setWidth);
    }

    setWidth = () => {
        this.setState({width: Dimensions.get('window').width});
    }

    componentDidMount = () => {
        if(!this.props.timeline) {
            this.props.initTimeline(this.props.id);
        }
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <KeyboardAwareScrollView style={{paddingBottom: 50}}>
                    <BGBanner>
                        <AppTextInput
                            inputStyle={styles.nameInput}
                            value={this.props.settlement.name}
                            onChangeText={this.setName}
                        />
                    </BGBanner>
                    <View style={[sharedStyles.centeredWrapper, {flexDirection: "row", justifyContent: "space-between", marginTop: -38, zIndex: 100}]}>
                        <SurvivalLimit id={this.props.id}/>
                        <Population id={this.props.id}/>
                        <DeathCount id={this.props.id}/>
                    </View>
                    <Hr />
                    <Endeavors id={this.props.id}/>
                    <Hr />
                    {this.props.hasSculpture && <InspirationalStatue id={this.props.id}/>}
                    {this.props.hasSculpture && <Hr />}
                    <Locations id={this.props.id}/>
                    <Hr />
                    <Quarries id={this.props.id} />
                    <Hr />
                    <Notes id={this.props.id} />
                    <Hr />
                    <SettlementSettingsSection id={this.props.id}/>
                    <Hr />
                    <Admin id={this.props.id}/>
                    <View style={{alignItems: "flex-end", marginRight: "5%", marginBottom: 10}}>
                        <SettlementActionLog id={this.props.id} />
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }

    setName = (val) => {
        this.props.settlementActionWrapper('changeName', [this.props.id, val]);
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return {
        id: settlementId,
        hasSculpture: (state.settlementReducer.settlements[settlementId].innovations.indexOf('sculpture') !== -1),
        settlement: state.settlementReducer.settlements[settlementId],
        timeline: state.timelineReducer[settlementId],
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({changePage, settlementActionWrapper, initTimeline}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Settlement);

var styles = StyleSheet.create({
    nameInput: {
        marginLeft: 45,
        marginRight: 45,
        textAlign: "center",
        color: "white",
        fontSize: 30,
        padding: 0
    },
});
