'use strict';

import React, { PureComponent } from 'react';

import {
    View,
    FlatList,
} from 'react-native';

import glossary from '../data/glossary';
import BackMenu from './shared/BackMenu';
import AppText from './shared/app-text';
import styles from '../style/styles';
import { t } from '../helpers/intl';
import ColorContext from '../context/ColorContext';
import Zebra from './shared/Zebra';
import AppTextInput from './shared/AppTextInput';
import Icon from './shared/Icon';

export default class Glossary extends PureComponent {
    state = {
        search: '',
    };

    render() {
        return (
            <ColorContext.Consumer>
                {colors =>
                    <View style={styles.container}>
                        <BackMenu />
                        <AppText style={styles.pageHeader}>{t("Glossary")}</AppText>
                        <View style={[styles.rowCentered, styles.width95, styles.centered]}>
                            <Icon name="magnify" color={colors.TEXT} size={25}/>
                            <AppTextInput
                                value={this.state.search}
                                onChangeText={this.setSearch}
                                realtimeChange={true}
                                style={styles.width95}
                                placeholder={t("Search")}
                            />
                        </View>
                        <FlatList
                            data={this.getData()}
                            renderItem={this.renderEntry}
                            keyExtractor={(item, index) => index + ""}
                        />
                    </View>
                }
            </ColorContext.Consumer>
        );
    }

    setSearch = (value) => {
        this.setState({search: value});
    }

    renderEntry = (item) => {
        const entry = item.item;

        return <Zebra zebra={item.index % 2 !== 0}>
            <AppText style={[styles.itemText, styles.icomoon, {margin: 10, marginTop: 5, marginBottom: 5, lineHeight: 20}]}><AppText style={styles.bold}>{entry.title}</AppText>: {entry.description}</AppText>
        </Zebra>
    }

    getData = () => {
        if(this.state.search) {
            let results = glossary.filter(this.filterGlossary);
            return results.sort(this.sortSearchResults);
        } else {
            return glossary;
        }
    }

    sortSearchResults = (a, b) => {
        let re = new RegExp(this.state.search,"ig");
        if(a.title.match(re) === null && b.title.match(re) !== null) {
            return 1;
        } else if(a.title.match(re) !== null && b.title.match(re) === null) {
            return -1
        } else {
            if(a.title > b.title) {
                return 1;
            } else if(a.title < b.title) {
                return -1
            } else {
                return 0;
            }
        }
    }

    filterGlossary = (entry) => {
        let re = new RegExp(this.state.search,"ig");
        return (entry.title.match(re) !== null || entry.description.match(re) !== null);
    }
}
