'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { timelineActionWrapper } from '../actions/timeline';
import { settlementActionWrapper } from '../actions/settlement';
import { getRandomInt, getExpansionParensForStoryEvent, getEventTitlePluralForType, getEventConfigForType } from '../helpers/helpers';

import {
    View,
    ScrollView,
} from 'react-native';

import styles from '../style/styles';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import { getSortedStoryEvents, getSortedNemesisEncounters, getSortedSpecialShowdowns, getSortedQuarryShowdowns } from '../selectors/settlement';
import AppText from './shared/app-text';
import RoundControl from './shared/buttons/RoundControl';
import Popup from './shared/popup';
import ListItem from './shared/ListItem';
import BGBanner from './shared/BGBanner';
import PillListItem from './shared/PillListItem';
import Hr from './shared/Hr';

class TimelineLy extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showAddEvent: false,
            addEventType: "",
        };
    }

    addToTimeline = (value) => {
        if (value === "") {
            return;
        }

        this.props.timelineActionWrapper('addToTimeline', [this.props.settlementId, this.props.ly, this.state.addEventType, value]);
        this.hideAddEvent();
    }

    addRandomSettlementEvent = () => {
        let events = this.props.settlementEvents.filter(event => event !== "firstDay");
        let event = events[getRandomInt(events.length)];
        this.props.timelineActionWrapper('addToTimeline', [this.props.settlementId, this.props.ly, "settlementEvents", event]);
    }

    removeFromTimeline = (type, value) => {
        return () => {
            if (value === "") {
                return;
            }

            this.props.timelineActionWrapper('removeFromTimeline', [this.props.settlementId, this.props.ly, type, value]);
        }
    }

    renderEvents = () => {
        let sections = [];

        for(let eventType in this.props.events) {
            sections.push(<View style={styles.container} key={eventType}>
                <View style={[styles.rowCentered, styles.container, styles.spaceBetween]}>
                    <AppText style={styles.title}>{getEventTitlePluralForType(eventType)}</AppText>
                    <View style={styles.row}>
                        {eventType === "settlementEvents" &&
                            <RoundControl
                                icon="dice-multiple"
                                onPress={this.addRandomSettlementEvent}
                                style={{marginRight: 10}}
                            />}
                        {this.getPickerButton(eventType)}
                    </View>
                </View>
                {this.renderEventsOfType(eventType, this.props.events[eventType])}
                <Hr />
            </View>)
        }

        return sections;
    }

    renderEventsOfType = (type, events) => {
        return events.map((event, i) => {
            let text = event;
            let extra = "";
            let config = getEventConfigForType(type);

            if(type === "storyEvents") {
                extra = getExpansionParensForStoryEvent(event);
            }

            if(config && config[event]) {
                text = config[event].title;
            }

            return <View style={styles.rowCentered} key={i}>
                <PillListItem title={text + " " + extra} icon="close" onPress={this.removeFromTimeline(type, event)}/>
            </View>
        })
    }

    getPickerButton = (type) => {
        return <RoundControl icon="plus" onPress={this.showPicker} id={type} style={{alignSelf: "flex-end"}}/>
    }

    getAddTitleByType = (type) => {
        let title = "";

        if(type === "settlementEvents") {
            title = t("Add Settlement Event");
        } else if(type === "storyEvents") {
            title = t("Add Story Event");
        } else if(type === "nemesisEncounters") {
            title = t("Add Nemesis Encounter");
        } else if(type === "specialShowdowns") {
            title = t("Add Special Showdown");
        } else if(type === "showdowns") {
            title = t("Add Showdown");
        }

        return title;
    }

    showPicker = (type) => {
        this.setState({showAddEvent: true, addEventType: type});
    }

    hideAddEvent = () => {
        this.setState({showAddEvent: false, addEventType: ""});
    }

    getEventPickerList = () => {
        let config = getEventConfigForType(this.state.addEventType);

        let events = this.getEventsByType(this.state.addEventType);

        return events.map((event, i) => {
            let title = config[event] ? config[event].title : event;
            return <ListItem
                key={i}
                title={title}
                onPress={this.addToTimeline}
                id={event}
                buttonText={t("Add")}
            />
        });
    }

    getEventsByType = (type) => {
        let events = [];

        if(type === "settlementEvents") {
            events = this.props.settlementEvents;
        } else if(type === "storyEvents") {
            events = this.props.storyEvents;
        } else if(type === "nemesisEncounters") {
            events = this.props.nemesisEncounters;
        } else if(type === "specialShowdowns") {
            events = this.props.specialShowdowns;
        } else if(type === "showdowns") {
            events = this.props.showdowns;
        }

        return events;
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <BGBanner>
                    <AppText style={styles.pageHeader} color={this.props.colors.WHITE}>{t("Lantern Year %year%", {year: this.props.ly})}</AppText>
                </BGBanner>
                <View style={[styles.width90, styles.centered, {marginTop: 20}]}>
                    {this.renderEvents()}
                </View>
                {this.state.showAddEvent && <Popup
                    visible={this.state.showAddEvent}
                    onDismissed={this.hideAddEvent}
                    title={this.getAddTitleByType(this.state.addEventType)}
                >
                    {this.getEventPickerList()}
                </Popup>}
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    let ly = state.pageReducer.timelineLy !== undefined ? state.pageReducer.timelineLy : state.pageReducer.prevTimelineLy;
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;

    return {
        settlementId: settlementId,
        timeline: state.timelineReducer[settlementId],
        settlementEvents: state.settlementReducer.settlements[settlementId].allSettlementEvents.sort(),
        nemesisEncounters: getSortedNemesisEncounters(state),
        showdowns: getSortedQuarryShowdowns(state),
        specialShowdowns: getSortedSpecialShowdowns(state),
        storyEvents: getSortedStoryEvents(state),
        currentYear: state.settlementReducer.settlements[settlementId].ly,
        ly,
        events: state.timelineReducer[settlementId][ly],
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ timelineActionWrapper, settlementActionWrapper }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TimelineLy);
