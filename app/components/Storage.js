'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';

import ScrollableTabView from 'react-native-scrollable-tab-view-universal';

import Text from './shared/app-text';
import sharedStyles from '../style/styles';

import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import Resources from './settlement/storage/Resources';
import BGBanner from './shared/BGBanner';
import Gear from './settlement/storage/Gear';

import {
    ScrollView,
} from 'react-native';
import styles from '../style/styles';

class Storage extends Component {
    render() {
        return (
            <ScrollView style={sharedStyles.container}>
                <BGBanner>
                    <Text style={sharedStyles.pageHeader} color={this.props.colors.WHITE}>{this.props.settlementName}</Text>
                </BGBanner>
                <ScrollableTabView
                    locked={true}
                    tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                    tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                    tabBarInactiveTextColor={this.props.colors.TEXT}
                    tabBarTextStyle={styles.tabText}
                >
                    <Resources tabLabel={t("Resources")} id={this.props.id}/>
                    <Gear tabLabel={t("Gear")} id={this.props.id}/>
                </ScrollableTabView>
            </ScrollView>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return {
        id: settlementId,
        settlementName: state.settlementReducer.settlements[settlementId].name,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Storage);
