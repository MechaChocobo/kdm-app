'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import Collapsible from 'react-native-collapsible';
import { RecyclerListView, DataProvider, LayoutProvider } from "recyclerlistview";

import {
    View,
    TouchableOpacity,
    FlatList,
    TouchableWithoutFeedback,
    Modal,
    Dimensions,
} from 'react-native';

import {goToSettlement, goToSurvivor, goToSurvivorList, changePage, clearSurvivorIndex} from '../actions/router';
import { timelineActionWrapper } from '../actions/timeline';
import { settlementActionWrapper } from '../actions/settlement';
import {survivorActionWrapper, setPinned} from '../actions/survivors';
import {getAliveSurvivors, getDepartingSurvivorIds, getPinnedSurvivors} from '../selectors/survivors';
import {SurvivorLineItem, getCardHeight, INDEX_MOD} from '../components/survivor/survivor-line-item';

import RoundedIcon from './shared/rounded-icon';
import Text from './shared/app-text';
import sharedStyles from '../style/styles';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import PageIndicator from './shared/PageIndicator';
import Sort from './survivor/Sort';
import { getSettlementId } from '../selectors/settlement';
import { initRecylcerContext } from '../helpers/recycler-context';
import Icon from './shared/Icon';
import { getWidthFromPercentage } from '../helpers/helpers';

const TYPE_TOOLTIP = 'tooltip';
const TYPE_SORT = 'sort';
const TYPE_PADDING = 'padding';

class SurvivorsAlive extends Component {
    list = null;

    constructor(props) {
        super(props);

        let { width } = Dimensions.get("window");

        let dataProvider = new DataProvider((r1, r2) => {
            return r1 != r2;
        }).cloneWithRows(["tooltip", "sort", ...this.props.survivors, "padding"]);

        this.layoutProvider = new LayoutProvider((index) => {
            if(index === 0) {
                return TYPE_TOOLTIP;
            } else if(index === 1) {
                return TYPE_SORT;
            } else if(index === this.props.survivors.length + INDEX_MOD) {
                return TYPE_PADDING;
            } else {
                return index;
            }
        },
        (type, dim, index) => {
            dim.width = width;
            if(type === TYPE_TOOLTIP) {
                dim.height = 40;
            } else if(type === TYPE_SORT) {
                if(this.props.isSubscriber !== true) {
                    dim.height = 0;
                } else {
                    dim.height = 100;
                }
            } else if(type === TYPE_PADDING) {
                dim.height = 10;
            } else {
                dim.height = getCardHeight(this.props.numLinesSurvivorCard);
            }
        });

        this.recyclerContext = initRecylcerContext('survivor-home-alive');

        this.state = {
            showTooltip: false,
            showDetails: false,
            detailsIndex: 0,
            detailsSurvivorIndex: 0,
            dataProvider: dataProvider,
        };
    }

    componentDidUpdate(prevProps) {
        if(prevProps.survivors !== this.props.survivors) {
            this.setState({dataProvider: new DataProvider((r1, r2) => {
                return r1 != r2;
            }).cloneWithRows(["tooltip", "sort", ...this.props.survivors, "padding"])});
        }
    }

    survivorPressed = (survivor, index) => {
        this.props.goToSurvivor(survivor.id);
    }

    toggleDeparting = (id) => {
        // pass: settlementId, id, ly
        this.props.survivorActionWrapper('toggleDeparting', [this.props.settlementId, id, this.props.ly]);
    }

    toggleSkipNextHunt = (id) => {
        this.props.survivorActionWrapper('toggleSkipNextHunt', [this.props.settlementId, id]);
    }

    toggleReroll = (id) => {
        this.props.survivorActionWrapper('toggleReroll', [this.props.settlementId, id]);
    }

    togglePinned = (id) => {
        let value = this.props.pinnedSurvivors[id] === true ? false : true;
        this.props.setPinned(this.props.settlementId, id, value);
    }

    getSwipeButtons = (survivor) => {
        if(survivor.departing) {
            return [
                <TouchableOpacity style={{height: "100%", flexDirection: "row", alignItems:"center"}} onPress={this.toggleDeparting(survivor.id)}><Text>Leave Departing</Text></TouchableOpacity>
            ];
        } else {
            return [
                <TouchableOpacity style={{height: "100%", flexDirection: "row", alignItems:"center"}} onPress={this.toggleDeparting(survivor.id)}><Text>Join Departing</Text></TouchableOpacity>
            ];
        }
    }

    toggleToolTip = () => {
        this.setState({showTooltip: !this.state.showTooltip});
    }

    setSortAttribute = (attr) => {
        this.props.setSurvivorSortAttribute(this.props.settlementId, attr);
    }

    toggleSortDirection = () => {
        this.props.setSurvivorSortOrder(this.props.settlementId, this.props.sortOrder === "asc" ? "desc" : "asc");
    }

    renderSurvivor = (type, item) => {
        if(item === "padding") {
            return <View style={{height: 10}} />
        }

        if(item === "sort") {
            if(this.props.isSubscriber !== true) {
                return null;
            }

            return <Sort />
        }

        if(item === "tooltip") {
            let content = t("Swipe a survivor to the left or long press the departing icon to mark them as a Departing survivor.");

            if(this.props.isSubscriber === true) {
                content += t("You can change the order of sorting (low->high vs high->low) by tapping the arrow next to the sort order.");
            }

            return(
                <View style={{marginLeft: 20, marginRight: 20, marginTop: 5}}>
                    <View style={{flexDirection: "row", justifyContent: "space-between"}}>
                        <RoundedIcon icon="account-plus" size={30} gradient={true} onPress={this.goToCreateSurvivor} />
                        <RoundedIcon icon="help" size={30} gradient={true} onPress={this.toggleToolTip} />
                    </View>
                    <Collapsible collapsed={!this.state.showTooltip}>
                        <Text style={sharedStyles.itemText}>{content}</Text>
                    </Collapsible>
                </View>
            )
        }

        let survivor = item;

        return (
            <SurvivorLineItem
                survivor={survivor}
                isSotf={this.props.isSotf}
                ly={this.props.ly}
                campaign={this.props.campaign}
                onPress={this.survivorPressed}
                index={type}
                onDepart={this.toggleDeparting}
                onShowMore={this.onShowMore}
                onSkipNextHunt={this.toggleSkipNextHunt}
                onReroll={this.toggleReroll}
                onPinned={this.togglePinned}
                numLinesSurvivorCard={this.props.numLinesSurvivorCard}
                pinned={this.props.pinnedSurvivors[survivor.id] === true}
                tapToDepart={this.props.tapToDepart}
            />
        );
    }

    onShowMore = (index) => {
        this.setState({showDetails: true, detailsSurvivorIndex: index});
    }

    renderSurvivorDetail = (item) => {
        return (
            <SurvivorLineItem
                survivor={item.item}
                isSotf={this.props.isSotf}
                ly={this.props.ly}
                campaign={this.props.campaign}
                onPress={this.survivorPressed}
                index={item.index}
                detail={true}
                onDepart={this.toggleDeparting}
                onSkipNextHunt={this.toggleSkipNextHunt}
                onReroll={this.toggleReroll}
                tapToDepart={this.props.tapToDepart}
            />
        );
    }

    goToCreateSurvivor = () => {
        this.props.changePage("create-survivor");
    }

    handleDetailScroll = (e) => {
        let offset = e.nativeEvent.contentOffset;
        let viewSize = e.nativeEvent.layoutMeasurement;
        let index = Math.floor((offset.x +(viewSize.width * .10)) / viewSize.width);

        this.setState({detailsSurvivorIndex: index});
    }

    getDetailList = () => {
        return (
            <FlatList
                data={this.props.survivors}
                renderItem={this.renderSurvivorDetail}
                keyExtractor={(item, index) => index + ""}
                horizontal={true}
                pagingEnabled={true}
                initialScrollIndex={this.state.detailsSurvivorIndex}
                onScroll={this.handleDetailScroll}
                getItemLayout={this.getDetailLayout}
            />
        );
    }

    getDetailLayout = (data, index) => {
        let width = Dimensions.get('window').width;

        return {
            length: width,
            offset: width * index,
            index
        }
    }

    onDetailClose = () => {
        this.setState({showDetails: false, detailsScrollFired: false});
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <RecyclerListView
                    layoutProvider={this.layoutProvider}
                    dataProvider={this.state.dataProvider}
                    rowRenderer={this.renderSurvivor}
                    contextProvider={this.recyclerContext}
                />
                <Modal
                    visible={this.state.showDetails}
                    onRequestClose={this.onDetailClose}
                    transparent={true}
                >
                    <TouchableWithoutFeedback onPress={this.onDetailClose}>
                        <View style={{justifyContent: "center", flex: 1, backgroundColor: "rgba(0, 0, 0, .8)"}}>
                            <Icon name="close" size={30} style={{position: "absolute", right: getWidthFromPercentage(.05), top: 5}} color={this.props.colors.HIGHLIGHT}/>
                            <View style={[sharedStyles.centered, {marginTop: 20}]}>
                                <PageIndicator numPages={this.props.survivors.length} currentPage={this.state.detailsSurvivorIndex} />
                            </View>
                            {this.getDetailList()}
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
            </View>
        );
    }
}

function mapStateToProps(state, props) {
    let settlementId = getSettlementId(state);
    return {
        settlementId: settlementId,
        isSotf: (state.settlementReducer.settlements[settlementId].principles.newLife === "survivalOfTheFittest"),
        survivalLimit: state.settlementReducer.settlements[settlementId].survivalLimit,
        survivors: getAliveSurvivors(state),
        ly: state.settlementReducer.settlements[settlementId].ly,
        campaign: state.settlementReducer.settlements[settlementId].campaign,
        departingSurvivors: getDepartingSurvivorIds(state),
        lastSelectedSurvivorIndex: state.pageReducer.lastSelectedSurvivorIndex,
        survivorOffset: state.pageReducer.survivorOffset,
        isSubscriber: state.purchases.isSubscriber,
        sortAttribute: state.settlementReducer.settlements[settlementId].survivorSortAttr,
        sortOrder: state.settlementReducer.settlements[settlementId].survivorSortOrder,
        numLinesSurvivorCard: state.settingsReducer.numLinesSurvivorCard,
        pinnedSurvivors: getPinnedSurvivors(state),
        tapToDepart: state.settingsReducer.tapToDepart,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({goToSettlement, goToSurvivor, goToSurvivorList, changePage, survivorActionWrapper, timelineActionWrapper, settlementActionWrapper, clearSurvivorIndex, setPinned}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SurvivorsAlive);
