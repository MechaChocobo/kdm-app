'use strict';

import React, { Component } from 'react';

import {
    View,
} from 'react-native';

import SurvivalActions from './survivor/survival-actions';
import Survival from './survivor/survival';
import Insanity from './survivor/insanity';
import Stats from './survivor/stats';
import FightingArts from './survivor/fighting-arts';
import Disorders from './survivor/disorders';
import Abilities from './survivor/abilities';
import SevereInjuries from './survivor/severe-injuries';
import Armor from './survivor/armor';
import Notes from './survivor/notes';
import Hr from './shared/Hr';
import styles from '../style/styles';
import Constellations from './survivor/constellations';
import SurvivorActionLog from './survivor/SurvivorActionLog';

export default class SurvivorMain extends Component {
    render() {
        return (
            <View>
                <View style={{flexDirection: "row", paddingTop: "2%"}}>
                    <View style={{width: "37%"}}>
                        <Survival id={this.props.id} settlementId={this.props.settlementId} tooltipRef={this.props.tooltipRef}/>
                    </View>
                    <View style={{width: "29%"}}>
                        <SurvivalActions id={this.props.id} settlementId={this.props.settlementId}/>
                    </View>
                    <View style={{width: "33%"}}>
                        <Insanity id={this.props.id} settlementId={this.props.settlementId}/>
                    </View>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <Armor id={this.props.id} settlementId={this.props.settlementId} onIncrementorShow={this.props.onIncrementorShow} onIncrementorHide={this.props.onIncrementorHide} />
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <Stats id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                {this.props.campaign === "PotStars" && <View>
                    <Hr />
                    <View>
                        <Constellations id={this.props.id} settlementId={this.props.settlementId}/>
                    </View>
                </View>}
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <FightingArts id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <Disorders id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <Abilities id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <View style={[styles.centered, styles.width90]}>
                    <SevereInjuries id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
                <Hr />
                <Notes id={this.props.id} settlementId={this.props.settlementId}/>
                <View style={{alignItems: "flex-end", marginRight: "5%", marginBottom: 10, marginTop: 20}}>
                    <SurvivorActionLog id={this.props.id} settlementId={this.props.settlementId}/>
                </View>
            </View>
        );
    }
}

SurvivorMain.propTypes = {
}

SurvivorMain.defaultProps = {
}
