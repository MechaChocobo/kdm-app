'use strict';

import React, { Component } from 'react';

import {
    FlatList,
    View,
} from 'react-native';

import {bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {changePage, goToSettlement} from '../actions/router';
import { getActiveSettlements } from '../selectors/settlement';
import SettlementLineItem from './settlement/settlement-line-item';
import Button from '../components/shared/button';
import sharedStyles from '../style/styles';
import { t } from '../helpers/intl';
import { deleteSettlement, toggleArchive } from '../actions/settlement';
import { getColors } from '../selectors/general';
import { resetListContext } from '../helpers/recycler-context';
import RoundControl from './shared/buttons/RoundControl';

class SettlementsActive extends Component {
    render() {
        return (
            <View style={sharedStyles.container} ref={(ref) => this.tipParent = ref}>
                <FlatList
                    data={this.props.settlements}
                    renderItem={this.renderSettlement}
                    keyExtractor={(item, index) => index + ""}
                />
                <View style={[sharedStyles.rowCentered, {padding: "4%", paddingTop: 10}]}>
                    <Button
                        style={{ flex: 1, marginRight: 50}}
                        onPress={this.goToCreateSettlement}
                        title={"+ " + t("New Settlement")}
                        color={this.props.colors.HIGHLIGHT}
                        shape='rounded'
                    />
                    <RoundControl
                        icon="sk-milestones"
                        size={30}
                        onPress={this.goToGlobalMilestones}
                        iconSize={25}
                    />
                </View>
            </View>
        );
    }

    goToGlobalMilestones = () => {
        this.props.changePage('global-milestones');
    }

    goToCreateSettlement = () => {
        this.props.changePage('create-settlement');
    }

    goToSettlement = (id) => {
        resetListContext(); // resets saved scroll indexes of lists
        this.props.goToSettlement(id);
    }

    archiveSettlement = (id) => {
        this.props.toggleArchive(id);
    }

    deleteSettlement = (id) => {
        this.props.deleteSettlement(id);
    }

    renderSettlement = (item) => {
        return (
            <SettlementLineItem
                settlement={item.item}
                customExpansions={this.props.customExpansions}
                onPress={this.goToSettlement}
                onArchive={this.archiveSettlement}
                onDelete={this.deleteSettlement}
                light={item.index % 2 !== 0}
            />
        )
    }
};

function mapStateToProps(state, props) {
    return {
        settlements: getActiveSettlements(state),
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        changePage,
        goToSettlement,
        deleteSettlement,
        toggleArchive,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SettlementsActive);
