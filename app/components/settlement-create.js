'use strict';

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { getColors } from '../selectors/general';
import MultiPageContainer from './shared/MultiPageContainer';
import SettlementCreateStep1 from './SettlementCreateStep1';
import SettlementCreateStep2 from './SettlementCreateStep2';
import { settlementCreateReset } from '../actions/settlementCreate';

class CreateSettlement extends Component {
    componentWillUnmount() {
        this.props.settlementCreateReset();
    }

    render() {
        return (
            <MultiPageContainer
                pages={{
                    step1: <SettlementCreateStep1 />,
                    step2: <SettlementCreateStep2 />,
                }}
                defaultPage="step1"
            />
        );
    }
}

function mapStateToProps(state, props) {
    return {
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        settlementCreateReset,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSettlement);
