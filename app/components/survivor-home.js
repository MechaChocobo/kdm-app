'use strict';

import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import {
    View,
} from 'react-native';

import {goToSettlement, goToSurvivor, setSurvivorHomeTab} from '../actions/router';
import { getSurvivors } from '../actions/survivors';
import { getAliveMaleSurvivors, getAliveFemaleSurvivors, getDepartingSurvivors } from '../selectors/survivors';
import Text from './shared/app-text';
import sharedStyles from '../style/styles';
import SurvivorsAlive from './survivor-home-alive';
import SurvivorsDead from './survivor-home-dead';
import SurvivorsDeparting from './survivor-home-departing';
import { t } from '../helpers/intl';
import { getColors } from '../selectors/general';
import BackMenu from './shared/BackMenu';

class Survivors extends Component {
    initialPage = null;

    constructor(props) {
        super(props);

        // on iOS updating initialpage on each render causes weird scrolling behavior
        this.initialPage = this.props.chosenTab !== null ? this.props.chosenTab : (this.props.numDeparting > 0 ? 0 : 1);

        this.state = {
            currentPage: this.initialPage,
        }
    }

    render() {
        return (
            <View style={sharedStyles.container}>
                <BackMenu />
                <Text style={sharedStyles.pageHeader}>{t("Survivors")}</Text>
                <ScrollableTabView
                    locked={true}
                    tabBarUnderlineStyle={{backgroundColor: this.props.colors.HIGHLIGHT}}
                    tabBarActiveTextColor={this.props.colors.HIGHLIGHT}
                    tabBarInactiveTextColor={this.props.colors.TEXT}
                    initialPage={this.initialPage}
                    onChangeTab={this.tabSelected}
                    page={this.state.currentPage}
                >
                    <SurvivorsDeparting tabLabel={t("Departing") + " (" + this.props.numDeparting + ")"} onReturn={this.goToActive}/>
                    <SurvivorsAlive tabLabel={t("Alive")+" (" + this.props.numAlive + ") - "+this.props.numMale+" ["+t("M")+"] / "+this.props.numFemale+" ["+t("F")+"]"}/>
                    <SurvivorsDead tabLabel={t("Dead")+" (" + this.props.numDead + ")"}/>
                </ScrollableTabView>
            </View>
        );
    }

    goToActive = () => {
        this.setState({currentPage: 1});
    }

    tabSelected = (args) => {
        this.setState({currentPage: args.i});
        this.props.setSurvivorHomeTab(args.i);
    }
}

function mapStateToProps(state, props) {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return {
        id: settlementId,
        numAlive: Object.values(state.survivorReducer.survivors[settlementId]).filter(survivor => survivor.dead === false).length,
        numDead: Object.values(state.survivorReducer.survivors[settlementId]).filter(survivor => survivor.dead === true).length,
        numMale: getAliveMaleSurvivors(state).length,
        numFemale: getAliveFemaleSurvivors(state).length,
        numDeparting: getDepartingSurvivors(state).length,
        chosenTab: state.pageReducer.survivorHomeTab,
        colors: getColors(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getSurvivors, goToSettlement, goToSurvivor, setSurvivorHomeTab}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Survivors);
