'use strict';

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';

import ColorContext from '../context/ColorContext';
import Icon from './shared/Icon';
import RoundControl from './shared/buttons/RoundControl';
import AppText from './shared/app-text';
import { getExpansionParensForStoryEvent, getEventConfigForType, getEventTitleForType, CHECKBOX_SIZE } from '../helpers/helpers';
import styles from '../style/styles';
import { t } from '../helpers/intl';
import Zebra from './shared/Zebra';
import Hr from './shared/Hr';
import LightModal from './shared/LightModal';

const MAX_EVENTS = 3;
export const TEXT_EVENT_HEIGHT = 22;
export const MIN_EVENTS_NUM = 3;

export default class TimelineListItem extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            showDetail: false,
        }
    }

    onLayout = (e) => {
        // for testing height to figure out layout
        console.warn(e.nativeEvent.layout.height);
    }

    showDetail = () => {
        this.setState({showDetail: true});
    }

    hideDetail = () => {
        this.setState({showDetail: false});
    }

    isOversize = () => {
        let num = 0;
        for(let type in this.props.events) {
            num += this.props.events[type].length;
        }

        return num > MAX_EVENTS;
    }

    render() {
        return (
            <ColorContext.Consumer>
                {colors => {
                    if(this.isOversize()) {
                        return <TouchableOpacity onPress={this.showDetail}>
                            {this.getContent(colors, true)}
                            {this.state.showDetail && <LightModal
                                visible={this.state.showDetail}
                                onDismissed={this.hideDetail}
                                centered={false}
                            >
                                <AppText style={[styles.title, {marginBottom: 20}, styles.centered]}>{t("Lantern Year %year%", {year: this.props.ly})}</AppText>
                                {this.renderEvents(false)}
                            </LightModal>}
                        </TouchableOpacity>
                    } else {
                        return this.getContent(colors)
                    }
                }}
            </ColorContext.Consumer>
        );
    }

    getContent = (colors, oversize = false) => {
        return <Zebra zebra={this.props.isActiveLy}>
                <View style={{padding: "2%", paddingTop: 8, paddingBottom: 7}}>
                    <View style={[styles.rowCentered, styles.spaceBetween]}>
                        <View style={styles.rowCentered}>
                            <Icon name={this.props.isPassed ? "checkbox-blank" : "checkbox-blank-outline"} color={this.props.isActiveLy ? colors.HIGHLIGHT : colors.TEXT} size={CHECKBOX_SIZE}/>
                            <AppText style={[styles.title]} color={this.props.isActiveLy ? colors.HIGHLIGHT : colors.TEXT}>{t("Lantern Year %year%", {year: this.props.ly})}</AppText>
                        </View>
                        <View style={styles.row}>
                            <RoundControl
                                icon="dice-multiple"
                                onPress={this.props.onAddRandomSettlementEvent}
                                id={this.props.ly}
                                style={{marginRight: 10}}
                            />
                            <RoundControl icon='pencil' onPress={this.goToLy}/>
                        </View>
                    </View>
                    <View style={[styles.contentWrapper, {minHeight: TEXT_EVENT_HEIGHT * MIN_EVENTS_NUM}]}>
                        {this.renderEvents(false)}
                    </View>
                </View>
                <Hr style={localStyles.marginBottom0}/>
            </Zebra>
    }

    goToLy = () => {
        this.props.goToLy(this.props.ly);
    }

    renderEvents = (restrict = true) => {
        let events = [];

        for(let eventType in this.props.events) {
            for(let i = 0; i < this.props.events[eventType].length; i++) {
                events.push({text: this.props.events[eventType][i], type: eventType});
            }
        }

        let oversize = 0;
        if(restrict && events.length > MAX_EVENTS) {
            oversize = events.length - MAX_EVENTS + 1;
            events = events.slice(0, MAX_EVENTS - 1);
            events.push({type: '', text: t("%num% more events, tap to see all", {num: oversize})});
        }

        return events.map((event, i) => {
            return this.getEventTextElement(event, i);
        });
    }

    getEventTextElement = (event, i) => {
        let text = event.text;
        let extra = "";
        let config = getEventConfigForType(event.type);

        if(event.type === "storyEvents") {
            extra = getExpansionParensForStoryEvent(text);
        }

        if(config && config[text]) {
            text = config[text].title;
        }

        let title = getEventTitleForType(event.type);

        return <AppText style={styles.itemText} numberOfLines={1} key={i}>
            {title ? title + ':' : ''} {text} {extra}
        </AppText>
    }
}

TimelineListItem.propTypes = {
    events: PropTypes.object.isRequired,
    ly: PropTypes.number.isRequired,
    isActiveLy: PropTypes.bool,
    goToLy: PropTypes.func.isRequired,
    isPassed: PropTypes.bool,
}

TimelineListItem.defaultProps = {
    isActiveLy: false,
    isPassed: false,
}

const localStyles = StyleSheet.create({
    marginBottom0: {
        marginBottom: 0,
    }
});
