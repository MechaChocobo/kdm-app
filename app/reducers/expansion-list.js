import { SET_EXPANSION_LIST, SET_EXPANSION_LIST_LOADING, SET_EXPANSION_LIST_FILTER } from "../actions/expansion-list";
import { RESET } from "../actions/global";

let initialState = {
    expansions: {},
    listUpdateTime: '',
    loading: false,
    filter: 'all',
};

export const expansionList = (state = initialState, action) => {
    if(action.type === SET_EXPANSION_LIST) {
        let newState = {...state};
        newState.expansions = action.expansions;
        newState.listUpdateTime = Date.now();
        return newState;
    } else if(action.type === SET_EXPANSION_LIST_LOADING) {
        let newState = {...state};
        newState.loading = action.value;
        return newState;
    } else if(action.type === SET_EXPANSION_LIST_FILTER) {
        let newState = {...state};
        newState.filter = action.value;
        return newState;
    } else if(action.type === RESET) {
        let newState = {...state};
        newState.expansions = [];
        newState.listUpdateTime = '';
        newState.filter = 'all';
        return newState;
    } else {
        return state;
    }
}
