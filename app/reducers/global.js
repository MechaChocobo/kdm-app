import { SET_HOST_IP_ADDRESS } from "../actions/network";
import { UPDATE_HOST_CONTACT_TIME, SET_INTEGRITY_CHECK } from "../actions/global";

let initialState = {
    hostIp: '',
    lastHostContact: null,
    integrityCheck: false,
};

export const global = (state = initialState, action) => {
    if(action.type === SET_HOST_IP_ADDRESS) {
        let newState = {...state};
        newState.hostIp = action.val;
        return newState;
    } else if(action.type === UPDATE_HOST_CONTACT_TIME) {
        let newState = {...state};
        newState.lastHostContact = Date.now();
        return newState;
    }   else if(action.type === SET_INTEGRITY_CHECK) {
        let newState = {...state};
        newState.integrityCheck = action.value;
        return newState;
    } else {
        return state;
    }
}
