import {pageReducer} from './page';
import {settlementReducer} from './settlement';
import {survivorReducer} from './survivors';
import {timelineReducer} from './timeline';
import {networkReducer} from './network';
import {settingsReducer} from './settings';
import {purchases} from './purchases';
import {customExpansions} from './custom-expansions';
import {settlementCreate} from './settlementCreate';
import {global} from './global';
import {milestones} from './milestones';
import {expansionList} from './expansion-list';

// Combine all the reducers
const rootReducer = {
    pageReducer,
    settlementReducer,
    survivorReducer,
    timelineReducer,
    networkReducer,
    settingsReducer,
    purchases,
    customExpansions,
    settlementCreate,
    global,
    milestones,
    expansionList,
};

export default rootReducer;
