import { SET_SHOULD_SHOW_ADS, SET_IS_SUBSCRIBER, ADD_PURCHASED_ITEM, SET_PURCHASED_ITEMS } from "../actions/purchases";
import { RESET } from "../actions/global";

let initialState = {shouldShowAds: true, isSubscriber: false, purchasedItems: []};
export const purchases = (state = initialState, action) => {
    if(action.type === SET_SHOULD_SHOW_ADS) {
        let newState = {...state};
        newState.shouldShowAds = action.shouldShow;
        return newState;
    } else if(action.type === SET_IS_SUBSCRIBER) {
        let newState = {...state};
        newState.isSubscriber = action.isSubscriber;

        if(newState.isSubscriber === true) {
            newState.shouldShowAds = false;
        }

        return newState;
    } else if(action.type === RESET) {
        let newState = {...state};

        newState.isSubscriber = false;
        newState.shouldShowAds = true;

        return newState;
    } else if(action.type === SET_PURCHASED_ITEMS) {
        let newState = {...state};
        newState.purchasedItems = action.items;
        return newState;
    } else if(action.type === ADD_PURCHASED_ITEM) {
        let newState = {...state};
        newState.purchasedItems.push(action.item);
        return newState;
    } else {
        return state;
    }
}
