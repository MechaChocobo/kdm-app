import CompareVersions from 'compare-versions';

import { INIT_TIMELINE, ADD_TO_TIMELINE, REMOVE_FROM_TIMELINE, IMPORT_TIMELINE, ADD_YEAR, TIMELINE_ADD_ACTION_LOG } from "../actions/timeline";
import { DELETE_SETTLEMENT, COPY_SETTLEMENT, DELETE_ALL_STORY_EVENT, DELETE_ALL_SHOWDOWN, DELETE_ALL_NEMESIS_ENCOUNTERS, DELETE_ALL_SETTLEMENT_EVENT } from "../actions/settlement";
import { SCHEMA_VALIDATE } from "../actions/settings";

import {initTimeline, updateEventsTo17, getStoryEventConfig, getShowdownConfig, addActionLog, getSettlementEventConfig, findItemsNotInConfig, removeItemsFromList} from "../helpers/helpers";
import { DO_INTEGRITY_CHECK } from '../actions/global';

let timelineState = {'logs' : {}};
export const timelineReducer = (state = timelineState, action) => {
    if(action.type == INIT_TIMELINE) {
        let newState = {...state};
        let timeline = initTimeline(action.campaign, action.expansions, action.customTimelines);
        newState[action.id] = timeline;
        newState.logs[action.id] = [];
        return newState;
    } else if(action.type == ADD_TO_TIMELINE) {
        let newState = {...state};
        newState[action.id] = [...newState[action.id]];
        newState[action.id][action.ly] = {...newState[action.id][action.ly]};
        newState[action.id][action.ly][action.eventType] = [...newState[action.id][action.ly][action.eventType], action.value];
        return newState;
    } else if(action.type == REMOVE_FROM_TIMELINE) {
        let newState = {...state};
        newState[action.id] = [...newState[action.id]];
        newState[action.id][action.ly] = {...newState[action.id][action.ly]};
        newState[action.id][action.ly][action.eventType] = [...newState[action.id][action.ly][action.eventType]];
        newState[action.id][action.ly][action.eventType].splice(newState[action.id][action.ly][action.eventType].indexOf(action.value), 1);
        return newState;
    } else if(action.type == IMPORT_TIMELINE) {
        let newState = {...state};
        newState[action.id] = action.timeline;
        return newState;
    } else if(action.type == ADD_YEAR) {
        let newState = {...state};
        newState[action.id] = [...newState[action.id]]
        newState[action.id].push({
            "settlementEvents":  [],
            "storyEvents":       [],
            "nemesisEncounters": [],
            "specialShowdowns":  [],
            "showdowns":         []
        });
        return newState;
    } else if(action.type == DELETE_SETTLEMENT) {
        let newState = {...state};
        delete newState[action.id];
        delete newState.logs[action.id];
        return newState;
    } else if(action.type == COPY_SETTLEMENT) {
        let newState = {...state};
        newState[action.newId] = JSON.parse(JSON.stringify(newState[action.oldId]));
        return newState;
    } else if(action.type == DELETE_ALL_STORY_EVENT) {
        let newState = {...state};
        for(let i = 0; i < newState[action.settlementId].length; i++) {
            if(newState[action.settlementId][i].storyEvents.indexOf(action.event) !== -1) {
                newState[action.settlementId][i].storyEvents = [...newState[action.settlementId][i].storyEvents];
                newState[action.settlementId][i].storyEvents.splice(newState[action.settlementId][i].storyEvents.indexOf(action.event), 1);
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_SHOWDOWN) {
        let newState = {...state};
        for(let i = 0; i < newState[action.settlementId].length; i++) {
            if(newState[action.settlementId][i].showdowns.indexOf(action.event) !== -1) {
                newState[action.settlementId][i].showdowns = [...newState[action.settlementId][i].showdowns];
                newState[action.settlementId][i].showdowns.splice(newState[action.settlementId][i].showdowns.indexOf(action.event), 1);
            }

            if(newState[action.settlementId][i].specialShowdowns.indexOf(action.event) !== -1) {
                newState[action.settlementId][i].specialShowdowns = [...newState[action.settlementId][i].specialShowdowns];
                newState[action.settlementId][i].specialShowdowns.splice(newState[action.settlementId][i].specialShowdowns.indexOf(action.event), 1);
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_NEMESIS_ENCOUNTERS) {
        let newState = {...state};
        for(let i = 0; i < newState[action.settlementId].length; i++) {
            if(newState[action.settlementId][i].nemesisEncounters.indexOf(action.event) !== -1) {
                newState[action.settlementId][i].nemesisEncounters = [...newState[action.settlementId][i].nemesisEncounters];
                newState[action.settlementId][i].nemesisEncounters.splice(newState[action.settlementId][i].nemesisEncounters.indexOf(action.event), 1);
            }

            if(newState[action.settlementId][i].specialShowdowns.indexOf(action.event) !== -1) {
                newState[action.settlementId][i].specialShowdowns = [...newState[action.settlementId][i].specialShowdowns];
                newState[action.settlementId][i].specialShowdowns.splice(newState[action.settlementId][i].specialShowdowns.indexOf(action.event), 1);
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_SETTLEMENT_EVENT) {
        let newState = {...state};
        for(let i = 0; i < newState[action.settlementId].length; i++) {
            if(newState[action.settlementId][i].settlementEvents.indexOf(action.event) !== -1) {
                newState[action.settlementId][i].settlementEvents = [...newState[action.settlementId][i].settlementEvents];
                newState[action.settlementId][i].settlementEvents.splice(newState[action.settlementId][i].settlementEvents.indexOf(action.event), 1);
            }
        }
        return newState;
    } else if(action.type == TIMELINE_ADD_ACTION_LOG) {
        let newState = {...state};
        if(!newState.logs) {
            newState.logs = {};
        }

        if(!newState.logs[action.id]) {
            newState.logs[action.id] = [];
        }

        newState.logs[action.id] = addActionLog([...newState.logs[action.id]], action.log, action.force);
        return newState;
    } else if(action.type === DO_INTEGRITY_CHECK) {
        let newState = {...state};

        const StoryEventConfig = getStoryEventConfig();
        const ShowdownConfig = getShowdownConfig();
        const SettlementEventConfig = getSettlementEventConfig();

        let extra = [];
        for(let id in newState) {
            for(let i = 0; i < newState[id].length; i++) {
                if(extra = findItemsNotInConfig(newState[id][i].storyEvents, StoryEventConfig)) {
                    newState[id][i].storyEvents = removeItemsFromList(extra, newState[id][i].storyEvents);
                }

                if(extra = findItemsNotInConfig(newState[id][i].showdowns, ShowdownConfig)) {
                    newState[id][i].showdowns = removeItemsFromList(extra, newState[id][i].showdowns);
                }

                if(extra = findItemsNotInConfig(newState[id][i].nemesisEncounters, ShowdownConfig)) {
                    newState[id][i].nemesisEncounters = removeItemsFromList(extra, newState[id][i].nemesisEncounters);
                }
                if(extra = findItemsNotInConfig(newState[id][i].specialShowdowns, ShowdownConfig)) {
                    newState[id][i].specialShowdowns = removeItemsFromList(extra, newState[id][i].specialShowdowns);
                }

                if(extra = findItemsNotInConfig(newState[id][i].settlementEvents, SettlementEventConfig)) {
                    newState[id][i].settlementEvents = removeItemsFromList(extra, newState[id][i].settlementEvents);
                }
            }
        }

        return newState;
    } else if(action.type == SCHEMA_VALIDATE) {
        let newState = {...state};
        if(CompareVersions(action.currentVersion, "1.4.1") === -1) {
            for(let id in newState) {
                if(newState[id][25] && newState[id][25].nemesisEncounters && newState[id][25].nemesisEncounters[0] === "The Great Devourer") {
                    if(newState[id][21].nemesisEncounters && newState[id][21].nemesisEncounters[0] === "King's Man") {
                        newState[id][21].nemesisEncounters[0] = "King's Man Level 2";
                    }

                    if(newState[id][22].nemesisEncounters && newState[id][22].nemesisEncounters[0] === "Butcher") {
                        newState[id][22].nemesisEncounters[0] = "Butcher Level 3";
                    }

                    if(newState[id][23].nemesisEncounters && newState[id][23].nemesisEncounters[0] === "King's Man") {
                        newState[id][23].nemesisEncounters[0] = "King's Man Level 3";
                    }

                    if(newState[id][24].nemesisEncounters && newState[id][24].nemesisEncounters[0] === "The Hand") {
                        newState[id][24].nemesisEncounters[0] = "The Hand Level 3";
                    }
                }
            }
        }

        if(CompareVersions(action.currentVersion, "1.7.0") === -1) {
            let ShowdownConfig = getShowdownConfig();
            let StoryEventConfig = getStoryEventConfig();
            for(let id in newState) {
                for(let i = 0; i < newState[id].length; i++) {
                    newState[id][i].storyEvents = updateEventsTo17(newState[id][i].storyEvents, StoryEventConfig);
                    newState[id][i].showdowns = updateEventsTo17(newState[id][i].showdowns, ShowdownConfig);
                    newState[id][i].nemesisEncounters = updateEventsTo17(newState[id][i].nemesisEncounters, ShowdownConfig);
                }
            }
        }

        if(CompareVersions(action.currentVersion, "2.2.0") === -1) {
            newState.logs = {};
            for(let id in newState) {
                newState.logs[id] = [];
            }
        }

        return newState;
    } else {
        return state;
    }
};
