import CompareVersions from 'compare-versions';

import {
    SURVIVAL_INCREASE, SURVIVAL_DECREASE,
    POPULATION_INCREASE, POPULATION_DECREASE,
    DEATHCOUNT_INCREASE, DEATHCOUNT_DECREASE,
    INNOVATION_ADD, INNOVATION_REMOVE,
    ENDEAVOR_INCREASE, ENDEAVOR_DECREASE,
    RESOURCE_INCREASE, RESOURCE_DECREASE,
    GEAR_INCREASE, GEAR_DECREASE,
    LOCATION_ADD, LOCATION_REMOVE,
    NAME_CHANGE,
    PRINCIPLE_SET,
    PRINCIPLE_UNSET,
    CREATE_SETTLEMENT,
    IMPORT_SETTLEMENT,
    END_YEAR, UNEND_YEAR,
    INSPIRATIONAL_STATUE_SET,
    NOTE_ADD_SETTLEMENT, NOTE_REMOVE_SETTLEMENT,
    DELETE_SETTLEMENT,
    TOGGLE_ARCHIVE,
    ADD_EXPANSIONS,
    INNOVATION_DECK_ADD,
    INNOVATION_DECK_REMOVE,
    COPY_SETTLEMENT,
    SET_SURVIVOR_SORT_ATTR,
    SET_SURVIVOR_SORT_ORDER,
    EDIT_NOTE_SETTLEMENT,
    RESOURCE_DELETE,
    GEAR_DELETE,
    DELETE_ALL_FIGHTING_ART,
    DELETE_ALL_DISORDER,
    DELETE_ALL_ABILITY,
    DELETE_ALL_SEVERE_INJURY,
    DELETE_ALL_STORY_EVENT,
    DELETE_ALL_SHOWDOWN,
    DELETE_ALL_NEMESIS_ENCOUNTER,
    DELETE_ALL_SETTLEMENT_EVENT,
    ADD_CUSTOM_EXPANSION_TO_SETTLEMENT,
    REMOVE_CUSTOM_EXPANSION_FROM_SETTLEMENT,
    DOWNGRADE_CUSTOM_EXPANSION_IN_SETTLEMENT,
    DELETE_ALL_WEAPON_PROFICIENCY,
    GEAR_CURSED_DELETE,
    ENDEAVOR_SET,
    SETTLEMENT_ADD_ACTION_LOG,
    CRAFT_GEAR,
    SETTLEMENT_SET_MILESTONE_VALUE,
    SETTLEMENT_INNOVATION_ADD_ALL,
    SETTLEMENT_FIGHTING_ART_ADD_ALL,
    SETTLEMENT_FIGHTING_ART_REMOVE_ALL,
    SETTLEMENT_INNOVATION_REMOVE_ALL,
    SETTLEMENT_DISORDER_ADD_ALL,
    SETTLEMENT_DISORDER_REMOVE_ALL,
    SETTLEMENT_SE_EVENT_ADD_ALL,
    SETTLEMENT_SE_EVENT_REMOVE_ALL,
    SETTLEMENT_LOCATION_ADD_ALL,
    SETTLEMENT_LOCATION_REMOVE_ALL,
    LOADOUT_DELETE,
    SETTLEMENT_WEAPON_PROFICIENCY_ADD_ALL,
    SETTLEMENT_WEAPON_PROFICIENCY_REMOVE_ALL,
    SETTLEMENT_GEAR_REMOVE_ALL,
    SETTLEMENT_GEAR_ADD_ALL,
    SETTLEMENT_RESOURCE_ADD_ALL,
    SETTLEMENT_RESOURCE_REMOVE_ALL,
    SETTLEMENT_ABILITY_ADD_ALL,
    SETTLEMENT_ABILITY_REMOVE_ALL,
    SETTLEMENT_SET_DEPARTING,
    SET_CURRENT_SHOWDOWN,
    ADD_COMPLETED_SHOWDOWN,
    SET_COMPLETED_SHOWDOWNS,
    DELETE_ALL_ARMOR_SET,
    SETTLEMENT_ARMOR_SET_ADD_ALL,
    SETTLEMENT_ARMOR_SET_REMOVE_ALL,
    SETTLEMENT_SET_MILESTONE_COMPLETE,
    SETTLEMENT_ADD_MILESTONE_REWARDS,
    SETTLEMENT_REMOVE_MILESTONE_REWARDS,
    SETTLEMENT_DELETE_MILESTONE,
    SETTLEMENT_SET_DRAWN_INNOVATIONS,
    SETTLEMENT_ADD_QUARRY,
    SETTLEMENT_REMOVE_QUARRY,
    ADD_PARTIAL_EXPANSION,
    REMOVE_PARTIAL_EXPANSION,
} from '../actions/settlement';


import {createSettlement, setPrinciple, unsetPrinciple, toCamelCase, addExpansionsToSettlement, addCursedGearToSettlement, addCustomExpansionToSettlement, sortStorage, getInnovationConfig, getLocationConfig, addStorageToSettlement, getStoryEventConfig, updateEventsTo17, getShowdownConfig, addActionLog, getResourceConfig, findItemsNotInConfig, removeItemsFromList, getFightingArtConfig, getDisorderConfig, getAbilityConfig, getGearConfig, addArmorSetsToSettlement, getMilestoneConfig, addItemToSettlementAttr, addItemToStorage, findItemsNotInStorageConfig, removeStorageItemsFromList, getSettlementEventConfig, getSevereInjuryConfig, getShowdownsFromQuarry, addItemToAll, removeItemFromAll} from '../helpers/helpers';
import {addInnovationToSettlement, removeInnovationFromSettlement, getInnovationDeck} from '../helpers/innovations';
import { DEAD_SET, LOADOUT_SAVE } from '../actions/survivors';
import { SCHEMA_VALIDATE } from '../actions/settings';
import { RESET, DO_INTEGRITY_CHECK } from '../actions/global';
import { DELETE_ALL_SETTLEMENT_MILESTONE_CONDITION } from '../actions/custom-expansions';
import { GOTO_SETTLEMENT } from '../actions/router';

let settlementState = { settlements: {} };
export const settlementReducer = (state = settlementState, action) => {
    if (action.type == SURVIVAL_INCREASE) {
        let newState = { ...state };
        newState.settlements[action.id].survivalLimit = newState.settlements[action.id].survivalLimit + 1;
        return newState;
    } else if (action.type == SURVIVAL_DECREASE) {
        let newState = { ...state };
        if (newState.settlements[action.id].survivalLimit > 1) {
            newState.settlements[action.id].survivalLimit = newState.settlements[action.id].survivalLimit - 1;
        }
        return newState;
    } else if (action.type == POPULATION_INCREASE) {
        let newState = { ...state };
        newState.settlements[action.id].population = newState.settlements[action.id].population + 1;
        return newState;
    } else if (action.type == POPULATION_DECREASE) {
        let newState = { ...state };
        if (newState.settlements[action.id].population > 0) {
            newState.settlements[action.id].population = newState.settlements[action.id].population - 1;
        }
        return newState;
    } else if (action.type == DEATHCOUNT_INCREASE) {
        let newState = { ...state };
        newState.settlements[action.id].deathCount = newState.settlements[action.id].deathCount + 1;
        return newState;
    } else if (action.type == DEATHCOUNT_DECREASE) {
        let newState = { ...state };
        if (newState.settlements[action.id].deathCount > 0) {
            newState.settlements[action.id].deathCount = newState.settlements[action.id].deathCount - 1;
        }
        return newState;
    } else if (action.type == ENDEAVOR_INCREASE) {
        let newState = { ...state };
        newState.settlements[action.id].endeavors = newState.settlements[action.id].endeavors + 1;
        return newState;
    } else if (action.type == ENDEAVOR_DECREASE) {
        let newState = { ...state };
        if (newState.settlements[action.id].endeavors > 0) {
            newState.settlements[action.id].endeavors = newState.settlements[action.id].endeavors - 1;
        }
        return newState;
    } else if (action.type == ENDEAVOR_SET) {
        let newState = { ...state };
        newState.settlements[action.id].endeavors = action.num;
        return newState;
    } else if (action.type == INNOVATION_ADD) {
        let newState = { ...state };
        let settlement = addInnovationToSettlement(newState.settlements[action.id], action.name);
        newState.settlements[action.id] = settlement;
        return newState;
    } else if (action.type == INNOVATION_REMOVE) {
        let newState = { ...state };
        let settlement = removeInnovationFromSettlement(newState.settlements[action.id], action.name);
        if(action.all === true && settlement.allInnovations.indexOf(action.name) !== -1) {
            settlement.allInnovations.splice(settlement.allInnovations.indexOf(action.name), 1);
        }
        newState.settlements[action.id] = settlement;
        return newState;
    } else if (action.type == RESOURCE_INCREASE) {
        let newState = { ...state };
        newState.settlements[action.id].storage = {...newState.settlements[action.id].storage};
        newState.settlements[action.id].storage.resources = {...newState.settlements[action.id].storage.resources};
        newState.settlements[action.id].storage.resources[action.deck][action.name]++;

        return newState;
    } else if (action.type == RESOURCE_DECREASE) {
        let newState = { ...state };
        newState.settlements[action.id].storage = {...newState.settlements[action.id].storage};
        newState.settlements[action.id].storage.resources = {...newState.settlements[action.id].storage.resources};
        if(newState.settlements[action.id].storage.resources[action.deck][action.name] > 0) {
            newState.settlements[action.id].storage.resources[action.deck][action.name]--;
        }

        return newState;
    } else if (action.type == RESOURCE_DELETE) {
        if(!state.settlements[action.id].storage.resources[action.deck]) {
            return state;
        }

        let newState = { ...state };
        newState.settlements[action.id].storage = {...newState.settlements[action.id].storage};
        newState.settlements[action.id].storage.resources = {...newState.settlements[action.id].storage.resources};
        newState.settlements[action.id].storage.resources[action.deck] = {...newState.settlements[action.id].storage.resources[action.deck]};
        delete newState.settlements[action.id].storage.resources[action.deck][action.resourceId];

        if(Object.keys(newState.settlements[action.id].storage.resources[action.deck]).length === 0) {
            delete newState.settlements[action.id].storage.resources[action.deck];
        }

        return newState;
    } else if (action.type == GEAR_INCREASE) {
        let newState = { ...state };
        newState.settlements[action.id].storage = {...newState.settlements[action.id].storage};
        newState.settlements[action.id].storage.gear = {...newState.settlements[action.id].storage.gear};
        newState.settlements[action.id].storage.gear[action.deck][action.name]++;
        return newState;
    } else if (action.type == GEAR_DECREASE) {
        let newState = { ...state };
        newState.settlements[action.id].storage = {...newState.settlements[action.id].storage};
        newState.settlements[action.id].storage.gear = {...newState.settlements[action.id].storage.gear};
        if(newState.settlements[action.id].storage.gear[action.deck][action.name] > 0) {
            newState.settlements[action.id].storage.gear[action.deck][action.name]--;
        }

        return newState;
    } else if (action.type == GEAR_DELETE) {
        if(!state.settlements[action.id].storage.gear[action.deck]) {
            return state;
        }

        let newState = { ...state };
        newState.settlements[action.id].storage = {...newState.settlements[action.id].storage};
        newState.settlements[action.id].storage.gear = {...newState.settlements[action.id].storage.gear};

        delete newState.settlements[action.id].storage.gear[action.deck][action.gearId];

        if(Object.keys(newState.settlements[action.id].storage.gear[action.deck]).length === 0) {
            delete newState.settlements[action.id].storage.gear[action.deck];
        }

        // clear it from saved loadouts too
        newState.settlements[action.id].loadouts = {...newState.settlements[action.id].loadouts};
        for(let loadoutId in newState.settlements[action.id].loadouts) {
            let gear = JSON.parse(JSON.stringify(newState.settlements[action.id].loadouts[loadoutId].gear));
            for(let x = 0; x < 3; x++) {
                for(let y = 0; y < 3; y++) {
                    if(gear[x][y]) {
                        if(gear[x][y].location === action.deck && gear[x][y].name === action.gearId) {
                            gear[x][y] = null;
                        }
                    }
                }
            }
            newState.settlements[action.id].loadouts[loadoutId].gear = gear;
        }

        return newState;
    } else if (action.type == LOCATION_ADD) {
        let newState = { ...state };
        newState.settlements[action.id].locations = [...newState.settlements[action.id].locations, action.name];
        return newState;
    } else if (action.type == LOCATION_REMOVE) {
        let newState = { ...state };
        newState.settlements[action.id].locations = [...newState.settlements[action.id].locations];
        newState.settlements[action.id].locations.splice(newState.settlements[action.id].locations.indexOf(action.name), 1);
        if(action.all === true && newState.settlements[action.id].allLocations.indexOf(action.name) !== -1) {
            newState.settlements[action.id].allLocations.splice(newState.settlements[action.id].allLocations.indexOf(action.name), 1);
        }
        return newState;
    } else if (action.type == NAME_CHANGE) {
        let newState = { ...state };
        newState.settlements[action.id] = {...newState.settlements[action.id]};
        newState.settlements[action.id].name = action.name;
        return newState;
    } else if (action.type == PRINCIPLE_SET) {
        let newState = { ...state };
        let settlement = setPrinciple(newState.settlements[action.id], action.principle, action.name, newState.settlements[action.id].principles[action.principle]);
        newState.settlements[action.id] = settlement;
        return newState;
    } else if (action.type == PRINCIPLE_UNSET) {
        let newState = { ...state };
        let settlement = unsetPrinciple(newState.settlements[action.id], action.principle);
        newState.settlements[action.id] = settlement;
        return newState;
    } else if (action.type == CREATE_SETTLEMENT) {
        let newState = { ...state };
        let settlement = createSettlement(action.id, action.name, action.campaign, action.expansions);
        newState.settlements[settlement.id] = settlement;
        return newState;
    } else if (action.type == IMPORT_SETTLEMENT) {
        let newState = { ...state };
        newState.settlements = { ...newState.settlements };
        newState.settlements[action.settlement.id] = action.settlement;
        return newState;
    } else if (action.type == END_YEAR) {
        let newState = { ...state };
        newState.settlements[action.id].ly = newState.settlements[action.id].ly + 1;
        return newState;
    } else if (action.type == UNEND_YEAR) {
        let newState = { ...state };
        newState.settlements[action.id].ly = newState.settlements[action.id].ly - 1;
        return newState;
    } else if (action.type == INSPIRATIONAL_STATUE_SET) {
        let newState = { ...state };
        newState.settlements[action.id].inspirationalStatue = action.name;
        return newState;
    } else if (action.type == NOTE_ADD_SETTLEMENT) {
        let newState = { ...state };
        newState.settlements[action.id].notes.push(action.note);
        return newState;
    } else if (action.type == NOTE_REMOVE_SETTLEMENT) {
        let newState = { ...state };
        newState.settlements[action.id].notes = [...newState.settlements[action.id].notes];
        for(var i = 0; i < newState.settlements[action.id].notes.length; i++) {
            if(newState.settlements[action.id].notes[i] === action.note) {
                newState.settlements[action.id].notes.splice(i, 1);
                break;
            }
        }
        return newState;
    } else if (action.type == EDIT_NOTE_SETTLEMENT) {
        let newState = { ...state };
        newState.settlements[action.id].notes = action.note;
        return newState;
    } else if (action.type == DELETE_SETTLEMENT) {
        let newState = { ...state };
        newState.settlements = { ...newState.settlements };
        delete newState.settlements[action.id];
        return newState;
    } else if (action.type == TOGGLE_ARCHIVE) {
        let newState = { ...state };
        newState.settlements = { ...newState.settlements };
        newState.settlements[action.id].archived = !newState.settlements[action.id].archived;
        return newState;
    } else if (action.type == DEAD_SET) {
        let newState = { ...state };
        if(action.dead === true) {
            newState.settlements[action.settlementId].population = newState.settlements[action.settlementId].population - 1;
            newState.settlements[action.settlementId].deathCount = newState.settlements[action.settlementId].deathCount + 1;
        } else if(action.dead === false) {
            newState.settlements[action.settlementId].population = newState.settlements[action.settlementId].population + 1;
            newState.settlements[action.settlementId].deathCount = newState.settlements[action.settlementId].deathCount - 1;
        }
        return newState;
    } else if (action.type == ADD_EXPANSIONS) {
        let newState = { ...state };

        newState.settlements[action.id] = addExpansionsToSettlement(newState.settlements[action.id], action.expansions);

        return newState;
    } else if (action.type == INNOVATION_DECK_ADD) {
        let newState = { ...state };

        if(newState.settlements[action.id].innovationDeck.indexOf(action.name) === -1) {
            newState.settlements[action.id].innovationDeck = [...newState.settlements[action.id].innovationDeck];
            newState.settlements[action.id].innovationDeck.push(action.name);
        }

        return newState;
    } else if (action.type == INNOVATION_DECK_REMOVE) {
        let newState = { ...state };

        if(newState.settlements[action.id].innovationDeck.indexOf(action.name) !== -1) {
            newState.settlements[action.id].innovationDeck = [...newState.settlements[action.id].innovationDeck];
            newState.settlements[action.id].innovationDeck.splice(newState.settlements[action.id].innovationDeck.indexOf(action.name), 1);
        }

        return newState;
    } else if (action.type == COPY_SETTLEMENT) {
        let newState = { ...state };

        newState.settlements[action.newId] = JSON.parse(JSON.stringify(newState.settlements[action.oldId]));
        newState.settlements[action.newId].name = newState.settlements[action.newId].name + " (Copy)";
        newState.settlements[action.newId].id = action.newId;

        return newState;
    } else if (action.type == SET_SURVIVOR_SORT_ATTR) {
        let newState = { ...state };

        newState.settlements[action.id].survivorSortAttr = action.attr;

        return newState;
    } else if (action.type == SET_SURVIVOR_SORT_ORDER) {
        let newState = { ...state };

        newState.settlements[action.id].survivorSortOrder = action.order;

        return newState;
    } else if (action.type == DELETE_ALL_FIGHTING_ART) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allFightingArts.indexOf(action.faId) !== -1) {
            newState.settlements[action.settlementId].allFightingArts = [...newState.settlements[action.settlementId].allFightingArts];
            newState.settlements[action.settlementId].allFightingArts.splice(newState.settlements[action.settlementId].allFightingArts.indexOf(action.faId), 1);
        }

        return newState;
    } else if (action.type == DELETE_ALL_DISORDER) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allDisorders.indexOf(action.disorderId) !== -1) {
            newState.settlements[action.settlementId].allDisorders = [...newState.settlements[action.settlementId].allDisorders];
            newState.settlements[action.settlementId].allDisorders.splice(newState.settlements[action.settlementId].allDisorders.indexOf(action.disorderId), 1);
        }


        return newState;
    } else if (action.type == DELETE_ALL_ABILITY) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allAbilities.indexOf(action.abilityId) !== -1) {
            newState.settlements[action.settlementId].allAbilities = [...newState.settlements[action.settlementId].allAbilities];
            newState.settlements[action.settlementId].allAbilities.splice(newState.settlements[action.settlementId].allAbilities.indexOf(action.abilityId), 1);
        }

        return newState;
    } else if (action.type == DELETE_ALL_WEAPON_PROFICIENCY) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allProficiencies.indexOf(action.profId) !== -1) {
            newState.settlements[action.settlementId].allProficiencies = [...newState.settlements[action.settlementId].allProficiencies];
            newState.settlements[action.settlementId].allProficiencies.splice(newState.settlements[action.settlementId].allProficiencies.indexOf(action.profId), 1);
        }

        return newState;
    } else if (action.type == DELETE_ALL_ARMOR_SET) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allArmorSets.indexOf(action.armorId) !== -1) {
            newState.settlements[action.settlementId].allArmorSets = [...newState.settlements[action.settlementId].allArmorSets];
            newState.settlements[action.settlementId].allArmorSets.splice(newState.settlements[action.settlementId].allArmorSets.indexOf(action.armorId), 1);
        }

        return newState;
    } else if (action.type == DELETE_ALL_SEVERE_INJURY) {
        let newState = { ...state };
        if(newState.settlements[action.settlementId].allSevereInjuries.indexOf(action.injuryId) !== -1) {
            newState.settlements[action.settlementId].allSevereInjuries = [...newState.settlements[action.settlementId].allSevereInjuries];
            newState.settlements[action.settlementId].allSevereInjuries.splice(newState.settlements[action.settlementId].allSevereInjuries.indexOf(action.injuryId), 1);
        }

        return newState;
    } else if (action.type == DELETE_ALL_STORY_EVENT) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allStoryEvents.indexOf(action.event) !== -1) {
            newState.settlements[action.settlementId].allStoryEvents = [...newState.settlements[action.settlementId].allStoryEvents];
            newState.settlements[action.settlementId].allStoryEvents.splice(newState.settlements[action.settlementId].allStoryEvents.indexOf(action.event), 1);
        }

        return newState;
    } else if (action.type == DELETE_ALL_SHOWDOWN) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allShowdowns.indexOf(action.event) !== -1) {
            newState.settlements[action.settlementId].allShowdowns = [...newState.settlements[action.settlementId].allShowdowns];
            newState.settlements[action.settlementId].allShowdowns.splice(newState.settlements[action.settlementId].allShowdowns.indexOf(action.event), 1);
        }

        newState.settlements[action.settlementId].quarryShowdowns = removeItemFromAll(newState.settlements[action.settlementId].allShowdowns, action.event);

        return newState;
    } else if (action.type == DELETE_ALL_NEMESIS_ENCOUNTER) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allNemesisEncounters.indexOf(action.event) !== -1) {
            newState.settlements[action.settlementId].allNemesisEncounters = [...newState.settlements[action.settlementId].allNemesisEncounters];
            newState.settlements[action.settlementId].allNemesisEncounters.splice(newState.settlements[action.settlementId].allNemesisEncounters.indexOf(action.event), 1);
        }

        return newState;
    } else if (action.type == DELETE_ALL_SETTLEMENT_EVENT) {
        let newState = { ...state };

        if(newState.settlements[action.settlementId].allSettlementEvents.indexOf(action.event) !== -1) {
            newState.settlements[action.settlementId].allSettlementEvents = [...newState.settlements[action.settlementId].allSettlementEvents];
            newState.settlements[action.settlementId].allSettlementEvents.splice(newState.settlements[action.settlementId].allSettlementEvents.indexOf(action.event), 1);
        }

        return newState;
    } else if (action.type == ADD_CUSTOM_EXPANSION_TO_SETTLEMENT) {
        let newState = { ...state };
        newState.settlements[action.settlementId] = addCustomExpansionToSettlement(newState.settlements[action.settlementId], action.expansion, action.mode);
        return newState;
    } else if (action.type == REMOVE_CUSTOM_EXPANSION_FROM_SETTLEMENT) {
        let newState = { ...state };
        if(newState.settlements[action.settlementId].customExpansions[action.expansion]) {
            newState.settlements[action.settlementId].customExpansions = {...newState.settlements[action.settlementId].customExpansions};
            delete newState.settlements[action.settlementId].customExpansions[action.expansion];
        } else if(newState.settlements[action.settlementId].expansions[action.expansion]) {
            newState.settlements[action.settlementId].expansions = {...newState.settlements[action.settlementId].expansions};
            delete newState.settlements[action.settlementId].expansions[action.expansion];
        }
        return newState;
    } else if (action.type == DOWNGRADE_CUSTOM_EXPANSION_IN_SETTLEMENT) {
        let newState = { ...state };
        newState.settlements[action.settlementId].customExpansions = {...newState.settlements[action.settlementId].customExpansions};
        for(let i = 0; i < action.expansions.length; i++) {
            newState.settlements[action.settlementId].customExpansions[action.expansions[i]] = action.type;
        }
        return newState;
    } else if (action.type == GEAR_CURSED_DELETE) {
        let newState = { ...state };
        let cursed = [...newState.settlements[action.id].allCursedItems];

        for(let i = 0; i < newState.settlements[action.id].allCursedItems.length; i++) {
            if(
                newState.settlements[action.id].allCursedItems[i].location === action.deck &&
                newState.settlements[action.id].allCursedItems[i].item === action.gearId
            ) {
                cursed.splice(i, 1);
                break;
            }
        }
        newState.settlements[action.id].allCursedItems = cursed;
        return newState;
    } else if (action.type == RESET) {
        let newState = { ...state };

        for(let settlementId in newState.settlements) {
            newState.settlements[settlementId].survivorSortAttr = "name";
            newState.settlements[settlementId].survivorSortOrder = "asc";
        }

        return newState;
    } else if(action.type == SETTLEMENT_ADD_ACTION_LOG) {
        let newState = {...state};
        newState.settlements[action.id].log = addActionLog([...newState.settlements[action.id].log], action.log);
        return newState;
    } else if(action.type == CRAFT_GEAR) {
        let newState = {...state};
        newState.settlements[action.id].storage = {...newState.settlements[action.id].storage};
        newState.settlements[action.id].storage.gear = {...newState.settlements[action.id].storage.gear};
        newState.settlements[action.id].storage.resources = {...newState.settlements[action.id].storage.resources};

        newState.settlements[action.id].storage.gear[action.gear.location][action.gear.name] += 1;

        for(let i = 0; i < action.resources.length; i++) {
            newState.settlements[action.id].storage.resources[action.resources[i].location][action.resources[i].name] -= 1;
        }

        for(let i = 0; i < action.gearCost.length; i++) {
            newState.settlements[action.id].storage.gear[action.gearCost[i].location][action.gearCost[i].name] -= 1;
        }

        return newState;
    } else if(action.type == SETTLEMENT_SET_MILESTONE_VALUE) {
        let newState = {...state};

        newState.settlements[action.id].milestones = {...newState.settlements[action.id].milestones};
        newState.settlements[action.id].milestones[action.milestoneId].conditions = {...newState.settlements[action.id].milestones[action.milestoneId].conditions};
        newState.settlements[action.id].milestones[action.milestoneId].conditions[action.conditionId] = {...newState.settlements[action.id].milestones[action.milestoneId].conditions[action.conditionId]};
        newState.settlements[action.id].milestones[action.milestoneId].conditions[action.conditionId].value = action.value;
        if(action.value >= action.target) {
            newState.settlements[action.id].milestones[action.milestoneId].conditions[action.conditionId].complete = true;
        } else {
            newState.settlements[action.id].milestones[action.milestoneId].conditions[action.conditionId].complete = false;
        }

        return newState;
    } else if(action.type == SETTLEMENT_SET_MILESTONE_COMPLETE) {
        let newState = {...state};
        newState.settlements[action.id].milestones = {...newState.settlements[action.id].milestones};
        newState.settlements[action.id].milestones[action.milestoneId].complete = action.value;
        return newState;
    } else if(action.type == SETTLEMENT_ADD_MILESTONE_REWARDS) {
        let newState = {...state};
        if(action.rewards) {
            if(action.rewards.addFightingArt) {
                for(let i = 0; i < action.rewards.addFightingArt.length; i++) {
                    newState.settlements[action.id].allFightingArts = addItemToAll(newState.settlements[action.id].allFightingArts, action.rewards.addFightingArt[i]);
                }
            }

            if(action.rewards.addGear) {
                const GearConfig = getGearConfig();
                for(let i = 0; i < action.rewards.addGear.length; i++) {
                    newState.settlements[action.id].storage.gear = addItemToAllStorage(newState.settlements[action.id].storage.gear, action.rewards.addGear[i].location, action.rewards.addGear[i].id, GearConfig);
                }
            }

            if(action.rewards.addResource) {
                const ResourceConfig = getResourceConfig();
                for(let i = 0; i < action.rewards.addResource.length; i++) {
                    newState.settlements[action.id].storage.resources = addItemToAllStorage(newState.settlements[action.id].storage.resources, action.rewards.addResource[i].location, action.rewards.addResource[i].id, ResourceConfig);
                }
            }

            if(action.rewards.addSettlementEvent) {
                for(let i = 0; i < action.rewards.addSettlementEvent.length; i++) {
                    newState.settlements[action.id].allSettlementEvents = addItemToAll(newState.settlements[action.id].allSettlementEvents, action.rewards.addSettlementEvent[i]);
                }
            }
        }
        return newState;
    } else if(action.type == SETTLEMENT_REMOVE_MILESTONE_REWARDS) {
        let newState = {...state};
        if(action.rewards) {
            if(action.rewards.addFightingArt) {
                for(let i = 0; i < action.rewards.addFightingArt.length; i++) {
                    newState.settlements[action.id].allFightingArts = removeItemFromAll(newState.settlements[action.id].allFightingArts, action.rewards.addFightingArt[i]);
                }
            }

            if(action.rewards.addGear) {
                for(let i = 0; i < action.rewards.addGear.length; i++) {
                    newState.settlements[action.id].storage.gear = removeItemFromAllStorage(newState.settlements[action.id].storage.gear, action.rewards.addGear[i].location, action.rewards.addGear[i].id);
                }
            }

            if(action.rewards.addResource) {
                for(let i = 0; i < action.rewards.addResource.length; i++) {
                    newState.settlements[action.id].storage.resources = removeItemFromAllStorage(newState.settlements[action.id].storage.resources, action.rewards.addResource[i].location, action.rewards.addResource[i].id);
                }
            }

            if(action.rewards.addSettlementEvent) {
                for(let i = 0; i < action.rewards.addSettlementEvent.length; i++) {
                    newState.settlements[action.id].allSettlementEvents = removeItemFromAll(newState.settlements[action.id].allSettlementEvents, action.rewards.addSettlementEvent[i]);
                }
            }
        }
        return newState;
    } else if(action.type == SETTLEMENT_INNOVATION_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allInnovations = addItemToAll(newState.settlements[action.id].allInnovations, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_INNOVATION_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allInnovations = removeItemFromAll(newState.settlements[action.id].allInnovations, action.name);
        newState.settlements[action.id].innovationDeck = removeItemFromAll(newState.settlements[action.id].innovationDeck, action.name);
        if(newState.settlements[action.id].drawnInnovations && newState.settlements[action.id].drawnInnovations.length > 0) {
            newState.settlements[action.id].drawnInnovations = removeItemFromAll(newState.settlements[action.id].drawnInnovations, action.name);
        }
        return newState;
    } else if(action.type == SETTLEMENT_FIGHTING_ART_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allFightingArts = addItemToAll(newState.settlements[action.id].allFightingArts, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_FIGHTING_ART_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allFightingArts = removeItemFromAll(newState.settlements[action.id].allFightingArts, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_DISORDER_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allDisorders = addItemToAll(newState.settlements[action.id].allDisorders, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_DISORDER_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allDisorders = removeItemFromAll(newState.settlements[action.id].allDisorders, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_SE_EVENT_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allSettlementEvents = addItemToAll(newState.settlements[action.id].allSettlementEvents, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_SE_EVENT_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allSettlementEvents = removeItemFromAll(newState.settlements[action.id].allSettlementEvents, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_LOCATION_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allLocations = addItemToAll(newState.settlements[action.id].allLocations, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_LOCATION_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allLocations = removeItemFromAll(newState.settlements[action.id].allLocations, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_WEAPON_PROFICIENCY_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allProficiencies = addItemToAll(newState.settlements[action.id].allProficiencies, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_WEAPON_PROFICIENCY_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allProficiencies = removeItemFromAll(newState.settlements[action.id].allProficiencies, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_GEAR_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].storage.gear = addItemToAllStorage(newState.settlements[action.id].storage.gear, action.location, action.gearId, getGearConfig());
        return newState;
    } else if(action.type == SETTLEMENT_GEAR_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].storage.gear = removeItemFromAllStorage(newState.settlements[action.id].storage.gear, action.location, action.gearId);
        return newState;
    } else if(action.type == SETTLEMENT_RESOURCE_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].storage.resources = addItemToAllStorage(newState.settlements[action.id].storage.resources, action.location, action.resourceId, getResourceConfig());
        return newState;
    } else if(action.type == SETTLEMENT_RESOURCE_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].storage.resources = removeItemFromAllStorage(newState.settlements[action.id].storage.resources, action.location, action.resourceId);
        return newState;
    } else if(action.type == SETTLEMENT_ABILITY_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allAbilities = addItemToAll(newState.settlements[action.id].allAbilities, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_ABILITY_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allAbilities = removeItemFromAll(newState.settlements[action.id].allAbilities, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_ARMOR_SET_ADD_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allArmorSets = addItemToAll(newState.settlements[action.id].allArmorSets, action.name);
        return newState;
    } else if(action.type == SETTLEMENT_ARMOR_SET_REMOVE_ALL) {
        let newState = {...state};
        newState.settlements[action.id].allArmorSets = removeItemFromAll(newState.settlements[action.id].allArmorSets, action.name);
        return newState;
    } else if(action.type == LOADOUT_SAVE) {
        let newState = {...state};
        if(!newState.settlements[action.settlementId].loadouts) {
            newState.settlements[action.settlementId].loadouts = {};
        } else {
            newState.settlements[action.settlementId].loadouts = {...newState.settlements[action.settlementId].loadouts};
        }

        newState.settlements[action.settlementId].loadouts[action.loadout.id] = JSON.parse(JSON.stringify(action.loadout));

        return newState;
    } else if(action.type == LOADOUT_DELETE) {
        let newState = {...state};
        newState.settlements[action.id].loadouts = {...newState.settlements[action.id].loadouts};
        delete newState.settlements[action.id].loadouts[action.loadoutId];
        return newState;
    } else if(action.type == SETTLEMENT_SET_DEPARTING) {
        let newState = {...state};
        newState.settlements[action.id].departing = action.value;
        return newState;
    } else if(action.type == SET_CURRENT_SHOWDOWN) {
        let newState = {...state};
        newState.settlements[action.id].currentShowdown = action.showdown;
        return newState;
    } else if(action.type == ADD_COMPLETED_SHOWDOWN) {
        let newState = {...state};
        if(!newState.settlements[action.id].completedShowdowns) {
            newState.settlements[action.id].completedShowdowns = [];
        }
        newState.settlements[action.id].completedShowdowns = [...newState.settlements[action.id].completedShowdowns];
        newState.settlements[action.id].completedShowdowns.push(action.showdown);
        return newState;
    } else if(action.type == SET_COMPLETED_SHOWDOWNS) {
        let newState = {...state};
        newState.settlements[action.id].completedShowdowns = action.value;
        return newState;
    } else if(action.type == DELETE_ALL_SETTLEMENT_MILESTONE_CONDITION) {
        let newState = {...state};
        for(let id in newState.settlements) {
            if(newState.settlements[id].milestones[action.id]) {
                delete newState.settlements[id].milestones[action.id].conditions[action.conditionId];
            }
        }
        return newState;
    } else if(action.type == SETTLEMENT_DELETE_MILESTONE) {
        let newState = {...state};
        if(newState.settlements[action.id].milestones[action.milestoneId]) {
            delete newState.settlements[action.id].milestones[action.milestoneId];
        }
        return newState;
    } else if(action.type == SETTLEMENT_SET_DRAWN_INNOVATIONS) {
        let newState = {...state};
        newState.settlements[action.id].drawnInnovations = action.innovations;
        return newState;
    } else if(action.type == SETTLEMENT_ADD_QUARRY) {
        let newState = {...state};
        let showdowns = getShowdownsFromQuarry(action.quarry);
        for(let i = 0; i < showdowns.length; i++) {
            newState.settlements[action.id].quarryShowdowns = addItemToAll(newState.settlements[action.id].quarryShowdowns, showdowns[i]);
        }
        return newState;
    } else if(action.type == SETTLEMENT_REMOVE_QUARRY) {
        let newState = {...state};
        let showdowns = getShowdownsFromQuarry(action.quarry);
        for(let i = 0; i < showdowns.length; i++) {
            newState.settlements[action.id].quarryShowdowns = removeItemFromAll(newState.settlements[action.id].quarryShowdowns, showdowns[i]);
        }
        return newState;
    } else if(action.type == ADD_PARTIAL_EXPANSION) {
        let newState = {...state};
        if(!newState.settlements[action.id].partialExpansions) {
            newState.settlements[action.id].partialExpansions = {};
        }

        // if the expansion is actually assigned then we don't need to track anything here
        if(newState.settlements[action.id].customExpansions[action.expansion]) {
            delete newState.settlements[action.id].partialExpansions[action.expansion];
            return newState;
        }

        if(!newState.settlements[action.id].partialExpansions[action.expansion]) {
            newState.settlements[action.id].partialExpansions[action.expansion] = 1;
        } else {
            newState.settlements[action.id].partialExpansions[action.expansion] += 1;
        }

        return newState;
    } else if(action.type == REMOVE_PARTIAL_EXPANSION) {
        let newState = {...state};
        if(!newState.settlements[action.id].partialExpansions) {
            newState.settlements[action.id].partialExpansions = {};
        }

        // if the expansion is actually assigned then we don't need to track anything here
        if(newState.settlements[action.id].customExpansions[action.expansion]) {
            delete newState.settlements[action.id].partialExpansions[action.expansion];
            return newState;
        }

        if(!newState.settlements[action.id].partialExpansions[action.expansion]) {
            return newState;
        } else {
            newState.settlements[action.id].partialExpansions[action.expansion] -= 1;

            if(newState.settlements[action.id].partialExpansions[action.expansion] === 0) {
                delete newState.settlements[action.id].partialExpansions[action.expansion];
            }
        }

        return newState;
    } else if(action.type === DO_INTEGRITY_CHECK) {
        let newState = {...state};
        const LocationConfig = getLocationConfig();
        const InnovationConfig = getInnovationConfig();
        const FightingArtConfig = getFightingArtConfig();
        const DisorderConfig = getDisorderConfig();
        const AbilityConfig = getAbilityConfig();
        const MilestoneConfig = getMilestoneConfig();
        const ResourceConfig = getResourceConfig();
        const GearConfig = getGearConfig();
        const StoryEventConfig = getStoryEventConfig();
        const ShowdownConfig = getShowdownConfig();
        const SettlementEventConfig = getSettlementEventConfig();
        const SevereInjuryConfig = getSevereInjuryConfig();

        for(let id in newState.settlements) {
            let extra = [];
            if(extra = findItemsNotInConfig(newState.settlements[id].locations, LocationConfig)) {
                newState.settlements[id].locations = removeItemsFromList(extra, newState.settlements[id].locations);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allLocations, LocationConfig)) {
                newState.settlements[id].allLocations = removeItemsFromList(extra, newState.settlements[id].allLocations);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].innovations, InnovationConfig)) {
                newState.settlements[id].innovations = removeItemsFromList(extra, newState.settlements[id].innovations);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allInnovations, InnovationConfig)) {
                newState.settlements[id].allInnovations = removeItemsFromList(extra, newState.settlements[id].allInnovations);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].innovationDeck, InnovationConfig)) {
                newState.settlements[id].innovationDeck = removeItemsFromList(extra, newState.settlements[id].innovationDeck);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].drawnInnovations, InnovationConfig)) {
                newState.settlements[id].drawnInnovations = removeItemsFromList(extra, newState.settlements[id].drawnInnovations);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allFightingArts, FightingArtConfig)) {
                newState.settlements[id].allFightingArts = removeItemsFromList(extra, newState.settlements[id].allFightingArts);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allDisorders, DisorderConfig)) {
                newState.settlements[id].allDisorders = removeItemsFromList(extra, newState.settlements[id].allDisorders);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allAbilities, AbilityConfig)) {
                newState.settlements[id].allAbilities = removeItemsFromList(extra, newState.settlements[id].allAbilities);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allSevereInjuries, SevereInjuryConfig)) {
                newState.settlements[id].allSevereInjuries = removeItemsFromList(extra, newState.settlements[id].allSevereInjuries);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allStoryEvents, StoryEventConfig)) {
                newState.settlements[id].allStoryEvents = removeItemsFromList(extra, newState.settlements[id].allStoryEvents);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allShowdowns, ShowdownConfig)) {
                newState.settlements[id].allShowdowns = removeItemsFromList(extra, newState.settlements[id].allShowdowns);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].quarryShowdowns, ShowdownConfig)) {
                newState.settlements[id].quarryShowdowns = removeItemsFromList(extra, newState.settlements[id].quarryShowdowns);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allNemesisEncounters, ShowdownConfig)) {
                newState.settlements[id].allNemesisEncounters = removeItemsFromList(extra, newState.settlements[id].allNemesisEncounters);
            }

            if(extra = findItemsNotInConfig(newState.settlements[id].allSettlementEvents, SettlementEventConfig)) {
                newState.settlements[id].allSettlementEvents = removeItemsFromList(extra, newState.settlements[id].allSettlementEvents);
            }

            if(extra = findItemsNotInStorageConfig(newState.settlements[id].storage.resources, ResourceConfig)) {
                newState.settlements[id].storage.resources = removeStorageItemsFromList(extra, newState.settlements[id].storage.resources);
            }

            if(extra = findItemsNotInStorageConfig(newState.settlements[id].storage.gear, GearConfig)) {
                newState.settlements[id].storage.gear = removeStorageItemsFromList(extra, newState.settlements[id].storage.gear);
            }

            for(let loadoutId in newState.settlements[id].loadouts) {
                if(
                    newState.settlements[id].loadouts[loadoutId] &&
                    newState.settlements[id].loadouts[loadoutId].gear &&
                    newState.settlements[id].loadouts[loadoutId].gear.length > 0
                ) {
                    for(let i = 0; i < newState.settlements[id].loadouts[loadoutId].gear.length; i++) {
                        for(let j = 0; j < newState.settlements[id].loadouts[loadoutId].gear[i].length; j++) {
                            const gear = newState.settlements[id].loadouts[loadoutId].gear[i][j];
                            if(gear) {
                                if(!GearConfig[gear.location] || !GearConfig[gear.location][gear.name]) {
                                   newState.settlements[id].loadouts[loadoutId].gear[i][j] = null;
                                }
                            }
                        }
                    }
                }
            }

            for(let milestoneId in newState.settlements[id].milestones) {
                if(!MilestoneConfig[milestoneId]) {
                    delete newState.settlements[id].milestones[milestoneId];
                }
            }
        }
        return newState;
    } else if(action.type == SCHEMA_VALIDATE) {
        let newState = {...state};

        // overhauled storage in 0.10.6
        if(CompareVersions(action.currentVersion, "0.10.6") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].storage.gear = {...newState.settlements[settlementId].storage.gear};
                for(let location in newState.settlements[settlementId].storage.gear) {
                    let newStorage = {};
                    for(var i = 0; i < newState.settlements[settlementId].storage.gear[location].length; i++) {
                        newStorage[toCamelCase(newState.settlements[settlementId].storage.gear[location][i].title)] = newState.settlements[settlementId].storage.gear[location][i].quantity;
                    }
                    newState.settlements[settlementId].storage.gear[location] = newStorage;
                }

                newState.settlements[settlementId].storage.resources = {...newState.settlements[settlementId].storage.resources};
                for(let location in newState.settlements[settlementId].storage.resources) {
                    let newStorage = {};
                    for(var i = 0; i < newState.settlements[settlementId].storage.resources[location].length; i++) {
                        if(location === "Strange" && newState.settlements[settlementId].storage.resources[location][i].title === "Legendary Lungs") {
                            newState.settlements[settlementId].storage.resources[location][i].title = "Legendary Horns";
                        }

                        newStorage[toCamelCase(newState.settlements[settlementId].storage.resources[location][i].title)] = newState.settlements[settlementId].storage.resources[location][i].quantity;
                    }
                    newState.settlements[settlementId].storage.resources[location] = newStorage;
                }
            }
        }

        // phoenix related gear and resource typos fixed in 0.10.9
        if(CompareVersions(action.currentVersion, "0.10.9") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].storage.gear = {...newState.settlements[settlementId].storage.gear};
                if(newState.settlements[settlementId].storage.gear.plumery.phoenixfaulds !== undefined) {
                    let temp = newState.settlements[settlementId].storage.gear.plumery.phoenixfaulds;
                    delete newState.settlements[settlementId].storage.gear.plumery.phoenixfaulds;
                    newState.settlements[settlementId].storage.gear.plumery.phoenixFaulds = temp;

                    let gearSorted = Object.keys(newState.settlements[settlementId].storage.gear.plumery).sort();
                    let gear = {};
                    for(let i = 0; i < gearSorted.length; i++) {
                        gear[gearSorted[i]] = newState.settlements[settlementId].storage.gear.plumery[gearSorted[i]];
                    }

                    newState.settlements[settlementId].storage.gear.plumery = gear;
                }

                if(newState.settlements[settlementId].storage.resources.Phoenix.smallFrathers !== undefined) {
                    let temp = newState.settlements[settlementId].storage.resources.Phoenix.smallFrathers;
                    delete newState.settlements[settlementId].storage.resources.Phoenix.smallFrathers;
                    newState.settlements[settlementId].storage.resources.Phoenix.smallFeathers = temp;
                }

                if(newState.settlements[settlementId].storage.resources.Phoenix.wishBone !== undefined) {
                    let temp = newState.settlements[settlementId].storage.resources.Phoenix.wishBone;
                    delete newState.settlements[settlementId].storage.resources.Phoenix.wishBone;
                    newState.settlements[settlementId].storage.resources.Phoenix.wishbone = temp;
                }

                let resourcesSorted = Object.keys(newState.settlements[settlementId].storage.resources.Phoenix).sort();
                let resources = {};
                for(let i = 0; i < resourcesSorted.length; i++) {
                    resources[resourcesSorted[i]] = newState.settlements[settlementId].storage.resources.Phoenix[resourcesSorted[i]];
                }

                newState.settlements[settlementId].storage.resources.Phoenix = resources;
            }
        }

        // added global endeavors in 1.0.0
        // changed expansions and campaign format as well
        if(CompareVersions(action.currentVersion, "1.0.0") === -1) {
            for(let settlementId in newState.settlements) {
                if(!newState.settlements[settlementId].globalEndeavors) {
                    newState.settlements[settlementId].globalEndeavors = [];
                    newState.settlements[settlementId].settlementEffects = {};
                }

                if(newState.settlements[settlementId].campaign === "PotL") {
                    newState.settlements[settlementId].campaign = "PotLantern";
                }

                if(Array.isArray(newState.settlements[settlementId].expansions) && newState.settlements[settlementId].expansions.length === 1) {
                    newState.settlements[settlementId].expansions = {"core": "all"};
                }
            }
        }

        // accidentally had sunEater as an ability, removed in 1.0.1
        if(CompareVersions(action.currentVersion, "1.0.1") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].allAbilities.indexOf("sunEater") !== -1) {
                    newState.settlements[settlementId].allAbilities.splice(newState.settlements[settlementId].allAbilities.indexOf("sunEater"), 1);
                }
            }
        }

        if(CompareVersions(action.currentVersion, "1.1.0") === -1) {
            for(let settlementId in newState.settlements) {
                // add missing story events to PotBloom
                if(newState.settlements[settlementId].campaign === "PotBloom") {
                    newState.settlements[settlementId].allStoryEvents.push("Necrotoxic Mistletoe");
                    newState.settlements[settlementId].allStoryEvents.push("Sense Memory");
                }

                // initialize custom story events array
                newState.settlements[settlementId].customStoryEvents = [];
            }
        }

        // made innovation deck its own property in 1.2.0
        if(CompareVersions(action.currentVersion, "1.2.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].innovationDeck = getInnovationDeck(newState.settlements[settlementId].innovations, newState.settlements[settlementId].allInnovations);
            }
        }

        // added cursed gear and survivor sorting in 1.3.0
        if(CompareVersions(action.currentVersion, "1.3.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].allCursedItems = [];
                newState.settlements[settlementId] = addCursedGearToSettlement(newState.settlements[settlementId], newState.settlements[settlementId].expansions);

                newState.settlements[settlementId].survivorSortAttr = "name";
                newState.settlements[settlementId].survivorSortOrder = "asc";
            }
        }
        // added frenzy in 1.3.1
        if(CompareVersions(action.currentVersion, "1.3.1") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].allAbilities.indexOf("frenzy") === -1) {
                    newState.settlements[settlementId].allAbilities.push("frenzy");
                }
            }
        }

        // added eye patch to potsun in 1.3.8
        if(CompareVersions(action.currentVersion, "1.3.8") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].campaign === "PotSun") {
                    newState.settlements[settlementId].storage.gear["Rare Gear"].eyePatch = 0;

                    newState.settlements[settlementId].storage.gear["Rare Gear"] = sortStorage(newState.settlements[settlementId].storage, "gear", "Rare Gear");
                }

                if(newState.settlements[settlementId].campaign === "PotStars") {
                    newState.settlements[settlementId].allNemesisEncounters = newState.settlements[settlementId].allNemesisEncounters.concat(["The Tyrant Level 1", "The Tyrant Level 2", "The Tyrant Level 3"]);
                    newState.settlements[settlementId].allNemesisEncounters.sort();

                    newState.settlements[settlementId].allAbilities = newState.settlements[settlementId].allAbilities.concat(["constellationWitch", "constellationStorm", "constellationSculptor", "constellationRust", "constellationReaper", "constellationGoblin", "constellationAbsolute", "constellationGambler"]);
                }
            }
        }

        // fixed dragon king quarry typo in 1.3.9
        if(CompareVersions(action.currentVersion, "1.3.9") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].campaign === "PotStars") {
                    newState.settlements[settlementId].expansions["dragon-king"] = "cards";
                }

                if(newState.settlements[settlementId].campaign === "PotSun") {
                    newState.settlements[settlementId].expansions["sunstalker"] = "cards";
                }

                for(let i = 0; i < newState.settlements[settlementId].allShowdowns.length; i++) {
                    if(newState.settlements[settlementId].allShowdowns[i] === "Dragon Kings Level 1") {
                        newState.settlements[settlementId].allShowdowns[i] = "Dragon King Level 1";
                    } else if(newState.settlements[settlementId].allShowdowns[i] === "Dragon Kings Level 2") {
                        newState.settlements[settlementId].allShowdowns[i] = "Dragon King Level 2";
                    } else if(newState.settlements[settlementId].allShowdowns[i] === "Dragon Kings Level 3") {
                        newState.settlements[settlementId].allShowdowns[i] = "Dragon King Level 3";
                    }
                }
            }
        }

        // added custom expansions in 1.4.0
        if(CompareVersions(action.currentVersion, "1.4.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].customExpansions = {};
            }
        }

        // added some missing rare gear to potsun in 1.4.1
        if(CompareVersions(action.currentVersion, "1.4.1") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].campaign === "PotSun") {
                    if(!newState.settlements[settlementId].storage.gear['Rare Gear'].apostleCrown) {
                        newState.settlements[settlementId].storage.gear['Rare Gear'].apostleCrown = 0;
                        newState.settlements[settlementId].storage.gear['Rare Gear'].prismMace = 0;
                        newState.settlements[settlementId].storage.gear['Rare Gear'].sunringBow = 0;
                        newState.settlements[settlementId].storage.gear['Rare Gear'].sunVestments = 0;

                        newState.settlements[settlementId].storage.gear["Rare Gear"] = sortStorage(newState.settlements[settlementId].storage, "gear", "Rare Gear");
                    }
                }
            }
        }

        // innovations and locations might be slightly f'd in 1.5.2
        if(CompareVersions(action.currentVersion, "1.5.2") === -1) {
            for(let settlementId in newState.settlements) {
                let settlement = newState.settlements[settlementId];
                if(Object.keys(settlement.customExpansions).length > 0) {
                    settlement.allInnovations = settlement.allInnovations.filter(removeItemsNotInConfig(getInnovationConfig()));
                    settlement.innovations = settlement.innovations.filter(removeItemsNotInConfig(getInnovationConfig()));
                    settlement.innovationDeck = settlement.innovationDeck.filter(removeItemsNotInConfig(getInnovationConfig()));
                    settlement.allLocations = settlement.allLocations.filter(removeItemsNotInConfig(getLocationConfig()));
                    settlement.locations = settlement.locations.filter(removeItemsNotInConfig(getLocationConfig()));
                }

                if(settlement.expansions['dung-beetle-knight'] === "all") {
                    settlement = addStorageToSettlement(settlement, "gear", {
                        "wetResinCrafter": {
                            "calcifiedCenturyGreaves": 0,
                            "calcifiedDiggingClaw": 0,
                            "calcifiedShoulderPads": 0,
                        },
                        "Rare Gear": {
                            "regeneratingBlade": 0,
                            "calcifiedJuggernautBlade": 0,
                        }
                    });
                }
            }
        }

        // consolidated notes in 1.5.3
        if(CompareVersions(action.currentVersion, "1.5.3") === -1) {
            for(let settlementId in newState.settlements) {
                let settlement = newState.settlements[settlementId];
                let notes = settlement.notes;
                if(Array.isArray(notes) && notes.length > 0) {
                    let newNote = notes.join("\n");
                    settlement.notes = newNote;
                } else {
                    settlement.notes = "";
                }
            }
        }

        // big story event/showdown data storage refactor in 1.7.0
        if(CompareVersions(action.currentVersion, "1.7.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].allStoryEvents = updateEventsTo17(newState.settlements[settlementId].allStoryEvents, getStoryEventConfig());
                newState.settlements[settlementId].allShowdowns = updateEventsTo17(newState.settlements[settlementId].allShowdowns, getShowdownConfig());
                newState.settlements[settlementId].allNemesisEncounters = updateEventsTo17(newState.settlements[settlementId].allNemesisEncounters, getShowdownConfig());
            }
        }

        // added "Dead Inside" impairment in 1.7.1
        if(CompareVersions(action.currentVersion, "1.7.1") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].allAbilities.push("deadInside");
            }
        }

        // restore missing white lions in 1.7.6
        if(CompareVersions(action.currentVersion, "1.7.6") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].allShowdowns.indexOf('whiteLionLevel1') === -1) {
                    newState.settlements[settlementId].allShowdowns.push('whiteLionLevel1');
                }

                if(newState.settlements[settlementId].allShowdowns.indexOf('whiteLionLevel2') === -1) {
                    newState.settlements[settlementId].allShowdowns.push('whiteLionLevel2');
                }

                if(newState.settlements[settlementId].allShowdowns.indexOf('whiteLionLevel3') === -1) {
                    newState.settlements[settlementId].allShowdowns.push('whiteLionLevel3');
                }
            }
        }

        // made Leader addable in PotSun
        if(CompareVersions(action.currentVersion, "1.7.8") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].campaign === "PotSun") {
                    if(newState.settlements[settlementId].allFightingArts.indexOf("leader") === -1) {
                        newState.settlements[settlementId].allFightingArts.push("leader");
                    }
                }
            }
        }


        // added missing Super Hair ability
        if(CompareVersions(action.currentVersion, "2.1.0") === -1) {
            for(let settlementId in newState.settlements) {
                let settlement = newState.settlements[settlementId];
                if(settlement.expansions['lonely-tree'] === "all") {
                    if(newState.settlements[settlementId].allAbilities.indexOf("superHair") === -1) {
                        newState.settlements[settlementId].allAbilities.push("superHair");
                    }
                }
            }
        }

        if(CompareVersions(action.currentVersion, "2.2.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].log = [];
            }
        }

        if(CompareVersions(action.currentVersion, "2.2.2") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].campaign === "PotSun") {
                    newState.settlements[settlementId].storage.resources["Strange"]["lifeString"] = 0;
                    newState.settlements[settlementId].storage.resources["Strange"] = sortStorage(newState.settlements[settlementId].storage, "resources", "Strange", getResourceConfig());
                }
            }
        }

        if(CompareVersions(action.currentVersion, "2.2.3") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].campaign === "PotSun") {
                    if(newState.settlements[settlementId].storage.resources["Strange"]["lifeString"] !== undefined) {
                        if(newState.settlements[settlementId].allInnovations.indexOf("sunLanguage") !== -1) {
                            continue; // really is PotSun, carry on
                        }

                        if(newState.settlements[settlementId].allInnovations.indexOf("dragonSpeech") !== -1) {
                            newState.settlements[settlementId].campaign = "PotStars";
                            delete newState.settlements[settlementId].storage.resources["Strange"]["lifeString"];
                            continue;
                        }

                        if(newState.settlements[settlementId].globalEndeavors.length > 1) {
                            for(let i = 0; i < newState.settlements[settlementId].globalEndeavors.length; i++) {
                                if(newState.settlements[settlementId].globalEndeavors[i].title === "Forest Run") {
                                    newState.settlements[settlementId].campaign = "PotBloom";
                                    delete newState.settlements[settlementId].storage.resources["Strange"]["lifeString"];
                                    continue;
                                }
                            }
                        }

                        newState.settlements[settlementId].campaign = "PotLantern";
                        delete newState.settlements[settlementId].storage.resources["Strange"]["lifeString"];
                    }
                }
            }
        }

        // Added milestone tracking in 2.4.0
        if(CompareVersions(action.currentVersion, "2.4.0") === -1) {
            for(let settlementId in newState.settlements) {
                if(newState.settlements[settlementId].campaign === "PotSun") {
                    newState.settlements[settlementId].milestones = {
                        "firstDeath": {"complete": false, "message": "principleDeath"},
                        "innovationCount": {"complete": false, "value": 8, "message": "edgedTonometry"},
                        "population": {"complete": false, "value": 15, "message": "principleSociety"}
                    };
                } else if(newState.settlements[settlementId].campaign === "PotStars") {
                    newState.settlements[settlementId].milestones = {
                        "firstBirth": {"complete": false, "message": "principleNewLife"},
                        "firstDeath": {"complete": false, "message": "principleDeath"},
                        "population": {"complete": false, "value": 15, "message": "principleSociety"}
                    };
                } else {
                    newState.settlements[settlementId].milestones = {
                        "firstBirth": {"complete": false, "message": "principleNewLife"},
                        "firstDeath": {"complete": false, "message": "principleDeath"},
                        "innovationCount": {"complete": false, "value": 5, "message": "hoodedKnight"},
                        "population": {"complete": false, "value": 15, "message": "principleSociety"}
                    };
                }
            }
        }

        if(CompareVersions(action.currentVersion, "2.6.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].loadouts = {};
            }
        }

        if(CompareVersions(action.currentVersion, "2.7.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId] = addArmorSetsToSettlement(newState.settlements[settlementId], newState.settlements[settlementId].expansions);
            }
        }

        // milestone update in 2.8
        if(CompareVersions(action.currentVersion, "2.8.0") === -1) {
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].milestones = {...newState.settlements[settlementId].milestones};
                for(let id in newState.settlements[settlementId].milestones) {
                    newState.settlements[settlementId].milestones[id].conditions = {};
                    if(id === "firstBirth" || id === "firstDeath") {
                        newState.settlements[settlementId].milestones[id].conditions[id] = {
                            value: newState.settlements[settlementId].milestones[id].value = newState.settlements[settlementId].milestones[id].complete ? 1 : 0,
                            complete: newState.settlements[settlementId].milestones[id].complete,
                        }
                    } else if(id === "innovationCount") {
                        let max = newState.settlements[settlementId].milestones[id].message === "hoodedKnight" ? 5 : 8;
                        let newId = newState.settlements[settlementId].milestones[id].message;
                        newState.settlements[settlementId].milestones[newId] = {...newState.settlements[settlementId].milestones[id]};
                        newState.settlements[settlementId].milestones[newId].conditions = {};
                        delete newState.settlements[settlementId].milestones[newId].message;
                        delete newState.settlements[settlementId].milestones[id];
                        newState.settlements[settlementId].milestones[newId].conditions[newId] = {
                            value: newState.settlements[settlementId].milestones[newId].value = Math.min(max, newState.settlements[settlementId].innovations.length),
                            complete: newState.settlements[settlementId].milestones[newId].complete,
                        }
                    } else if(id === "population") {
                        newState.settlements[settlementId].milestones[id].conditions[id] = {
                            value: newState.settlements[settlementId].milestones[id].value = Math.min(15, newState.settlements[settlementId].population),
                            complete: newState.settlements[settlementId].milestones[id].complete,
                        }
                    }

                    if(newState.settlements[settlementId].milestones[id]) {
                        delete newState.settlements[settlementId].milestones[id].message;
                    }
                }
            }
        }

        // added quarry list in 2.11.0
        if(CompareVersions(action.currentVersion, "2.11.0") === -1) {
            const ShowdownConfig = getShowdownConfig();
            for(let settlementId in newState.settlements) {
                newState.settlements[settlementId].quarryShowdowns = [];
                for(let i = 0; i < newState.settlements[settlementId].allShowdowns.length; i++) {
                    if(
                        ShowdownConfig[newState.settlements[settlementId].allShowdowns[i]] &&
                        ShowdownConfig[newState.settlements[settlementId].allShowdowns[i]].quarry
                    ) {
                        newState.settlements[settlementId].quarryShowdowns.push(newState.settlements[settlementId].allShowdowns[i]);
                    }
                }
            }
        }

        return newState;
    } else {
        return state;
    }
}

function removeItemsNotInConfig(config) {
    return (item) => {
        if(!config[item]) {
            return false;
        }

        return true;
    }
}

function addItemToAllStorage(storage, location, id, config) {
    storage = {...storage};

    if(!storage[location]) {
        storage[location] = {};
    } else {
        storage[location] = {...storage[location]};
    }

    storage[location][id] = 0;

    storage[location] = sortStorage(storage, location, config);

    return storage;
}

function removeItemFromAllStorage(storage, location, id) {
    if(!storage[location]) {
        return storage;
    }

    storage = {...storage};
    storage[location] = {...storage[location]};

    delete storage[location][id];

    if(Object.keys(storage[location]).length === 0) {
        delete storage[location];
    }

    return storage;
}
