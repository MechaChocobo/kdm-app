import { SETTLEMENT_CREATE_SET_NAME,
    SETTLEMENT_CREATE_SET_EXPANSIONS,
    SETTLEMENT_CREATE_SET_CUSTOM_EXPANSIONS,
    SETTLEMENT_CREATE_SET_CAMPAIGN,
    SETTLEMENT_CREATE_SET_CREATE_SURVIVORS,
    SETTLEMENT_CREATE_RESET
} from "../actions/settlementCreate";

let initialState = {
    name: "",
    createSurvivors: true,
    expansions: {},
    campaign: "PotLantern",
    selectedCustomExpansions: {},
};

export const settlementCreate = (state = initialState, action) => {
    if(action.type === SETTLEMENT_CREATE_SET_NAME) {
        let newState = {...state};
        newState.name = action.name;
        return newState;
    } else if(action.type === SETTLEMENT_CREATE_SET_EXPANSIONS) {
        let newState = {...state};
        newState.expansions = action.expansions;
        return newState;
    } else if(action.type === SETTLEMENT_CREATE_SET_CUSTOM_EXPANSIONS) {
        let newState = {...state};
        newState.selectedCustomExpansions = action.expansions;
        return newState;
    } else if(action.type === SETTLEMENT_CREATE_SET_CAMPAIGN) {
        let newState = {...state};
        newState.campaign = action.campaign;
        return newState;
    } else if(action.type === SETTLEMENT_CREATE_SET_CREATE_SURVIVORS) {
        let newState = {...state};
        newState.createSurvivors = action.value;
        return newState;
    } else if(action.type === SETTLEMENT_CREATE_RESET) {
        return initialState;
    } else {
        return state;
    }
}
