import CompareVersions from 'compare-versions';

import {
    CREATE_SURVIVOR,
    CHANGE_GENDER,
    SURVIVOR_NAME_CHANGE,
    FATHER_SET, MOTHER_SET,
    SKIP_NEXT_HUNT_TOGGLE,
    RETIRED_TOGGLE,
    SET_HAS_REROLL, REROLL_TOGGLE,
    DEAD_SET,
    SET_UNAVAILABLE,
    SURVIVOR_SURVIVAL_SET, SURVIVOR_SURVIVAL_INCREASE, SURVIVOR_SURVIVAL_DECREASE,
    CAN_SPEND_SURVIVAL_TOGGLE,
    SET_CAN_GAIN_SURVIVAL,
    SURVIVAL_ACTION_SET,
    INSANITY_SET, SURVIVOR_INSANITY_INCREASE, SURVIVOR_INSANITY_DECREASE, BRAIN_INJURY_TOGGLE,
    XP_SET, XP_INCREASE, XP_DECREASE,
    COURAGE_SET, COURAGE_INCREASE, COURAGE_DECREASE,
    UNDERSTANDING_SET, UNDERSTANDING_INCREASE, UNDERSTANDING_DECREASE,
    ARMOR_SET, ARMOR_ALL_INCREASE, ARMOR_ALL_DECREASE, ARMOR_RESET, WOUND_TOGGLE,
    SURVIVOR_STAT_SET, SURVIVOR_TEMP_STAT_SET,
    ADD_STATS_TO_ALL_SURVIVORS, REMOVE_STATS_FROM_ALL_SURVIVORS,
    WEAPON_PROF_INCREASE, WEAPON_PROF_DECREASE, WEAPON_PROF_CHANGE,
    CONSTELLATION_MAP_TOGGLE, CONSTELLATION_SET,
    FIGHTING_ART_ADD, FIGHTING_ART_REMOVE,
    DISORDER_ADD, DISORDER_REMOVE,
    ABILITY_ADD, ABILITY_REMOVE,
    SEVERE_INJURY_ADD, SEVERE_INJURY_REMOVE,
    NOTE_EDIT_SURVIVOR,
    AFFINITY_SET,
    CURSED_GEAR_ADD, CURSED_GEAR_REMOVE,
    DEPARTING_COLOR_SET,
    DEPART_TOGGLE,
    RETURN_SURVIVORS,
    INIT_SURVIVORS,
    IMPORT_SURVIVORS,
    COPY_SURVIVOR,
    DELETE_SURVIVOR,
    SET_EPITHET,
    SET_PINNED,
    SURVIVOR_ADD_ACTION_LOG,
    LOADOUT_CHANGE_GEAR,
    LOADOUT_CHANGE_NAME,
    LOADOUT_SAVE,
    LOADOUT_SET,
    SET_ARMOR_SET,
    SWAP_LOADOUT_SPACE,
    SET_WEAPON_PROFICIENCY_EARNED,
    SET_LOADOUT_ATTRIBUTES,
    APPLY_LOADOUT_STATS,
    SET_CAUSE_OF_DEATH,
} from "../actions/survivors";

import {PRINCIPLE_SET, PRINCIPLE_UNSET, INNOVATION_ADD, INNOVATION_REMOVE, COPY_SETTLEMENT, DELETE_ALL_FIGHTING_ART, DELETE_ALL_DISORDER, DELETE_ALL_ABILITY, DELETE_ALL_SEVERE_INJURY, DELETE_ALL_WEAPON_PROFICIENCY, GEAR_CURSED_DELETE, LOADOUT_DELETE, GEAR_DELETE, DELETE_ALL_ARMOR_SET} from '../actions/settlement';

import {
    initSurvivors, createSurvivor,
    addAbility, removeAbility,
    addFightingArt, removeFightingArt,
    addDisorder, removeDisorder,
    addSevereInjury, removeSevereInjury,
    applyStatChanges, removeStatChanges, setConstellationMap, getInnovationConfig, getSevereInjuryConfig, setSurvivorConstellation, getWeaponProfConfig, addActionLog, getGearConfig, getArmorConfig, getCompletedLoadoutAffinities, removeItemsFromList, findItemsNotInConfig, getFightingArtConfig, getDisorderConfig, getAbilityConfig
} from '../helpers/helpers';
import { DELETE_SETTLEMENT } from "../actions/settlement";
import { SCHEMA_VALIDATE } from "../actions/settings";
import { DO_INTEGRITY_CHECK } from '../actions/global';

let SurvivorState = {survivors: {}, sort: {}, sortOrder: {}, pinnedSurvivors: {}};
export const survivorReducer = (state = SurvivorState, action) => {
    if(action.type == SURVIVOR_NAME_CHANGE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].name = action.name;

        let names = action.name.split(' ');
        if(names[names.length -1] === 'Noble') {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 2, 1);
        } else if(names[names.length -1] === 'Reincarnated') {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 1, 0);
        }

        return newState;
    } else if(action.type === SURVIVOR_STAT_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].attributes[action.stat] = action.val;        // passed updated stat value as argument

        // check constellations
        if(action.stat === "STR" && action.val >= 3) {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 3, 2);
        }
        if(action.stat === "ACC" && action.val >= 1) {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 2, 3);
        }

        return newState;
    } else if(action.type == SURVIVOR_TEMP_STAT_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].tempAttributes[action.stat] = action.val;    // passed updated stat value as argument
        return newState;
    } else if(action.type == XP_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].xp = action.val;
        return newState;
    } else if(action.type == XP_INCREASE) {
        let newState = getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].xp < 16) {
            newState.survivors[action.settlementId][action.id].xp = newState.survivors[action.settlementId][action.id].xp + 1;
        }
        return newState;
    } else if(action.type == XP_DECREASE) {
        let newState = getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].xp > 0) {
            newState.survivors[action.settlementId][action.id].xp = newState.survivors[action.settlementId][action.id].xp - 1;
        }
        return newState;
    } else if(action.type == COURAGE_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].courage = action.val;

        if(action.val === 9) {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 3, 3);
        }

        return newState;
    } else if(action.type == COURAGE_INCREASE) {
        let newState = getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].courage < 9) {
            newState.survivors[action.settlementId][action.id].courage = newState.survivors[action.settlementId][action.id].courage + 1;
        }

        if(newState.survivors[action.settlementId][action.id].courage === 9) {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 3, 3);
        }

        return newState;
    } else if(action.type == COURAGE_DECREASE) {
        let newState = getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].courage > 0) {
            newState.survivors[action.settlementId][action.id].courage = newState.survivors[action.settlementId][action.id].courage - 1;
        }
        return newState;
    } else if(action.type == UNDERSTANDING_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].understanding = action.val;

        if(action.val === 9) {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 0, 0);
        }

        return newState;
    } else if(action.type == UNDERSTANDING_INCREASE) {
        let newState = getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].understanding < 9) {
            newState.survivors[action.settlementId][action.id].understanding = newState.survivors[action.settlementId][action.id].understanding + 1;
        }

        if(newState.survivors[action.settlementId][action.id].understanding === 9) {
            newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 0, 0);
        }

        return newState;
    } else if(action.type == UNDERSTANDING_DECREASE) {
        let newState = getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].understanding > 0) {
            newState.survivors[action.settlementId][action.id].understanding = newState.survivors[action.settlementId][action.id].understanding - 1;
        }
        return newState;
    } else if(action.type == WEAPON_PROF_INCREASE) {
        let ProficiencyConfig = getWeaponProfConfig();
        let newState = getNewState(state, action);
        let prof = newState.survivors[action.settlementId][action.id].weaponProficiency;

        // prevent user from incrementing past 8
        if(prof.rank < 8) {
            prof.rank = prof.rank + 1;

            // check if current rank is a weapon specialization rank (if prof.rank is a Key)
            if( ProficiencyConfig[prof.weapon] &&
                ProficiencyConfig[prof.weapon].ranks &&
                prof.rank in ProficiencyConfig[prof.weapon].ranks
            ) {
                // if rank grants abilities, add each
                let abilities = ProficiencyConfig[prof.weapon].ranks[prof.rank]["abilities"];
                if(abilities) {
                    for(let i = 0; i < abilities.length; i++) {
                        // add weapon ability (if survivor doesn't have it already)
                        let wepAbility = abilities[i];
                        if(newState.survivors[action.settlementId][action.id].abilities.indexOf(wepAbility) === -1) {
                            newState.survivors[action.settlementId][action.id] = addAbility(newState.survivors[action.settlementId][action.id], wepAbility);
                        }
                    }

                    // set Constellation flag at rank 8 if rank 8 is a weapon mastery
                    if(ProficiencyConfig[prof.weapon].ranks[prof.rank]["title"] === "Weapon Mastery") {
                        newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], true, 2, 2);
                    }
                }
            }
        }
        return newState;
    } else if(action.type == WEAPON_PROF_DECREASE) {
        let ProficiencyConfig = getWeaponProfConfig();
        let newState = getNewState(state, action);
        let prof = newState.survivors[action.settlementId][action.id].weaponProficiency;

        // prevent user from decrementing past rank 0, but allows decrementing weapons that don't appear in ProficiencyConfig (e.g., "- None -")
        if(prof.rank > 0) {
            prof.rank = prof.rank - 1;
        }

        // if rank falls below specialization threshold of prof.weapon, remove specialization ability (prof.rank is one less than actual rank)
        // make sure weapon appears in ProficiencyConfig, has ranks, and has a specialization rank
        if(
            ProficiencyConfig[prof.weapon] && "ranks" in ProficiencyConfig[prof.weapon] &&
            ProficiencyConfig[prof.weapon].ranks[prof.rank + 1]
        ) {

            // set ability name, then remove ability from survivor
            let abilities = ProficiencyConfig[prof.weapon].ranks[prof.rank + 1].abilities;
            if(abilities) {
                for(let i = 0; i < abilities.length; i++) {
                    // add weapon ability (always weapon specializaiton?)
                    let wepSpec = abilities[i];
                    if(newState.survivors[action.settlementId][action.id].abilities.indexOf(wepSpec) !== -1) {
                        newState.survivors[action.settlementId][action.id] = removeAbility(newState.survivors[action.settlementId][action.id], wepSpec);
                    }
                }
            }
        }

        return newState;
    } else if(action.type == WEAPON_PROF_CHANGE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].weaponProficiency = {...newState.survivors[action.settlementId][action.id].weaponProficiency};
        newState.survivors[action.settlementId][action.id].weaponProficiency.weapon = action.name;
        return newState;
    } else if(action.type == FIGHTING_ART_ADD) {
        let newState = getNewState(state, action);
        let survivor = addFightingArt(newState.survivors[action.settlementId][action.id], action.name);

        if(action.name === "championsRite") {
            survivor = setConstellationMap(survivor, true, 1, 3);
        } else if(action.name === "fatedBlow") {
            survivor = setConstellationMap(survivor, true, 0, 2);
        } else if(action.name === "frozenStar") {
            survivor = setConstellationMap(survivor, true, 1, 1);
        } else if(action.name === "unbreakable") {
            survivor = setConstellationMap(survivor, true, 3, 1);
        }

        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == FIGHTING_ART_REMOVE) {
        let newState = getNewState(state, action);
        let survivor = removeFightingArt(newState.survivors[action.settlementId][action.id], action.name);
        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == DISORDER_ADD) {
        let newState = getNewState(state, action);
        let survivor = addDisorder(newState.survivors[action.settlementId][action.id], action.name);

        if(action.name === "destined") {
            survivor = setConstellationMap(survivor, true, 0, 1);
        }

        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == DISORDER_REMOVE) {
        let newState = getNewState(state, action);
        let survivor = removeDisorder(newState.survivors[action.settlementId][action.id], action.name);
        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == ABILITY_ADD) {
        let newState = getNewState(state, action);
        let survivor = addAbility(newState.survivors[action.settlementId][action.id], action.name);

        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == ABILITY_REMOVE) {
        let newState = getNewState(state, action);
        let survivor = removeAbility(newState.survivors[action.settlementId][action.id], action.name);
        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == SEVERE_INJURY_ADD) {
        let InjuryConfig = getSevereInjuryConfig();
        if(InjuryConfig[action.name].numPossible) {
            let count = 0;
            for(var i = 0; i < state.survivors[action.settlementId][action.id].severeInjuries.length; i++) {
                if(state.survivors[action.settlementId][action.id].severeInjuries[i] === action.name) {
                    count++;
                }
            }

            if(count >= InjuryConfig[action.name].numPossible) {
                return state;
            }
        }
        let newState = getNewState(state, action);
        let survivor = addSevereInjury(newState.survivors[action.settlementId][action.id], action.name);
        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == SEVERE_INJURY_REMOVE) {
        let newState = getNewState(state, action);
        let survivor = removeSevereInjury(newState.survivors[action.settlementId][action.id], action.name);
        newState.survivors[action.settlementId][action.id] = survivor;
        return newState;
    } else if(action.type == REROLL_TOGGLE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].reroll = !newState.survivors[action.settlementId][action.id].reroll;
        return newState;
    } else if(action.type == DEAD_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].dead = action.dead;
        if(action.dead === true) {
            newState.survivors[action.settlementId][action.id].departing = false;
            newState.survivors[action.settlementId][action.id].returning = false;
            newState.survivors[action.settlementId][action.id].timeOfDeath = Date.now();
            if(newState.pinnedSurvivors && newState.pinnedSurvivors[action.settlementId]) {
                if(newState.pinnedSurvivors[action.settlementId][action.id]) {
                    delete newState.pinnedSurvivors[action.settlementId][action.id];
                    if(Object.keys(newState.pinnedSurvivors[action.settlementId]).length === 0) {
                        delete newState.pinnedSurvivors[action.settlementId];
                    }
                }
            }
        }
        return newState;
    } else if(action.type == CREATE_SURVIVOR) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};
        let survivor = createSurvivor(action.settlementId, action.id, action.gender, action.name, action.parents, action.bonuses, action.campaign);
        newState.survivors[action.settlementId][survivor.id] = survivor;
        return newState;
    } else if(action.type == INIT_SURVIVORS) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};
        let survivors = initSurvivors(action.settlementId, action.createSurvivors, action.campaign);
        newState.survivors[action.settlementId] = survivors;
        return newState;
    } else if(action.type == DEPART_TOGGLE) {
        let newState = getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].departing !== false) {
            newState.survivors[action.settlementId][action.id].departing = false;
        } else {
            newState.survivors[action.settlementId][action.id].departing = true;
        }
        return newState;
    } else if(action.type == CAN_SPEND_SURVIVAL_TOGGLE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].canSpendSurvival = !newState.survivors[action.settlementId][action.id].canSpendSurvival;
        return newState;
    } else if(action.type == IMPORT_SURVIVORS) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};
        newState.survivors[action.settlementId] = action.survivors;
        return newState;
    } else if(action.type == ARMOR_ALL_INCREASE) {
        let newState = getNewState(state, action);

        for(let location in newState.survivors[action.settlementId][action.id].armor) {
            newState.survivors[action.settlementId][action.id].armor[location] = {...newState.survivors[action.settlementId][action.id].armor[location]};
            newState.survivors[action.settlementId][action.id].armor[location].value = newState.survivors[action.settlementId][action.id].armor[location].value + 1;
        }

        return newState;
    } else if(action.type == ARMOR_ALL_DECREASE) {
        let newState = getNewState(state, action);

        for(let location in newState.survivors[action.settlementId][action.id].armor) {
            if(newState.survivors[action.settlementId][action.id].armor[location].value > 0) {
                newState.survivors[action.settlementId][action.id].armor[location] = {...newState.survivors[action.settlementId][action.id].armor[location]};
                newState.survivors[action.settlementId][action.id].armor[location].value = newState.survivors[action.settlementId][action.id].armor[location].value - 1;
            }
        }

        return newState;
    } else if(action.type == ARMOR_RESET) {
        let newState = getNewState(state, action);

        let gearSkip = [];
        let baseArmor = 0;

        if(newState.survivors[action.settlementId][action.id].loadout && newState.survivors[action.settlementId][action.id].loadout.armorSet) {
            const set = newState.survivors[action.settlementId][action.id].loadout.armorSet;
            const ArmorConfig = getArmorConfig();

            if(ArmorConfig[set].armor) {
                baseArmor = ArmorConfig[set].armor;
            }

            if(ArmorConfig[set].flat) {
                gearSkip = ArmorConfig[set].pieces;
            }
        }

        // crystal skin gives 3
        if(newState.survivors[action.settlementId][action.id].abilities.indexOf('crystalSkin') !== -1) {
            baseArmor += 3;
        }

        // iridescent hide gives up to 3 depending on completed affinities
        if(newState.survivors[action.settlementId][action.id].abilities.indexOf('iridescentHide') !== -1) {
            const armorAffinities = getCompletedLoadoutAffinities(newState.survivors[action.settlementId][action.id]);
            const affinities = newState.survivors[action.settlementId][action.id].affinities;

            let armorCount = 0;

            if(armorAffinities.red + affinities.red > 0) {
                armorCount += 1;
            }

            if(armorAffinities.green + affinities.green > 0) {
                armorCount += 1;
            }

            if(armorAffinities.blue + affinities.blue > 0) {
                armorCount += 1;
            }

            baseArmor += armorCount;
        }

        for(let location in newState.survivors[action.settlementId][action.id].armor) {
            newState.survivors[action.settlementId][action.id].armor[location] = {...newState.survivors[action.settlementId][action.id].armor[location]};
            newState.survivors[action.settlementId][action.id].armor[location].value = baseArmor;

            newState.survivors[action.settlementId][action.id].armor[location].heavy = false;
            if(newState.survivors[action.settlementId][action.id].armor[location].light !== undefined) {
                newState.survivors[action.settlementId][action.id].armor[location].light = false;
            }
        }

        let hasShieldSpec = action.hasShieldMastery || newState.survivors[action.settlementId][action.id].abilities.indexOf("spec-shield") !== -1;
        let hasShield = false;

        // if they have a loadout and are wearing armor then reset to their armor values
        if(newState.survivors[action.settlementId][action.id].loadout && newState.survivors[action.settlementId][action.id].loadout.gear) {
            const gear = newState.survivors[action.settlementId][action.id].loadout.gear;
            const GearConfig = getGearConfig();
            for(let x = 0; x < gear.length; x++) {
                for(let y = 0; y < gear[x].length; y++) {
                    if(
                        gear[x][y] &&
                        GearConfig[gear[x][y].location] &&
                        GearConfig[gear[x][y].location][gear[x][y].name] &&
                        GearConfig[gear[x][y].location][gear[x][y].name].armor
                    ) {

                        if(
                            !hasShield &&
                            GearConfig[gear[x][y].location][gear[x][y].name].keywords &&
                            GearConfig[gear[x][y].location][gear[x][y].name].keywords.indexOf("shield") !== -1
                        ) {
                            hasShield = true;
                        }

                        let skip = false;
                        if(gearSkip.length > 0) {
                            for(let i = 0; i < gearSkip.length; i++) {
                                if(gearSkip[i].location === gear[x][y].location && gearSkip[i].name === gear[x][y].name) {
                                    skip = true;
                                    break;
                                }
                            }
                        }

                        if(skip) {
                            continue;
                        }

                        const armor = GearConfig[gear[x][y].location][gear[x][y].name].armor;
                        for(let i = 0; i < armor.locations.length; i++) {
                            if(armor.locations[i] === "all") {
                                for(let location in newState.survivors[action.settlementId][action.id].armor) {
                                    newState.survivors[action.settlementId][action.id].armor[location].value += armor.value;
                                }
                            } else {
                                newState.survivors[action.settlementId][action.id].armor[armor.locations[i]].value += armor.value;
                            }
                        }
                    }
                }
            }
        }

        if(hasShieldSpec && hasShield) {
            for(let location in newState.survivors[action.settlementId][action.id].armor) {
                newState.survivors[action.settlementId][action.id].armor[location] = {...newState.survivors[action.settlementId][action.id].armor[location]};
                newState.survivors[action.settlementId][action.id].armor[location].value += 1;
            }
        }

        newState.survivors[action.settlementId][action.id].insanity = {...newState.survivors[action.settlementId][action.id].insanity};
        newState.survivors[action.settlementId][action.id].insanity.light = false;

        return newState;
    } else if(action.type == ARMOR_SET) {
        let newState = getNewState(state, action);

        newState.survivors[action.settlementId][action.id].armor[action.location] = {...newState.survivors[action.settlementId][action.id].armor[action.location]};
        newState.survivors[action.settlementId][action.id].armor[action.location].value = action.val;

        return newState;
    } else if(action.type == WOUND_TOGGLE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].armor[action.location] = {...newState.survivors[action.settlementId][action.id].armor[action.location]};
        newState.survivors[action.settlementId][action.id].armor[action.location][action.severity] = !newState.survivors[action.settlementId][action.id].armor[action.location][action.severity];
        return newState;
    } else if(action.type == BRAIN_INJURY_TOGGLE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].insanity = {...newState.survivors[action.settlementId][action.id].insanity};
        newState.survivors[action.settlementId][action.id].insanity.light = !newState.survivors[action.settlementId][action.id].insanity.light;
        return newState;
    } else if(action.type == ADD_STATS_TO_ALL_SURVIVORS) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};
        let survivors = newState.survivors[action.settlementId];
        for(var id in survivors) {
            let survivor = applyStatChanges(survivors[id], action.stats);
            survivors[id] = {...survivor};
        }
        newState.survivors[action.settlementId] = {...survivors};
        return newState;
    } else if(action.type == PRINCIPLE_SET) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.id] = {...newState.survivors[action.id]};
        if(action.stats && action.statType === "increase") {
            let survivors = newState.survivors[action.id];
            for(var id in survivors) {
                let survivor = applyStatChanges(survivors[id], action.stats);
                survivors[id] = {...survivor};
            }
            newState.survivors[action.id] = {...survivors};
        } else if(action.stats && action.statType === "decrease") {
            let survivors = newState.survivors[action.id];
            for(var id in survivors) {
                let survivor = removeStatChanges(survivors[id], action.stats);
                survivors[id] = {...survivor};
            }
            newState.survivors[action.id] = {...survivors};
        }
        return newState;
    } else if(action.type == PRINCIPLE_UNSET) {
        let newState = {...state};
        if(action.stats && action.statType === "decrease") {
            newState.survivors = {...newState.survivors};
            newState.survivors[action.id] = {...newState.survivors[action.id]};
            let survivors = newState.survivors[action.id];
            for(var id in survivors) {
                let survivor = removeStatChanges(survivors[id], action.stats);
                survivors[id] = {...survivor};
            }
            newState.survivors[action.id] = {...survivors};
        }
        return newState;
    } else if(action.type == REMOVE_STATS_FROM_ALL_SURVIVORS) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};
        let survivors = newState.survivors[action.settlementId];
        for(var id in survivors) {
            let survivor = removeStatChanges(survivors[id], action.stats);
            survivors[id] = {...survivor};
        }
        newState.survivors[action.settlementId] = {...survivors};
        return newState;
    } else if(action.type == CHANGE_GENDER) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].gender = action.gender;
        return newState;
    } else if(action.type == NOTE_EDIT_SURVIVOR) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].notes = action.note;
        return newState;
    } else if(action.type == RETIRED_TOGGLE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].retired = !newState.survivors[action.settlementId][action.id].retired;
        return newState;
    } else if(action.type == RETURN_SURVIVORS) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};

        for(let id in newState.survivors[action.settlementId]) {
            if(newState.survivors[action.settlementId][id].skipNextHunt === true && newState.survivors[action.settlementId][id].departing === false) {
                newState.survivors[action.settlementId][id] = {...newState.survivors[action.settlementId][id]};
                newState.survivors[action.settlementId][id].skipNextHunt = false;
                if(newState.survivors[action.settlementId][id].disorders.indexOf("postTraumaticStress") !== -1) {
                    newState.survivors[action.settlementId][id].disorders.splice(newState.survivors[action.settlementId][id].disorders.indexOf("postTraumaticStress"), 1);
                }
            }

            if(newState.survivors[action.settlementId][id].departing === true) {
                newState.survivors[action.settlementId][id] = {...newState.survivors[action.settlementId][id]};
                newState.survivors[action.settlementId][id].departing = false;
                newState.survivors[action.settlementId][id].returning = true;
                newState.survivors[action.settlementId][id].tempAttributes = {"MOV": 0,"STR": 0,"EVA": 0,"ACC": 0,"LCK": 0,"SPD": 0};
                newState.survivors[action.settlementId][id].weaponProficiency = {...newState.survivors[action.settlementId][id].weaponProficiency};
                newState.survivors[action.settlementId][id].weaponProficiency.earned = false;

                if(newState.survivors[action.settlementId][id].abilities.indexOf("frenzy") !== -1) {
                    newState.survivors[action.settlementId][id].abilities.splice(newState.survivors[action.settlementId][id].abilities.indexOf("frenzy"), 1);
                }

                if(newState.survivors[action.settlementId][id].disorders.indexOf("secretive") !== -1) {
                    newState.survivors[action.settlementId][id].skipNextHunt = true;
                }

            } else if(newState.survivors[action.settlementId][id].returning === true || newState.survivors[action.settlementId][id].returning >= 0) {
                newState.survivors[action.settlementId][id] = {...newState.survivors[action.settlementId][id]};
                newState.survivors[action.settlementId][id].returning = false;
            }
        }

        return newState;
    } else if(action.type == SKIP_NEXT_HUNT_TOGGLE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].skipNextHunt = !newState.survivors[action.settlementId][action.id].skipNextHunt;
        return newState;
    } else if(action.type == FATHER_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].parents.father = action.father;
        return newState;
    } else if(action.type == MOTHER_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].parents.mother = action.mother;
        return newState;
    } else if(action.type == DELETE_SETTLEMENT) {
        let newState = {...state};
        delete newState.survivors[action.id];
        return newState;
    } else if(action.type == DELETE_SURVIVOR) {
        let newState = getNewState(state, action);
        delete newState.survivors[action.settlementId][action.id];
        // also wipe their lineage
        for(let id in newState.survivors[action.settlementId]) {
            if(newState.survivors[action.settlementId][id].parents.father == action.id) {
                newState.survivors[action.settlementId][id].parents.father = null;
            }

            if(newState.survivors[action.settlementId][id].parents.mother == action.id) {
                newState.survivors[action.settlementId][id].parents.mother = null;
            }
        }
        return newState;
    } else if(action.type == DEPARTING_COLOR_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id] = {...newState.survivors[action.settlementId][action.id]};
        newState.survivors[action.settlementId][action.id].departingColor = action.color;
        return newState;
    } else if(action.type == INNOVATION_ADD) {
        let newState = {...state};
        let InnovationConfig = getInnovationConfig();

        if(InnovationConfig[action.name].allSurvivors) {
            for(var id in newState.survivors[action.id]) {
                newState.survivors[action.id][id] = applyStatChanges(newState.survivors[action.id][id], InnovationConfig[action.name].allSurvivors);
            }
        }

        return newState;
    } else if(action.type == INNOVATION_REMOVE) {
        let newState = {...state};
        let InnovationConfig = getInnovationConfig();

        if(InnovationConfig[action.name].allSurvivors) {
            for(var id in newState.survivors[action.id]) {
                newState.survivors[action.id][id] = removeStatChanges(newState.survivors[action.id][id], InnovationConfig[action.name].allSurvivors);
            }
        }

        return newState;
    } else if(action.type == CONSTELLATION_MAP_TOGGLE) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id] = setConstellationMap(newState.survivors[action.settlementId][action.id], !newState.survivors[action.settlementId][action.id].constellationMap[action.row][action.col], action.row, action.col, true);
        return newState;
    } else if(action.type == CONSTELLATION_SET) {
        let newState =getNewState(state, action);

        newState.survivors[action.settlementId][action.id] = setSurvivorConstellation(newState.survivors[action.settlementId][action.id], action.name);
        return newState;
    } else if(action.type == AFFINITY_SET) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].affinities = {...newState.survivors[action.settlementId][action.id].affinities};
        newState.survivors[action.settlementId][action.id].affinities[action.color] = action.val;
        return newState;
    } else if(action.type == SURVIVAL_ACTION_SET) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].survivalActions = {...newState.survivors[action.settlementId][action.id].survivalActions};
        newState.survivors[action.settlementId][action.id].survivalActions[action.action] = action.value;
        return newState;
    } else if(action.type == CURSED_GEAR_ADD) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].cursedItems = [...newState.survivors[action.settlementId][action.id].cursedItems];
        newState.survivors[action.settlementId][action.id].cursedItems.push({location: action.location, item: action.item});
        return newState;
    } else if(action.type == CURSED_GEAR_REMOVE) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].cursedItems = [...newState.survivors[action.settlementId][action.id].cursedItems];

        for(let i = 0; i < newState.survivors[action.settlementId][action.id].cursedItems.length; i++) {
            let item = newState.survivors[action.settlementId][action.id].cursedItems[i];

            if(item.location === action.location && item.item === action.item) {
                newState.survivors[action.settlementId][action.id].cursedItems.splice(i, 1);
                break;
            }

        }

        return newState;
    } else if(action.type == SET_CAN_GAIN_SURVIVAL) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].canGainSurvival = action.val;

        return newState;
    } else if(action.type == SURVIVOR_SURVIVAL_SET) {
        let newState =getNewState(state, action);

        // if increasing survival, and survivor can't gain survival, or is at the survival limit, return
        if(newState.survivors[action.settlementId][action.id].survival < action.val) {
            if(newState.survivors[action.settlementId][action.id].canGainSurvival === false) {
                return state;
            }

            if(action.survivalLimit) {
                if(action.val >= action.survivalLimit) {
                    return state;
                }
            }
        }

        // otherwise set new value
        newState.survivors[action.settlementId][action.id].survival = action.val;

        return newState;
    } else if(action.type == SURVIVOR_SURVIVAL_INCREASE) {
        let newState =getNewState(state, action);

        if(newState.survivors[action.settlementId][action.id].canGainSurvival === false) {
            return state;
        }

        if(action.survivalLimit) {
            if(newState.survivors[action.settlementId][action.id].survival >= action.survivalLimit) {
                return state;
            }
        }

        newState.survivors[action.settlementId][action.id].survival = newState.survivors[action.settlementId][action.id].survival + 1;
        return newState;
    } else if(action.type == SURVIVOR_SURVIVAL_DECREASE) {
        let newState =getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].survival > 0) {
            newState.survivors[action.settlementId][action.id].survival = newState.survivors[action.settlementId][action.id].survival - 1;
        }
        return newState;
    } else if(action.type == INSANITY_SET) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].insanity.value = action.val;

        return newState;
    } else if(action.type == SURVIVOR_INSANITY_INCREASE) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].insanity.value = newState.survivors[action.settlementId][action.id].insanity.value + 1;
        return newState;
    } else if(action.type == SURVIVOR_INSANITY_DECREASE) {
        let newState =getNewState(state, action);
        if(newState.survivors[action.settlementId][action.id].insanity.value > 0) {
            newState.survivors[action.settlementId][action.id].insanity.value = newState.survivors[action.settlementId][action.id].insanity.value - 1;
        }
        return newState;
    } else if(action.type == SET_UNAVAILABLE) {
        let newState =getNewState(state, action);
        newState.survivors[action.settlementId][action.id].unavailable = action.unavailable;
        return newState;
    } else if(action.type == COPY_SURVIVOR) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};
        let oldSurvivor = newState.survivors[action.settlementId][action.oldId];
        let newSurvivor = JSON.parse(JSON.stringify(oldSurvivor));
        newSurvivor.id = action.newId;
        newSurvivor.name += " (Copy)";
        newState.survivors[action.settlementId][action.newId] = newSurvivor;
        return newState;
    } else if(action.type == COPY_SETTLEMENT) {
        let newState = {...state};
        newState.survivors[action.newId] = JSON.parse(JSON.stringify(newState.survivors[action.oldId]));
        return newState;
    } else if(action.type == DELETE_ALL_FIGHTING_ART) {
        let newState = {...state};
        for(let id in newState.survivors[action.settlementId]) {
            let survivor = newState.survivors[action.settlementId][id];
            if(survivor.fightingArts.indexOf(action.faId) !== -1) {
                survivor = removeFightingArt(survivor, action.faId);
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_DISORDER) {
        let newState = {...state};
        for(let id in newState.survivors[action.settlementId]) {
            let survivor = newState.survivors[action.settlementId][id];
            if(survivor.disorders.indexOf(action.disorderId) !== -1) {
                survivor = removeDisorder(survivor, action.disorderId);
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_ABILITY) {
        let newState = {...state};
        for(let id in newState.survivors[action.settlementId]) {
            let survivor = newState.survivors[action.settlementId][id];
            if(survivor.abilities.indexOf(action.abilityId) !== -1) {
                survivor = removeAbility(survivor, action.abilityId);
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_WEAPON_PROFICIENCY) {
        let newState = {...state};
        for(let id in newState.survivors[action.settlementId]) {
            let survivor = newState.survivors[action.settlementId][id];
            if(survivor.weaponProficiency.weapon == action.profId) {
                survivor = survivor.weaponProficiency.weapon = "";
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_ARMOR_SET) {
        let newState = {...state};
        for(let id in newState.survivors[action.settlementId]) {
            let survivor = newState.survivors[action.settlementId][id];
            if(survivor.loadout && survivor.loadout.armorSet == action.armorId) {
                survivor.loadout.armorSet = "";
            }
        }
        return newState;
    } else if(action.type == DELETE_ALL_SEVERE_INJURY) {
        let newState = {...state};
        for(let id in newState.survivors[action.settlementId]) {
            let survivor = newState.survivors[action.settlementId][id];
            if(survivor.severeInjuries.indexOf(action.injuryId) !== -1) {
                survivor = removeSevereInjury(survivor, action.injuryId);
            }
        }
        return newState;
    } else if(action.type == SET_HAS_REROLL) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].hasReroll = action.hasReroll;
        return newState;
    } else if (action.type == GEAR_CURSED_DELETE) {
        let newState = { ...state };
        let survivors = newState.survivors[action.id];
        for(let id in survivors) {
            let cursed = survivors[id].cursedItems;
            for(let i = 0; i < cursed.length; i++) {
                if(
                    cursed[i].location === action.deck &&
                    cursed[i].item === action.gearId
                ) {
                    cursed.splice(i, 1);
                    break;
                }
            }
        }

        return newState;
    } else if(action.type == SET_EPITHET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].epithet = action.value;
        return newState;
    } else if(action.type == SET_PINNED) {
        let newState = getNewState(state, action);
        if(!newState.pinnedSurvivors) {
            newState.pinnedSurvivors = {};
        }

        if(!newState.pinnedSurvivors[action.settlementId]) {
            newState.pinnedSurvivors[action.settlementId] = {};
        }

        if(action.value === true) {
            newState.pinnedSurvivors[action.settlementId][action.id] = true;
        } else {
            delete newState.pinnedSurvivors[action.settlementId][action.id];
            if(Object.keys(newState.pinnedSurvivors[action.settlementId]).length === 0) {
                delete newState.pinnedSurvivors[action.settlementId];
            }
        }

        return newState;
    } else if(action.type == SURVIVOR_ADD_ACTION_LOG) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].log = addActionLog([...newState.survivors[action.settlementId][action.id].log], action.log);
        return newState;
    } else if(action.type == LOADOUT_CHANGE_GEAR) {
        /**
         * Loadout object structure:
         * {
         *   id <uuid>, optional
         *   name <string>, optional
         *   gear <ArrayOf({name, location})
         * }
         */
        if(action.x > 2 || action.y > 2) {
            return state;
        }

        let newState = getNewState(state, action);
        if(!newState.survivors[action.settlementId][action.id].loadout) {
            newState.survivors[action.settlementId][action.id].loadout = {};
        }

        if(!newState.survivors[action.settlementId][action.id].loadout.gear) {
            newState.survivors[action.settlementId][action.id].loadout.gear = [
                [null, null, null],
                [null, null, null],
                [null, null, null],
            ];
        }

        newState.survivors[action.settlementId][action.id].loadout = JSON.parse(JSON.stringify(newState.survivors[action.settlementId][action.id].loadout));
        newState.survivors[action.settlementId][action.id].loadout.gear[action.x][action.y] = action.gear;
        newState.survivors[action.settlementId][action.id].loadout.unsaved = true;
        return newState;
    } else if(action.type == LOADOUT_CHANGE_NAME) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].loadout = {...newState.survivors[action.settlementId][action.id].loadout};
        newState.survivors[action.settlementId][action.id].loadout.name = action.name;
        newState.survivors[action.settlementId][action.id].loadout.unsaved = true;
        return newState;
    } else if(action.type == LOADOUT_SAVE) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].loadout = JSON.parse(JSON.stringify(action.loadout));

        // also update any survivors who have this same loadout id who haven't made changes
        for(let id in newState.survivors[action.settlementId]) {
            if(
                newState.survivors[action.settlementId][id].loadout &&
                newState.survivors[action.settlementId][id].loadout.id === action.loadout.id &&
                !newState.survivors[action.settlementId][id].loadout.unsaved
            ) {
                newState.survivors[action.settlementId][id].loadout = JSON.parse(JSON.stringify(action.loadout));
            }
        }

        return newState;
    } else if(action.type == LOADOUT_DELETE) {
        let newState = {...state};
        for(let id in newState.survivors[action.id]) {
            if(newState.survivors[action.id][id].loadout && newState.survivors[action.id][id].loadout.id === action.loadoutId) {
                newState.survivors[action.id][id].loadout = {};
            }
        }
        return newState;
    } else if(action.type == LOADOUT_SET) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].loadout = JSON.parse(JSON.stringify(action.loadout));
        if(action.unsaved) {
            newState.survivors[action.settlementId][action.id].loadout.unsaved = true;
        }
        return newState;
    } else if (action.type == GEAR_DELETE) {
        let newState = {...state};
        for(let id in newState.survivors[action.id]) {
            if(newState.survivors[action.id][id].loadout && newState.survivors[action.id][id].loadout.gear) {
                let gear = JSON.parse(JSON.stringify(newState.survivors[action.id][id].loadout.gear));
                for(let x = 0; x < 3; x++) {
                    for(let y = 0; y < 3; y++) {
                        if(gear[x][y]) {
                            if(gear[x][y].location === action.deck && gear[x][y].name === action.gearId) {
                                gear[x][y] = null;
                            }
                        }
                    }
                }

                newState.survivors[action.id][id].loadout.gear = gear;
            }
        }
        return newState;
    } else if (action.type == SET_ARMOR_SET) {
        let newState = getNewState(state, action);
        if(!newState.survivors[action.settlementId][action.id].loadout) {
            newState.survivors[action.settlementId][action.id].loadout = {};
        }
        newState.survivors[action.settlementId][action.id].loadout.armorSet = action.name;
        newState.survivors[action.settlementId][action.id].loadout.unsaved = true;
        return newState;
    } else if (action.type == SWAP_LOADOUT_SPACE) {
        if(!state.survivors[action.settlementId][action.id].loadout || !state.survivors[action.settlementId][action.id].loadout.gear) {
            return state;
        }
        let newState = getNewState(state, action);
        let swap = state.survivors[action.settlementId][action.id].loadout.gear[action.oldX][action.oldY];
        state.survivors[action.settlementId][action.id].loadout.gear[action.oldX][action.oldY] = state.survivors[action.settlementId][action.id].loadout.gear[action.newX][action.newY];
        state.survivors[action.settlementId][action.id].loadout.gear[action.newX][action.newY] = swap;
        state.survivors[action.settlementId][action.id].loadout.unsaved = true;
        return newState;
    } else if (action.type == SET_WEAPON_PROFICIENCY_EARNED) {
        let newState = getNewState(state, action);
        state.survivors[action.settlementId][action.id].weaponProficiency.earned = action.value;
        return newState;
    } else if (action.type == SET_LOADOUT_ATTRIBUTES) {
        let newState = getNewState(state, action);
        if(!newState.survivors[action.settlementId][action.id].loadout) {
            newState.survivors[action.settlementId][action.id].loadout = {};
        }
        newState.survivors[action.settlementId][action.id].loadout.attributes = action.attributes;
        newState.survivors[action.settlementId][action.id].loadout.unsaved = true;
        return newState;
    } else if (action.type == APPLY_LOADOUT_STATS) {
        let newState = getNewState(state, action);
        if(
            newState.survivors[action.settlementId][action.id].loadout &&
            newState.survivors[action.settlementId][action.id].loadout.attributes
        ) {
            for(let stat in newState.survivors[action.settlementId][action.id].loadout.attributes) {
                newState.survivors[action.settlementId][action.id].tempAttributes[stat] += newState.survivors[action.settlementId][action.id].loadout.attributes[stat];
            }
        }
        return newState;
    } else if (action.type == SET_CAUSE_OF_DEATH) {
        let newState = getNewState(state, action);
        newState.survivors[action.settlementId][action.id].causeOfDeath = action.value;
        return newState;
    } else if (action.type == DO_INTEGRITY_CHECK) {
        let newState = {...state};
        const FightingArtConfig = getFightingArtConfig();
        const DisorderConfig = getDisorderConfig();
        const AbilityConfig = getAbilityConfig();
        const SevereInjuryConfig = getSevereInjuryConfig();
        const GearConfig = getGearConfig();

        for(let settlementId in newState.survivors) {
            for(let id in newState.survivors[settlementId]) {
                let extra = [];
                if(extra = findItemsNotInConfig(newState.survivors[settlementId][id].fightingArts, FightingArtConfig)) {
                    newState.survivors[settlementId][id].fightingArts = removeItemsFromList(extra, newState.survivors[settlementId][id].fightingArts);
                }

                if(extra = findItemsNotInConfig(newState.survivors[settlementId][id].disorders, DisorderConfig)) {
                    newState.survivors[settlementId][id].disorders = removeItemsFromList(extra, newState.survivors[settlementId][id].disorders);
                }

                if(extra = findItemsNotInConfig(newState.survivors[settlementId][id].abilities, AbilityConfig)) {
                    newState.survivors[settlementId][id].abilities = removeItemsFromList(extra, newState.survivors[settlementId][id].abilities);
                }

                if(extra = findItemsNotInConfig(newState.survivors[settlementId][id].severeInjuries, SevereInjuryConfig)) {
                    newState.survivors[settlementId][id].severeInjuries = removeItemsFromList(extra, newState.survivors[settlementId][id].severeInjuries);
                }

                // purge loadouts of disappeared gear
                if(newState.survivors[settlementId][id].loadout && newState.survivors[settlementId][id].loadout.gear) {
                    for(let i = 0; i < newState.survivors[settlementId][id].loadout.gear.length; i++) {
                        for(let j = 0; j < newState.survivors[settlementId][id].loadout.gear[i].length; j++) {
                            const gear = newState.survivors[settlementId][id].loadout.gear[i][j];
                            if(gear) {
                                if(!GearConfig[gear.location] || !GearConfig[gear.location][gear.name]) {
                                    newState.survivors[settlementId][id].loadout.gear[i][j] = null;
                                }
                            }
                        }
                    }
                }
            }
        }
        return newState;
    } else if(action.type == SCHEMA_VALIDATE) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        // added temporary stats and departing colors in 0.10.0
        if(CompareVersions(action.currentVersion, "0.10.0") === -1) {
            for(let settlementId in newState.survivors) {
                newState.survivors[settlementId] = {...newState.survivors[settlementId]};
                for(let id in newState.survivors[settlementId]) {
                    newState.survivors[settlementId][id].tempAttributes = {
                        "MOV": 0,
                        "STR": 0,
                        "EVA": 0,
                        "ACC": 0,
                        "LCK": 0,
                        "SPD": 0
                    };
                    newState.survivors[settlementId][id].departingColor = "";
                }
            }
        }

        if(CompareVersions(action.currentVersion, "1.3.1") === -1) {
            for(let settlementId in newState.survivors) {
                newState.survivors[settlementId] = {...newState.survivors[settlementId]};
                for(let id in newState.survivors[settlementId]) {
                    if(newState.survivors[settlementId][id].constellationMap) {
                        delete newState.survivors[settlementId][id].survivalActions.endure;
                    }
                }
            }
        }

        // consolidated notes in 1.5.3
        if(CompareVersions(action.currentVersion, "1.5.3") === -1) {
            for(let settlementId in newState.survivors) {
                newState.survivors[settlementId] = {...newState.survivors[settlementId]};
                for(let id in newState.survivors[settlementId]) {
                    let survivor = newState.survivors[settlementId][id];
                    let notes = survivor.notes;
                    if(Array.isArray(notes) && notes.length > 0) {
                        let newNote = notes.join("\n");
                        survivor.notes = newNote;
                    } else {
                        survivor.notes = "";
                    }
                }
            }
        }

        if(CompareVersions(action.currentVersion, "2.2.0") === -1) {
            for(let settlementId in newState.survivors) {
                newState.survivors[settlementId] = {...newState.survivors[settlementId]};
                for(let id in newState.survivors[settlementId]) {
                    newState.survivors[settlementId][id].log = [];
                }
            }
        }

        if(CompareVersions(action.currentVersion, "2.6.0") === -1) {
            for(let settlementId in newState.survivors) {
                newState.survivors[settlementId] = {...newState.survivors[settlementId]};
                for(let id in newState.survivors[settlementId]) {
                    newState.survivors[settlementId][id].loadout = {};
                }
            }
        }

        return newState;
    } else {
        return state;
    }

    function getNewState(state, action) {
        let newState = {...state};
        newState.survivors = {...newState.survivors};
        newState.survivors[action.settlementId] = {...newState.survivors[action.settlementId]};
        newState.survivors[action.settlementId][action.id] = {...newState.survivors[action.settlementId][action.id]};

        return newState;
    }
};
