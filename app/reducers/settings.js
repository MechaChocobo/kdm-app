import {VERSION_SET, SET_LOCALE, SET_CAMERA, SET_THEME, SET_THEME_COLOR, RESET_THEME, SET_COLOR, RESET_COLORS, SET_DEPART_VIEW, SET_BACKUP_SETTINGS, IMPORT_SETTINGS, SET_NUM_LINES_SURVIVOR_CARD, SET_DATE_FORMAT, SET_TAP_TO_DEPART, SET_AUTO_HOST_SETTLEMENT, SET_SURVIVOR_NAME_GENERATION, SET_DEFAULT_IP_ADDRESS, SET_SHOW_DEPARTING_INDEX} from '../actions/settings';
import { setCachedLocale } from '../helpers/intl';
import colors from '../style/colors';
import themes from '../style/themes';
import { DELETE_SETTLEMENT } from '../actions/settlement';

let initialState = {
    version: '',
    locale: "",
    camera: "back",
    theme: "dark",
    themeType: "dark",
    customDarkThemeColors: {...themes.dark},
    customLightThemeColors: {...themes.light},
    colors: {...colors},
    departView: "swipe",
    backupSettings: true,
    numLinesSurvivorCard: 2,
    dateFormat: "mdy",
    tapToDepart: false,
    autoHostSettlement: null,
    survivorNameGeneration: "net",
    defaultIpAddress: '',
    showDepartingIndex: false,
};

export const settingsReducer = (state = initialState, action) => {
    if(action.type === VERSION_SET) {
        let newState = {...state, version: action.version};
        return newState;
    } else if(action.type === SET_LOCALE) {
        let newState = {...state, locale: action.locale};
        setCachedLocale(newState.locale);
        return newState;
    } else if(action.type === SET_CAMERA) {
        let newState = {...state, camera: action.camera};
        return newState;
    } else if(action.type === SET_THEME) {
        let newState = {...state, theme: action.theme};
        return newState;
    } else if(action.type === SET_THEME_COLOR) {
        let newState = {...state};

        if(action.themeType === "customDark") {
            newState.customDarkThemeColors = {...newState.customDarkThemeColors};
            newState.customDarkThemeColors[action.colorType] = action.color;
        } else {
            newState.customLightThemeColors = {...newState.customLightThemeColors};
            newState.customLightThemeColors[action.colorType] = action.color;
        }

        return newState;
    } else if(action.type === RESET_THEME) {
        let newState = {...state};

        if(action.themeType === "customDark") {
            newState.customDarkThemeColors = {...themes.dark};
        } else {
            newState.customLightThemeColors = {...themes.light};
        }

        return newState;
    } else if(action.type === SET_COLOR) {
        let newState = {...state};
        newState.colors = {...newState.colors};
        newState.colors[action.colorType] = action.color;

        return newState;
    } else if(action.type === RESET_COLORS) {
        let newState = {...state};

        newState.colors = {...colors};

        return newState;
    } else if(action.type === SET_DEPART_VIEW) {
        let newState = {...state};

        newState.departView = action.value;

        return newState;
    } else if(action.type === SET_BACKUP_SETTINGS) {
        let newState = {...state};

        newState.backupSettings = action.value;

        return newState;
    } else if(action.type === IMPORT_SETTINGS) {
        let newState = {...state};

        if(newState.locale !== action.settings.locale) {
            setCachedLocale(action.settings.locale);
        }

        newState = {...newState, ...action.settings};

        return newState;
    } else if(action.type === SET_NUM_LINES_SURVIVOR_CARD) {
        if(action.value > 10 || action.value < 1) {
            return state;
        }

        let newState = {...state};

        newState.numLinesSurvivorCard = action.value;

        return newState;
    } else if(action.type === SET_DATE_FORMAT) {
        let newState = {...state};
        newState.dateFormat = action.value;
        return newState;
    } else if(action.type === SET_TAP_TO_DEPART) {
        let newState = {...state};
        newState.tapToDepart = action.value;
        return newState;
    } else if(action.type === SET_AUTO_HOST_SETTLEMENT) {
        let newState = {...state};
        newState.autoHostSettlement = action.id;
        return newState;
    } else if(action.type === DELETE_SETTLEMENT) {
        let newState = {...state};
        if(newState.autoHostSettlement === action.id) {
            newState.autoHostSettlement = null;
        }

        return newState;
    } else if(action.type === SET_SURVIVOR_NAME_GENERATION) {
        let newState = {...state};
        newState.survivorNameGeneration = action.value;
        return newState;
    } else if(action.type === SET_DEFAULT_IP_ADDRESS) {
        let newState = {...state};
        newState.defaultIpAddress = action.value;
        return newState;
    } else if(action.type === SET_SHOW_DEPARTING_INDEX) {
        let newState = {...state};
        newState.showDepartingIndex = action.value;
        return newState;
    } else {
        return state;
    }
}
