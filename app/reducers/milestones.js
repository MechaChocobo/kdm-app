import { APPLY_GLOBAL_MILESTONE, DELETE_MILESTONE, DELETE_ALL_GLOBAL_MILESTONE_CONDITION, DELETE_ALL_GLOBAL_SETTLEMENT_MILESTONE_CONDITION } from "../actions/custom-expansions";
import { SET_MILESTONE_COMPLETE, IMPORT_MILESTONES, SET_CONDITION_VALUE, SET_SETTLEMENT_CONDITION_VALUE, ADD_SETTLEMENT_TO_GLOBAL_MILESTONES, ADD_SETTLEMENT_TO_CONDITIONS, REMOVE_SETTLEMENT_FROM_CONDITIONS } from "../actions/milestones";
import { DELETE_SETTLEMENT } from "../actions/settlement";
import { DO_INTEGRITY_CHECK } from "../actions/global";
import { getMilestoneConfig } from "../helpers/helpers";

/**
*
 */

let initialState = {
};

export const milestones = (state = initialState, action) => {
    if(action.type === APPLY_GLOBAL_MILESTONE) {
        let newState = {...state};
        if(!newState[action.milestone.id]) {
            newState[action.milestone.id] = {
                id: action.milestone.id,
                complete: false,
            }
        }

        if(action.milestone.conditions) {
            newState[action.milestone.id] = applyConditions(newState[action.milestone.id], action.milestone.conditions);
        }

        if(action.milestone.settlementConditions) {
            newState[action.milestone.id] = applySettlementConditions(newState[action.milestone.id], action.milestone.settlementConditions, action.settlementIds);
        }
        return newState;
    } else if(action.type === SET_MILESTONE_COMPLETE) {
        let newState = {...state};
        newState[action.id] = {...newState[action.id]};
        newState[action.id].complete = action.value;
        return newState;
    } else if(action.type === SET_CONDITION_VALUE) {
        let newState = {...state};
        newState[action.milestoneId] = {...newState[action.milestoneId]};
        newState[action.milestoneId].conditions = {...newState[action.milestoneId].conditions};
        newState[action.milestoneId].conditions[action.id] = {...newState[action.milestoneId].conditions[action.id]};
        newState[action.milestoneId].conditions[action.id].value = action.value;
        if(action.value >= action.target) {
            newState[action.milestoneId].conditions[action.id].complete = true;
        } else {
            newState[action.milestoneId].conditions[action.id].complete = false;
        }
        return newState;
    } else if(action.type === SET_SETTLEMENT_CONDITION_VALUE) {
        let newState = {...state};
        newState[action.milestoneId] = {...newState[action.milestoneId]};
        newState[action.milestoneId].settlementConditions = {...newState[action.milestoneId].settlementConditions};
        newState[action.milestoneId].settlementConditions[action.id] = {...newState[action.milestoneId].settlementConditions[action.id]};
        newState[action.milestoneId].settlementConditions[action.id].settlements = {...newState[action.milestoneId].settlementConditions[action.id].settlements};
        newState[action.milestoneId].settlementConditions[action.id].settlements[action.settlementId] = {...newState[action.milestoneId].settlementConditions[action.id].settlements[action.settlementId]};
        newState[action.milestoneId].settlementConditions[action.id].settlements[action.settlementId].value = action.value;
        if(action.value >= action.target) {
            newState[action.milestoneId].settlementConditions[action.id].complete = true;
            newState[action.milestoneId].settlementConditions[action.id].settlements[action.settlementId].complete = true;
        } else {
            newState[action.milestoneId].settlementConditions[action.id].settlements[action.settlementId].complete = false;
            // if no other settlements have completed this milestone then undo completion at a global level
            if(newState[action.milestoneId].settlementConditions[action.id].complete === true) {
                let complete = false;
                for(let id in newState[action.milestoneId].settlementConditions[action.id].settlements) {
                    if(newState[action.milestoneId].settlementConditions[action.id].settlements[id].complete === true) {
                        complete = true;
                        break;
                    }
                }

                if(!complete) {
                    newState[action.milestoneId].settlementConditions[action.id].complete = false;
                }
            }
        }
        return newState;
    } else if(action.type === IMPORT_MILESTONES) {
        let newState = {...state, ...action.milestones};
        return newState;
    } else if(action.type === DELETE_SETTLEMENT) {
        let newState = {...state};
        for(let id in newState) {
            if(newState[id].settlementConditions && Object.keys(newState[id].settlementConditions).length > 0) {
                for(let conditionId in newState[id].settlementConditions) {
                    if(newState[id].settlementConditions[conditionId].settlements[action.id]) {
                        delete newState[id].settlementConditions[conditionId].settlements[action.id];
                    }
                }
            }
        }
        return newState;
    } else if(action.type === DELETE_MILESTONE) {
        let newState = {...state};
        delete newState[action.id];
        return newState;
    } else if(action.type === DELETE_ALL_GLOBAL_MILESTONE_CONDITION) {
        let newState = {...state};
        delete newState[action.id].conditions[action.conditionId];
        return newState;
    } else if(action.type === DELETE_ALL_GLOBAL_SETTLEMENT_MILESTONE_CONDITION) {
        let newState = {...state};
        if(newState[action.id] && newState[action.id].settlementConditions[action.conditionId]) {
            delete newState[action.id].settlementConditions[action.conditionId];
        }
        return newState;
    } else if(action.type === ADD_SETTLEMENT_TO_CONDITIONS) {
        let newState = {...state};
        if(newState[action.milestoneId].settlementConditions) {
            for(let conditionId in newState[action.milestoneId].settlementConditions) {
                if(!newState[action.milestoneId].settlementConditions[conditionId].settlements[action.id]) {
                    newState[action.milestoneId].settlementConditions[conditionId].settlements[action.id] = {
                        value: 0,
                        complete: false,
                    }
                }
            }
        }
        return newState;
    } else if(action.type === REMOVE_SETTLEMENT_FROM_CONDITIONS) {
        let newState = {...state};
        if(newState[action.milestoneId].settlementConditions) {
            for(let conditionId in newState[action.milestoneId].settlementConditions) {
                if(newState[action.milestoneId].settlementConditions[conditionId].settlements[action.id]) {
                    delete newState[action.milestoneId].settlementConditions[conditionId].settlements[action.id]
                }
            }
        }
        return newState;
    } else if(action.type === DO_INTEGRITY_CHECK) {
        let newState = {...state};
        const MilestoneConfig = getMilestoneConfig();
        for(let id in newState) {
            if(!MilestoneConfig[id]) {
                delete newState[id];
            }
        }
        return newState;
    } else {
        return state;
    }
}

function applyConditions(milestone, conditions) {
    if(conditions) {
        if(!milestone.conditions) {
            milestone.conditions = {};
        }

        for(let id in conditions) {
            if(!milestone.conditions[id]) {
                milestone.conditions[id] = {
                    value: 0,
                    complete: false,
                };
            }
        }
    }

    return milestone;
}

function applySettlementConditions(milestone, conditions, settlementIds) {
    if(conditions) {
        if(!milestone.settlementConditions) {
            milestone.settlementConditions = {};
        }

        for(let id in conditions) {
            if(!milestone.settlementConditions[id]) {
                milestone.settlementConditions[id] = {
                    complete: false,
                    settlements: {},
                };
            }

            for(let i = 0; i < settlementIds.length; i++) {
                if(!milestone.settlementConditions[id].settlements[settlementIds[i]]) {
                    milestone.settlementConditions[id].settlements[settlementIds[i]] = {
                        value: 0,
                        complete: false,
                    };
                }
            }
        }
    }

    return milestone;
}
