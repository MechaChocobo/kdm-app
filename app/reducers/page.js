import { PAGE_UPDATE, GOTO_SETTLEMENT, GOTO_SURVIVOR, GO_BACK, SURVIVOR_INDEX_CLEAR, GOTO_SURVIVOR_LIST, SET_PAGE, SET_DEPARTING_SURVIVOR, GOTO_CUSTOM_EXPANSION, RESET_NAV_STACK, SET_GOING_BACK, SET_PAGE_TRANSITIONING, SET_MENU_DRAWER_OPEN, GOTO_TIMELINE_LY, SET_SURVIVOR_HOME_TAB, ADD_TRANSITION_ACTION, CLEAR_TRANSITION_ACTIONS } from "../actions/router";
import { DELETE_SETTLEMENT } from "../actions/settlement";
import { DELETE_SURVIVOR, RETURN_SURVIVORS } from "../actions/survivors";

let pageState = {
    page: [{page: 'home', settlementId: null, survivorId: null}],
    settlementId: null,
    prevSettlementId: null,
    survivorId: null,
    prevSurvivorId: null,
    selectedSurvivorIndex: 0,
    departingSurvivorId: null,
    customExpansionId: null,
    goingBack: false,
    transitioning: false,
    menuDrawerOpen: false,
    timelineLy: null,
    prevTimelineLy: null,
    survivorHomeTab: null,
    transitionCompleteActions: [],
};

export const pageReducer = (state = pageState, action) => {
    if(action.type == PAGE_UPDATE) {
        let newState = {...state};
        let prevPage = newState.page[newState.page.length - 1];
        let settlementId = prevPage.settlementId;
        let survivorId = prevPage.survivorId;
        let prevSettlementId = newState.prevSettlementId;
        let prevSurvivorId = newState.prevSurvivorId;

        if(prevPage.settlementId) {
            prevSettlementId = prevPage.settlementId;
        }

        if(prevPage.survivorId) {
            prevSurvivorId = prevPage.survivorId;
        }

        if(action.page == "home") {
            prevSettlementId = settlementId;
            prevSurvivorId = survivorId;
            settlementId = null;
            survivorId = null;
        }

        newState.page = [...newState.page, {page: action.page, settlementId: settlementId, survivorId: survivorId}];
        newState.settlementId = settlementId;
        newState.survivorId = survivorId;
        newState.prevSettlementId = prevSettlementId;
        newState.prevSurvivorId = prevSurvivorId;
        return newState;
    } else if(action.type == GOTO_SETTLEMENT) {
        let newState = {...state};
        newState.page = [...newState.page, {page: "settlement", settlementId: action.id, survivorId: null}];
        newState.settlementId = action.id;
        return newState;
    } else if(action.type == GOTO_SURVIVOR) {
        let newState = {...state};
        let prevPage = newState.page[newState.page.length - 1];
        newState.page = [...newState.page, {page: "survivor", settlementId: prevPage.settlementId, survivorId: action.id}];
        newState.survivorId = action.id;
        return newState;
    } else if(action.type == GOTO_TIMELINE_LY) {
        let newState = {...state};
        let prevPage = newState.page[newState.page.length - 1];
        newState.page = [...newState.page, {page: "timeline-ly", settlementId: prevPage.settlementId, survivorId: null, timelineLy: action.id}];
        newState.timelineLy = action.id;
        return newState;
    } else if(action.type == GO_BACK) {
        let newState = {...state};
        newState.page = [...newState.page];
        newState.page.splice(newState.page.length - 1, 1);
        newState.prevSettlementId = newState.settlementId;
        newState.settlementId = newState.page[newState.page.length - 1].settlementId;
        newState.prevSurvivorId = newState.survivorId;
        newState.survivorId = newState.page[newState.page.length - 1].survivorId;
        newState.prevTimelineLy = newState.timelineLy;
        newState.timelineLy = newState.page[newState.page.length - 1].timelineLy;
        newState.goingBack = true;
        return newState;
    } else if(action.type == SURVIVOR_INDEX_CLEAR) {
        return {...state, lastSelectedSurvivorIndex: 0};
    } else if(action.type == DELETE_SETTLEMENT) {
        let newState = {...state};
        newState.page = newState.page.filter(page => page.settlementId !== action.id);
        return newState;
    } else if(action.type == DELETE_SURVIVOR) {
        let newState = {...state};
        newState.page = newState.page.filter(page => page.survivorId !== action.id);
        newState.lastSelectedSurvivorIndex = 0;
        return newState;
    } else if(action.type == GOTO_SURVIVOR_LIST) {
        let newState = {...state};
        newState.page = [...newState.page, {page: "survivor-list", settlementId: newState.settlementId, survivorId: null}];
        newState.selectedSurvivorIndex = action.index;
        return newState;
    } else if(action.type == SET_PAGE) {
        let newState = {...state};
        newState.page = [{page: action.page, settlementId: null, survivorId: null}];
        return newState;
    } else if(action.type == SET_DEPARTING_SURVIVOR) {
        let newState = {...state};
        newState.departingSurvivorId = action.id;
        return newState;
    } else if(action.type == GOTO_CUSTOM_EXPANSION) {
        let newState = {...state};
        newState.customExpansionId = action.id;
        newState.page = [...newState.page, {page: "custom-expansion", settlementId: null, survivorId: null}];
        return newState;
    } else if(action.type == RETURN_SURVIVORS) {
        let newState = {...state};

        let prevPage = newState.page[newState.page.length - 1];
        // if you're on the departing survivors when they return then we need to move you
        if(prevPage.page === "survivor-list") {
            newState.page.splice(newState.page.length - 1, 1);
            newState.page = [...newState.page, {page: "survivor", settlementId: prevPage.settlementId, survivorId: newState.departingSurvivorId}];
            newState.survivorId = newState.departingSurvivorId;
            newState.departingSurvivorId = null;
        }

        return newState;
    } else if(action.type == RESET_NAV_STACK) {
        let newState = {...state};

        let currentPage = newState.page[newState.page.length - 1];
        newState.page = [{page: "home", settlementId: null, survivorId: null}, currentPage];

        return newState;
    } else if(action.type == SET_GOING_BACK) {
        let newState = {...state};

        newState.goingBack = action.value;

        return newState;
    } else if(action.type == SET_PAGE_TRANSITIONING) {
        let newState = {...state};

        newState.transitioning = action.value;

        return newState;
    } else if(action.type === SET_MENU_DRAWER_OPEN) {
        let newState = {...state};

        newState.menuDrawerOpen = action.value;
        return newState;
    } else if(action.type === SET_SURVIVOR_HOME_TAB) {
        let newState = {...state};

        newState.survivorHomeTab = action.index;
        return newState;
    } else if(action.type === ADD_TRANSITION_ACTION) {
        let newState = {...state};

        newState.transitionCompleteActions.push({fn: action.fn, args: action.args});
        return newState;
    } else if(action.type === CLEAR_TRANSITION_ACTIONS) {
        let newState = {...state};

        newState.transitionCompleteActions = [];
        return newState;
    } else {
        return state;
    }
};
