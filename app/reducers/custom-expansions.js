import Uuid from 'react-native-uuid';
import CompareVersions from 'compare-versions';

import { setCustomExpansionConfigs, getUuidSeedFromString } from "../helpers/helpers";
import { SET_INNOVATION_CONFIG, ADD_INNOVATION, DELETE_INNOVATION, SET_LOCATION_CONFIG, DELETE_LOCATION, ADD_LOCATION, SET_RESOURCE_CONFIG, DELETE_SINGLE_RESOURCE, ADD_RESOURCE, DELETE_RESOURCE_SOURCE, ADD_RESOURCE_SOURCE, SET_GEAR_CONFIG, DELETE_SINGLE_GEAR, ADD_GEAR, ADD_GEAR_LOCATION, DELETE_GEAR_LOCATION, SET_FA_CONFIG, DELETE_FA, ADD_FA, SET_DISORDER_CONFIG, DELETE_DISORDER, ADD_DISORDER, SET_ABILITY_CONFIG, DELETE_ABILITY, ADD_ABILITY, SET_INJURY_CONFIG, DELETE_INJURY, ADD_INJURY, SET_SETTLEMENT_EVENT_CONFIG, DELETE_SETTLEMENT_EVENT, ADD_SETTLEMENT_EVENT, SET_STORY_EVENT, ADD_STORY_EVENT, DELETE_STORY_EVENT, SET_SHOWDOWN, ADD_SHOWDOWN, DELETE_SHOWDOWN, SET_NEMESIS, ADD_NEMESIS, DELETE_NEMESIS, SET_LY_EVENT_CONFIG, ADD_LY_TO_TIMELINE, DELETE_LY, CREATE_EXPANSION, IMPORT_EXPANSIONS, VALIDATE_CE_SCHEMA, DELETE_EXPANSION, SET_CUSTOM_EXPANSION_NAME, SET_WEAPON_PROF_CONFIG, DELETE_WEAPON_PROF, ADD_WEAPON_PROF, SET_GEAR_CURSED, SET_ARMOR_SET_CONFIG, DELETE_ARMOR_SET, ADD_ARMOR_SET, ADD_MILESTONE, SET_MILESTONE_CONFIG, DELETE_MILESTONE, SET_CUSTOM_EXPANSION_DESCRIPTION, SET_CUSTOM_EXPANSION_VERSION, SET_CUSTOM_EXPANSION_RELEASE_NOTE, SET_SHOWDOWN_CONFIG } from "../actions/custom-expansions";
import { SCHEMA_VALIDATE } from "../actions/settings";

/**
 * Expansion schema should be:
 * {
 *      name: String,
 *      id: String,
 *
 *      // settlement stuff
 *      principles: {
 *          newLife: [string options]
 *      },
 *      innovations: {},
 *      locations: {},
 *      globalEndeavors: {},
 *      resources: {},
 *      gear: {},
 *      cursed gear: {},
 *      weapon specialty: {}
 *
 *      // timeline
 *      // ability to add events to the timeline
 *      story events: {},
 *      settlement events: {},
 *      showdowns: [],
 *      nemesis encounters: [],
 *
 *      // survivor stuff
 *      fighting arts: {},
 *      disorders: {},
 *      severe injuries: {},
 *      abilities/impairments: {},
 *      weaponProficiencies: {},
 *      milestones: {},
 * }
 * Milestone:
 * {
 *   title: string,
 *   id: string,
 *   description: string,
 *   type: GLOBAL | SETTLEMENT
 *   ?conditions: {
 *     <id>: Condition,
 *   }
 *   ?settlementConditions: {
 *     <id>: Condition,
 *   }
 *   ?effects: Effect,
 *   ?reward: string,
 * }
 *
 * Condition: {
 *   id: string,
 *   description: string,
 *   value: number,
 *   ?reward: string,
 *   ?effects: Effect,
 * }
 *
 * Effect: {
 *   ?addFightingArt: string/array
 *   ?addGear: string/array
 * }
 */

let initialState = {};

export const MILESTONE_TYPE_GLOBAL = 1;
export const MILESTONE_TYPE_SETTLEMENT = 2;

export const customExpansions = (state = initialState, action) => {
    if(action.type === SET_INNOVATION_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].innovationConfig[action.innovationId] = action.config;
        if(!newState[action.expansionId].innovationConfig[action.innovationId].expansion) {
            newState[action.expansionId].innovationConfig[action.innovationId].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_INNOVATION) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].innovations = [...newState[action.expansionId].innovations];
        newState[action.expansionId].innovationConfig = {...newState[action.expansionId].innovationConfig};

        newState[action.expansionId].innovations.splice(newState[action.expansionId].innovations.indexOf(action.innovationId), 1);
        delete newState[action.expansionId].innovationConfig[action.innovationId];

        for(let id in newState[action.expansionId].innovationConfig) {
            if(newState[action.expansionId].innovationConfig[id].consequenceOf.indexOf(action.innovationId) !== -1) {
                newState[action.expansionId].innovationConfig[id].consequenceOf.splice(newState[action.expansionId].innovationConfig[id].consequenceOf.indexOf(action.innovationId), 1);
                newState[action.expansionId].innovationConfig[id].consequenceOf = [...newState[action.expansionId].innovationConfig[id].consequenceOf];
            }
        }

        return newState;
    } else if(action.type === ADD_INNOVATION) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].innovations = [...newState[action.expansionId].innovations];
        newState[action.expansionId].innovationConfig = {...newState[action.expansionId].innovationConfig};

        let id = Uuid.v4();

        newState[action.expansionId].innovations.splice(0, 0, id);
        newState[action.expansionId].innovationConfig[id] = {
            "name" : id,
            "title": "",
            "consequenceOf": [],
            "description": "",
            "settlementEffects": {
            },
            expansion: action.expansionId,
        }

        return newState;
    } else if(action.type === SET_LOCATION_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].locationConfig[action.locationId] = action.config;
        if(!newState[action.expansionId].locationConfig[action.locationId].expansion) {
            newState[action.expansionId].locationConfig[action.locationId].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_LOCATION) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].locations = [...newState[action.expansionId].locations];
        newState[action.expansionId].locationConfig = {...newState[action.expansionId].locationConfig};

        newState[action.expansionId].locations.splice(newState[action.expansionId].locations.indexOf(action.locationId), 1);
        delete newState[action.expansionId].locationConfig[action.locationId];

        return newState;
    } else if(action.type === ADD_LOCATION) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].locations = [...newState[action.expansionId].locations];
        newState[action.expansionId].locationConfig = {...newState[action.expansionId].locationConfig};

        let id = Uuid.v4();
        newState[action.expansionId].locations.splice(0, 0, id);
        newState[action.expansionId].locationConfig[id] = {
            "name" : id,
            "title": "",
            "settlementEffects": {
                "endeavors" : []
            },
            expansion: action.expansionId,
        }

        return newState;
    } else if(action.type === SET_RESOURCE_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].resourceConfig = {...newState[action.expansionId].resourceConfig};
        newState[action.expansionId].resourceConfig[action.source] = {...newState[action.expansionId].resourceConfig[action.source]};

        newState[action.expansionId].resourceConfig[action.source][action.resourceId] = action.config;

        if(!newState[action.expansionId].resourceConfig[action.source][action.resourceId].expansion) {
            newState[action.expansionId].resourceConfig[action.source][action.resourceId].expansion = action.expansionId;
        }

        return newState;
    } else if(action.type === DELETE_SINGLE_RESOURCE) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].resourceConfig = {...newState[action.expansionId].resourceConfig};
        newState[action.expansionId].resourceConfig[action.source] = {...newState[action.expansionId].resourceConfig[action.source]};

        if(newState[action.expansionId].milestones) {
            newState[action.expansionId].milestones = removeStorageRewardFromMilestones(newState[action.expansionId].milestones, 'addResource', action.source, action.resourceId);
        }

        delete newState[action.expansionId].resourceConfig[action.source][action.resourceId];

        return newState;
    } else if(action.type === ADD_RESOURCE) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].resourceConfig = {...newState[action.expansionId].resourceConfig};

        let id = Uuid.v4();
        let newResource = {};
        newResource[id] = {
            id,
            title: "",
            description: "",
            keywords: [],
            expansion: action.expansionId,
        }

        newState[action.expansionId].resourceConfig[action.source] = {...newResource, ...newState[action.expansionId].resourceConfig[action.source]};

        return newState;
    } else if(action.type === ADD_RESOURCE_SOURCE) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].resourceConfig = {...newState[action.expansionId].resourceConfig};
        if(!newState[action.expansionId].resourceConfig[action.source]) {
            let newResource = {};
            newResource[action.source] = {};
            let id = Uuid.v4();
            newResource[action.source][id] = {
                id,
                title: "",
                description: "",
                keywords: [],
                expansion: action.expansionId,
            }
            newState[action.expansionId].resourceConfig = {...newResource, ...newState[action.expansionId].resourceConfig};
        }

        return newState;
    } else if(action.type === DELETE_RESOURCE_SOURCE) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].resourceConfig = {...newState[action.expansionId].resourceConfig};

        if(newState[action.expansionId].milestones) {
            for(let resourceId in newState[action.expansionId].resourceConfig[action.source]) {
                newState[action.expansionId].milestones = removeStorageRewardFromMilestones(newState[action.expansionId].milestones, 'addResource', action.source, resourceId);
            }
        }

        delete newState[action.expansionId].resourceConfig[action.source];

        return newState;
    } else if(action.type === SET_GEAR_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].gearConfig = {...newState[action.expansionId].gearConfig};
        newState[action.expansionId].gearConfig[action.location] = {...newState[action.expansionId].gearConfig[action.location]};

        newState[action.expansionId].gearConfig[action.location][action.gearId] = action.config;

        if(!newState[action.expansionId].gearConfig[action.location][action.gearId].expansion) {
            newState[action.expansionId].gearConfig[action.location][action.gearId].expansion = action.expansionId;
        }

        return newState;
    } else if(action.type === DELETE_SINGLE_GEAR) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].gearConfig = {...newState[action.expansionId].gearConfig};
        newState[action.expansionId].gearConfig[action.location] = {...newState[action.expansionId].gearConfig[action.location]};

        delete newState[action.expansionId].gearConfig[action.location][action.gearId];
        if(newState[action.expansionId].milestones) {
            newState[action.expansionId].milestones = removeStorageRewardFromMilestones(newState[action.expansionId].milestones, 'addGear', action.location, action.gearId);
        }
        return newState;
    } else if(action.type === ADD_GEAR) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].gearConfig = {...newState[action.expansionId].gearConfig};

        let id = Uuid.v4();
        let newGear = {};
        newGear[id] = {
            id,
            title: "",
            description: "",
            expansion: action.expansionId,
        }

        newState[action.expansionId].gearConfig[action.location] = {...newGear, ...newState[action.expansionId].gearConfig[action.location]};

        return newState;
    } else if(action.type === ADD_GEAR_LOCATION) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].gearConfig = {...newState[action.expansionId].gearConfig};
        if(!newState[action.expansionId].gearConfig[action.location]) {
            let newGear = {};
            newGear[action.location] = {};
            let id = Uuid.v4();
            newGear[action.location][id] = {
                id,
                title: "",
                description: "",
                expansion: action.expansionId,
            }
            newState[action.expansionId].gearConfig = {...newGear, ...newState[action.expansionId].gearConfig};
        }

        return newState;
    } else if(action.type === DELETE_GEAR_LOCATION) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].gearConfig = {...newState[action.expansionId].gearConfig};

        if(newState[action.expansionId].milestones) {
            for(let gearId in newState[action.expansionId].gearConfig[action.location]) {
                newState[action.expansionId].milestones = removeStorageRewardFromMilestones(newState[action.expansionId].milestones, 'addGear', action.location, gearId);
            }
        }

        delete newState[action.expansionId].gearConfig[action.location];

        return newState;
    } else if(action.type === SET_FA_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].fightingArtConfig = {...newState[action.expansionId].fightingArtConfig};
        newState[action.expansionId].fightingArtConfig[action.id] = action.config;
        if(!newState[action.expansionId].fightingArtConfig[action.id].expansion) {
            newState[action.expansionId].fightingArtConfig[action.id].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_FA) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].fightingArts = [...newState[action.expansionId].fightingArts];
        newState[action.expansionId].fightingArtConfig = {...newState[action.expansionId].fightingArtConfig};

        delete newState[action.expansionId].fightingArtConfig[action.id];
        newState[action.expansionId].fightingArts.splice(newState[action.expansionId].fightingArts.indexOf(action.id), 1);

        if(newState[action.expansionId].milestones) {
            newState[action.expansionId].milestones = removeRewardFromMilestones(newState[action.expansionId].milestones, 'addFightingArt', action.id);
        }

        return newState;
    } else if(action.type === ADD_FA) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].fightingArts = [...newState[action.expansionId].fightingArts];
        newState[action.expansionId].fightingArtConfig = {...newState[action.expansionId].fightingArtConfig};

        let id = Uuid.v4();
        newState[action.expansionId].fightingArts.splice(0, 0, id);
        newState[action.expansionId].fightingArtConfig[id] = {
            name: id,
            title: "",
            description: "",
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === SET_DISORDER_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].disorderConfig = {...newState[action.expansionId].disorderConfig};
        newState[action.expansionId].disorderConfig[action.id] = action.config;
        if(!newState[action.expansionId].disorderConfig[action.id].expansion) {
            newState[action.expansionId].disorderConfig[action.id].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_DISORDER) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].disorders = [...newState[action.expansionId].disorders];
        newState[action.expansionId].disorderConfig = {...newState[action.expansionId].disorderConfig};

        delete newState[action.expansionId].disorderConfig[action.id];
        newState[action.expansionId].disorders.splice(newState[action.expansionId].disorders.indexOf(action.id), 1);
        return newState;
    } else if(action.type === ADD_DISORDER) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].disorders = [...newState[action.expansionId].disorders];
        newState[action.expansionId].disorderConfig = {...newState[action.expansionId].disorderConfig};

        let id = Uuid.v4();
        newState[action.expansionId].disorders.splice(0, 0, id);
        newState[action.expansionId].disorderConfig[id] = {
            name: id,
            title: "",
            description: "",
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === SET_ABILITY_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].abilityConfig = {...newState[action.expansionId].abilityConfig};
        newState[action.expansionId].abilityConfig[action.id] = action.config;
        if(!newState[action.expansionId].abilityConfig[action.id].expansion) {
            newState[action.expansionId].abilityConfig[action.id].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_ABILITY) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].abilities = [...newState[action.expansionId].abilities];
        newState[action.expansionId].abilityConfig = {...newState[action.expansionId].abilityConfig};

        delete newState[action.expansionId].abilityConfig[action.id];
        newState[action.expansionId].abilities.splice(newState[action.expansionId].abilities.indexOf(action.id), 1);
        return newState;
    } else if(action.type === ADD_ABILITY) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].abilities = [...newState[action.expansionId].abilities];
        newState[action.expansionId].abilityConfig = {...newState[action.expansionId].abilityConfig};

        let id = Uuid.v4();
        newState[action.expansionId].abilities.splice(0, 0, id);
        newState[action.expansionId].abilityConfig[id] = {
            name: id,
            title: "",
            description: "",
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === SET_INJURY_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].severeInjuryConfig = {...newState[action.expansionId].severeInjuryConfig};
        newState[action.expansionId].severeInjuryConfig[action.id] = action.config;
        return newState;
    } else if(action.type === DELETE_INJURY) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].severeInjuries = [...newState[action.expansionId].severeInjuries];
        newState[action.expansionId].severeInjuryConfig = {...newState[action.expansionId].severeInjuryConfig};

        delete newState[action.expansionId].severeInjuryConfig[action.id];
        newState[action.expansionId].severeInjuries.splice(newState[action.expansionId].severeInjuries.indexOf(action.id), 1);
        return newState;
    } else if(action.type === ADD_INJURY) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].severeInjuries = [...newState[action.expansionId].severeInjuries];
        newState[action.expansionId].severeInjuryConfig = {...newState[action.expansionId].severeInjuryConfig};

        let id = Uuid.v4();
        newState[action.expansionId].severeInjuries.splice(0, 0, id);
        newState[action.expansionId].severeInjuryConfig[id] = {
            name: id,
            title: "",
            description: "",
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === SET_SETTLEMENT_EVENT_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].settlementEventConfig[action.eventId] = action.config;
        if(!newState[action.expansionId].settlementEventConfig[action.eventId].expansion) {
            newState[action.expansionId].settlementEventConfig[action.eventId].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_SETTLEMENT_EVENT) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].settlementEvents = [...newState[action.expansionId].settlementEvents];
        newState[action.expansionId].settlementEventConfig = {...newState[action.expansionId].settlementEventConfig};

        newState[action.expansionId].settlementEvents.splice(newState[action.expansionId].settlementEvents.indexOf(action.eventId), 1);
        delete newState[action.expansionId].settlementEventConfig[action.eventId];

        return newState;
    } else if(action.type === ADD_SETTLEMENT_EVENT) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].settlementEvents = [...newState[action.expansionId].settlementEvents];
        newState[action.expansionId].settlementEventConfig = {...newState[action.expansionId].settlementEventConfig};

        let id = Uuid.v4();
        newState[action.expansionId].settlementEvents.splice(0, 0, id);
        newState[action.expansionId].settlementEventConfig[id] = {
            "name" : id,
            "title": "",
            "description": "",
            "settlementEffects": {
                "endeavors" : []
            },
            expansion: action.expansionId,
        }

        return newState;
    } else if(action.type === SET_STORY_EVENT) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].storyEvents = {...newState[action.expansionId].storyEvents};
        newState[action.expansionId].storyEvents[action.id].title = action.text;
        return newState;
    } else if(action.type === ADD_STORY_EVENT) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].storyEvents = {...newState[action.expansionId].storyEvents};
        let id = Uuid.v4();
        newState[action.expansionId].storyEvents[id] = {
            "name" : id,
            "title": "",
        }
        return newState;
    } else if(action.type === DELETE_STORY_EVENT) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].storyEvents = {...newState[action.expansionId].storyEvents};
        delete newState[action.expansionId].storyEvents[action.id];
        return newState;
    } else if(action.type === SET_SHOWDOWN) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].showdowns = {...newState[action.expansionId].showdowns};
        newState[action.expansionId].showdowns[action.id].title = action.text;
        return newState;
    } else if(action.type === SET_SHOWDOWN_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].showdowns = {...newState[action.expansionId].showdowns};
        newState[action.expansionId].showdowns[action.id] = {...action.config};
        return newState;
    } else if(action.type === ADD_SHOWDOWN) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].showdowns = {...newState[action.expansionId].showdowns};
        let id = Uuid.v4();
        newState[action.expansionId].showdowns[id] = {
            "name" : id,
            "title": "",
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === DELETE_SHOWDOWN) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].showdowns = {...newState[action.expansionId].showdowns};
        delete newState[action.expansionId].showdowns[action.id];
        return newState;
    } else if(action.type === SET_NEMESIS) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].nemesisEncounters = {...newState[action.expansionId].nemesisEncounters};
        newState[action.expansionId].nemesisEncounters[action.id].title = action.text;
        return newState;
    } else if(action.type === ADD_NEMESIS) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].nemesisEncounters = {...newState[action.expansionId].nemesisEncounters};
        let id = Uuid.v4();
        newState[action.expansionId].nemesisEncounters[id] = {
            "name" : id,
            "title": "",
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === DELETE_NEMESIS) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].nemesisEncounters = {...newState[action.expansionId].nemesisEncounters};
        delete newState[action.expansionId].nemesisEncounters[action.id];
        return newState;
    } else if(action.type === SET_LY_EVENT_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].timeline = {...newState[action.expansionId].timeline};
        newState[action.expansionId].timeline[action.ly] = action.config;
        return newState;
    } else if(action.type === ADD_LY_TO_TIMELINE) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].timeline = {...newState[action.expansionId].timeline};
        newState[action.expansionId].timeline[action.ly] = {};
        return newState;
    } else if(action.type === DELETE_LY) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].timeline = {...newState[action.expansionId].timeline};
        delete newState[action.expansionId].timeline[action.ly];
        return newState;
    } else if(action.type === IMPORT_EXPANSIONS) {
        let newState = {...state};

        for(let i = 0; i < action.expansions.length; i++) {
            newState[action.expansions[i].id] = action.expansions[i];
        }

        setCustomExpansionConfigs(Object.values(newState));

        return newState;
    } else if(action.type === CREATE_EXPANSION) {
        let newState = {...state};
        let id = Uuid.v4();
        newState[id] = {
            id,
            name: action.title,
            description: action.description,
            version: action.version,
            releaseNote: '',
            innovations: [],
            innovationConfig: {},
            disorders: [],
            disorderConfig: {},
            fightingArts: [],
            fightingArtConfig: {},
            abilities: [],
            abilityConfig: {},
            severeInjuries: [],
            severeInjuryConfig: {},
            locations: [],
            locationConfig: {},
            resourceConfig: {},
            gearConfig: {},
            weaponProfs: [],
            weaponProfConfig: {},
            armorSets: [],
            armorSetConfig: {},
            settlementEvents: [],
            settlementEventConfig: {},
            showdowns: {},
            nemesisEncounters: {},
            storyEvents: {},
            timeline: {},
            milestones: {},
        };

        return newState;
    } else if(action.type === DELETE_EXPANSION) {
        let newState = {...state};

        delete newState[action.id];

        return newState;
    } else if(action.type === SET_CUSTOM_EXPANSION_NAME) {
        let newState = {...state};
        newState[action.id] = {...newState[action.id]};
        newState[action.id].name = action.name;
        return newState;
    } else if(action.type === SET_CUSTOM_EXPANSION_DESCRIPTION) {
        let newState = {...state};
        newState[action.id] = {...newState[action.id]};
        newState[action.id].description = action.description;
        return newState;
    } else if(action.type === SET_CUSTOM_EXPANSION_VERSION) {
        let newState = {...state};
        newState[action.id] = {...newState[action.id]};
        newState[action.id].version = action.version;
        return newState;
    } else if(action.type === SET_CUSTOM_EXPANSION_RELEASE_NOTE) {
        let newState = {...state};
        newState[action.id] = {...newState[action.id]};
        newState[action.id].releaseNote = action.releaseNote;
        return newState;
    } else if(action.type === SET_WEAPON_PROF_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].weaponProfConfig = {...newState[action.expansionId].weaponProfConfig};
        newState[action.expansionId].weaponProfConfig[action.id] = action.config;
        if(!newState[action.expansionId].weaponProfConfig[action.id].expansion) {
            newState[action.expansionId].weaponProfConfig[action.id].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_WEAPON_PROF) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].weaponProfs = [...newState[action.expansionId].weaponProfs];
        newState[action.expansionId].weaponProfConfig = {...newState[action.expansionId].weaponProfConfig};

        delete newState[action.expansionId].weaponProfConfig[action.id];
        newState[action.expansionId].weaponProfs.splice(newState[action.expansionId].weaponProfs.indexOf(action.id), 1);
        return newState;
    } else if(action.type === ADD_WEAPON_PROF) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].weaponProfs = [...newState[action.expansionId].weaponProfs];
        newState[action.expansionId].weaponProfConfig = {...newState[action.expansionId].weaponProfConfig};

        let id = Uuid.v4();
        newState[action.expansionId].weaponProfs.splice(0, 0, id);
        newState[action.expansionId].weaponProfConfig[id] = {
            name: id,
            title: "",
            ranks: {},
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === SET_ARMOR_SET_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].armorSetConfig = {...newState[action.expansionId].armorSetConfig};
        newState[action.expansionId].armorSetConfig[action.id] = action.config;
        if(!newState[action.expansionId].armorSetConfig[action.id].expansion) {
            newState[action.expansionId].armorSetConfig[action.id].expansion = action.expansionId;
        }
        return newState;
    } else if(action.type === DELETE_ARMOR_SET) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].armorSets = [...newState[action.expansionId].armorSets];
        newState[action.expansionId].armorSetConfig = {...newState[action.expansionId].armorSetConfig};

        delete newState[action.expansionId].armorSetConfig[action.id];
        newState[action.expansionId].armorSets.splice(newState[action.expansionId].armorSets.indexOf(action.id), 1);
        return newState;
    } else if(action.type === ADD_ARMOR_SET) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        if(!newState[action.expansionId].armorSets) {
            newState[action.expansionId].armorSets = [];
            newState[action.expansionId].armorSetConfig = {};
        } else {
            newState[action.expansionId].armorSets = [...newState[action.expansionId].armorSets];
            newState[action.expansionId].armorSetConfig = {...newState[action.expansionId].armorSetConfig};
        }

        let id = Uuid.v4();
        newState[action.expansionId].armorSets.splice(0, 0, id);
        newState[action.expansionId].armorSetConfig[id] = {
            name: id,
            title: "",
            description: "",
            armor: 1,
            expansion: action.expansionId,
        }
        return newState;
    } else if(action.type === SET_GEAR_CURSED) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].gearConfig = {...newState[action.expansionId].gearConfig};
        newState[action.expansionId].gearConfig[action.location] = {...newState[action.expansionId].gearConfig[action.location]};
        newState[action.expansionId].gearConfig[action.location][action.gearId] = {...newState[action.expansionId].gearConfig[action.location][action.gearId]};

        newState[action.expansionId].gearConfig[action.location][action.gearId].cursed = action.cursed;
        return newState;
    } else if(action.type === ADD_MILESTONE) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        if(!newState[action.expansionId].milestones) {
            newState[action.expansionId].milestones = {};
        } else {
            newState[action.expansionId].milestones = {...newState[action.expansionId].milestones};
        }

        let id = Uuid.v4();
        newState[action.expansionId].milestones[id] = {
            id,
            title: "",
            description: "",
            type: action.milestoneType,
            expansion: action.expansionId,
            conditions: {},
            effects: {},
            reward: "",
        }

        if(action.milestoneType === MILESTONE_TYPE_GLOBAL) {
            newState[action.expansionId].milestones[id].settlementConditions = {};
        }

        return newState;
    } else if(action.type === SET_MILESTONE_CONFIG) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].milestones = {...newState[action.expansionId].milestones};
        newState[action.expansionId].milestones[action.id] = action.config;
        return newState;
    } else if(action.type === DELETE_MILESTONE) {
        let newState = {...state};
        newState[action.expansionId] = {...newState[action.expansionId]};
        newState[action.expansionId].milestones = {...newState[action.expansionId].milestones};
        delete newState[action.expansionId].milestones[action.id];
        return newState;
    } else if(action.type === SCHEMA_VALIDATE) {
        let newState = {...state};

        if(CompareVersions(action.currentVersion, "1.7.0") === -1) {
            for(let id in newState) {
                newState[id].storyEvents = updateEventsTo17(newState[id].storyEvents);
                newState[id].showdowns = updateEventsTo17(newState[id].showdowns);
                newState[id].nemesisEncounters = updateEventsTo17(newState[id].nemesisEncounters);
            }
        }

        if(CompareVersions(action.currentVersion, "2.5.0") === -1) {
            for(let id in newState) {
                newState[id].fightingArtConfig = addExpansionIdToConfig(id, newState[id].fightingArtConfig);
                newState[id].disorderConfig = addExpansionIdToConfig(id, newState[id].disorderConfig);
                newState[id].innovationConfig = addExpansionIdToConfig(id, newState[id].innovationConfig);
                newState[id].locationConfig = addExpansionIdToConfig(id, newState[id].locationConfig);
                newState[id].settlementEventConfig = addExpansionIdToConfig(id, newState[id].settlementEventConfig);
            }
        }

        if(CompareVersions(action.currentVersion, "2.7.0") === -1) {
            for(let id in newState) {
                newState[id].weaponProfConfig = addExpansionIdToConfig(id, newState[id].weaponProfConfig);
                newState[id].gearConfig = addExpansionIdToStorageConfig(id, newState[id].gearConfig);
                newState[id].resourceConfig = addExpansionIdToStorageConfig(id, newState[id].resourceConfig);
                newState[id].abilityConfig = addExpansionIdToConfig(id, newState[id].abilityConfig);
            }
        }

        return newState;
    } else {
        return state;
    }
}

function addExpansionIdToStorageConfig(id, storage) {
    for(let location in storage) {
        storage[location] = addExpansionIdToConfig(id, storage[location]);
    }

    return storage;
}

function addExpansionIdToConfig(id, config) {
    for(let key in config) {
        config[key].expansion = id;
    }

    return config;
}

function updateEventsTo17(events) {
    let newEvents = {};
    if(Array.isArray(events)) {
        for(let i = 0; i < events.length; i++) {
            let eventId = Uuid.v4({random: getUuidSeedFromString(events[i])});
            newEvents[eventId] = {
                name: eventId,
                title: events[i]
            }
        }
    }

    return newEvents;
}

function removeRewardFromMilestones(milestones, type, id) {
    for(let milestoneId in milestones) {
        if(milestones[milestoneId].effects &&
            milestones[milestoneId].effects[type] &&
            milestones[milestoneId].effects[type].indexOf(id) !== -1
        ) {
            milestones[milestoneId].effects[type].splice(milestones[milestoneId].effects[type].indexOf(id), 1);
        }
    }

    return milestones;
}

function removeStorageRewardFromMilestones(milestones, type, location, id) {
    for(let milestoneId in milestones) {
        if(milestones[milestoneId].effects &&
            milestones[milestoneId].effects[type]
        ) {
            for(let i = 0; i < milestones[milestoneId].effects[type].length; i++) {
                if(milestones[milestoneId].effects[type][i].location === location && milestones[milestoneId].effects[type][i].id === id) {
                    milestones[milestoneId].effects[type].splice(i, 1);
                    break;
                }
            }
        }
    }

    return milestones;
}
