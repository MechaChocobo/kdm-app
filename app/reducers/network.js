import Toast from 'react-native-simple-toast';

import {
    SERVER_START, SERVER_STOP,
    SETUP_SOCKET,
    SEND_SERVER_PACKET, SEND_CLIENT_PACKET,
    CLIENT_START, CLIENT_STOP,
    SHARE_ID,
    NETWORK_LOCK, NETWORK_UNLOCK, LOADING_UP, LOADING_DOWN, SET_RECONNECT_FUNC, RECONNECT_LOCKOUT, ERROR_CONTROLS_OVERRIDE, SET_HOST_IP_ADDRESS
} from '../actions/network';
import {RESET} from "../actions/global";
import SimpleToast from 'react-native-simple-toast';

let networkState = {
    server: null,
    serverSocket: {},
    client: null,
    myIpAddress: null,
    hostIpAddress: null,
    shareId: null,
    networkLock: false,
    loading: 0,
    clientError: "",
    reconnectFunc: null,
    reconnectLockout: false,
    clientConnected: false,
    errorControlsOverride: false,
};

export const networkReducer = (state = networkState, action) => {
    if(action.type == SERVER_START) {
        return {...state, server: action.server};
    } else if(action.type == SERVER_STOP) {
        if(state.server) {
            state.server.close();
        }
        return {...state, server: null, serverSocket: {}, shareId: null};
    } else if(action.type == SETUP_SOCKET) {
        let newState = {...state};
        const address = action.socket.address().address;

        if(newState.serverSocket[address]) {
            newState.serverSocket[address].end();
        }

        newState.serverSocket[address] = action.socket;
        return newState;
    } else if(action.type == SEND_SERVER_PACKET) {
        let newState = {...state};
        if(typeof action.data !== "string") {
            action.data = JSON.stringify(action.data);
        }

        if(Object.keys(newState.serverSocket).length > 0) {
            for(let address in newState.serverSocket) {
                if(newState.serverSocket[address]._state === 0) { // disconnected state
                    delete newState.serverSocket[address];
                    continue;
                }

                newState.serverSocket[address].write(action.data);
            }
        }

        return newState;
    } else if(action.type == SEND_CLIENT_PACKET) {
        if(state.client) {
            if(typeof action.data !== "string") {
                action.data = JSON.stringify(action.data);
            }
            try {
                state.client.write(action.data);
            } catch(e) {
                Toast.show("Network error, attempting reconnect");
                state.client.end();
                setTimeout(() => {
                    state.reconnectFunc(false);
                }, 0);
                return {...state, client: null, shareId: null, clientError: "Network error"}
            }
        }
        return state;
    } else if(action.type == CLIENT_START) {
        return {...state, client: action.client, hostIpAddress: action.remoteIp, clientConnected: true, clientError: ''};
    } else if(action.type == CLIENT_STOP) {
        if(state.client) {
            state.client.end();
        }
        if(action.error) {
            SimpleToast.show(action.error, Toast.LONG);
        }
        return {...state, client: null, shareId: null, clientError: (action.error ? action.error : ""), clientConnected: false, errorControlsOverride: false};
    } else if(action.type == SHARE_ID) {
        return {...state, shareId: action.shareId};
    } else if(action.type == NETWORK_LOCK) {
        return {...state, networkLock: true};
    } else if(action.type == NETWORK_UNLOCK) {
        return {...state, networkLock: false};
    } else if(action.type == LOADING_UP) {
        return {...state, loading: state.loading + 1};
    } else if(action.type == LOADING_DOWN) {
        return {...state, loading: state.loading - 1};
    } else if(action.type == SET_RECONNECT_FUNC) {
        return {...state, reconnectFunc: action.func};
    } else if(action.type == RECONNECT_LOCKOUT) {
        return {...state, reconnectLockout: action.val};
    } else if(action.type == ERROR_CONTROLS_OVERRIDE) {
        return {...state, errorControlsOverride: action.val};
    } else if(action.type == SET_HOST_IP_ADDRESS) {
        return {...state, hostIpAddress: action.val};
    } else if(action.type == RESET) {
        if(state.server) {
            state.server.close();
        }

        if(state.client) {
            state.client.end();
        }

        return {...networkState};
    } else {
        return state;
    }
}
