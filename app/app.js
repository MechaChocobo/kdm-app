import React, { Component } from 'react';
import { PersistGate } from 'redux-persist/es/integration/react';
import { Provider } from 'react-redux';
import { Client, Configuration } from 'bugsnag-react-native';

import Base from './components/base';
import configureStore from './store';
const { persistor, store } = configureStore();

const onBeforeLift = () => {
    // take some action before the gate lifts
}
const version = require('../package.json').version;
// hooks up bugsnag
const config = new Configuration('d64071e5cee71975a33f9ca2fe0e5ddd');
config.appVersion = version;

if(!__DEV__) {
    const bugsnag = new Client(config);
}

export default class Root extends Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate persistor={persistor} onBeforeLift={onBeforeLift}>
                    {bootstrapped => <Base bootstrapped={bootstrapped} />}
                </PersistGate>
            </Provider>
        );
    }
}
