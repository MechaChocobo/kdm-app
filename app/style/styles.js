import {StyleSheet, Platform} from 'react-native';

const fontFamily = Platform.select({
    ios: "Helvetica",
    android: ""
});

export const OVERLAY_ZINDEX = 10000;

let styles = StyleSheet.create({
    container: {
        flex:1,
    },
    wrapper: {
        marginLeft: "2%",
        marginRight: "2%",
        marginTop: "2%",
        padding: 5,
    },
    centeredWrapper: {
        width: "90%",
        alignSelf: "center",
    },
    title : {
        fontSize: 22,
        fontWeight: "bold",
        paddingLeft: 5
    },
    row : {
        flexDirection: "row"
    },
    itemText: {
        fontSize: 16,
        fontWeight: "300"
    },
    textHeavy: {
        fontSize: 18,
        fontWeight: "600"
    },
    textLineHeight: {
        fontSize: 16,
        fontWeight: "300",
        lineHeight: 21
    },
    itemRowBorder : {
        flexDirection: "row",
        margin: 8,
        alignItems: "center",
        borderBottomWidth: 1,
    },
    itemRow : {
        flexDirection: "row",
        margin: 8,
        flexWrap: "wrap"
    },
    value: {
        fontSize: 20,
        fontWeight: "500",
        marginRight: 5,
        marginLeft: 5
    },
    largeValue: {
        fontSize: 26,
        fontWeight: "500",
    },
    smallValue: {
        fontSize: 20,
        fontWeight: "300",
    },
    incrementorWrapper: {
        flex: 1,
        flexDirection: "row",
    },
    header: {
        fontSize: 18,
        fontWeight: "400"
    },
    headerWrapper: {
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 8
    },
    pageHeader: {
        fontSize: 30,
        textAlign: "center",
    },
    listRow:{
        padding: 10,
    },
    listRowWrapper: {
        marginLeft: 20,
        marginRight: 20,
    },
    listItemDescription:{
        marginTop: 5,
        fontSize: 14,
    },
    textHeader: {
        fontWeight: "bold",
        fontSize: 20,
    },
    wrappedTextItemTitle: {
        fontWeight: "bold",
        fontSize: 16
    },
    bold: {
        fontWeight: "bold",
    },
    hyperlink: {
        textDecorationLine: "underline"
    },
    hr: {
        borderBottomWidth: 1,
        width: "100%",
        marginTop: 10,
        marginBottom: 10,
        alignSelf: "center",
        borderColor: "rgba(256, 256, 256, 0.5)",
    },
    hrSlim: {
        borderColor: "rgba(256, 256, 256, 0.5)",
        width: "100%",
        borderBottomWidth: 1,
        marginTop: 2,
        marginBottom: 2,
        alignSelf: "center"
    },
    darkBorder50: {
        borderColor: "rgba(0, 0, 0, 0.5)",
    },
    lightBorder50: {
        borderColor: "rgba(256, 256, 256, 0.5)",
    },
    width100: {
        width: "100%",
    },
    width90: {
        width: "90%",
    },
    width95: {
        width: "95%",
    },
    scrollableTabText: {
        color: "white"
    },
    rowCentered: {
        flexDirection: "row",
        alignItems: "center"
    },
    swipeoutButton: {
        height: "100%",
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingLeft: 5,
    },
    italic: {
        fontStyle: "italic"
    },
    centered: {
        alignSelf: "center"
    },
    centeredItems: {
        alignItems: "center"
    },
    disabled: {
        opacity: .3
    },
    lightDisabled: {
        opacity: .5
    },
    roundedContainer: {
        borderRadius: 5,
        overflow: "hidden",
    },
    whiteOpaqueBackground: {
        backgroundColor: "rgba(256, 256, 256, 0.2)"
    },
    blackOpaqueBackground: {
        backgroundColor: "rgba(0, 0, 0, 0.2)"
    },
    darkBlackOpaqueBackground: {
        backgroundColor: "rgba(0, 0, 0, 0.8)"
    },
    rightAddControl: {
        position: 'absolute',
        zIndex: 1001,
        top: 50,
        right: '50%',
        marginRight: -35,
    },
    leftAddControl: {
        position: 'absolute',
        zIndex: 1001,
        top: 50,
        left: '50%',
        marginLeft: -35,
    },
    valueFontSize: {
        fontSize: 35,
    },
    headerFontSize: {
        fontSize: 24,
    },
    spaceBetween: {
        justifyContent: "space-between",
    },
    spaceEvenly: {
        justifyContent: "space-evenly",
    },
    justifyCentered: {
        justifyContent: "center",
    },
    icomoon: {
        fontFamily: "icomoon",
    },
    darkOverlay: {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        // zIndex: OVERLAY_ZINDEX,
        position: "absolute",
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
    },
    aboveOverlay: {
        zIndex: OVERLAY_ZINDEX + 1,
    },
    selfFlexStart: {
        alignSelf: 'flex-start'
    },
    flex50: {
        flex: .5
    },
    flex25: {
        flex: .25
    },
    flex33: {
        flex: .33
    },
    flex40: {
        flex: .40
    },
    flex67: {
        flex: .67
    },
    width50: {
        width: "50%"
    },
    width60: {
        width: "60%"
    },
    width40: {
        width: "40%"
    },
    marginBottom20: {
        marginBottom: 20
    },
    fontSize20: {
        fontSize: 20
    },
    flexShrink: {
        flexShrink: 1,
    },
    tabText: {
        fontFamily,
    },
    empty: {},
});

export default styles;
