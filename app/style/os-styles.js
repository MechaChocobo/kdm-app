import {
    Platform
} from 'react-native';

let pickerStyle = Platform.select({
    ios:  {marginTop: 10, marginBottom: 8},
    android: {}
});

let pickerTextStyle = Platform.select({
    ios:  {color:"white", fontSize: 16, padding: 10},
    android: {color: "white"}
});

exports.pickerStyle = pickerStyle;
exports.pickerTextStyle = pickerTextStyle;
