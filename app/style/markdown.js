export default markdownStyle = {
    paragraph: {
        fontSize: 16, fontWeight: "300", marginTop: 0, marginBottom: 0
    },
    listItemUnorderedContent: {
        fontSize: 16, fontWeight: "300", marginTop: 0, marginBottom: 0
    },
    listItemBullet: {},
    heading2: {
        marginBottom: 0
    },
    heading3: {
        marginBottom: 0
    }
}
