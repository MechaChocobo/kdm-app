let colors = {
    "GREEN": "#33fd28",
    "ORANGE": "#ff8000",
    "RED": "#da1010",
    "BROWN": "#714f3e",
    "GRAY": "#808080",
    "GREENDARK": "#019617",
    "BLUE": "#0c85f2",
    "PURPLE" : "#6d2c9a",
    "PINK" : "#f374d4",
    "WHITE" : "#ffffff",
    "YELLOW" : "#ffff00",
    "TEAL" : "#008080",
    "GOLD" : "#fdb400",
}

export default colors;
