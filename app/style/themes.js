let themes = {
    dark : {
        TEXT: "#ffffff",
        MENU: "#f6f6f6",
        GRADIENT_START: "#f68f54",
        GRADIENT_END: "#f374d4",
        GRADIENT_START_BANNER: "#f68f54",
        GRADIENT_END_BANNER: "#f374d4",
        GRADIENT_START_BACKGROUND: "#0d192d",
        GRADIENT_END_BACKGROUND: "#323d61",
        HIGHLIGHT: "#f7924b",
        INVERT_TEXT: "#000000",
        NAV_BAR_BACKGROUND: "#ffffff",
        NAV_BAR_ICON: "#0d192d",
        NAV_BAR_TEXT: "#000000",
        isDark: true,
    },
    light: {
        TEXT: "#000000",
        MENU: "#333e62",
        GRADIENT_START: "#f68f54",
        GRADIENT_END: "#f374d4",
        GRADIENT_START_BANNER: "#0d192d",
        GRADIENT_END_BANNER: "#323d61",
        GRADIENT_START_BACKGROUND: "#ffffff",
        GRADIENT_END_BACKGROUND: "#ffffff",
        HIGHLIGHT: "#f7924b",
        INVERT_TEXT: "#ffffff",
        NAV_BAR_BACKGROUND: "#ffffff",
        NAV_BAR_ICON: "#0d192d",
        NAV_BAR_TEXT: "#000000",
        isDark: false,
    }
}

export default themes;
