export const SET_SHOULD_SHOW_ADS = "SET_SHOULD_SHOW_ADS";
export const SET_IS_SUBSCRIBER = "SET_IS_SUBSCRIBER";
export const SET_PURCHASED_ITEMS = "SET_PURCHASED_ITEMS";
export const ADD_PURCHASED_ITEM = "ADD_PURCHASED_ITEM";

export function setShouldShowAds(shouldShow) {
    return (dispatch, getState) => {
        dispatch({type: SET_SHOULD_SHOW_ADS, shouldShow});
    }
}

export function setIsSubscriber(isSubscriber) {
    return (dispatch, getState) => {
        dispatch({type: SET_IS_SUBSCRIBER, isSubscriber});
    }
}

export function setPurchasedItems(items) {
    return (dispatch, getState) => {
        dispatch({type: SET_PURCHASED_ITEMS, items});
    }
}

export function addPurchasedItem(item) {
    return (dispatch, getState) => {
        dispatch({type: ADD_PURCHASED_ITEM, item});
    }
}
