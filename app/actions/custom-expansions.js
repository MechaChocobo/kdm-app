import compareVersions from "compare-versions";

import { removeInnovation, removeLocation, deleteResource, deleteGear, deleteAllFightingArt, deleteAllDisorder, deleteAllAbility, deleteAllSevereInjury, deleteAllStoryEvent, deleteAllShowdown, deleteAllNemesisEncounters, deleteAllSettlementEvent, removeCustomExpansionsFromSettlement, deleteAllWeaponProficiency, deleteCursedGear, addCursedGear, deleteAllArmorSet, deleteSettlementMilestone } from "./settlement";
import { getSettlementsWithCustomExpansion, isObjectEmpty } from "../helpers/helpers";
import { t } from '../helpers/intl';
import { networkBlock } from './network';
import { setIntegrityCheck } from "./global";
import { MILESTONE_TYPE_GLOBAL, MILESTONE_TYPE_SETTLEMENT } from "../reducers/custom-expansions";

const packageInfo = require('../../package.json');

export const SET_INNOVATION_CONFIG = "SET_INNOVATION_CONFIG";
export const ADD_INNOVATION = "ADD_INNOVATION";
export const DELETE_INNOVATION = "DELETE_INNOVATION";
export const SET_LOCATION_CONFIG = "SET_LOCATION_CONFIG";
export const ADD_LOCATION = "ADD_LOCATION";
export const DELETE_LOCATION = "DELETE_LOCATION";
export const SET_RESOURCE_CONFIG = "SET_RESOURCE_CONFIG";
export const DELETE_SINGLE_RESOURCE = "DELETE_SINGLE_RESOURCE";
export const ADD_RESOURCE = "ADD_RESOURCE";
export const DELETE_RESOURCE_SOURCE = "DELETE_RESOURCE_SOURCE";
export const ADD_RESOURCE_SOURCE = "ADD_RESOURCE_SOURCE";
export const SET_GEAR_CONFIG = "SET_GEAR_CONFIG";
export const DELETE_SINGLE_GEAR = "DELETE_SINGLE_GEAR";
export const ADD_GEAR = "ADD_GEAR";
export const DELETE_GEAR_LOCATION = "DELETE_GEAR_LOCATION";
export const ADD_GEAR_LOCATION = "ADD_GEAR_LOCATION";
export const SET_FA_CONFIG = "SET_FA_CONFIG";
export const DELETE_FA = "DELETE_FA";
export const ADD_FA = "ADD_FA";
export const SET_DISORDER_CONFIG = "SET_DISORDER_CONFIG";
export const DELETE_DISORDER = "DELETE_DISORDER";
export const ADD_DISORDER = "ADD_DISORDER";
export const SET_ABILITY_CONFIG = "SET_ABILITY_CONFIG";
export const DELETE_ABILITY = "DELETE_ABILITY";
export const ADD_ABILITY = "ADD_ABILITY";
export const SET_INJURY_CONFIG = "SET_INJURY_CONFIG";
export const DELETE_INJURY = "DELETE_INJURY";
export const ADD_INJURY = "ADD_INJURY";
export const SET_SETTLEMENT_EVENT_CONFIG = "SET_SETTLEMENT_EVENT_CONFIG";
export const ADD_SETTLEMENT_EVENT = "ADD_SETTLEMENT_EVENT";
export const DELETE_SETTLEMENT_EVENT = "DELETE_SETTLEMENT_EVENT";
export const SET_STORY_EVENT = "SET_STORY_EVENT";
export const ADD_STORY_EVENT = "ADD_STORY_EVENT";
export const DELETE_STORY_EVENT = "DELETE_STORY_EVENT";
export const SET_SHOWDOWN = "SET_SHOWDOWN";
export const ADD_SHOWDOWN = "ADD_SHOWDOWN";
export const DELETE_SHOWDOWN = "DELETE_SHOWDOWN";
export const SET_NEMESIS = "SET_NEMESIS";
export const ADD_NEMESIS = "ADD_NEMESIS";
export const DELETE_NEMESIS = "DELETE_NEMESIS";
export const SET_LY_EVENT_CONFIG = "SET_LY_EVENT_CONFIG";
export const ADD_LY_TO_TIMELINE = "ADD_LY_TO_TIMELINE";
export const DELETE_LY = "DELETE_LY";
export const CREATE_EXPANSION = "CREATE_EXPANSION";
export const IMPORT_EXPANSIONS = "IMPORT_EXPANSIONS";
export const VALIDATE_CE_SCHEMA = "VALIDATE_CE_SCHEMA";
export const DELETE_EXPANSION = "DELETE_EXPANSION";
export const SET_CUSTOM_EXPANSION_NAME = "SET_CUSTOM_EXPANSION_NAME";
export const SET_CUSTOM_EXPANSION_DESCRIPTION = "SET_CUSTOM_EXPANSION_DESCRIPTION";
export const SET_CUSTOM_EXPANSION_VERSION = "SET_CUSTOM_EXPANSION_VERSION";
export const SET_CUSTOM_EXPANSION_RELEASE_NOTE = "SET_CUSTOM_EXPANSION_RELEASE_NOTE";
export const ADD_WEAPON_PROF = "ADD_WEAPON_PROF";
export const SET_WEAPON_PROF_CONFIG = "SET_WEAPON_PROF_CONFIG";
export const DELETE_WEAPON_PROF = "DELETE_WEAPON_PROF";
export const SET_GEAR_CURSED = "SET_GEAR_CURSED";
export const ADD_ARMOR_SET = "ADD_ARMOR_SET";
export const SET_ARMOR_SET_CONFIG = "SET_ARMOR_SET_CONFIG";
export const DELETE_ARMOR_SET = "DELETE_ARMOR_SET";
export const ADD_MILESTONE = "ADD_MILESTONE";
export const SET_MILESTONE_CONFIG = "SET_MILESTONE_CONFIG";
export const DELETE_MILESTONE = "DELETE_MILESTONE";
export const APPLY_GLOBAL_MILESTONE = "APPLY_GLOBAL_MILESTONE";
export const DELETE_ALL_GLOBAL_MILESTONE_CONDITION = "DELETE_ALL_GLOBAL_MILESTONE_CONDITION";
export const DELETE_ALL_GLOBAL_SETTLEMENT_MILESTONE_CONDITION = "DELETE_ALL_GLOBAL_SETTLEMENT_MILESTONE_CONDITION";
export const DELETE_ALL_SETTLEMENT_MILESTONE_CONDITION = "DELETE_ALL_SETTLEMENT_MILESTONE_CONDITION";
export const SET_SHOWDOWN_CONFIG = "SET_SHOWDOWN_CONFIG";

export function setInnovationConfig(expansionId, innovationId, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_INNOVATION_CONFIG, expansionId, innovationId, config});
    }
}

export function setLocationConfig(expansionId, locationId, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_LOCATION_CONFIG, expansionId, locationId, config});
    }
}

export function addInnovation(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_INNOVATION, expansionId});
    }
}

export function deleteInnovation(expansionId, innovationId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);

        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allInnovations.indexOf(innovationId) !== -1) {
                dispatch(removeInnovation(settlements[i].id, innovationId, true));
            }
        }

        dispatch({type: DELETE_INNOVATION, expansionId, innovationId});
        dispatch(setIntegrityCheck(true));
    }
}

export function addLocation(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_LOCATION, expansionId});
    }
}

export function deleteLocation(expansionId, locationId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);

        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allLocations.indexOf(locationId) !== -1) {
                dispatch(removeLocation(settlements[i].id, locationId, true));
            }
        }

        dispatch({type: DELETE_LOCATION, expansionId, locationId});
        dispatch(setIntegrityCheck(true));
    }
}

export function setResourceConfig(expansionId, source, resourceId, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_RESOURCE_CONFIG, expansionId, source, resourceId, config});
    }
}

export function deleteSingleResource(expansionId, source, resourceId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(resourceId in settlements[i].storage.resources[source]) {
                dispatch(deleteResource(settlements[i].id, source, resourceId));
            }
        }

        dispatch({type: DELETE_SINGLE_RESOURCE, expansionId, source, resourceId});
        dispatch(setIntegrityCheck(true));
    }
}

export function deleteResourceSource(expansionId, source) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        let resources = Object.keys(state.customExpansions[expansionId].resourceConfig[source]);
        for(let i = 0; i < settlements.length; i++) {
            for(let j = 0; j < resources.length; j++) {
                if(resources[j] in settlements[i].storage.resources[source]) {
                    dispatch(deleteResource(settlements[i].id, source, resources[j]));
                }
            }
        }

        dispatch({type: DELETE_RESOURCE_SOURCE, expansionId, source});
        dispatch(setIntegrityCheck(true));
    }
}

export function addResource(expansionId, source) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_RESOURCE, expansionId, source});
    }
}

export function addResourceSource(expansionId, source) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_RESOURCE_SOURCE, expansionId, source});
    }
}

export function setGearConfig(expansionId, location, gearId, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_GEAR_CONFIG, expansionId, location, gearId, config});
    }
}

export function setGearCursed(expansionId, location, gearId, cursed) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);

        if(cursed === false) {
            for(let i = 0; i < settlements.length; i++) {
                let setCursed = settlements[i].allCursedItems;

                for(let j = 0; j < setCursed.length; j++) {
                    if(setCursed[j].location === location && setCursed[j].item === gearId) {
                        // also remove from survivors who have this added
                        dispatch(deleteCursedGear(settlements[i].id, location, gearId));
                        break;
                    }
                }
            }
        }

        dispatch({type: SET_GEAR_CURSED, expansionId, location, gearId, cursed});
    }
}

export function deleteSingleGear(expansionId, location, gearId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);

        for(let i = 0; i < settlements.length; i++) {
            if(gearId in settlements[i].storage.gear[location]) {
                dispatch(deleteGear(settlements[i].id, location, gearId));
            }

            let setCursed = settlements[i].allCursedItems;
            for(let j = 0; j < setCursed.length; j++) {
                if(setCursed[j].location === location && setCursed[j].item === gearId) {
                    // also remove from survivors who have this added
                    dispatch(deleteCursedGear(settlements[i].id, location, gearId));
                    break;
                }
            }
        }

        dispatch({type: DELETE_SINGLE_GEAR, expansionId, location, gearId});
        dispatch(setIntegrityCheck(true));
    }
}

export function addGear(expansionId, location) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_GEAR, expansionId, location});
    }
}

export function addGearLocation(expansionId, location) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_GEAR_LOCATION, expansionId, location});
    }
}

export function deleteGearLocation(expansionId, location) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        let gear = Object.keys(state.customExpansions[expansionId].gearConfig[location]);
        for(let i = 0; i < settlements.length; i++) {
            for(let j = 0; j < gear.length; j++) {
                if(settlements[i].storage.gear[location] && gear[j] in settlements[i].storage.gear[location]) {
                    dispatch(deleteGear(settlements[i].id, location, gear[j]));
                }

                let setCursed = settlements[i].allCursedItems;
                for(let k = 0; k < setCursed.length; k++) {
                    if(setCursed[k].location === location && setCursed[k].item === gear[j]) {
                        // also remove from survivors who have this added
                        dispatch(deleteCursedGear(settlements[i].id, location, gear[j]));
                        break;
                    }
                }
            }
        }

        dispatch({type: DELETE_GEAR_LOCATION, expansionId, location});
        dispatch(setIntegrityCheck(true));
    }
}

export function setFAConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_FA_CONFIG, expansionId, id, config});
    }
}

export function deleteFA(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allFightingArts.indexOf(id) !== -1) {
                dispatch(deleteAllFightingArt(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_FA, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function addFA(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_FA, expansionId});
    }
}

export function setDisorderConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_DISORDER_CONFIG, expansionId, id, config});
    }
}

export function deleteDisorder(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allDisorders.indexOf(id) !== -1) {
                dispatch(deleteAllDisorder(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_DISORDER, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function addDisorder(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_DISORDER, expansionId});
    }
}

export function setAbilityConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_ABILITY_CONFIG, expansionId, id, config});
    }
}

export function deleteAbility(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allAbilities.indexOf(id) !== -1) {
                dispatch(deleteAllAbility(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_ABILITY, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function addAbility(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_ABILITY, expansionId});
    }
}

export function setSevereInjuryConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_INJURY_CONFIG, expansionId, id, config});
    }
}

export function deleteSevereInjury(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allSevereInjuries.indexOf(id) !== -1) {
                dispatch(deleteAllSevereInjury(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_INJURY, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function addSevereInjury(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_INJURY, expansionId});
    }
}

export function setSettlementEventConfig(expansionId, eventId, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_SETTLEMENT_EVENT_CONFIG, expansionId, eventId, config});
    }
}

export function addSettlementEvent(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_SETTLEMENT_EVENT, expansionId});
    }
}

export function deleteSettlementEvent(expansionId, eventId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allSettlementEvents.indexOf(eventId) !== -1) {
                dispatch(deleteAllSettlementEvent(settlements[i].id, eventId));
            }
        }
        dispatch({type: DELETE_SETTLEMENT_EVENT, expansionId, eventId});
        dispatch(setIntegrityCheck(true));
    }
}

export function setStoryEvent(expansionId, id, text) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_STORY_EVENT, expansionId, id, text});
    }
}

export function addStoryEvent(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_STORY_EVENT, expansionId});
    }
}

export function deleteStoryEvent(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allStoryEvents.indexOf(id) !== -1) {
                dispatch(deleteAllStoryEvent(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_STORY_EVENT, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function setShowdown(expansionId, id, text) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_SHOWDOWN, expansionId, id, text});
    }
}

export function setShowdownConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_SHOWDOWN_CONFIG, expansionId, id, config});
    }
}

export function addShowdown(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_SHOWDOWN, expansionId});
    }
}

export function deleteShowdown(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allShowdowns.indexOf(id) !== -1) {
                dispatch(deleteAllShowdown(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_SHOWDOWN, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function setNemesis(expansionId, id, text) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_NEMESIS, expansionId, id, text});
    }
}

export function addNemesis(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_NEMESIS, expansionId});
    }
}

export function deleteNemesis(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allNemesisEncounters.indexOf(id) !== -1) {
                dispatch(deleteAllNemesisEncounters(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_NEMESIS, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function setLyEventConfig(expansionId, ly, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_LY_EVENT_CONFIG, expansionId, ly, config});
    }
}

export function addLyToTimeline(expansionId, ly) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_LY_TO_TIMELINE, expansionId, ly});
    }
}

export function deleteLyFromTimeline(expansionId, ly) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: DELETE_LY, expansionId, ly});
    }
}

export function createExpansion(title, description, version) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: CREATE_EXPANSION, title, description, version});
    }
}

export function importCustomExpansions(expansions) {
    return (dispatch, getState) => {
        if(!Array.isArray(expansions)) {
            expansions = [expansions];
        }

        dispatch({type: IMPORT_EXPANSIONS, expansions});
        dispatch(setIntegrityCheck(true));
    }
}

export function validateCESchema(currentVersion) {
    return (dispatch, getState) => {
        dispatch({type: VALIDATE_CE_SCHEMA, currentVersion});
    }
}

export function deleteExpansion(id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, id);
        for(let i = 0; i < settlements.length; i++) {
            dispatch(removeCustomExpansionsFromSettlement(settlements[i].id, [id]));
        }
        if(state.customExpansions[id].milestones) {
            for(let milestoneId in state.customExpansions[id].milestones) {
                dispatch(deleteMilestone(id, milestoneId));
            }
        }
        dispatch({type: DELETE_EXPANSION, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function setName(id, name) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_CUSTOM_EXPANSION_NAME, id, name});
    }
}

export function setDescription(id, description) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_CUSTOM_EXPANSION_DESCRIPTION, id, description});
    }
}

export function setVersion(id, version) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_CUSTOM_EXPANSION_VERSION, id, version});
    }
}

export function setReleaseNote(id, releaseNote) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_CUSTOM_EXPANSION_RELEASE_NOTE, id, releaseNote});
    }
}

export function setWeaponProfConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_WEAPON_PROF_CONFIG, expansionId, id, config});
    }
}

export function deleteWeaponProf(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allProficiencies.indexOf(id) !== -1) {
                dispatch(deleteAllWeaponProficiency(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_WEAPON_PROF, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function addWeaponProf(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_WEAPON_PROF, expansionId});
    }
}

export function setArmorSetConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: SET_ARMOR_SET_CONFIG, expansionId, id, config});
    }
}

export function deleteArmorSet(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            if(settlements[i].allArmorSets.indexOf(id) !== -1) {
                dispatch(deleteAllArmorSet(settlements[i].id, id));
            }
        }
        dispatch({type: DELETE_ARMOR_SET, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function addArmorSet(expansionId) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_ARMOR_SET, expansionId});
    }
}

export function addMilestone(expansionId, milestoneType) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: ADD_MILESTONE, expansionId, milestoneType});
    }
}

export function setMilestoneConfig(expansionId, id, config) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        let state = getState();
        let current = state.customExpansions[expansionId].milestones[id];

        if(config.type === MILESTONE_TYPE_GLOBAL) {
            if(!isObjectEmpty(current.conditions)) {
                for(let conditionId in current.conditions) {
                    if(!config.conditions[conditionId]) {
                        dispatch({type: DELETE_ALL_GLOBAL_MILESTONE_CONDITION, id, conditionId});
                    }
                }
            }

            if(!isObjectEmpty(current.settlementConditions)) {
                for(let conditionId in current.settlementConditions) {
                    if(!config.settlementConditions[conditionId]) {
                        dispatch({type: DELETE_ALL_GLOBAL_SETTLEMENT_MILESTONE_CONDITION, id, conditionId});
                    }
                }
            }
        } else if(config.type === MILESTONE_TYPE_SETTLEMENT) {
            if(!isObjectEmpty(current.conditions)) {
                for(let conditionId in current.conditions) {
                    if(!config.conditions[conditionId]) {
                        dispatch({type: DELETE_ALL_SETTLEMENT_MILESTONE_CONDITION, id, conditionId});
                    }
                }
            }
        }

        dispatch({type: SET_MILESTONE_CONFIG, expansionId, id, config});
    }
}

export function deleteMilestone(expansionId, id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot edit an expansion while networked"))(dispatch, getState)) return;
        const state = getState();
        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansionId);
        for(let i = 0; i < settlements.length; i++) {
            dispatch(deleteSettlementMilestone(settlements[i].id, id));
        }
        dispatch({type: DELETE_MILESTONE, expansionId, id});
        dispatch(setIntegrityCheck(true));
    }
}

export function applyGlobalMilestones() {
    return (dispatch, getState) => {
        const state = getState();
        for(let id in state.customExpansions) {
            if(state.customExpansions[id].milestones) {
                for(let milestoneId in state.customExpansions[id].milestones) {
                    if(
                        state.customExpansions[id].milestones[milestoneId].type === MILESTONE_TYPE_GLOBAL
                    ) {
                        let settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, id);
                        let settlementIds = [];
                        for(let i = 0; i < settlements.length; i++) {
                            settlementIds.push(settlements[i].id);
                        }
                        dispatch({type: APPLY_GLOBAL_MILESTONE, milestone: state.customExpansions[id].milestones[milestoneId], settlementIds});
                    }
                }
            }
        }
    }
}

export function importJsonExpansion(jsonData, source = null) {
    return (dispatch, getState) => {
        let expansion = {};
        try {
            expansion = JSON.parse(jsonData);
        } catch(e) {
            return t("Unable to process downloaded expansion");
        }

        if(!expansion.expansion || !expansion.version) {
            return t("The downloaded file does not appear to contain a valid expansion");
        }

        if(expansion.version && compareVersions(expansion.version, packageInfo.version) === 1) {
            return t("Trying to import an expansion from a newer version of Scribe (%version%), please update to the latest version.", {version: data.version});
        }

        if(source) {
            expansion.expansion.source = source;
        }

        dispatch(importCustomExpansions(expansion.expansion));
        return true;
    }
}
