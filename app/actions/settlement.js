import Resources from '../config/resources-expansions.json';
import Gear from '../config/gear-expansions.json';
import Locations from '../config/locations-expansions.json';
import Innovations from '../config/innovations-expansions.json';
import FightingArts from '../config/fighting-arts-expansions.json';
import Disorders from '../config/disorders-expansions.json';
import Abilities from '../config/abilities-expansions.json';
import Proficiencies from '../config/weapon-profs-expansions.json';
import SettlementEvents from '../config/settlement-events-expansions.json';
import StoryEvents from '../config/story-events-expansions.json';
import NemesisEncounters from '../config/nemesis-expansions.json';
import Showdowns from '../config/showdowns-expansions.json';
import Armor from '../config/armor-expansions.json';

export const DATA_AVAILABLE = 'DATA_AVAILABLE';
export const NAME_CHANGE = "NAME_CHANGE";
export const SURVIVAL_INCREASE = "SURVIVAL_INCREASE";
export const SURVIVAL_DECREASE = "SURVIVAL_DECREASE";
export const POPULATION_INCREASE = "POPULATION_INCREASE";
export const POPULATION_DECREASE = "POPULATION_DECREASE";
export const DEATHCOUNT_INCREASE = "DEATHCOUNT_INCREASE";
export const DEATHCOUNT_DECREASE = "DEATHCOUNT_DECREASE";
export const ENDEAVOR_INCREASE = "ENDEAVOR_INCREASE";
export const ENDEAVOR_DECREASE = "ENDEAVOR_DECREASE";
export const ENDEAVOR_SET = "ENDEAVOR_SET";
export const INSPIRATIONAL_STATUE_SET = "INSPIRATIONAL_STATUE_SET";
export const LOCATION_ADD = "LOCATION_ADD";
export const LOCATION_REMOVE = "LOCATION_REMOVE";
export const INNOVATION_ADD = "INNOVATION_ADD";
export const INNOVATION_REMOVE = "INNOVATION_REMOVE";
export const INNOVATION_DECK_ADD = "INNOVATION_DECK_ADD";
export const INNOVATION_DECK_REMOVE = "INNOVATION_DECK_REMOVE";
export const PRINCIPLE_SET = "PRINCIPLE_SET";
export const PRINCIPLE_UNSET = "PRINCIPLE_UNSET";
export const RESOURCE_INCREASE = "RESOURCE_INCREASE";
export const RESOURCE_DECREASE = "RESOURCE_DECREASE";
export const RESOURCE_DELETE = "RESOURCE_DELETE";
export const GEAR_INCREASE = "GEAR_INCREASE";
export const GEAR_DECREASE = "GEAR_DECREASE";
export const GEAR_DELETE = "GEAR_DELETE";
export const EDIT_NOTE_SETTLEMENT = "EDIT_NOTE_SETTLEMENT";
export const END_YEAR = "END_YEAR";
export const UNEND_YEAR = "UNEND_YEAR";
export const CREATE_SETTLEMENT = "CREATE_SETTLEMENT";
export const COPY_SETTLEMENT = "COPY_SETTLEMENT";
export const IMPORT_SETTLEMENT = "IMPORT_SETTLEMENT";
export const DELETE_SETTLEMENT = "DELETE_SETTLEMENT";
export const TOGGLE_ARCHIVE = "TOGGLE_ARCHIVE";
export const ADD_EXPANSIONS = "ADD_EXPANSIONS";
export const SET_SURVIVOR_SORT_ATTR = "SET_SURVIVOR_SORT_ATTR";
export const SET_SURVIVOR_SORT_ORDER = "SET_SURVIVOR_SORT_ORDER";
export const DELETE_ALL_FIGHTING_ART = "DELETE_ALL_FIGHTING_ART";
export const DELETE_ALL_DISORDER = "DELETE_ALL_DISORDER";
export const DELETE_ALL_ABILITY = "DELETE_ALL_ABILITY";
export const DELETE_ALL_WEAPON_PROFICIENCY = "DELETE_ALL_WEAPON_PROFICIENCY";
export const DELETE_ALL_SEVERE_INJURY = "DELETE_ALL_SEVERE_INJURY";
export const DELETE_ALL_STORY_EVENT = "DELETE_ALL_STORY_EVENT";
export const DELETE_ALL_SHOWDOWN = "DELETE_ALL_SHOWDOWN";
export const DELETE_ALL_NEMESIS_ENCOUNTER = "DELETE_ALL_NEMESIS_ENCOUNTER";
export const DELETE_ALL_SETTLEMENT_EVENT = "DELETE_ALL_SETTLEMENT_EVENT";
export const ADD_CUSTOM_EXPANSION_TO_SETTLEMENT = "ADD_CUSTOM_EXPANSION_TO_SETTLEMENT";
export const REMOVE_CUSTOM_EXPANSION_FROM_SETTLEMENT = "REMOVE_CUSTOM_EXPANSION_FROM_SETTLEMENT";
export const DOWNGRADE_CUSTOM_EXPANSION_IN_SETTLEMENT = "DOWNGRADE_CUSTOM_EXPANSION_IN_SETTLEMENT";
export const GEAR_CURSED_DELETE = "GEAR_CURSED_DELETE";
export const GEAR_CURSED_ADD = "GEAR_CURSED_ADD";
export const SETTLEMENT_ADD_ACTION_LOG = "SETTLEMENT_ADD_ACTION_LOG";
export const CRAFT_GEAR = "CRAFT_GEAR";
export const SETTLEMENT_SET_MILESTONE_VALUE = "SETTLEMENT_SET_MILESTONE_VALUE";
export const SETTLEMENT_INNOVATION_ADD_ALL = "SETTLEMENT_INNOVATION_ADD_ALL";
export const SETTLEMENT_INNOVATION_REMOVE_ALL = "SETTLEMENT_INNOVATION_REMOVE_ALL";
export const SETTLEMENT_FIGHTING_ART_ADD_ALL = "SETTLEMENT_FIGHTING_ART_ADD_ALL";
export const SETTLEMENT_FIGHTING_ART_REMOVE_ALL = "SETTLEMENT_FIGHTING_ART_REMOVE_ALL";
export const SETTLEMENT_DISORDER_ADD_ALL = "SETTLEMENT_DISORDER_ADD_ALL";
export const SETTLEMENT_DISORDER_REMOVE_ALL = "SETTLEMENT_DISORDER_REMOVE_ALL";
export const SETTLEMENT_SE_EVENT_ADD_ALL = "SETTLEMENT_SE_EVENT_ADD_ALL";
export const SETTLEMENT_SE_EVENT_REMOVE_ALL = "SETTLEMENT_SE_EVENT_REMOVE_ALL";
export const SETTLEMENT_LOCATION_ADD_ALL = "SETTLEMENT_LOCATION_ADD_ALL";
export const SETTLEMENT_LOCATION_REMOVE_ALL = "SETTLEMENT_LOCATION_REMOVE_ALL";
export const LOADOUT_DELETE = "LOADOUT_DELETE";
export const SETTLEMENT_WEAPON_PROFICIENCY_ADD_ALL = "SETTLEMENT_WEAPON_PROFICIENCY_ADD_ALL";
export const SETTLEMENT_WEAPON_PROFICIENCY_REMOVE_ALL = "SETTLEMENT_WEAPON_PROFICIENCY_REMOVE_ALL";
export const SETTLEMENT_GEAR_ADD_ALL = "SETTLEMENT_GEAR_ADD_ALL";
export const SETTLEMENT_GEAR_REMOVE_ALL = "SETTLEMENT_GEAR_REMOVE_ALL";
export const SETTLEMENT_RESOURCE_ADD_ALL = "SETTLEMENT_RESOURCE_ADD_ALL";
export const SETTLEMENT_RESOURCE_REMOVE_ALL = "SETTLEMENT_RESOURCE_REMOVE_ALL";
export const SETTLEMENT_ABILITY_ADD_ALL = "SETTLEMENT_ABILITY_ADD_ALL";
export const SETTLEMENT_ABILITY_REMOVE_ALL = "SETTLEMENT_ABILITY_REMOVE_ALL";
export const SETTLEMENT_SET_DEPARTING = "SETTLEMENT_SET_DEPARTING";
export const SET_CURRENT_SHOWDOWN = "SET_CURRENT_SHOWDOWN";
export const ADD_COMPLETED_SHOWDOWN = "ADD_COMPLETED_SHOWDOWN";
export const SET_COMPLETED_SHOWDOWNS = "SET_COMPLETED_SHOWDOWNS";
export const DELETE_ALL_ARMOR_SET = "DELETE_ALL_ARMOR_SET";
export const SETTLEMENT_ARMOR_SET_ADD_ALL = "SETTLEMENT_ARMOR_SET_ADD_ALL";
export const SETTLEMENT_ARMOR_SET_REMOVE_ALL = "SETTLEMENT_ARMOR_SET_REMOVE_ALL";
export const SETTLEMENT_SET_MILESTONE_COMPLETE = "SETTLEMENT_SET_MILESTONE_COMPLETE";
export const SETTLEMENT_ADD_MILESTONE_REWARDS = "SETTLEMENT_ADD_MILESTONE_REWARDS";
export const SETTLEMENT_REMOVE_MILESTONE_REWARDS = "SETTLEMENT_REMOVE_MILESTONE_REWARDS";
export const SETTLEMENT_DELETE_MILESTONE = "SETTLEMENT_DELETE_MILESTONE";
export const SETTLEMENT_SET_DRAWN_INNOVATIONS = "SETTLEMENT_SET_DRAWN_INNOVATIONS";
export const SETTLEMENT_ADD_QUARRY = "SETTLEMENT_ADD_QUARRY";
export const SETTLEMENT_REMOVE_QUARRY = "SETTLEMENT_REMOVE_QUARRY";
export const ADD_PARTIAL_EXPANSION = "ADD_PARTIAL_EXPANSION";
export const REMOVE_PARTIAL_EXPANSION = "REMOVE_PARTIAL_EXPANSION";

import {networkBlock, sendServerPacket, sendClientPacket} from './network';
import { t } from '../helpers/intl';
import { returnSurvivors, setSurvival, setInsanity, resetArmor, applyLoadoutStats } from './survivors.js';
import { SETTLEMENT_MESSAGE_SURVIVAL_INCREASE, SETTLEMENT_MESSAGE_SURVIVAL_DECREASE, SETTLEMENT_MESSAGE_POPULATION_INCREASE, SETTLEMENT_MESSAGE_POPULATION_DECREASE, SETTLEMENT_MESSAGE_DEATH_COUNT_INCREASE, SETTLEMENT_MESSAGE_DEATH_COUNT_DECREASE, SETTLEMENT_MESSAGE_ADD_INNOVATION, SETTLEMENT_MESSAGE_REMOVE_INNOVATION, SETTLEMENT_MESSAGE_ENDEAVOR_DECREASE, SETTLEMENT_MESSAGE_ENDEAVOR_INCREASE, SETTLEMENT_MESSAGE_RESOURCE_INCREASE, SETTLEMENT_MESSAGE_RESOURCE_DECREASE, SETTLEMENT_MESSAGE_GEAR_INCREASE, SETTLEMENT_MESSAGE_GEAR_DECREASE, SETTLEMENT_MESSAGE_ADD_LOCATION, SETTLEMENT_MESSAGE_REMOVE_LOCATION, SETTLEMENT_MESSAGE_NAME_SET, SETTLEMENT_MESSAGE_PRINCIPLE_SET, SETTLEMENT_MESSAGE_PRINCIPLE_UNSET, SETTLEMENT_MESSAGE_YEAR_NEXT, SETTLEMENT_MESSAGE_YEAR_PREVIOUS, SETTLEMENT_MESSAGE_STATUE_SET, SETTLEMENT_MESSAGE_ARCHIVED_NO, SETTLEMENT_MESSAGE_ARCHIVED_YES, SETTLEMENT_MESSAGE_NOTE_EDIT, SETTLEMENT_MESSAGE_GEAR_CRAFTED, SETTLEMENT_MESSAGE_MILESTONE_YES, SETTLEMENT_MESSAGE_MILESTONE_NO, SETTLEMENT_MESSAGE_INNOVATION_ALL_REMOVE, SETTLEMENT_MESSAGE_INNOVATION_ALL_ADD, SETTLEMENT_MESSAGE_FIGHITNG_ART_ALL_ADD, SETTLEMENT_MESSAGE_FIGHITNG_ART_ALL_REMOVE, SETTLEMENT_MESSAGE_DISORDER_ALL_REMOVE, SETTLEMENT_MESSAGE_DISORDER_ALL_ADD, SETTLEMENT_MESSAGE_SE_EVENT_ALL_REMOVE, SETTLEMENT_MESSAGE_SE_EVENT_ALL_ADD, SETTLEMENT_MESSAGE_LOCATION_ALL_ADD, SETTLEMENT_MESSAGE_LOCATION_ALL_REMOVE, SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_ADD, SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_REMOVE, MESSAGE_LOADOUT_DELETED, SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_ADD, SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_REMOVE, SETTLEMENT_MESSAGE_GEAR_ALL_ADD, SETTLEMENT_MESSAGE_GEAR_ALL_REMOVE, SETTLEMENT_MESSAGE_RESOURCE_ALL_REMOVE, SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD, SETTLEMENT_MESSAGE_ABILITY_ALL_REMOVE, SETTLEMENT_MESSAGE_ABILITY_ALL_ADD } from '../helpers/LogMessages.js';
import { getInnovationConfig, getLocationConfig, getResourceConfig, getGearConfig, getPrincipleName, getPrinciplesConfig, getFightingArtConfig, getDisorderConfig, getSettlementEventConfig, getWeaponProfConfig, getAbilityConfig, getArmorConfig, getMilestoneConfig, allConditionsComplete, getMilestoneConditionValue, getExpansionTitle } from '../helpers/helpers.js';
import { getDepartingSurvivors, getAliveSurvivors } from '../selectors/survivors.js';
import SimpleToast from 'react-native-simple-toast';
import { updateHostContactTime } from './global.js';
import { getDepartingSurvival, getDepartingInsanity } from '../selectors/settlement.js';
import { addToTimeline } from './timeline.js';
import { MILESTONE_TYPE_GLOBAL } from '../reducers/custom-expansions.js';
import { addSettlementToConditions, removeSettlementFromConditions } from './milestones.js';

// container object holding names of functions (for dispatches)
// organized roughly in order of where it appears in the settlement's section of the app
const settlementActions = {
    "changeName": changeName,
    "increaseSurvival": increaseSurvival,
    "decreaseSurvival": decreaseSurvival,
    "increasePopulation": increasePopulation,
    "decreasePopulation": decreasePopulation,
    "increaseDeathCount": increaseDeathCount,
    "decreaseDeathCount": decreaseDeathCount,
    "increaseEndeavors": increaseEndeavors,
    "decreaseEndeavors": decreaseEndeavors,
    "setEndeavors": setEndeavors,
    "setInspirationalStatue": setInspirationalStatue,
    "addLocation": addLocation,
    "removeLocation": removeLocation,
    "addInnovation": addInnovation,
    "removeInnovation": removeInnovation,
    "addToInnovationDeck": addToInnovationDeck,
    "removeFromInnovationDeck": removeFromInnovationDeck,
    "setPrinciple": setPrinciple,
    "unsetPrinciple": unsetPrinciple,
    "increaseResource": increaseResource,
    "decreaseResource": decreaseResource,
    "increaseGear": increaseGear,
    "decreaseGear": decreaseGear,
    "editNote": editNote,
    "endYear": endYear,
    "unendYear": unendYear,
    "craftGear": craftGear,
    "setMilestoneValue": setMilestoneValue,
    "addFightingArtToAll": addFightingArtToAll,
    "removeFightingArtFromAll": removeFightingArtFromAll,
    "addInnovationToAll": addInnovationToAll,
    "removeInnovationFromAll": removeInnovationFromAll,
    "addDisorderToAll": addDisorderToAll,
    "removeDisorderFromAll": removeDisorderFromAll,
    "addSettlementEventToAll": addSettlementEventToAll,
    "removeSettlementEventFromAll": removeSettlementEventFromAll,
    "addSettlementLocationToAll": addSettlementLocationToAll,
    "removeSettlementLocationFromAll": removeSettlementLocationFromAll,
    "addSettlementWeaponProficiencyToAll": addSettlementWeaponProficiencyToAll,
    "removeSettlementWeaponProficiencyFromAll": removeSettlementWeaponProficiencyFromAll,
    "deleteLoadout": deleteLoadout,
    "removeSettlementGearFromAll": removeSettlementGearFromAll,
    "addSettlementGearToAll": addSettlementGearToAll,
    "addSettlementResourceToAll": addSettlementResourceToAll,
    "removeSettlementResourceFromAll": removeSettlementResourceFromAll,
    "addAbilityToAll": addAbilityToAll,
    "removeAbilityFromAll": removeAbilityFromAll,
    "setSettlementDeparting": setSettlementDeparting,
    "setCurrentShowdown": setCurrentShowdown,
    "addCompletedShowdown": addCompletedShowdown,
    "removeSettlementArmorSetFromAll": removeSettlementArmorSetFromAll,
    "addSettlementArmorSetToAll": addSettlementArmorSetToAll,
    "setDrawnInnovations": setDrawnInnovations,
    "addQuarry": addQuarry,
    "removeQuarry": removeQuarry,
    "contentToggleWrapper": contentToggleWrapper,
    "storageContentToggleWrapper": storageContentToggleWrapper,
};

export function settlementActionWrapper(action, args) {
    return (dispatch, getState) => {
        if(!settlementActions[action]) {
            return;
        }

        let state = getState();
        let isHost = state.networkReducer.server !== null;
        let isClient = state.networkReducer.client !== null;
        let share = state.networkReducer.shareId !== null;

        if(share && state.networkReducer.shareId !== args[0]) {
            share = false;
        }

        if(state.networkReducer.loading > 0) {
            return;
        }

        if(share) {
            if(isHost) {
                dispatch(sendServerPacket({reducer: 'settlementReducer', action, args}));
                dispatch(settlementActions[action](...args));
            } else if(isClient) {
                dispatch(sendClientPacket({reducer: 'settlementReducer', action, args}));
            }
        } else {
            dispatch(settlementActions[action](...args));
        }
    }
}

export function doSettlementHostAction(action, args) {
    return (dispatch) => {
        dispatch(settlementActions[action](...args));
        dispatch(updateHostContactTime());
    };
}

export function increaseSurvival(id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_SURVIVAL_INCREASE, {num: getState().settlementReducer.settlements[id].survivalLimit + 1}));
        dispatch({type: SURVIVAL_INCREASE, id});
    };
}

export function decreaseSurvival(id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_SURVIVAL_DECREASE, {num: getState().settlementReducer.settlements[id].survivalLimit - 1}));
        dispatch({type: SURVIVAL_DECREASE, id});
    };
}

export function increasePopulation(id) {
    return (dispatch, getState) => {
        const state = getState();
        if(
            state.settlementReducer.settlements[id].milestones.population &&
            state.settlementReducer.settlements[id].milestones.population.complete === false
        ) {
            const MilestoneConfig = getMilestoneConfig();
            dispatch(setMilestoneValue(id, "population", "population", state.settlementReducer.settlements[id].population + 1, getMilestoneConditionValue("population", "population", MilestoneConfig)));
        }

        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_POPULATION_INCREASE, {num: getState().settlementReducer.settlements[id].population + 1}));
        dispatch({type: POPULATION_INCREASE, id});
    };
}

export function decreasePopulation(id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_POPULATION_DECREASE, {num: getState().settlementReducer.settlements[id].population - 1}));
        dispatch({type: POPULATION_DECREASE, id});
    };
}

export function increaseDeathCount(id) {
    return (dispatch, getState) => {
        const state = getState();
        if(
            state.settlementReducer.settlements[id].milestones.firstDeath &&
            state.settlementReducer.settlements[id].milestones.firstDeath.complete === false
        ) {
            const MilestoneConfig = getMilestoneConfig();
            dispatch(setMilestoneValue(id, "firstDeath", "firstDeath", 1, getMilestoneConditionValue("firstDeath", "firstDeath", MilestoneConfig)));
        }
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_DEATH_COUNT_INCREASE, {num: getState().settlementReducer.settlements[id].deathCount + 1}));
        dispatch({type: DEATHCOUNT_INCREASE, id});
    };
}

export function decreaseDeathCount(id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_DEATH_COUNT_DECREASE, {num: getState().settlementReducer.settlements[id].deathCount - 1}));
        dispatch({type: DEATHCOUNT_DECREASE, id});
    };
}

export function addInnovation(id, name) {
    return (dispatch, getState) => {
        const state = getState();
        if(
            state.settlementReducer.settlements[id].milestones.hoodedKnight &&
            state.settlementReducer.settlements[id].milestones.hoodedKnight.complete === false
        ) {
            const MilestoneConfig = getMilestoneConfig();
            dispatch(setMilestoneValue(id, "hoodedKnight", "hoodedKnight", state.settlementReducer.settlements[id].innovations.length + 1, getMilestoneConditionValue("hoodedKnight", "hoodedKnight", MilestoneConfig)));
        }

        if(
            state.settlementReducer.settlements[id].milestones.edgedTonometry &&
            state.settlementReducer.settlements[id].milestones.edgedTonometry.complete === false
        ) {
            const MilestoneConfig = getMilestoneConfig();
            dispatch(setMilestoneValue(id, "edgedTonometry", "edgedTonometry", state.settlementReducer.settlements[id].innovations.length + 1, getMilestoneConditionValue("edgedTonometry", "edgedTonometry", MilestoneConfig)));
        }

        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ADD_INNOVATION, {name: getInnovationConfig()[name].title}));
        dispatch({type: INNOVATION_ADD, id, name});
    }
}

export function removeInnovation(id, name, all = false) {
    return (dispatch) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_REMOVE_INNOVATION, {name: getInnovationConfig()[name].title}));
        dispatch({type: INNOVATION_REMOVE, id, name, all});
    }
}

export function increaseEndeavors(id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ENDEAVOR_INCREASE, {num: getState().settlementReducer.settlements[id].endeavors + 1}));
        dispatch({type: ENDEAVOR_INCREASE, id});
    }
}

export function decreaseEndeavors(id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ENDEAVOR_DECREASE, {num: getState().settlementReducer.settlements[id].endeavors - 1}));
        dispatch({type: ENDEAVOR_DECREASE, id});
    }
}

export function setEndeavors(id, num) {
    return (dispatch, getState) => {
        let currentVal = getState().settlementReducer.settlements[id].endeavors;

        if(currentVal < num) {
            dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ENDEAVOR_INCREASE, {num}));
        } else if(currentVal > num) {
            dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ENDEAVOR_DECREASE, {num}));
        }

        dispatch({type: ENDEAVOR_SET, id, num});
    }
}

export function increaseResource(id, deck, name) {
    return (dispatch, getState) => {
        let locationConfig = getLocationConfig();
        let loc = locationConfig[deck] ? locationConfig[deck].title : deck;
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_INCREASE, {loc, name: getResourceConfig()[deck][name].title, num: getState().settlementReducer.settlements[id].storage.resources[deck][name] + 1}));
        dispatch({type: RESOURCE_INCREASE, id, deck, name});
    }
}

export function decreaseResource(id, deck, name) {
    return (dispatch, getState) => {
        let locationConfig = getLocationConfig();
        let loc = locationConfig[deck] ? locationConfig[deck].title : deck;
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_DECREASE, {loc, name: getResourceConfig()[deck][name].title, num: getState().settlementReducer.settlements[id].storage.resources[deck][name] - 1}));
        dispatch({type: RESOURCE_DECREASE, id, deck, name});
    }
}

export function deleteResource(id, deck, resourceId) {
    return (dispatch) => {
        dispatch({type: RESOURCE_DELETE, id, deck, resourceId});
    }
}

export function increaseGear(id, deck, name) {
    return (dispatch, getState) => {
        let locationConfig = getLocationConfig();
        let loc = locationConfig[deck] ? locationConfig[deck].title : deck;
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_GEAR_INCREASE, {loc, name: getGearConfig()[deck][name].title, num: getState().settlementReducer.settlements[id].storage.gear[deck][name] + 1}));
        dispatch({type: GEAR_INCREASE, id, deck, name});
    }
}

export function decreaseGear(id, deck, name) {
    return (dispatch, getState) => {
        let locationConfig = getLocationConfig();
        let loc = locationConfig[deck] ? locationConfig[deck].title : deck;
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_GEAR_DECREASE, {loc, name: getGearConfig()[deck][name].title, num: getState().settlementReducer.settlements[id].storage.gear[deck][name] - 1}));
        dispatch({type: GEAR_DECREASE, id, deck, name});
    }
}

export function deleteGear(id, deck, gearId) {
    return (dispatch) => {
        dispatch({type: GEAR_DELETE, id, deck, gearId});
    }
}

export function deleteCursedGear(id, deck, gearId) {
    return (dispatch) => {
        dispatch({type: GEAR_CURSED_DELETE, id, deck, gearId});
    }
}

export function addCursedGear(id, deck, gearId) {
    return (dispatch) => {
        dispatch({type: GEAR_CURSED_ADD, id, deck, gearId});
    }
}

export function addLocation(id, name) {
    return (dispatch) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ADD_LOCATION, {name: getLocationConfig()[name].title}));
        dispatch({type: LOCATION_ADD, id, name});
    }
}

export function removeLocation(id, name, all = false) {
    return (dispatch) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_REMOVE_LOCATION, {name: getLocationConfig()[name].title}));
        dispatch({type: LOCATION_REMOVE, id, name, all});
    }
}

export function changeName(id, name) {
    return (dispatch) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_NAME_SET, {name}));
        dispatch({type: NAME_CHANGE, id, name});
    }
}

export function setPrinciple(id, principle, name, statType, stats) {
    return (dispatch, getState) => {
        const settlement = getState().settlementReducer.settlements[id];
        if(settlement.principles[principle] === name) {
            return;
        }

        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_PRINCIPLE_SET, {princ: getPrincipleName(principle), name: getPrinciplesConfig()[principle][name].title}));
        dispatch({type: PRINCIPLE_SET, id, principle, name, statType, stats});
    }
}

export function unsetPrinciple(id, principle, statType, stats) {
    return (dispatch) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_PRINCIPLE_UNSET, {princ: getPrincipleName(principle)}));
        dispatch({type: PRINCIPLE_UNSET, id, principle, statType, stats});
    }
}

export function createSettlement(id, name, expansions = [{"core": "all"}], campaign = "PotLantern", customExpansions = {}) {
    return (dispatch) => {
        dispatch({type: CREATE_SETTLEMENT, id, name, expansions, campaign, customExpansions});
    }
}

export function importSettlement(settlement) {
    return (dispatch) => {
        dispatch({type: IMPORT_SETTLEMENT, settlement});
    }
}

export function endYear(id) {
    return (dispatch, getState) => {
        dispatch(calculateEndeavors(id));
        dispatch(returnSurvivors(id));
        let state = getState();
        let current = state.settlementReducer.settlements[id].ly;
        let max = state.timelineReducer[id].length;
        if(current < max - 1) {
            dispatch(addActionLog(id, SETTLEMENT_MESSAGE_YEAR_NEXT, {ly: current + 1}));
            dispatch({type: END_YEAR, id});
            dispatch(setCurrentShowdown(id, null));
            dispatch(setCompletedShowdowns(id, []));
            dispatch(setSettlementDeparting(id, false));
        }
    }
}

function calculateEndeavors(id) {
    return (dispatch, getState) => {
        const state = getState();
        const departing = getDepartingSurvivors(state);

        let endeavors = departing.length;
        for(let i = 0; i < departing.length; i++) {
            if(departing[i].disorders.indexOf("postTraumaticStress") !== -1) {
                endeavors -= 1;
                continue;
            }

            if(departing[i].abilities.indexOf("tinker") !== -1) {
                endeavors += 1;
            }
        }

        if(state.settlementReducer.settlements[id].innovations.indexOf("cooking") !== -1) {
            endeavors += 1;
        }

        if(state.settlementReducer.settlements[id].principles.society === "collectiveToil") {
            endeavors += Math.floor(getAliveSurvivors(state).length / 10);
        }

        endeavors = endeavors + state.settlementReducer.settlements[id].endeavors;

        dispatch(setEndeavors(id, endeavors));
    }
}

export function unendYear(id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_YEAR_PREVIOUS, {ly: getState().settlementReducer.settlements[id].ly - 1}));
        dispatch({type: UNEND_YEAR, id});
    }
}

export function setInspirationalStatue(id, name) {
    return (dispatch) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_STATUE_SET, {name: getFightingArtConfig()[name].title}));
        dispatch({type: INSPIRATIONAL_STATUE_SET, id, name});
    }
}

export function deleteSettlement(id) {
    return (dispatch, getState) => {
        if(networkBlock(t("Cannot delete an expansion while networked"))(dispatch, getState)) return;
        dispatch({type: DELETE_SETTLEMENT, id});
    }
}

export function toggleArchive(id) {
    return (dispatch, getState) => {
        if(getState().settlementReducer.settlements[id].archived) {
            dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ARCHIVED_NO));
        } else {
            dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ARCHIVED_YES));
        }

        dispatch({type: TOGGLE_ARCHIVE, id});
    }
}

export function addExpansions(id, expansions) {
    return (dispatch) => {
        dispatch({type: ADD_EXPANSIONS, id, expansions});
    }
}

export function addToInnovationDeck(id, name) {
    return (dispatch) => {
        dispatch({type: INNOVATION_DECK_ADD, id, name});
    }
}

export function removeFromInnovationDeck(id, name) {
    return (dispatch) => {
        dispatch({type: INNOVATION_DECK_REMOVE, id, name});
    }
}

export function copySettlement(oldId, newId) {
    return (dispatch) => {
        dispatch({type: COPY_SETTLEMENT, oldId, newId});
    }
}

export function setSurvivorSortAttribute(id, attr) {
    return (dispatch) => {
        dispatch({type: SET_SURVIVOR_SORT_ATTR, id, attr});
    }
}

export function setSurvivorSortOrder(id, order) {
    return (dispatch) => {
        dispatch({type: SET_SURVIVOR_SORT_ORDER, id, order});
    }
}

export function editNote(id, note) {
    return (dispatch) => {
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_NOTE_EDIT, {note}));
        dispatch({type: EDIT_NOTE_SETTLEMENT, id, note});
    }
}

export function deleteAllFightingArt(settlementId, faId) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_FIGHTING_ART, settlementId, faId});
    }
}

export function deleteAllDisorder(settlementId, disorderId) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_DISORDER, settlementId, disorderId});
    }
}

export function deleteAllAbility(settlementId, abilityId) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_ABILITY, settlementId, abilityId});
    }
}

export function deleteAllWeaponProficiency(settlementId, profId) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_WEAPON_PROFICIENCY, settlementId, profId});
    }
}

export function deleteAllArmorSet(settlementId, armorId) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_ARMOR_SET, settlementId, armorId});
    }
}

export function deleteAllSevereInjury(settlementId, injuryId) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_SEVERE_INJURY, settlementId, injuryId});
    }
}

export function deleteAllStoryEvent(settlementId, event) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_STORY_EVENT, settlementId, event});
    }
}

export function deleteAllShowdown(settlementId, event) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_SHOWDOWN, settlementId, event});
    }
}

export function deleteAllNemesisEncounters(settlementId, event) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_NEMESIS_ENCOUNTER, settlementId, event});
    }
}

export function deleteAllSettlementEvent(settlementId, event) {
    return (dispatch) => {
        dispatch({type: DELETE_ALL_SETTLEMENT_EVENT, settlementId, event});
    }
}

export function addCustomExpansionsToSettlement(settlementId, expansions) {
    return (dispatch, getState) => {
        for(let id in expansions) {
            dispatch(addCustomExpansionToSettlement(settlementId, id, expansions[id]));
        }
    }
}

export function addCustomExpansionToSettlement(settlementId, expansion, mode) {
    return (dispatch, getState) => {
        const state = getState();

        dispatch({type: ADD_CUSTOM_EXPANSION_TO_SETTLEMENT, settlementId, expansion: state.customExpansions[expansion], mode});

        if(state.customExpansions[expansion].milestones) {
            for(let id in state.customExpansions[expansion].milestones) {
                if(
                    state.customExpansions[expansion].milestones[id].type === MILESTONE_TYPE_GLOBAL &&
                    state.customExpansions[expansion].milestones[id].effects &&
                    Object.keys(state.customExpansions[expansion].milestones[id].effects).length > 0
                ) {
                    if(state.milestones[id] && state.milestones[id].complete === true) {
                        dispatch(addMilestoneRewardsToSettlement(settlementId, state.customExpansions[expansion].milestones[id].effects));
                    }
                }

                if(
                    state.customExpansions[expansion].milestones[id].type === MILESTONE_TYPE_GLOBAL &&
                    state.customExpansions[expansion].milestones[id].settlementConditions &&
                    Object.keys(state.customExpansions[expansion].milestones[id].settlementConditions).length > 0
                ) {
                    dispatch(addSettlementToConditions(settlementId, id));
                }
            }
        }
    }
}

export function removeCustomExpansionsFromSettlement(settlementId, expansions) {
    return (dispatch, getState) => {
        let state = getState();

        for(let i = 0; i < expansions.length; i++) {
            let expansion = state.customExpansions[expansions[i]];
            dispatch(removeCustomInnovationsFromSettlement(settlementId, expansion.innovations));
            dispatch(removeCustomResourcesFromSettlement(settlementId, expansion.resourceConfig));
            dispatch(removeCustomGearFromSettlement(settlementId, expansion.gearConfig));
            dispatch(removeCursedGearFromSettlement(settlementId, expansion.gearConfig));
            dispatch(removeCustomLocationsFromSettlement(settlementId, expansion.locations));
            dispatch(removeCustomFightingArtsFromSettlement(settlementId, expansion.fightingArts));
            dispatch(removeCustomDisordersFromSettlement(settlementId, expansion.disorders));
            dispatch(removeCustomAbilitiesFromSettlement(settlementId, expansion.abilities));
            dispatch(removeCustomInjuriesFromSettlement(settlementId, expansion.severeInjuries));
            dispatch(removeCustomSettlementEventsFromSettlement(settlementId, expansion.settlementEvents));
            dispatch(removeCustomStoryEventsFromSettlement(settlementId, expansion.storyEvents));
            dispatch(removeCustomShowdownsFromSettlement(settlementId, expansion.showdowns));
            dispatch(removeCustomNemesisEncountersFromSettlement(settlementId, expansion.nemesisEncounters));
            dispatch(removeCustomWeaponProficienciesFromSettlement(settlementId, expansion.weaponProfs));
            dispatch(removeCustomArmorSetsFromSettlement(settlementId, expansion.armorSets));

            if(expansion.milestones) {
                for(let id in expansion.milestones) {
                    if(
                        expansion.milestones[id].type === MILESTONE_TYPE_GLOBAL &&
                        expansion.milestones[id].settlementConditions &&
                        Object.keys(expansion.milestones[id].settlementConditions).length > 0
                    ) {
                        dispatch(removeSettlementFromConditions(settlementId, id));
                    }
                }
            }
        }

        dispatch(removeExpansionsFromSettlementList(settlementId, expansions));
    }
}

export function removeExpansionsFromSettlement(settlementId, expansions) {
    return (dispatch, getState) => {
        for(let i = 0; i < expansions.length; i++) {
            dispatch(removeCustomInnovationsFromSettlement(settlementId, Innovations[expansions[i]]));
            dispatch(removeCustomResourcesFromSettlement(settlementId, Resources[expansions[i]]));
            dispatch(removeCustomGearFromSettlement(settlementId, Gear[expansions[i]]));
            dispatch(removeCursedGearFromSettlement(settlementId, Gear[expansions[i]]));
            dispatch(removeCustomLocationsFromSettlement(settlementId, Locations[expansions[i]]));
            dispatch(removeCustomFightingArtsFromSettlement(settlementId, FightingArts[expansions[i]]));
            dispatch(removeCustomDisordersFromSettlement(settlementId, Disorders[expansions[i]]));
            dispatch(removeCustomAbilitiesFromSettlement(settlementId, Abilities[expansions[i]]));
            dispatch(removeCustomSettlementEventsFromSettlement(settlementId, SettlementEvents[expansions[i]]));
            dispatch(removeCustomStoryEventsFromSettlement(settlementId, StoryEvents[expansions[i]]));
            dispatch(removeCustomShowdownsFromSettlement(settlementId, Showdowns[expansions[i]]));
            dispatch(removeCustomNemesisEncountersFromSettlement(settlementId, NemesisEncounters[expansions[i]]));
            dispatch(removeCustomWeaponProficienciesFromSettlement(settlementId, Proficiencies[expansions[i]]));
            dispatch(removeCustomArmorSetsFromSettlement(settlementId, Armor[expansions[i]]));
        }

        dispatch(removeExpansionsFromSettlementList(settlementId, expansions));
    }
}

export function downgradeExpansionsInSettlement(settlementId, expansions) {
    return (dispatch, getState) => {
        for(let i = 0; i < expansions.length; i++) {
            dispatch(removeCustomResourcesFromSettlement(settlementId, Resources[expansions[i]]));
            dispatch(removeCustomGearFromSettlement(settlementId, Gear[expansions[i]]));
            dispatch(removeCursedGearFromSettlement(settlementId, Gear[expansions[i]]));
            dispatch(removeCustomLocationsFromSettlement(settlementId, Locations[expansions[i]]));
            dispatch(removeCustomSettlementEventsFromSettlement(settlementId, SettlementEvents[expansions[i]]));
            dispatch(removeCustomStoryEventsFromSettlement(settlementId, StoryEvents[expansions[i]]));
            dispatch(removeCustomShowdownsFromSettlement(settlementId, Showdowns[expansions[i]]));
            dispatch(removeCustomNemesisEncountersFromSettlement(settlementId, NemesisEncounters[expansions[i]]));
            dispatch(removeCustomArmorSetsFromSettlement(settlementId, Armor[expansions[i]]));
        }

        dispatch(removeExpansionsFromSettlementList(settlementId, expansions));
    }
}

export function downgradeCustomExpansionsInSettlement(settlementId, expansions) {
    return (dispatch, getState) => {
        let state = getState();

        for(let i = 0; i < expansions.length; i++) {
            let expansion = state.customExpansions[expansions[i]];
            dispatch(removeCustomResourcesFromSettlement(settlementId, expansion.resourceConfig));
            dispatch(removeCustomGearFromSettlement(settlementId, expansion.gearConfig));
            dispatch(removeCursedGearFromSettlement(settlementId, expansion.gearConfig));
            dispatch(removeCustomLocationsFromSettlement(settlementId, expansion.locations));
            dispatch(removeCustomStoryEventsFromSettlement(settlementId, expansion.storyEvents));
            dispatch(removeCustomInjuriesFromSettlement(settlementId, expansion.severeInjuries));
            dispatch(removeCustomSettlementEventsFromSettlement(settlementId, expansion.settlementEvents));
            dispatch(removeCustomShowdownsFromSettlement(settlementId, expansion.showdowns));
            dispatch(removeCustomNemesisEncountersFromSettlement(settlementId, expansion.nemesisEncounters));
            dispatch(removeCustomArmorSetsFromSettlement(settlementId, expansion.armorSets));
        }

        dispatch(setCustomExpansionType(settlementId, expansions, "cards"));
    }
}

export function setCustomExpansionType(settlementId, expansions, type) {
    return (dispatch, getState) => {
        dispatch({type: DOWNGRADE_CUSTOM_EXPANSION_IN_SETTLEMENT, settlementId, expansions, type});
    }
}

export function removeCustomInnovationsFromSettlement(settlementId, innovations) {
    return (dispatch, getState) => {
        if(innovations) {
            for(let i = 0; i < innovations.length; i++) {
                dispatch(removeInnovation(settlementId, innovations[i], true));
            }
        }
    }
}

export function removeCustomLocationsFromSettlement(settlementId, locations) {
    return (dispatch, getState) => {
        if(locations) {
            for(let i = 0; i < locations.length; i++) {
                dispatch(removeLocation(settlementId, locations[i], true));
            }
        }
    }
}

export function removeCustomResourcesFromSettlement(settlementId, resourceConfig) {
    return (dispatch, getState) => {
        if(resourceConfig) {
            for(let source in resourceConfig) {
                for(let resourceId in resourceConfig[source]) {
                    dispatch(deleteResource(settlementId, source, resourceId));
                }
            }
        }
    }
}

export function removeCustomGearFromSettlement(settlementId, gearConfig) {
    return (dispatch, getState) => {
        if(gearConfig) {
            for(let source in gearConfig) {
                for(let gearId in gearConfig[source]) {
                    dispatch(deleteGear(settlementId, source, gearId));
                }
            }
        }
    }
}

export function removeCursedGearFromSettlement(settlementId, gearConfig) {
    return (dispatch, getState) => {
        if(gearConfig) {
            for(let source in gearConfig) {
                for(let gearId in gearConfig[source]) {
                    dispatch(deleteCursedGear(settlementId, source, gearId));
                }
            }
        }
    }
}

export function removeCustomFightingArtsFromSettlement(settlementId, arts) {
    return (dispatch, getState) => {
        if(arts) {
            for(let i = 0; i < arts.length; i++) {
                dispatch(deleteAllFightingArt(settlementId, arts[i]));
            }
        }
    }
}

export function removeCustomDisordersFromSettlement(settlementId, disorders) {
    return (dispatch, getState) => {
        if(disorders) {
            for(let i = 0; i < disorders.length; i++) {
                dispatch(deleteAllDisorder(settlementId, disorders[i]));
            }
        }
    }
}

export function removeCustomAbilitiesFromSettlement(settlementId, abilities) {
    return (dispatch, getState) => {
        if(abilities) {
            for(let i = 0; i < abilities.length; i++) {
                dispatch(deleteAllAbility(settlementId, abilities[i]));
            }
        }
    }
}

export function removeCustomArmorSetsFromSettlement(settlementId, armor) {
    return (dispatch, getState) => {
        if(armor) {
            for(let i = 0; i < armor.length; i++) {
                dispatch(deleteAllArmorSet(settlementId, armor[i]));
            }
        }
    }
}

export function removeCustomWeaponProficienciesFromSettlement(settlementId, proficiencies) {
    return (dispatch, getState) => {
        if(proficiencies) {
            for(let i = 0; i < proficiencies.length; i++) {
                dispatch(deleteAllWeaponProficiency(settlementId, proficiencies[i]));
            }
        }
    }
}

export function removeCustomInjuriesFromSettlement(settlementId, injuries) {
    return (dispatch, getState) => {
        if(injuries) {
            for(let i = 0; i < injuries.length; i++) {
                dispatch(deleteAllSevereInjury(settlementId, injuries[i]));
            }
        }
    }
}

export function removeCustomSettlementEventsFromSettlement(settlementId, events) {
    return (dispatch, getState) => {
        if(events) {
            for(let i = 0; i < events.length; i++) {
                dispatch(deleteAllSettlementEvent(settlementId, events[i]));
            }
        }
    }
}

export function removeCustomStoryEventsFromSettlement(settlementId, events) {
    return (dispatch, getState) => {
        if(events) {
            for(let i = 0; i < events.length; i++) {
                dispatch(deleteAllStoryEvent(settlementId, events[i]));
            }
        }
    }
}

export function removeCustomShowdownsFromSettlement(settlementId, events) {
    return (dispatch, getState) => {
        if(events) {
            for(let i = 0; i < events.length; i++) {
                dispatch(deleteAllShowdown(settlementId, events[i]));
            }
        }
    }
}

export function removeCustomNemesisEncountersFromSettlement(settlementId, events) {
    return (dispatch, getState) => {
        if(events) {
            for(let i = 0; i < events.length; i++) {
                dispatch(deleteAllNemesisEncounters(settlementId, events[i]));
            }
        }
    }
}

export function removeExpansionsFromSettlementList(settlementId, expansions) {
    return (dispatch, getState) => {
        for(let i = 0; i < expansions.length; i++) {
            dispatch({type: REMOVE_CUSTOM_EXPANSION_FROM_SETTLEMENT, settlementId, expansion: expansions[i]});
        }
    }
}

export function addActionLog(id, message, args = {}) {
    return (dispatch) => {
        const time = Date.now();
        let log = {t: time, m: message};
        if(args && Object.keys(args).length > 0) {
            log.a = args;
        }
        dispatch({type: SETTLEMENT_ADD_ACTION_LOG, id, log});
    }
}

export function craftGear(id, gear, resources, gearCost = []) {
    return (dispatch, getState) => {
        const GearConfig = getGearConfig();
        let cost = resourcesToTitles(resources);
        if(gearCost.length > 0) {
            cost += ', ' + gearToTitles(gearCost);
        }
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_GEAR_CRAFTED, {gear: GearConfig[gear.location][gear.name].title, cost}));
        dispatch({type: CRAFT_GEAR, id, gear, resources, gearCost});
    }
}

export function addInnovationToAll(id, name) {
    return (dispatch, getState) => {
        const InnovationConfig = getInnovationConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_INNOVATION_ALL_ADD, {name: InnovationConfig[name].title}));
        dispatch({type: SETTLEMENT_INNOVATION_ADD_ALL, id, name});
    }
}

export function removeInnovationFromAll(id, name) {
    return (dispatch, getState) => {
        const InnovationConfig = getInnovationConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_INNOVATION_ALL_REMOVE, {name: InnovationConfig[name].title}));
        dispatch({type: SETTLEMENT_INNOVATION_REMOVE_ALL, id, name});
    }
}

export function addFightingArtToAll(id, name) {
    return (dispatch, getState) => {
        const FAConfig = getFightingArtConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_ADD, {name: FAConfig[name].title}));
        dispatch({type: SETTLEMENT_FIGHTING_ART_ADD_ALL, id, name});
    }
}

export function removeFightingArtFromAll(id, name) {
    return (dispatch, getState) => {
        const FAConfig = getFightingArtConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_REMOVE, {name: FAConfig[name].title}));
        dispatch({type: SETTLEMENT_FIGHTING_ART_REMOVE_ALL, id, name});
    }
}

export function removeDisorderFromAll(id, name) {
    return (dispatch, getState) => {
        const DisorderConfig = getDisorderConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_DISORDER_ALL_REMOVE, {name: DisorderConfig[name].title}));
        dispatch({type: SETTLEMENT_DISORDER_REMOVE_ALL, id, name});
    }
}

export function addDisorderToAll(id, name) {
    return (dispatch, getState) => {
        const DisorderConfig = getDisorderConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_DISORDER_ALL_ADD, {name: DisorderConfig[name].title}));
        dispatch({type: SETTLEMENT_DISORDER_ADD_ALL, id, name});
    }
}

export function removeAbilityFromAll(id, name) {
    return (dispatch, getState) => {
        const AbilityConfig = getAbilityConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ABILITY_ALL_REMOVE, {name: AbilityConfig[name].title}));
        dispatch({type: SETTLEMENT_ABILITY_REMOVE_ALL, id, name});
    }
}

export function addAbilityToAll(id, name) {
    return (dispatch, getState) => {
        const AbilityConfig = getAbilityConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_ABILITY_ALL_ADD, {name: AbilityConfig[name].title}));
        dispatch({type: SETTLEMENT_ABILITY_ADD_ALL, id, name});
    }
}

export function removeSettlementEventFromAll(id, name) {
    return (dispatch, getState) => {
        const SettlementEventConfig = getSettlementEventConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_SE_EVENT_ALL_REMOVE, {name: SettlementEventConfig[name].title}));
        dispatch({type: SETTLEMENT_SE_EVENT_REMOVE_ALL, id, name});
    }
}

export function addSettlementEventToAll(id, name) {
    return (dispatch, getState) => {
        const SettlementEventConfig = getSettlementEventConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_SE_EVENT_ALL_ADD, {name: SettlementEventConfig[name].title}));
        dispatch({type: SETTLEMENT_SE_EVENT_ADD_ALL, id, name});
    }
}

export function removeSettlementLocationFromAll(id, name) {
    return (dispatch, getState) => {
        const config = getLocationConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_LOCATION_ALL_REMOVE, {name: config[name].title}));
        dispatch({type: SETTLEMENT_LOCATION_REMOVE_ALL, id, name});
    }
}

export function addSettlementLocationToAll(id, name) {
    return (dispatch, getState) => {
        const config = getLocationConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_LOCATION_ALL_ADD, {name: config[name].title}));
        dispatch({type: SETTLEMENT_LOCATION_ADD_ALL, id, name});
    }
}

export function removeSettlementWeaponProficiencyFromAll(id, name) {
    return (dispatch, getState) => {
        const config = getWeaponProfConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_REMOVE, {name: config[name].title}));
        dispatch({type: SETTLEMENT_WEAPON_PROFICIENCY_REMOVE_ALL, id, name});
    }
}

export function addSettlementWeaponProficiencyToAll(id, name) {
    return (dispatch, getState) => {
        const config = getWeaponProfConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_ADD, {name: config[name].title}));
        dispatch({type: SETTLEMENT_WEAPON_PROFICIENCY_ADD_ALL, id, name});
    }
}

export function removeSettlementArmorSetFromAll(id, name) {
    return (dispatch, getState) => {
        const config = getArmorConfig();
        dispatch(addActionLog(id, SETTLEMENT_ARMOR_SET_REMOVE_ALL, {name: config[name].title}));
        dispatch({type: SETTLEMENT_ARMOR_SET_REMOVE_ALL, id, name});
    }
}

export function addSettlementArmorSetToAll(id, name) {
    return (dispatch, getState) => {
        const config = getArmorConfig();
        dispatch(addActionLog(id, SETTLEMENT_ARMOR_SET_ADD_ALL, {name: config[name].title}));
        dispatch({type: SETTLEMENT_ARMOR_SET_ADD_ALL, id, name});
    }
}

function resourcesToTitles(resources) {
    const ResourceConfig = getResourceConfig();

    let counts = {};
    for(let i = 0; i < resources.length; i++) {
        const title = ResourceConfig[resources[i].location][resources[i].name].title;
        if(!counts[title]) {
            counts[title] = 1;
        } else {
            counts[title] = counts[title] + 1;
        }
    }

    let titles = [];
    for(let title in counts) {
        if(counts[title] > 1) {
            titles.push(title + ' x ' + counts[title]);
        } else {
            titles.push(title);
        }
    }

    return titles.join(', ');
}

function gearToTitles(gear) {
    const GearConfig = getGearConfig();

    let counts = {};
    for(let i = 0; i < gear.length; i++) {
        const title = GearConfig[gear[i].location][gear[i].name].title;
        if(!counts[title]) {
            counts[title] = 1;
        } else {
            counts[title] = counts[title] + 1;
        }
    }

    let titles = [];
    for(let title in counts) {
        if(counts[title] > 1) {
            titles.push(title + ' x ' + counts[title]);
        } else {
            titles.push(title);
        }
    }

    return titles.join(', ');
}

export function setMilestoneValue(id, milestoneId, conditionId, value, target) {
    return (dispatch, getState) => {
        dispatch(_setMilestoneValue(id, milestoneId, conditionId, value, target)).then(state => {
            const complete = allConditionsComplete(state.settlementReducer.settlements[id].milestones[milestoneId]);
            if(state.settlementReducer.settlements[id].milestones[milestoneId].complete === false && complete) {
                dispatch(setSettlementMilestoneComplete(id, milestoneId, true));
            } else if(state.settlementReducer.settlements[id].milestones[milestoneId].complete === true && !complete) {
                dispatch(setSettlementMilestoneComplete(id, milestoneId, false));
            }
        });
    }
}

function _setMilestoneValue(id, milestoneId, conditionId, value, target) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_SET_MILESTONE_VALUE, id, milestoneId, conditionId, value, target});
        return Promise.resolve(getState());
    }
}

export function deleteSettlementMilestone(id, milestoneId) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_DELETE_MILESTONE, id, milestoneId});
    }
}

function setSettlementMilestoneComplete(id, milestoneId, value) {
    return (dispatch, getState) => {
        const MilestoneConfig = getMilestoneConfig();
        if(value === true) {
            dispatch(addActionLog(id, SETTLEMENT_MESSAGE_MILESTONE_YES, {name: MilestoneConfig[milestoneId].title}));
            dispatch(addMilestoneRewardsToSettlement(id, MilestoneConfig[milestoneId].effects));
            SimpleToast.show(MilestoneConfig[milestoneId].reward, SimpleToast.LONG);
        } else {
            dispatch(removeMilestoneRewardsFromSettlement(id, MilestoneConfig[milestoneId].effects));
            dispatch(addActionLog(id, SETTLEMENT_MESSAGE_MILESTONE_NO, {name: MilestoneConfig[milestoneId].title}));
        }
        dispatch({type: SETTLEMENT_SET_MILESTONE_COMPLETE, id, milestoneId, value});
    }
}

export function addMilestoneRewardsToSettlement(id, rewards) {
    return (dispatch, getState) => {
        if(rewards) {
            dispatch({type: SETTLEMENT_ADD_MILESTONE_REWARDS, id, rewards});
        }
    }
}

export function removeMilestoneRewardsFromSettlement(id, rewards) {
    return (dispatch, getState) => {
        if(rewards) {
            dispatch({type: SETTLEMENT_REMOVE_MILESTONE_REWARDS, id, rewards});
        }
    }
}

export function deleteLoadout(id, loadoutId) {
    return (dispatch, getState) => {
        const state = getState();
        const loadout = state.settlementReducer.settlements[id].loadouts[loadoutId]

        if(loadout) {
            dispatch(addActionLog(id, MESSAGE_LOADOUT_DELETED, {name: loadout.name}));
        }

        dispatch({type: LOADOUT_DELETE, id, loadoutId});
    }
}

export function removeSettlementGearFromAll(id, location, gearId) {
    return (dispatch, getState) => {
        const config = getGearConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_GEAR_ALL_REMOVE, {name: config[location][gearId].title}));
        dispatch({type: SETTLEMENT_GEAR_REMOVE_ALL, id, location, gearId});
    }
}

export function addSettlementGearToAll(id, location, gearId) {
    return (dispatch, getState) => {
        const config = getGearConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_GEAR_ALL_ADD, {name: config[location][gearId].title}));
        dispatch({type: SETTLEMENT_GEAR_ADD_ALL, id, location, gearId});
    }
}

export function removeSettlementResourceFromAll(id, location, resourceId) {
    return (dispatch, getState) => {
        const config = getResourceConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_ALL_REMOVE, {name: config[location][resourceId].title}));
        dispatch({type: SETTLEMENT_RESOURCE_REMOVE_ALL, id, location, resourceId});
    }
}

export function addSettlementResourceToAll(id, location, resourceId) {
    return (dispatch, getState) => {
        const config = getResourceConfig();
        dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD, {name: config[location][resourceId].title}));
        dispatch({type: SETTLEMENT_RESOURCE_ADD_ALL, id, location, resourceId});
    }
}

export function setSettlementDeparting(id, value) {
    return (dispatch, getState) => {
        if(value === true) {
            let state = getState();
            const departingSurvivors = getDepartingSurvivors(state);
            const survival = getDepartingSurvival(state);
            const insanity = getDepartingInsanity(state);
            const survivalLimit = state.settlementReducer.settlements[id].survivalLimit;

            for(let i = 0; i < departingSurvivors.length; i++) {
                if(survival) {
                    let newSurvival = Math.min(departingSurvivors[i].survival + survival, survivalLimit);
                    if(newSurvival !== departingSurvivors[i].survival) {
                        dispatch(setSurvival(id, departingSurvivors[i].id, newSurvival));
                    }
                }

                if(insanity) {
                    dispatch(setInsanity(id, departingSurvivors[i].id, departingSurvivors[i].insanity.value + insanity));
                }

                dispatch(resetArmor(id, departingSurvivors[i].id));
                dispatch(applyLoadoutStats(id, departingSurvivors[i].id));
            }

            const currentShowdown = state.settlementReducer.settlements[id].currentShowdown;
            const ly = state.settlementReducer.settlements[id].ly;
            if(!eventOnTimeline(state.timelineReducer[id][ly], currentShowdown.type, currentShowdown.monster)) {
                dispatch(addToTimeline(id, ly, currentShowdown.type, currentShowdown.monster));
            }

            dispatch(addCompletedShowdown(id, {...currentShowdown}));
        }
        // dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD, {name: config[location][resourceId].title}));
        dispatch({type: SETTLEMENT_SET_DEPARTING, id, value});
    }
}

function eventOnTimeline(events, type, name) {
    return events[type].indexOf(name) !== -1;
}

export function setCurrentShowdown(id, showdown) {
    return (dispatch, getState) => {
        // dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD, {name: config[location][resourceId].title}));
        dispatch({type: SET_CURRENT_SHOWDOWN, id, showdown});
    }
}

export function addCompletedShowdown(id, showdown) {
    return (dispatch, getState) => {
        // dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD, {name: config[location][resourceId].title}));
        dispatch({type: ADD_COMPLETED_SHOWDOWN, id, showdown});
    }
}

export function setCompletedShowdowns(id, value) {
    return (dispatch, getState) => {
        // dispatch(addActionLog(id, SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD, {name: config[location][resourceId].title}));
        dispatch({type: SET_COMPLETED_SHOWDOWNS, id, value});
    }
}

export function setDrawnInnovations(id, innovations) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_SET_DRAWN_INNOVATIONS, id, innovations});
    }
}

export function addQuarry(id, quarry) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_ADD_QUARRY, id, quarry});
    }
}

export function removeQuarry(id, quarry) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_REMOVE_QUARRY, id, quarry});
    }
}

function addPartialExpansionToSettlement(id, expansion) {
    return (dispatch, getState) => {
        dispatch({type: ADD_PARTIAL_EXPANSION, id, expansion});
    }
}

function removePartialExpansionFromSettlement(id, expansion) {
    return (dispatch, getState) => {
        dispatch({type: REMOVE_PARTIAL_EXPANSION, id, expansion});
    }
}

export function contentToggleWrapper(fn, id, itemId, config, remove = false) {
    return (dispatch, getState) => {
        const expansion = config[itemId].expansion;
        if(expansion && !getExpansionTitle(expansion)) {
            if(remove) {
                dispatch(removePartialExpansionFromSettlement(id, expansion));
            } else {
                dispatch(addPartialExpansionToSettlement(id, expansion));
            }
        }
        dispatch(settlementActions[fn](id, itemId));
    }
}

export function storageContentToggleWrapper(fn, id, location, itemId, config, remove = false) {
    return (dispatch, getState) => {
        const expansion = config[location][itemId].expansion;
        if(expansion && !getExpansionTitle(expansion)) {
            if(remove) {
                dispatch(removePartialExpansionFromSettlement(id, expansion));
            } else {
                dispatch(addPartialExpansionToSettlement(id, expansion));
            }
        }
        dispatch(settlementActions[fn](id, location, itemId));
    }
}
