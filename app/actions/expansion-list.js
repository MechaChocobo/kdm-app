export const SET_EXPANSION_LIST = "SET_EXPANSION_LIST";
export const SET_EXPANSION_LIST_LOADING = "SET_EXPANSION_LIST_LOADING";
export const SET_EXPANSION_LIST_FILTER = "SET_EXPANSION_LIST_FILTER";

export function setExpansionList(expansions) {
    return (dispatch, getState) => {
        dispatch({type: SET_EXPANSION_LIST, expansions});
    }
}

export function setExpansionListLoading(value) {
    return (dispatch, getState) => {
        dispatch({type: SET_EXPANSION_LIST_LOADING, value});
    }
}

export function setExpansionListFilter(value) {
    return (dispatch, getState) => {
        dispatch({type: SET_EXPANSION_LIST_FILTER, value});
    }
}
