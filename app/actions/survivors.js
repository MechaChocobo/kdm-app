import Uuid from 'react-native-uuid';
import SimpleToast from 'react-native-simple-toast';

import {addActionLog as settlementAddActionLog, setSettlementDeparting, setCurrentShowdown, increasePopulation, addInnovation} from './settlement';

export const CREATE_SURVIVOR = "CREATE_SURVIVOR";
export const CHANGE_GENDER = "CHANGE_GENDER";
export const SURVIVOR_NAME_CHANGE = "SURVIVOR_NAME_CHANGE";
export const FATHER_SET = "FATHER_SET";
export const MOTHER_SET = "MOTHER_SET";
export const SKIP_NEXT_HUNT_TOGGLE = "SKIP_NEXT_HUNT_TOGGLE";
export const RETIRED_TOGGLE = "RETIRED_TOGGLE";
export const SET_HAS_REROLL = "SET_HAS_REROLL";
export const REROLL_TOGGLE = "REROLL_TOGGLE";
export const DEAD_SET = "DEAD_SET";
export const SET_UNAVAILABLE = "SET_UNAVAILABLE";
export const SURVIVOR_SURVIVAL_SET = "SET_SURVIVOR_SURVIVAL_SET";
export const SURVIVOR_SURVIVAL_INCREASE = "SURVIVOR_SURVIVAL_INCREASE";
export const SURVIVOR_SURVIVAL_DECREASE = "SURVIVOR_SURVIVAL_DECREASE";
export const CAN_SPEND_SURVIVAL_TOGGLE = "CAN_SPEND_SURVIVAL_TOGGLE";
export const SET_CAN_GAIN_SURVIVAL = "SET_CAN_GAIN_SURVIVAL";
export const SURVIVAL_ACTION_SET = "SURVIVAL_ACTION_SET";
export const INSANITY_SET = "INSANITY_SET";
export const SURVIVOR_INSANITY_INCREASE = "SURVIVOR_INSANITY_INCREASE";
export const SURVIVOR_INSANITY_DECREASE = "SURVIVOR_INSANITY_DECREASE";
export const BRAIN_INJURY_TOGGLE = "BRAIN_INJURY_TOGGLE";
export const XP_SET = "XP_SET";
export const XP_INCREASE = "XP_INCREASE";
export const XP_DECREASE = "XP_DECREASE";
export const COURAGE_SET = "COURAGE_SET";
export const COURAGE_INCREASE = "COURAGE_INCREASE";
export const COURAGE_DECREASE = "COURAGE_DECREASE";
export const UNDERSTANDING_SET = "UNDERSTANDING_SET";
export const UNDERSTANDING_INCREASE = "UNDERSTANDING_INCREASE";
export const UNDERSTANDING_DECREASE = "UNDERSTANDING_DECREASE";
export const ARMOR_SET = "ARMOR_SET";
export const ARMOR_ALL_INCREASE = "ARMOR_ALL_INCREASE";
export const ARMOR_ALL_DECREASE = "ARMOR_ALL_DECREASE";
export const ARMOR_RESET = "ARMOR_RESET";
export const WOUND_TOGGLE = "WOUND_TOGGLE";
export const SURVIVOR_STAT_SET = "SURVIVOR_STAT_SET";
export const SURVIVOR_TEMP_STAT_SET = "SURVIVOR_TEMP_STAT_SET";
export const ADD_STATS_TO_ALL_SURVIVORS = "ADD_STATS_TO_ALL_SURVIVORS";
export const REMOVE_STATS_FROM_ALL_SURVIVORS = "REMOVE_STATS_FROM_ALL_SURVIVORS";
export const WEAPON_PROF_INCREASE = "WEAPON_PROF_INCREASE";
export const WEAPON_PROF_DECREASE = "WEAPON_PROF_DECREASE";
export const WEAPON_PROF_CHANGE = "WEAPON_PROF_CHANGE";
export const CONSTELLATION_MAP_TOGGLE = "CONSTELLATION_MAP_TOGGLE";
export const CONSTELLATION_SET = "CONSTELLATION_SET";
export const FIGHTING_ART_ADD = "FIGHTING_ART_ADD";
export const FIGHTING_ART_REMOVE = "FIGHTING_ART_REMOVE";
export const DISORDER_ADD = "DISORDER_ADD";
export const DISORDER_REMOVE = "DISORDER_REMOVE";
export const ABILITY_ADD = "ABILITY_ADD";
export const ABILITY_REMOVE = "ABILITY_REMOVE";
export const SEVERE_INJURY_ADD = "SEVERE_INJURY_ADD";
export const SEVERE_INJURY_REMOVE = "SEVERE_INJURY_REMOVE";
export const NOTE_EDIT_SURVIVOR = "NOTE_EDIT_SURVIVOR";
export const AFFINITY_SET = "AFFINITY_SET";
export const CURSED_GEAR_ADD = "CURSED_GEAR_ADD";
export const CURSED_GEAR_REMOVE = "CURSED_GEAR_REMOVE";
export const DEPARTING_COLOR_SET = "DEPARTING_COLOR_SET";
export const DEPART_TOGGLE = "DEPART_TOGGLE";
export const RETURN_SURVIVORS = "RETURN_SURVIVORS";
export const INIT_SURVIVORS = "INIT_SURVIVORS";
export const IMPORT_SURVIVORS = "IMPORT_SURVIVORS";
export const COPY_SURVIVOR = "COPY_SURVIVOR";
export const DELETE_SURVIVOR = "DELETE_SURVIVOR";
export const SWAP_ABILITY = "SWAP_ABILITY";
export const SET_EPITHET = "SET_EPITHET";
export const SET_PINNED = "SET_PINNED";
export const SURVIVOR_ADD_ACTION_LOG = "SURVIVOR_ADD_ACTION_LOG";
export const LOADOUT_CHANGE_GEAR = "LOADOUT_CHANGE_GEAR";
export const LOADOUT_CHANGE_NAME = "LOADOUT_CHANGE_NAME";
export const LOADOUT_SAVE = "LOADOUT_SAVE";
export const LOADOUT_SET = "LOADOUT_SET";
export const SET_ARMOR_SET = "SET_ARMOR_SET";
export const SWAP_LOADOUT_SPACE = "SWAP_LOADOUT_SPACE";
export const SET_WEAPON_PROFICIENCY_EARNED = "SET_WEAPON_PROFICIENCY_EARNED";
export const SET_LOADOUT_ATTRIBUTES = "SET_LOADOUT_ATTRIBUTES";
export const APPLY_LOADOUT_STATS = "APPLY_LOADOUT_STATS";
export const SET_CAUSE_OF_DEATH = "SET_CAUSE_OF_DEATH";

import {sendServerPacket, sendClientPacket} from './network';
import { SURVIVOR_MESSAGE_SURVIVAL_INCREASE, SURVIVOR_MESSAGE_SURVIVAL_DECREASE, SURVIVOR_MESSAGE_NAME_CHANGE, SURVIVOR_MESSAGE_FATHER_SET, SURVIVOR_MESSAGE_MOTHER_SET, SURVIVOR_MESSAGE_SKIP_HUNT_YES, SURVIVOR_MESSAGE_SKIP_HUNT_NO, SURVIVOR_MESSAGE_GENDER_MALE, SURVIVOR_MESSAGE_GENDER_FEMALE, SURVIVOR_MESSAGE_HAS_REROLL_YES, SURVIVOR_MESSAGE_HAS_REROLL_NO, SURVIVOR_MESSAGE_REROLL_NO, SURVIVOR_MESSAGE_REROLL_YES, SURVIVOR_MESSAGE_RETIRED_NO, SURVIVOR_MESSAGE_RETIRED_YES, SURVIVOR_MESSAGE_DEAD_YES, SURVIVOR_MESSAGE_DEAD_NO, SURVIVOR_MESSAGE_UNAVAILABLE_YES, SURVIVOR_MESSAGE_UNAVAILABLE_NO, SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_YES, SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_NO, SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_YES, SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_NO, SURVIVOR_MESSAGE_SURVIVAL_ACTION_YES, SURVIVOR_MESSAGE_SURVIVAL_ACTION_NO, SURVIVOR_MESSAGE_INSANITY_INCREASE, SURVIVOR_MESSAGE_INSANITY_DECREASE, SURVIVOR_MESSAGE_BRAIN_INJURY_NO, SURVIVOR_MESSAGE_BRAIN_INJURY_YES, SURVIVOR_MESSAGE_XP_INCREASE, SURVIVOR_MESSAGE_XP_DECREASE, SURVIVOR_MESSAGE_COURAGE_INCREASE, SURVIVOR_MESSAGE_COURAGE_DECREASE, SURVIVOR_MESSAGE_UNDERSTANDING_INCREASE, SURVIVOR_MESSAGE_UNDERSTANDING_DECREASE, SURVIVOR_MESSAGE_ARMOR_INCREASE, SURVIVOR_MESSAGE_ARMOR_DECREASE, SURVIVOR_MESSAGE_ARMOR_ALL_INCREASE, SURVIVOR_MESSAGE_ARMOR_ALL_DECREASE, SURVIVOR_MESSAGE_ARMOR_RESET, SURVIVOR_MESSAGE_LIGHT_WOUND_YES, SURVIVOR_MESSAGE_HEAVY_WOUND_NO, SURVIVOR_MESSAGE_HEAVY_WOUND_YES, SURVIVOR_MESSAGE_LIGHT_WOUND_NO, SURVIVOR_MESSAGE_STAT_INCREASED, SURVIVOR_MESSAGE_STAT_DECREASED, SURVIVOR_MESSAGE_TEMP_STAT_INCREASED, SURVIVOR_MESSAGE_TEMP_STAT_DECREASED, SURVIVOR_MESSAGE_WEAPON_PROF_INCREASED, SURVIVOR_MESSAGE_WEAPON_PROF_DECREASED, SURVIVOR_MESSAGE_WEAPON_PROF_SET, SURVIVOR_MESSAGE_SET_CONSTELLATION, SURVIVOR_MESSAGE_ADD_FIGHTING_ART, SURVIVOR_MESSAGE_REMOVE_FIGHTING_ART, SURVIVOR_MESSAGE_ADD_DISORDER, SURVIVOR_MESSAGE_REMOVE_DISORDER, SURVIVOR_MESSAGE_ADD_ABILITY, SURVIVOR_MESSAGE_REMOVE_ABILITY, SURVIVOR_MESSAGE_ADD_SEVERE_INJURY, SURVIVOR_MESSAGE_REMOVE_SEVERE_INJURY, SURVIVOR_MESSAGE_EDIT_NOTE, SURVIVOR_MESSAGE_AFFINITY_LOST, SURVIVOR_MESSAGE_AFFINITY_GAINED, SURVIVOR_MESSAGE_ADD_CURSED_GEAR, SURVIVOR_MESSAGE_REMOVE_CURSED_GEAR, SURVIVOR_MESSAGE_COLOR_SET, SURVIVOR_MESSAGE_DEPARTING_YES, SURVIVOR_MESSAGE_DEPARTING_NO, SURVIVOR_MESSAGE_NEW_LOADOUT, SURVIVOR_MESSAGE_LOADOUT_NAME_CHANGED, MESSAGE_LOADOUT_SAVED, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED, SURVIVOR_MESSAGE_LOADOUT_SET, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_NULL, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED_NULL, SURVIVOR_MESSAGE_ARMOR_SET_SET, SURVIVOR_MESSAGE_ARMOR_SET_REMOVE, SURVIVOR_MESSAGE_WEAPON_PROF_EARNED, SURVIVOR_MESSAGE_WEAPON_PROF_UNEARNED, SURVIVOR_MESSAGE_CAUSE_OF_DEATH } from '../helpers/LogMessages';
import { getWeaponProfConfig, getFightingArtConfig, getDisorderConfig, getAbilityConfig, getSevereInjuryConfig, getGearConfig, getArmorConfig, getMilestoneConfig, getMilestoneConditionValue } from '../helpers/helpers';
import { setMilestoneValue } from './settlement';
import { updateHostContactTime } from './global';
import { getDepartingSurvivors } from '../selectors/survivors';
import { t } from '../helpers/intl';

// container object holding names of functions (for dispatches)
// organized roughly in order of where it appears in the survivor's section of the app
const survivorActions = {
    "createSurvivor": createSurvivor,
    "changeGender": changeGender,
    "changeName": changeName,
    "setFather": setFather,
    "setMother": setMother,
    "toggleSkipNextHunt": toggleSkipNextHunt,
    "toggleRetired": toggleRetired,
    "setHasReroll": setHasReroll,
    "toggleReroll": toggleReroll,
    "setDead": setDead,
    "setUnavailable": setUnavailable,
    "setSurvival": setSurvival,
    "increaseSurvival": increaseSurvival,
    "decreaseSurvival": decreaseSurvival,
    "toggleCanSpendSurvival": toggleCanSpendSurvival,
    "setCanGainSurvival": setCanGainSurvival,
    "setSurvivalAction": setSurvivalAction,
    "setInsanity": setInsanity,
    "increaseInsanity": increaseInsanity,
    "decreaseInsanity": decreaseInsanity,
    "toggleBrainInjury": toggleBrainInjury,
    "setXp": setXp,
    "increaseXp": increaseXp,
    "decreaseXp": decreaseXp,
    "setCourage": setCourage,
    "increaseCourage": increaseCourage,
    "decreaseCourage": decreaseCourage,
    "setUnderstanding": setUnderstanding,
    "increaseUnderstanding": increaseUnderstanding,
    "decreaseUnderstanding": decreaseUnderstanding,
    "setArmor": setArmor,
    "increaseAllArmor": increaseAllArmor,
    "decreaseAllArmor": decreaseAllArmor,
    "resetArmor": resetArmor,
    "toggleWound": toggleWound,
    "setStat": setStat,
    "setTempStat": setTempStat,
    "addStatsToAllSurvivors": addStatsToAllSurvivors,           // not currently in use
    "removeStatsFromAllSurvivors": removeStatsFromAllSurvivors, // not currently in use
    "increaseWeaponProf": increaseWeaponProf,
    "decreaseWeaponProf": decreaseWeaponProf,
    "changeWeaponProf": changeWeaponProf,
    "toggleConstellationMap": toggleConstellationMap,
    "setConstellation": setConstellation,
    "addFightingArt": addFightingArt,
    "removeFightingArt": removeFightingArt,
    "addDisorder": addDisorder,
    "removeDisorder": removeDisorder,
    "addAbility": addAbility,
    "removeAbility": removeAbility,
    "addSevereInjury": addSevereInjury,
    "removeSevereInjury": removeSevereInjury,
    "editNote": editNote,
    "setAffinity": setAffinity,
    "addCursedGear": addCursedGear,
    "removeCursedGear": removeCursedGear,
    "setDepartingColor": setDepartingColor,
    "toggleDeparting": toggleDeparting,
    "returnSurvivors": returnSurvivors,
    "swapAbility": swapAbility,
    "copySurvivor": copySurvivor,
    "setEpithet": setEpithet,
    "setLoadoutGear": setLoadoutGear,
    "setLoadoutName": setLoadoutName,
    "saveLoadout": saveLoadout,
    "setLoadout": setLoadout,
    "setArmorSet": setArmorSet,
    "swapLoadoutSpace": swapLoadoutSpace,
    "setWeaponProficiencyEarned": setWeaponProficiencyEarned,
    "setLoadoutAttributes": setLoadoutAttributes,
    "setCauseOfDeath": setCauseOfDeath,
}

export function survivorActionWrapper(action, args) {
    return (dispatch, getState) => {
        if(!survivorActions[action] && action !== "doBatchAction") {
            return;
        }

        let state = getState();
        let isHost = state.networkReducer.server !== null;
        let isClient = state.networkReducer.client !== null;
        let share = state.networkReducer.shareId !== null;

        if(action === "doBatchAction") {
            if(args.ids.settlementId !== state.networkReducer.shareId) {
                share = false;
            }
        } else if(state.networkReducer.shareId !== args[0]) {
            share = false;
        }

        if(share) {
            if(isHost) {
                dispatch(sendServerPacket({reducer: 'survivorReducer', action, args}));
                if(action === "doBatchAction") {
                    // batch actions pass args as an object, and are handled differently
                    dispatch(doBatchAction(args.action, args.ids, args.args));
                } else {
                    dispatch(survivorActions[action](...args));
                }
            } else if(isClient) {
                dispatch(sendClientPacket({reducer: 'survivorReducer', action, args}));
            }
        } else {
            if(action === "doBatchAction") {
                // batch actions pass args as an object, and are handled differently
                dispatch(doBatchAction(args.action, args.ids, args.args));
            } else {
                dispatch(survivorActions[action](...args));
            }
        }
    }
}

export function doSurvivorHostAction(action, args) {
    return (dispatch) => {
        if(action === "doBatchAction") {
            dispatch(doBatchAction(args.action, args.ids, args.args));
        } else {
            dispatch(survivorActions[action](...args));
        }
        dispatch(updateHostContactTime());
    };
}

function doBatchAction(action, ids, args) {
    return (dispatch, getState) => {
        // loop through each survivor (IDs stored in args[1] as an array)
        for(let i = 0; i < ids.survivorIds.length; i++) {
            dispatch(survivorActions[action](ids.settlementId, ids.survivorIds[i], ...args));
        }
    }
}

// functions appear in the same order as const survivorActions[]
// or, in roughly the same order as the survivor's section of the app
export function createSurvivor(settlementId, id, name, gender, parents, bonuses, numSurvivors = 0, campaign = "PotLantern") {
    return (dispatch, getState) => {
        let state = getState();
        // new life principle check
        if(
            state.settlementReducer.settlements[settlementId].milestones.firstBirth &&
            state.settlementReducer.settlements[settlementId].milestones.firstBirth.complete === false &&
            (parents.mother !== "" && parents.father !== "")
        ) {
            MilestoneConfig = getMilestoneConfig();
            dispatch(setMilestoneValue(settlementId, "firstBirth", "firstBirth", 1, getMilestoneConditionValue("firstBirth", "firstBirth", MilestoneConfig)));
        }

        // increase population
        if(numSurvivors >= state.settlementReducer.settlements[settlementId].population || (parents.father !== "" && parents.mother !== "")) {
            dispatch(increasePopulation(settlementId));
        }

        dispatch({type: CREATE_SURVIVOR, settlementId, id, name, gender, parents, bonuses, numSurvivors, campaign});
    }
}

export function changeGender(settlementId, id, gender) {
    return (dispatch) => {
        if(gender === "M") {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_GENDER_MALE));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_GENDER_FEMALE));
        }
        dispatch({type: CHANGE_GENDER, settlementId, id, gender});
    }
}

export function changeName(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_NAME_CHANGE, {name}));
        dispatch({type: SURVIVOR_NAME_CHANGE, settlementId, id, name});
    }
}

export function setFather(settlementId, id, father) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_FATHER_SET, {name: getState().survivorReducer.survivors[settlementId][father].name}));
        dispatch({type: FATHER_SET, settlementId, id, father});
    }
}

export function setMother(settlementId, id, mother) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_MOTHER_SET, {name: getState().survivorReducer.survivors[settlementId][mother].name}));
        dispatch({type: MOTHER_SET, settlementId, id, mother});
    }
}

export function toggleSkipNextHunt(settlementId, id) {
    return (dispatch, getState) => {
        if(getState().survivorReducer.survivors[settlementId][id].skipNextHunt === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SKIP_HUNT_NO));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SKIP_HUNT_YES));
        }

        dispatch({type: SKIP_NEXT_HUNT_TOGGLE, settlementId, id});
    }
}

export function toggleRetired(settlementId, id) {
    return (dispatch, getState) => {
        if(getState().survivorReducer.survivors[settlementId][id].retired === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_RETIRED_NO));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_RETIRED_YES));
        }

        dispatch({type: RETIRED_TOGGLE, settlementId, id});
    }
}

export function setHasReroll(settlementId, id, hasReroll) {
    return (dispatch) => {
        if(hasReroll === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_HAS_REROLL_YES));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_HAS_REROLL_NO));
        }

        dispatch({type: SET_HAS_REROLL, settlementId, id, hasReroll});
    }
}

export function toggleReroll(settlementId, id) {
    return (dispatch, getState) => {
        if(getState().survivorReducer.survivors[settlementId][id].reroll === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_REROLL_NO));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_REROLL_YES));
        }

        dispatch({type: REROLL_TOGGLE, settlementId, id});
    }
}

export function setDead(settlementId, id, dead) {
    return (dispatch, getState) => {
        if(dead === true) {
            const state = getState();
            if(
                state.settlementReducer.settlements[settlementId].milestones.firstDeath &&
                state.settlementReducer.settlements[settlementId].milestones.firstDeath.complete === false
            ) {
                MilestoneConfig = getMilestoneConfig();
                dispatch(setMilestoneValue(settlementId, "firstDeath", "firstDeath", 1, getMilestoneConditionValue("firstDeath", "firstDeath", MilestoneConfig)));
            }
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_DEAD_YES));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_DEAD_NO));
        }

        dispatch({type: DEAD_SET, settlementId, id, dead});
    }
}

export function setUnavailable(settlementId, id, unavailable) {
    return (dispatch) => {
        if(unavailable === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_UNAVAILABLE_YES));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_UNAVAILABLE_NO));
        }

        dispatch({type: SET_UNAVAILABLE, settlementId, id, unavailable});
    }
}

export function setSurvival(settlementId, id, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].survival;
        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SURVIVAL_INCREASE, {num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SURVIVAL_DECREASE, {num: val}));
        }
        dispatch({type: SURVIVOR_SURVIVAL_SET, settlementId, id, val});
    }
}

// used for batch actions
export function increaseSurvival(settlementId, id) {
    return (dispatch, getState) => {
        let state = getState();
        let limit = state.settlementReducer.settlements[settlementId].survivalLimit;
        let survival = state.survivorReducer.survivors[settlementId][id].survival;

        // prevent survivor's survival from exceeding the limit
        if(survival >= limit) {
            return;
        }

        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SURVIVAL_INCREASE, {num: state.survivorReducer.survivors[settlementId][id].survival + 1}));
        dispatch({type: SURVIVOR_SURVIVAL_INCREASE, settlementId, id});
    }
}

// used for batch actions
export function decreaseSurvival(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SURVIVAL_DECREASE, {num: getState().survivorReducer.survivors[settlementId][id].survival + 1}));
        dispatch({type: SURVIVOR_SURVIVAL_DECREASE, settlementId, id});
    }
}

export function toggleCanSpendSurvival(settlementId, id) {
    return (dispatch, getState) => {
        if(getState().survivorReducer.survivors[settlementId][id].canSpendSurvival === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_NO));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_YES));
        }

        dispatch({type: CAN_SPEND_SURVIVAL_TOGGLE, settlementId, id});
    }
}

export function setCanGainSurvival(settlementId, id, val) {
    return (dispatch) => {
        if(val === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_YES));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_NO));
        }

        dispatch({type: SET_CAN_GAIN_SURVIVAL, settlementId, id, val});
    }
}

export function setSurvivalAction(settlementId, id, action, value) {
    return (dispatch) => {
        if(value === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SURVIVAL_ACTION_YES, {action}));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SURVIVAL_ACTION_NO, {action}));
        }

        dispatch({type: SURVIVAL_ACTION_SET, settlementId, id, action, value});
    }
}

export function setInsanity(settlementId, id, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].insanity.value;

        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_INSANITY_INCREASE, {num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_INSANITY_DECREASE, {num: val}));
        }

        dispatch({type: INSANITY_SET, settlementId, id, val});
    }
}

// used for batch actions
export function increaseInsanity(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_INSANITY_INCREASE, {num: getState().survivorReducer.survivors[settlementId][id].insanity.value + 1}));
        dispatch({type: SURVIVOR_INSANITY_INCREASE, settlementId, id});
    }
}

// used for batch actions
export function decreaseInsanity(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_INSANITY_DECREASE, {num: getState().survivorReducer.survivors[settlementId][id].insanity.value -1}));
        dispatch({type: SURVIVOR_INSANITY_DECREASE, settlementId, id});
    }
}

export function toggleBrainInjury(settlementId, id) {
    return (dispatch, getState) => {
        if(getState().survivorReducer.survivors[settlementId][id].insanity.light === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_BRAIN_INJURY_NO));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_BRAIN_INJURY_YES));
        }

        dispatch({type: BRAIN_INJURY_TOGGLE, settlementId, id});
    }
}

export function setXp(settlementId, id, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].xp;

        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_XP_INCREASE, {num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_XP_DECREASE, {num: val}));
        }

        dispatch({type: XP_SET, settlementId, id, val});
    }
}

// used for batch actions
export function increaseXp(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_XP_INCREASE, {num: getState().survivorReducer.survivors[settlementId][id].xp + 1}));
        dispatch({type: XP_INCREASE, settlementId, id});
    }
}

// used for batch actions
export function decreaseXp(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_XP_DECREASE, {num: getState().survivorReducer.survivors[settlementId][id].xp - 1}));
        dispatch({type: XP_DECREASE, settlementId, id});
    }
}

export function setCourage(settlementId, id, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].courage;

        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_COURAGE_INCREASE, {num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_COURAGE_DECREASE, {num: val}));
        }

        dispatch({type: COURAGE_SET, settlementId, id, val});
    }
}

// used for batch actions
export function increaseCourage(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_COURAGE_INCREASE, {num: getState().survivorReducer.survivors[settlementId][id].courage + 1}));
        dispatch({type: COURAGE_INCREASE, settlementId, id});
    }
}

// used for batch actions
export function decreaseCourage(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_COURAGE_DECREASE, {num: getState().survivorReducer.survivors[settlementId][id].courage - 1}));
        dispatch({type: COURAGE_DECREASE, settlementId, id});
    }
}

export function setUnderstanding(settlementId, id, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].understanding;

        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_UNDERSTANDING_INCREASE, {num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_UNDERSTANDING_DECREASE, {num: val}));
        }

        dispatch({type: UNDERSTANDING_SET, settlementId, id, val});
    }
}

// used for batch actions
export function increaseUnderstanding(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_UNDERSTANDING_INCREASE, {num: getState().survivorReducer.survivors[settlementId][id].understanding + 1}));
        dispatch({type: UNDERSTANDING_INCREASE, settlementId, id});
    }
}

// used for batch actions
export function decreaseUnderstanding(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_UNDERSTANDING_DECREASE, {num: getState().survivorReducer.survivors[settlementId][id].understanding - 1}));
        dispatch({type: UNDERSTANDING_DECREASE, settlementId, id});
    }
}

export function setArmor(settlementId, id, location, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].armor[location].value;

        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ARMOR_INCREASE, {location: location.charAt(0).toUpperCase() + location.slice(1), num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ARMOR_DECREASE, {location: location.charAt(0).toUpperCase() + location.slice(1), num: val}));
        }

        dispatch({type: ARMOR_SET, settlementId, id, location, val});
    }
}

export function increaseAllArmor(settlementId, id) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ARMOR_ALL_INCREASE));
        dispatch({type: ARMOR_ALL_INCREASE, settlementId, id});
    }
}

export function decreaseAllArmor(settlementId, id) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ARMOR_ALL_DECREASE));
        dispatch({type: ARMOR_ALL_DECREASE, settlementId, id});
    }
}

export function resetArmor(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ARMOR_RESET));
        const state = getState();
        const hasShieldMastery = state.settlementReducer.settlements[settlementId].innovations.indexOf("masteryShield") !== -1;
        dispatch({type: ARMOR_RESET, settlementId, id, hasShieldMastery});
    }
}

export function toggleWound(settlementId, id, location, severity) {
    return (dispatch, getState) => {
        if(severity === "light") {
            if(getState().survivorReducer.survivors[settlementId][id].armor[location].light === true) {
                dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LIGHT_WOUND_NO, {location: location.charAt(0).toUpperCase() + location.slice(1)}));
            } else {
                dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LIGHT_WOUND_YES, {location: location.charAt(0).toUpperCase() + location.slice(1)}));
            }
        } else if(severity === "heavy") {
            if(getState().survivorReducer.survivors[settlementId][id].armor[location].heavy === true) {
                dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_HEAVY_WOUND_NO, {location: location.charAt(0).toUpperCase() + location.slice(1)}));
            } else {
                dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_HEAVY_WOUND_YES, {location: location.charAt(0).toUpperCase() + location.slice(1)}));
            }
        }

        dispatch({type: WOUND_TOGGLE, settlementId, id, location, severity});
    }
}

export function setStat(settlementId, id, stat, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].attributes[stat];

        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_STAT_INCREASED, {stat, num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_STAT_DECREASED, {stat, num: val}));
        }

        dispatch({type: SURVIVOR_STAT_SET, settlementId, id, stat, val});
    }
}

export function setTempStat(settlementId, id, stat, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].tempAttributes[stat];

        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_TEMP_STAT_INCREASED, {stat, num: val}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_TEMP_STAT_DECREASED, {stat, num: val}));
        }
        dispatch({type: SURVIVOR_TEMP_STAT_SET, settlementId, id, stat, val});
    }
}

export function addStatsToAllSurvivors(settlementId, stats) {
    return {type: ADD_STATS_TO_ALL_SURVIVORS, settlementId, stats};
}

export function removeStatsFromAllSurvivors(settlementId, stats) {
    return {type: REMOVE_STATS_FROM_ALL_SURVIVORS, settlementId, stats};
}

export function increaseWeaponProf(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_WEAPON_PROF_INCREASED, {num: getState().survivorReducer.survivors[settlementId][id].weaponProficiency.rank + 1}));
        dispatch({type: WEAPON_PROF_INCREASE, settlementId, id});

        let state = getState();
        let ProficiencyConfig = getWeaponProfConfig();
        let rank = state.survivorReducer.survivors[settlementId][id].weaponProficiency.rank;
        let weapon = state.survivorReducer.survivors[settlementId][id].weaponProficiency.weapon;
        if( rank >= 8 &&
            ProficiencyConfig[weapon] &&
            "ranks" in ProficiencyConfig[weapon] &&
            ProficiencyConfig[weapon].ranks[rank] &&
            ProficiencyConfig[weapon].ranks[rank]["title"] === "Weapon Mastery"
        ) {
            let innovations = ProficiencyConfig[weapon].ranks[rank]["innovations"];
            // add weapon's innovation to the settlement, if there are ranks and innovations
            // seems to now be assessing rank at its proper rank, at this point
            if( ProficiencyConfig[weapon].ranks[rank] &&
                ProficiencyConfig[weapon].ranks[rank]["innovations"]) {

                // iterate through each innovation to add
                for(let i = 0; i < innovations.length; i++) {
                    dispatch(addInnovation(settlementId, innovations[i]));
                }

                SimpleToast.show(t("Achieved %mastery% mastery, added innovation to settlement!", {mastery: ProficiencyConfig[weapon].title}));
            }
        }
    }
}

export function decreaseWeaponProf(settlementId, id) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_WEAPON_PROF_DECREASED, {num: getState().survivorReducer.survivors[settlementId][id].weaponProficiency.rank - 1}));
        dispatch({type: WEAPON_PROF_DECREASE, settlementId, id});
    }
}

export function changeWeaponProf(settlementId, id, name) {
    return (dispatch) => {
        let config = getWeaponProfConfig();
        // saw some crashes here so just checking
        let prof = config[name] ? config[name].title : name;
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_WEAPON_PROF_SET, {prof}));
        dispatch({type: WEAPON_PROF_CHANGE, settlementId, id, name});
    }
}

export function toggleConstellationMap(settlementId, id, row, col) {
    return (dispatch) => {
        dispatch({type: CONSTELLATION_MAP_TOGGLE, settlementId, id, row, col});
    }
}

export function setConstellation(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_SET_CONSTELLATION, {name}));
        dispatch({type: CONSTELLATION_SET, settlementId, id, name});
    }
}

export function addFightingArt(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ADD_FIGHTING_ART, {name: getFightingArtConfig()[name].title}));
        dispatch({type: FIGHTING_ART_ADD, settlementId, id, name});
    }
}

export function removeFightingArt(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_REMOVE_FIGHTING_ART, {name: getFightingArtConfig()[name].title}));
        dispatch({type: FIGHTING_ART_REMOVE, settlementId, id, name});
    }
}

export function addDisorder(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ADD_DISORDER, {name: getDisorderConfig()[name].title}));
        dispatch({type: DISORDER_ADD, settlementId, id, name});
    }
}

export function removeDisorder(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_REMOVE_DISORDER, {name: getDisorderConfig()[name].title}));
        dispatch({type: DISORDER_REMOVE, settlementId, id, name});
    }
}

export function addAbility(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ADD_ABILITY, {name: getAbilityConfig()[name].title}));
        dispatch({type: ABILITY_ADD, settlementId, id, name});
    }
}

export function removeAbility(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_REMOVE_ABILITY, {name: getAbilityConfig()[name].title}));
        dispatch({type: ABILITY_REMOVE, settlementId, id, name});
    }
}

export function swapAbility(settlementId, id, oldAbil, newAbil) {
    return (dispatch) => {
        dispatch(removeAbility(settlementId, id, oldAbil));
        dispatch(addAbility(settlementId, id, newAbil));
    }
}

export function addSevereInjury(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ADD_SEVERE_INJURY, {name: getSevereInjuryConfig()[name].title}));
        dispatch({type: SEVERE_INJURY_ADD, settlementId, id, name});
    }
}

export function removeSevereInjury(settlementId, id, name) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_REMOVE_SEVERE_INJURY, {name: getSevereInjuryConfig()[name].title}));
        dispatch({type: SEVERE_INJURY_REMOVE, settlementId, id, name});
    }
}

export function editNote(settlementId, id, note) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_EDIT_NOTE, {note}));
        dispatch({type: NOTE_EDIT_SURVIVOR, settlementId, id, note});
    }
}

export function setAffinity(settlementId, id, color, val) {
    return (dispatch, getState) => {
        let currentVal = getState().survivorReducer.survivors[settlementId][id].affinities[color];
        if(currentVal < val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_AFFINITY_GAINED, {color}));
        } else if(currentVal > val) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_AFFINITY_LOST, {color}));
        }

        dispatch({type: AFFINITY_SET, settlementId, id, color, val});
    }
}
export function addCursedGear(settlementId, id, location, item) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ADD_CURSED_GEAR, {gear: getGearConfig()[location][item].title}));
        dispatch({type: CURSED_GEAR_ADD, settlementId, id, location, item});
    }
}

export function removeCursedGear(settlementId, id, location, item) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_REMOVE_CURSED_GEAR, {gear: getGearConfig()[location][item].title}));
        dispatch({type: CURSED_GEAR_REMOVE, settlementId, id, location, item});
    }
}

export function setDepartingColor(settlementId, id, color) {
    return (dispatch) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_COLOR_SET, {color}));
        dispatch({type: DEPARTING_COLOR_SET, settlementId, id, color});
    }
}

export function toggleDeparting(settlementId, id, ly) {
    return (dispatch, getState) => {
        if(getState().survivorReducer.survivors[settlementId][id].departing === true) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_DEPARTING_NO));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_DEPARTING_YES));
        }
        dispatch({type: DEPART_TOGGLE, settlementId, id, ly});
    }
}

export function returnSurvivors(settlementId) {
    return (dispatch, getState) => {
        let state = getState();
        const survivors = getDepartingSurvivors(state);
        if(
            state.settlementReducer.settlements[settlementId].currentShowdown &&
            state.settlementReducer.settlements[settlementId].currentShowdown.monster !== "theWatcher" &&
            state.settlementReducer.settlements[settlementId].currentShowdown.monster !== "goldSmokeKnight"
        ) {
            for(let i = 0; i < survivors.length; i++) {
                if(survivors[i].weaponProficiency.earned) {
                    dispatch(increaseWeaponProf(settlementId, survivors[i].id));
                }
            }
        }

        dispatch(setSettlementDeparting(settlementId, false));
        dispatch(setCurrentShowdown(settlementId, null));

        dispatch({type: RETURN_SURVIVORS, settlementId});
    }
}

// the rest of the functions are called directly, and don't appear in

export function initSurvivors(settlementId, createSurvivors = true, campaign = "PotLantern") {
    return (dispatch) => {
        dispatch({type: INIT_SURVIVORS, settlementId, createSurvivors, campaign});
    }
}

export function importSurvivors(settlementId, survivors) {
    return (dispatch) => {
        dispatch({type: IMPORT_SURVIVORS, settlementId, survivors});
    }
}

export function copySurvivor(settlementId, oldId, newId) {
    return (dispatch) => {
        dispatch({type: COPY_SURVIVOR, settlementId, oldId, newId});
    }
}

export function deleteSurvivor(settlementId, id) {
    return (dispatch) => {
        dispatch({type: DELETE_SURVIVOR, settlementId, id});
    }
}

export function setEpithet(settlementId, id, value) {
    return (dispatch) => {
        dispatch({type: SET_EPITHET, settlementId, id, value});
    }
}

export function setPinned(settlementId, id, value) {
    return (dispatch) => {
        dispatch({type: SET_PINNED, settlementId, id, value});
    }
}

export function setLoadoutGear(settlementId, id, x, y, gear) {
    return (dispatch, getState) => {
        const state = getState();
        const loadout = state.survivorReducer.survivors[settlementId][id].loadout;

        if(loadout.gear && gear) {
            const gearStorage = JSON.parse(JSON.stringify(state.settlementReducer.settlements[settlementId].storage.gear));
            for(let i = 0; i < 3; i++) {
                for(let j = 0; j < 3; j++) {
                    let used = loadout.gear[i][j];
                    if(used) {
                        if(used.location === gear.location && used.name === gear.name) {
                            if(gearStorage[used.location][used.name] === 1) {
                                dispatch({type: LOADOUT_CHANGE_GEAR, settlementId, id, x: i, y: j, gear: null});
                                break;
                            } else {
                                gearStorage[used.location][used.name] = gearStorage[used.location][used.name] - 1;
                            }
                        }
                    }
                }
            }
        }

        const GearConfig = getGearConfig();
        if(loadout.name) {
            if(!gear) {
                const prevGear = loadout.gear[x][y];
                if(prevGear) {
                    dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_NULL, {gearName: GearConfig[prevGear.location][prevGear.name].title, loadoutName: loadout.name}));
                }
            } else {
                dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET, {gearName: GearConfig[gear.location][gear.name].title, loadoutName: loadout.name}));
            }
        } else {
            if(!gear) {
                const prevGear = loadout.gear[x][y];
                if(prevGear) {
                    dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED_NULL, {gearName: GearConfig[prevGear.location][prevGear.name].title}));
                }
            } else {
                dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED, {gearName: GearConfig[gear.location][gear.name].title}));
            }
        }

        dispatch({type: LOADOUT_CHANGE_GEAR, settlementId, id, x, y, gear});
    }
}

export function setLoadoutName(settlementId, id, name) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LOADOUT_NAME_CHANGED, {name}));
        dispatch({type: LOADOUT_CHANGE_NAME, settlementId, id, name});
    }
}

export function saveLoadout(settlementId, id, loadout) {
    return (dispatch, getState) => {
        if(!loadout.name) {
            return;
        }

        if(!loadout.id) {
            loadout.id = Uuid.v4();
        }

        delete loadout.unsaved;

        dispatch(settlementAddActionLog(settlementId, MESSAGE_LOADOUT_SAVED, {name: loadout.name}));
        dispatch({type: LOADOUT_SAVE, settlementId, id, loadout});
    }
}

export function setLoadout(settlementId, id, loadout, unsaved = false) {
    return (dispatch, getState) => {
        if(Object.keys(loadout).length === 0) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_NEW_LOADOUT));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_LOADOUT_SET, {name: loadout.name}));
        }

        dispatch({type: LOADOUT_SET, settlementId, id, loadout, unsaved});
    }
}

export function setArmorSet(settlementId, id, name) {
    return (dispatch, getState) => {
        const ArmorConfig = getArmorConfig();
        if(ArmorConfig[name]) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ARMOR_SET_SET, {name: ArmorConfig[name].title}));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_ARMOR_SET_REMOVE));
        }
        dispatch({type: SET_ARMOR_SET, settlementId, id, name});
    }
}

export function setLoadoutAttributes(settlementId, id, attributes) {
    return (dispatch, getState) => {
        dispatch({type: SET_LOADOUT_ATTRIBUTES, settlementId, id, attributes});
    }
}

export function applyLoadoutStats(settlementId, id) {
    return (dispatch, getState) => {
        dispatch({type: APPLY_LOADOUT_STATS, settlementId, id});
    }
}

export function swapLoadoutSpace(settlementId, id, oldX, oldY, newX, newY) {
    return (dispatch, getState) => {
        dispatch({type: SWAP_LOADOUT_SPACE, settlementId, id, oldX, oldY, newX, newY});
    }
}

export function setWeaponProficiencyEarned(settlementId, id, value) {
    return (dispatch, getState) => {
        if(value) {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_WEAPON_PROF_EARNED));
        } else {
            dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_WEAPON_PROF_UNEARNED));
        }
        dispatch({type: SET_WEAPON_PROFICIENCY_EARNED, settlementId, id, value});
    }
}

export function setCauseOfDeath(settlementId, id, value) {
    return (dispatch, getState) => {
        dispatch(addActionLog(settlementId, id, SURVIVOR_MESSAGE_CAUSE_OF_DEATH, {cause: value}));
        dispatch({type: SET_CAUSE_OF_DEATH, settlementId, id, value});
    }
}

function addActionLog(settlementId, id, message, args = {}) {
    return (dispatch) => {
        const time = Date.now();
        let log = {t: time, m: message};
        if(args && Object.keys(args).length > 0) {
            log.a = args;
        }
        dispatch({type: SURVIVOR_ADD_ACTION_LOG, settlementId, id, log});
    }
}
