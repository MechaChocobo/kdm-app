export const VERSION_SET = "VERSION_SET";
export const SCHEMA_VALIDATE = "SCHEMA_VALIDATE";
export const SET_LOCALE = "SET_LOCALE";
export const SET_CAMERA = "SET_CAMERA";
export const SET_THEME = "SET_THEME";
export const SET_THEME_COLOR = "SET_THEME_COLOR";
export const RESET_THEME = "RESET_THEME";
export const SET_COLOR = "SET_COLOR";
export const RESET_COLORS = "RESET_COLORS";
export const SET_DEPART_VIEW = "SET_DEPART_VIEW";
export const SET_BACKUP_SETTINGS = "SET_BACKUP_SETTINGS";
export const IMPORT_SETTINGS = "IMPORT_SETTINGS";
export const SET_NUM_LINES_SURVIVOR_CARD = "SET_NUM_LINES_SURVIVOR_CARD";
export const SET_DATE_FORMAT = "SET_DATE_FORMAT";
export const SET_TAP_TO_DEPART = "SET_TAP_TO_DEPART";
export const SET_AUTO_HOST_SETTLEMENT = "SET_AUTO_HOST_SETTLEMENT";
export const SET_SURVIVOR_NAME_GENERATION = "SET_SURVIVOR_NAME_GENERATION";
export const SET_DEFAULT_IP_ADDRESS = "SET_DEFAULT_IP_ADDRESS";
export const SET_SHOW_DEPARTING_INDEX = "SET_SHOW_DEPARTING_INDEX";

export function setVersion(version) {
    return {type: VERSION_SET, version};
}

export function validateSchema(currentVersion) {
    return {type: SCHEMA_VALIDATE, currentVersion};
}

export function setLocale(locale) {
    return {type: SET_LOCALE, locale};
}

export function setCamera(camera) {
    return {type: SET_CAMERA, camera};
}

export function setTheme(theme) {
    return {type: SET_THEME, theme};
}

export function setThemeColor(themeType, colorType, color) {
    return {type: SET_THEME_COLOR, themeType, colorType, color};
}

export function resetTheme(themeType) {
    return {type: RESET_THEME, themeType};
}

export function setColor(colorType, color) {
    return {type: SET_COLOR, colorType, color};
}

export function resetColors() {
    return {type: RESET_COLORS};
}

export function setDepartingView(value) {
    return {type: SET_DEPART_VIEW, value};
}

export function setBackupSettings(value) {
    return {type: SET_BACKUP_SETTINGS, value};
}

export function importSettings(settings) {
    return {type: IMPORT_SETTINGS, settings};
}

export function setNumLinesSurvivorCard(value) {
    return {type: SET_NUM_LINES_SURVIVOR_CARD, value};
}

export function setDateFormat(value) {
    return {type: SET_DATE_FORMAT, value};
}

export function setTapToDepart(value) {
    return {type: SET_TAP_TO_DEPART, value};
}

export function setAutoHostSettlement(id) {
    return {type: SET_AUTO_HOST_SETTLEMENT, id};
}

export function setSurvivorNameGeneration(value) {
    return {type: SET_SURVIVOR_NAME_GENERATION, value};
}

export function setDefaultIpAddress(value) {
    return {type: SET_DEFAULT_IP_ADDRESS, value};
}

export function setShowDepartingIndex(value) {
    return {type: SET_SHOW_DEPARTING_INDEX, value};
}
