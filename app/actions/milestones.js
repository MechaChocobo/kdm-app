import SimpleToast from "react-native-simple-toast";
import { addMilestoneRewardsToSettlement, removeMilestoneRewardsFromSettlement } from "./settlement";
import { sendServerPacket, sendClientPacket } from "./network";
import { allConditionsComplete, getSettlementsWithCustomExpansion, getMilestoneConfig } from "../helpers/helpers";
import { updateHostContactTime } from "./global";

export const SET_MILESTONE_COMPLETE = "SET_MILESTONE_COMPLETE";
export const IMPORT_MILESTONES = "IMPORT_MILESTONES";
export const SET_CONDITION_VALUE = "SET_CONDITION_VALUE";
export const SET_SETTLEMENT_CONDITION_VALUE = "SET_SETTLEMENT_CONDITION_VALUE";
export const ADD_SETTLEMENT_TO_CONDITIONS = "ADD_SETTLEMENT_TO_CONDITIONS";
export const REMOVE_SETTLEMENT_FROM_CONDITIONS = "REMOVE_SETTLEMENT_FROM_CONDITIONS";

const actions = {
    'setGlobalMilestoneComplete' : setGlobalMilestoneComplete,
    'setConditionValue' : setConditionValue,
    'setSettlementConditionValue' : setSettlementConditionValue,
}

export function importMilestones(milestones = {}) {
    return (dispatch, getState) => {
        dispatch({type: IMPORT_MILESTONES, milestones});
    }
}

export function milestoneActionWrapper(action, args) {
    return (dispatch, getState) => {
        if(!actions[action]) {
            return;
        }

        const state = getState();
        const isHost = state.networkReducer.server !== null;
        const isClient = state.networkReducer.client !== null;
        const share = isHost || isClient;

        if(state.networkReducer.loading > 0) {
            return;
        }

        if(share) {
            if(isHost) {
                dispatch(sendServerPacket({reducer: 'milestones', action, args}));
                dispatch(actions[action](...args));
            } else if(isClient) {
                dispatch(sendClientPacket({reducer: 'milestones', action, args}));
            }
        } else {
            dispatch(actions[action](...args));
        }
    }
}

export function doMilestoneHostAction(action, args) {
    return (dispatch) => {
        dispatch(actions[action](...args));
        dispatch(updateHostContactTime());
    };
}

export function setConditionValue(milestoneId, id, value, target) {
    return (dispatch, getState) => {
        dispatch(_setConditionValue(milestoneId, id, value, target)).then((state) => {
            if(allConditionsComplete(state.milestones[milestoneId])) {
                dispatch(setGlobalMilestoneComplete(milestoneId, true));
            }
        });
    }
}

function _setConditionValue(milestoneId, id, value, target) {
    return (dispatch, getState) => {
        dispatch({type: SET_CONDITION_VALUE, milestoneId, id, value, target});
        return Promise.resolve(getState());
    }
}

export function setGlobalMilestoneComplete(id, value) {
    return (dispatch, getState) => {
        const state = getState();

        if(state.milestones[id].complete === true && value === true) {
            return;
        }

        const config = getMilestoneConfig();
        if(value === true) {
            if(config[id]) {
                if(config[id].reward) {
                    SimpleToast.show(config[id].reward, SimpleToast.LONG);
                }

                if(config[id].effects) {
                    dispatch(applyGlobalMilestoneEffects(id, config[id].effects));
                }
            }
        } if(value === false) {
            if(config[id].effects) {
                dispatch(applyGlobalMilestoneEffects(id, config[id].effects, true));
            }
        }

        dispatch({type: SET_MILESTONE_COMPLETE, id, value});
    }
}

function applyGlobalMilestoneEffects(id, effects, remove = false) {
    return (dispatch, getState) => {
        const MilestoneConfig = getMilestoneConfig();

        if(!MilestoneConfig[id]) {
            return;
        }

        const state = getState();
        const expansion = MilestoneConfig[id].expansion;

        const settlements = getSettlementsWithCustomExpansion(state.settlementReducer.settlements, expansion);

        for(let i = 0; i < settlements.length; i++) {
            if(remove === true) {
                dispatch(removeMilestoneRewardsFromSettlement(settlements[i].id, effects));
            } else {
                dispatch(addMilestoneRewardsToSettlement(settlements[i].id, effects));
            }
        }
    }
}

export function setSettlementConditionValue(milestoneId, id, settlementId, value, target) {
    return (dispatch, getState) => {
        dispatch(_setSettlementConditionValue(milestoneId, id, settlementId, value, target)).then((state) => {
            const complete = allConditionsComplete(state.milestones[milestoneId]);
            if(state.milestones[milestoneId].complete === false && complete) {
                dispatch(setGlobalMilestoneComplete(milestoneId, true));
            } else if(state.milestones[milestoneId].complete === true && !complete) {
                dispatch(setGlobalMilestoneComplete(milestoneId, false));
            }
        });
    }
}

function _setSettlementConditionValue(milestoneId, id, settlementId, value, target) {
    return (dispatch, getState) => {
        dispatch({type: SET_SETTLEMENT_CONDITION_VALUE, settlementId, milestoneId, id, value, target});
        return Promise.resolve(getState());
    }
}

export function addSettlementToConditions(id, milestoneId) {
    return (dispatch, getState) => {
        dispatch({type: ADD_SETTLEMENT_TO_CONDITIONS, id, milestoneId});
    }
}

export function removeSettlementFromConditions(id, milestoneId) {
    return (dispatch, getState) => {
        dispatch({type: REMOVE_SETTLEMENT_FROM_CONDITIONS, id, milestoneId});
    }
}
