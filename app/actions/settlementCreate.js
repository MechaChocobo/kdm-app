import Uuid from 'react-native-uuid';

import { addCustomExpansionsToSettlement, createSettlement, increasePopulation } from "./settlement";
import { initSurvivors } from "./survivors";
import { initTimeline } from "./timeline";
import { goBack, goToSettlement } from "./router";

export const SETTLEMENT_CREATE_SET_NAME = 'SETTLEMENT_CREATE_SET_NAME';
export const SETTLEMENT_CREATE_SET_EXPANSIONS = 'SETTLEMENT_CREATE_SET_EXPANSIONS';
export const SETTLEMENT_CREATE_SET_CUSTOM_EXPANSIONS = 'SETTLEMENT_CREATE_SET_CUSTOM_EXPANSIONS';
export const SETTLEMENT_CREATE_SET_CAMPAIGN = 'SETTLEMENT_CREATE_SET_CAMPAIGN';
export const SETTLEMENT_CREATE_SET_CREATE_SURVIVORS = 'SETTLEMENT_CREATE_SET_CREATE_SURVIVORS';
export const SETTLEMENT_CREATE_RESET = 'SETTLEMENT_CREATE_RESET';

export function setSettlementCreateName(name) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_CREATE_SET_NAME, name});
    }
}

export function setSettlementCreateExpansions(expansions) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_CREATE_SET_EXPANSIONS, expansions});
    }
}

export function setSettlementCreateCustomExpansions(expansions) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_CREATE_SET_CUSTOM_EXPANSIONS, expansions});
    }
}

export function setSettlementCreateCampaign(campaign) {
    return (dispatch, getState) => {
        let expansions = {...getState().expansions};

        if(campaign === "PotSun") {
            expansions.sunstalker = "cards";
        } else if (campaign === "PotStars") {
            expansions["dragon-king"] = "cards";
        } else if (campaign === "PotBloom") {
            expansions["flower-knight"] = "cards";
        }

        dispatch({type: SETTLEMENT_CREATE_SET_CAMPAIGN, campaign});
        dispatch(setSettlementCreateExpansions(expansions));
    }
}

export function setSettlementCreateSurvivors(value) {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_CREATE_SET_CREATE_SURVIVORS, value});
    }
}

export function settlementCreateReset() {
    return (dispatch, getState) => {
        dispatch({type: SETTLEMENT_CREATE_RESET});
    }
}

export function launchSettlementCreate() {
    return (dispatch, getState) => {
        let state = getState();
        let id = Uuid.v4();
        let expansions = state.settlementCreate.expansions;
        expansions.core = "all";
        dispatch(createSettlement(id, state.settlementCreate.name, expansions, state.settlementCreate.campaign));
        dispatch(addCustomExpansionsToSettlement(id, state.settlementCreate.selectedCustomExpansions));
        dispatch(initSurvivors(id, state.settlementCreate.createSurvivors, state.settlementCreate.campaign));
        if(state.settlementCreate.createSurvivors) {
            for (let i = 0; i < 4; i++) {
                dispatch(increasePopulation(id));
            }
        }
        dispatch(initTimeline(id, expansions, state.settlementCreate.campaign, state.settlementCreate.selectedCustomExpansions));
        dispatch(goBack()); // clears this from history so back goes to the settlement list
        dispatch(goToSettlement(id));
    }
}
