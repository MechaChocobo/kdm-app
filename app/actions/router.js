export const PAGE_UPDATE = 'PAGE_UPDATE';
export const GOTO_SETTLEMENT = 'GOTO_SETTLEMENT';
export const GOTO_SURVIVOR = 'GOTO_SURVIVOR';
export const GO_BACK = "GO_BACK";
export const GOTO_SURVIVOR_LIST = "GOTO_SURVIVOR_LIST";
export const SET_PAGE = "SET_PAGE";
export const SET_DEPARTING_SURVIVOR = "SET_DEPARTING_SURVIVOR";
export const GOTO_CUSTOM_EXPANSION = "GOTO_CUSTOM_EXPANSION";
export const RESET_NAV_STACK = "RESET_NAV_STACK";
export const SET_GOING_BACK = "SET_GOING_BACK";
export const SET_PAGE_TRANSITIONING = "SET_PAGE_TRANSITIONING";
export const SET_MENU_DRAWER_OPEN = "SET_MENU_DRAWER_OPEN";
export const GOTO_TIMELINE_LY = "GOTO_TIMELINE_LY";
export const SET_SURVIVOR_HOME_TAB = "SET_SURVIVOR_HOME_TAB";
export const ADD_TRANSITION_ACTION = "ADD_TRANSITION_ACTION";
export const CLEAR_TRANSITION_ACTIONS = "CLEAR_TRANSITION_ACTIONS";

export function changePage(page) {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: PAGE_UPDATE, page});
    }
}

export function goToSettlement(id) {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: GOTO_SETTLEMENT, id});
    }
}

export function goToSurvivor(id) {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: GOTO_SURVIVOR, id});
    }
}

export function goToTimelineLy(id) {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: GOTO_TIMELINE_LY, id});
    }
}

export function goBack() {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: GO_BACK});
    }
}

export function clearSurvivorIndex() {
    return (dispatch) => {
        dispatch({type: SURVIVOR_INDEX_CLEAR});
    }
}

export function goToSurvivorList(index) {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: GOTO_SURVIVOR_LIST, index});
    }
}

export function setPage(page) {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: SET_PAGE, page});
    }
}

export function setDepartingSurvivor(id) {
    return (dispatch) => {
        dispatch({type: SET_DEPARTING_SURVIVOR, id});
    }
}

export function goToCustomExpansion(id) {
    return (dispatch, getState) => {
        let state = getState();
        if(state.pageReducer.transitioning) {
            return;
        }
        dispatch({type: GOTO_CUSTOM_EXPANSION, id});
    }
}

export function resetNavigationStack() {
    return (dispatch) => {
        dispatch({type: RESET_NAV_STACK});
    }
}

export function setGoingBack(value) {
    return (dispatch) => {
        dispatch({type: SET_GOING_BACK, value});
    }
}

export function setPageTransitioning(value) {
    return (dispatch, getState) => {
        dispatch({type: SET_PAGE_TRANSITIONING, value});
        let state = getState();
        if(value === false && state.pageReducer.transitionCompleteActions.length > 0) {
            for(let i = 0; i < state.pageReducer.transitionCompleteActions.length; i++) {
                let action = state.pageReducer.transitionCompleteActions[i];
                let fn = action.fn;
                let args = action.args;
                dispatch(fn(...args));
            }

            dispatch(clearTransitionActions());
        }
    }
}

export function setMenuDrawerOpen(value) {
    return (dispatch) => {
        dispatch({type: SET_MENU_DRAWER_OPEN, value});
    }
}

export function setSurvivorHomeTab(index) {
    return (dispatch) => {
        dispatch({type: SET_SURVIVOR_HOME_TAB, index});
    }
}

export function addTransitionAction(fn, args = []) {
    return (dispatch) => {
        dispatch({type: ADD_TRANSITION_ACTION, fn, args});
    }
}

export function clearTransitionActions() {
    return (dispatch) => {
        dispatch({type: CLEAR_TRANSITION_ACTIONS});
    }
}
