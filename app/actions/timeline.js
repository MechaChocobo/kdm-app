export const INIT_TIMELINE = 'INIT_TIMELINE';
export const ADD_TO_TIMELINE = 'ADD_TO_TIMELINE';
export const REMOVE_FROM_TIMELINE = 'REMOVE_FROM_TIMELINE';
export const IMPORT_TIMELINE = 'IMPORT_TIMELINE';
export const ADD_YEAR = 'ADD_YEAR';
export const TIMELINE_ADD_ACTION_LOG = 'TIMELINE_ADD_ACTION_LOG';

import {sendServerPacket, sendClientPacket} from './network';
import { TIMELINE_MESSAGE_EVENT_ADDED, TIMELINE_MESSAGE_EVENT_REMOVED, TIMELINE_MESSAGE_YEAR_ADDED } from '../helpers/LogMessages';
import { getEventTitleForType, getEventConfigForType } from '../helpers/helpers';
import { updateHostContactTime } from './global';

// TODO
const timelineActions = {
    "addToTimeline": addToTimeline,
    "removeFromTimeline": removeFromTimeline,
    "addYear": addYear
};

export function timelineActionWrapper(action, args) {
    return (dispatch, getState) => {
        if(!timelineActions[action]) {
            return;
        }

        let state = getState();
        let isHost = state.networkReducer.server !== null;
        let isClient = state.networkReducer.client !== null;
        let share = state.networkReducer.shareId !== null;

        if(share && state.networkReducer.shareId !== args[0]) {
            share = false;
        }

        if(state.networkReducer.loading > 0) {
            return;
        }

        if(share) {
            if(isHost) {
                dispatch(sendServerPacket({reducer: 'timelineReducer', action, args}));
                dispatch(timelineActions[action](...args));
            } else if(isClient) {
                dispatch(sendClientPacket({reducer: 'timelineReducer', action, args}));
            }
        } else {
            dispatch(timelineActions[action](...args));
        }
    }
}

export function doTimelineHostAction(action, args) {
    return (dispatch) => {
        dispatch(timelineActions[action](...args));
        dispatch(updateHostContactTime());
    };
}

export function initTimeline(id, expansions = [{"core": "all"}], campaign = "PotLantern", customExpansions = {}) {
    return (dispatch, getState) => {
        let state = getState();
        let customTimelines = [];

        for (const id in customExpansions) {
            if(customExpansions[id] === "all") {
                customTimelines.push(state.customExpansions[id].timeline);
            }
        }

        dispatch({type: INIT_TIMELINE, id, campaign, expansions, customTimelines});
    }
}

export function addToTimeline(id, ly, eventType, value) {
    return (dispatch) => {
        const config = getEventConfigForType(eventType);
        let name = value;
        if(config[name]) {
            name = config[name].title;
        }

        dispatch(addActionLog(id, TIMELINE_MESSAGE_EVENT_ADDED, {type: getEventTitleForType(eventType), name, ly}));
        dispatch({type: ADD_TO_TIMELINE, id, ly, eventType, value});
    }
}

export function removeFromTimeline(id, ly, eventType, value) {
    return (dispatch) => {
        const config = getEventConfigForType(eventType);
        let name = value;
        if(config[name]) {
            name = config[name].title;
        }

        dispatch(addActionLog(id, TIMELINE_MESSAGE_EVENT_REMOVED, {type: getEventTitleForType(eventType), name, ly}));
        dispatch({type: REMOVE_FROM_TIMELINE, id, ly, eventType, value});
    }
}

export function importTimeline(id, timeline) {
    return (dispatch) => {
        dispatch({type: IMPORT_TIMELINE, id, timeline});
    }
}

export function addYear(id) {
    return (dispatch) => {
        dispatch(addActionLog(id, TIMELINE_MESSAGE_YEAR_ADDED));
        dispatch({type: ADD_YEAR, id});
    }
}

function addActionLog(id, message, args = {}, force = true) {
    return (dispatch) => {
        const time = Date.now();
        let log = {t: time, m: message};
        if(args && Object.keys(args).length > 0) {
            log.a = args;
        }
        dispatch({type: TIMELINE_ADD_ACTION_LOG, id, log, force});
    }
}
