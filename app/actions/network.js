import Toast from 'react-native-simple-toast';
import { isNetworked } from '../selectors/network';

export const SERVER_START = "SERVER_START";
export const SERVER_STOP = "SERVER_STOP";
export const SETUP_SOCKET = "SETUP_SOCKET";
export const SEND_SERVER_PACKET = "SEND_SERVER_PACKET";
export const SEND_CLIENT_PACKET = "SEND_CLIENT_PACKET";
export const CLIENT_START = "CLIENT_START";
export const CLIENT_STOP = "CLIENT_STOP";
export const SHARE_ID = "SHARE_ID";
export const NETWORK_LOCK = "NETWORK_LOCK";
export const NETWORK_UNLOCK = "NETWORK_UNLOCK";
export const LOADING_UP = "LOADING_UP";
export const LOADING_DOWN = "LOADING_DOWN";
export const SET_RECONNECT_FUNC = "SET_RECONNECT_FUNC";
export const RECONNECT_LOCKOUT = "RECONNECT_LOCKOUT";
export const ERROR_CONTROLS_OVERRIDE = "ERROR_CONTROLS_OVERRIDE";
export const SET_HOST_IP_ADDRESS = "SET_HOST_IP_ADDRESS";

export function startServer(server) {
    return (dispatch) => {
        dispatch({type: SERVER_START, server});
    }
}

export function stopServer() {
    return (dispatch) => {
        dispatch({type: SERVER_STOP});
    }
}

export function setupSocket(socket) {
    return (dispatch) => {
        dispatch({type: SETUP_SOCKET, socket});
    }
}

export function sendServerPacket(data) {
    return (dispatch) => {
        dispatch({type: SEND_SERVER_PACKET, data});
    }
}

export function sendClientPacket(data) {
    return (dispatch) => {
        dispatch({type: SEND_CLIENT_PACKET, data});
    }
}

export function clientStart(client, remoteIp) {
    return (dispatch) => {
        dispatch({type: CLIENT_START, client, remoteIp});
    }
}

export function clientStop(error = "") {
    return (dispatch) => {
        dispatch({type: CLIENT_STOP, error});
    }
}

export function setShareId(shareId) {
    return (dispatch) => {
        dispatch({type: SHARE_ID, shareId});
        return Promise.resolve();
    }
}

export function lockNetwork() {
    return (dispatch) => {
        dispatch({type: NETWORK_LOCK});
    }
}

export function unlockNetwork() {
    return (dispatch) => {
        dispatch({type: NETWORK_UNLOCK});
    }
}

export function loadingUp() {
    return (dispatch) => {
        dispatch({type: LOADING_UP});
    }
}

export function loadingDown() {
    return (dispatch) => {
        dispatch({type: LOADING_DOWN});
    }
}

export function setReconnectFunction(func) {
    return (dispatch) => {
        dispatch({type: SET_RECONNECT_FUNC, func});
    }
}

export function setReconnectLockout(val) {
    return (dispatch) => {
        dispatch({type: RECONNECT_LOCKOUT, val});
    }
}

export function setErrorControlsOverride(val) {
    return (dispatch) => {
        dispatch({type: ERROR_CONTROLS_OVERRIDE, val});
    }
}

export function setHostIpAddress(val) {
    return (dispatch) => {
        dispatch({type: SET_HOST_IP_ADDRESS, val});
    }
}

export function networkBlock(message) {
    return (dispatch, getState) => {
        if(isNetworked(getState())) {
            Toast.show(message);
            return true;
        }

        return false;
    }
}
