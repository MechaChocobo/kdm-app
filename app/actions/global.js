import { setHostIpAddress } from "./network";

export const RESET = "RESET";
export const UPDATE_HOST_CONTACT_TIME = "UPDATE_HOST_CONTACT_TIME";
export const SET_INTEGRITY_CHECK = "SET_INTEGRITY_CHECK";
export const DO_INTEGRITY_CHECK = "DO_INTEGRITY_CHECK";

const HOST_TIMEOUT = 3600000; // 1 hour in MS

export function reset() {
    return (dispatch, getState) => {
        let state = getState();
        dispatch({type: RESET});

        if(state.global.hostIp && (Date.now() - state.global.lastHostContact) < HOST_TIMEOUT) {
            dispatch(setHostIpAddress(state.global.hostIp));
        } else {
            dispatch(setHostIpAddress(''));
        }
    }
}

export function updateHostContactTime() {
    return {type: UPDATE_HOST_CONTACT_TIME};
}


export function setIntegrityCheck(value) {
    return (dispatch, getState) => {
        dispatch({type: SET_INTEGRITY_CHECK, value});
    }
}

export function doIntegrityCheck() {
    return (dispatch, getState) => {
        dispatch({type: DO_INTEGRITY_CHECK});
        dispatch(setIntegrityCheck(false));
    }
}
