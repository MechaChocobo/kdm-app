import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import {
    persistStore,
    persistCombineReducers,
} from 'redux-persist';
import storage from 'redux-persist/es/storage';
import rootReducers from '../app/reducers/index';
import Reactotron from '../config/ReactotronConfig';

const config = {
    key: 'root',
    storage,
    blacklist: ["networkReducer", "pageReducer", "settlementCreate"]
};

const middleware = [thunk];

const reducers = persistCombineReducers(config, rootReducers);
const enhancers = [applyMiddleware(...middleware)];
const persistConfig = { enhancers };
// const store = createStore(rootReducers, undefined, compose(...enhancers));
const store = Reactotron.createStore(reducers, compose(...enhancers));
const persistor = persistStore(store, persistConfig, () => {
    //console.log(store.getState());
});

const configureStore = () => {
    return { persistor, store };
}

export default configureStore;
