import reactStringReplace from 'react-string-replace';
import React from 'react';
import {Text, Linking} from 'react-native';

import sharedStyles from '../style/styles';
import translations from '../data/translations';

let locale = "en";

export function t(string, args = {}, forceLocale = null) {
    let usedLocale = forceLocale ? forceLocale : locale;
    if(translations[string] && translations[string][usedLocale]) {
        string = translations[string][usedLocale];
    }

    if(!string) {
        return '';
    }

    if(Object.keys(args).length > 0) {
        for(let key in args) {
            if(key === "linkUrl") {
                continue;
            }

            let re = new RegExp("\\%"+key+"\\%","g");
            string = string.replace(re, args[key]);
        }
    }

    let str = false;
    if(string.match(/%a%.*?%a%/)) {
        str = true;
    }

    if(string.match(/%b%.*?%b%/)) {
        string = reactStringReplace(string, /%b%(.*?)%b%/, (match, i) => (
            <Text style={{fontWeight: "bold"}} key={i}>{match}</Text>
        ));
    }

    if(str) {
        style = [sharedStyles.hyperlink];

        if(args.linkColor) {
            style.push({color: args.linkColor});
        }

        string = reactStringReplace(string, /%a%(.*?)%a%/, (match, i) => (
            <Text style={style} onPress={() => Linking.openURL(args.linkUrl)} key={i}>{match}</Text>
        ));
    }

    return string;
}

export function tt(string, args = {}) {
    if(locale !== "en" && translations[string] && translations[string][locale]) {
        return t(string, args) + ' [' + t(string, args, "en") + ']'
    } else {
        return t(string, args);
    }
}

export function setCachedLocale(l) {
    locale = l;
}

export function getSupportedLocales() {
    return {
        "en": {
            title: "English"
        },
        "de": {
            title: "Deutsch"
        },
        "pl": {
            title: "Polski"
        },
        "fr": {
            title: "Français"
        },
        "es": {
            title: "Español"
        }
    };
}
