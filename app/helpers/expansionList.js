import SimpleToast from "react-native-simple-toast";

const PASTEBIN_RAW_URL = "https://pastebin.com/raw/"

export function fetchExpansion(urlId) {
    return fetch(PASTEBIN_RAW_URL + urlId, {headers: {'Cache-Control': 'no-cache'}}).then((response) => {
        return response.text();
    }).then((response) => {
        return response;
    }).catch((error) => {
        SimpleToast.show(t("Error downloading expansion: %err%", {err: error}));
    });
}

export function sortExpansionList(a, b) {
    if(a.new && !b.new) {
        return -1
    } else if(!a.new && b.new) {
        return 1;
    }

    if(a.title < b.title) {
        return -1;
    } else if(a.title > b.title) {
        return 1;
    }

    return 0;
}
