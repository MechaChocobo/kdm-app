export const SURVIVOR_MESSAGE_SURVIVAL_INCREASE = 1;
export const SURVIVOR_MESSAGE_SURVIVAL_DECREASE = 2;
export const SURVIVOR_MESSAGE_GENDER_MALE = 3;
export const SURVIVOR_MESSAGE_GENDER_FEMALE = 4;
export const SURVIVOR_MESSAGE_NAME_CHANGE = 5;
export const SURVIVOR_MESSAGE_FATHER_SET = 6;
export const SURVIVOR_MESSAGE_MOTHER_SET = 7;
export const SURVIVOR_MESSAGE_SKIP_HUNT_YES = 8;
export const SURVIVOR_MESSAGE_SKIP_HUNT_NO = 9;
export const SURVIVOR_MESSAGE_RETIRED_YES = 10;
export const SURVIVOR_MESSAGE_RETIRED_NO = 11;
export const SURVIVOR_MESSAGE_HAS_REROLL_YES = 12;
export const SURVIVOR_MESSAGE_HAS_REROLL_NO = 13;
export const SURVIVOR_MESSAGE_REROLL_YES = 14;
export const SURVIVOR_MESSAGE_REROLL_NO = 15;
export const SURVIVOR_MESSAGE_DEAD_YES = 16;
export const SURVIVOR_MESSAGE_DEAD_NO = 17;
export const SURVIVOR_MESSAGE_UNAVAILABLE_YES = 18;
export const SURVIVOR_MESSAGE_UNAVAILABLE_NO = 19;
export const SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_YES = 20;
export const SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_NO = 21;
export const SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_YES = 22;
export const SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_NO = 23;
export const SURVIVOR_MESSAGE_SURVIVAL_ACTION_YES = 24;
export const SURVIVOR_MESSAGE_SURVIVAL_ACTION_NO = 25;
export const SURVIVOR_MESSAGE_INSANITY_INCREASE = 26;
export const SURVIVOR_MESSAGE_INSANITY_DECREASE = 27;
export const SURVIVOR_MESSAGE_BRAIN_INJURY_YES = 28;
export const SURVIVOR_MESSAGE_BRAIN_INJURY_NO = 29;
export const SURVIVOR_MESSAGE_XP_INCREASE = 30;
export const SURVIVOR_MESSAGE_XP_DECREASE = 31;
export const SURVIVOR_MESSAGE_COURAGE_INCREASE = 32;
export const SURVIVOR_MESSAGE_COURAGE_DECREASE = 33;
export const SURVIVOR_MESSAGE_UNDERSTANDING_INCREASE = 34;
export const SURVIVOR_MESSAGE_UNDERSTANDING_DECREASE = 35;
export const SURVIVOR_MESSAGE_ARMOR_INCREASE = 36;
export const SURVIVOR_MESSAGE_ARMOR_DECREASE = 37;
export const SURVIVOR_MESSAGE_ARMOR_ALL_INCREASE = 38;
export const SURVIVOR_MESSAGE_ARMOR_ALL_DECREASE = 39;
export const SURVIVOR_MESSAGE_ARMOR_RESET = 40;
export const SURVIVOR_MESSAGE_LIGHT_WOUND_YES = 41;
export const SURVIVOR_MESSAGE_LIGHT_WOUND_NO = 42;
export const SURVIVOR_MESSAGE_HEAVY_WOUND_YES = 43;
export const SURVIVOR_MESSAGE_HEAVY_WOUND_NO = 44;
export const SURVIVOR_MESSAGE_STAT_INCREASED = 45;
export const SURVIVOR_MESSAGE_STAT_DECREASED = 46;
export const SURVIVOR_MESSAGE_TEMP_STAT_INCREASED = 47;
export const SURVIVOR_MESSAGE_TEMP_STAT_DECREASED = 48;
export const SURVIVOR_MESSAGE_WEAPON_PROF_INCREASED = 49;
export const SURVIVOR_MESSAGE_WEAPON_PROF_DECREASED = 50;
export const SURVIVOR_MESSAGE_WEAPON_PROF_SET = 51;
export const SURVIVOR_MESSAGE_SET_CONSTELLATION = 52;
export const SURVIVOR_MESSAGE_ADD_FIGHTING_ART = 53;
export const SURVIVOR_MESSAGE_REMOVE_FIGHTING_ART = 54;
export const SURVIVOR_MESSAGE_ADD_DISORDER = 55;
export const SURVIVOR_MESSAGE_REMOVE_DISORDER = 56;
export const SURVIVOR_MESSAGE_ADD_ABILITY = 57;
export const SURVIVOR_MESSAGE_REMOVE_ABILITY = 58;
export const SURVIVOR_MESSAGE_ADD_SEVERE_INJURY = 59;
export const SURVIVOR_MESSAGE_REMOVE_SEVERE_INJURY = 60;
export const SURVIVOR_MESSAGE_EDIT_NOTE = 61;
export const SURVIVOR_MESSAGE_AFFINITY_GAINED = 62;
export const SURVIVOR_MESSAGE_AFFINITY_LOST = 63;
export const SURVIVOR_MESSAGE_ADD_CURSED_GEAR = 64;
export const SURVIVOR_MESSAGE_REMOVE_CURSED_GEAR = 65;
export const SURVIVOR_MESSAGE_COLOR_SET = 66;
export const SURVIVOR_MESSAGE_DEPARTING_YES = 67;
export const SURVIVOR_MESSAGE_DEPARTING_NO = 68;
export const SETTLEMENT_MESSAGE_SURVIVAL_INCREASE = 69;
export const SETTLEMENT_MESSAGE_SURVIVAL_DECREASE = 70;
export const SETTLEMENT_MESSAGE_POPULATION_INCREASE = 71;
export const SETTLEMENT_MESSAGE_POPULATION_DECREASE = 72;
export const SETTLEMENT_MESSAGE_DEATH_COUNT_INCREASE = 73;
export const SETTLEMENT_MESSAGE_DEATH_COUNT_DECREASE = 74;
export const SETTLEMENT_MESSAGE_ADD_INNOVATION = 75;
export const SETTLEMENT_MESSAGE_REMOVE_INNOVATION = 76;
export const SETTLEMENT_MESSAGE_ENDEAVOR_INCREASE = 77;
export const SETTLEMENT_MESSAGE_ENDEAVOR_DECREASE = 78;
export const SETTLEMENT_MESSAGE_RESOURCE_INCREASE = 79;
export const SETTLEMENT_MESSAGE_RESOURCE_DECREASE = 80;
export const SETTLEMENT_MESSAGE_GEAR_INCREASE = 81;
export const SETTLEMENT_MESSAGE_GEAR_DECREASE = 82;
export const SETTLEMENT_MESSAGE_ADD_LOCATION = 83;
export const SETTLEMENT_MESSAGE_REMOVE_LOCATION = 84;
export const SETTLEMENT_MESSAGE_NAME_SET = 85;
export const SETTLEMENT_MESSAGE_PRINCIPLE_SET = 86;
export const SETTLEMENT_MESSAGE_PRINCIPLE_UNSET = 87;
export const SETTLEMENT_MESSAGE_YEAR_NEXT = 88;
export const SETTLEMENT_MESSAGE_YEAR_PREVIOUS = 89;
export const SETTLEMENT_MESSAGE_STATUE_SET = 90;
export const SETTLEMENT_MESSAGE_ARCHIVED_YES = 91;
export const SETTLEMENT_MESSAGE_ARCHIVED_NO = 92;
export const SETTLEMENT_MESSAGE_NOTE_EDIT = 93;
export const TIMELINE_MESSAGE_EVENT_ADDED = 94;
export const TIMELINE_MESSAGE_EVENT_REMOVED = 95;
export const TIMELINE_MESSAGE_YEAR_ADDED = 96;
export const SETTLEMENT_MESSAGE_GEAR_CRAFTED = 97;
export const SETTLEMENT_MESSAGE_MILESTONE_YES = 98;
export const SETTLEMENT_MESSAGE_MILESTONE_NO = 99;
export const SETTLEMENT_MESSAGE_INNOVATION_ALL_ADD = 100;
export const SETTLEMENT_MESSAGE_INNOVATION_ALL_REMOVE = 101;
export const SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_ADD = 102;
export const SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_REMOVE = 103;
export const SETTLEMENT_MESSAGE_DISORDER_ALL_ADD = 104;
export const SETTLEMENT_MESSAGE_DISORDER_ALL_REMOVE = 105;
export const SETTLEMENT_MESSAGE_SE_EVENT_ALL_ADD = 106;
export const SETTLEMENT_MESSAGE_SE_EVENT_ALL_REMOVE = 107;
export const SETTLEMENT_MESSAGE_LOCATION_ALL_ADD = 108;
export const SETTLEMENT_MESSAGE_LOCATION_ALL_REMOVE = 109;
export const SURVIVOR_MESSAGE_LOADOUT_GEAR_SET = 110;
export const SURVIVOR_MESSAGE_LOADOUT_NAME_CHANGED = 111;
export const MESSAGE_LOADOUT_SAVED = 112;
export const SURVIVOR_MESSAGE_LOADOUT_SET = 113;
export const SURVIVOR_MESSAGE_LOADOUT_COPIED = 114;
export const MESSAGE_LOADOUT_DELETED = 115;
export const SURVIVOR_MESSAGE_NEW_LOADOUT = 116;
export const SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED = 117;
export const SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_NULL = 118;
export const SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED_NULL = 119;
export const SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_ADD = 120;
export const SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_REMOVE = 121;
export const SETTLEMENT_MESSAGE_GEAR_ALL_ADD = 122;
export const SETTLEMENT_MESSAGE_GEAR_ALL_REMOVE = 123;
export const SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD = 124;
export const SETTLEMENT_MESSAGE_RESOURCE_ALL_REMOVE = 125;
export const SETTLEMENT_MESSAGE_ABILITY_ALL_ADD = 126;
export const SETTLEMENT_MESSAGE_ABILITY_ALL_REMOVE = 127;
export const SETTLEMENT_MESSAGE_ARMOR_SET_ALL_ADD = 128;
export const SETTLEMENT_MESSAGE_ARMOR_SET_ALL_REMOVE = 129;
export const SURVIVOR_MESSAGE_ARMOR_SET_SET = 130;
export const SURVIVOR_MESSAGE_ARMOR_SET_REMOVE = 131;
export const SURVIVOR_MESSAGE_WEAPON_PROF_EARNED = 132;
export const SURVIVOR_MESSAGE_WEAPON_PROF_UNEARNED = 133;
export const SURVIVOR_MESSAGE_CAUSE_OF_DEATH = 134;

export const messageMap = {
    [SURVIVOR_MESSAGE_SURVIVAL_INCREASE]: "Survival increased to %num%",
    [SURVIVOR_MESSAGE_SURVIVAL_DECREASE]: "Survival decreased to %num%",
    [SURVIVOR_MESSAGE_GENDER_MALE]: "Gender changed to male",
    [SURVIVOR_MESSAGE_GENDER_FEMALE]: "Gender changed to female",
    [SURVIVOR_MESSAGE_NAME_CHANGE]: "Name changed to %name%",
    [SURVIVOR_MESSAGE_FATHER_SET]: "Father set to %name%",
    [SURVIVOR_MESSAGE_MOTHER_SET]: "Mother set to %name%",
    [SURVIVOR_MESSAGE_SKIP_HUNT_YES]: "Skipping next hunt",
    [SURVIVOR_MESSAGE_SKIP_HUNT_NO]: "No longer skipping next hunt",
    [SURVIVOR_MESSAGE_RETIRED_YES]: "Retired",
    [SURVIVOR_MESSAGE_RETIRED_NO]: "No longer retired",
    [SURVIVOR_MESSAGE_HAS_REROLL_YES]: "Has reroll enabled",
    [SURVIVOR_MESSAGE_HAS_REROLL_NO]: "No longer has reroll enabled",
    [SURVIVOR_MESSAGE_REROLL_YES]: "Regained reroll",
    [SURVIVOR_MESSAGE_REROLL_NO]: "Used reroll",
    [SURVIVOR_MESSAGE_DEAD_YES]: "Died",
    [SURVIVOR_MESSAGE_DEAD_NO]: "Resurrected!",
    [SURVIVOR_MESSAGE_UNAVAILABLE_YES]: "Set to unavailable",
    [SURVIVOR_MESSAGE_UNAVAILABLE_NO]: "No longer unavailable",
    [SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_YES]: "Can spend survival again",
    [SURVIVOR_MESSAGE_CAN_SPEND_SURVIVAL_NO]: "Can no longer spend survival",
    [SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_YES]: "Can gain survival again",
    [SURVIVOR_MESSAGE_CAN_GAIN_SURVIVAL_NO]: "Can no longer gain survival",
    [SURVIVOR_MESSAGE_SURVIVAL_ACTION_YES]: "Unlocked survival action: %action%",
    [SURVIVOR_MESSAGE_SURVIVAL_ACTION_NO]: "Lost survival action: %action%",
    [SURVIVOR_MESSAGE_INSANITY_INCREASE]: "Insanity increased to %num%",
    [SURVIVOR_MESSAGE_INSANITY_DECREASE]: "Insanity decreased to %num%",
    [SURVIVOR_MESSAGE_BRAIN_INJURY_YES]: "Light brain injury received",
    [SURVIVOR_MESSAGE_BRAIN_INJURY_NO]: "Light brain injury healed",
    [SURVIVOR_MESSAGE_XP_INCREASE]: "Hunt XP increased to %num%",
    [SURVIVOR_MESSAGE_XP_DECREASE]: "Hunt XP decreased to %num%",
    [SURVIVOR_MESSAGE_COURAGE_INCREASE]: "Courage increased to %num%",
    [SURVIVOR_MESSAGE_COURAGE_DECREASE]: "Courage decreased to %num%",
    [SURVIVOR_MESSAGE_UNDERSTANDING_INCREASE]: "Understanding increased to %num%",
    [SURVIVOR_MESSAGE_UNDERSTANDING_DECREASE]: "Understanding decreased to %num%",
    [SURVIVOR_MESSAGE_ARMOR_INCREASE]: "%location% armor increased to %num%",
    [SURVIVOR_MESSAGE_ARMOR_DECREASE]: "%location% armor decreased to %num%",
    [SURVIVOR_MESSAGE_ARMOR_ALL_INCREASE]: "All armor increased",
    [SURVIVOR_MESSAGE_ARMOR_ALL_DECREASE]: "All armor decreased",
    [SURVIVOR_MESSAGE_ARMOR_RESET]: "Armor and wounds reset",
    [SURVIVOR_MESSAGE_LIGHT_WOUND_YES]: "%location% light injury received",
    [SURVIVOR_MESSAGE_LIGHT_WOUND_NO]: "%location% light injury healed",
    [SURVIVOR_MESSAGE_HEAVY_WOUND_YES]: "%location% heavy injury received",
    [SURVIVOR_MESSAGE_HEAVY_WOUND_NO]: "%location% heavy injury healed",
    [SURVIVOR_MESSAGE_STAT_INCREASED]: "%stat% increased to %num%",
    [SURVIVOR_MESSAGE_STAT_DECREASED]: "%stat% decreased to %num%",
    [SURVIVOR_MESSAGE_TEMP_STAT_INCREASED]: "Temporary stat %stat% increased to %num%",
    [SURVIVOR_MESSAGE_TEMP_STAT_DECREASED]: "Temporary stat %stat% decreased to %num%",
    [SURVIVOR_MESSAGE_WEAPON_PROF_INCREASED]: "Weapon proficiency increased to %num%",
    [SURVIVOR_MESSAGE_WEAPON_PROF_DECREASED]: "Weapon proficiency decreased to %num%",
    [SURVIVOR_MESSAGE_WEAPON_PROF_SET]: "Weapon proficiency changed to %prof%",
    [SURVIVOR_MESSAGE_SET_CONSTELLATION]: "Constellation set to %name%",
    [SURVIVOR_MESSAGE_ADD_FIGHTING_ART]: "Learned fighting art %name%",
    [SURVIVOR_MESSAGE_REMOVE_FIGHTING_ART]: "Lost fighting art %name%",
    [SURVIVOR_MESSAGE_ADD_DISORDER]: "Gained disorder %name%",
    [SURVIVOR_MESSAGE_REMOVE_DISORDER]: "Lost disorder %name%",
    [SURVIVOR_MESSAGE_ADD_ABILITY]: "Gained ability %name%",
    [SURVIVOR_MESSAGE_REMOVE_ABILITY]: "Lost ability %name%",
    [SURVIVOR_MESSAGE_ADD_SEVERE_INJURY]: "Suffered severe injury %name%",
    [SURVIVOR_MESSAGE_REMOVE_SEVERE_INJURY]: "Healed severe injury %name%",
    [SURVIVOR_MESSAGE_EDIT_NOTE]: "Note edited to '%note%'",
    [SURVIVOR_MESSAGE_AFFINITY_LOST]: "Lost %color% affinity",
    [SURVIVOR_MESSAGE_AFFINITY_GAINED]: "Gained %color% affinity",
    [SURVIVOR_MESSAGE_ADD_CURSED_GEAR]: "Gained cursed gear %gear%",
    [SURVIVOR_MESSAGE_REMOVE_CURSED_GEAR]: "Lost cursed gear %gear%",
    [SURVIVOR_MESSAGE_COLOR_SET]: "Color set to %color%",
    [SURVIVOR_MESSAGE_DEPARTING_YES]: "Set as departing",
    [SURVIVOR_MESSAGE_DEPARTING_NO]: "No longer departing",
    [SETTLEMENT_MESSAGE_SURVIVAL_INCREASE]: "Survival limit increased to %num%",
    [SETTLEMENT_MESSAGE_SURVIVAL_DECREASE]: "Survival limit decreased to %num%",
    [SETTLEMENT_MESSAGE_POPULATION_INCREASE]: "Population increased to %num%",
    [SETTLEMENT_MESSAGE_POPULATION_DECREASE]: "Population decreased to %num%",
    [SETTLEMENT_MESSAGE_DEATH_COUNT_INCREASE]: "Death Count increased to %num%",
    [SETTLEMENT_MESSAGE_DEATH_COUNT_DECREASE]: "Death Count decreased to %num%",
    [SETTLEMENT_MESSAGE_ADD_INNOVATION]: "Added innovation %name%",
    [SETTLEMENT_MESSAGE_REMOVE_INNOVATION]: "Removed innovation %name%",
    [SETTLEMENT_MESSAGE_ENDEAVOR_INCREASE]: "Endeavors increased to %num%",
    [SETTLEMENT_MESSAGE_ENDEAVOR_DECREASE]: "Endeavors decreased to %num%",
    [SETTLEMENT_MESSAGE_RESOURCE_INCREASE]: "Resource %name% (%loc%) increased to %num%",
    [SETTLEMENT_MESSAGE_RESOURCE_DECREASE]: "Resource %name% (%loc%) decreased to %num%",
    [SETTLEMENT_MESSAGE_GEAR_INCREASE]: "Gear %name% (%loc%) increased to %num%",
    [SETTLEMENT_MESSAGE_GEAR_DECREASE]: "Gear %name% (%loc%) decreased to %num%",
    [SETTLEMENT_MESSAGE_ADD_LOCATION]: "Gained location %name%",
    [SETTLEMENT_MESSAGE_REMOVE_LOCATION]: "Lost location %name%",
    [SETTLEMENT_MESSAGE_NAME_SET]: "Name changed to %name%",
    [SETTLEMENT_MESSAGE_PRINCIPLE_SET]: "Principle %princ% set to %name%",
    [SETTLEMENT_MESSAGE_PRINCIPLE_UNSET]: "Principle %princ% reset",
    [SETTLEMENT_MESSAGE_YEAR_NEXT]: "Lantern year advanced to %ly%",
    [SETTLEMENT_MESSAGE_YEAR_PREVIOUS]: "Lantern year reset to %ly%",
    [SETTLEMENT_MESSAGE_STATUE_SET]: "Inspirational Statue set to %name%",
    [SETTLEMENT_MESSAGE_ARCHIVED_YES]: "Settlement archived",
    [SETTLEMENT_MESSAGE_ARCHIVED_NO]: "Settlement unarchived",
    [SETTLEMENT_MESSAGE_NOTE_EDIT]: "Note edited to '%note%'",
    [TIMELINE_MESSAGE_EVENT_ADDED]: "Added %type% %name% to LY %ly%",
    [TIMELINE_MESSAGE_EVENT_REMOVED]: "Removed %type% %name% to LY %ly%",
    [TIMELINE_MESSAGE_YEAR_ADDED]: "Added another LY",
    [SETTLEMENT_MESSAGE_GEAR_CRAFTED]: "Crafted gear %gear%, cost paid: %cost%",
    [SETTLEMENT_MESSAGE_MILESTONE_YES]: "Achieved milestone \"%name%\"",
    [SETTLEMENT_MESSAGE_MILESTONE_NO]: "Unachieved milestone \"%name%\"",
    [SETTLEMENT_MESSAGE_INNOVATION_ALL_ADD]: "Added innovation \"%name%\" to list of all innovations",
    [SETTLEMENT_MESSAGE_INNOVATION_ALL_REMOVE]: "Removed innovation \"%name%\" from list of all innovations",
    [SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_ADD]: "Added fighting art \"%name%\" to list of all fighting arts",
    [SETTLEMENT_MESSAGE_FIGHTING_ART_ALL_REMOVE]: "Removed fighting art \"%name%\" from list of all fighting arts",
    [SETTLEMENT_MESSAGE_DISORDER_ALL_ADD]: "Added disorder \"%name%\" to list of all disorders",
    [SETTLEMENT_MESSAGE_DISORDER_ALL_REMOVE]: "Removed disorder \"%name%\" from list of all disorders",
    [SETTLEMENT_MESSAGE_SE_EVENT_ALL_ADD]: "Added settlement event \"%name%\" to list of all settlement events",
    [SETTLEMENT_MESSAGE_SE_EVENT_ALL_REMOVE]: "Removed settlement event \"%name%\" from list of all settlement events",
    [SETTLEMENT_MESSAGE_LOCATION_ALL_ADD]: "Added location \"%name%\" to list of all locations",
    [SETTLEMENT_MESSAGE_LOCATION_ALL_REMOVE]: "Removed location \"%name%\" from list of all locations",
    [SURVIVOR_MESSAGE_LOADOUT_GEAR_SET]: "Added gear %gearName% to loadout %loadoutName%",
    [SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED]: "Added gear %gearName% to unnamed loadout",
    [SURVIVOR_MESSAGE_LOADOUT_NAME_CHANGED]: "Loadout name changed to %name%",
    [MESSAGE_LOADOUT_SAVED]: "Loadout %name% saved",
    [SURVIVOR_MESSAGE_LOADOUT_SET]: "Switched loadout to %name%",
    [SURVIVOR_MESSAGE_LOADOUT_COPIED]: "Copied loadout to %name%",
    [MESSAGE_LOADOUT_DELETED]: "Loadout %name% deleted",
    [SURVIVOR_MESSAGE_NEW_LOADOUT]: "New unnamed loadout created",
    [SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_NULL]: "Removed gear %gearName% from loadout %loadoutName%",
    [SURVIVOR_MESSAGE_LOADOUT_GEAR_SET_UNNAMED_NULL]: "Removed gear %gearName% from unnamed loadout",
    [SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_ADD]: "Added weapon proficiency \"%name%\" to list of all proficiencies",
    [SETTLEMENT_MESSAGE_WEAPON_PROFICIENCY_ALL_REMOVE]: "Removed weapon proficiency \"%name%\" from list of all proficiencies",
    [SETTLEMENT_MESSAGE_GEAR_ALL_ADD]: "Added gear \"%name%\" to list of all gear",
    [SETTLEMENT_MESSAGE_GEAR_ALL_REMOVE]: "Removed gear \"%name%\" from list of all gear",
    [SETTLEMENT_MESSAGE_RESOURCE_ALL_ADD]: "Added resource \"%name%\" to list of all resources",
    [SETTLEMENT_MESSAGE_RESOURCE_ALL_REMOVE]: "Removed resource \"%name%\" from list of all resources",
    [SETTLEMENT_MESSAGE_ABILITY_ALL_ADD]: "Added ability \"%name%\" to list of all abilities",
    [SETTLEMENT_MESSAGE_ABILITY_ALL_REMOVE]: "Removed ability \"%name%\" from list of all abilities",
    [SETTLEMENT_MESSAGE_ARMOR_SET_ALL_ADD]: "Added armor set \"%name%\" to list of all armor sets",
    [SETTLEMENT_MESSAGE_ARMOR_SET_ALL_REMOVE]: "Removed armor set \"%name%\" from list of all armor sets",
    [SURVIVOR_MESSAGE_ARMOR_SET_SET]: "Armor set changed to %name%",
    [SURVIVOR_MESSAGE_ARMOR_SET_REMOVE]: "Armor set removed",
    [SURVIVOR_MESSAGE_WEAPON_PROF_EARNED]: "Weapon proficiency point earned",
    [SURVIVOR_MESSAGE_WEAPON_PROF_UNEARNED]: "Weapon proficiency point un-earned",
    [SURVIVOR_MESSAGE_CAUSE_OF_DEATH]: "Cause of death set to \"%cause%\"",
}
