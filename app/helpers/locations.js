import { getLocationConfig, sortIdArrayByConfigTitle } from "./helpers";

export function getAvailableLocations(locations, allLocations) {
    let filtered = allLocations.filter(location => (
        locations.indexOf(location) == -1
    ));

    let LocationConfig = getLocationConfig();
    return sortIdArrayByConfigTitle(filtered, LocationConfig);
}
