import RecyclerContext from "./RecyclerContext";

let context = {};

export function resetListContext() {
    context = {};
}

export function initRecylcerContext(key) {
    if(context[key]) {
        return getContext(key);
    }

    context[key] = new RecyclerContext(key);
    return getContext(key);
}

export function getContext(key) {
    return context[key];
}
