var Uuid = require('react-native-uuid');
import PropTypes from 'prop-types';
import {Dimensions} from 'react-native';

// buncha cruncha configs
import SettlementBaseCore from '../config/base-settlements/core.json';
import SettlementBasePotSun from '../config/base-settlements/potsun.json';
import SettlementBasePotStars from '../config/base-settlements/potstars.json';
import Resources from '../config/resources-expansions.json';
import ResourceConfig from '../config/resources.json';
import Gear from '../config/gear-expansions.json';
import GearConfig from '../config/gear.json';
import Locations from '../config/locations-expansions.json';
import Innovations from '../config/innovations-expansions.json';
import FightingArts from '../config/fighting-arts-expansions.json';
import Disorders from '../config/disorders-expansions.json';
import Abilities from '../config/abilities-expansions.json';
import SevereInjuries from '../config/severe-injuries-expansions.json';
import Proficiencies from '../config/weapon-profs-expansions.json';
import Armor from '../config/armor-expansions.json';
import SurvivorBase from '../config/base-survivors/survivor-base.json';
import SurvivorBaseSun from '../config/base-survivors/survivor-base-sun.json';
import SurvivorBaseStars from '../config/base-survivors/survivor-base-stars.json';
import PrinciplesConfig from '../config/principles.json';
import InnovationConfig from '../config/innovations.json';
import LocationConfig from '../config/locations.json';
import SettlementEvents from '../config/settlement-events-expansions.json';
import SettlementEventConfig from '../config/settlement-events.json';
import StoryEvents from '../config/story-events-expansions.json';
import NemesisEncounters from '../config/nemesis-expansions.json';
import Showdowns from '../config/showdowns-expansions.json';
import FightingArtConfig from '../config/fighting-arts.json';
import DisorderConfig from '../config/disorders.json';
import WeaponProfConfig from '../config/weapon-profs.json';
import SevereInjuryConfig from '../config/severe-injuries.json';
import AbilityConfig from '../config/abilities.json';
import StoryEventConfig from '../config/story-events.json';
import ShowdownConfig from '../config/showdowns.json';
import ArmorConfig from '../config/armor.json';
import MilestoneConfig from '../config/milestones.json';
import { t } from './intl';
import { MILESTONE_TYPE_SETTLEMENT } from '../reducers/custom-expansions.js';

let customInnovationConfig = {};
let customLocationConfig = {};
let customAbilityConfig = {};
let customDisorderConfig = {};
let customFightingArtConfig = {};
let customSevereInjuryConfig = {};
let customWeaponProfConfig = {};
let customResourceConfig = {};
let customGearConfig = {};
let customPrinciplesConfig = {};
let customSettlementEventConfig = {};
let customStoryEventConfig = {};
let customShowdownConfig = {};
let customArmorConfig = {};
let customMilestoneConfig = {};

const IMAGE_RATIO = 2.096;

export const CHECKBOX_SIZE = 22;

let expansions = {
    "dragon-king" : "Dragon King",
    "dragon-king" : "Dragon King",
    "dung-beetle-knight" : "Dung Beetle Knight",
    "flower-knight" : "Flower Knight",
    "gorm" : "Gorm",
    "green-knight" : "Green Knight",
    "lion-god" : "Lion God",
    "lion-knight" : "Lion Knight",
    "lonely-tree" : "Lonely Tree",
    "manhunter" : "Manhunter",
    "slenderman" : "Slenderman",
    "spidicules" : "Spidicules",
    "sunstalker" : "Sunstalker",
}

let expansionAbbreviations = {
    "dragon-king" : "DK",
    "dung-beetle-knight" : "DBK",
    "flower-knight" : "FK",
    "gorm" : "GO",
    "green-knight" : "GK",
    "lion-god" : "LG",
    "lion-knight" : "LK",
    "lonely-tree" : "LT",
    "manhunter" : "MH",
    "slenderman" : "SD",
    "spidicules" : "SP",
    "sunstalker" : "SS",
}

let campaigns = {
    "PotLantern": {title: "People of the Lantern"},
    "PotSun": {title: "People of the Sun"},
    "PotStars": {title: "People of the Stars"},
    "PotBloom": {title: "People of the Bloom"}
}

export function getUnselectedArray(selected, allItems) {
    let filtered = allItems.filter(item => (
        selected.indexOf(item) == -1
    ));

    return filtered.sort(sortArrayByTitle);
}

export function sortArrayByTitle(config) {
    return (a, b) => {
        let aTitle = a;
        let bTitle = b;

        if(config[a]) {
            aTitle = config[a].title;
        }

        if(config[b]) {
            bTitle = config[b].title;
        }

        if(aTitle.toUpperCase() > bTitle.toUpperCase()) {
            return 1;
        } else if(aTitle.toUpperCase() < bTitle.toUpperCase()) {
            return -1;
        } else {
            return 0;
        }
    }
}

export function sortIdArrayByConfigTitle(ids, config) {
    return ids.sort((a, b) => {
        let aTitle = config[a] ? config[a].title : a;
        let bTitle = config[b] ? config[b].title : b;
        if(aTitle.toUpperCase() > bTitle.toUpperCase()) {
            return 1;
        } else if(aTitle.toUpperCase() < bTitle.toUpperCase()) {
            return -1;
        } else {
            return 0;
        }
    });
}

export function sortObjectArrayByTitle(a, b) {
    if(a.title > b.title) {
        return 1;
    } else if(a.title < b.title) {
        return -1;
    } else {
        return 0;
    }
}

export function sortArrayByName(a, b) {
    if(a.name.toUpperCase() > b.name.toUpperCase()) {
        return 1;
    } else if(a.name.toUpperCase() < b.name.toUpperCase()) {
        return -1;
    } else {
        return 0;
    }
}

export function initSurvivors(settlementId, createSurvivors, campaign) {
    let survivors = {};
    if(createSurvivors) {
        // 2 males
        let newSurvivor = createSurvivor(settlementId, null, "M", "Zachary", undefined, undefined, campaign);
        newSurvivor.departing = true;
        newSurvivor.loadout = getInitialLoadout("Zachary");
        survivors[newSurvivor.id] = newSurvivor;
        newSurvivor = createSurvivor(settlementId, null, "M", "Allister", undefined, undefined, campaign);
        newSurvivor.loadout = getInitialLoadout("Allister");
        newSurvivor.departing = true;
        survivors[newSurvivor.id] = newSurvivor;

        // 2 females
        newSurvivor = createSurvivor(settlementId, null, "F", "Lucy", undefined, undefined, campaign);
        newSurvivor.loadout = getInitialLoadout("Lucy");
        newSurvivor.departing = true;
        survivors[newSurvivor.id] = newSurvivor;
        newSurvivor = createSurvivor(settlementId, null, "F", "Erza", undefined, undefined, campaign);
        newSurvivor.loadout = getInitialLoadout("Erza");
        newSurvivor.departing = true;
        survivors[newSurvivor.id] = newSurvivor;
    }

    return survivors;
}

function getInitialLoadout(name) {
    return {
        name,
        gear: [
            [{location: "Starting Gear", name: "cloth"}, {location: "Starting Gear", name: "foundingStone"}, null],
            [null, null, null],
            [null, null, null],
        ],
        unsaved: true,
        armorSet: "",
    }
}

export function createSurvivor(settlementId, id = null, gender = "M", name = "Gary", parents = {father: "", mother: ""}, bonuses = {principles: {}, innovations: [], settlement: {}}, campaign = "PotLantern") {
    // JSON parse deep copies while {...} is shallow
    let baseConfig = SurvivorBase;

    if(campaign === "PotSun") {
        baseConfig = SurvivorBaseSun;
    } else if (campaign === "PotStars") {
        baseConfig = SurvivorBaseStars;
    }

    let newSurvivor = JSON.parse(JSON.stringify(baseConfig));
    newSurvivor.gender = gender;
    newSurvivor.name = name;
    newSurvivor.settlement = settlementId;
    newSurvivor.id = id === null ? Uuid.v4() : id;
    newSurvivor.key = newSurvivor.id;

    if(parents.father !== "" && parents.mother !== "") {
        newSurvivor.parents.father = parents.father;
        newSurvivor.parents.mother = parents.mother;
    }
    newSurvivor = applyOnBirthBonuses(newSurvivor, bonuses, (parents.father !== "" && parents.mother !== ""));

    return newSurvivor;
}

function applyOnBirthBonuses(survivor, bonuses, parents = false) {
    let InnovationConfig = getInnovationConfig();
    let {principles, innovations, settlement} = bonuses;

    for (const key in principles) {
        if(principles[key]) {
            let principle = PrinciplesConfig[key][principles[key]];
            if(parents && principle.newBornEffects) {
                survivor = applyStatChanges(survivor, principle.newBornEffects);
            }

            if(principle.allNewSurvivors) {
                survivor = applyStatChanges(survivor, principle.allNewSurvivors);
            }
        }
    }

    for(var i = 0; i < innovations.length; i++) {
        let innovation = InnovationConfig[innovations[i]];
        if(parents && innovation.newBornEffects) {
            survivor = applyStatChanges(survivor, innovation.newBornEffects);
        }

        if(innovation.allNewSurvivors) {
            survivor = applyStatChanges(survivor, innovation.allNewSurvivors);
        }
    }

    if((parents && settlement.newBornEffects)) {
        survivor = applyStatChanges(survivor, settlement.newBornEffects);
    }

    if(settlement.allNewSurvivors) {
        survivor = applyStatChanges(survivor, settlement.allNewSurvivors);
    }

    return survivor;
}

export function createSettlement(id, name, campaign, expansions) {
    let settlement = {};
    if(campaign == "PotLantern") {
        settlement = JSON.parse(JSON.stringify(SettlementBaseCore));
    } else if(campaign === "PotSun") {
        settlement = JSON.parse(JSON.stringify(SettlementBasePotSun));
    } else if(campaign === "PotStars") {
        settlement = JSON.parse(JSON.stringify(SettlementBasePotStars));
    } else if(campaign === "PotBloom") {
        settlement = JSON.parse(JSON.stringify(SettlementBaseCore));
    }

    settlement.id = id;
    settlement.name = name;
    settlement.expansions = expansions;
    settlement.campaign = campaign;

    settlement = addExpansionContentToSettlement(settlement, settlement.expansions);
    settlement = applyCampaignTweaks(settlement);

    settlement.storage.gear["Starting Gear"]["cloth"] = 4;
    settlement.storage.gear["Starting Gear"]["foundingStone"] = 4;

    return settlement;
}

function addExpansionContentToSettlement(settlement, expansions) {
    settlement = addResourceStorageToSettlement(settlement, expansions);
    settlement = addGearStorageToSettlement(settlement, expansions);
    settlement = addCursedGearToSettlement(settlement, expansions);
    settlement = addLocationsToSettlement(settlement, expansions);
    settlement = addInnovationsToSettlement(settlement, expansions);
    settlement = addProficienciesToSettlement(settlement, expansions);
    settlement = addFightingArtsToSettlement(settlement, expansions);
    settlement = addDisordersToSettlement(settlement, expansions);
    settlement = addAbilitiesToSettlement(settlement, expansions);
    settlement = addSevereInjuriesToSettlement(settlement, expansions);
    settlement = addSettlementEventsToSettlement(settlement, expansions);
    settlement = addStoryEventsToSettlement(settlement, expansions);
    settlement = addNemesisEncountersToSettlement(settlement, expansions);
    settlement = addShowdownsToSettlement(settlement, expansions);
    settlement = addArmorSetsToSettlement(settlement, expansions);

    return settlement;
}

export function addCustomExpansionToSettlement(settlement, expansion, type = "all") {
    let obj = {};
    obj[expansion.id] = type;
    let config = {};

    let resourceConfig = {};
    for(let source in expansion.resourceConfig) {
        for(let id in expansion.resourceConfig[source]) {
            if(!resourceConfig[source]) {
                resourceConfig[source] = {};
            }

            resourceConfig[source][id] = 0;
        }
    }

    config[expansion.id] = resourceConfig;
    settlement = addResourceStorageToSettlement(settlement, obj, config, expansion.resourceConfig);

    let gearConfig = {};
    for(let source in expansion.gearConfig) {
        for(let id in expansion.gearConfig[source]) {
            if(!gearConfig[source]) {
                gearConfig[source] = {};
            }

            gearConfig[source][id] = 0;
        }
    }
    config[expansion.id] = gearConfig;
    settlement = addGearStorageToSettlement(settlement, obj, config, expansion.gearConfig);
    settlement = addCursedGearToSettlement(settlement, obj, config);
    config[expansion.id] = expansion.locations;
    settlement = addLocationsToSettlement(settlement, obj, config, expansion.locationConfig);
    config[expansion.id] = expansion.innovations;
    settlement = addInnovationsToSettlement(settlement, obj, config, expansion.innovationConfig);
    config[expansion.id] = expansion.weaponProfs;
    settlement = addProficienciesToSettlement(settlement, obj, config, expansion.weaponProfConfig);
    config[expansion.id] = expansion.fightingArts;
    settlement = addFightingArtsToSettlement(settlement, obj, config, expansion.fightingArtConfig);
    config[expansion.id] = expansion.disorders;
    settlement = addDisordersToSettlement(settlement, obj, config, expansion.disorderConfig);
    config[expansion.id] = expansion.abilities;
    settlement = addAbilitiesToSettlement(settlement, obj, config, expansion.abilityConfig);
    config[expansion.id] = expansion.severeInjuries;
    settlement = addSevereInjuriesToSettlement(settlement, obj, config, expansion.severeInjuryConfig);
    config[expansion.id] = expansion.settlementEvents;
    settlement = addSettlementEventsToSettlement(settlement, obj, config, expansion.settlementEventConfig);
    config[expansion.id] = expansion.armorSets;
    settlement = addArmorSetsToSettlement(settlement, obj, config, expansion.armorSetConfig);
    config[expansion.id] = Object.keys(expansion.storyEvents);
    settlement = addStoryEventsToSettlement(settlement, obj, config);
    config[expansion.id] = Object.keys(expansion.nemesisEncounters);
    settlement = addNemesisEncountersToSettlement(settlement, obj, config);
    config[expansion.id] = Object.keys(expansion.showdowns);
    settlement = addShowdownsToSettlement(settlement, obj, config, expansion.showdowns);
    config[expansion.id] = expansion.milestones;
    settlement = addMilestonesToSettlement(settlement, obj, config);

    if(!settlement.customExpansions) {
        settlement.customExpansions = {};
    }

    settlement.customExpansions[expansion.id] = type;
    return settlement;
}

export function setCustomExpansionConfigs(expansions) {
    // reset so they aren't cached
    customInnovationConfig = {};
    customLocationConfig = {};
    customAbilityConfig = {};
    customDisorderConfig = {};
    customFightingArtConfig = {};
    customSevereInjuryConfig = {};
    customWeaponProfConfig = {};
    customResourceConfig = {};
    customGearConfig = {};
    customPrinciplesConfig = {};
    customSettlementEventConfig = {};
    customStoryEventConfig = {};
    customShowdownConfig = {};
    customArmorConfig = {};
    customMilestoneConfig = {};

    for(let i = 0; i < expansions.length; i++) {
        if(expansions[i].innovationConfig) {
            customInnovationConfig = {...customInnovationConfig, ...expansions[i].innovationConfig}
        }

        if(expansions[i].disorderConfig) {
            customDisorderConfig = {...customDisorderConfig, ...expansions[i].disorderConfig}
        }

        if(expansions[i].fightingArtConfig) {
            customFightingArtConfig = {...customFightingArtConfig, ...expansions[i].fightingArtConfig}
        }

        if(expansions[i].locationConfig) {
            customLocationConfig = {...customLocationConfig, ...expansions[i].locationConfig}
        }

        if(expansions[i].weaponProfConfig) {
            customWeaponProfConfig = {...customWeaponProfConfig, ...expansions[i].weaponProfConfig}
        }

        if(expansions[i].abilityConfig) {
            customAbilityConfig = {...customAbilityConfig, ...expansions[i].abilityConfig}
        }

        if(expansions[i].severeInjuryConfig) {
            customSevereInjuryConfig = {...customSevereInjuryConfig, ...expansions[i].severeInjuryConfig}
        }

        if(expansions[i].armorSetConfig) {
            customArmorConfig = {...customArmorConfig, ...expansions[i].armorSetConfig}
        }

        if(expansions[i].settlementEventConfig) {
            customSettlementEventConfig = {...customSettlementEventConfig, ...expansions[i].settlementEventConfig}
        }

        if(expansions[i].storyEvents) {
            customStoryEventConfig = {...customStoryEventConfig, ...expansions[i].storyEvents}
        }

        if(expansions[i].showdowns) {
            customShowdownConfig = {...customShowdownConfig, ...expansions[i].showdowns}
        }

        // note that this is also mergin into customShowdownConfig
        if(expansions[i].nemesisEncounters) {
            customShowdownConfig = {...customShowdownConfig, ...expansions[i].nemesisEncounters}
        }

        if(expansions[i].resourceConfig) {
            customResourceConfig = mergeStorage(customResourceConfig, expansions[i].resourceConfig);
        }

        if(expansions[i].gearConfig) {
            customGearConfig = mergeStorage(customGearConfig, expansions[i].gearConfig);
        }

        if(expansions[i].milestones) {
            customMilestoneConfig = {...customMilestoneConfig, ...expansions[i].milestones}
        }
    }
}

// apply specific tweaks that aren't in the base config, like removing things from the core game
function applyCampaignTweaks(settlement) {
    if(settlement.campaign === "PotSun") {
        settlement.allInnovations.splice(settlement.allInnovations.indexOf("language"), 1);
        settlement.allNemesisEncounters.splice(settlement.allNemesisEncounters.indexOf("Watcher"), 1);
        settlement.allNemesisEncounters.splice(settlement.allNemesisEncounters.indexOf("Gold Smoke Knight"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("hoodedKnight"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("watched"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("armoredStrangers"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("regalVisit"), 1);
        settlement.allLocations.splice(settlement.allLocations.indexOf("lanternHoard"), 1);
        settlement.allLocations.splice(settlement.allLocations.indexOf("exhaustedLanternHoard"), 1);
    } else if(settlement.campaign === "PotStars") {
        settlement.allInnovations.splice(settlement.allInnovations.indexOf("language"), 1);
        settlement.allInnovations.splice(settlement.allInnovations.indexOf("lanternOven"), 1);
        settlement.allInnovations.splice(settlement.allInnovations.indexOf("family"), 1);
        settlement.allInnovations.splice(settlement.allInnovations.indexOf("clanOfDeath"), 1);
        settlement.allNemesisEncounters.splice(settlement.allNemesisEncounters.indexOf("Watcher"), 1);
        settlement.allNemesisEncounters.splice(settlement.allNemesisEncounters.indexOf("Gold Smoke Knight"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("watched"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("handsOfHeat"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("armoredStrangers"), 1);
        settlement.allStoryEvents.splice(settlement.allStoryEvents.indexOf("regalVisit"), 1);
        settlement.allLocations.splice(settlement.allLocations.indexOf("lanternHoard"), 1);
        settlement.allLocations.splice(settlement.allLocations.indexOf("exhaustedLanternHoard"), 1);

    } else if(settlement.campaign === "PotBloom") {
        settlement = addStorageToSettlement(settlement, "resources", Resources["flower-knight"], getResourceConfig());
        settlement = addStorageToSettlement(settlement, "gear", Gear["flower-knight"], getGearConfig());
        settlement.globalEndeavors.push({endeavorCost: 1, title: "Forest Run", description: "You may exchange any number of monster resources for that number of random Flower resources."});
        settlement.allDisorders.splice(settlement.allDisorders.indexOf("flowerAddiction"), 1);
        settlement.allStoryEvents.push("aWarmVirus");
        settlement.allStoryEvents.push("necrotoxicMistletoe");
        settlement.allStoryEvents.push("senseMemory");
        settlement.allFightingArts.push("acanthusDoctor");
        settlement.settlementEffects.newBornEffects = {attributes: {LCK: 1}, affinities: {green: 1, red: -2}};
    }

    return settlement;
}

function addResourceStorageToSettlement(settlement, expansions, config = Resources, itemConfig = null) {
    for(var expansion in expansions) {
        if(expansions[expansion] === "cards") {
            continue;
        }
        settlement = addStorageToSettlement(settlement, "resources", config[expansion], getResourceConfig(), itemConfig);
    }

    return settlement;
}

function addGearStorageToSettlement(settlement, expansions, config = Gear, itemConfig = null) {
    for(var expansion in expansions) {
        if(expansions[expansion] === "cards") {
            continue;
        }

        settlement = addStorageToSettlement(settlement, "gear", config[expansion], getGearConfig(), itemConfig);
    }

    return settlement;
}

export function addCursedGearToSettlement(settlement, expansions, config = Gear) {
    let gearConfig = getGearConfig();
    for(var expansion in expansions) {
        let gear = config[expansion];

        for(let location in gear) {
            for(let item in gear[location]) {
                if(gearConfig[location][item].cursed === true) {
                    let found = false;
                    for(let i = 0; i < settlement.allCursedItems.length; i++) {
                        if(settlement.allCursedItems[i].location === location && settlement.allCursedItems[i].item === item) {
                            found = true;
                        }
                    }

                    if(!found) {
                        settlement.allCursedItems.push({location, item});
                    }
                }
            }
        }

    }

    settlement.allCursedItems.sort((a, b) => {
        if(gearConfig[a.location][a.item].title > gearConfig[b.location][b.item].title) {
            return 1;
        } else if(gearConfig[a.location][a.item].title < gearConfig[b.location][b.item].title) {
            return -1;
        } else {
            return 0;
        }
    });

    return settlement;
}

export function addStorageToSettlement(settlement, type, storage, config = null, itemConfig = null) {
    if(!storage) {
        return settlement;
    }

    settlement.storage[type] = {...settlement.storage[type]};

    storage = JSON.parse(JSON.stringify(storage));

    if(itemConfig) {
        for(let location in storage) {
            for(let id in storage[location]) {
                if(itemConfig[location] && itemConfig[location][id]) {
                    if(itemConfig[location][id].skipAdd === true) {
                        delete storage[location][id];
                    }

                    if(itemConfig[location][id].replace) {
                        const item = itemConfig[location][id].replace;
                        if(settlement.storage[type][item.location] && settlement.storage[type][item.location][item.id] !== undefined) {
                            delete settlement.storage[type][item.location][item.id];
                        }
                    }
                }
            }
        }
    }

    for (const key in storage) {
        // doesn't overwrite so that we can add expansions on the fly after settlement creation
        if(!settlement.storage[type][key]) {
            settlement.storage[type][key] = storage[key];
            settlement.storage[type][key] = sortStorage(settlement.storage[type], key, config);
        } else {
            // will add in missing items to locations or types that already exist
            for (const item in storage[key]) {
                if(!settlement.storage[type][key][item]) {
                    settlement.storage[type][key][item] = storage[key][item];
                }
            }
            settlement.storage[type][key] = sortStorage(settlement.storage[type], key, config);
        }
    }

    return settlement;
}

export function sortStorage(storage, key, config = null) {
    let sorted = {};
    let keys = Object.keys(storage[key]);
    if(config === null) {
        keys = keys.sort();
    } else {
        keys = keys.sort((a, b) => {
            if(!config[key]) {
                return 0;
            }

            let aTitle = config[key][a] ? config[key][a].title : a;
            let bTitle = config[key][b] ? config[key][b].title : b;

            if(aTitle > bTitle) {
                return 1;
            } else if(aTitle < bTitle) {
                return -1;
            } else {
                return 0;
            }
        });
    }
    for(let i = 0; i < keys.length; i++) {
        sorted[keys[i]] = storage[key][keys[i]];
    }
    return sorted;
}

function addConfigToSettlement(settlement, type, config, expansions, itemConfig = null) {
    let list = [];

    for(let expansion in expansions) {
        // if the expansion only adds cards then we whitelist
        if(expansions[expansion] === "cards") {
            if(!(
                type === "allDisorders" ||
                type === "allInnovations" ||
                type === "allFightingArts" ||
                type === "allProficiencies"
            )) {
                continue;
            }
        }

        if(config[expansion]) {
            if(itemConfig) {
                list = config[expansion].filter((name) => {
                    return (itemConfig[name] && itemConfig[name].skipAdd === true) ? false : true;
                });
            } else {
                list = list.concat(config[expansion]);
            }
        }
    }

    if(settlement[type] && settlement[type].length > 0) {
        // remove items from the settlement if this new item replaces it
        if(itemConfig) {
            for(let i = 0; i < list.length; i++) {
                if(itemConfig[list[i]] && itemConfig[list[i]].replace) {
                    if(settlement[type].indexOf(itemConfig[list[i]].replace) !== -1) {
                        settlement[type].splice(settlement[type].indexOf(itemConfig[list[i]].replace), 1);
                    }
                }
            }
        }

        // filter out things that are already in there just in case
        let toAdd = list.filter( function( item ) {
            return settlement[type].indexOf( item ) < 0;
        });

        settlement[type] = settlement[type].concat(toAdd);
    } else {
        settlement[type] = list;
    }

    return settlement;
}

function addMilestonesToSettlement(settlement, expansions, config) {
    for(let expansion in expansions) {
        if(!config[expansion]) {
            continue;
        }
        if(expansions[expansion] === "cards") {
            continue;
        }

        let milestones = config[expansion];

        for(let milestoneId in milestones) {
            if(milestones[milestoneId].type === MILESTONE_TYPE_SETTLEMENT) {
                if(!settlement.milestones[milestoneId]) {
                    settlement.milestones[milestoneId] = {
                        id: milestoneId,
                        complete: false,
                        conditions: {}
                    };
                }

                if(milestones[milestoneId].conditions) {
                    for(let conditionId in milestones[milestoneId].conditions) {
                        if(!settlement.milestones[milestoneId].conditions[conditionId]) {
                            settlement.milestones[milestoneId].conditions[conditionId] = {
                                value: 0,
                                complete: false,
                            }
                        }
                    }
                }
            }
        }
    }

    return settlement;
}

function addLocationsToSettlement(settlement, expansions, config = Locations, itemConfig = null) {
    return addConfigToSettlement(settlement, "allLocations", config, expansions, itemConfig);
}

function addInnovationsToSettlement(settlement, expansions, config = Innovations, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allInnovations", config, expansions, itemConfig);

    if(expansions["manhunter"] && expansions["manhunter"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", "crimsonCandy");
    }

    if(expansions["lion-god"] && expansions["lion-god"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", "theKnowledgeWorm");
    }

    if(expansions["spidicules"] && expansions["spidicules"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", ["silkRefining", "leglessBall"]);
    }

    if(expansions["gorm"] && expansions["gorm"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", ["nigredo", "albedo", "citrinitas", "rubedo"]);
    }

    if(expansions["lion-knight"] && expansions["lion-knight"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", ["blackMask", "whiteMask", "stoicStatue"]);
    }

    if(expansions["dung-beetle-knight"] && expansions["dung-beetle-knight"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", "subterraneanAgriculture");
    }

    if(expansions["slenderman"] && expansions["slenderman"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", "darkWaterResearch");
    }

    if(expansions["sunstalker"] && expansions["sunstalker"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allInnovations", "filletingTable");
    }

    return settlement;
}

function addProficienciesToSettlement(settlement, expansions, config = Proficiencies, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allProficiencies", config, expansions, itemConfig);

    return settlement;
}

export function addArmorSetsToSettlement(settlement, expansions, config = Armor, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allArmorSets", config, expansions, itemConfig);

    return settlement;
}

function addFightingArtsToSettlement(settlement, expansions, config = FightingArts, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allFightingArts", config, expansions, itemConfig);

    if(expansions["dung-beetle-knight"] && expansions["dung-beetle-knight"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allFightingArts", "beetleStrength");
    }

    if(expansions["spidicules"] && expansions["spidicules"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allFightingArts", "silkSurgeon");
    }

    if(expansions["flower-knight"] && expansions["flower-knight"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allFightingArts", ["fencing", "acanthusDoctor", "trueBlade"]);
    }

    if(expansions["lion-god"] && expansions["lion-god"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allFightingArts", "necromancer");
    }

    if(expansions["manhunter"] && expansions["manhunter"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allFightingArts", "eternalWill");
    }

    return settlement;
}

function addDisordersToSettlement(settlement, expansions, config = Disorders, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allDisorders", config, expansions, itemConfig);
    if(expansions["flower-knight"] && expansions["flower-knight"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allDisorders", "flowerAddiction");
    }

    return settlement;
}

function addAbilitiesToSettlement(settlement, expansions, config = Abilities, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allAbilities", config, expansions, itemConfig);

    if(expansions["dragon-king"]) {
        settlement = addItemToSettlementAttr(settlement, "allAbilities", ["spec-scythe", "mastery-scythe"]);
    }

    return settlement;
}

function addSettlementEventsToSettlement(settlement, expansions, config = SettlementEvents, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allSettlementEvents", config, expansions, itemConfig);

    return settlement;
}

function addNemesisEncountersToSettlement(settlement, expansions, config = NemesisEncounters) {
    settlement = addConfigToSettlement(settlement, "allNemesisEncounters", config, expansions);

    return settlement;
}

function addShowdownsToSettlement(settlement, expansions, config = Showdowns, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allShowdowns", config, expansions);
    if(itemConfig) {
        for(let expansion in config) {
            for(let i = 0; i < config[expansion].length; i++) {
                if(itemConfig[config[expansion][i]]) {
                    if(itemConfig[config[expansion][i]].quarry) {
                        settlement.quarryShowdowns = addItemToAll(settlement.quarryShowdowns, itemConfig[config[expansion][i]].name);
                    } else {
                        settlement.quarryShowdowns = removeItemFromAll(settlement.quarryShowdowns, itemConfig[config[expansion][i]].name);
                    }
                }
            }
        }
    }
    return settlement;
}

function addStoryEventsToSettlement(settlement, expansions, config = StoryEvents) {
    settlement = addConfigToSettlement(settlement, "allStoryEvents", config, expansions);

    if(expansions["flower-knight"] && expansions["flower-knight"] === "all") {
        settlement = addItemToSettlementAttr(settlement, "allStoryEvents", "aWarmVirus");
    }

    return settlement;
}

function addSevereInjuriesToSettlement(settlement, expansions, config = SevereInjuries, itemConfig = null) {
    settlement = addConfigToSettlement(settlement, "allSevereInjuries", config, expansions, itemConfig);
    return settlement;
}

export function setPrinciple(settlement, principle, newPrinciple, oldPrinciple) {
    settlement.principles = {...settlement.principles};
    settlement.principles[principle] = newPrinciple;

    // remove any benefits from the old principle
    if(oldPrinciple !== false) {
        if(PrinciplesConfig[principle][oldPrinciple].settlementEffects) {
            for (const key in PrinciplesConfig[principle][oldPrinciple].settlementEffects) {
                settlement[key] -= PrinciplesConfig[principle][oldPrinciple].settlementEffects[key];
            }
        }
    }

    // add any benefits from the new principle
    if(PrinciplesConfig[principle][newPrinciple].settlementEffects) {
        for (const key in PrinciplesConfig[principle][newPrinciple].settlementEffects) {
            settlement[key] += PrinciplesConfig[principle][newPrinciple].settlementEffects[key];
        }
    }

    return settlement;
}

export function unsetPrinciple(settlement, principle) {
    settlement.principles = {...settlement.principles};
    // remove any settlement benefits
    let current = settlement.principles[principle];
    if(PrinciplesConfig[principle][current] && PrinciplesConfig[principle][current].settlementEffects) {
        for (const key in PrinciplesConfig[principle][current].settlementEffects) {
            settlement[key] -= PrinciplesConfig[principle][current].settlementEffects[key];
        }
    }

    // unset the principle
    settlement.principles[principle] = false;

    return settlement;
}

export function initTimeline(campaign = "PotLantern", expansions = [{"core": "all"}], customTimelines = []) {
    let timeline = [];
    let num = 31;
    if(campaign === "PotSun" || campaign === "PotStars") {
        num = 26;
    }

    for (let i = 0; i < num; i++) {
        timeline.push({
            "settlementEvents": [],
            "storyEvents": [],
            "nemesisEncounters": [],
            "specialShowdowns": [],
            "showdowns": []
        });
    }

    timeline = addCampaignEventsToTimeline(timeline, campaign);
    timeline = addExpansionEventsToTimeline(timeline, expansions, campaign);
    timeline = addCustomTimelinesToTimeline(timeline, customTimelines);

    return timeline;
}

function addCustomTimelinesToTimeline(timeline, customTimelines) {
    for (let i = 0; i < customTimelines.length; i++) {
        for (const ly in customTimelines[i]) {
            if(timeline[ly]) {
                let events = customTimelines[i][ly];
                for (const type in events) {
                    timeline[ly][type] = timeline[ly][type].concat(events[type]);
                }
            }
        }
    }

    return timeline;
}

function addCampaignEventsToTimeline(timeline, campaign) {
    let timelineBase = require('../config/timeline-campaign-base.json');

    let extraTimeline = {};
    // they share a timeline
    if(campaign === "PotBloom") {
        campaign = "PotLantern";
        extraTimeline = {
            "5" : {
                "storyEvents": ["Gain 1 Sleeping Virus Flower rare gear and suffer -1 population"]
            },
            "10" : {
                "storyEvents": ["Gain 1 Sleeping Virus Flower rare gear and suffer -1 population"]
            },
            "15" : {
                "storyEvents": ["Gain 1 Sleeping Virus Flower rare gear and suffer -1 population"]
            },
            "20" : {
                "storyEvents": ["Gain 1 Sleeping Virus Flower rare gear and suffer -1 population"]
            }
        }
    }

    timelineBase = timelineBase[campaign];

    for (const ly in timelineBase) {
        for (const key in timelineBase[ly]) {
            timeline[ly][key] = timelineBase[ly][key];
        }
    }

    if(Object.keys(extraTimeline).length > 0) {
        for (const ly in extraTimeline) {
            for (const key in extraTimeline[ly]) {
                if(timeline[ly]) {
                    timeline[ly][key] = timeline[ly][key].concat(extraTimeline[ly][key]);
                }
            }
        }
    }

    return timeline;
}

function addExpansionEventsToTimeline(timeline, expansions, campaign) {
    let timelineBase = require('../config/timeline-expansions.json');
    for(var expansion in expansions) {
        if(expansions[expansion] === "cards") {
            continue;
        }

        if(timelineBase[expansion]) {
            let expansionEvents = timelineBase[expansion];

            if(expansion === "slenderman") {
                if(campaign === "PotStars") {
                    expansionEvents = {};
                } else if(campaign === "PotSun") {
                    expansionEvents = {
                        removeEvents : {
                            21 : {"nemesisEncounters" : ["kingsManLevel2"]},
                            23 : {"nemesisEncounters" : ["kingsManLevel3"]},
                        },
                        21 : {"nemesisEncounters" : ["Nemesis Encounter"]},
                        23 : {"nemesisEncounters" : ["Nemesis Encounter"]},
                    }
                }
            }

            for (const ly in expansionEvents) {
                if(ly == "removeEvents") {
                    timeline = removeEventsFromTimeline(timeline, expansionEvents[ly]);
                    continue;
                }

                for (const key in expansionEvents[ly]) {
                    if(timeline[ly]) {
                        timeline[ly][key] = timeline[ly][key].concat(expansionEvents[ly][key]);
                    }
                }
            }
        }
    }

    return timeline;
}

export function addExpansionsToSettlement(settlement, expansions) {
    settlement = addExpansionContentToSettlement(settlement, expansions);

    for(let expansion in expansions) {
        settlement.expansions[expansion] = expansions[expansion];
    }

    settlement.storage.resources = {...settlement.storage.resources};
    settlement.storage.gear = {...settlement.storage.gear};

    return settlement;
}

function removeEventsFromTimeline(timeline, events) {
    for (const ly in events) {
        for (const key in events[ly]) {
            for(var i = 0; i < events[ly][key].length; i++) {
                if(timeline[ly]) {
                    if(timeline[ly][key].indexOf(events[ly][key][i]) !== -1) {
                        timeline[ly][key].splice(timeline[ly][key].indexOf(events[ly][key][i]), 1);
                    }
                }
            }
        }
    }

    return timeline;
}

export function addAbility(survivor, name) {
    let abilities = getAbilityConfig();
    if(!abilities[name]) {
        return survivor;
    }

    if(survivor.abilities.indexOf(name) === -1) {
        survivor.abilities = [...survivor.abilities, name];
        survivor = applySurvivorEffects(survivor, abilities[name]);
    }

    if(name === "pristine") {
        survivor = setConstellationMap(survivor, true, 0, 3);
    } else if(name === "oraclesEye") {
        survivor = setConstellationMap(survivor, true, 3, 0);
    } else if(name === "iridescentHide") {
        survivor = setConstellationMap(survivor, true, 1, 2);
    }

    return survivor;
}

export function removeAbility(survivor, name) {
    let abilities = getAbilityConfig();
    survivor.abilities = [...survivor.abilities];
    survivor.abilities.splice(survivor.abilities.indexOf(name), 1);
    survivor = removeSurvivorEffects(survivor, abilities[name]);
    return survivor;
}

export function addFightingArt(survivor, name) {
    let fightingArts = getFightingArtConfig();
    survivor.fightingArts = [...survivor.fightingArts, name];
    survivor = applySurvivorEffects(survivor, fightingArts[name]);
    return survivor;
}

export function removeFightingArt(survivor, name) {
    let fightingArts = getFightingArtConfig();
    survivor.fightingArts = [...survivor.fightingArts];
    survivor.fightingArts.splice(survivor.fightingArts.indexOf(name), 1);
    survivor = removeSurvivorEffects(survivor, fightingArts[name]);
    return survivor;
}

export function addDisorder(survivor, name) {
    let disorders = getDisorderConfig();
    survivor.disorders = [...survivor.disorders, name];
    survivor = applySurvivorEffects(survivor, disorders[name]);
    return survivor;
}

export function removeDisorder(survivor, name) {
    let disorders = getDisorderConfig();
    survivor.disorders = [...survivor.disorders];
    survivor.disorders.splice(survivor.disorders.indexOf(name), 1);
    survivor = removeSurvivorEffects(survivor, disorders[name]);
    return survivor;
}

export function addSevereInjury(survivor, name) {
    let injuries = getSevereInjuryConfig();
    let config = {...injuries[name]};
    if(name === "blind" && survivor.severeInjuries.indexOf(name) !== -1) {
        config.survivorEffects = {attributes: {"ACC": -3}, retired: true};
    }
    if(name === "dismemberedLeg" && survivor.severeInjuries.indexOf(name) !== -1) {
        config.survivorEffects = {attributes: {"MOV": -2}, retired: true};
    }
    survivor.severeInjuries = [...survivor.severeInjuries, name];
    survivor = applySurvivorEffects(survivor, config);
    return survivor;
}

export function removeSevereInjury(survivor, name) {
    let injuries = getSevereInjuryConfig();
    survivor.severeInjuries = [...survivor.severeInjuries];
    let config = {...injuries[name]};
    survivor.severeInjuries.splice(survivor.severeInjuries.indexOf(name), 1);
    if(name === "blind" && survivor.severeInjuries.indexOf(name) !== -1) {
        config.survivorEffects = {attributes: {"ACC": -3}, retired: true};
    }
    if(name === "dismemberedLeg" && survivor.severeInjuries.indexOf(name) !== -1) {
        config.survivorEffects = {attributes: {"MOV": -2}, retired: true};
    }
    survivor = removeSurvivorEffects(survivor, config);
    return survivor;
}

function applySurvivorEffects(survivor, config) {
    if(config.survivorEffects) {
        survivor = applyStatChanges(survivor, config.survivorEffects);
    }

    return survivor;
}

export function applyStatChanges(survivor, changes) {
    if(changes) {
        for (const attr in changes) {
            if(attr === "abilities") {
                for(let i = 0; i < changes[attr].length; i++) {
                    survivor = addAbility(survivor, changes[attr][i]);
                }
                continue;
            }

            if(attr === "disorders") {
                for(let i = 0; i < changes[attr].length; i++) {
                    survivor = addDisorder(survivor, changes[attr][i]);
                }
                continue;
            }

            if(attr === "fightingArts") {
                for(let i = 0; i < changes[attr].length; i++) {
                    survivor = addFightingArt(survivor, changes[attr][i]);
                }
                continue;
            }

            if(typeof changes[attr] == "object") {
                for (const stat in changes[attr]) {
                    if(typeof changes[attr][stat] == "number") {
                        survivor[attr][stat] += changes[attr][stat];
                    } else if (typeof changes[attr][stat] == "boolean") {
                        survivor[attr][stat] = changes[attr][stat];
                    }
                }
            } else if(typeof changes[attr] == "number"){
                survivor[attr] += changes[attr];
            } else if(typeof changes[attr] == "boolean"){
                survivor[attr] = changes[attr];
            }
        }
    }

    return survivor;
}

export function removeStatChanges(survivor, changes) {
    if(changes) {
        for (const attr in changes) {
            if(attr === "abilities") {
                for(let i = 0; i < changes[attr].length; i++) {
                    survivor = removeAbility(survivor, changes[attr][i]);
                }
                continue;
            }

            if(attr === "disorders") {
                for(let i = 0; i < changes[attr].length; i++) {
                    survivor = removeDisorder(survivor, changes[attr][i]);
                }
                continue;
            }

            if(attr === "fightingArts") {
                for(let i = 0; i < changes[attr].length; i++) {
                    survivor = removeFightingArt(survivor, changes[attr][i]);
                }
                continue;
            }

            if(typeof changes[attr] == "object") {
                for (const stat in changes[attr]) {
                    if(typeof changes[attr][stat] == "number") {
                        survivor[attr][stat] -= changes[attr][stat];
                    } else if (typeof changes[attr][stat] == "boolean") {
                        survivor[attr][stat] = !changes[attr][stat];
                    }
                }
            } else if(typeof changes[attr] == "number"){
                survivor[attr] -= changes[attr];
            } else if(typeof changes[attr] == "boolean"){
                survivor[attr] = !changes[attr];
            }
        }
    }

    return survivor;
}

function removeSurvivorEffects(survivor, config) {
    if(config.survivorEffects) {
        survivor = removeStatChanges(survivor, config.survivorEffects);
    }

    return survivor;
}

export function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

export function getColorCode(code, colors) {
    if(code === "red") {
        return colors.RED;
    } else if(code === "blue") {
        return colors.BLUE
    } else if(code === "green") {
        return colors.GREENDARK
    } else if(code === "brown") {
        return colors.BROWN
    } else if(code === "gray") {
        return colors.GRAY
    } else if(code === "purple") {
        return colors.PURPLE
    } else if(code === "white") {
        return colors.WHITE
    } else if(code === "yellow") {
        return colors.YELLOW
    } else if(code === "orange") {
        return colors.ORANGE
    } else if(code === "teal") {
        return colors.TEAL
    } else {
        return code;
    }
}

export function getExpansionTitle(expansion, customExpansions = {}) {
    // special case for the core since it's not an expansion
    if(expansion === "core") {
        return "Core";
    }

    if(customExpansions[expansion]) {
        return customExpansions[expansion].name;
    }

    return expansions[expansion];
}

export function getExpansionAbbreviation(expansion) {
    return expansionAbbreviations[expansion];
}

export function getExpansionLabeledTitle(config) {
    if(!config.expansion) {
        return config.title;
    }

    return "("+ getExpansionAbbreviation(config.expansion) +") " + config.title;
}

export function getExpansionList() {
    return Object.keys(expansions);
}

export function getCampaignList() {
    return Object.keys(campaigns);
}

export function getCampaignConfig() {
    return campaigns;
}

export function getCampaignTitle(campaign) {
    return campaigns[campaign].title;
}

export function setConstellationMap(survivor, val, row, col, force = false) {
    if(survivor.constellation === undefined) {
        return survivor;
    }

    if(survivor.constellation !== "" && !force) {
        return survivor;
    }

    survivor.constellationMap = [...survivor.constellationMap];

    survivor.constellationMap[row][col] = val;

    if(survivor.constellation === "") {
        survivor = checkForConstellation(survivor);
    }

    return survivor;
}

function checkForConstellation(survivor) {
    let map = survivor.constellationMap;
    let foundRow = false;
    let foundCol = false;

    for(let i = 0; i < map.length; i++) {
        foundRow = i;
        for (let j = 0; j < map[i].length; j++) {
            if(!map[i][j]) {
                foundRow = false;
                break;
            }
        }

        if(foundRow !== false) {
            break;
        }
    }

    for(let i = 0; i < map.length; i++) {
        foundCol = i;
        for (let j = 0; j < map[i].length; j++) {
            if(!map[j][i]) {
                foundCol = false;
                break;
            }
        }

        if(foundCol !== false) {
            break;
        }
    }

    if(foundCol !== false) {
        let colMap = getConstellationCols();
        survivor = setSurvivorConstellation(survivor, colMap[foundCol]);
    } else if(foundRow !== false) {
        let rowMap = getConstellationRows();
        survivor = setSurvivorConstellation(survivor, rowMap[foundRow]);
    }

    return survivor;
}

export function setSurvivorConstellation(survivor, constellation) {
    let removeOnly = survivor.constellation == constellation;

    if(survivor.constellation !== "") {
        // remove previous constellation effects
        survivor = removeAbility(survivor, "constellation"+constellation);
        if(removeOnly) {
            survivor.constellation = "";
        }
    }

    if(!removeOnly) {
        survivor = addSurvivorConstellation(survivor, constellation);
    }


    return survivor;
}

function addSurvivorConstellation(survivor, constellation) {
    survivor.constellation = constellation;

    survivor = addAbility(survivor, "constellation"+constellation);

    return survivor;
}

export function getConstellationRows() {
    return ["Gambler", "Absolute", "Sculptor", "Goblin"];
}

export function getConstellationCols() {
    return ["Witch", "Rust", "Storm", "Reaper"];
}

export function toCamelCase (str) {
    return str.toLowerCase().replace(/[^a-z\? ]/ig, '').replace(/(?:^\w|[A-Z]|\b\w)/g, (ltr, idx) => idx === 0 ? ltr.toLowerCase() : ltr.toUpperCase()).replace(/\s+/g, '');
}

export function toFileSafeName(str) {
    return str.toLowerCase().replace(/[^a-z ]/ig, '').replace(/ +/ig, '-');
}

export function capitalizeFirst(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

function addItemToSettlementAttr(settlement, attr, item) {
    if(!Array.isArray(item)) {
        item = [item];
    }

    let toAdd = item.filter( function( el ) {
        return settlement[attr].indexOf( el ) < 0;
    });

    settlement[attr] = settlement[attr].concat(toAdd);

    return settlement;
}

export function getExpansionParensForStoryEvent(event) {
    let config = getStoryEventConfig();

    if(config[event] && config[event].expansion && expansions[config[event].expansion]) {
        return " ("+expansions[config[event].expansion]+")";
    }

    return "";
}

export function getInnovationConfig() {
    if(Object.keys(customInnovationConfig).length > 0) {
        return {...InnovationConfig, ...customInnovationConfig};
    }

    return InnovationConfig;
}

export function getLocationConfig() {
    if(Object.keys(customLocationConfig).length > 0) {
        return {...LocationConfig, ...customLocationConfig};
    }

    return LocationConfig;
}

export function getBaseLocationConfig() {
    return LocationConfig;
}

export function getBaseResourceConfig() {
    return ResourceConfig;
}

export function getBaseGearConfig() {
    return GearConfig;
}

export function getAbilityConfig() {
    if(Object.keys(customAbilityConfig).length > 0) {
        return {...AbilityConfig, ...customAbilityConfig};
    }

    return AbilityConfig;
}

export function getDisorderConfig() {
    if(Object.keys(customDisorderConfig).length > 0) {
        return {...DisorderConfig, ...customDisorderConfig};
    }

    return DisorderConfig;
}

export function getFightingArtConfig() {
    if(Object.keys(customFightingArtConfig).length > 0) {
        return {...FightingArtConfig, ...customFightingArtConfig};
    }

    return FightingArtConfig;
}

export function getSevereInjuryConfig() {
    if(Object.keys(customSevereInjuryConfig).length > 0) {
        return {...SevereInjuryConfig, ...customSevereInjuryConfig};
    }

    return SevereInjuryConfig;
}

export function getWeaponProfConfig() {
    if(Object.keys(customWeaponProfConfig).length > 0) {
        return {...WeaponProfConfig, ...customWeaponProfConfig};
    }

    return WeaponProfConfig;
}

export function getSettlementEventConfig() {
    if(Object.keys(customSettlementEventConfig).length > 0) {
        return {...SettlementEventConfig, ...customSettlementEventConfig};
    }

    return SettlementEventConfig;
}

export function getStoryEventConfig() {
    if(Object.keys(customStoryEventConfig).length > 0) {
        return {...StoryEventConfig, ...customStoryEventConfig};
    }

    return StoryEventConfig;
}

export function getShowdownConfig() {
    if(Object.keys(customShowdownConfig).length > 0) {
        return {...ShowdownConfig, ...customShowdownConfig};
    }

    return ShowdownConfig;
}

export function getResourceConfig() {
    if(Object.keys(customResourceConfig).length > 0) {
        return mergeStorage(ResourceConfig, customResourceConfig);
    }

    return ResourceConfig;
}

export function getGearConfig() {
    if(Object.keys(customGearConfig).length > 0) {
        return mergeStorage(GearConfig, customGearConfig);
    }

    return GearConfig;
}

export function getArmorConfig() {
    if(Object.keys(customArmorConfig).length > 0) {
        return {...ArmorConfig, ...customArmorConfig};
    }

    return ArmorConfig;
}

export function getPrinciplesConfig() {
    if(Object.keys(customPrinciplesConfig).length > 0) {
        return {...PrinciplesConfig, ...customPrinciplesConfig};
    }

    return PrinciplesConfig;
}

export function getMilestoneConfig() {
    if(Object.keys(customMilestoneConfig).length > 0) {
        return {...MilestoneConfig, ...customMilestoneConfig};
    }

    return MilestoneConfig;
}

function mergeStorage(original, addition) {
    original = JSON.parse(JSON.stringify(original));
    for(let location in addition) {
        if(!original[location]) {
            original[location] = {...addition[location]};
            continue;
        }

        for (const item in addition[location]) {
            original[location][item] = {...addition[location][item]};
        }
    }

    return original;
}

export function getNumDragonTraits(set) {
    let num = 0;

    for(let i = 0; i < set.length; i++) {
        for(let j = 0; j < set[i].length; j++) {
            if(set[i][j] === true) {
                num++;
            }
        }
    }

    return num;
}

export function expansionHasSettlementSection(exp) {
    let has = false;

    if(
        (exp.innovations && exp.innovations.length > 0) ||
        (exp.locations && exp.locations.length > 0) ||
        (exp.resourceConfig && Object.keys(exp.resourceConfig).length > 0) ||
        (exp.gearConfig && Object.keys(exp.gearConfig).length > 0)
    ) {
        has = true;
    }

    return has;
}

export function expansionHasSurvivorSection(exp) {
    let has = false;

    if(
        (exp.fightingArts && exp.fightingArts.length > 0) ||
        (exp.disorders && exp.disorders.length > 0) ||
        (exp.severeInjuries && exp.severeInjuries.length > 0) ||
        (exp.weaponProfs && exp.weaponProfs.length > 0) ||
        (exp.abilities && exp.abilities.length > 0)
    ) {
        has = true;
    }

    return has;
}

export function expansionHasTimelineSection(exp) {
    let has = false;

    if(
        (exp.settlementEvents && exp.settlementEvents.length > 0) ||
        (exp.storyEvents && Object.keys(exp.storyEvents).length > 0) ||
        (exp.showdowns && Object.keys(exp.showdowns).length > 0) ||
        (exp.nemesisEncounters && Object.keys(exp.nemesisEncounters).length > 0)
    ) {
        has = true;
    }

    return has;
}

export function getEndeavorList(locations, innovations, globals) {
    let InnovationConfig = getInnovationConfig();
    let LocationConfig = getLocationConfig();
    let list = [];

    for(let i = 0; i < locations.length; i++) {
        let endeavors = LocationConfig[locations[i]].settlementEffects ? LocationConfig[locations[i]].settlementEffects.endeavors : false;
        if(endeavors) {
            let item = {title: LocationConfig[locations[i]].title, endeavors, tags: [], type: "location"};
            list.push(item);
        }
    }

    for(let i = 0; i < innovations.length; i++) {
        let config = InnovationConfig[innovations[i]];
        let endeavors = config.settlementEffects ? config.settlementEffects.endeavors : false;
        if(endeavors) {
            let item = {title: config.title, endeavors, tags: (config.tags ? config.tags : []), type: "innovation"};
            list.push(item);
        }
    }

    list.sort((a, b) => {
        if(a.title > b.title) {
            return 1;
        } else if(a.title < b.title) {
            return -1;
        } else {
            return 0;
        }
    });

    let globalEndeavors = [];
    if(globals.length > 0) {
        globalEndeavors = [{title: t("Settlement"), endeavors: globals, type: "global"}];
    }

    list = globalEndeavors.concat(list);

    return list;
}

export function getModdableSurvivorStats() {
    return {
        "attributes.STR": {"name" : "attributes.STR", "title": t("Strength"), "type": PropTypes.number},
        "attributes.ACC": {"name" : "attributes.ACC", "title": t("Accuracy"), "type": PropTypes.number},
        "attributes.MOV": {"name" : "attributes.MOV", "title": t("Movement"), "type": PropTypes.number},
        "attributes.LCK": {"name" : "attributes.LCK", "title": t("Luck"), "type": PropTypes.number},
        "attributes.EVA": {"name" : "attributes.EVA", "title": t("Evasion"), "type": PropTypes.number},
        "attributes.SPD": {"name" : "attributes.SPD", "title": t("Speed"), "type": PropTypes.number},
        "insanity.value": {"name" : "insanity.value", "title": t("Insanity"), "type": PropTypes.number},
        "survival": {"name" : "survival", "title": t("Survival"), "type": PropTypes.number},
        "courage": {"name" : "courage", "title": t("Courage"), "type": PropTypes.number},
        "understanding": {"name" : "understanding", "title": t("Understanding"), "type": PropTypes.number},
        "xp": {"name" : "xp", "title": t("Hunt XP"), "type": PropTypes.number},
        "skipNextHunt": {"name" : "skipNextHunt", "title": t("Skip Next Hunt"), "type": PropTypes.bool, "defaultVal": true},
    };
}

export function getSettlementsWithCustomExpansion(settlements, expansionId) {
    if(!Array.isArray(settlements)) {
        settlements = Object.values(settlements);
    }

    let ids = [];

    for(let i = 0; i < settlements.length; i++) {
        if(settlements[i].customExpansions && settlements[i].customExpansions[expansionId]) {
            ids.push(settlements[i]);
        }
    }

    return ids;
}

export function getImageWidthFromHeight(height) {
    return Math.ceil(height * IMAGE_RATIO);
}

export function getHeightFromPercentage(perc) {
    return Dimensions.get('window').height * perc;
}

export function getWidthFromPercentage(perc) {
    return Dimensions.get('window').width * perc;
}

export function getImageWidthFromHeightPercentage(perc) {
    return Dimensions.get('window').height * perc * IMAGE_RATIO;
}

export function padNumToDigits(num, digits = 2) {
    num = "" + num;

    return num.length >= digits ? num : new Array(digits - num.length + 1).join("0") + num;
}

export function updateEventsTo17(events, config) {
    let newEvents = [];

    for (let i = 0; i < events.length; i++) {
        let found = false;
        for (const key in config) {
            if(config[key].title === events[i]) {
                newEvents.push(key);
                found = true;
            }
        }

        if(events[i] === "Pheonix Feather") {
            found = true;
            newEvents.push("phoenixFeather");
        }

        if(events[i] === "Regal Visitors") {
            found = true;
            newEvents.push("regalVisit");
        }

        if(events[i] === "Tomb") {
            found = true;
            newEvents.push("theTomb");
        }

        if(events[i] === "Nemesis Encounter") {
            found = true;
            newEvents.push("Nemesis Encounter");
        }

        if(events[i] === "Nemesis Level 2") {
            found = true;
            newEvents.push("Nemesis Level 2");
        }

        if(events[i] === "Nemesis Level 3") {
            found = true;
            newEvents.push("Nemesis Level 3");
        }

        if(events[i] === "It's already here") {
            found = true;
            newEvents.push("itsAlreadyHere");
        }

        if(events[i] === "The Silver City") {
            found = true;
            newEvents.push("theSilverCity");
        }

        if(events[i] === "Kings Man Level 1") {
            found = true;
            newEvents.push("kingsManLevel1");
        }

        if(events[i] === "Kings Man Level 2") {
            found = true;
            newEvents.push("kingsManLevel2");
        }

        if(events[i] === "Kings Man Level 3") {
            found = true;
            newEvents.push("kingsManLevel3");
        }

        if(!found) {
            newEvents.push(Uuid.v4({random: getUuidSeedFromString(events[i])}));
        }
    }

    return newEvents;
}

export function getUuidSeedFromString(str) {
    let seed = [];

    if(str.length < 16) {
        str = ("                " + str).slice(-16);
    }

    // take first 8 and last 8 to form seed string
    let first = str.slice(0, 8);
    let last = str.slice(-8);
    str = first + last;

    for(let i = 0; i < str.length; i++) {
        seed.push(str.charCodeAt(i) % 256);
    }

    return seed;
}

export function getAbilitiesByType(list, type) {
    let config = getAbilityConfig();
    let filteredAbilities = list.filter((ability) => {
        if(config[ability].type === type) {
            return true;
        }

        return false;
    });

    let allAbilities = {};
    for(let i = 0; i < filteredAbilities.length; i++) {
        allAbilities[filteredAbilities[i]] = config[filteredAbilities[i]];
    }

    return allAbilities;
}

export function getEventConfigForType(type) {
    if(type === "settlementEvents") {
        return getSettlementEventConfig();
    } else if(type === "storyEvents") {
        return getStoryEventConfig();
    } else if(type === "nemesisEncounters" || type === "specialShowdowns" || type === "showdowns") {
        return getShowdownConfig();
    } else {
        return null;
    }
}

export function getEventTitleForType(type) {
    if(type === "settlementEvents") {
        return t("Settlement Event");
    } else if(type === "storyEvents") {
        return t("Story Event");
    } else if(type === "nemesisEncounters") {
        return t("Nemesis Encounter");
    } else if(type === "specialShowdowns") {
        return t("Special Showdown");
    } else if(type === "showdowns") {
        return t("Showdown");
    } else {
        return null;
    }
}

export function getEventTitlePluralForType(type) {
    if(type === "settlementEvents") {
        return t("Settlement Events");
    } else if(type === "storyEvents") {
        return t("Story Events");
    } else if(type === "nemesisEncounters") {
        return t("Nemesis Encounters");
    } else if(type === "specialShowdowns") {
        return t("Special Showdowns");
    } else if(type === "showdowns") {
        return t("Showdowns");
    } else {
        return null;
    }
}

export function addActionLog(logs, newLog, force = false) {
    if(logs.length > 0) {
        const prev = logs[logs.length - 1];
        if(!force && prev.m === newLog.m) {
            logs.splice(logs.length - 1, 1);
        }
    }
    logs.push(newLog);

    return logs;
}

export function getPrincipleName(principle) {
    if(principle === "newLife") {
        return "New Life";
    } else if(principle === "death") {
        return "Death"
    } else if(principle === "conviction") {
        return "Conviction"
    } else if(principle === "society") {
        return "Society"
    }
}

export function getLocationTitle(name, config) {
    if(config[name]) {
        return config[name].title;
    }

    return name;
}

export function findItemsNotInConfig(items, config) {
    if(!Array.isArray(items)) {
        return false;
    }

    let not = [];

    for(let i = 0; i < items.length; i++) {
        if(!config[items[i]]) {
            not.push(items[i]);
        }
    }

    if(not.length > 0) {
        return not;
    }

    return false;
}

export function findItemsNotInStorageConfig(storage, config) {
    let not = [];

    for(let location in storage) {
        for(let id in storage[location]) {
            if(!config[location] || !config[location][id]) {
                not.push({location, id});
            }
        }
    }

    if(not.length > 0) {
        return not;
    }

    return false;
}

/**
 * Meant to be used with the return value of findItemsNotInStorageConfig above
 * @param {array} items list of items to remove ([{location: "", id: ""}...])
 * @param {object} storage resource or gear storage to edit
 */
export function removeStorageItemsFromList(items, storage) {
    for(let i = 0; i < items.length; i++) {
        if(storage[items[i].location] && storage[items[i].location][items[i].id] !== undefined) {
            delete storage[items[i].location][items[i].id];
            if(Object.keys(storage[items[i].location]).length === 0) {
                delete storage[items[i].location];
            }
        }
    }

    return storage;
}

export function removeItemsFromList(items, list) {
    for(let i = 0; i < items.length; i++) {
        if(list.indexOf(items[i]) !== -1) {
            list.splice(list.indexOf(items[i]), 1);
        }
    }

    return list;
}

export function allConditionsComplete(milestone) {
    if(!milestone.conditions && !milestone.settlementConditions) {
        return false;
    }

    let complete = true;

    if(milestone.conditions) {
        for(let id in milestone.conditions) {
            if(!milestone.conditions[id].complete) {
                complete = false;
                break;
            }
        }
    }

    if(complete && milestone.settlementConditions) {
        for(let id in milestone.settlementConditions) {
            if(!milestone.settlementConditions[id].complete) {
                complete = false;
                break;
            }
        }
    }

    return complete;
}

export function isObjectEmpty(obj) {
    if(obj && typeof obj === "object" && Object.keys(obj).length > 0) {
        return false;
    }

    return true;
}

export function getCompletedLoadoutAffinities(survivor) {
    let affinities = {
        red: 0,
        blue: 0,
        green: 0,
    };

    if(!survivor.loadout || !survivor.loadout.gear) {
        return affinities;
    }

    const addToAll = (affinities, num = 1) => {
        for (const color in affinities) {
            affinities[color] += num;
        }
        return affinities
    }

    let prismatic = survivor.loadout.armorSet === "cycloid" ? true : false;

    let gearGrid = survivor.loadout.gear;
    const GearConfig = getGearConfig();
    for (let i = 0; i < gearGrid.length; i++) {
        for (let j = 0; j < gearGrid[i].length; j++) {
            let gear = gearGrid[i][j];
            if(gear) {
                gear = GearConfig[gear.location][gear.name];
            } else {
                continue;
            }

            if(gear.affinities) {
                // bottom
                if(gear.affinities.bottom && i < gearGrid.length - 1 && gearGrid[i + 1][j]) {
                    const otherGear = GearConfig[gearGrid[i + 1][j].location][gearGrid[i + 1][j].name];
                    if(otherGear.affinities && otherGear.affinities.top) {
                        if(prismatic) {
                            affinities = addToAll(affinities);
                        } else {
                            if(gear.affinities.bottom === otherGear.affinities.top) {
                                affinities[gear.affinities.bottom] += 1;
                            }
                        }
                    }
                }

                // right
                if(gear.affinities.right && j < gearGrid[i].length - 1 && gearGrid[i][j + 1]) {
                    const otherGear = GearConfig[gearGrid[i][j + 1].location][gearGrid[i][j + 1].name];
                    if(otherGear.affinities && otherGear.affinities.left) {
                        if(prismatic) {
                            affinities = addToAll(affinities);
                        } else {
                            if(gear.affinities.right === otherGear.affinities.left) {
                                affinities[gear.affinities.right] += 1;
                            }
                        }
                    }
                }

                if(gear.affinities.complete) {
                    for (const color in gear.affinities.complete) {
                        if(prismatic) {
                            affinities = addToAll(affinities, gear.affinities.complete[color]);
                        } else {
                            affinities[color] += gear.affinities.complete[color];
                        }
                    }
                }
            }

        }
    }

    return affinities;
}

export function getMilestoneConditionValue(id, conditionId, config) {
    if(config[id] && config[id].conditions[conditionId]) {
        return config[id].conditions[conditionId].value;
    }

    return Infinity;
}

export function sortStorageByExpansion(storage, config) {
    let sorted = {};

    for(let location in storage) {
        for(let key in storage[location]) {
            const expansion = config[location][key].expansion ? config[location][key].expansion : "core";

            if(!sorted[expansion]) {
                sorted[expansion] = {};
            }

            if(!sorted[expansion][location]) {
                sorted[expansion][location] = {};
            }

            sorted[expansion][location][key] = storage[location][key];
        }
    }

    return sorted;
}

export function sortItemsByExpansion(items, config) {
    let sorted = {};

    for(let i = 0; i < items.length; i++) {
        let expansion = config[items[i]].expansion;
        if(!expansion) {
            expansion = "core";
        }

        if(!sorted[expansion]) {
            sorted[expansion] = [];
        }

        sorted[expansion].push(items[i]);
    }

    return sorted;
}

export function getItemsByExpansion(config) {
    let allItems = Object.keys(config);
    return sortItemsByExpansion(allItems, config);
}

export function getSortedExpansions(expansions) {
    return expansions.sort(sortExpansionsByTitle);
}

function sortExpansionsByTitle(a, b) {
    a = getExpansionTitle(a);
    b = getExpansionTitle(b);

    if(a > b) {
        return 1;
    } else if(a < b) {
        return -1;
    } else {
        return 0;
    }
}

export function getShowdownsFromQuarry(quarry) {
    const ShowdownConfig = getShowdownConfig();
    let showdowns = [];

    for(let showdown in ShowdownConfig) {
        if(ShowdownConfig[showdown].quarry === quarry) {
            showdowns.push(showdown);
        }
    }

    return showdowns;
}

export function getQuarriesForShowdowns(showdowns) {
    const ShowdownConfig = getShowdownConfig();
    let quarries = {};

    for(let i = 0; i < showdowns.length; i++) {
        if(
            ShowdownConfig[showdowns[i]] &&
            ShowdownConfig[showdowns[i]].quarry
        ) {
            quarries[ShowdownConfig[showdowns[i]].quarry] = true;
        }
    }

    return Object.keys(quarries).sort();
}

export function removeItemFromAll(all, item) {
    if(!all) {
        return [];
    }

    if(all.indexOf(item) !== -1) {
        all = [...all];
        all.splice(all.indexOf(item), 1);
    }

    return all;
}

export function addItemToAll(all, item) {
    if(!all) {
        all = [];
    }

    if(all.indexOf(item) === -1) {
        all = [...all];
        all.push(item);
    }

    return all;
}
