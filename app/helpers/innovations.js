import { getInnovationConfig, sortIdArrayByConfigTitle } from './helpers';

export function getInnovationDeck(innovations, allInnovations) {
    let InnovationConfig = getInnovationConfig();
    let filtered = allInnovations.filter((innovation) => {
        if(innovations.indexOf(innovation) !== -1) {
            return false;
        }

        if(InnovationConfig[innovation].default === true) {
            return true;
        }

        if(Array.isArray(InnovationConfig[innovation].consequenceOf)) {
            for(var i = 0; i < InnovationConfig[innovation].consequenceOf.length; i++) {
                if(innovations.indexOf(InnovationConfig[innovation].consequenceOf[i]) > -1) {
                    return true;
                }
            }
        } else if(innovations.indexOf(InnovationConfig[innovation].consequenceOf) > -1) {
            return true;
        }

        return false;
    });

    return sortIdArrayByConfigTitle(filtered, InnovationConfig);
}

/**
 * Pass an innovation (and your current innovations) to get all the new innovations to add
 *
 * @param innovation - string name of innovation to fetch the consequences of
 * @param currentInnovations - array<string> the current innovations in the settlement (to remove dupes)
 * @param allInnovations - array<string> all of the innovations available to the settlement
 */
export function getInnovationConsequences(innovation, currentInnovations, allInnovations) {
    let filtered = allInnovations.filter((inn) => {
        let InnovationConfig = getInnovationConfig();

        if(currentInnovations.indexOf(inn) !== -1) {
            return false;
        }

        if(Array.isArray(InnovationConfig[inn].consequenceOf)) {
            if(InnovationConfig[inn].consequenceOf.indexOf(innovation) !== -1) {
                return true;
            }
        } else if(InnovationConfig[inn].consequenceOf === innovation) {
            return true;
        }

        return false;
    });

    return filtered;
}

export function getAvailableInnovations(innovations, allInnovations) {
    let filtered = allInnovations.filter(innovation => (
        innovations.indexOf(innovation) == -1
    ));

    let InnovationConfig = getInnovationConfig();
    return sortIdArrayByConfigTitle(filtered, InnovationConfig);
}

export function addInnovationToSettlement(settlement, name) {
    // if settlement already has the innovation, short circuit the function
    if(settlement.innovations.indexOf(name) !== -1) {
        return settlement;
    }

    // otherwise, add innovation to settlement
    let InnovationConfig = getInnovationConfig();
    settlement.innovations = [...settlement.innovations, name];

    if(InnovationConfig[name].settlementEffects) {
        let effects = InnovationConfig[name].settlementEffects;

        if(effects.survivalLimit) {
            settlement.survivalLimit += effects.survivalLimit;
        }
    }

    if(settlement.innovationDeck.indexOf(name) !== -1) {
        settlement.innovationDeck.splice(settlement.innovationDeck.indexOf(name), 1);
    }

    // for extra safety make sure the consequences aren't already in the deck
    let consequences = getInnovationConsequences(name, settlement.innovations, settlement.allInnovations);
    consequences = consequences.filter((name) => {
        return (settlement.innovationDeck.indexOf(name) === -1)
    });

    settlement.innovationDeck = settlement.innovationDeck.concat(consequences);

    return settlement;
}

export function removeInnovationFromSettlement(settlement, name) {
    let InnovationConfig = getInnovationConfig();
    settlement.innovations = [...settlement.innovations];
    if(settlement.innovations.indexOf(name) !== -1) {
        settlement.innovations.splice(settlement.innovations.indexOf(name), 1);
    }

    if(InnovationConfig[name].settlementEffects) {
        let effects = InnovationConfig[name].settlementEffects;

        if(effects.survivalLimit) {
            settlement.survivalLimit -= effects.survivalLimit;
        }
    }

    if(settlement.innovationDeck.indexOf(name) === -1) {
        // always return starting innovations to the deck
        if(InnovationConfig[name].default === true) {
            settlement.innovationDeck.push(name);
        }

        // also return to deck if the innovation that unlocks it is still around
        if(Array.isArray(InnovationConfig[name].consequenceOf)) {
            for(let i = 0; i < InnovationConfig[name].consequenceOf.length; i++) {
                if(settlement.innovations.indexOf(InnovationConfig[name].consequenceOf[i]) !== -1) {
                    settlement.innovationDeck.push(name);
                }
            }
        } else if(settlement.innovations.indexOf(InnovationConfig[name].consequenceOf) !== -1) {
            settlement.innovationDeck.push(name);
        }
    }

    return settlement;
}
