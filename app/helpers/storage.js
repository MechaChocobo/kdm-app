import Toast from 'react-native-simple-toast';
import { getInnovationConfig, getResourceConfig, getGearConfig, getLocationConfig, getDisorderConfig, getFightingArtConfig } from "./helpers";
import { getResourcesForOverview, getResources } from "../selectors/resource-storage";
import { getGear } from "../selectors/gear-storage";
import { getInnovations } from "../selectors/innovations";
import { getAliveSurvivors } from "../selectors/survivors";
import { getLocations } from "../selectors/settlement";
import { t } from './intl';

/**
 * {
 *   resourceTypes: {hide: 1, bone: 2}
 *   resources: [{location: "whiteLion", name: "greatCatBones", qty: 1}]
 *   gear: [{location: "skinnery", name: "rawhideHeadBand", qty: 1}]
 *   requires: [{type: "innovationKeyword", value: "heat"}, {type: "disorder", value: "prey"}]
 * }
 */
export function isGearCraftable(cost, state) {
    if(!cost) {
        return false;
    }

    // if there are multiple options for crafting only 1 needs to be valid
    if(Array.isArray(cost)) {
        let result = true;
        for(let i = 0; i < cost.length; i++) {
            result = isGearCraftable(cost[i], state);
            if(result === true) {
                return true;
            }
        }

        return result;
    }

    let errors = [];

    const ResourceConfig = getResourceConfig();

    // fills any missing keys to empty
    cost = {...{resourceTypes: {}, resources: [], gear: [], requires: []}, ...cost};

    const resourceTypes = cost.resourceTypes;
    const resources = cost.resources;
    const gearCost = cost.gear;
    const prerequisites = cost.requires;

    let resourceStorage = JSON.parse(JSON.stringify(getResources(state)));
    let resourceCounts = JSON.parse(JSON.stringify(getResourcesForOverview(state)));

    for(let i = 0; i < resources.length; i++) {
        const resource = ResourceConfig[resources[i].location][resources[i].name];
        if(!resourceStorage[resources[i].location]) {
            let title = resources[i].name;
            if(ResourceConfig[resources[i].location] && ResourceConfig[resources[i].location][resources[i].name]) {
                title = ResourceConfig[resources[i].location][resources[i].name].title;
            }
            errors.push(t("Missing required resource %resource%", {resource: title}));
            continue;
        }

        if(resources[i].qty > resourceStorage[resources[i].location][resources[i].name]) {
            errors.push(t("Missing required resource %resource%", {resource: ResourceConfig[resources[i].location][resources[i].name].title}));
        }

        // since this resource is required subtract it from our keywords
        if(resource.keywords) {
            for(let j = 0; j < resource.keywords.length; j++) {
                if(resourceCounts[resource.keywords[j]]) {
                    resourceCounts[resource.keywords[j]] = resourceCounts[resource.keywords[j]] - resources[i].qty;
                }
            }
        }

        resourceStorage[resources[i].location][resources[i].name] = (resourceStorage[resources[i].location][resources[i].name] - resources[i].qty);
    }

    let numFound = 0;
    for(let keyword in resourceTypes) {
        // special case -- "any" means any resource counts
        // should always be passed last if that works
        if(keyword === "any") {
            const numResources = getNumResources(resourceStorage);
            if(resourceTypes[keyword] > (numResources - numFound)) {
                errors.push(t("Not enough resources - have %numHave%, need %numNeed%", {numHave: numResources, numNeed: resourceTypes[keyword]}));
            }
        }

        if(resourceTypes[keyword] > resourceCounts[keyword]) {
            errors.push(t("Not enough %keyword% - have %numHave%, need %numNeed%", {keyword, numHave: resourceCounts[keyword], numNeed: resourceTypes[keyword]}));
        } else {
            numFound += resourceTypes[keyword];
        }
    }

    const gearStorage = getGear(state);
    for(let i = 0; i < gearCost.length; i++) {
        if(!gearStorage[gearCost[i].location]) {
            const GearConfig = getGearConfig();
            let gear = gearCost[i].name;
            if(GearConfig[gearCost[i].location] && GearConfig[gearCost[i].location][gearCost[i].name]) {
                gear = GearConfig[gearCost[i].location][gearCost[i].name].title;
            }
            errors.push(t("Missing required gear %gear%", {gear}));
            continue;
        }

        if(gearCost[i].qty > gearStorage[gearCost[i].location][gearCost[i].name]) {
            const GearConfig = getGearConfig();
            errors.push(t("Missing required gear %gear%", {gear: GearConfig[gearCost[i].location][gearCost[i].name].title}));
        }
    }

    for(let i = 0; i < prerequisites.length; i++) {
        let req = prerequisites[i];
        if(req.type === "innovationKeyword") {
            const innovations = getInnovations(state);
            const InnovationConfig = getInnovationConfig();
            let found = false;
            for(let j = 0; j < innovations.length; j++) {
                const innovation = InnovationConfig[innovations[j]];
                if(innovation.keywords && innovation.keywords.indexOf(req.value) !== -1) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                errors.push(t("Missing innovation that grants %type%", {type: req.value}));
            }
        } else if(req.type === "innovation") {
            const innovations = getInnovations(state);
            if(innovations.indexOf(req.value) === -1) {
                const InnovationConfig = getInnovationConfig();
                errors.push(t("Missing required innovation %name%", {name: InnovationConfig[req.value].title}));
            }
        } else if(req.type === "location") {
            const locations = getLocations(state);
            if(locations.indexOf(req.value) === -1) {
                const LocationConfig = getLocationConfig();
                errors.push(t("Missing required location %name%", {name: LocationConfig[req.value].title}));
            }
        } else if(req.type === "disorder") {
            const survivors = getAliveSurvivors(state);
            let found = false;
            for(let j = 0; j < survivors.length; j++) {
                if(survivors[j].disorders.indexOf(req.value) !== -1) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                const DisorderConfig = getDisorderConfig();
                errors.push(t("Requires a survivor with the disorder %name%", {name: DisorderConfig[req.value].title}));
            }

        } else if(req.type === "fightingArt") {
            const survivors = getAliveSurvivors(state);
            let found = false;
            for(let j = 0; j < survivors.length; j++) {
                if(survivors[j].fightingArts.indexOf(req.value) !== -1) {
                    found = true;
                    break;
                }
            }

            if(!found) {
                const FAConfig = getFightingArtConfig();
                errors.push(t("Requires a survivor with the fighting art %name%", {name: FAConfig[req.value].title}));
            }
        }
    }

    return errors.length > 0 ? errors.join('\n') : true;
}

export function orderResourcesByCraftingPriority(resourceStorage) {
    const ResourceConfig = getResourceConfig();

    let resources = [];

    for(let location in resourceStorage) {
        for(let resource in resourceStorage[location]) {
            if(resourceStorage[location][resource] > 0) {
                for(let i = 0; i < resourceStorage[location][resource]; i++) {
                    resources.push({location, name: resource});
                }
            }
        }
    }

    resources.sort(sortResourcesByCrafting(resources, ResourceConfig));

    return resources;
}

export function sortResourcesByCrafting(resources, config) {
    return (a, b) => {
        let aPriority = getPriority(config[a.location][a.name]);
        let bPriority = getPriority(config[b.location][b.name]);

        if(aPriority > bPriority) {
            return 1;
        } else if(aPriority < bPriority) {
            return -1;
        } else {
            return 0;
        }
    }
}

function getPriority(config) {
    if(config.craftingPriority !== undefined) {
        return config.craftingPriority;
    }

    if(config.keywords && config.keywords.length > 0) {
        let keywords = [...config.keywords];
        if(keywords.indexOf("consumable") !== -1) {
            keywords.splice(keywords.indexOf("consumable"), 1);
        }

        if(keywords.length > 0) {
            return keywords.length;
        } else {
            return 1;
        }
    }

    return 1;
}

export function getDefaultResource(type, resources) {
    let ResourceConfig = getResourceConfig();
    let selected = null;

    if(type === "any") {
        selected = resources[0];
        resources.splice(0, 1);
    } else {
        for(let i = 0; i < resources.length; i++) {
            if(
                ResourceConfig[resources[i].location][resources[i].name].keywords &&
                ResourceConfig[resources[i].location][resources[i].name].keywords.indexOf(type) !== -1
            ) {
                selected = resources[i];
                resources.splice(i, 1);
                break;
            }
        }
    }


    return [selected, resources];
}

function getNumResources(resources) {
    let num = 0;

    for (const type in resources) {
        for (const item in resources[type]) {
            if(resources[type][item] > 0) {
                num += resources[type][item];
            }
        }
    }

    return num;
}
