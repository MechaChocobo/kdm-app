import maleNames from '../data/maleNames';
import femaleNames from '../data/femaleNames';
import { getRandomInt } from './helpers';

export function getRandomName(gender) {
    let data = gender === "M" ? maleNames : femaleNames;

    let total = 0;
    let categoryCounts = {};
    for(let category in data) {
        categoryCounts[category] = data[category].length;
        total += data[category].length;
    }

    let choice = getRandomInt(total);
    let seen = 0;
    for(let category in data) {
        if(choice >= categoryCounts[category] + seen) {
            seen += categoryCounts[category];
            continue
        }

        return data[category][choice - seen];
    }
}
