import { createSelector } from 'reselect';
import { MILESTONE_TYPE_GLOBAL } from '../reducers/custom-expansions';

const getCustomExpansions = (state) => {
    return Object.values(state.customExpansions);
}

const getSettlementId = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return settlementId;
}

const getMilestones = state => state.milestones;

export const getGlobalMilestones = createSelector([getCustomExpansions], (expansions) => {
    let milestones = {};

    for(let i = 0; i < expansions.length; i++) {
        if(expansions[i].milestones && Object.keys(expansions[i].milestones).length > 0) {
            let expStones = Object.values(expansions[i].milestones);
            for(let j = 0; j < expStones.length; j++) {
                if(expStones[j].type === MILESTONE_TYPE_GLOBAL) {
                    milestones[expStones[j].id] = expStones[j];
                }
            }
        }
    }

    return milestones;
});

export const getSettlementMilestones = createSelector([getSettlementId, getMilestones], (settlementId, milestones) => {
    let settlementMilestones = {};
    for(let id in milestones) {
        if(milestones[id].settlementConditions && Object.keys(milestones[id].settlementConditions).length > 0) {
            settlementMilestones[id] = {
                conditions: {}
            };
            for(let conditionId in milestones[id].settlementConditions) {
                if(milestones[id].settlementConditions[conditionId].settlements[settlementId]) {
                    settlementMilestones[id].conditions[conditionId] = milestones[id].settlementConditions[conditionId].settlements[settlementId];
                }
            }
        }
    }

    return settlementMilestones;
});
