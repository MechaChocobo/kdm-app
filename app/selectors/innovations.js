import { createSelector } from 'reselect';
import { getInnovationConfig } from '../helpers/helpers';

export const getInnovations = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return state.settlementReducer.settlements[settlementId].innovations;
}

export const getDepartingTexts = createSelector([getInnovations], (innovations) => {
    let InnovationConfig = getInnovationConfig();
    let texts = [];
    for(let i = 0; i < innovations.length; i++) {
        if(InnovationConfig[innovations[i]].departingText) {
            texts.push(InnovationConfig[innovations[i]].departingText);
        }
    }

    return texts;
});
