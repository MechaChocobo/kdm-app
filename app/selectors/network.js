import { createSelector } from 'reselect';

const getNetwork = (state) => {
    return state.networkReducer;
}

export const isNetworked = createSelector([getNetwork], (network) => {
    if(network.server !== null) {
        return true;
    }

    if(network.client !== null) {
        return true;
    }

    return false;
});

export const isDisconnectedClient = createSelector([getNetwork], (network) => {
    let disconnected = false;

    if(
        (!network.client && network.clientConnected) ||
        network.clientError !== ''
    ) {
        disconnected = true;
    }

    if(network.loading) {
        disconnected = false;
    }

    return disconnected;
});

export const shouldShowErrorControls = createSelector([getNetwork], (network) => {
    if(network.errorControlsOverride === true) {
        return true;
    }

    let disconnected = false;

    if(
        (!network.client && network.clientConnected) ||
        network.clientError !== ''
    ) {
        disconnected = true;
    }

    return disconnected;
});
