import { createSelector } from 'reselect';
import { sortArrayByName, getNumDragonTraits } from '../helpers/helpers';

const getSurvivors = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return state.survivorReducer.survivors[settlementId];
}

const getSurvivorSortOrder = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return state.settlementReducer.settlements[settlementId].survivorSortOrder;
}

const getSurvivorSortAttribute = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return state.settlementReducer.settlements[settlementId].survivorSortAttr;
}

const getPinnedIds = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    let pinned = state.survivorReducer.pinnedSurvivors ? state.survivorReducer.pinnedSurvivors[settlementId] : {};
    if(!pinned) {
        pinned = {};
    }
    return pinned;
}

export const getAliveSurvivors = createSelector([getSurvivors,getSurvivorSortOrder, getSurvivorSortAttribute, getPinnedIds], (survivors, order, attribute, pinned) => {
    survivors = Object.values(survivors);
    survivors = survivors.filter(survivor => survivor.dead === false);

    return survivors.sort((a, b) => {
        if(pinned[a.id] === true && !pinned[b.id]) {
            return -1;
        } else if(!pinned[a.id] && pinned[b.id] === true) {
            return 1;
        }

        if(attribute === "name") {
            if(a.returning === true && !b.returning) {
                return -1;
            } else if(!a.returning && b.returning === true) {
                return 1;
            }
        }

        return compareSurvivors(a, b, attribute, order);
    });
});

function compareSurvivors(a, b, attr, order) {
    let aComp = getSurvivorCompAttr(a, attr);
    let bComp = getSurvivorCompAttr(b, attr);

    if(aComp > bComp) {
        return order === "asc" ? 1 : -1;
    } else if(aComp < bComp) {
        return order === "asc" ? -1 : 1;
    } else {
        return 0;
    }
}

function getSurvivorCompAttr(survivor, attr) {
    if(attr === "name") {
        return survivor.name.toUpperCase();
    } else if(attr === "xp") {
        return survivor.xp;
    } else if(attr === "courage") {
        return survivor.courage;
    } else if(attr === "understanding") {
        return survivor.understanding;
    } else if(attr === "survival") {
        return survivor.survival;
    } else if(attr === "insanity") {
        return survivor.insanity.value;
    } else if(attr === "weaponProfName") {
        return survivor.weaponProficiency.weapon;
    } else if(attr === "weaponProfAmount") {
        return survivor.weaponProficiency.rank;
    } else if(attr === "str") {
        return survivor.attributes.STR;
    } else if(attr === "mov") {
        return survivor.attributes.MOV;
    } else if(attr === "acc") {
        return survivor.attributes.ACC;
    } else if(attr === "eva") {
        return survivor.attributes.EVA;
    } else if(attr === "lck") {
        return survivor.attributes.LCK;
    } else if(attr === "spd") {
        return survivor.attributes.SPD;
    } else if(attr === "numSevereInjuries") {
        return survivor.severeInjuries.length;
    } else if(attr === "numFA") {
        return survivor.fightingArts.length;
    } else if(attr === "numDisorders") {
        return survivor.disorders.length;
    } else if(attr === "numDragonTraits") {
        return getNumDragonTraits(survivor.constellationMap);
    } else if(attr === "constellation") {
        return survivor.constellation;
    } else if(attr === "color") {
        return (survivor.departingColor ? survivor.departingColor : "") + survivor.name;
    }
}

export const getDeadSurvivors = createSelector([getSurvivors], (survivors) => {
    survivors = Object.values(survivors);
    survivors = survivors.filter(survivor => survivor.dead === true);

    return survivors.sort((a, b) => {
        let aTime = a.timeOfDeath ? a.timeOfDeath : 0;
        let bTime = b.timeOfDeath ? b.timeOfDeath : 0;

        return bTime - aTime;
    });
});

export const getAliveMaleSurvivors = createSelector([getSurvivors], (survivors) => {
    survivors = Object.values(survivors);
    survivors = survivors.filter((survivor) => {
        return (survivor.dead === false && survivor.gender === "M")
    });

    return survivors.sort(sortArrayByName);
});

export const getAliveFemaleSurvivors = createSelector([getSurvivors], (survivors) => {
    survivors = Object.values(survivors);
    survivors = survivors.filter((survivor) => {
        return (survivor.dead === false && survivor.gender === "F")
    });

    return survivors.sort(sortArrayByName);
});

export const getDepartingSurvivorIds = createSelector([getSurvivors], (survivors) => {
    survivors = Object.values(survivors);
    survivors = survivors.filter((survivor) => {
        return (survivor.departing !== false && survivor.dead === false)
    });

    let ids = [];
    for(var i = 0; i < survivors.length; i++) {
        ids.push(survivors[i].id);
    }

    return ids;
});

export const getDepartingSurvivors = createSelector([getSurvivors,getSurvivorSortOrder, getSurvivorSortAttribute, getPinnedIds], (survivors, order, attribute, pinned) => {
    survivors = Object.values(survivors);
    survivors = survivors.filter((survivor) => {
        return (survivor.departing !== false && survivor.dead === false)
    });

    return survivors.sort((a, b) => {
        if(pinned[a.id] === true && !pinned[b.id]) {
            return -1;
        } else if(!pinned[a.id] && pinned[b.id] === true) {
            return 1;
        }

        return compareSurvivors(a, b, attribute, order);
    });
});

export const getSkipNextHuntSurvivors = createSelector([getSurvivors], (survivors) => {
    survivors = Object.values(survivors);
    survivors = survivors.filter((survivor) => {
        return (survivor.skipNextHunt !== false && survivor.dead === false && survivor.departing === false)
    });

    return survivors;
});

export const getPinnedSurvivors = createSelector([getPinnedIds], (pinned) => {
    return pinned ? pinned : {};
});
