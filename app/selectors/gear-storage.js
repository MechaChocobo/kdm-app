import { createSelector } from 'reselect';

export const getGear = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return state.settlementReducer.settlements[settlementId].storage.gear;
}

export const getSortedGear = createSelector([getGear], (gear) => {
    return sortGear(gear);
});

export const getOwnedGear = createSelector([getGear], (gear) => {
    gear = JSON.parse(JSON.stringify(gear));

    for(let location in gear) {
        for(let id in gear[location]) {
            if(gear[location][id] === 0) {
                delete gear[location][id];
            }
        }

        if(Object.keys(gear[location]).length === 0) {
            delete gear[location];
        }
    }

    sorted = sortGear(gear);

    return sorted;
});

function sortGear(gear) {
    let locations = Object.keys(gear);
    locations.sort((a, b) => {
        let first = a.replace(/\s/g,'').toLowerCase();
        let second = b.replace(/\s/g,'').toLowerCase();

        if(first > second) {
            return 1;
        } else if(first < second) {
            return -1;
        } else {
            return 0;
        }
    });

    let sortedGear = {};
    for (let i = 0; i < locations.length; i++) {
        sortedGear[locations[i]] = gear[locations[i]];
    }

    return sortedGear;
}

export const getGearForDetailedOverview = createSelector([getGear], (gear) => {
    let overviewGear = {};
    let storageTypes = Object.keys(gear);
    storageTypes.sort((a, b) => {
        let first = a.replace(/\s/g,'').toLowerCase();
        let second = b.replace(/\s/g,'').toLowerCase();

        if(first > second) {
            return 1;
        } else if(first < second) {
            return -1;
        } else {
            return 0;
        }
    });

    for(var j = 0; j < storageTypes.length; j++) {
        let hasItems = false;
        for (const item in gear[storageTypes[j]]) {
            if(gear[storageTypes[j]][item] > 0) {
                hasItems = true;
                break;
            }
        }

        if(hasItems) {
            overviewGear[storageTypes[j]] = gear[storageTypes[j]];
        }
    }

    return overviewGear;
});

export const getGearCountsByType = createSelector([getGear], (gear) => {
    let summaries = {};

    for (const type in gear) {
        for (const item in gear[type]) {
            if(!summaries[type]) {
                summaries[type] = 0;
            }

            summaries[type] += gear[type][item];
        }
    }

    return summaries;
});
