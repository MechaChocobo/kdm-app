import { createSelector } from 'reselect';
import colors from '../style/colors';
import themes from '../style/themes';

const getSettings = (state) => {
    return state.settingsReducer;
}

export const getColors = createSelector([getSettings], (settings) => {
    let colorList = {};

    if(settings.theme === "dark" || settings.theme === "light") {
        colorList = {...colors, ...settings.colors, ...themes[settings.theme]};
    } else if(settings.theme === "customDark") {
        colorList = {...colors, ...settings.colors, ...themes.dark, ...settings.customDarkThemeColors};
    } else if(settings.theme === "customLight") {
        colorList = {...colors, ...settings.colors, ...themes.light, ...settings.customLightThemeColors};
    }

    return colorList;
});

export const getCustomDarkThemeColors = createSelector([getSettings], (settings) => {
   return {...themes.dark, ...settings.customDarkThemeColors};
});

export const getCustomLightThemeColors = createSelector([getSettings], (settings) => {
   return {...themes.light, ...settings.customLightThemeColors};
});
