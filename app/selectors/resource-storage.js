import { createSelector } from 'reselect';
import { getResourceConfig } from '../helpers/helpers';

export const getResources = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return state.settlementReducer.settlements[settlementId].storage.resources;
}

const getExpansions = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return state.settlementReducer.settlements[settlementId].expansions;
}

const getResourceSummaryTypes = (expansions) => {
    let types = {
        "bone": 0,
        "hide": 0,
        "organ": 0,
        "scrap": 0,
        "leather": 0,
        "iron": 0,
    };

    if(expansions['flower-knight'] === "all") {
        types.flower = 0;
    }

    if(expansions['dung-beetle-knight'] === "all") {
        types.dung = 0;
    }

    if(expansions['spidicules'] === "all") {
        types.silk = 0;
    }

    return types;
};

export const getSortedResources = createSelector([getResources], (resources) => {
    let storageTypes = Object.keys(resources);
    storageTypes.sort();
    let sortedResources = {};
    for (let i = 0; i < storageTypes.length; i++) {
        sortedResources[storageTypes[i]] = resources[storageTypes[i]];
    }

    return sortedResources;
});

export const getResourcesForDetailedOverview = createSelector([getResources], (resources) => {
    let overviewResources = {};
    let storageTypes = Object.keys(resources);
    storageTypes.sort();

    for(var j = 0; j < storageTypes.length; j++) {
        let hasItems = false;
        for (const item in resources[storageTypes[j]]) {
            if(resources[storageTypes[j]][item] > 0) {
                hasItems = true;
                break;
            }
        }

        if(hasItems) {
            overviewResources[storageTypes[j]] = resources[storageTypes[j]];
        }
    }

    return overviewResources;
});

export const getResourcesForOverview = createSelector([getResources, getExpansions], (resources, expansions) => {
    let overview = getResourceSummaryTypes(expansions);
    let ResourceConfig = getResourceConfig();

    for (const type in resources) {
        for (const item in resources[type]) {
            if(resources[type][item] > 0) {
                if(ResourceConfig[type][item].keywords && ResourceConfig[type][item].keywords.length) {
                    for (let j = 0; j < ResourceConfig[type][item].keywords.length; j++) {
                        const keyword = ResourceConfig[type][item].keywords[j];
                        if(overview[keyword] !== undefined) {
                            overview[keyword] = overview[keyword] + resources[type][item];
                        }
                    }
                }
            }
        }
    }

    return overview;
});

export const getResourceCountsByType = createSelector([getResources], (resources) => {
    let summaries = {};

    for (const type in resources) {
        for (const item in resources[type]) {
            if(!summaries[type]) {
                summaries[type] = 0;
            }

            summaries[type] += resources[type][item];
        }
    }

    return summaries;
});
