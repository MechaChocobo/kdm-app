import { createSelector } from 'reselect';
import { getSettlementEventConfig } from '../helpers/helpers';

const getReturningSurvivors = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    let survivors = Object.values(state.survivorReducer.survivors[settlementId]);
    let ly = state.settlementReducer.settlements[settlementId].ly;

    return survivors.filter(survivor => survivor.returning === ly);
}

const getCurrentSettlementEvents = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    let ly = state.settlementReducer.settlements[settlementId].ly;
    return state.timelineReducer[settlementId][ly].settlementEvents;
}

export const getCurrentYearEndeavors = createSelector([getReturningSurvivors, getCurrentSettlementEvents], (survivors, events) => {
    let SettlementEventConfig = getSettlementEventConfig();
    let endeavors = [];
    survivors.sort((a, b) => {
        let aName = a.name.toUpperCase();
        let bName = b.name.toUpperCase();

        if(aName > bName) {
            return 1;
        } else if(aName < bName) {
            return -1;
        } else {
            return 0;
        }
    });

    events.sort();

    for(var i = 0; i < events.length; i++) {
        if(SettlementEventConfig[events[i]].settlementEffects && SettlementEventConfig[events[i]].settlementEffects.endeavors) {
            endeavors.push({title: SettlementEventConfig[events[i]].title, endeavors: SettlementEventConfig[events[i]].settlementEffects.endeavors, type: "settlementEvent"});
        }
    }

    for(var i = 0; i < survivors.length; i++) {
        if(survivors[i].abilities.indexOf('matchmaker') !== -1) {
            endeavors.push({title: survivors[i].name, endeavors: [{title: 'Matchmaker', endeavorCost: 1, description: "Once per year you may trigger Intimacy", type: "survivorAbility"}]});
        }
    }


    return endeavors;
});
