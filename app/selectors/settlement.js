import { createSelector } from 'reselect';
import { getFightingArtConfig, getInnovationConfig, sortArrayByTitle, getStoryEventConfig, getShowdownConfig, getPrinciplesConfig, getDisorderConfig, getSettlementEventConfig, getLocationConfig, getWeaponProfConfig, getGearConfig, getResourceConfig, getAbilityConfig, getArmorConfig, sortIdArrayByConfigTitle, sortItemsByExpansion, sortStorageByExpansion } from '../helpers/helpers';
import { getGear } from './gear-storage';
import { getResources } from './resource-storage';

export const getSettlementId = (state) => {
    let settlementId = state.pageReducer.settlementId ? state.pageReducer.settlementId : state.pageReducer.prevSettlementId;
    return settlementId;
}

export const getInnovations = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].innovations;
}

export const getAllInnovations = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allInnovations;
}

export const getLocations = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].locations;
}

export const getAllLocations = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allLocations;
}

const getFightingArts = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allFightingArts;
}

const getDisorders = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allDisorders;
}

const getAbilities = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allAbilities;
}

const getArmorSets = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allArmorSets;
}

const getSettlementEvents = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allSettlementEvents;
}

const getSettlements = (state) => {
    return Object.values(state.settlementReducer.settlements);
}

const getStoryEvents = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allStoryEvents.concat(state.settlementReducer.settlements[settlementId].customStoryEvents);
}

const getShowdowns = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allShowdowns;
}

const getQuarryShowdowns = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].quarryShowdowns;
}

const getNemesisEncounters = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allNemesisEncounters;
}

const getPrinciples = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].principles;
}

const getWeaponProficiencies = (state) => {
    let settlementId = getSettlementId(state);
    return state.settlementReducer.settlements[settlementId].allProficiencies;
}

export const getDepartingSurvival = createSelector([getInnovations], (innovations) => {
    let InnovationConfig = getInnovationConfig();

    let survival = 0;
    for(var i = 0; i < innovations.length; i++) {
        if(InnovationConfig[innovations[i]].settlementEffects && InnovationConfig[innovations[i]].settlementEffects.survivalDepart) {
            survival += InnovationConfig[innovations[i]].settlementEffects.survivalDepart;
        }
    }

    return survival;
});

export const getDepartingInsanity = createSelector([getInnovations], (innovations) => {
    let InnovationConfig = getInnovationConfig();

    let insanity = 0;
    for(var i = 0; i < innovations.length; i++) {
        if(InnovationConfig[innovations[i]].settlementEffects && InnovationConfig[innovations[i]].settlementEffects.insanityDepart) {
            insanity += InnovationConfig[innovations[i]].settlementEffects.insanityDepart;
        }
    }

    return insanity;
});

export const getNonSecretFightingArts = createSelector([getFightingArts], (arts) => {
    let FightingArtConfig = getFightingArtConfig();

    return arts.filter((art) => {
        if(!FightingArtConfig[art]) {
            return false;
        }

        return FightingArtConfig[art].type !== "secret";
    })
});

export const getActiveSettlements = createSelector([getSettlements], (settlements) => {
    return settlements.filter((settlement) => {
        return settlement.archived === false;
    })
});

export const getArchivedSettlements = createSelector([getSettlements], (settlements) => {
    return settlements.filter((settlement) => {
        return settlement.archived === true;
    })
});

export const getSortedStoryEvents = createSelector([getStoryEvents], (events) => {
    return events.sort(sortArrayByTitle(getStoryEventConfig()));
});

export const getSortedShowdowns = createSelector([getShowdowns], (events) => {
    return events.sort(sortArrayByTitle(getShowdownConfig()));
});

export const getSortedQuarryShowdowns = createSelector([getQuarryShowdowns], (events) => {
    return events.sort(sortArrayByTitle(getShowdownConfig()));
});

export const getSortedNemesisEncounters = createSelector([getNemesisEncounters], (events) => {
    return events.sort(sortArrayByTitle(getShowdownConfig()));
});

export const getSortedSpecialShowdowns = createSelector([getShowdowns, getNemesisEncounters], (showdowns, encounters) => {
    return showdowns.concat(encounters).sort(sortArrayByTitle(getShowdownConfig()));
});

export const getSortedArmorSets = createSelector([getArmorSets], (armor) => {
    const config = getArmorConfig();
    return sortIdArrayByConfigTitle(armor, config);
});

export const getSettlementFightingArtsByExpansion = createSelector([getFightingArts], (fightingArts) => {
    const FAConfig = getFightingArtConfig();
    return sortItemsByExpansion(fightingArts, FAConfig);
});

export const getSettlementInnovationsByExpansion = createSelector([getAllInnovations], (innovations) => {
    const InnovationConfig = getInnovationConfig();
    return sortItemsByExpansion(innovations, InnovationConfig);
});

export const getSettlementDisordersByExpansion = createSelector([getDisorders], (disorders) => {
    const DisorderConfig = getDisorderConfig();
    return sortItemsByExpansion(disorders, DisorderConfig);
});

export const getSettlementEventsByExpansion = createSelector([getSettlementEvents], (events) => {
    const config = getSettlementEventConfig();
    return sortItemsByExpansion(events, config);
});

export const getSettlementLocationsByExpansion = createSelector([getAllLocations], (locations) => {
    const config = getLocationConfig();
    return sortItemsByExpansion(locations, config);
});

export const getWeaponProficienciesByExpansion = createSelector([getWeaponProficiencies], (proficiencies) => {
    const config = getWeaponProfConfig();
    return sortItemsByExpansion(proficiencies, config);
});

export const getSettlementGearByExpansion = createSelector([getGear], (gear) => {
    const config = getGearConfig();
    return sortStorageByExpansion(gear, config);
});

export const getSettlementResourcesByExpansion = createSelector([getResources], (resources) => {
    const config = getResourceConfig();
    return sortStorageByExpansion(resources, config);
});

export const getSettlementAbilitiesByExpansion = createSelector([getAbilities], (abilities) => {
    const config = getAbilityConfig();
    return sortItemsByExpansion(abilities, config);
});

export const getSettlementArmorSetsByExpansion = createSelector([getArmorSets], (armor) => {
    const config = getArmorConfig();
    return sortItemsByExpansion(armor, config);
});

export const getSettlementBonuses = createSelector([getInnovations, getPrinciples], (innovations, principles) => {
    let InnovationConfig = getInnovationConfig();
    let PrinciplesConfig = getPrinciplesConfig();

    let bonuses = {};

    for(let i = 0; i < innovations.length; i++) {
        let innovation = InnovationConfig[innovations[i]];
        bonuses = extractBonuses(bonuses, 'innovation', innovation, innovations[i]);
    }

    for(let type in principles) {
        if(principles[type] !== false) {
            bonuses = extractBonuses(bonuses, 'principle', PrinciplesConfig[type][principles[type]], type + ';' + principles[type]);
        }
    }

    return bonuses;
});

export function extractBonuses(bonuses, type, config, id) {
    if(config.settlementEffects) {
        if(config.settlementEffects.survivalLimit !== undefined) {
            bonuses = addBonusItem(bonuses, 'survivalLimit', {
                type,
                id,
                value: config.settlementEffects.survivalLimit,
            });
        }

        if(
            config.settlementEffects.survivalDepart !== undefined ||
            config.settlementEffects.insanityDepart !== undefined ||
            config.departingText !== undefined
        ) {
            let value = {};
            if(config.settlementEffects.survivalDepart !== undefined) {
                value.survivalDepart = config.settlementEffects.survivalDepart;
            }

            if(config.settlementEffects.insanityDepart !== undefined) {
                value.insanityDepart = config.settlementEffects.insanityDepart;
            }

            if(config.departingText !== undefined) {
                value.departingText = config.departingText;
            }

            bonuses = addBonusItem(bonuses, 'departing', {
                type,
                id,
                value,
            });
        }

    }

    if(config.allNewSurvivors) {
        let tempConfig = JSON.parse(JSON.stringify(config.allNewSurvivors));
        if(tempConfig.survivalActions) {
            bonuses = addBonusItem(bonuses, 'survivalActions', {
                type,
                id,
                value: Object.keys(tempConfig.survivalActions)[0],
            });
            delete tempConfig.survivalActions;
        }

        if(Object.keys(tempConfig).length > 0) {
            bonuses = addBonusItem(bonuses, 'allNewSurvivors', {
                type,
                id,
                value: tempConfig,
            });
        }
    }

    if(config.newBornEffects) {
        bonuses = addBonusItem(bonuses, 'newBornEffects', {
            type,
            id,
            value: config.newBornEffects,
        });
    }

    return bonuses;
}

function addBonusItem(obj, key, value) {
    if(!obj[key]) {
        obj[key] = [];
    }

    obj[key].push(value);

    return obj;
}
